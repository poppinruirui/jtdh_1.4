
xhc2b.png
size: 2001,483
format: RGBA8888
filter: Linear,Linear
repeat: none
balllight01
  rotate: false
  xy: 1758, 58
  size: 148, 148
  orig: 148, 148
  offset: 0, 0
  index: -1
c1
  rotate: false
  xy: 1919, 265
  size: 81, 85
  orig: 81, 85
  offset: 0, 0
  index: -1
c2
  rotate: true
  xy: 1388, 253
  size: 50, 69
  orig: 50, 69
  offset: 0, 0
  index: -1
c2z
  rotate: true
  xy: 1388, 201
  size: 50, 69
  orig: 50, 69
  offset: 0, 0
  index: -1
c3
  rotate: false
  xy: 1919, 352
  size: 65, 131
  orig: 65, 131
  offset: 0, 0
  index: -1
c3z
  rotate: true
  xy: 671, 45
  size: 65, 131
  orig: 65, 131
  offset: 0, 0
  index: -1
c4
  rotate: false
  xy: 934, 42
  size: 59, 58
  orig: 59, 58
  offset: 0, 0
  index: -1
c4z
  rotate: false
  xy: 1908, 46
  size: 59, 58
  orig: 59, 58
  offset: 0, 0
  index: -1
c5
  rotate: false
  xy: 1712, 0
  size: 72, 56
  orig: 72, 56
  offset: 0, 0
  index: -1
c6
  rotate: true
  xy: 0, 3
  size: 33, 70
  orig: 33, 70
  offset: 0, 0
  index: -1
c6z
  rotate: true
  xy: 72, 3
  size: 33, 70
  orig: 33, 70
  offset: 0, 0
  index: -1
c7
  rotate: true
  xy: 856, 62
  size: 38, 76
  orig: 38, 76
  offset: 0, 0
  index: -1
c7z
  rotate: true
  xy: 1884, 6
  size: 38, 76
  orig: 38, 76
  offset: 0, 0
  index: -1
c8
  rotate: true
  xy: 1919, 204
  size: 59, 67
  orig: 59, 67
  offset: 0, 0
  index: -1
c8z
  rotate: true
  xy: 856, 1
  size: 59, 67
  orig: 59, 67
  offset: 0, 0
  index: -1
c9
  rotate: false
  xy: 804, 56
  size: 50, 54
  orig: 50, 54
  offset: 0, 0
  index: -1
c9z
  rotate: false
  xy: 804, 0
  size: 50, 54
  orig: 50, 54
  offset: 0, 0
  index: -1
ca
  rotate: true
  xy: 1786, 10
  size: 46, 96
  orig: 46, 96
  offset: 0, 0
  index: -1
caz
  rotate: false
  xy: 1908, 106
  size: 46, 96
  orig: 46, 96
  offset: 0, 0
  index: -1
cb
  rotate: false
  xy: 1956, 156
  size: 45, 46
  orig: 45, 46
  offset: 0, 0
  index: -1
cbz
  rotate: false
  xy: 1956, 108
  size: 45, 46
  orig: 45, 46
  offset: 0, 0
  index: -1
light02
  rotate: false
  xy: 671, 112
  size: 185, 184
  orig: 185, 184
  offset: 0, 0
  index: -1
lightning01a
  rotate: true
  xy: 507, 298
  size: 185, 474
  orig: 185, 474
  offset: 0, 0
  index: -1
lightning01b
  rotate: false
  xy: 1762, 208
  size: 155, 275
  orig: 155, 275
  offset: 0, 0
  index: -1
lightning02a
  rotate: true
  xy: 0, 38
  size: 247, 418
  orig: 247, 418
  offset: 0, 0
  index: -1
lightning02b
  rotate: true
  xy: 1459, 259
  size: 224, 301
  orig: 224, 301
  offset: 0, 0
  index: -1
lightning03
  rotate: true
  xy: 0, 287
  size: 196, 505
  orig: 196, 505
  offset: 0, 0
  index: -1
lightning04
  rotate: true
  xy: 983, 305
  size: 178, 474
  orig: 178, 474
  offset: 0, 0
  index: -1
particle02
  rotate: false
  xy: 1642, 134
  size: 114, 123
  orig: 114, 123
  offset: 0, 0
  index: -1
ring02
  rotate: false
  xy: 420, 37
  size: 249, 248
  orig: 249, 248
  offset: 0, 0
  index: -1
sl01a
  rotate: false
  xy: 858, 102
  size: 142, 194
  orig: 142, 194
  offset: 0, 0
  index: -1
sl01b
  rotate: true
  xy: 1002, 6
  size: 140, 194
  orig: 140, 194
  offset: 0, 0
  index: -1
sl01c
  rotate: true
  xy: 1198, 11
  size: 138, 194
  orig: 138, 194
  offset: 0, 0
  index: -1
sl02a
  rotate: false
  xy: 1394, 3
  size: 105, 105
  orig: 105, 105
  offset: 0, 0
  index: -1
sl02b
  rotate: false
  xy: 1501, 4
  size: 104, 104
  orig: 104, 104
  offset: 0, 0
  index: -1
sl02c
  rotate: false
  xy: 1607, 4
  size: 103, 104
  orig: 103, 104
  offset: 0, 0
  index: -1
sl03a
  rotate: false
  xy: 1002, 148
  size: 191, 155
  orig: 191, 155
  offset: 0, 0
  index: -1
sl03b
  rotate: false
  xy: 1195, 151
  size: 191, 152
  orig: 191, 152
  offset: 0, 0
  index: -1
sl03c
  rotate: false
  xy: 1459, 110
  size: 181, 147
  orig: 181, 147
  offset: 0, 0
  index: -1
