﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffManager : MonoBehaviour {

    public static BuffManager s_Instance = null;

    static Vector3 vecTempPos = new Vector3();

    public enum eBuffType
    {
        raise,
        accelerate,
        off,
    };

    public UIBuffCounter[] m_aryBuffCounter;

    public float m_fGap = 64f;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}

    float m_fTimeElapse = 0f;
	
	// Update is called once per frame
	void Update () {

        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse < 1f)
        {
            return;
        }
        m_fTimeElapse = 0;

        RefreshBuff();

    }

    public void RefreshBuff()
    {
        for (int i = 0; i < m_aryBuffCounter.Length; i++ )
        {
            m_aryBuffCounter[i].gameObject.SetActive( false );
        }

        int nCount = 0;

        UIBuffCounter buff_counter = null; 

        float fRaise = Main.s_Instance.GetRaise();
       
        if (fRaise > 1f)
        {
            buff_counter = m_aryBuffCounter[(int)eBuffType.raise];
            buff_counter.gameObject.SetActive( true );
            vecTempPos.x = -(nCount++) * m_fGap;
            vecTempPos.y = 0;
            vecTempPos.z = 0;
            buff_counter.transform.localPosition = vecTempPos;
            buff_counter._txtValue.text = "x" + fRaise.ToString();
        }

        float fSpeedAccelerate = Main.s_Instance.GetAccelerate_New();// Main.s_Instance.GetSpeedAccelerate();
        if (fSpeedAccelerate  > 1 )
        {
            buff_counter = m_aryBuffCounter[(int)eBuffType.accelerate];
            buff_counter.gameObject.SetActive(true);
            vecTempPos.x = -(nCount++) * m_fGap;
            vecTempPos.y = 0;
            vecTempPos.z = 0;
            buff_counter.transform.localPosition = vecTempPos;

            buff_counter._txtValue.text = "x" + fSpeedAccelerate.ToString();
        }

        float fRealTimeDiscount = TanGeChe.s_Instance.GetRealTimeDiscount();
        if ( TanGeChe.s_Instance.GetRealTimeDiscount() < 0 )
        {
            buff_counter = m_aryBuffCounter[(int)eBuffType.off];
            buff_counter.gameObject.SetActive(true);
            vecTempPos.x = -(nCount++) * m_fGap;
            vecTempPos.y = 0;
            vecTempPos.z = 0;
            buff_counter.transform.localPosition = vecTempPos;

            buff_counter._txtValue.text = ( fRealTimeDiscount * 100f ).ToString("f0") + "%";
        }

    }


} // end class

