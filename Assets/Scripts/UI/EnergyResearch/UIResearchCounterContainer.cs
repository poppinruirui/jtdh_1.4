﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIResearchCounterContainer : MonoBehaviour
{
    public Text _txtIntro;
    public Image _imgBg;

    ResearchCounter m_BoundResearchCounter = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetContainerVisible( bool bVisible )
    {
        _txtIntro.gameObject.SetActive(bVisible);
        _imgBg.gameObject.SetActive(bVisible);
    }

    public void RecycleBoundResearchCounter()
    {
        if ( m_BoundResearchCounter )
        {
            m_BoundResearchCounter.gameObject.SetActive( false );
        }
        m_BoundResearchCounter = null;
    }

    public void BindResearchCounter( ResearchCounter research_counter )
    {
        m_BoundResearchCounter = research_counter;
    }

} // end class
