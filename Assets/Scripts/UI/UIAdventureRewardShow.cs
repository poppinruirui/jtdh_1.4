﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAdventureRewardShow : MonoBehaviour {

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtName;
    public Text _txtNum;
    public Image _imgAvatar;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickMe()
    {
        this.gameObject.SetActive( false );
    }

    public void UpdateInfo(UIAdventureProfitCounter profit)
    {
        switch( profit.m_eProfitType )
        {
            case AdventureManager.eAdventureProfitType.coin_raise_item:
                {
                    _imgAvatar.sprite = ResourceManager.s_Instance.m_aryCoinRaiseItemIcon[profit.m_nItemId];
                }
                break;
            case AdventureManager.eAdventureProfitType.green_cash:
                {
                    _imgAvatar.sprite = ResourceManager.s_Instance.m_aryCoinDiamondItemIcon[profit.m_nItemId];
                }
                break;
        } // end switch
    }


} // end class
