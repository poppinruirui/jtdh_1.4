﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour {

    public static UIManager s_Instance = null;

    // coin fly animation pos
    public GameObject _goStart;
    public GameObject _goStart_Admin;
    public GameObject _goEnd;
    public GameObject _goMiddleLeft;
    public GameObject _goMiddleRight;



    public GameObject[] m_aryVariousUI;
    public GameObject[] m_aryVariousUI_Center;
    public GameObject[] m_aryVariousUI_Bottom;

    public Vector3 vecTempInitPosOfTopButtons = new Vector3();


    public const float OFFSET_FOR_LIUHAI = -80f;
    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();
    Vector3 m_vecScreenBottom_World = new Vector3();

    // init pos
    public Vector3 m_vecBottomButtonsInitPos = new Vector3();
    public Vector3 m_vecGainAndCollect = new Vector3();
    public Vector3 m_vecAdventureAndScience = new Vector3();
    public Vector3 m_vecSubContainerForBigMap = new Vector3();


    public float m_fOffsetToScreenBottom_World = 0;

    // 需要适配的UI
    public GameObject _uiTestKnob;

    public GameObject _uiTopButtons;
    public GameObject _uiBottomButtons;
    public GameObject _uiDPS;
    public GameObject _uiCPosButtons;

    public GameObject _uiGainAndCollect;
    public GameObject _uiAdventureAndScience;
    public GameObject _uiSubContainerForBigMap;
    public GameObject _uiSubContainerForBigMap_Content;

    public Canvas _canvasMain;
    public GameObject _UI;
    public CanvasScaler _canvasScaler;

    public Vector2 m_vecCoinFlySeesion0EndPosRangeX = new Vector2();
    public Vector2 m_vecCoinFlySeesion0EndPosRangeY = new Vector2();
    public Vector2 m_vecCoinFlyStartPos = new Vector2();
    public Vector2 m_vecCoinFlyEndPos = new Vector2();
    public float m_fSession0Time = 0.5f;
    public float m_fSession1Time = 1.5f;

    public GameObject _containerFlyingCoins;

    /// <summary>
    /// UI
    /// </summary>
    public GameObject[] m_arySomeCtrlsDueToXingKong;

    // end UI


    public CFrameAnimationEffect _goEffectClick;

    public enum eUiId
    {
        homepage_prestige_btn,
        homepage_container_money,
        homepage_gain_overview,
        homepage_dps_container,
        homepage_skills_panel,
        homepage_bottom_buttons,
        homepage_cheat_btn,

        shoppinmall_top_money_counters,

        bigmap_top_money_counters,


    };

    public Vector2[] m_aryUICtrlPos_1920_1080;
    public Vector2[] m_aryUICtrlPos_2436_1125;
    public GameObject[] m_aryUICtrl;
    public Vector2[] m_aryResolution;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {


        _canvasScaler.matchWidthOrHeight = JTDHSceneManager.STANDARD_HEIGHT / (float)Screen.height;


        CheckIPhoneX();


        Text[] aryTexts =  this.GetComponentsInChildren<Text>();
        for (int i = 0; i < aryTexts.Length; i++ )
        {
            Text txt = aryTexts[i];
            txt.font = ResourceManager.s_Instance._font;
        }
	}
	
	// Update is called once per frame
	void Update () {
        OnTapTap();




    }

    public bool  CheckIPhoneX()
    {
        bool IsIphoneXDevice = false;
        string modelStr = SystemInfo.deviceModel;
#if UNITY_IOS
        // iPhoneX:"iPhone10,3","iPhone10,6"  iPhoneXR:"iPhone11,8"  iPhoneXS:"iPhone11,2"  iPhoneXS Max:"iPhone11,6"
        IsIphoneXDevice = modelStr.Equals("iPhone10,3") || modelStr.Equals("iPhone10,6") || modelStr.Equals("iPhone11,8") || modelStr.Equals("iPhone11,2") || modelStr.Equals("iPhone11,4");
#endif
      //  Main.s_Instance._txtDebugInfo.text = modelStr;
        return IsIphoneXDevice;
    }

    public static void UpdateDropdownView(Dropdown dropdownItem, List<string> showNames)
    {
        dropdownItem.options.Clear();
        Dropdown.OptionData tempData;
        for (int i = 0; i < showNames.Count; i++)
        {
            tempData = new Dropdown.OptionData();
            tempData.text = showNames[i];
            dropdownItem.options.Add(tempData);
        }
        dropdownItem.captionText.text = showNames[0];
    }

    public static bool IsPointerOverUI()
    {
        PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
        eventData.pressPosition = Input.mousePosition;
        eventData.position = Input.mousePosition;

        List<RaycastResult> list = new List<RaycastResult>();
        UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);

        return list.Count > 0;

    }

    public void SetSomeUiVisibleDueToStarSky( bool bVisible )
    {
        for (int i = 0; i < m_arySomeCtrlsDueToXingKong.Length; i++ )
        {
            if ( m_arySomeCtrlsDueToXingKong[i] != null )
            {
                m_arySomeCtrlsDueToXingKong[i].SetActive( bVisible );
            }
        }
    }

    
    public void OnTapTap()
    {
        if ( Input.GetMouseButtonDown(0) )
        {
         //   ShowClickEffect();
        }

        if (Input.GetMouseButtonUp(0))
        {
         //   ShowClickEffect();
        }

    }

    public void ShowClickEffect()
    {
        Vector2 _pos = Vector2.one;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_canvasMain.transform as RectTransform,
                Input.mousePosition, _canvasMain.worldCamera, out _pos);
        _goEffectClick.transform.localPosition = _pos;
        _goEffectClick.gameObject.SetActive( true );
        _goEffectClick.BeginPlay(false);

    }

    public void SetUiVisible( bool bVisible )
    {
        _UI.SetActive( bVisible );
    }

    public void OnOpenUI()
    {
        TanGeChe.s_Instance.s_nRecommendCount = 0;
    }


    public void DoSomeAdjust()
    {
        JTDHSceneManager.s_Instance.DoAdjust( _uiDPS );
        JTDHSceneManager.s_Instance.DoAdjust(_uiCPosButtons);

        JTDHSceneManager.s_Instance.DoAdjust(_uiBottomButtons);
        JTDHSceneManager.s_Instance.DoAdjust(_uiTopButtons);

        vecTempPos = _uiBottomButtons.transform.localPosition;
        vecTempPos.y = -(float)Screen.height / 2f;
        _uiBottomButtons.transform.localPosition = vecTempPos;


       

        vecTempPos = _uiTopButtons.transform.localPosition;
        vecTempPos.y = (float)Screen.height / 2f;
        _uiTopButtons.transform.localPosition = vecTempPos;

        vecTempInitPosOfTopButtons = _uiTopButtons.transform.localPosition;

        if ( CheckIPhoneX() )
        {
            vecTempPos = _uiTopButtons.transform.localPosition;
            vecTempPos.y += OFFSET_FOR_LIUHAI;
            _uiTopButtons.transform.localPosition = vecTempPos;

            vecTempPos = _uiBottomButtons.transform.localPosition;
            vecTempPos.y += 70f;
            _uiBottomButtons.transform.localPosition = vecTempPos;


            vecTempPos = _uiSubContainerForBigMap.transform.localPosition;
            vecTempPos.y -= 200f;
           _uiSubContainerForBigMap.transform.localPosition = vecTempPos;

        
        }

        for (int i = 0; i < m_aryVariousUI_Bottom.Length; i++ )
        {
            GameObject the_ui = m_aryVariousUI_Bottom[i];
            if (the_ui == null)
            {
                continue;
            }
            JTDHSceneManager.s_Instance.DoAdjust(the_ui);
            vecTempPos = the_ui.transform.localPosition;
            vecTempPos.y = -(float)Screen.height / 2f;
            the_ui.transform.localPosition = vecTempPos;

        }

        for (int i = 0; i < m_aryVariousUI_Center.Length; i++ )
        {
            GameObject the_ui = m_aryVariousUI_Center[i];
            if (the_ui == null)
            {
                continue;
            }

            JTDHSceneManager.s_Instance.DoAdjust(the_ui);
        }

        for (int i = 0; i < m_aryVariousUI.Length; i++ )
        {
            GameObject the_ui = m_aryVariousUI[i];
            if ( the_ui == null )
            {
                continue;
            }
            JTDHSceneManager.s_Instance.DoAdjust(the_ui);
            vecTempPos = the_ui.transform.localPosition;
            vecTempPos.y = (float)Screen.height / 2f;
            the_ui.transform.localPosition = vecTempPos;
            if (CheckIPhoneX())
            {
                vecTempPos = the_ui.transform.localPosition;
                vecTempPos.y += OFFSET_FOR_LIUHAI;
                the_ui.transform.localPosition = vecTempPos;
            }
        }


        // Main.s_Instance._txtDebugInfo.text = JTDHSceneManager.s_Instance.m_fAdjustShit.ToString();

        m_vecGainAndCollect = _uiGainAndCollect.transform.localPosition;
        m_vecAdventureAndScience = _uiAdventureAndScience.transform.localPosition;



        m_vecSubContainerForBigMap = _uiSubContainerForBigMap.transform.localPosition;
    }

} // end class
