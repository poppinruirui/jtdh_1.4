﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMsgBox : MonoBehaviour {

    public enum eCallBackEventType
    {
        none,
        begin_energy_unlocking, // 开始能源解锁
        refresh_adventrue,      // 重玩探险
    };

    eCallBackEventType m_eType = eCallBackEventType.none;

    public static UIMsgBox s_Instance = null;

    public GameObject _goContainerMain;

    public GameObject _containerOnlyOk;
    public GameObject _containerYesNo;

    public Text _txtMsgOnlyOk;
    public Text _txtMsgYesNo;

    float m_fTimeElapse = 0f;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Loop();
	}

    public ResearchCounter _BoundResearchCounter = null;

    public void ShowMsg(string szMsg, eCallBackEventType eType = eCallBackEventType.none, float fTotalTime = 1)
    {
        m_eType = eType;

        _containerYesNo.SetActive(false);
        _containerOnlyOk.SetActive(false);
        if (m_eType == eCallBackEventType.none)
        {
            _containerOnlyOk.SetActive( true );
        }
        else
        {
            _containerYesNo.SetActive(true);
        }

        _txtMsgOnlyOk.text = szMsg;
        _txtMsgYesNo.text = szMsg;

        m_fTimeElapse = fTotalTime;
        _goContainerMain.SetActive(true);
    }

    void Loop()
    {
        if ( m_fTimeElapse <= 0f )
        {
            return;
        }

        m_fTimeElapse -= Time.deltaTime;

        if (m_fTimeElapse <= 0f)
        {
            _goContainerMain.SetActive( false );
        }
    }

    public void CloseMsg()
    {
        _goContainerMain.SetActive(false);
    }

    public void OnClickButton_Cancel()
    {
        CloseMsg();
    }

    public void OnClickButton_Ok()
    {
        CloseMsg();

        switch ( m_eType )
        {
            case eCallBackEventType.begin_energy_unlocking:
                {
                    _BoundResearchCounter.DoPreUnlock();
                }
                break;
            case eCallBackEventType.refresh_adventrue: //重玩探险
                {
                    AdventureManager_New.s_Instance.ReallyDoRefresh();
                }
                break;
        };

       


    }

}
