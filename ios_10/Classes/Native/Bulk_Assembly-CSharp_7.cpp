﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// AudioManager
struct AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23;
// BaseScale
struct BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063;
// BaseScale/delegateScaleEnd
struct delegateScaleEnd_t697D8B0D83495ED3E1853627A6FCACD68409376F;
// CFrameAnimationEffect
struct CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10;
// DataManager
struct DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30;
// District
struct District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A;
// FreshGuide
struct FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3;
// Lot[]
struct LotU5BU5D_tDF39035F8B8E0E8A8915E650FAC90FE38BFD335F;
// Main
struct Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1;
// Main/sAutoRunLocation[]
struct sAutoRunLocationU5BU5D_t1B4B4B1E8F7FDC3043F544A45744E0D6A98B8864;
// MapManager
struct MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C;
// MoneyCounter
struct MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF;
// MoneyCounter[]
struct MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A;
// MoneyTrigger
struct MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464;
// Paw
struct Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011;
// Paw/delegateMoveEnd
struct delegateMoveEnd_t1F76CEA26063F0B11A971CAB2747FA1BFA8241F7;
// Planet
struct Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B;
// Planet[]
struct PlanetU5BU5D_tFAD0DE71ED69B99F92FC1525DDAE53548F72893D;
// ResearchCounter
struct ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD;
// ResourceManager
struct ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78;
// SceneUiButton
struct SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507;
// SnowFlake
struct SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70;
// Spine.Animation
struct Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482;
// Spine.AnimationState
struct AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0;
// Spine.AnimationState/TrackEntryDelegate
struct TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F;
// Spine.AnimationState/TrackEntryEventDelegate
struct TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE;
// Spine.AnimationStateData
struct AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C;
// Spine.Atlas
struct Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4;
// Spine.EventQueue
struct EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77;
// Spine.ExposedList`1<Spine.Event>
struct ExposedList_1_t0883A674779C02F9CCBF02B511F843FBBD8CEE67;
// Spine.ExposedList`1<Spine.TrackEntry>
struct ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99;
// Spine.ExposedList`1<System.Int32>
struct ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960;
// Spine.ExposedList`1<System.Single>
struct ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F;
// Spine.Pool`1<Spine.TrackEntry>
struct Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF;
// Spine.Skeleton
struct Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782;
// Spine.SkeletonData
struct SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20;
// Spine.TrackEntry
struct TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276;
// Spine.Unity.AtlasAsset
struct AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47;
// Spine.Unity.AtlasAsset[]
struct AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182;
// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh>
struct DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC;
// Spine.Unity.MeshGenerator
struct MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157;
// Spine.Unity.MeshGeneratorDelegate
struct MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4;
// Spine.Unity.SkeletonDataAsset
struct SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5;
// Spine.Unity.SkeletonGraphic
struct SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A;
// Spine.Unity.SkeletonRendererInstruction
struct SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A;
// Spine.Unity.UpdateBonesDelegate
struct UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513;
// StartBelt
struct StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t148D4FE58B48D51DD45913A7B6EAA61E30D4B285;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<AudioManager/eSE,System.Collections.Generic.List`1<UnityEngine.AudioSource>>
struct Dictionary_2_tCB4384C8D69355F95AE7A997BD0EBA8D389A27B2;
// System.Collections.Generic.Dictionary`2<FreshGuide/eGuideType,System.Boolean>
struct Dictionary_2_tE758D927E90AD1ECFDE01DDC3DB458104EDA3D92;
// System.Collections.Generic.Dictionary`2<Main/ePosType,System.Collections.Generic.List`1<Main/sAutoRunLocation>>
struct Dictionary_2_t8BBCAEE5B6551D2028E4CE6BBACCA1D12D8070C0;
// System.Collections.Generic.Dictionary`2<SkillManager/eSkillType,Skill>
struct Dictionary_2_t5BEFDBA4420F57163BF6DF9697D75434371B6E60;
// System.Collections.Generic.Dictionary`2<System.Int32,DataManager/sPlanetConfig>
struct Dictionary_2_t35A7F0D04BB2998E9DD0CB0865E4F0442AE488AF;
// System.Collections.Generic.Dictionary`2<System.Int32,ResearchCounter>
struct Dictionary_2_t1D475F8089C35FF3718700296FB38C143E09149F;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct Dictionary_2_t27BCF11A3E40FEB6B728C0EA39A63FAB1AE16D5C;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8;
// System.Collections.Generic.Dictionary`2<System.Int32,UIResearchCounterContainer>
struct Dictionary_2_tC10E085FDFF6C3AB8005D16563E90234EF657A79;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Sprite[]>
struct Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E;
// System.Collections.Generic.Dictionary`2<System.String,DataManager/sAutomobileConfig>
struct Dictionary_2_t575E591ADF0DE43EE8FBD5432FDB44C619218BBC;
// System.Collections.Generic.Dictionary`2<System.String,DataManager/sPrestigeConfig>
struct Dictionary_2_t6927BC972CD2E0E92AF0DF540164BAF9ECB65E17;
// System.Collections.Generic.Dictionary`2<System.String,DataManager/sTrackConfig>
struct Dictionary_2_t0FCB090B06AD243E1AEFE7A77D6E44B002171473;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UIVehicleCounter>>
struct Dictionary_2_t23E80E3DB14B3FC8A2D8537F3FCAC9C8780F1654;
// System.Collections.Generic.Dictionary`2<System.String,System.Double>
struct Dictionary_2_tD9E97F40821434226F92E86AFDAE50D233468602;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t89F484A77FD6673A172CE80BD0A37031FBD1FD6A;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.String,UIZengShouCounter>
struct Dictionary_2_tA42C789C20DBB051A4F863D661877067E6AE8081;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E;
// System.Collections.Generic.List`1<Admin>
struct List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34;
// System.Collections.Generic.List`1<District/sLevelAndCost>
struct List_1_tB9FA86EA4317FBDAC182273F5D02A08CB5B68D86;
// System.Collections.Generic.List`1<Main/sAutoRunLocation>
struct List_1_t1517B4D31CFE0D5A31B279B7CC226F6BA9FA9E43;
// System.Collections.Generic.List`1<Plane>
struct List_1_t47760497A2262813AF37CFF6B714399A5DE17D22;
// System.Collections.Generic.List`1<System.Double>
struct List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t026D7A8C4D989218772DB3E051A624F753A60859;
// System.Collections.Generic.List`1<UIFlyingCoin>
struct List_1_tB36688DD81A4ADEF07F2EB8C42C12B19153C0C95;
// System.Collections.Generic.List`1<UIItemInBag>
struct List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB;
// System.Collections.Generic.List`1<UIVehicleCounter>
struct List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t64BA96BFC713F221050385E91C868CE455C245D6;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UIAdministratorCounter
struct UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78;
// UIDistrict[]
struct UIDistrictU5BU5D_tDD148C70027D94D63D232B5C9FBB142D5EA721CB;
// UIMainLand[]
struct UIMainLandU5BU5D_tF159C8131B1BA4E2EEDF316C136F4BA6CE07B280;
// UIManager
struct UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C;
// UIPlanet[]
struct UIPlanetU5BU5D_t8587E1E90D0FB3095F0BBAA9DF7CE4A5A6A94298;
// UIPrestige
struct UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C;
// UIStars
struct UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B;
// UIZengShouCounter
struct UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF;
// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Font
struct Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017;
// UnityEngine.TextAsset
struct TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E;
// UnityEngine.TextGenerator
struct TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB;
// UnityEngine.UI.CanvasScaler
struct CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.FontData
struct FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnlockNewPlaneManager
struct UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B;
// WeatherManager
struct WeatherManager_t77BC50C3104A8AAC79F5FEA210AC31B904ECD196;
// dou
struct dou_t6892A900BE5AC1C3D8B4DFBA12DC4445953E1EFD;

extern RuntimeClass* AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_il2cpp_TypeInfo_var;
extern RuntimeClass* DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_il2cpp_TypeInfo_var;
extern RuntimeClass* FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650_il2cpp_TypeInfo_var;
extern RuntimeClass* Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_il2cpp_TypeInfo_var;
extern RuntimeClass* MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_il2cpp_TypeInfo_var;
extern RuntimeClass* TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_il2cpp_TypeInfo_var;
extern RuntimeClass* UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var;
extern RuntimeClass* UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral099600A10A944114AAC406D136B625FB416DD779;
extern String_t* _stringLiteral309784E462C9CE80EC50F54AFDFB1E3B3AB31858;
extern String_t* _stringLiteralA80854C74FC8CD3D0E53284034962DAFBE6B500F;
extern String_t* _stringLiteralC510EA100EEE1C261FE63B56E1F3390BFB85F481;
extern const RuntimeMethod* GameObject_GetComponent_TisSnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_m4B69970D1C279F5DF4E82E0549BA7738FB538EF7_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m70B8A20433AEEDEB942CD3EEC229497AB693E9D6_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E_RuntimeMethod_var;
extern const uint32_t UIZengShouCounter_OnClick_ZengShou_mBF06A58B9F4504B81DF059C063C1967D070335D0_MetadataUsageId;
extern const uint32_t UIZengShouCounter_SetPlanetIdAndDistrictId_m9CCB17E056A92CDCAEAAA29AE78F0063FCFE680F_MetadataUsageId;
extern const uint32_t UnlockNewPlaneManager_Awake_m4544CEBF76793762E75BC39B5C02292718040688_MetadataUsageId;
extern const uint32_t UnlockNewPlaneManager_Begin_m099DE7F42069707FED56E3B0C7E9A76DE85E2178_MetadataUsageId;
extern const uint32_t UnlockNewPlaneManager_End_NewVersion_m354AD213F4DFE5EDE856637A70D10D0377C818A7_MetadataUsageId;
extern const uint32_t UnlockNewPlaneManager_End_m29520DC54380DF192A61130906FE5C2F713377F0_MetadataUsageId;
extern const uint32_t UnlockNewPlaneManager_Loop_NewVersion_mFC19BCACA343BEA3A1A473D122BB84DE40868327_MetadataUsageId;
extern const uint32_t UnlockNewPlaneManager__ctor_mEDBECCBFF99FAF208045C5296CDEC9A1B9EAECAD_MetadataUsageId;
extern const uint32_t WeatherManager_DeleteSnowFlake_mD97E3FFB7ED3A0AA60B8DC14AF9A186B0DF274DC_MetadataUsageId;
extern const uint32_t WeatherManager_NewSnowFlake_mFB9DF3C7E9501BDF52756311194B8765379D12E7_MetadataUsageId;

struct AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182;
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ANIMATIONSTATE_T8C505E02BE0DB4858362AA74C02FB30085250DE0_H
#define ANIMATIONSTATE_T8C505E02BE0DB4858362AA74C02FB30085250DE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState
struct  AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0  : public RuntimeObject
{
public:
	// Spine.AnimationStateData Spine.AnimationState::data
	AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C * ___data_5;
	// Spine.Pool`1<Spine.TrackEntry> Spine.AnimationState::trackEntryPool
	Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF * ___trackEntryPool_6;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.AnimationState::tracks
	ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * ___tracks_7;
	// Spine.ExposedList`1<Spine.Event> Spine.AnimationState::events
	ExposedList_1_t0883A674779C02F9CCBF02B511F843FBBD8CEE67 * ___events_8;
	// Spine.EventQueue Spine.AnimationState::queue
	EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77 * ___queue_9;
	// System.Collections.Generic.HashSet`1<System.Int32> Spine.AnimationState::propertyIDs
	HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * ___propertyIDs_10;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.AnimationState::mixingTo
	ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * ___mixingTo_11;
	// System.Boolean Spine.AnimationState::animationsChanged
	bool ___animationsChanged_12;
	// System.Single Spine.AnimationState::timeScale
	float ___timeScale_13;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Start
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Start_14;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Interrupt
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Interrupt_15;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::End
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___End_16;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Dispose
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Dispose_17;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Complete
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Complete_18;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.AnimationState::Event
	TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE * ___Event_19;

public:
	inline static int32_t get_offset_of_data_5() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___data_5)); }
	inline AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C * get_data_5() const { return ___data_5; }
	inline AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C ** get_address_of_data_5() { return &___data_5; }
	inline void set_data_5(AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C * value)
	{
		___data_5 = value;
		Il2CppCodeGenWriteBarrier((&___data_5), value);
	}

	inline static int32_t get_offset_of_trackEntryPool_6() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___trackEntryPool_6)); }
	inline Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF * get_trackEntryPool_6() const { return ___trackEntryPool_6; }
	inline Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF ** get_address_of_trackEntryPool_6() { return &___trackEntryPool_6; }
	inline void set_trackEntryPool_6(Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF * value)
	{
		___trackEntryPool_6 = value;
		Il2CppCodeGenWriteBarrier((&___trackEntryPool_6), value);
	}

	inline static int32_t get_offset_of_tracks_7() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___tracks_7)); }
	inline ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * get_tracks_7() const { return ___tracks_7; }
	inline ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 ** get_address_of_tracks_7() { return &___tracks_7; }
	inline void set_tracks_7(ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * value)
	{
		___tracks_7 = value;
		Il2CppCodeGenWriteBarrier((&___tracks_7), value);
	}

	inline static int32_t get_offset_of_events_8() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___events_8)); }
	inline ExposedList_1_t0883A674779C02F9CCBF02B511F843FBBD8CEE67 * get_events_8() const { return ___events_8; }
	inline ExposedList_1_t0883A674779C02F9CCBF02B511F843FBBD8CEE67 ** get_address_of_events_8() { return &___events_8; }
	inline void set_events_8(ExposedList_1_t0883A674779C02F9CCBF02B511F843FBBD8CEE67 * value)
	{
		___events_8 = value;
		Il2CppCodeGenWriteBarrier((&___events_8), value);
	}

	inline static int32_t get_offset_of_queue_9() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___queue_9)); }
	inline EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77 * get_queue_9() const { return ___queue_9; }
	inline EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77 ** get_address_of_queue_9() { return &___queue_9; }
	inline void set_queue_9(EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77 * value)
	{
		___queue_9 = value;
		Il2CppCodeGenWriteBarrier((&___queue_9), value);
	}

	inline static int32_t get_offset_of_propertyIDs_10() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___propertyIDs_10)); }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * get_propertyIDs_10() const { return ___propertyIDs_10; }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E ** get_address_of_propertyIDs_10() { return &___propertyIDs_10; }
	inline void set_propertyIDs_10(HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * value)
	{
		___propertyIDs_10 = value;
		Il2CppCodeGenWriteBarrier((&___propertyIDs_10), value);
	}

	inline static int32_t get_offset_of_mixingTo_11() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___mixingTo_11)); }
	inline ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * get_mixingTo_11() const { return ___mixingTo_11; }
	inline ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 ** get_address_of_mixingTo_11() { return &___mixingTo_11; }
	inline void set_mixingTo_11(ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * value)
	{
		___mixingTo_11 = value;
		Il2CppCodeGenWriteBarrier((&___mixingTo_11), value);
	}

	inline static int32_t get_offset_of_animationsChanged_12() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___animationsChanged_12)); }
	inline bool get_animationsChanged_12() const { return ___animationsChanged_12; }
	inline bool* get_address_of_animationsChanged_12() { return &___animationsChanged_12; }
	inline void set_animationsChanged_12(bool value)
	{
		___animationsChanged_12 = value;
	}

	inline static int32_t get_offset_of_timeScale_13() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___timeScale_13)); }
	inline float get_timeScale_13() const { return ___timeScale_13; }
	inline float* get_address_of_timeScale_13() { return &___timeScale_13; }
	inline void set_timeScale_13(float value)
	{
		___timeScale_13 = value;
	}

	inline static int32_t get_offset_of_Start_14() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___Start_14)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Start_14() const { return ___Start_14; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Start_14() { return &___Start_14; }
	inline void set_Start_14(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Start_14 = value;
		Il2CppCodeGenWriteBarrier((&___Start_14), value);
	}

	inline static int32_t get_offset_of_Interrupt_15() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___Interrupt_15)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Interrupt_15() const { return ___Interrupt_15; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Interrupt_15() { return &___Interrupt_15; }
	inline void set_Interrupt_15(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Interrupt_15 = value;
		Il2CppCodeGenWriteBarrier((&___Interrupt_15), value);
	}

	inline static int32_t get_offset_of_End_16() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___End_16)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_End_16() const { return ___End_16; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_End_16() { return &___End_16; }
	inline void set_End_16(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___End_16 = value;
		Il2CppCodeGenWriteBarrier((&___End_16), value);
	}

	inline static int32_t get_offset_of_Dispose_17() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___Dispose_17)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Dispose_17() const { return ___Dispose_17; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Dispose_17() { return &___Dispose_17; }
	inline void set_Dispose_17(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Dispose_17 = value;
		Il2CppCodeGenWriteBarrier((&___Dispose_17), value);
	}

	inline static int32_t get_offset_of_Complete_18() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___Complete_18)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Complete_18() const { return ___Complete_18; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Complete_18() { return &___Complete_18; }
	inline void set_Complete_18(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Complete_18 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_18), value);
	}

	inline static int32_t get_offset_of_Event_19() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___Event_19)); }
	inline TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE * get_Event_19() const { return ___Event_19; }
	inline TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE ** get_address_of_Event_19() { return &___Event_19; }
	inline void set_Event_19(TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE * value)
	{
		___Event_19 = value;
		Il2CppCodeGenWriteBarrier((&___Event_19), value);
	}
};

struct AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0_StaticFields
{
public:
	// Spine.Animation Spine.AnimationState::EmptyAnimation
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * ___EmptyAnimation_0;

public:
	inline static int32_t get_offset_of_EmptyAnimation_0() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0_StaticFields, ___EmptyAnimation_0)); }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * get_EmptyAnimation_0() const { return ___EmptyAnimation_0; }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 ** get_address_of_EmptyAnimation_0() { return &___EmptyAnimation_0; }
	inline void set_EmptyAnimation_0(Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * value)
	{
		___EmptyAnimation_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyAnimation_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATE_T8C505E02BE0DB4858362AA74C02FB30085250DE0_H
#ifndef TRACKENTRY_TC06EF190EA88E3477B5CDDC2AABB2F01B6B12276_H
#define TRACKENTRY_TC06EF190EA88E3477B5CDDC2AABB2F01B6B12276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TrackEntry
struct  TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276  : public RuntimeObject
{
public:
	// Spine.Animation Spine.TrackEntry::animation
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * ___animation_0;
	// Spine.TrackEntry Spine.TrackEntry::next
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * ___next_1;
	// Spine.TrackEntry Spine.TrackEntry::mixingFrom
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * ___mixingFrom_2;
	// System.Int32 Spine.TrackEntry::trackIndex
	int32_t ___trackIndex_3;
	// System.Boolean Spine.TrackEntry::loop
	bool ___loop_4;
	// System.Single Spine.TrackEntry::eventThreshold
	float ___eventThreshold_5;
	// System.Single Spine.TrackEntry::attachmentThreshold
	float ___attachmentThreshold_6;
	// System.Single Spine.TrackEntry::drawOrderThreshold
	float ___drawOrderThreshold_7;
	// System.Single Spine.TrackEntry::animationStart
	float ___animationStart_8;
	// System.Single Spine.TrackEntry::animationEnd
	float ___animationEnd_9;
	// System.Single Spine.TrackEntry::animationLast
	float ___animationLast_10;
	// System.Single Spine.TrackEntry::nextAnimationLast
	float ___nextAnimationLast_11;
	// System.Single Spine.TrackEntry::delay
	float ___delay_12;
	// System.Single Spine.TrackEntry::trackTime
	float ___trackTime_13;
	// System.Single Spine.TrackEntry::trackLast
	float ___trackLast_14;
	// System.Single Spine.TrackEntry::nextTrackLast
	float ___nextTrackLast_15;
	// System.Single Spine.TrackEntry::trackEnd
	float ___trackEnd_16;
	// System.Single Spine.TrackEntry::timeScale
	float ___timeScale_17;
	// System.Single Spine.TrackEntry::alpha
	float ___alpha_18;
	// System.Single Spine.TrackEntry::mixTime
	float ___mixTime_19;
	// System.Single Spine.TrackEntry::mixDuration
	float ___mixDuration_20;
	// System.Single Spine.TrackEntry::interruptAlpha
	float ___interruptAlpha_21;
	// System.Single Spine.TrackEntry::totalAlpha
	float ___totalAlpha_22;
	// Spine.ExposedList`1<System.Int32> Spine.TrackEntry::timelineData
	ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * ___timelineData_23;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.TrackEntry::timelineDipMix
	ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * ___timelineDipMix_24;
	// Spine.ExposedList`1<System.Single> Spine.TrackEntry::timelinesRotation
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___timelinesRotation_25;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Start
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Start_26;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Interrupt
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Interrupt_27;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::End
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___End_28;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Dispose
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Dispose_29;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Complete
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Complete_30;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.TrackEntry::Event
	TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE * ___Event_31;

public:
	inline static int32_t get_offset_of_animation_0() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___animation_0)); }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * get_animation_0() const { return ___animation_0; }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 ** get_address_of_animation_0() { return &___animation_0; }
	inline void set_animation_0(Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * value)
	{
		___animation_0 = value;
		Il2CppCodeGenWriteBarrier((&___animation_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___next_1)); }
	inline TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * get_next_1() const { return ___next_1; }
	inline TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((&___next_1), value);
	}

	inline static int32_t get_offset_of_mixingFrom_2() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___mixingFrom_2)); }
	inline TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * get_mixingFrom_2() const { return ___mixingFrom_2; }
	inline TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 ** get_address_of_mixingFrom_2() { return &___mixingFrom_2; }
	inline void set_mixingFrom_2(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * value)
	{
		___mixingFrom_2 = value;
		Il2CppCodeGenWriteBarrier((&___mixingFrom_2), value);
	}

	inline static int32_t get_offset_of_trackIndex_3() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___trackIndex_3)); }
	inline int32_t get_trackIndex_3() const { return ___trackIndex_3; }
	inline int32_t* get_address_of_trackIndex_3() { return &___trackIndex_3; }
	inline void set_trackIndex_3(int32_t value)
	{
		___trackIndex_3 = value;
	}

	inline static int32_t get_offset_of_loop_4() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___loop_4)); }
	inline bool get_loop_4() const { return ___loop_4; }
	inline bool* get_address_of_loop_4() { return &___loop_4; }
	inline void set_loop_4(bool value)
	{
		___loop_4 = value;
	}

	inline static int32_t get_offset_of_eventThreshold_5() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___eventThreshold_5)); }
	inline float get_eventThreshold_5() const { return ___eventThreshold_5; }
	inline float* get_address_of_eventThreshold_5() { return &___eventThreshold_5; }
	inline void set_eventThreshold_5(float value)
	{
		___eventThreshold_5 = value;
	}

	inline static int32_t get_offset_of_attachmentThreshold_6() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___attachmentThreshold_6)); }
	inline float get_attachmentThreshold_6() const { return ___attachmentThreshold_6; }
	inline float* get_address_of_attachmentThreshold_6() { return &___attachmentThreshold_6; }
	inline void set_attachmentThreshold_6(float value)
	{
		___attachmentThreshold_6 = value;
	}

	inline static int32_t get_offset_of_drawOrderThreshold_7() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___drawOrderThreshold_7)); }
	inline float get_drawOrderThreshold_7() const { return ___drawOrderThreshold_7; }
	inline float* get_address_of_drawOrderThreshold_7() { return &___drawOrderThreshold_7; }
	inline void set_drawOrderThreshold_7(float value)
	{
		___drawOrderThreshold_7 = value;
	}

	inline static int32_t get_offset_of_animationStart_8() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___animationStart_8)); }
	inline float get_animationStart_8() const { return ___animationStart_8; }
	inline float* get_address_of_animationStart_8() { return &___animationStart_8; }
	inline void set_animationStart_8(float value)
	{
		___animationStart_8 = value;
	}

	inline static int32_t get_offset_of_animationEnd_9() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___animationEnd_9)); }
	inline float get_animationEnd_9() const { return ___animationEnd_9; }
	inline float* get_address_of_animationEnd_9() { return &___animationEnd_9; }
	inline void set_animationEnd_9(float value)
	{
		___animationEnd_9 = value;
	}

	inline static int32_t get_offset_of_animationLast_10() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___animationLast_10)); }
	inline float get_animationLast_10() const { return ___animationLast_10; }
	inline float* get_address_of_animationLast_10() { return &___animationLast_10; }
	inline void set_animationLast_10(float value)
	{
		___animationLast_10 = value;
	}

	inline static int32_t get_offset_of_nextAnimationLast_11() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___nextAnimationLast_11)); }
	inline float get_nextAnimationLast_11() const { return ___nextAnimationLast_11; }
	inline float* get_address_of_nextAnimationLast_11() { return &___nextAnimationLast_11; }
	inline void set_nextAnimationLast_11(float value)
	{
		___nextAnimationLast_11 = value;
	}

	inline static int32_t get_offset_of_delay_12() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___delay_12)); }
	inline float get_delay_12() const { return ___delay_12; }
	inline float* get_address_of_delay_12() { return &___delay_12; }
	inline void set_delay_12(float value)
	{
		___delay_12 = value;
	}

	inline static int32_t get_offset_of_trackTime_13() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___trackTime_13)); }
	inline float get_trackTime_13() const { return ___trackTime_13; }
	inline float* get_address_of_trackTime_13() { return &___trackTime_13; }
	inline void set_trackTime_13(float value)
	{
		___trackTime_13 = value;
	}

	inline static int32_t get_offset_of_trackLast_14() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___trackLast_14)); }
	inline float get_trackLast_14() const { return ___trackLast_14; }
	inline float* get_address_of_trackLast_14() { return &___trackLast_14; }
	inline void set_trackLast_14(float value)
	{
		___trackLast_14 = value;
	}

	inline static int32_t get_offset_of_nextTrackLast_15() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___nextTrackLast_15)); }
	inline float get_nextTrackLast_15() const { return ___nextTrackLast_15; }
	inline float* get_address_of_nextTrackLast_15() { return &___nextTrackLast_15; }
	inline void set_nextTrackLast_15(float value)
	{
		___nextTrackLast_15 = value;
	}

	inline static int32_t get_offset_of_trackEnd_16() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___trackEnd_16)); }
	inline float get_trackEnd_16() const { return ___trackEnd_16; }
	inline float* get_address_of_trackEnd_16() { return &___trackEnd_16; }
	inline void set_trackEnd_16(float value)
	{
		___trackEnd_16 = value;
	}

	inline static int32_t get_offset_of_timeScale_17() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___timeScale_17)); }
	inline float get_timeScale_17() const { return ___timeScale_17; }
	inline float* get_address_of_timeScale_17() { return &___timeScale_17; }
	inline void set_timeScale_17(float value)
	{
		___timeScale_17 = value;
	}

	inline static int32_t get_offset_of_alpha_18() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___alpha_18)); }
	inline float get_alpha_18() const { return ___alpha_18; }
	inline float* get_address_of_alpha_18() { return &___alpha_18; }
	inline void set_alpha_18(float value)
	{
		___alpha_18 = value;
	}

	inline static int32_t get_offset_of_mixTime_19() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___mixTime_19)); }
	inline float get_mixTime_19() const { return ___mixTime_19; }
	inline float* get_address_of_mixTime_19() { return &___mixTime_19; }
	inline void set_mixTime_19(float value)
	{
		___mixTime_19 = value;
	}

	inline static int32_t get_offset_of_mixDuration_20() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___mixDuration_20)); }
	inline float get_mixDuration_20() const { return ___mixDuration_20; }
	inline float* get_address_of_mixDuration_20() { return &___mixDuration_20; }
	inline void set_mixDuration_20(float value)
	{
		___mixDuration_20 = value;
	}

	inline static int32_t get_offset_of_interruptAlpha_21() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___interruptAlpha_21)); }
	inline float get_interruptAlpha_21() const { return ___interruptAlpha_21; }
	inline float* get_address_of_interruptAlpha_21() { return &___interruptAlpha_21; }
	inline void set_interruptAlpha_21(float value)
	{
		___interruptAlpha_21 = value;
	}

	inline static int32_t get_offset_of_totalAlpha_22() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___totalAlpha_22)); }
	inline float get_totalAlpha_22() const { return ___totalAlpha_22; }
	inline float* get_address_of_totalAlpha_22() { return &___totalAlpha_22; }
	inline void set_totalAlpha_22(float value)
	{
		___totalAlpha_22 = value;
	}

	inline static int32_t get_offset_of_timelineData_23() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___timelineData_23)); }
	inline ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * get_timelineData_23() const { return ___timelineData_23; }
	inline ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 ** get_address_of_timelineData_23() { return &___timelineData_23; }
	inline void set_timelineData_23(ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * value)
	{
		___timelineData_23 = value;
		Il2CppCodeGenWriteBarrier((&___timelineData_23), value);
	}

	inline static int32_t get_offset_of_timelineDipMix_24() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___timelineDipMix_24)); }
	inline ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * get_timelineDipMix_24() const { return ___timelineDipMix_24; }
	inline ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 ** get_address_of_timelineDipMix_24() { return &___timelineDipMix_24; }
	inline void set_timelineDipMix_24(ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * value)
	{
		___timelineDipMix_24 = value;
		Il2CppCodeGenWriteBarrier((&___timelineDipMix_24), value);
	}

	inline static int32_t get_offset_of_timelinesRotation_25() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___timelinesRotation_25)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_timelinesRotation_25() const { return ___timelinesRotation_25; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_timelinesRotation_25() { return &___timelinesRotation_25; }
	inline void set_timelinesRotation_25(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___timelinesRotation_25 = value;
		Il2CppCodeGenWriteBarrier((&___timelinesRotation_25), value);
	}

	inline static int32_t get_offset_of_Start_26() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___Start_26)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Start_26() const { return ___Start_26; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Start_26() { return &___Start_26; }
	inline void set_Start_26(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Start_26 = value;
		Il2CppCodeGenWriteBarrier((&___Start_26), value);
	}

	inline static int32_t get_offset_of_Interrupt_27() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___Interrupt_27)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Interrupt_27() const { return ___Interrupt_27; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Interrupt_27() { return &___Interrupt_27; }
	inline void set_Interrupt_27(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Interrupt_27 = value;
		Il2CppCodeGenWriteBarrier((&___Interrupt_27), value);
	}

	inline static int32_t get_offset_of_End_28() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___End_28)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_End_28() const { return ___End_28; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_End_28() { return &___End_28; }
	inline void set_End_28(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___End_28 = value;
		Il2CppCodeGenWriteBarrier((&___End_28), value);
	}

	inline static int32_t get_offset_of_Dispose_29() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___Dispose_29)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Dispose_29() const { return ___Dispose_29; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Dispose_29() { return &___Dispose_29; }
	inline void set_Dispose_29(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Dispose_29 = value;
		Il2CppCodeGenWriteBarrier((&___Dispose_29), value);
	}

	inline static int32_t get_offset_of_Complete_30() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___Complete_30)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Complete_30() const { return ___Complete_30; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Complete_30() { return &___Complete_30; }
	inline void set_Complete_30(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Complete_30 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_30), value);
	}

	inline static int32_t get_offset_of_Event_31() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___Event_31)); }
	inline TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE * get_Event_31() const { return ___Event_31; }
	inline TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE ** get_address_of_Event_31() { return &___Event_31; }
	inline void set_Event_31(TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE * value)
	{
		___Event_31 = value;
		Il2CppCodeGenWriteBarrier((&___Event_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRY_TC06EF190EA88E3477B5CDDC2AABB2F01B6B12276_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_TBA8D772D87B6502B2A4D0EFE166C846285F50650_H
#define LIST_1_TBA8D772D87B6502B2A4D0EFE166C846285F50650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650, ____items_1)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650_StaticFields, ____emptyArray_5)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get__emptyArray_5() const { return ____emptyArray_5; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_TBA8D772D87B6502B2A4D0EFE166C846285F50650_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef DOU_T6892A900BE5AC1C3D8B4DFBA12DC4445953E1EFD_H
#define DOU_T6892A900BE5AC1C3D8B4DFBA12DC4445953E1EFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// dou
struct  dou_t6892A900BE5AC1C3D8B4DFBA12DC4445953E1EFD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOU_T6892A900BE5AC1C3D8B4DFBA12DC4445953E1EFD_H
#ifndef SAUTOMOBILECONFIG_T0F7305B3147D29DC77AFD945BD716711D8271B20_H
#define SAUTOMOBILECONFIG_T0F7305B3147D29DC77AFD945BD716711D8271B20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/sAutomobileConfig
struct  sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20 
{
public:
	// System.Int32 DataManager/sAutomobileConfig::nId
	int32_t ___nId_0;
	// System.Int32 DataManager/sAutomobileConfig::nLevel
	int32_t ___nLevel_1;
	// System.Int32 DataManager/sAutomobileConfig::nResId
	int32_t ___nResId_2;
	// System.String DataManager/sAutomobileConfig::szName
	String_t* ___szName_3;
	// System.Int32 DataManager/sAutomobileConfig::eCoinType
	int32_t ___eCoinType_4;
	// System.Double DataManager/sAutomobileConfig::nStartPrice_CoinValue
	double ___nStartPrice_CoinValue_5;
	// System.Single DataManager/sAutomobileConfig::fPriceRaisePercentPerBuy
	float ___fPriceRaisePercentPerBuy_6;
	// System.Int32 DataManager/sAutomobileConfig::nPriceDiamond
	int32_t ___nPriceDiamond_7;
	// System.Int32 DataManager/sAutomobileConfig::nCanUnlockLevel
	int32_t ___nCanUnlockLevel_8;
	// System.String DataManager/sAutomobileConfig::szSuitableTrackId
	String_t* ___szSuitableTrackId_9;
	// System.Double DataManager/sAutomobileConfig::nBaseGain
	double ___nBaseGain_10;
	// System.Single DataManager/sAutomobileConfig::fBaseSpeed
	float ___fBaseSpeed_11;
	// System.Int32 DataManager/sAutomobileConfig::nDropLevel
	int32_t ___nDropLevel_12;
	// System.Int32 DataManager/sAutomobileConfig::nDropInterval
	int32_t ___nDropInterval_13;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_nLevel_1() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nLevel_1)); }
	inline int32_t get_nLevel_1() const { return ___nLevel_1; }
	inline int32_t* get_address_of_nLevel_1() { return &___nLevel_1; }
	inline void set_nLevel_1(int32_t value)
	{
		___nLevel_1 = value;
	}

	inline static int32_t get_offset_of_nResId_2() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nResId_2)); }
	inline int32_t get_nResId_2() const { return ___nResId_2; }
	inline int32_t* get_address_of_nResId_2() { return &___nResId_2; }
	inline void set_nResId_2(int32_t value)
	{
		___nResId_2 = value;
	}

	inline static int32_t get_offset_of_szName_3() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___szName_3)); }
	inline String_t* get_szName_3() const { return ___szName_3; }
	inline String_t** get_address_of_szName_3() { return &___szName_3; }
	inline void set_szName_3(String_t* value)
	{
		___szName_3 = value;
		Il2CppCodeGenWriteBarrier((&___szName_3), value);
	}

	inline static int32_t get_offset_of_eCoinType_4() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___eCoinType_4)); }
	inline int32_t get_eCoinType_4() const { return ___eCoinType_4; }
	inline int32_t* get_address_of_eCoinType_4() { return &___eCoinType_4; }
	inline void set_eCoinType_4(int32_t value)
	{
		___eCoinType_4 = value;
	}

	inline static int32_t get_offset_of_nStartPrice_CoinValue_5() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nStartPrice_CoinValue_5)); }
	inline double get_nStartPrice_CoinValue_5() const { return ___nStartPrice_CoinValue_5; }
	inline double* get_address_of_nStartPrice_CoinValue_5() { return &___nStartPrice_CoinValue_5; }
	inline void set_nStartPrice_CoinValue_5(double value)
	{
		___nStartPrice_CoinValue_5 = value;
	}

	inline static int32_t get_offset_of_fPriceRaisePercentPerBuy_6() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___fPriceRaisePercentPerBuy_6)); }
	inline float get_fPriceRaisePercentPerBuy_6() const { return ___fPriceRaisePercentPerBuy_6; }
	inline float* get_address_of_fPriceRaisePercentPerBuy_6() { return &___fPriceRaisePercentPerBuy_6; }
	inline void set_fPriceRaisePercentPerBuy_6(float value)
	{
		___fPriceRaisePercentPerBuy_6 = value;
	}

	inline static int32_t get_offset_of_nPriceDiamond_7() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nPriceDiamond_7)); }
	inline int32_t get_nPriceDiamond_7() const { return ___nPriceDiamond_7; }
	inline int32_t* get_address_of_nPriceDiamond_7() { return &___nPriceDiamond_7; }
	inline void set_nPriceDiamond_7(int32_t value)
	{
		___nPriceDiamond_7 = value;
	}

	inline static int32_t get_offset_of_nCanUnlockLevel_8() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nCanUnlockLevel_8)); }
	inline int32_t get_nCanUnlockLevel_8() const { return ___nCanUnlockLevel_8; }
	inline int32_t* get_address_of_nCanUnlockLevel_8() { return &___nCanUnlockLevel_8; }
	inline void set_nCanUnlockLevel_8(int32_t value)
	{
		___nCanUnlockLevel_8 = value;
	}

	inline static int32_t get_offset_of_szSuitableTrackId_9() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___szSuitableTrackId_9)); }
	inline String_t* get_szSuitableTrackId_9() const { return ___szSuitableTrackId_9; }
	inline String_t** get_address_of_szSuitableTrackId_9() { return &___szSuitableTrackId_9; }
	inline void set_szSuitableTrackId_9(String_t* value)
	{
		___szSuitableTrackId_9 = value;
		Il2CppCodeGenWriteBarrier((&___szSuitableTrackId_9), value);
	}

	inline static int32_t get_offset_of_nBaseGain_10() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nBaseGain_10)); }
	inline double get_nBaseGain_10() const { return ___nBaseGain_10; }
	inline double* get_address_of_nBaseGain_10() { return &___nBaseGain_10; }
	inline void set_nBaseGain_10(double value)
	{
		___nBaseGain_10 = value;
	}

	inline static int32_t get_offset_of_fBaseSpeed_11() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___fBaseSpeed_11)); }
	inline float get_fBaseSpeed_11() const { return ___fBaseSpeed_11; }
	inline float* get_address_of_fBaseSpeed_11() { return &___fBaseSpeed_11; }
	inline void set_fBaseSpeed_11(float value)
	{
		___fBaseSpeed_11 = value;
	}

	inline static int32_t get_offset_of_nDropLevel_12() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nDropLevel_12)); }
	inline int32_t get_nDropLevel_12() const { return ___nDropLevel_12; }
	inline int32_t* get_address_of_nDropLevel_12() { return &___nDropLevel_12; }
	inline void set_nDropLevel_12(int32_t value)
	{
		___nDropLevel_12 = value;
	}

	inline static int32_t get_offset_of_nDropInterval_13() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nDropInterval_13)); }
	inline int32_t get_nDropInterval_13() const { return ___nDropInterval_13; }
	inline int32_t* get_address_of_nDropInterval_13() { return &___nDropInterval_13; }
	inline void set_nDropInterval_13(int32_t value)
	{
		___nDropInterval_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DataManager/sAutomobileConfig
struct sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20_marshaled_pinvoke
{
	int32_t ___nId_0;
	int32_t ___nLevel_1;
	int32_t ___nResId_2;
	char* ___szName_3;
	int32_t ___eCoinType_4;
	double ___nStartPrice_CoinValue_5;
	float ___fPriceRaisePercentPerBuy_6;
	int32_t ___nPriceDiamond_7;
	int32_t ___nCanUnlockLevel_8;
	char* ___szSuitableTrackId_9;
	double ___nBaseGain_10;
	float ___fBaseSpeed_11;
	int32_t ___nDropLevel_12;
	int32_t ___nDropInterval_13;
};
// Native definition for COM marshalling of DataManager/sAutomobileConfig
struct sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20_marshaled_com
{
	int32_t ___nId_0;
	int32_t ___nLevel_1;
	int32_t ___nResId_2;
	Il2CppChar* ___szName_3;
	int32_t ___eCoinType_4;
	double ___nStartPrice_CoinValue_5;
	float ___fPriceRaisePercentPerBuy_6;
	int32_t ___nPriceDiamond_7;
	int32_t ___nCanUnlockLevel_8;
	Il2CppChar* ___szSuitableTrackId_9;
	double ___nBaseGain_10;
	float ___fBaseSpeed_11;
	int32_t ___nDropLevel_12;
	int32_t ___nDropInterval_13;
};
#endif // SAUTOMOBILECONFIG_T0F7305B3147D29DC77AFD945BD716711D8271B20_H
#ifndef SPRESTIGECONFIG_T397FC1BE163EC0B938F2EC3B56694DCA340BAC63_H
#define SPRESTIGECONFIG_T397FC1BE163EC0B938F2EC3B56694DCA340BAC63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/sPrestigeConfig
struct  sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63 
{
public:
	// System.Collections.Generic.List`1<System.Double> DataManager/sPrestigeConfig::aryCoinCost
	List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786 * ___aryCoinCost_0;
	// System.Collections.Generic.List`1<System.Single> DataManager/sPrestigeConfig::aryCoinPromote
	List_1_t026D7A8C4D989218772DB3E051A624F753A60859 * ___aryCoinPromote_1;

public:
	inline static int32_t get_offset_of_aryCoinCost_0() { return static_cast<int32_t>(offsetof(sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63, ___aryCoinCost_0)); }
	inline List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786 * get_aryCoinCost_0() const { return ___aryCoinCost_0; }
	inline List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786 ** get_address_of_aryCoinCost_0() { return &___aryCoinCost_0; }
	inline void set_aryCoinCost_0(List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786 * value)
	{
		___aryCoinCost_0 = value;
		Il2CppCodeGenWriteBarrier((&___aryCoinCost_0), value);
	}

	inline static int32_t get_offset_of_aryCoinPromote_1() { return static_cast<int32_t>(offsetof(sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63, ___aryCoinPromote_1)); }
	inline List_1_t026D7A8C4D989218772DB3E051A624F753A60859 * get_aryCoinPromote_1() const { return ___aryCoinPromote_1; }
	inline List_1_t026D7A8C4D989218772DB3E051A624F753A60859 ** get_address_of_aryCoinPromote_1() { return &___aryCoinPromote_1; }
	inline void set_aryCoinPromote_1(List_1_t026D7A8C4D989218772DB3E051A624F753A60859 * value)
	{
		___aryCoinPromote_1 = value;
		Il2CppCodeGenWriteBarrier((&___aryCoinPromote_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DataManager/sPrestigeConfig
struct sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63_marshaled_pinvoke
{
	List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786 * ___aryCoinCost_0;
	List_1_t026D7A8C4D989218772DB3E051A624F753A60859 * ___aryCoinPromote_1;
};
// Native definition for COM marshalling of DataManager/sPrestigeConfig
struct sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63_marshaled_com
{
	List_1_t378F8551A015C3771CC9632236BEEE5A0DE62786 * ___aryCoinCost_0;
	List_1_t026D7A8C4D989218772DB3E051A624F753A60859 * ___aryCoinPromote_1;
};
#endif // SPRESTIGECONFIG_T397FC1BE163EC0B938F2EC3B56694DCA340BAC63_H
#ifndef STRACKCONFIG_TC4F21E8F5FB966041FF5D2421A3AB04E318A005B_H
#define STRACKCONFIG_TC4F21E8F5FB966041FF5D2421A3AB04E318A005B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/sTrackConfig
struct  sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B 
{
public:
	// System.Int32 DataManager/sTrackConfig::nId
	int32_t ___nId_0;
	// System.String DataManager/sTrackConfig::szName
	String_t* ___szName_1;
	// System.Int32 DataManager/sTrackConfig::nPlanetId
	int32_t ___nPlanetId_2;
	// System.Double DataManager/sTrackConfig::nUnlockPrice
	double ___nUnlockPrice_3;
	// System.Single DataManager/sTrackConfig::fEarning
	float ___fEarning_4;
	// System.Double DataManager/sTrackConfig::nBuyAdminBasePrice
	double ___nBuyAdminBasePrice_5;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_szName_1() { return static_cast<int32_t>(offsetof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B, ___szName_1)); }
	inline String_t* get_szName_1() const { return ___szName_1; }
	inline String_t** get_address_of_szName_1() { return &___szName_1; }
	inline void set_szName_1(String_t* value)
	{
		___szName_1 = value;
		Il2CppCodeGenWriteBarrier((&___szName_1), value);
	}

	inline static int32_t get_offset_of_nPlanetId_2() { return static_cast<int32_t>(offsetof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B, ___nPlanetId_2)); }
	inline int32_t get_nPlanetId_2() const { return ___nPlanetId_2; }
	inline int32_t* get_address_of_nPlanetId_2() { return &___nPlanetId_2; }
	inline void set_nPlanetId_2(int32_t value)
	{
		___nPlanetId_2 = value;
	}

	inline static int32_t get_offset_of_nUnlockPrice_3() { return static_cast<int32_t>(offsetof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B, ___nUnlockPrice_3)); }
	inline double get_nUnlockPrice_3() const { return ___nUnlockPrice_3; }
	inline double* get_address_of_nUnlockPrice_3() { return &___nUnlockPrice_3; }
	inline void set_nUnlockPrice_3(double value)
	{
		___nUnlockPrice_3 = value;
	}

	inline static int32_t get_offset_of_fEarning_4() { return static_cast<int32_t>(offsetof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B, ___fEarning_4)); }
	inline float get_fEarning_4() const { return ___fEarning_4; }
	inline float* get_address_of_fEarning_4() { return &___fEarning_4; }
	inline void set_fEarning_4(float value)
	{
		___fEarning_4 = value;
	}

	inline static int32_t get_offset_of_nBuyAdminBasePrice_5() { return static_cast<int32_t>(offsetof(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B, ___nBuyAdminBasePrice_5)); }
	inline double get_nBuyAdminBasePrice_5() const { return ___nBuyAdminBasePrice_5; }
	inline double* get_address_of_nBuyAdminBasePrice_5() { return &___nBuyAdminBasePrice_5; }
	inline void set_nBuyAdminBasePrice_5(double value)
	{
		___nBuyAdminBasePrice_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DataManager/sTrackConfig
struct sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B_marshaled_pinvoke
{
	int32_t ___nId_0;
	char* ___szName_1;
	int32_t ___nPlanetId_2;
	double ___nUnlockPrice_3;
	float ___fEarning_4;
	double ___nBuyAdminBasePrice_5;
};
// Native definition for COM marshalling of DataManager/sTrackConfig
struct sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B_marshaled_com
{
	int32_t ___nId_0;
	Il2CppChar* ___szName_1;
	int32_t ___nPlanetId_2;
	double ___nUnlockPrice_3;
	float ___fEarning_4;
	double ___nBuyAdminBasePrice_5;
};
#endif // STRACKCONFIG_TC4F21E8F5FB966041FF5D2421A3AB04E318A005B_H
#ifndef SLEVELANDCOST_T54392C7967F904A78CC7723B1259304E036E0EBA_H
#define SLEVELANDCOST_T54392C7967F904A78CC7723B1259304E036E0EBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// District/sLevelAndCost
struct  sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA 
{
public:
	// System.Int32 District/sLevelAndCost::nLevel
	int32_t ___nLevel_0;
	// System.Double District/sLevelAndCost::dCurPrice
	double ___dCurPrice_1;
	// System.Double District/sLevelAndCost::dCost
	double ___dCost_2;
	// System.Int32 District/sLevelAndCost::nCurBuyTimes
	int32_t ___nCurBuyTimes_3;
	// System.Int32 District/sLevelAndCost::nToBuyTime
	int32_t ___nToBuyTime_4;

public:
	inline static int32_t get_offset_of_nLevel_0() { return static_cast<int32_t>(offsetof(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA, ___nLevel_0)); }
	inline int32_t get_nLevel_0() const { return ___nLevel_0; }
	inline int32_t* get_address_of_nLevel_0() { return &___nLevel_0; }
	inline void set_nLevel_0(int32_t value)
	{
		___nLevel_0 = value;
	}

	inline static int32_t get_offset_of_dCurPrice_1() { return static_cast<int32_t>(offsetof(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA, ___dCurPrice_1)); }
	inline double get_dCurPrice_1() const { return ___dCurPrice_1; }
	inline double* get_address_of_dCurPrice_1() { return &___dCurPrice_1; }
	inline void set_dCurPrice_1(double value)
	{
		___dCurPrice_1 = value;
	}

	inline static int32_t get_offset_of_dCost_2() { return static_cast<int32_t>(offsetof(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA, ___dCost_2)); }
	inline double get_dCost_2() const { return ___dCost_2; }
	inline double* get_address_of_dCost_2() { return &___dCost_2; }
	inline void set_dCost_2(double value)
	{
		___dCost_2 = value;
	}

	inline static int32_t get_offset_of_nCurBuyTimes_3() { return static_cast<int32_t>(offsetof(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA, ___nCurBuyTimes_3)); }
	inline int32_t get_nCurBuyTimes_3() const { return ___nCurBuyTimes_3; }
	inline int32_t* get_address_of_nCurBuyTimes_3() { return &___nCurBuyTimes_3; }
	inline void set_nCurBuyTimes_3(int32_t value)
	{
		___nCurBuyTimes_3 = value;
	}

	inline static int32_t get_offset_of_nToBuyTime_4() { return static_cast<int32_t>(offsetof(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA, ___nToBuyTime_4)); }
	inline int32_t get_nToBuyTime_4() const { return ___nToBuyTime_4; }
	inline int32_t* get_address_of_nToBuyTime_4() { return &___nToBuyTime_4; }
	inline void set_nToBuyTime_4(int32_t value)
	{
		___nToBuyTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLEVELANDCOST_T54392C7967F904A78CC7723B1259304E036E0EBA_H
#ifndef SSKILLCONFIG_TC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E_H
#define SSKILLCONFIG_TC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkillManager/sSkillConfig
struct  sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E 
{
public:
	// System.Single SkillManager/sSkillConfig::fValue
	float ___fValue_0;
	// System.Single SkillManager/sSkillConfig::nColdDown
	float ___nColdDown_1;
	// System.Single SkillManager/sSkillConfig::nDuration
	float ___nDuration_2;

public:
	inline static int32_t get_offset_of_fValue_0() { return static_cast<int32_t>(offsetof(sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E, ___fValue_0)); }
	inline float get_fValue_0() const { return ___fValue_0; }
	inline float* get_address_of_fValue_0() { return &___fValue_0; }
	inline void set_fValue_0(float value)
	{
		___fValue_0 = value;
	}

	inline static int32_t get_offset_of_nColdDown_1() { return static_cast<int32_t>(offsetof(sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E, ___nColdDown_1)); }
	inline float get_nColdDown_1() const { return ___nColdDown_1; }
	inline float* get_address_of_nColdDown_1() { return &___nColdDown_1; }
	inline void set_nColdDown_1(float value)
	{
		___nColdDown_1 = value;
	}

	inline static int32_t get_offset_of_nDuration_2() { return static_cast<int32_t>(offsetof(sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E, ___nDuration_2)); }
	inline float get_nDuration_2() const { return ___nDuration_2; }
	inline float* get_address_of_nDuration_2() { return &___nDuration_2; }
	inline void set_nDuration_2(float value)
	{
		___nDuration_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSKILLCONFIG_TC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#define DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#define SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_HighlightedSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_PressedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_DisabledSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef ESE_TEBFC5E9CC8CA17804BEE458B4DAE21100B2641FD_H
#define ESE_TEBFC5E9CC8CA17804BEE458B4DAE21100B2641FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager/eSE
struct  eSE_tEBFC5E9CC8CA17804BEE458B4DAE21100B2641FD 
{
public:
	// System.Int32 AudioManager/eSE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eSE_tEBFC5E9CC8CA17804BEE458B4DAE21100B2641FD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESE_TEBFC5E9CC8CA17804BEE458B4DAE21100B2641FD_H
#ifndef ESE_NEW_TED4E1E0AAC6B985EE81D0661351734AC7DFFDFC2_H
#define ESE_NEW_TED4E1E0AAC6B985EE81D0661351734AC7DFFDFC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager/eSe_New
struct  eSe_New_tED4E1E0AAC6B985EE81D0661351734AC7DFFDFC2 
{
public:
	// System.Int32 AudioManager/eSe_New::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eSe_New_tED4E1E0AAC6B985EE81D0661351734AC7DFFDFC2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESE_NEW_TED4E1E0AAC6B985EE81D0661351734AC7DFFDFC2_H
#ifndef ESCALEAXIS_T2049C60FBA33DD8A1C5CAC79ECF499AF981117A2_H
#define ESCALEAXIS_T2049C60FBA33DD8A1C5CAC79ECF499AF981117A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseScale/eScaleAxis
struct  eScaleAxis_t2049C60FBA33DD8A1C5CAC79ECF499AF981117A2 
{
public:
	// System.Int32 BaseScale/eScaleAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eScaleAxis_t2049C60FBA33DD8A1C5CAC79ECF499AF981117A2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESCALEAXIS_T2049C60FBA33DD8A1C5CAC79ECF499AF981117A2_H
#ifndef EMONEYTYPE_T9A22FBEEECC2803A11A7756315481457B389F4C1_H
#define EMONEYTYPE_T9A22FBEEECC2803A11A7756315481457B389F4C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/eMoneyType
struct  eMoneyType_t9A22FBEEECC2803A11A7756315481457B389F4C1 
{
public:
	// System.Int32 DataManager/eMoneyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eMoneyType_t9A22FBEEECC2803A11A7756315481457B389F4C1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMONEYTYPE_T9A22FBEEECC2803A11A7756315481457B389F4C1_H
#ifndef EGUIDETYPE_T90774CD9942800464AB6E6A8D72E0F77452E0288_H
#define EGUIDETYPE_T90774CD9942800464AB6E6A8D72E0F77452E0288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FreshGuide/eGuideType
struct  eGuideType_t90774CD9942800464AB6E6A8D72E0F77452E0288 
{
public:
	// System.Int32 FreshGuide/eGuideType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eGuideType_t90774CD9942800464AB6E6A8D72E0F77452E0288, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EGUIDETYPE_T90774CD9942800464AB6E6A8D72E0F77452E0288_H
#ifndef SAUTORUNLOCATION_T1B6EE8072FCC8B63AD96FF14493824FE7F1AC949_H
#define SAUTORUNLOCATION_T1B6EE8072FCC8B63AD96FF14493824FE7F1AC949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main/sAutoRunLocation
struct  sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949 
{
public:
	// System.Int32 Main/sAutoRunLocation::nLocationIndex
	int32_t ___nLocationIndex_0;
	// UnityEngine.Vector3 Main/sAutoRunLocation::vecSrc
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecSrc_1;
	// UnityEngine.Vector3 Main/sAutoRunLocation::vecDest
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecDest_2;
	// UnityEngine.Vector3 Main/sAutoRunLocation::vecDir
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecDir_3;
	// UnityEngine.Vector3 Main/sAutoRunLocation::vecShowDir
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecShowDir_4;
	// System.Single Main/sAutoRunLocation::fRotateAngle
	float ___fRotateAngle_5;
	// System.Single Main/sAutoRunLocation::fDeltaAngleToNextNode
	float ___fDeltaAngleToNextNode_6;
	// System.Single Main/sAutoRunLocation::fDistanceToNextNode
	float ___fDistanceToNextNode_7;

public:
	inline static int32_t get_offset_of_nLocationIndex_0() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___nLocationIndex_0)); }
	inline int32_t get_nLocationIndex_0() const { return ___nLocationIndex_0; }
	inline int32_t* get_address_of_nLocationIndex_0() { return &___nLocationIndex_0; }
	inline void set_nLocationIndex_0(int32_t value)
	{
		___nLocationIndex_0 = value;
	}

	inline static int32_t get_offset_of_vecSrc_1() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___vecSrc_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecSrc_1() const { return ___vecSrc_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecSrc_1() { return &___vecSrc_1; }
	inline void set_vecSrc_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecSrc_1 = value;
	}

	inline static int32_t get_offset_of_vecDest_2() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___vecDest_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecDest_2() const { return ___vecDest_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecDest_2() { return &___vecDest_2; }
	inline void set_vecDest_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecDest_2 = value;
	}

	inline static int32_t get_offset_of_vecDir_3() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___vecDir_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecDir_3() const { return ___vecDir_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecDir_3() { return &___vecDir_3; }
	inline void set_vecDir_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecDir_3 = value;
	}

	inline static int32_t get_offset_of_vecShowDir_4() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___vecShowDir_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecShowDir_4() const { return ___vecShowDir_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecShowDir_4() { return &___vecShowDir_4; }
	inline void set_vecShowDir_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecShowDir_4 = value;
	}

	inline static int32_t get_offset_of_fRotateAngle_5() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___fRotateAngle_5)); }
	inline float get_fRotateAngle_5() const { return ___fRotateAngle_5; }
	inline float* get_address_of_fRotateAngle_5() { return &___fRotateAngle_5; }
	inline void set_fRotateAngle_5(float value)
	{
		___fRotateAngle_5 = value;
	}

	inline static int32_t get_offset_of_fDeltaAngleToNextNode_6() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___fDeltaAngleToNextNode_6)); }
	inline float get_fDeltaAngleToNextNode_6() const { return ___fDeltaAngleToNextNode_6; }
	inline float* get_address_of_fDeltaAngleToNextNode_6() { return &___fDeltaAngleToNextNode_6; }
	inline void set_fDeltaAngleToNextNode_6(float value)
	{
		___fDeltaAngleToNextNode_6 = value;
	}

	inline static int32_t get_offset_of_fDistanceToNextNode_7() { return static_cast<int32_t>(offsetof(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949, ___fDistanceToNextNode_7)); }
	inline float get_fDistanceToNextNode_7() const { return ___fDistanceToNextNode_7; }
	inline float* get_address_of_fDistanceToNextNode_7() { return &___fDistanceToNextNode_7; }
	inline void set_fDistanceToNextNode_7(float value)
	{
		___fDistanceToNextNode_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAUTORUNLOCATION_T1B6EE8072FCC8B63AD96FF14493824FE7F1AC949_H
#ifndef EDISTRICTSTATUS_TA21EDB657F7184F24E00E5C52D26254A33458EC8_H
#define EDISTRICTSTATUS_TA21EDB657F7184F24E00E5C52D26254A33458EC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapManager/eDistrictStatus
struct  eDistrictStatus_tA21EDB657F7184F24E00E5C52D26254A33458EC8 
{
public:
	// System.Int32 MapManager/eDistrictStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eDistrictStatus_tA21EDB657F7184F24E00E5C52D26254A33458EC8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDISTRICTSTATUS_TA21EDB657F7184F24E00E5C52D26254A33458EC8_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#define COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_NormalColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_HighlightedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_PressedColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_DisabledColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifndef FILLMETHOD_T0DB7332683118B7C7D2748BE74CFBF19CD19F8C5_H
#define FILLMETHOD_T0DB7332683118B7C7D2748BE74CFBF19CD19F8C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T0DB7332683118B7C7D2748BE74CFBF19CD19F8C5_H
#ifndef TYPE_T96B8A259B84ADA5E7D3B1F13AEAE22175937F38A_H
#define TYPE_T96B8A259B84ADA5E7D3B1F13AEAE22175937F38A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T96B8A259B84ADA5E7D3B1F13AEAE22175937F38A_H
#ifndef MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#define MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#ifndef SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#define SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_tF089B96B46A592693753CBF23C52A3887632D210 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_tF089B96B46A592693753CBF23C52A3887632D210, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#ifndef TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#define TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_tA9261C608B54C52324084A0B080E7A3E0548A181 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tA9261C608B54C52324084A0B080E7A3E0548A181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifndef SPLANETCONFIG_TB3752904FD33E8B33195C2F67744EF265D750AA9_H
#define SPLANETCONFIG_TB3752904FD33E8B33195C2F67744EF265D750AA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/sPlanetConfig
struct  sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9 
{
public:
	// System.String DataManager/sPlanetConfig::szName
	String_t* ___szName_0;
	// System.Int32 DataManager/sPlanetConfig::nId
	int32_t ___nId_1;
	// System.Double DataManager/sPlanetConfig::nUnlockPrice
	double ___nUnlockPrice_2;
	// DataManager/eMoneyType DataManager/sPlanetConfig::eUnlockMoneyType
	int32_t ___eUnlockMoneyType_3;
	// System.Double DataManager/sPlanetConfig::dInitCoin
	double ___dInitCoin_4;
	// System.Double DataManager/sPlanetConfig::dInitSkillPoint
	double ___dInitSkillPoint_5;

public:
	inline static int32_t get_offset_of_szName_0() { return static_cast<int32_t>(offsetof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9, ___szName_0)); }
	inline String_t* get_szName_0() const { return ___szName_0; }
	inline String_t** get_address_of_szName_0() { return &___szName_0; }
	inline void set_szName_0(String_t* value)
	{
		___szName_0 = value;
		Il2CppCodeGenWriteBarrier((&___szName_0), value);
	}

	inline static int32_t get_offset_of_nId_1() { return static_cast<int32_t>(offsetof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9, ___nId_1)); }
	inline int32_t get_nId_1() const { return ___nId_1; }
	inline int32_t* get_address_of_nId_1() { return &___nId_1; }
	inline void set_nId_1(int32_t value)
	{
		___nId_1 = value;
	}

	inline static int32_t get_offset_of_nUnlockPrice_2() { return static_cast<int32_t>(offsetof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9, ___nUnlockPrice_2)); }
	inline double get_nUnlockPrice_2() const { return ___nUnlockPrice_2; }
	inline double* get_address_of_nUnlockPrice_2() { return &___nUnlockPrice_2; }
	inline void set_nUnlockPrice_2(double value)
	{
		___nUnlockPrice_2 = value;
	}

	inline static int32_t get_offset_of_eUnlockMoneyType_3() { return static_cast<int32_t>(offsetof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9, ___eUnlockMoneyType_3)); }
	inline int32_t get_eUnlockMoneyType_3() const { return ___eUnlockMoneyType_3; }
	inline int32_t* get_address_of_eUnlockMoneyType_3() { return &___eUnlockMoneyType_3; }
	inline void set_eUnlockMoneyType_3(int32_t value)
	{
		___eUnlockMoneyType_3 = value;
	}

	inline static int32_t get_offset_of_dInitCoin_4() { return static_cast<int32_t>(offsetof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9, ___dInitCoin_4)); }
	inline double get_dInitCoin_4() const { return ___dInitCoin_4; }
	inline double* get_address_of_dInitCoin_4() { return &___dInitCoin_4; }
	inline void set_dInitCoin_4(double value)
	{
		___dInitCoin_4 = value;
	}

	inline static int32_t get_offset_of_dInitSkillPoint_5() { return static_cast<int32_t>(offsetof(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9, ___dInitSkillPoint_5)); }
	inline double get_dInitSkillPoint_5() const { return ___dInitSkillPoint_5; }
	inline double* get_address_of_dInitSkillPoint_5() { return &___dInitSkillPoint_5; }
	inline void set_dInitSkillPoint_5(double value)
	{
		___dInitSkillPoint_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DataManager/sPlanetConfig
struct sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9_marshaled_pinvoke
{
	char* ___szName_0;
	int32_t ___nId_1;
	double ___nUnlockPrice_2;
	int32_t ___eUnlockMoneyType_3;
	double ___dInitCoin_4;
	double ___dInitSkillPoint_5;
};
// Native definition for COM marshalling of DataManager/sPlanetConfig
struct sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9_marshaled_com
{
	Il2CppChar* ___szName_0;
	int32_t ___nId_1;
	double ___nUnlockPrice_2;
	int32_t ___eUnlockMoneyType_3;
	double ___dInitCoin_4;
	double ___dInitSkillPoint_5;
};
#endif // SPLANETCONFIG_TB3752904FD33E8B33195C2F67744EF265D750AA9_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#define MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_TF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef SPRITE_TCA09498D612D08DE668653AF1E9C12BF53434198_H
#define SPRITE_TCA09498D612D08DE668653AF1E9C12BF53434198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_TCA09498D612D08DE668653AF1E9C12BF53434198_H
#ifndef TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#define TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#ifndef NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#define NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnUp_1)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnDown_2)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnLeft_3)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnRight_4)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#ifndef ATLASASSET_TA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47_H
#define ATLASASSET_TA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.AtlasAsset
struct  AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.TextAsset Spine.Unity.AtlasAsset::atlasFile
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___atlasFile_4;
	// UnityEngine.Material[] Spine.Unity.AtlasAsset::materials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___materials_5;
	// Spine.Atlas Spine.Unity.AtlasAsset::atlas
	Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4 * ___atlas_6;

public:
	inline static int32_t get_offset_of_atlasFile_4() { return static_cast<int32_t>(offsetof(AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47, ___atlasFile_4)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_atlasFile_4() const { return ___atlasFile_4; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_atlasFile_4() { return &___atlasFile_4; }
	inline void set_atlasFile_4(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___atlasFile_4 = value;
		Il2CppCodeGenWriteBarrier((&___atlasFile_4), value);
	}

	inline static int32_t get_offset_of_materials_5() { return static_cast<int32_t>(offsetof(AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47, ___materials_5)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_materials_5() const { return ___materials_5; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_materials_5() { return &___materials_5; }
	inline void set_materials_5(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___materials_5 = value;
		Il2CppCodeGenWriteBarrier((&___materials_5), value);
	}

	inline static int32_t get_offset_of_atlas_6() { return static_cast<int32_t>(offsetof(AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47, ___atlas_6)); }
	inline Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4 * get_atlas_6() const { return ___atlas_6; }
	inline Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4 ** get_address_of_atlas_6() { return &___atlas_6; }
	inline void set_atlas_6(Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4 * value)
	{
		___atlas_6 = value;
		Il2CppCodeGenWriteBarrier((&___atlas_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASASSET_TA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47_H
#ifndef SKELETONDATAASSET_TFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5_H
#define SKELETONDATAASSET_TFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonDataAsset
struct  SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Spine.Unity.AtlasAsset[] Spine.Unity.SkeletonDataAsset::atlasAssets
	AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182* ___atlasAssets_4;
	// System.Single Spine.Unity.SkeletonDataAsset::scale
	float ___scale_5;
	// UnityEngine.TextAsset Spine.Unity.SkeletonDataAsset::skeletonJSON
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___skeletonJSON_6;
	// System.String[] Spine.Unity.SkeletonDataAsset::fromAnimation
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___fromAnimation_7;
	// System.String[] Spine.Unity.SkeletonDataAsset::toAnimation
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___toAnimation_8;
	// System.Single[] Spine.Unity.SkeletonDataAsset::duration
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___duration_9;
	// System.Single Spine.Unity.SkeletonDataAsset::defaultMix
	float ___defaultMix_10;
	// UnityEngine.RuntimeAnimatorController Spine.Unity.SkeletonDataAsset::controller
	RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * ___controller_11;
	// Spine.SkeletonData Spine.Unity.SkeletonDataAsset::skeletonData
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 * ___skeletonData_12;
	// Spine.AnimationStateData Spine.Unity.SkeletonDataAsset::stateData
	AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C * ___stateData_13;

public:
	inline static int32_t get_offset_of_atlasAssets_4() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___atlasAssets_4)); }
	inline AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182* get_atlasAssets_4() const { return ___atlasAssets_4; }
	inline AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182** get_address_of_atlasAssets_4() { return &___atlasAssets_4; }
	inline void set_atlasAssets_4(AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182* value)
	{
		___atlasAssets_4 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAssets_4), value);
	}

	inline static int32_t get_offset_of_scale_5() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___scale_5)); }
	inline float get_scale_5() const { return ___scale_5; }
	inline float* get_address_of_scale_5() { return &___scale_5; }
	inline void set_scale_5(float value)
	{
		___scale_5 = value;
	}

	inline static int32_t get_offset_of_skeletonJSON_6() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___skeletonJSON_6)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_skeletonJSON_6() const { return ___skeletonJSON_6; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_skeletonJSON_6() { return &___skeletonJSON_6; }
	inline void set_skeletonJSON_6(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___skeletonJSON_6 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonJSON_6), value);
	}

	inline static int32_t get_offset_of_fromAnimation_7() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___fromAnimation_7)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_fromAnimation_7() const { return ___fromAnimation_7; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_fromAnimation_7() { return &___fromAnimation_7; }
	inline void set_fromAnimation_7(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___fromAnimation_7 = value;
		Il2CppCodeGenWriteBarrier((&___fromAnimation_7), value);
	}

	inline static int32_t get_offset_of_toAnimation_8() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___toAnimation_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_toAnimation_8() const { return ___toAnimation_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_toAnimation_8() { return &___toAnimation_8; }
	inline void set_toAnimation_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___toAnimation_8 = value;
		Il2CppCodeGenWriteBarrier((&___toAnimation_8), value);
	}

	inline static int32_t get_offset_of_duration_9() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___duration_9)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_duration_9() const { return ___duration_9; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_duration_9() { return &___duration_9; }
	inline void set_duration_9(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___duration_9 = value;
		Il2CppCodeGenWriteBarrier((&___duration_9), value);
	}

	inline static int32_t get_offset_of_defaultMix_10() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___defaultMix_10)); }
	inline float get_defaultMix_10() const { return ___defaultMix_10; }
	inline float* get_address_of_defaultMix_10() { return &___defaultMix_10; }
	inline void set_defaultMix_10(float value)
	{
		___defaultMix_10 = value;
	}

	inline static int32_t get_offset_of_controller_11() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___controller_11)); }
	inline RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * get_controller_11() const { return ___controller_11; }
	inline RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD ** get_address_of_controller_11() { return &___controller_11; }
	inline void set_controller_11(RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * value)
	{
		___controller_11 = value;
		Il2CppCodeGenWriteBarrier((&___controller_11), value);
	}

	inline static int32_t get_offset_of_skeletonData_12() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___skeletonData_12)); }
	inline SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 * get_skeletonData_12() const { return ___skeletonData_12; }
	inline SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 ** get_address_of_skeletonData_12() { return &___skeletonData_12; }
	inline void set_skeletonData_12(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 * value)
	{
		___skeletonData_12 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonData_12), value);
	}

	inline static int32_t get_offset_of_stateData_13() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___stateData_13)); }
	inline AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C * get_stateData_13() const { return ___stateData_13; }
	inline AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C ** get_address_of_stateData_13() { return &___stateData_13; }
	inline void set_stateData_13(AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C * value)
	{
		___stateData_13 = value;
		Il2CppCodeGenWriteBarrier((&___stateData_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONDATAASSET_TFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#define TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#ifndef TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#define TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef AUDIOMANAGER_T67152D1A926351222F6AD0C0F2442EAE024C7D23_H
#define AUDIOMANAGER_T67152D1A926351222F6AD0C0F2442EAE024C7D23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager
struct  AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioSource[] AudioManager::m_aryBGM
	AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* ___m_aryBGM_5;
	// UnityEngine.AudioSource[] AudioManager::m_arySE
	AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* ___m_arySE_6;
	// UnityEngine.AudioSource[] AudioManager::m_arySE_New
	AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* ___m_arySE_New_7;
	// UnityEngine.AudioSource AudioManager::_audioDefaultClickButton
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ____audioDefaultClickButton_8;
	// UnityEngine.GameObject[] AudioManager::m_aryAuidoPrefabs
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryAuidoPrefabs_9;
	// UnityEngine.AudioSource AudioManager::_BMG
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ____BMG_10;
	// System.Collections.Generic.Dictionary`2<AudioManager/eSE,System.Collections.Generic.List`1<UnityEngine.AudioSource>> AudioManager::m_dicPlayingSE
	Dictionary_2_tCB4384C8D69355F95AE7A997BD0EBA8D389A27B2 * ___m_dicPlayingSE_11;
	// System.Boolean AudioManager::m_bBGM
	bool ___m_bBGM_12;
	// System.Int32 AudioManager::m_nMusicIndex
	int32_t ___m_nMusicIndex_13;

public:
	inline static int32_t get_offset_of_m_aryBGM_5() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_aryBGM_5)); }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* get_m_aryBGM_5() const { return ___m_aryBGM_5; }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF** get_address_of_m_aryBGM_5() { return &___m_aryBGM_5; }
	inline void set_m_aryBGM_5(AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* value)
	{
		___m_aryBGM_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryBGM_5), value);
	}

	inline static int32_t get_offset_of_m_arySE_6() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_arySE_6)); }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* get_m_arySE_6() const { return ___m_arySE_6; }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF** get_address_of_m_arySE_6() { return &___m_arySE_6; }
	inline void set_m_arySE_6(AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* value)
	{
		___m_arySE_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySE_6), value);
	}

	inline static int32_t get_offset_of_m_arySE_New_7() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_arySE_New_7)); }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* get_m_arySE_New_7() const { return ___m_arySE_New_7; }
	inline AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF** get_address_of_m_arySE_New_7() { return &___m_arySE_New_7; }
	inline void set_m_arySE_New_7(AudioSourceU5BU5D_t82A9EDBE30FC15D21E12BC1B17BFCEEA6A23ABBF* value)
	{
		___m_arySE_New_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySE_New_7), value);
	}

	inline static int32_t get_offset_of__audioDefaultClickButton_8() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ____audioDefaultClickButton_8)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get__audioDefaultClickButton_8() const { return ____audioDefaultClickButton_8; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of__audioDefaultClickButton_8() { return &____audioDefaultClickButton_8; }
	inline void set__audioDefaultClickButton_8(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		____audioDefaultClickButton_8 = value;
		Il2CppCodeGenWriteBarrier((&____audioDefaultClickButton_8), value);
	}

	inline static int32_t get_offset_of_m_aryAuidoPrefabs_9() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_aryAuidoPrefabs_9)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryAuidoPrefabs_9() const { return ___m_aryAuidoPrefabs_9; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryAuidoPrefabs_9() { return &___m_aryAuidoPrefabs_9; }
	inline void set_m_aryAuidoPrefabs_9(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryAuidoPrefabs_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAuidoPrefabs_9), value);
	}

	inline static int32_t get_offset_of__BMG_10() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ____BMG_10)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get__BMG_10() const { return ____BMG_10; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of__BMG_10() { return &____BMG_10; }
	inline void set__BMG_10(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		____BMG_10 = value;
		Il2CppCodeGenWriteBarrier((&____BMG_10), value);
	}

	inline static int32_t get_offset_of_m_dicPlayingSE_11() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_dicPlayingSE_11)); }
	inline Dictionary_2_tCB4384C8D69355F95AE7A997BD0EBA8D389A27B2 * get_m_dicPlayingSE_11() const { return ___m_dicPlayingSE_11; }
	inline Dictionary_2_tCB4384C8D69355F95AE7A997BD0EBA8D389A27B2 ** get_address_of_m_dicPlayingSE_11() { return &___m_dicPlayingSE_11; }
	inline void set_m_dicPlayingSE_11(Dictionary_2_tCB4384C8D69355F95AE7A997BD0EBA8D389A27B2 * value)
	{
		___m_dicPlayingSE_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPlayingSE_11), value);
	}

	inline static int32_t get_offset_of_m_bBGM_12() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_bBGM_12)); }
	inline bool get_m_bBGM_12() const { return ___m_bBGM_12; }
	inline bool* get_address_of_m_bBGM_12() { return &___m_bBGM_12; }
	inline void set_m_bBGM_12(bool value)
	{
		___m_bBGM_12 = value;
	}

	inline static int32_t get_offset_of_m_nMusicIndex_13() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_nMusicIndex_13)); }
	inline int32_t get_m_nMusicIndex_13() const { return ___m_nMusicIndex_13; }
	inline int32_t* get_address_of_m_nMusicIndex_13() { return &___m_nMusicIndex_13; }
	inline void set_m_nMusicIndex_13(int32_t value)
	{
		___m_nMusicIndex_13 = value;
	}
};

struct AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields
{
public:
	// AudioManager AudioManager::s_Instance
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields, ___s_Instance_4)); }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMANAGER_T67152D1A926351222F6AD0C0F2442EAE024C7D23_H
#ifndef BASESCALE_TA9DBA1A65C015253341E09F34B301CB704B44063_H
#define BASESCALE_TA9DBA1A65C015253341E09F34B301CB704B44063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseScale
struct  BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// BaseScale/delegateScaleEnd BaseScale::eventScaleEnd
	delegateScaleEnd_t697D8B0D83495ED3E1853627A6FCACD68409376F * ___eventScaleEnd_4;
	// UnityEngine.Vector3 BaseScale::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_5;
	// System.Boolean BaseScale::m_bAutoaPlay
	bool ___m_bAutoaPlay_6;
	// System.Boolean BaseScale::m_bLoop
	bool ___m_bLoop_7;
	// System.Boolean BaseScale::_bHideWhenEnd
	bool ____bHideWhenEnd_8;
	// System.Single BaseScale::m_fScaleTime
	float ___m_fScaleTime_9;
	// System.Single BaseScale::m_fWaitTime
	float ___m_fWaitTime_10;
	// System.Single BaseScale::m_fMaxScale
	float ___m_fMaxScale_11;
	// System.Int32 BaseScale::m_nStatus
	int32_t ___m_nStatus_12;
	// System.Single BaseScale::m_fSpeed
	float ___m_fSpeed_13;
	// System.Single BaseScale::m_fRotateSpeed
	float ___m_fRotateSpeed_14;
	// System.Single BaseScale::m_fWaitTimeElapse
	float ___m_fWaitTimeElapse_15;
	// System.Single BaseScale::m_fInitScale
	float ___m_fInitScale_16;
	// System.Single BaseScale::m_fDestScale
	float ___m_fDestScale_17;
	// System.Boolean BaseScale::_bWithRotate
	bool ____bWithRotate_18;
	// System.Single BaseScale::_fMaxRotateAngle
	float ____fMaxRotateAngle_19;
	// System.Single BaseScale::m_fAngle
	float ___m_fAngle_20;
	// UnityEngine.GameObject BaseScale::_goShadow
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goShadow_21;
	// System.Boolean BaseScale::m_bShadowBegin
	bool ___m_bShadowBegin_22;
	// BaseScale/eScaleAxis BaseScale::m_eAxis
	int32_t ___m_eAxis_23;

public:
	inline static int32_t get_offset_of_eventScaleEnd_4() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___eventScaleEnd_4)); }
	inline delegateScaleEnd_t697D8B0D83495ED3E1853627A6FCACD68409376F * get_eventScaleEnd_4() const { return ___eventScaleEnd_4; }
	inline delegateScaleEnd_t697D8B0D83495ED3E1853627A6FCACD68409376F ** get_address_of_eventScaleEnd_4() { return &___eventScaleEnd_4; }
	inline void set_eventScaleEnd_4(delegateScaleEnd_t697D8B0D83495ED3E1853627A6FCACD68409376F * value)
	{
		___eventScaleEnd_4 = value;
		Il2CppCodeGenWriteBarrier((&___eventScaleEnd_4), value);
	}

	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___vecTempScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_5 = value;
	}

	inline static int32_t get_offset_of_m_bAutoaPlay_6() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_bAutoaPlay_6)); }
	inline bool get_m_bAutoaPlay_6() const { return ___m_bAutoaPlay_6; }
	inline bool* get_address_of_m_bAutoaPlay_6() { return &___m_bAutoaPlay_6; }
	inline void set_m_bAutoaPlay_6(bool value)
	{
		___m_bAutoaPlay_6 = value;
	}

	inline static int32_t get_offset_of_m_bLoop_7() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_bLoop_7)); }
	inline bool get_m_bLoop_7() const { return ___m_bLoop_7; }
	inline bool* get_address_of_m_bLoop_7() { return &___m_bLoop_7; }
	inline void set_m_bLoop_7(bool value)
	{
		___m_bLoop_7 = value;
	}

	inline static int32_t get_offset_of__bHideWhenEnd_8() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ____bHideWhenEnd_8)); }
	inline bool get__bHideWhenEnd_8() const { return ____bHideWhenEnd_8; }
	inline bool* get_address_of__bHideWhenEnd_8() { return &____bHideWhenEnd_8; }
	inline void set__bHideWhenEnd_8(bool value)
	{
		____bHideWhenEnd_8 = value;
	}

	inline static int32_t get_offset_of_m_fScaleTime_9() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fScaleTime_9)); }
	inline float get_m_fScaleTime_9() const { return ___m_fScaleTime_9; }
	inline float* get_address_of_m_fScaleTime_9() { return &___m_fScaleTime_9; }
	inline void set_m_fScaleTime_9(float value)
	{
		___m_fScaleTime_9 = value;
	}

	inline static int32_t get_offset_of_m_fWaitTime_10() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fWaitTime_10)); }
	inline float get_m_fWaitTime_10() const { return ___m_fWaitTime_10; }
	inline float* get_address_of_m_fWaitTime_10() { return &___m_fWaitTime_10; }
	inline void set_m_fWaitTime_10(float value)
	{
		___m_fWaitTime_10 = value;
	}

	inline static int32_t get_offset_of_m_fMaxScale_11() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fMaxScale_11)); }
	inline float get_m_fMaxScale_11() const { return ___m_fMaxScale_11; }
	inline float* get_address_of_m_fMaxScale_11() { return &___m_fMaxScale_11; }
	inline void set_m_fMaxScale_11(float value)
	{
		___m_fMaxScale_11 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_12() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_nStatus_12)); }
	inline int32_t get_m_nStatus_12() const { return ___m_nStatus_12; }
	inline int32_t* get_address_of_m_nStatus_12() { return &___m_nStatus_12; }
	inline void set_m_nStatus_12(int32_t value)
	{
		___m_nStatus_12 = value;
	}

	inline static int32_t get_offset_of_m_fSpeed_13() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fSpeed_13)); }
	inline float get_m_fSpeed_13() const { return ___m_fSpeed_13; }
	inline float* get_address_of_m_fSpeed_13() { return &___m_fSpeed_13; }
	inline void set_m_fSpeed_13(float value)
	{
		___m_fSpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_fRotateSpeed_14() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fRotateSpeed_14)); }
	inline float get_m_fRotateSpeed_14() const { return ___m_fRotateSpeed_14; }
	inline float* get_address_of_m_fRotateSpeed_14() { return &___m_fRotateSpeed_14; }
	inline void set_m_fRotateSpeed_14(float value)
	{
		___m_fRotateSpeed_14 = value;
	}

	inline static int32_t get_offset_of_m_fWaitTimeElapse_15() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fWaitTimeElapse_15)); }
	inline float get_m_fWaitTimeElapse_15() const { return ___m_fWaitTimeElapse_15; }
	inline float* get_address_of_m_fWaitTimeElapse_15() { return &___m_fWaitTimeElapse_15; }
	inline void set_m_fWaitTimeElapse_15(float value)
	{
		___m_fWaitTimeElapse_15 = value;
	}

	inline static int32_t get_offset_of_m_fInitScale_16() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fInitScale_16)); }
	inline float get_m_fInitScale_16() const { return ___m_fInitScale_16; }
	inline float* get_address_of_m_fInitScale_16() { return &___m_fInitScale_16; }
	inline void set_m_fInitScale_16(float value)
	{
		___m_fInitScale_16 = value;
	}

	inline static int32_t get_offset_of_m_fDestScale_17() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fDestScale_17)); }
	inline float get_m_fDestScale_17() const { return ___m_fDestScale_17; }
	inline float* get_address_of_m_fDestScale_17() { return &___m_fDestScale_17; }
	inline void set_m_fDestScale_17(float value)
	{
		___m_fDestScale_17 = value;
	}

	inline static int32_t get_offset_of__bWithRotate_18() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ____bWithRotate_18)); }
	inline bool get__bWithRotate_18() const { return ____bWithRotate_18; }
	inline bool* get_address_of__bWithRotate_18() { return &____bWithRotate_18; }
	inline void set__bWithRotate_18(bool value)
	{
		____bWithRotate_18 = value;
	}

	inline static int32_t get_offset_of__fMaxRotateAngle_19() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ____fMaxRotateAngle_19)); }
	inline float get__fMaxRotateAngle_19() const { return ____fMaxRotateAngle_19; }
	inline float* get_address_of__fMaxRotateAngle_19() { return &____fMaxRotateAngle_19; }
	inline void set__fMaxRotateAngle_19(float value)
	{
		____fMaxRotateAngle_19 = value;
	}

	inline static int32_t get_offset_of_m_fAngle_20() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_fAngle_20)); }
	inline float get_m_fAngle_20() const { return ___m_fAngle_20; }
	inline float* get_address_of_m_fAngle_20() { return &___m_fAngle_20; }
	inline void set_m_fAngle_20(float value)
	{
		___m_fAngle_20 = value;
	}

	inline static int32_t get_offset_of__goShadow_21() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ____goShadow_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goShadow_21() const { return ____goShadow_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goShadow_21() { return &____goShadow_21; }
	inline void set__goShadow_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goShadow_21 = value;
		Il2CppCodeGenWriteBarrier((&____goShadow_21), value);
	}

	inline static int32_t get_offset_of_m_bShadowBegin_22() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_bShadowBegin_22)); }
	inline bool get_m_bShadowBegin_22() const { return ___m_bShadowBegin_22; }
	inline bool* get_address_of_m_bShadowBegin_22() { return &___m_bShadowBegin_22; }
	inline void set_m_bShadowBegin_22(bool value)
	{
		___m_bShadowBegin_22 = value;
	}

	inline static int32_t get_offset_of_m_eAxis_23() { return static_cast<int32_t>(offsetof(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063, ___m_eAxis_23)); }
	inline int32_t get_m_eAxis_23() const { return ___m_eAxis_23; }
	inline int32_t* get_address_of_m_eAxis_23() { return &___m_eAxis_23; }
	inline void set_m_eAxis_23(int32_t value)
	{
		___m_eAxis_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESCALE_TA9DBA1A65C015253341E09F34B301CB704B44063_H
#ifndef CFRAMEANIMATIONEFFECT_TB09C367E8E6954F89BDD8004C69990C722AC2E10_H
#define CFRAMEANIMATIONEFFECT_TB09C367E8E6954F89BDD8004C69990C722AC2E10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFrameAnimationEffect
struct  CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Sprite[] CFrameAnimationEffect::m_aryFrameSprites
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryFrameSprites_4;
	// UnityEngine.UI.Image CFrameAnimationEffect::_imgMain
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgMain_5;
	// UnityEngine.SpriteRenderer CFrameAnimationEffect::_sprMain
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____sprMain_6;
	// System.Boolean CFrameAnimationEffect::m_bUI
	bool ___m_bUI_7;
	// System.Single CFrameAnimationEffect::m_fFrameInterval
	float ___m_fFrameInterval_8;
	// System.Single CFrameAnimationEffect::m_fTimeCount
	float ___m_fTimeCount_9;
	// System.Boolean CFrameAnimationEffect::m_bPlaying
	bool ___m_bPlaying_10;
	// System.Int32 CFrameAnimationEffect::m_nFrameIndex
	int32_t ___m_nFrameIndex_11;
	// System.Boolean CFrameAnimationEffect::m_bLoop
	bool ___m_bLoop_12;
	// System.Boolean CFrameAnimationEffect::m_bReverse
	bool ___m_bReverse_13;
	// System.Boolean CFrameAnimationEffect::m_bCurDir
	bool ___m_bCurDir_14;
	// System.Single CFrameAnimationEffect::m_fLoopInterval
	float ___m_fLoopInterval_15;
	// System.Boolean CFrameAnimationEffect::m_bWaiting
	bool ___m_bWaiting_16;
	// System.Single CFrameAnimationEffect::m_fWaitingTimeCount
	float ___m_fWaitingTimeCount_17;
	// System.Boolean CFrameAnimationEffect::m_bPlayAuto
	bool ___m_bPlayAuto_18;
	// System.Int32 CFrameAnimationEffect::m_nLoopTimes
	int32_t ___m_nLoopTimes_19;
	// System.Boolean CFrameAnimationEffect::m_bHideWhenEnd
	bool ___m_bHideWhenEnd_20;
	// System.Boolean CFrameAnimationEffect::m_bDestroyWhenEnd
	bool ___m_bDestroyWhenEnd_21;
	// System.Int32 CFrameAnimationEffect::m_nId
	int32_t ___m_nId_22;

public:
	inline static int32_t get_offset_of_m_aryFrameSprites_4() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_aryFrameSprites_4)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryFrameSprites_4() const { return ___m_aryFrameSprites_4; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryFrameSprites_4() { return &___m_aryFrameSprites_4; }
	inline void set_m_aryFrameSprites_4(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryFrameSprites_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryFrameSprites_4), value);
	}

	inline static int32_t get_offset_of__imgMain_5() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ____imgMain_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgMain_5() const { return ____imgMain_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgMain_5() { return &____imgMain_5; }
	inline void set__imgMain_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgMain_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_5), value);
	}

	inline static int32_t get_offset_of__sprMain_6() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ____sprMain_6)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__sprMain_6() const { return ____sprMain_6; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__sprMain_6() { return &____sprMain_6; }
	inline void set__sprMain_6(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____sprMain_6 = value;
		Il2CppCodeGenWriteBarrier((&____sprMain_6), value);
	}

	inline static int32_t get_offset_of_m_bUI_7() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bUI_7)); }
	inline bool get_m_bUI_7() const { return ___m_bUI_7; }
	inline bool* get_address_of_m_bUI_7() { return &___m_bUI_7; }
	inline void set_m_bUI_7(bool value)
	{
		___m_bUI_7 = value;
	}

	inline static int32_t get_offset_of_m_fFrameInterval_8() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_fFrameInterval_8)); }
	inline float get_m_fFrameInterval_8() const { return ___m_fFrameInterval_8; }
	inline float* get_address_of_m_fFrameInterval_8() { return &___m_fFrameInterval_8; }
	inline void set_m_fFrameInterval_8(float value)
	{
		___m_fFrameInterval_8 = value;
	}

	inline static int32_t get_offset_of_m_fTimeCount_9() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_fTimeCount_9)); }
	inline float get_m_fTimeCount_9() const { return ___m_fTimeCount_9; }
	inline float* get_address_of_m_fTimeCount_9() { return &___m_fTimeCount_9; }
	inline void set_m_fTimeCount_9(float value)
	{
		___m_fTimeCount_9 = value;
	}

	inline static int32_t get_offset_of_m_bPlaying_10() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bPlaying_10)); }
	inline bool get_m_bPlaying_10() const { return ___m_bPlaying_10; }
	inline bool* get_address_of_m_bPlaying_10() { return &___m_bPlaying_10; }
	inline void set_m_bPlaying_10(bool value)
	{
		___m_bPlaying_10 = value;
	}

	inline static int32_t get_offset_of_m_nFrameIndex_11() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_nFrameIndex_11)); }
	inline int32_t get_m_nFrameIndex_11() const { return ___m_nFrameIndex_11; }
	inline int32_t* get_address_of_m_nFrameIndex_11() { return &___m_nFrameIndex_11; }
	inline void set_m_nFrameIndex_11(int32_t value)
	{
		___m_nFrameIndex_11 = value;
	}

	inline static int32_t get_offset_of_m_bLoop_12() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bLoop_12)); }
	inline bool get_m_bLoop_12() const { return ___m_bLoop_12; }
	inline bool* get_address_of_m_bLoop_12() { return &___m_bLoop_12; }
	inline void set_m_bLoop_12(bool value)
	{
		___m_bLoop_12 = value;
	}

	inline static int32_t get_offset_of_m_bReverse_13() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bReverse_13)); }
	inline bool get_m_bReverse_13() const { return ___m_bReverse_13; }
	inline bool* get_address_of_m_bReverse_13() { return &___m_bReverse_13; }
	inline void set_m_bReverse_13(bool value)
	{
		___m_bReverse_13 = value;
	}

	inline static int32_t get_offset_of_m_bCurDir_14() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bCurDir_14)); }
	inline bool get_m_bCurDir_14() const { return ___m_bCurDir_14; }
	inline bool* get_address_of_m_bCurDir_14() { return &___m_bCurDir_14; }
	inline void set_m_bCurDir_14(bool value)
	{
		___m_bCurDir_14 = value;
	}

	inline static int32_t get_offset_of_m_fLoopInterval_15() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_fLoopInterval_15)); }
	inline float get_m_fLoopInterval_15() const { return ___m_fLoopInterval_15; }
	inline float* get_address_of_m_fLoopInterval_15() { return &___m_fLoopInterval_15; }
	inline void set_m_fLoopInterval_15(float value)
	{
		___m_fLoopInterval_15 = value;
	}

	inline static int32_t get_offset_of_m_bWaiting_16() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bWaiting_16)); }
	inline bool get_m_bWaiting_16() const { return ___m_bWaiting_16; }
	inline bool* get_address_of_m_bWaiting_16() { return &___m_bWaiting_16; }
	inline void set_m_bWaiting_16(bool value)
	{
		___m_bWaiting_16 = value;
	}

	inline static int32_t get_offset_of_m_fWaitingTimeCount_17() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_fWaitingTimeCount_17)); }
	inline float get_m_fWaitingTimeCount_17() const { return ___m_fWaitingTimeCount_17; }
	inline float* get_address_of_m_fWaitingTimeCount_17() { return &___m_fWaitingTimeCount_17; }
	inline void set_m_fWaitingTimeCount_17(float value)
	{
		___m_fWaitingTimeCount_17 = value;
	}

	inline static int32_t get_offset_of_m_bPlayAuto_18() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bPlayAuto_18)); }
	inline bool get_m_bPlayAuto_18() const { return ___m_bPlayAuto_18; }
	inline bool* get_address_of_m_bPlayAuto_18() { return &___m_bPlayAuto_18; }
	inline void set_m_bPlayAuto_18(bool value)
	{
		___m_bPlayAuto_18 = value;
	}

	inline static int32_t get_offset_of_m_nLoopTimes_19() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_nLoopTimes_19)); }
	inline int32_t get_m_nLoopTimes_19() const { return ___m_nLoopTimes_19; }
	inline int32_t* get_address_of_m_nLoopTimes_19() { return &___m_nLoopTimes_19; }
	inline void set_m_nLoopTimes_19(int32_t value)
	{
		___m_nLoopTimes_19 = value;
	}

	inline static int32_t get_offset_of_m_bHideWhenEnd_20() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bHideWhenEnd_20)); }
	inline bool get_m_bHideWhenEnd_20() const { return ___m_bHideWhenEnd_20; }
	inline bool* get_address_of_m_bHideWhenEnd_20() { return &___m_bHideWhenEnd_20; }
	inline void set_m_bHideWhenEnd_20(bool value)
	{
		___m_bHideWhenEnd_20 = value;
	}

	inline static int32_t get_offset_of_m_bDestroyWhenEnd_21() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_bDestroyWhenEnd_21)); }
	inline bool get_m_bDestroyWhenEnd_21() const { return ___m_bDestroyWhenEnd_21; }
	inline bool* get_address_of_m_bDestroyWhenEnd_21() { return &___m_bDestroyWhenEnd_21; }
	inline void set_m_bDestroyWhenEnd_21(bool value)
	{
		___m_bDestroyWhenEnd_21 = value;
	}

	inline static int32_t get_offset_of_m_nId_22() { return static_cast<int32_t>(offsetof(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10, ___m_nId_22)); }
	inline int32_t get_m_nId_22() const { return ___m_nId_22; }
	inline int32_t* get_address_of_m_nId_22() { return &___m_nId_22; }
	inline void set_m_nId_22(int32_t value)
	{
		___m_nId_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFRAMEANIMATIONEFFECT_TB09C367E8E6954F89BDD8004C69990C722AC2E10_H
#ifndef DATAMANAGER_TE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_H
#define DATAMANAGER_TE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager
struct  DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean DataManager::m_bOnlineConfig
	bool ___m_bOnlineConfig_5;
	// System.Int32 DataManager::MAX_PLANET_NUM
	int32_t ___MAX_PLANET_NUM_12;
	// System.Int32 DataManager::MAX_SKILL_POINT_NUM
	int32_t ___MAX_SKILL_POINT_NUM_13;
	// System.Int32 DataManager::MAX_TRACK_NUM_OF_PLANET
	int32_t ___MAX_TRACK_NUM_OF_PLANET_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> DataManager::m_dicCoinGainPerRound
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___m_dicCoinGainPerRound_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> DataManager::m_dicCoinVehiclePrice
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___m_dicCoinVehiclePrice_16;
	// System.Single[] DataManager::m_aryRoundTimeByLevel
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryRoundTimeByLevel_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> DataManager::m_dicCoinCostToPrestige
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___m_dicCoinCostToPrestige_18;
	// System.Collections.Generic.Dictionary`2<System.String,DataManager/sAutomobileConfig> DataManager::m_dicAutomobileConfig
	Dictionary_2_t575E591ADF0DE43EE8FBD5432FDB44C619218BBC * ___m_dicAutomobileConfig_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,DataManager/sPlanetConfig> DataManager::m_dicPlanetConfig
	Dictionary_2_t35A7F0D04BB2998E9DD0CB0865E4F0442AE488AF * ___m_dicPlanetConfig_20;
	// System.Collections.Generic.Dictionary`2<System.String,DataManager/sTrackConfig> DataManager::m_dicTrackConfig
	Dictionary_2_t0FCB090B06AD243E1AEFE7A77D6E44B002171473 * ___m_dicTrackConfig_21;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> DataManager::m_dicTrackAndLevel2ResId
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___m_dicTrackAndLevel2ResId_22;
	// DataManager/sAutomobileConfig DataManager::tempAutomobileConfig
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20  ___tempAutomobileConfig_23;
	// DataManager/sPlanetConfig DataManager::tempPlanetConfig
	sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9  ___tempPlanetConfig_24;
	// DataManager/sTrackConfig DataManager::tempTrackConfig
	sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B  ___tempTrackConfig_25;
	// DataManager/sPrestigeConfig DataManager::tempPrestigeConfig
	sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63  ___tempPrestigeConfig_26;
	// System.Collections.Generic.Dictionary`2<System.String,DataManager/sPrestigeConfig> DataManager::m_dicPrestigeConfig
	Dictionary_2_t6927BC972CD2E0E92AF0DF540164BAF9ECB65E17 * ___m_dicPrestigeConfig_27;
	// System.Boolean DataManager::m_bPlanetConfigLoaded
	bool ___m_bPlanetConfigLoaded_28;
	// System.Boolean DataManager::m_bAutomobileConfigLoaded
	bool ___m_bAutomobileConfigLoaded_29;
	// System.Boolean DataManager::m_bTrackConfigLoaded
	bool ___m_bTrackConfigLoaded_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> DataManager::m_dicLoadMyData_String
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___m_dicLoadMyData_String_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Double> DataManager::m_dicLoadMyData
	Dictionary_2_tD9E97F40821434226F92E86AFDAE50D233468602 * ___m_dicLoadMyData_32;
	// System.Boolean DataManager::m_bCanSave
	bool ___m_bCanSave_33;

public:
	inline static int32_t get_offset_of_m_bOnlineConfig_5() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_bOnlineConfig_5)); }
	inline bool get_m_bOnlineConfig_5() const { return ___m_bOnlineConfig_5; }
	inline bool* get_address_of_m_bOnlineConfig_5() { return &___m_bOnlineConfig_5; }
	inline void set_m_bOnlineConfig_5(bool value)
	{
		___m_bOnlineConfig_5 = value;
	}

	inline static int32_t get_offset_of_MAX_PLANET_NUM_12() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___MAX_PLANET_NUM_12)); }
	inline int32_t get_MAX_PLANET_NUM_12() const { return ___MAX_PLANET_NUM_12; }
	inline int32_t* get_address_of_MAX_PLANET_NUM_12() { return &___MAX_PLANET_NUM_12; }
	inline void set_MAX_PLANET_NUM_12(int32_t value)
	{
		___MAX_PLANET_NUM_12 = value;
	}

	inline static int32_t get_offset_of_MAX_SKILL_POINT_NUM_13() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___MAX_SKILL_POINT_NUM_13)); }
	inline int32_t get_MAX_SKILL_POINT_NUM_13() const { return ___MAX_SKILL_POINT_NUM_13; }
	inline int32_t* get_address_of_MAX_SKILL_POINT_NUM_13() { return &___MAX_SKILL_POINT_NUM_13; }
	inline void set_MAX_SKILL_POINT_NUM_13(int32_t value)
	{
		___MAX_SKILL_POINT_NUM_13 = value;
	}

	inline static int32_t get_offset_of_MAX_TRACK_NUM_OF_PLANET_14() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___MAX_TRACK_NUM_OF_PLANET_14)); }
	inline int32_t get_MAX_TRACK_NUM_OF_PLANET_14() const { return ___MAX_TRACK_NUM_OF_PLANET_14; }
	inline int32_t* get_address_of_MAX_TRACK_NUM_OF_PLANET_14() { return &___MAX_TRACK_NUM_OF_PLANET_14; }
	inline void set_MAX_TRACK_NUM_OF_PLANET_14(int32_t value)
	{
		___MAX_TRACK_NUM_OF_PLANET_14 = value;
	}

	inline static int32_t get_offset_of_m_dicCoinGainPerRound_15() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicCoinGainPerRound_15)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_m_dicCoinGainPerRound_15() const { return ___m_dicCoinGainPerRound_15; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_m_dicCoinGainPerRound_15() { return &___m_dicCoinGainPerRound_15; }
	inline void set_m_dicCoinGainPerRound_15(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___m_dicCoinGainPerRound_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicCoinGainPerRound_15), value);
	}

	inline static int32_t get_offset_of_m_dicCoinVehiclePrice_16() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicCoinVehiclePrice_16)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_m_dicCoinVehiclePrice_16() const { return ___m_dicCoinVehiclePrice_16; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_m_dicCoinVehiclePrice_16() { return &___m_dicCoinVehiclePrice_16; }
	inline void set_m_dicCoinVehiclePrice_16(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___m_dicCoinVehiclePrice_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicCoinVehiclePrice_16), value);
	}

	inline static int32_t get_offset_of_m_aryRoundTimeByLevel_17() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_aryRoundTimeByLevel_17)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryRoundTimeByLevel_17() const { return ___m_aryRoundTimeByLevel_17; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryRoundTimeByLevel_17() { return &___m_aryRoundTimeByLevel_17; }
	inline void set_m_aryRoundTimeByLevel_17(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryRoundTimeByLevel_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryRoundTimeByLevel_17), value);
	}

	inline static int32_t get_offset_of_m_dicCoinCostToPrestige_18() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicCoinCostToPrestige_18)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_m_dicCoinCostToPrestige_18() const { return ___m_dicCoinCostToPrestige_18; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_m_dicCoinCostToPrestige_18() { return &___m_dicCoinCostToPrestige_18; }
	inline void set_m_dicCoinCostToPrestige_18(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___m_dicCoinCostToPrestige_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicCoinCostToPrestige_18), value);
	}

	inline static int32_t get_offset_of_m_dicAutomobileConfig_19() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicAutomobileConfig_19)); }
	inline Dictionary_2_t575E591ADF0DE43EE8FBD5432FDB44C619218BBC * get_m_dicAutomobileConfig_19() const { return ___m_dicAutomobileConfig_19; }
	inline Dictionary_2_t575E591ADF0DE43EE8FBD5432FDB44C619218BBC ** get_address_of_m_dicAutomobileConfig_19() { return &___m_dicAutomobileConfig_19; }
	inline void set_m_dicAutomobileConfig_19(Dictionary_2_t575E591ADF0DE43EE8FBD5432FDB44C619218BBC * value)
	{
		___m_dicAutomobileConfig_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAutomobileConfig_19), value);
	}

	inline static int32_t get_offset_of_m_dicPlanetConfig_20() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicPlanetConfig_20)); }
	inline Dictionary_2_t35A7F0D04BB2998E9DD0CB0865E4F0442AE488AF * get_m_dicPlanetConfig_20() const { return ___m_dicPlanetConfig_20; }
	inline Dictionary_2_t35A7F0D04BB2998E9DD0CB0865E4F0442AE488AF ** get_address_of_m_dicPlanetConfig_20() { return &___m_dicPlanetConfig_20; }
	inline void set_m_dicPlanetConfig_20(Dictionary_2_t35A7F0D04BB2998E9DD0CB0865E4F0442AE488AF * value)
	{
		___m_dicPlanetConfig_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPlanetConfig_20), value);
	}

	inline static int32_t get_offset_of_m_dicTrackConfig_21() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicTrackConfig_21)); }
	inline Dictionary_2_t0FCB090B06AD243E1AEFE7A77D6E44B002171473 * get_m_dicTrackConfig_21() const { return ___m_dicTrackConfig_21; }
	inline Dictionary_2_t0FCB090B06AD243E1AEFE7A77D6E44B002171473 ** get_address_of_m_dicTrackConfig_21() { return &___m_dicTrackConfig_21; }
	inline void set_m_dicTrackConfig_21(Dictionary_2_t0FCB090B06AD243E1AEFE7A77D6E44B002171473 * value)
	{
		___m_dicTrackConfig_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicTrackConfig_21), value);
	}

	inline static int32_t get_offset_of_m_dicTrackAndLevel2ResId_22() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicTrackAndLevel2ResId_22)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_m_dicTrackAndLevel2ResId_22() const { return ___m_dicTrackAndLevel2ResId_22; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_m_dicTrackAndLevel2ResId_22() { return &___m_dicTrackAndLevel2ResId_22; }
	inline void set_m_dicTrackAndLevel2ResId_22(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___m_dicTrackAndLevel2ResId_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicTrackAndLevel2ResId_22), value);
	}

	inline static int32_t get_offset_of_tempAutomobileConfig_23() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___tempAutomobileConfig_23)); }
	inline sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20  get_tempAutomobileConfig_23() const { return ___tempAutomobileConfig_23; }
	inline sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20 * get_address_of_tempAutomobileConfig_23() { return &___tempAutomobileConfig_23; }
	inline void set_tempAutomobileConfig_23(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20  value)
	{
		___tempAutomobileConfig_23 = value;
	}

	inline static int32_t get_offset_of_tempPlanetConfig_24() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___tempPlanetConfig_24)); }
	inline sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9  get_tempPlanetConfig_24() const { return ___tempPlanetConfig_24; }
	inline sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9 * get_address_of_tempPlanetConfig_24() { return &___tempPlanetConfig_24; }
	inline void set_tempPlanetConfig_24(sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9  value)
	{
		___tempPlanetConfig_24 = value;
	}

	inline static int32_t get_offset_of_tempTrackConfig_25() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___tempTrackConfig_25)); }
	inline sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B  get_tempTrackConfig_25() const { return ___tempTrackConfig_25; }
	inline sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B * get_address_of_tempTrackConfig_25() { return &___tempTrackConfig_25; }
	inline void set_tempTrackConfig_25(sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B  value)
	{
		___tempTrackConfig_25 = value;
	}

	inline static int32_t get_offset_of_tempPrestigeConfig_26() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___tempPrestigeConfig_26)); }
	inline sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63  get_tempPrestigeConfig_26() const { return ___tempPrestigeConfig_26; }
	inline sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63 * get_address_of_tempPrestigeConfig_26() { return &___tempPrestigeConfig_26; }
	inline void set_tempPrestigeConfig_26(sPrestigeConfig_t397FC1BE163EC0B938F2EC3B56694DCA340BAC63  value)
	{
		___tempPrestigeConfig_26 = value;
	}

	inline static int32_t get_offset_of_m_dicPrestigeConfig_27() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicPrestigeConfig_27)); }
	inline Dictionary_2_t6927BC972CD2E0E92AF0DF540164BAF9ECB65E17 * get_m_dicPrestigeConfig_27() const { return ___m_dicPrestigeConfig_27; }
	inline Dictionary_2_t6927BC972CD2E0E92AF0DF540164BAF9ECB65E17 ** get_address_of_m_dicPrestigeConfig_27() { return &___m_dicPrestigeConfig_27; }
	inline void set_m_dicPrestigeConfig_27(Dictionary_2_t6927BC972CD2E0E92AF0DF540164BAF9ECB65E17 * value)
	{
		___m_dicPrestigeConfig_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicPrestigeConfig_27), value);
	}

	inline static int32_t get_offset_of_m_bPlanetConfigLoaded_28() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_bPlanetConfigLoaded_28)); }
	inline bool get_m_bPlanetConfigLoaded_28() const { return ___m_bPlanetConfigLoaded_28; }
	inline bool* get_address_of_m_bPlanetConfigLoaded_28() { return &___m_bPlanetConfigLoaded_28; }
	inline void set_m_bPlanetConfigLoaded_28(bool value)
	{
		___m_bPlanetConfigLoaded_28 = value;
	}

	inline static int32_t get_offset_of_m_bAutomobileConfigLoaded_29() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_bAutomobileConfigLoaded_29)); }
	inline bool get_m_bAutomobileConfigLoaded_29() const { return ___m_bAutomobileConfigLoaded_29; }
	inline bool* get_address_of_m_bAutomobileConfigLoaded_29() { return &___m_bAutomobileConfigLoaded_29; }
	inline void set_m_bAutomobileConfigLoaded_29(bool value)
	{
		___m_bAutomobileConfigLoaded_29 = value;
	}

	inline static int32_t get_offset_of_m_bTrackConfigLoaded_30() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_bTrackConfigLoaded_30)); }
	inline bool get_m_bTrackConfigLoaded_30() const { return ___m_bTrackConfigLoaded_30; }
	inline bool* get_address_of_m_bTrackConfigLoaded_30() { return &___m_bTrackConfigLoaded_30; }
	inline void set_m_bTrackConfigLoaded_30(bool value)
	{
		___m_bTrackConfigLoaded_30 = value;
	}

	inline static int32_t get_offset_of_m_dicLoadMyData_String_31() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicLoadMyData_String_31)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_m_dicLoadMyData_String_31() const { return ___m_dicLoadMyData_String_31; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_m_dicLoadMyData_String_31() { return &___m_dicLoadMyData_String_31; }
	inline void set_m_dicLoadMyData_String_31(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___m_dicLoadMyData_String_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLoadMyData_String_31), value);
	}

	inline static int32_t get_offset_of_m_dicLoadMyData_32() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_dicLoadMyData_32)); }
	inline Dictionary_2_tD9E97F40821434226F92E86AFDAE50D233468602 * get_m_dicLoadMyData_32() const { return ___m_dicLoadMyData_32; }
	inline Dictionary_2_tD9E97F40821434226F92E86AFDAE50D233468602 ** get_address_of_m_dicLoadMyData_32() { return &___m_dicLoadMyData_32; }
	inline void set_m_dicLoadMyData_32(Dictionary_2_tD9E97F40821434226F92E86AFDAE50D233468602 * value)
	{
		___m_dicLoadMyData_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLoadMyData_32), value);
	}

	inline static int32_t get_offset_of_m_bCanSave_33() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30, ___m_bCanSave_33)); }
	inline bool get_m_bCanSave_33() const { return ___m_bCanSave_33; }
	inline bool* get_address_of_m_bCanSave_33() { return &___m_bCanSave_33; }
	inline void set_m_bCanSave_33(bool value)
	{
		___m_bCanSave_33 = value;
	}
};

struct DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields
{
public:
	// DataManager DataManager::s_Instance
	DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * ___s_Instance_4;
	// System.String DataManager::url
	String_t* ___url_6;
	// System.String DataManager::url_formal
	String_t* ___url_formal_7;
	// System.String DataManager::url_test
	String_t* ___url_test_8;
	// System.String DataManager::url_offline
	String_t* ___url_offline_9;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields, ___s_Instance_4)); }
	inline DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_url_6() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields, ___url_6)); }
	inline String_t* get_url_6() const { return ___url_6; }
	inline String_t** get_address_of_url_6() { return &___url_6; }
	inline void set_url_6(String_t* value)
	{
		___url_6 = value;
		Il2CppCodeGenWriteBarrier((&___url_6), value);
	}

	inline static int32_t get_offset_of_url_formal_7() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields, ___url_formal_7)); }
	inline String_t* get_url_formal_7() const { return ___url_formal_7; }
	inline String_t** get_address_of_url_formal_7() { return &___url_formal_7; }
	inline void set_url_formal_7(String_t* value)
	{
		___url_formal_7 = value;
		Il2CppCodeGenWriteBarrier((&___url_formal_7), value);
	}

	inline static int32_t get_offset_of_url_test_8() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields, ___url_test_8)); }
	inline String_t* get_url_test_8() const { return ___url_test_8; }
	inline String_t** get_address_of_url_test_8() { return &___url_test_8; }
	inline void set_url_test_8(String_t* value)
	{
		___url_test_8 = value;
		Il2CppCodeGenWriteBarrier((&___url_test_8), value);
	}

	inline static int32_t get_offset_of_url_offline_9() { return static_cast<int32_t>(offsetof(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields, ___url_offline_9)); }
	inline String_t* get_url_offline_9() const { return ___url_offline_9; }
	inline String_t** get_address_of_url_offline_9() { return &___url_offline_9; }
	inline void set_url_offline_9(String_t* value)
	{
		___url_offline_9 = value;
		Il2CppCodeGenWriteBarrier((&___url_offline_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAMANAGER_TE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_H
#ifndef DISTRICT_T773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A_H
#define DISTRICT_T773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// District
struct  District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.DateTime District::m_dtLastWatchAdsVehicleFreeTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dtLastWatchAdsVehicleFreeTime_4;
	// Planet District::m_BoundPlanet
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * ___m_BoundPlanet_5;
	// MapManager/eDistrictStatus District::m_eStatus
	int32_t ___m_eStatus_6;
	// System.Int32 District::m_nId
	int32_t ___m_nId_7;
	// System.Int32 District::m_nUnlockPrice
	int32_t ___m_nUnlockPrice_8;
	// System.String District::m_szData
	String_t* ___m_szData_9;
	// System.Int32 District::m_nPrestigeTimes
	int32_t ___m_nPrestigeTimes_10;
	// System.Int32[] District::m_aryVehicleBuyTimes
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_aryVehicleBuyTimes_11;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> District::m_dicVehicleBuyTimes
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_dicVehicleBuyTimes_12;
	// System.Int32 District::m_nAdsRaiseTime
	int32_t ___m_nAdsRaiseTime_13;
	// System.Collections.Generic.Dictionary`2<SkillManager/eSkillType,Skill> District::m_dicSkill
	Dictionary_2_t5BEFDBA4420F57163BF6DF9697D75434371B6E60 * ___m_dicSkill_14;
	// SkillManager/sSkillConfig District::config
	sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E  ___config_15;
	// System.Int32 District::m_nAdsBaseTime
	int32_t ___m_nAdsBaseTime_16;
	// System.Collections.Generic.List`1<Plane> District::m_lstRunningPlanes
	List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * ___m_lstRunningPlanes_17;
	// System.Int32 District::m_nLevel
	int32_t ___m_nLevel_18;
	// System.Int32 District::m_nBuyAdminTimes
	int32_t ___m_nBuyAdminTimes_19;
	// UIAdministratorCounter District::m_CurUsingAdmin
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78 * ___m_CurUsingAdmin_20;
	// System.Collections.Generic.List`1<Admin> District::m_lstAdmins
	List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34 * ___m_lstAdmins_21;
	// ResearchCounter District::m_ResearchCounter
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * ___m_ResearchCounter_22;
	// System.Int32 District::m_nDropLevel
	int32_t ___m_nDropLevel_23;
	// System.Int32 District::m_nDropInterval
	int32_t ___m_nDropInterval_24;
	// System.Int32 District::m_nMaxCoinBuyLevel
	int32_t ___m_nMaxCoinBuyLevel_25;
	// System.Int32 District::m_nCurLotNum
	int32_t ___m_nCurLotNum_26;
	// System.DateTime District::m_dtAdsStartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dtAdsStartTime_27;
	// System.Int32 District::m_nAdsLeftTime
	int32_t ___m_nAdsLeftTime_28;
	// System.DateTime District::m_fStartOfflineTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_fStartOfflineTime_29;
	// System.DateTime District::m_fLastOnlineTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_fLastOnlineTime_30;
	// System.Double District::m_fOfflineGainPerSecondBase
	double ___m_fOfflineGainPerSecondBase_31;
	// System.Double District::m_fOfflineGainPerSecondReal
	double ___m_fOfflineGainPerSecondReal_32;
	// System.Boolean District::m_bOffline
	bool ___m_bOffline_33;
	// System.Double District::m_fDPS
	double ___m_fDPS_34;
	// System.Single District::m_fSpeedAccelerateRate
	float ___m_fSpeedAccelerateRate_35;
	// System.Single District::m_fTimeElapseOffline
	float ___m_fTimeElapseOffline_37;
	// System.Double District::m_fCurTotalOfflineGain
	double ___m_fCurTotalOfflineGain_38;
	// System.Collections.Generic.List`1<District/sLevelAndCost> District::m_lstLevelAndCost
	List_1_tB9FA86EA4317FBDAC182273F5D02A08CB5B68D86 * ___m_lstLevelAndCost_39;
	// District/sLevelAndCost District::m_TheSelectedOne
	sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA  ___m_TheSelectedOne_40;
	// System.Collections.Generic.Dictionary`2<System.Int32,ResearchCounter> District::m_dicEnergyResearchCounters
	Dictionary_2_t1D475F8089C35FF3718700296FB38C143E09149F * ___m_dicEnergyResearchCounters_41;
	// System.Single District::m_fTrackCoinRaise
	float ___m_fTrackCoinRaise_42;
	// System.Single District::m_fSpeedAccelerate
	float ___m_fSpeedAccelerate_43;
	// System.Single[] District::m_aryAccelerate
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryAccelerate_45;

public:
	inline static int32_t get_offset_of_m_dtLastWatchAdsVehicleFreeTime_4() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_dtLastWatchAdsVehicleFreeTime_4)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dtLastWatchAdsVehicleFreeTime_4() const { return ___m_dtLastWatchAdsVehicleFreeTime_4; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dtLastWatchAdsVehicleFreeTime_4() { return &___m_dtLastWatchAdsVehicleFreeTime_4; }
	inline void set_m_dtLastWatchAdsVehicleFreeTime_4(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dtLastWatchAdsVehicleFreeTime_4 = value;
	}

	inline static int32_t get_offset_of_m_BoundPlanet_5() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_BoundPlanet_5)); }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * get_m_BoundPlanet_5() const { return ___m_BoundPlanet_5; }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B ** get_address_of_m_BoundPlanet_5() { return &___m_BoundPlanet_5; }
	inline void set_m_BoundPlanet_5(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * value)
	{
		___m_BoundPlanet_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundPlanet_5), value);
	}

	inline static int32_t get_offset_of_m_eStatus_6() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_eStatus_6)); }
	inline int32_t get_m_eStatus_6() const { return ___m_eStatus_6; }
	inline int32_t* get_address_of_m_eStatus_6() { return &___m_eStatus_6; }
	inline void set_m_eStatus_6(int32_t value)
	{
		___m_eStatus_6 = value;
	}

	inline static int32_t get_offset_of_m_nId_7() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nId_7)); }
	inline int32_t get_m_nId_7() const { return ___m_nId_7; }
	inline int32_t* get_address_of_m_nId_7() { return &___m_nId_7; }
	inline void set_m_nId_7(int32_t value)
	{
		___m_nId_7 = value;
	}

	inline static int32_t get_offset_of_m_nUnlockPrice_8() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nUnlockPrice_8)); }
	inline int32_t get_m_nUnlockPrice_8() const { return ___m_nUnlockPrice_8; }
	inline int32_t* get_address_of_m_nUnlockPrice_8() { return &___m_nUnlockPrice_8; }
	inline void set_m_nUnlockPrice_8(int32_t value)
	{
		___m_nUnlockPrice_8 = value;
	}

	inline static int32_t get_offset_of_m_szData_9() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_szData_9)); }
	inline String_t* get_m_szData_9() const { return ___m_szData_9; }
	inline String_t** get_address_of_m_szData_9() { return &___m_szData_9; }
	inline void set_m_szData_9(String_t* value)
	{
		___m_szData_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_szData_9), value);
	}

	inline static int32_t get_offset_of_m_nPrestigeTimes_10() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nPrestigeTimes_10)); }
	inline int32_t get_m_nPrestigeTimes_10() const { return ___m_nPrestigeTimes_10; }
	inline int32_t* get_address_of_m_nPrestigeTimes_10() { return &___m_nPrestigeTimes_10; }
	inline void set_m_nPrestigeTimes_10(int32_t value)
	{
		___m_nPrestigeTimes_10 = value;
	}

	inline static int32_t get_offset_of_m_aryVehicleBuyTimes_11() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_aryVehicleBuyTimes_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_aryVehicleBuyTimes_11() const { return ___m_aryVehicleBuyTimes_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_aryVehicleBuyTimes_11() { return &___m_aryVehicleBuyTimes_11; }
	inline void set_m_aryVehicleBuyTimes_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_aryVehicleBuyTimes_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryVehicleBuyTimes_11), value);
	}

	inline static int32_t get_offset_of_m_dicVehicleBuyTimes_12() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_dicVehicleBuyTimes_12)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_dicVehicleBuyTimes_12() const { return ___m_dicVehicleBuyTimes_12; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_dicVehicleBuyTimes_12() { return &___m_dicVehicleBuyTimes_12; }
	inline void set_m_dicVehicleBuyTimes_12(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_dicVehicleBuyTimes_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicVehicleBuyTimes_12), value);
	}

	inline static int32_t get_offset_of_m_nAdsRaiseTime_13() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nAdsRaiseTime_13)); }
	inline int32_t get_m_nAdsRaiseTime_13() const { return ___m_nAdsRaiseTime_13; }
	inline int32_t* get_address_of_m_nAdsRaiseTime_13() { return &___m_nAdsRaiseTime_13; }
	inline void set_m_nAdsRaiseTime_13(int32_t value)
	{
		___m_nAdsRaiseTime_13 = value;
	}

	inline static int32_t get_offset_of_m_dicSkill_14() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_dicSkill_14)); }
	inline Dictionary_2_t5BEFDBA4420F57163BF6DF9697D75434371B6E60 * get_m_dicSkill_14() const { return ___m_dicSkill_14; }
	inline Dictionary_2_t5BEFDBA4420F57163BF6DF9697D75434371B6E60 ** get_address_of_m_dicSkill_14() { return &___m_dicSkill_14; }
	inline void set_m_dicSkill_14(Dictionary_2_t5BEFDBA4420F57163BF6DF9697D75434371B6E60 * value)
	{
		___m_dicSkill_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicSkill_14), value);
	}

	inline static int32_t get_offset_of_config_15() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___config_15)); }
	inline sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E  get_config_15() const { return ___config_15; }
	inline sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E * get_address_of_config_15() { return &___config_15; }
	inline void set_config_15(sSkillConfig_tC3EE1E2CD2099E7F6F0D05D27656D42D635FEC6E  value)
	{
		___config_15 = value;
	}

	inline static int32_t get_offset_of_m_nAdsBaseTime_16() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nAdsBaseTime_16)); }
	inline int32_t get_m_nAdsBaseTime_16() const { return ___m_nAdsBaseTime_16; }
	inline int32_t* get_address_of_m_nAdsBaseTime_16() { return &___m_nAdsBaseTime_16; }
	inline void set_m_nAdsBaseTime_16(int32_t value)
	{
		___m_nAdsBaseTime_16 = value;
	}

	inline static int32_t get_offset_of_m_lstRunningPlanes_17() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_lstRunningPlanes_17)); }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * get_m_lstRunningPlanes_17() const { return ___m_lstRunningPlanes_17; }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 ** get_address_of_m_lstRunningPlanes_17() { return &___m_lstRunningPlanes_17; }
	inline void set_m_lstRunningPlanes_17(List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * value)
	{
		___m_lstRunningPlanes_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRunningPlanes_17), value);
	}

	inline static int32_t get_offset_of_m_nLevel_18() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nLevel_18)); }
	inline int32_t get_m_nLevel_18() const { return ___m_nLevel_18; }
	inline int32_t* get_address_of_m_nLevel_18() { return &___m_nLevel_18; }
	inline void set_m_nLevel_18(int32_t value)
	{
		___m_nLevel_18 = value;
	}

	inline static int32_t get_offset_of_m_nBuyAdminTimes_19() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nBuyAdminTimes_19)); }
	inline int32_t get_m_nBuyAdminTimes_19() const { return ___m_nBuyAdminTimes_19; }
	inline int32_t* get_address_of_m_nBuyAdminTimes_19() { return &___m_nBuyAdminTimes_19; }
	inline void set_m_nBuyAdminTimes_19(int32_t value)
	{
		___m_nBuyAdminTimes_19 = value;
	}

	inline static int32_t get_offset_of_m_CurUsingAdmin_20() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_CurUsingAdmin_20)); }
	inline UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78 * get_m_CurUsingAdmin_20() const { return ___m_CurUsingAdmin_20; }
	inline UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78 ** get_address_of_m_CurUsingAdmin_20() { return &___m_CurUsingAdmin_20; }
	inline void set_m_CurUsingAdmin_20(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78 * value)
	{
		___m_CurUsingAdmin_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurUsingAdmin_20), value);
	}

	inline static int32_t get_offset_of_m_lstAdmins_21() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_lstAdmins_21)); }
	inline List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34 * get_m_lstAdmins_21() const { return ___m_lstAdmins_21; }
	inline List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34 ** get_address_of_m_lstAdmins_21() { return &___m_lstAdmins_21; }
	inline void set_m_lstAdmins_21(List_1_t8A3969FEBA51DA7D8779DAD3F075653A167D3A34 * value)
	{
		___m_lstAdmins_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstAdmins_21), value);
	}

	inline static int32_t get_offset_of_m_ResearchCounter_22() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_ResearchCounter_22)); }
	inline ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * get_m_ResearchCounter_22() const { return ___m_ResearchCounter_22; }
	inline ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD ** get_address_of_m_ResearchCounter_22() { return &___m_ResearchCounter_22; }
	inline void set_m_ResearchCounter_22(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * value)
	{
		___m_ResearchCounter_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ResearchCounter_22), value);
	}

	inline static int32_t get_offset_of_m_nDropLevel_23() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nDropLevel_23)); }
	inline int32_t get_m_nDropLevel_23() const { return ___m_nDropLevel_23; }
	inline int32_t* get_address_of_m_nDropLevel_23() { return &___m_nDropLevel_23; }
	inline void set_m_nDropLevel_23(int32_t value)
	{
		___m_nDropLevel_23 = value;
	}

	inline static int32_t get_offset_of_m_nDropInterval_24() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nDropInterval_24)); }
	inline int32_t get_m_nDropInterval_24() const { return ___m_nDropInterval_24; }
	inline int32_t* get_address_of_m_nDropInterval_24() { return &___m_nDropInterval_24; }
	inline void set_m_nDropInterval_24(int32_t value)
	{
		___m_nDropInterval_24 = value;
	}

	inline static int32_t get_offset_of_m_nMaxCoinBuyLevel_25() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nMaxCoinBuyLevel_25)); }
	inline int32_t get_m_nMaxCoinBuyLevel_25() const { return ___m_nMaxCoinBuyLevel_25; }
	inline int32_t* get_address_of_m_nMaxCoinBuyLevel_25() { return &___m_nMaxCoinBuyLevel_25; }
	inline void set_m_nMaxCoinBuyLevel_25(int32_t value)
	{
		___m_nMaxCoinBuyLevel_25 = value;
	}

	inline static int32_t get_offset_of_m_nCurLotNum_26() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nCurLotNum_26)); }
	inline int32_t get_m_nCurLotNum_26() const { return ___m_nCurLotNum_26; }
	inline int32_t* get_address_of_m_nCurLotNum_26() { return &___m_nCurLotNum_26; }
	inline void set_m_nCurLotNum_26(int32_t value)
	{
		___m_nCurLotNum_26 = value;
	}

	inline static int32_t get_offset_of_m_dtAdsStartTime_27() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_dtAdsStartTime_27)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dtAdsStartTime_27() const { return ___m_dtAdsStartTime_27; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dtAdsStartTime_27() { return &___m_dtAdsStartTime_27; }
	inline void set_m_dtAdsStartTime_27(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dtAdsStartTime_27 = value;
	}

	inline static int32_t get_offset_of_m_nAdsLeftTime_28() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_nAdsLeftTime_28)); }
	inline int32_t get_m_nAdsLeftTime_28() const { return ___m_nAdsLeftTime_28; }
	inline int32_t* get_address_of_m_nAdsLeftTime_28() { return &___m_nAdsLeftTime_28; }
	inline void set_m_nAdsLeftTime_28(int32_t value)
	{
		___m_nAdsLeftTime_28 = value;
	}

	inline static int32_t get_offset_of_m_fStartOfflineTime_29() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fStartOfflineTime_29)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_fStartOfflineTime_29() const { return ___m_fStartOfflineTime_29; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_fStartOfflineTime_29() { return &___m_fStartOfflineTime_29; }
	inline void set_m_fStartOfflineTime_29(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_fStartOfflineTime_29 = value;
	}

	inline static int32_t get_offset_of_m_fLastOnlineTime_30() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fLastOnlineTime_30)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_fLastOnlineTime_30() const { return ___m_fLastOnlineTime_30; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_fLastOnlineTime_30() { return &___m_fLastOnlineTime_30; }
	inline void set_m_fLastOnlineTime_30(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_fLastOnlineTime_30 = value;
	}

	inline static int32_t get_offset_of_m_fOfflineGainPerSecondBase_31() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fOfflineGainPerSecondBase_31)); }
	inline double get_m_fOfflineGainPerSecondBase_31() const { return ___m_fOfflineGainPerSecondBase_31; }
	inline double* get_address_of_m_fOfflineGainPerSecondBase_31() { return &___m_fOfflineGainPerSecondBase_31; }
	inline void set_m_fOfflineGainPerSecondBase_31(double value)
	{
		___m_fOfflineGainPerSecondBase_31 = value;
	}

	inline static int32_t get_offset_of_m_fOfflineGainPerSecondReal_32() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fOfflineGainPerSecondReal_32)); }
	inline double get_m_fOfflineGainPerSecondReal_32() const { return ___m_fOfflineGainPerSecondReal_32; }
	inline double* get_address_of_m_fOfflineGainPerSecondReal_32() { return &___m_fOfflineGainPerSecondReal_32; }
	inline void set_m_fOfflineGainPerSecondReal_32(double value)
	{
		___m_fOfflineGainPerSecondReal_32 = value;
	}

	inline static int32_t get_offset_of_m_bOffline_33() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_bOffline_33)); }
	inline bool get_m_bOffline_33() const { return ___m_bOffline_33; }
	inline bool* get_address_of_m_bOffline_33() { return &___m_bOffline_33; }
	inline void set_m_bOffline_33(bool value)
	{
		___m_bOffline_33 = value;
	}

	inline static int32_t get_offset_of_m_fDPS_34() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fDPS_34)); }
	inline double get_m_fDPS_34() const { return ___m_fDPS_34; }
	inline double* get_address_of_m_fDPS_34() { return &___m_fDPS_34; }
	inline void set_m_fDPS_34(double value)
	{
		___m_fDPS_34 = value;
	}

	inline static int32_t get_offset_of_m_fSpeedAccelerateRate_35() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fSpeedAccelerateRate_35)); }
	inline float get_m_fSpeedAccelerateRate_35() const { return ___m_fSpeedAccelerateRate_35; }
	inline float* get_address_of_m_fSpeedAccelerateRate_35() { return &___m_fSpeedAccelerateRate_35; }
	inline void set_m_fSpeedAccelerateRate_35(float value)
	{
		___m_fSpeedAccelerateRate_35 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapseOffline_37() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fTimeElapseOffline_37)); }
	inline float get_m_fTimeElapseOffline_37() const { return ___m_fTimeElapseOffline_37; }
	inline float* get_address_of_m_fTimeElapseOffline_37() { return &___m_fTimeElapseOffline_37; }
	inline void set_m_fTimeElapseOffline_37(float value)
	{
		___m_fTimeElapseOffline_37 = value;
	}

	inline static int32_t get_offset_of_m_fCurTotalOfflineGain_38() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fCurTotalOfflineGain_38)); }
	inline double get_m_fCurTotalOfflineGain_38() const { return ___m_fCurTotalOfflineGain_38; }
	inline double* get_address_of_m_fCurTotalOfflineGain_38() { return &___m_fCurTotalOfflineGain_38; }
	inline void set_m_fCurTotalOfflineGain_38(double value)
	{
		___m_fCurTotalOfflineGain_38 = value;
	}

	inline static int32_t get_offset_of_m_lstLevelAndCost_39() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_lstLevelAndCost_39)); }
	inline List_1_tB9FA86EA4317FBDAC182273F5D02A08CB5B68D86 * get_m_lstLevelAndCost_39() const { return ___m_lstLevelAndCost_39; }
	inline List_1_tB9FA86EA4317FBDAC182273F5D02A08CB5B68D86 ** get_address_of_m_lstLevelAndCost_39() { return &___m_lstLevelAndCost_39; }
	inline void set_m_lstLevelAndCost_39(List_1_tB9FA86EA4317FBDAC182273F5D02A08CB5B68D86 * value)
	{
		___m_lstLevelAndCost_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstLevelAndCost_39), value);
	}

	inline static int32_t get_offset_of_m_TheSelectedOne_40() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_TheSelectedOne_40)); }
	inline sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA  get_m_TheSelectedOne_40() const { return ___m_TheSelectedOne_40; }
	inline sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA * get_address_of_m_TheSelectedOne_40() { return &___m_TheSelectedOne_40; }
	inline void set_m_TheSelectedOne_40(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA  value)
	{
		___m_TheSelectedOne_40 = value;
	}

	inline static int32_t get_offset_of_m_dicEnergyResearchCounters_41() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_dicEnergyResearchCounters_41)); }
	inline Dictionary_2_t1D475F8089C35FF3718700296FB38C143E09149F * get_m_dicEnergyResearchCounters_41() const { return ___m_dicEnergyResearchCounters_41; }
	inline Dictionary_2_t1D475F8089C35FF3718700296FB38C143E09149F ** get_address_of_m_dicEnergyResearchCounters_41() { return &___m_dicEnergyResearchCounters_41; }
	inline void set_m_dicEnergyResearchCounters_41(Dictionary_2_t1D475F8089C35FF3718700296FB38C143E09149F * value)
	{
		___m_dicEnergyResearchCounters_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicEnergyResearchCounters_41), value);
	}

	inline static int32_t get_offset_of_m_fTrackCoinRaise_42() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fTrackCoinRaise_42)); }
	inline float get_m_fTrackCoinRaise_42() const { return ___m_fTrackCoinRaise_42; }
	inline float* get_address_of_m_fTrackCoinRaise_42() { return &___m_fTrackCoinRaise_42; }
	inline void set_m_fTrackCoinRaise_42(float value)
	{
		___m_fTrackCoinRaise_42 = value;
	}

	inline static int32_t get_offset_of_m_fSpeedAccelerate_43() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_fSpeedAccelerate_43)); }
	inline float get_m_fSpeedAccelerate_43() const { return ___m_fSpeedAccelerate_43; }
	inline float* get_address_of_m_fSpeedAccelerate_43() { return &___m_fSpeedAccelerate_43; }
	inline void set_m_fSpeedAccelerate_43(float value)
	{
		___m_fSpeedAccelerate_43 = value;
	}

	inline static int32_t get_offset_of_m_aryAccelerate_45() { return static_cast<int32_t>(offsetof(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A, ___m_aryAccelerate_45)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryAccelerate_45() const { return ___m_aryAccelerate_45; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryAccelerate_45() { return &___m_aryAccelerate_45; }
	inline void set_m_aryAccelerate_45(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryAccelerate_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAccelerate_45), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTRICT_T773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A_H
#ifndef FRESHGUIDE_T840B865F5BBA1373DC91F27C5939E88FD6095DD3_H
#define FRESHGUIDE_T840B865F5BBA1373DC91F27C5939E88FD6095DD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FreshGuide
struct  FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject FreshGuide::_containerScene
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerScene_6;
	// UnityEngine.GameObject FreshGuide::_goMask
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goMask_7;
	// System.Boolean FreshGuide::m_bMaskVisible
	bool ___m_bMaskVisible_8;
	// UnityEngine.UI.Text FreshGuide::_txtX
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtX_9;
	// UnityEngine.UI.Text FreshGuide::_txtY
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtY_10;
	// UnityEngine.UI.Text FreshGuide::_txtRadius
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtRadius_11;
	// UnityEngine.UI.Text FreshGuide::_txtPawX
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPawX_12;
	// UnityEngine.UI.Text FreshGuide::_txtPawY
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPawY_13;
	// UnityEngine.Material FreshGuide::m_matMask
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_matMask_14;
	// System.Single FreshGuide::m_fX
	float ___m_fX_15;
	// System.Single FreshGuide::m_fY
	float ___m_fY_16;
	// System.Single FreshGuide::m_fRadius
	float ___m_fRadius_17;
	// UnityEngine.GameObject FreshGuide::_uiRecommend
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiRecommend_18;
	// UnityEngine.GameObject FreshGuide::_lot0
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lot0_19;
	// UnityEngine.GameObject FreshGuide::_lot1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lot1_20;
	// UnityEngine.GameObject FreshGuide::_lot2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lot2_21;
	// UnityEngine.GameObject FreshGuide::_lot3
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lot3_22;
	// System.Collections.Generic.Dictionary`2<FreshGuide/eGuideType,System.Boolean> FreshGuide::m_dicDone
	Dictionary_2_tE758D927E90AD1ECFDE01DDC3DB458104EDA3D92 * ___m_dicDone_26;
	// Paw FreshGuide::_paw
	Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * ____paw_27;
	// CFrameAnimationEffect FreshGuide::_faClick
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * ____faClick_28;
	// FreshGuide/eGuideType FreshGuide::m_eCurType
	int32_t ___m_eCurType_29;
	// System.Int32 FreshGuide::m_nStatus
	int32_t ___m_nStatus_30;
	// System.Int32 FreshGuide::m_nSubStatus
	int32_t ___m_nSubStatus_31;
	// System.Int32[] FreshGuide::m_aryIntParams
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_aryIntParams_32;
	// System.Single[] FreshGuide::m_aryFloatParams
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryFloatParams_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> FreshGuide::m_dicFloatParams
	Dictionary_2_t89F484A77FD6673A172CE80BD0A37031FBD1FD6A * ___m_dicFloatParams_34;
	// System.Single FreshGuide::m_fTimeElapse
	float ___m_fTimeElapse_35;
	// System.Boolean FreshGuide::m_bMasking
	bool ___m_bMasking_36;

public:
	inline static int32_t get_offset_of__containerScene_6() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____containerScene_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerScene_6() const { return ____containerScene_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerScene_6() { return &____containerScene_6; }
	inline void set__containerScene_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerScene_6 = value;
		Il2CppCodeGenWriteBarrier((&____containerScene_6), value);
	}

	inline static int32_t get_offset_of__goMask_7() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____goMask_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goMask_7() const { return ____goMask_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goMask_7() { return &____goMask_7; }
	inline void set__goMask_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goMask_7 = value;
		Il2CppCodeGenWriteBarrier((&____goMask_7), value);
	}

	inline static int32_t get_offset_of_m_bMaskVisible_8() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_bMaskVisible_8)); }
	inline bool get_m_bMaskVisible_8() const { return ___m_bMaskVisible_8; }
	inline bool* get_address_of_m_bMaskVisible_8() { return &___m_bMaskVisible_8; }
	inline void set_m_bMaskVisible_8(bool value)
	{
		___m_bMaskVisible_8 = value;
	}

	inline static int32_t get_offset_of__txtX_9() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____txtX_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtX_9() const { return ____txtX_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtX_9() { return &____txtX_9; }
	inline void set__txtX_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtX_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtX_9), value);
	}

	inline static int32_t get_offset_of__txtY_10() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____txtY_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtY_10() const { return ____txtY_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtY_10() { return &____txtY_10; }
	inline void set__txtY_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtY_10 = value;
		Il2CppCodeGenWriteBarrier((&____txtY_10), value);
	}

	inline static int32_t get_offset_of__txtRadius_11() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____txtRadius_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtRadius_11() const { return ____txtRadius_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtRadius_11() { return &____txtRadius_11; }
	inline void set__txtRadius_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtRadius_11 = value;
		Il2CppCodeGenWriteBarrier((&____txtRadius_11), value);
	}

	inline static int32_t get_offset_of__txtPawX_12() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____txtPawX_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPawX_12() const { return ____txtPawX_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPawX_12() { return &____txtPawX_12; }
	inline void set__txtPawX_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPawX_12 = value;
		Il2CppCodeGenWriteBarrier((&____txtPawX_12), value);
	}

	inline static int32_t get_offset_of__txtPawY_13() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____txtPawY_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPawY_13() const { return ____txtPawY_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPawY_13() { return &____txtPawY_13; }
	inline void set__txtPawY_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPawY_13 = value;
		Il2CppCodeGenWriteBarrier((&____txtPawY_13), value);
	}

	inline static int32_t get_offset_of_m_matMask_14() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_matMask_14)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_matMask_14() const { return ___m_matMask_14; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_matMask_14() { return &___m_matMask_14; }
	inline void set_m_matMask_14(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_matMask_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_matMask_14), value);
	}

	inline static int32_t get_offset_of_m_fX_15() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_fX_15)); }
	inline float get_m_fX_15() const { return ___m_fX_15; }
	inline float* get_address_of_m_fX_15() { return &___m_fX_15; }
	inline void set_m_fX_15(float value)
	{
		___m_fX_15 = value;
	}

	inline static int32_t get_offset_of_m_fY_16() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_fY_16)); }
	inline float get_m_fY_16() const { return ___m_fY_16; }
	inline float* get_address_of_m_fY_16() { return &___m_fY_16; }
	inline void set_m_fY_16(float value)
	{
		___m_fY_16 = value;
	}

	inline static int32_t get_offset_of_m_fRadius_17() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_fRadius_17)); }
	inline float get_m_fRadius_17() const { return ___m_fRadius_17; }
	inline float* get_address_of_m_fRadius_17() { return &___m_fRadius_17; }
	inline void set_m_fRadius_17(float value)
	{
		___m_fRadius_17 = value;
	}

	inline static int32_t get_offset_of__uiRecommend_18() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____uiRecommend_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiRecommend_18() const { return ____uiRecommend_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiRecommend_18() { return &____uiRecommend_18; }
	inline void set__uiRecommend_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiRecommend_18 = value;
		Il2CppCodeGenWriteBarrier((&____uiRecommend_18), value);
	}

	inline static int32_t get_offset_of__lot0_19() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____lot0_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lot0_19() const { return ____lot0_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lot0_19() { return &____lot0_19; }
	inline void set__lot0_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lot0_19 = value;
		Il2CppCodeGenWriteBarrier((&____lot0_19), value);
	}

	inline static int32_t get_offset_of__lot1_20() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____lot1_20)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lot1_20() const { return ____lot1_20; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lot1_20() { return &____lot1_20; }
	inline void set__lot1_20(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lot1_20 = value;
		Il2CppCodeGenWriteBarrier((&____lot1_20), value);
	}

	inline static int32_t get_offset_of__lot2_21() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____lot2_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lot2_21() const { return ____lot2_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lot2_21() { return &____lot2_21; }
	inline void set__lot2_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lot2_21 = value;
		Il2CppCodeGenWriteBarrier((&____lot2_21), value);
	}

	inline static int32_t get_offset_of__lot3_22() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____lot3_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lot3_22() const { return ____lot3_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lot3_22() { return &____lot3_22; }
	inline void set__lot3_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lot3_22 = value;
		Il2CppCodeGenWriteBarrier((&____lot3_22), value);
	}

	inline static int32_t get_offset_of_m_dicDone_26() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_dicDone_26)); }
	inline Dictionary_2_tE758D927E90AD1ECFDE01DDC3DB458104EDA3D92 * get_m_dicDone_26() const { return ___m_dicDone_26; }
	inline Dictionary_2_tE758D927E90AD1ECFDE01DDC3DB458104EDA3D92 ** get_address_of_m_dicDone_26() { return &___m_dicDone_26; }
	inline void set_m_dicDone_26(Dictionary_2_tE758D927E90AD1ECFDE01DDC3DB458104EDA3D92 * value)
	{
		___m_dicDone_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicDone_26), value);
	}

	inline static int32_t get_offset_of__paw_27() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____paw_27)); }
	inline Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * get__paw_27() const { return ____paw_27; }
	inline Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 ** get_address_of__paw_27() { return &____paw_27; }
	inline void set__paw_27(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * value)
	{
		____paw_27 = value;
		Il2CppCodeGenWriteBarrier((&____paw_27), value);
	}

	inline static int32_t get_offset_of__faClick_28() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ____faClick_28)); }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * get__faClick_28() const { return ____faClick_28; }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 ** get_address_of__faClick_28() { return &____faClick_28; }
	inline void set__faClick_28(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * value)
	{
		____faClick_28 = value;
		Il2CppCodeGenWriteBarrier((&____faClick_28), value);
	}

	inline static int32_t get_offset_of_m_eCurType_29() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_eCurType_29)); }
	inline int32_t get_m_eCurType_29() const { return ___m_eCurType_29; }
	inline int32_t* get_address_of_m_eCurType_29() { return &___m_eCurType_29; }
	inline void set_m_eCurType_29(int32_t value)
	{
		___m_eCurType_29 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_30() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_nStatus_30)); }
	inline int32_t get_m_nStatus_30() const { return ___m_nStatus_30; }
	inline int32_t* get_address_of_m_nStatus_30() { return &___m_nStatus_30; }
	inline void set_m_nStatus_30(int32_t value)
	{
		___m_nStatus_30 = value;
	}

	inline static int32_t get_offset_of_m_nSubStatus_31() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_nSubStatus_31)); }
	inline int32_t get_m_nSubStatus_31() const { return ___m_nSubStatus_31; }
	inline int32_t* get_address_of_m_nSubStatus_31() { return &___m_nSubStatus_31; }
	inline void set_m_nSubStatus_31(int32_t value)
	{
		___m_nSubStatus_31 = value;
	}

	inline static int32_t get_offset_of_m_aryIntParams_32() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_aryIntParams_32)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_aryIntParams_32() const { return ___m_aryIntParams_32; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_aryIntParams_32() { return &___m_aryIntParams_32; }
	inline void set_m_aryIntParams_32(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_aryIntParams_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryIntParams_32), value);
	}

	inline static int32_t get_offset_of_m_aryFloatParams_33() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_aryFloatParams_33)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryFloatParams_33() const { return ___m_aryFloatParams_33; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryFloatParams_33() { return &___m_aryFloatParams_33; }
	inline void set_m_aryFloatParams_33(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryFloatParams_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryFloatParams_33), value);
	}

	inline static int32_t get_offset_of_m_dicFloatParams_34() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_dicFloatParams_34)); }
	inline Dictionary_2_t89F484A77FD6673A172CE80BD0A37031FBD1FD6A * get_m_dicFloatParams_34() const { return ___m_dicFloatParams_34; }
	inline Dictionary_2_t89F484A77FD6673A172CE80BD0A37031FBD1FD6A ** get_address_of_m_dicFloatParams_34() { return &___m_dicFloatParams_34; }
	inline void set_m_dicFloatParams_34(Dictionary_2_t89F484A77FD6673A172CE80BD0A37031FBD1FD6A * value)
	{
		___m_dicFloatParams_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicFloatParams_34), value);
	}

	inline static int32_t get_offset_of_m_fTimeElapse_35() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_fTimeElapse_35)); }
	inline float get_m_fTimeElapse_35() const { return ___m_fTimeElapse_35; }
	inline float* get_address_of_m_fTimeElapse_35() { return &___m_fTimeElapse_35; }
	inline void set_m_fTimeElapse_35(float value)
	{
		___m_fTimeElapse_35 = value;
	}

	inline static int32_t get_offset_of_m_bMasking_36() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3, ___m_bMasking_36)); }
	inline bool get_m_bMasking_36() const { return ___m_bMasking_36; }
	inline bool* get_address_of_m_bMasking_36() { return &___m_bMasking_36; }
	inline void set_m_bMasking_36(bool value)
	{
		___m_bMasking_36 = value;
	}
};

struct FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields
{
public:
	// UnityEngine.Vector3 FreshGuide::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_23;
	// UnityEngine.Vector3 FreshGuide::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_24;
	// FreshGuide FreshGuide::s_Instance
	FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * ___s_Instance_25;

public:
	inline static int32_t get_offset_of_vecTempPos_23() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields, ___vecTempPos_23)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_23() const { return ___vecTempPos_23; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_23() { return &___vecTempPos_23; }
	inline void set_vecTempPos_23(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_23 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_24() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields, ___vecTempScale_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_24() const { return ___vecTempScale_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_24() { return &___vecTempScale_24; }
	inline void set_vecTempScale_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_24 = value;
	}

	inline static int32_t get_offset_of_s_Instance_25() { return static_cast<int32_t>(offsetof(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields, ___s_Instance_25)); }
	inline FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * get_s_Instance_25() const { return ___s_Instance_25; }
	inline FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 ** get_address_of_s_Instance_25() { return &___s_Instance_25; }
	inline void set_s_Instance_25(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * value)
	{
		___s_Instance_25 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRESHGUIDE_T840B865F5BBA1373DC91F27C5939E88FD6095DD3_H
#ifndef MAIN_T7BD87A0CB813F834A2F93006206A3CE79188D0C1_H
#define MAIN_T7BD87A0CB813F834A2F93006206A3CE79188D0C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Main
struct  Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Main::m_goContainerEffects
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goContainerEffects_4;
	// UnityEngine.GameObject Main::_goStartPos
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goStartPos_5;
	// UnityEngine.GameObject[] Main::m_aryNodeStart
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryNodeStart_6;
	// UnityEngine.GameObject[] Main::m_aryNodeCenter
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryNodeCenter_7;
	// UnityEngine.GameObject[] Main::m_aryNodeEnd
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryNodeEnd_8;
	// System.Collections.Generic.Dictionary`2<Main/ePosType,System.Collections.Generic.List`1<Main/sAutoRunLocation>> Main::m_dicLocationPoints
	Dictionary_2_t8BBCAEE5B6551D2028E4CE6BBACCA1D12D8070C0 * ___m_dicLocationPoints_9;
	// UnityEngine.GameObject[] Main::m_aryStartLocationPointOfEachSeg
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryStartLocationPointOfEachSeg_10;
	// Main/sAutoRunLocation[] Main::m_aryStartLocationInfoPointOfEachSeg
	sAutoRunLocationU5BU5D_t1B4B4B1E8F7FDC3043F544A45744E0D6A98B8864* ___m_aryStartLocationInfoPointOfEachSeg_11;
	// UnityEngine.GameObject Main::m_containerLocations
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerLocations_12;
	// UnityEngine.UI.Text Main::_txtDebugInfo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDebugInfo_13;
	// UnityEngine.UI.Text Main::_txtOfflineDetailDebug
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtOfflineDetailDebug_14;
	// MoneyTrigger Main::_moneyTrigger
	MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464 * ____moneyTrigger_15;
	// UnityEngine.Sprite[] Main::m_aryMoneyTrigger
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryMoneyTrigger_16;
	// BaseScale Main::_basescaleCPosCoinIcon
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____basescaleCPosCoinIcon_17;
	// UnityEngine.Vector3 Main::m_vecLocalPosOnLot
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecLocalPosOnLot_22;
	// System.Single Main::m_nCurRaise
	float ___m_nCurRaise_23;
	// MoneyCounter[] Main::m_aryCoin0
	MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* ___m_aryCoin0_24;
	// MoneyCounter[] Main::m_aryCoin1
	MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* ___m_aryCoin1_25;
	// MoneyCounter[] Main::m_aryCoin2
	MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* ___m_aryCoin2_26;
	// UnityEngine.UI.Text Main::_txtPrestigeTimes
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrestigeTimes_27;
	// UnityEngine.UI.Text Main::_txtPrestigeTimes_Shadow
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrestigeTimes_Shadow_28;
	// UIStars Main::_starsPrestigeTimes
	UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * ____starsPrestigeTimes_29;
	// UnityEngine.UI.Text Main::_txtTotalCoinOfThisPlanet
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTotalCoinOfThisPlanet_30;
	// UnityEngine.TextMesh Main::_tmTotalCoinOfThisPlanet
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ____tmTotalCoinOfThisPlanet_31;
	// UnityEngine.GameObject Main::_panelCollectOfflineProfit
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelCollectOfflineProfit_32;
	// UnityEngine.UI.Text Main::_txtOfflineProfitOfThisDistrict
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtOfflineProfitOfThisDistrict_33;
	// UnityEngine.UI.Button Main::_btnOpenBigMap
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnOpenBigMap_34;
	// UnityEngine.UI.Button Main::_btnCloseBigMap
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnCloseBigMap_35;
	// MoneyCounter Main::_moneyCoin
	MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * ____moneyCoin_36;
	// MoneyCounter[] Main::m_aryActivePlanetCoin
	MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* ___m_aryActivePlanetCoin_37;
	// MoneyCounter Main::_moneyGreenCash
	MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * ____moneyGreenCash_38;
	// UnityEngine.UI.Text Main::_txtDiamond
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDiamond_39;
	// UnityEngine.UI.Text Main::_txtDiamond_Shadow
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDiamond_Shadow_40;
	// UnityEngine.GameObject Main::_panelRaiseDetail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelRaiseDetail_41;
	// UnityEngine.UI.Text Main::_txtRaiseDetail
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtRaiseDetail_42;
	// SceneUiButton Main::_btnBuy
	SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507 * ____btnBuy_43;
	// UnityEngine.GameObject Main::_BigMap
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____BigMap_44;
	// UnityEngine.GameObject Main::_panelPrestige
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelPrestige_45;
	// UIPrestige Main::_Prestige
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C * ____Prestige_46;
	// UnityEngine.TextMesh Main::_txtNumOfRunningPlane
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ____txtNumOfRunningPlane_47;
	// UnityEngine.UI.Text Main::_txtDPS
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDPS_48;
	// UnityEngine.GameObject Main::_goRecycleBox
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goRecycleBox_49;
	// System.Single Main::DISTANCE_UNIT
	float ___DISTANCE_UNIT_50;
	// System.Single Main::TURN_RADIUS
	float ___TURN_RADIUS_51;
	// UnityEngine.Vector2 Main::START_POS
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___START_POS_52;
	// System.Single Main::LENGTH_OF_TURN
	float ___LENGTH_OF_TURN_53;
	// System.Single Main::PERIMETER
	float ___PERIMETER_54;
	// System.Single Main::MERGE_OFFSET
	float ___MERGE_OFFSET_55;
	// System.Single Main::MERGE_TIME
	float ___MERGE_TIME_56;
	// System.Single Main::MERGE_WAIT_TIME
	float ___MERGE_WAIT_TIME_57;
	// UnityEngine.GameObject Main::_containerRunningPlanes
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerRunningPlanes_58;
	// System.Collections.Generic.List`1<Plane> Main::m_lstRunningPlanes
	List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * ___m_lstRunningPlanes_59;
	// System.Single[] Main::m_aryPosThreshold
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryPosThreshold_60;
	// UnityEngine.Vector2[] Main::m_aryStartPosOfEachSegment
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_aryStartPosOfEachSegment_61;
	// UnityEngine.Vector2[] Main::m_aryCircleCenterOfTurn
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_aryCircleCenterOfTurn_62;
	// System.Single Main::m_fMergeMoveSpeed
	float ___m_fMergeMoveSpeed_63;
	// Lot[] Main::m_aryLots
	LotU5BU5D_tDF39035F8B8E0E8A8915E650FAC90FE38BFD335F* ___m_aryLots_67;
	// UnityEngine.GameObject Main::m_aryLotsPositionPointsContainers
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_aryLotsPositionPointsContainers_68;
	// StartBelt Main::m_StartBelt
	StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD * ___m_StartBelt_69;
	// System.Single[] Main::m_aryRoundTimeByLevel
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryRoundTimeByLevel_70;
	// System.Collections.Generic.List`1<Plane> Main::m_lstAllPlanes
	List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * ___m_lstAllPlanes_71;
	// UnityEngine.Vector3 Main::m_vecStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecStartPos_72;
	// UnityEngine.Vector3[] Main::m_vecNodeStartPos
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_vecNodeStartPos_73;
	// UnityEngine.Vector3[] Main::m_vecNodeCenterPos
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_vecNodeCenterPos_74;
	// UnityEngine.Vector3[] Main::m_vecNodeEndPos
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_vecNodeEndPos_75;
	// System.Single Main::m_fRadius
	float ___m_fRadius_76;
	// System.Single Main::m_fVertLineLen
	float ___m_fVertLineLen_77;
	// System.Single Main::m_fHoriLineLen
	float ___m_fHoriLineLen_78;
	// System.Single Main::m_fRoundLen
	float ___m_fRoundLen_79;
	// System.Single Main::m_fPosAdjustDistance
	float ___m_fPosAdjustDistance_80;
	// System.Single Main::m_f10SecLoopElapse
	float ___m_f10SecLoopElapse_82;
	// System.Boolean Main::m_bIsVisible
	bool ___m_bIsVisible_83;
	// System.Collections.Generic.List`1<Plane> Main::lstTempPLanes
	List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * ___lstTempPLanes_84;
	// System.Single Main::m_fAccelerate
	float ___m_fAccelerate_85;
	// System.Single Main::m_fPreAccerlateTimeLeft
	float ___m_fPreAccerlateTimeLeft_86;
	// System.Single Main::m_fSpeedAccelerate
	float ___m_fSpeedAccelerate_87;
	// System.Single[] Main::m_aryAccelerate
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryAccelerate_89;
	// System.Boolean Main::m_bLostFocus
	bool ___m_bLostFocus_90;
	// System.Object Main::_panelAds
	RuntimeObject * ____panelAds_91;
	// System.Double Main::m_fOfflineProfit
	double ___m_fOfflineProfit_92;
	// UnityEngine.Vector3 Main::vecTempPosLeft
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPosLeft_93;
	// UnityEngine.Vector3 Main::vecTempPosRight
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPosRight_94;
	// System.Collections.Generic.List`1<Main/sAutoRunLocation> Main::m_lstRunLocations
	List_1_t1517B4D31CFE0D5A31B279B7CC226F6BA9FA9E43 * ___m_lstRunLocations_95;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>> Main::m_dicLotPosOfLevel
	Dictionary_2_t27BCF11A3E40FEB6B728C0EA39A63FAB1AE16D5C * ___m_dicLotPosOfLevel_96;
	// Main/sAutoRunLocation Main::selected_one
	sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949  ___selected_one_97;
	// System.Double Main::m_dShowingNumber
	double ___m_dShowingNumber_98;
	// System.Double Main::m_dNumberChangeSpeed
	double ___m_dNumberChangeSpeed_99;
	// System.Single Main::m_fCoinChangeTime
	float ___m_fCoinChangeTime_100;
	// System.Double Main::m_dDestCoinNumber
	double ___m_dDestCoinNumber_101;

public:
	inline static int32_t get_offset_of_m_goContainerEffects_4() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_goContainerEffects_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goContainerEffects_4() const { return ___m_goContainerEffects_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goContainerEffects_4() { return &___m_goContainerEffects_4; }
	inline void set_m_goContainerEffects_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goContainerEffects_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_goContainerEffects_4), value);
	}

	inline static int32_t get_offset_of__goStartPos_5() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____goStartPos_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goStartPos_5() const { return ____goStartPos_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goStartPos_5() { return &____goStartPos_5; }
	inline void set__goStartPos_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goStartPos_5 = value;
		Il2CppCodeGenWriteBarrier((&____goStartPos_5), value);
	}

	inline static int32_t get_offset_of_m_aryNodeStart_6() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryNodeStart_6)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryNodeStart_6() const { return ___m_aryNodeStart_6; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryNodeStart_6() { return &___m_aryNodeStart_6; }
	inline void set_m_aryNodeStart_6(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryNodeStart_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryNodeStart_6), value);
	}

	inline static int32_t get_offset_of_m_aryNodeCenter_7() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryNodeCenter_7)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryNodeCenter_7() const { return ___m_aryNodeCenter_7; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryNodeCenter_7() { return &___m_aryNodeCenter_7; }
	inline void set_m_aryNodeCenter_7(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryNodeCenter_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryNodeCenter_7), value);
	}

	inline static int32_t get_offset_of_m_aryNodeEnd_8() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryNodeEnd_8)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryNodeEnd_8() const { return ___m_aryNodeEnd_8; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryNodeEnd_8() { return &___m_aryNodeEnd_8; }
	inline void set_m_aryNodeEnd_8(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryNodeEnd_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryNodeEnd_8), value);
	}

	inline static int32_t get_offset_of_m_dicLocationPoints_9() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_dicLocationPoints_9)); }
	inline Dictionary_2_t8BBCAEE5B6551D2028E4CE6BBACCA1D12D8070C0 * get_m_dicLocationPoints_9() const { return ___m_dicLocationPoints_9; }
	inline Dictionary_2_t8BBCAEE5B6551D2028E4CE6BBACCA1D12D8070C0 ** get_address_of_m_dicLocationPoints_9() { return &___m_dicLocationPoints_9; }
	inline void set_m_dicLocationPoints_9(Dictionary_2_t8BBCAEE5B6551D2028E4CE6BBACCA1D12D8070C0 * value)
	{
		___m_dicLocationPoints_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLocationPoints_9), value);
	}

	inline static int32_t get_offset_of_m_aryStartLocationPointOfEachSeg_10() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryStartLocationPointOfEachSeg_10)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryStartLocationPointOfEachSeg_10() const { return ___m_aryStartLocationPointOfEachSeg_10; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryStartLocationPointOfEachSeg_10() { return &___m_aryStartLocationPointOfEachSeg_10; }
	inline void set_m_aryStartLocationPointOfEachSeg_10(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryStartLocationPointOfEachSeg_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryStartLocationPointOfEachSeg_10), value);
	}

	inline static int32_t get_offset_of_m_aryStartLocationInfoPointOfEachSeg_11() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryStartLocationInfoPointOfEachSeg_11)); }
	inline sAutoRunLocationU5BU5D_t1B4B4B1E8F7FDC3043F544A45744E0D6A98B8864* get_m_aryStartLocationInfoPointOfEachSeg_11() const { return ___m_aryStartLocationInfoPointOfEachSeg_11; }
	inline sAutoRunLocationU5BU5D_t1B4B4B1E8F7FDC3043F544A45744E0D6A98B8864** get_address_of_m_aryStartLocationInfoPointOfEachSeg_11() { return &___m_aryStartLocationInfoPointOfEachSeg_11; }
	inline void set_m_aryStartLocationInfoPointOfEachSeg_11(sAutoRunLocationU5BU5D_t1B4B4B1E8F7FDC3043F544A45744E0D6A98B8864* value)
	{
		___m_aryStartLocationInfoPointOfEachSeg_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryStartLocationInfoPointOfEachSeg_11), value);
	}

	inline static int32_t get_offset_of_m_containerLocations_12() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_containerLocations_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerLocations_12() const { return ___m_containerLocations_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerLocations_12() { return &___m_containerLocations_12; }
	inline void set_m_containerLocations_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerLocations_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerLocations_12), value);
	}

	inline static int32_t get_offset_of__txtDebugInfo_13() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtDebugInfo_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDebugInfo_13() const { return ____txtDebugInfo_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDebugInfo_13() { return &____txtDebugInfo_13; }
	inline void set__txtDebugInfo_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDebugInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&____txtDebugInfo_13), value);
	}

	inline static int32_t get_offset_of__txtOfflineDetailDebug_14() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtOfflineDetailDebug_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtOfflineDetailDebug_14() const { return ____txtOfflineDetailDebug_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtOfflineDetailDebug_14() { return &____txtOfflineDetailDebug_14; }
	inline void set__txtOfflineDetailDebug_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtOfflineDetailDebug_14 = value;
		Il2CppCodeGenWriteBarrier((&____txtOfflineDetailDebug_14), value);
	}

	inline static int32_t get_offset_of__moneyTrigger_15() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____moneyTrigger_15)); }
	inline MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464 * get__moneyTrigger_15() const { return ____moneyTrigger_15; }
	inline MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464 ** get_address_of__moneyTrigger_15() { return &____moneyTrigger_15; }
	inline void set__moneyTrigger_15(MoneyTrigger_t3E0BC259B1FB7AAF8B6EB02CB4A680B5C4198464 * value)
	{
		____moneyTrigger_15 = value;
		Il2CppCodeGenWriteBarrier((&____moneyTrigger_15), value);
	}

	inline static int32_t get_offset_of_m_aryMoneyTrigger_16() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryMoneyTrigger_16)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryMoneyTrigger_16() const { return ___m_aryMoneyTrigger_16; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryMoneyTrigger_16() { return &___m_aryMoneyTrigger_16; }
	inline void set_m_aryMoneyTrigger_16(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryMoneyTrigger_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMoneyTrigger_16), value);
	}

	inline static int32_t get_offset_of__basescaleCPosCoinIcon_17() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____basescaleCPosCoinIcon_17)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__basescaleCPosCoinIcon_17() const { return ____basescaleCPosCoinIcon_17; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__basescaleCPosCoinIcon_17() { return &____basescaleCPosCoinIcon_17; }
	inline void set__basescaleCPosCoinIcon_17(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____basescaleCPosCoinIcon_17 = value;
		Il2CppCodeGenWriteBarrier((&____basescaleCPosCoinIcon_17), value);
	}

	inline static int32_t get_offset_of_m_vecLocalPosOnLot_22() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_vecLocalPosOnLot_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecLocalPosOnLot_22() const { return ___m_vecLocalPosOnLot_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecLocalPosOnLot_22() { return &___m_vecLocalPosOnLot_22; }
	inline void set_m_vecLocalPosOnLot_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecLocalPosOnLot_22 = value;
	}

	inline static int32_t get_offset_of_m_nCurRaise_23() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_nCurRaise_23)); }
	inline float get_m_nCurRaise_23() const { return ___m_nCurRaise_23; }
	inline float* get_address_of_m_nCurRaise_23() { return &___m_nCurRaise_23; }
	inline void set_m_nCurRaise_23(float value)
	{
		___m_nCurRaise_23 = value;
	}

	inline static int32_t get_offset_of_m_aryCoin0_24() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryCoin0_24)); }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* get_m_aryCoin0_24() const { return ___m_aryCoin0_24; }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A** get_address_of_m_aryCoin0_24() { return &___m_aryCoin0_24; }
	inline void set_m_aryCoin0_24(MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* value)
	{
		___m_aryCoin0_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin0_24), value);
	}

	inline static int32_t get_offset_of_m_aryCoin1_25() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryCoin1_25)); }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* get_m_aryCoin1_25() const { return ___m_aryCoin1_25; }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A** get_address_of_m_aryCoin1_25() { return &___m_aryCoin1_25; }
	inline void set_m_aryCoin1_25(MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* value)
	{
		___m_aryCoin1_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin1_25), value);
	}

	inline static int32_t get_offset_of_m_aryCoin2_26() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryCoin2_26)); }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* get_m_aryCoin2_26() const { return ___m_aryCoin2_26; }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A** get_address_of_m_aryCoin2_26() { return &___m_aryCoin2_26; }
	inline void set_m_aryCoin2_26(MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* value)
	{
		___m_aryCoin2_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin2_26), value);
	}

	inline static int32_t get_offset_of__txtPrestigeTimes_27() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtPrestigeTimes_27)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrestigeTimes_27() const { return ____txtPrestigeTimes_27; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrestigeTimes_27() { return &____txtPrestigeTimes_27; }
	inline void set__txtPrestigeTimes_27(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrestigeTimes_27 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrestigeTimes_27), value);
	}

	inline static int32_t get_offset_of__txtPrestigeTimes_Shadow_28() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtPrestigeTimes_Shadow_28)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrestigeTimes_Shadow_28() const { return ____txtPrestigeTimes_Shadow_28; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrestigeTimes_Shadow_28() { return &____txtPrestigeTimes_Shadow_28; }
	inline void set__txtPrestigeTimes_Shadow_28(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrestigeTimes_Shadow_28 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrestigeTimes_Shadow_28), value);
	}

	inline static int32_t get_offset_of__starsPrestigeTimes_29() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____starsPrestigeTimes_29)); }
	inline UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * get__starsPrestigeTimes_29() const { return ____starsPrestigeTimes_29; }
	inline UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B ** get_address_of__starsPrestigeTimes_29() { return &____starsPrestigeTimes_29; }
	inline void set__starsPrestigeTimes_29(UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * value)
	{
		____starsPrestigeTimes_29 = value;
		Il2CppCodeGenWriteBarrier((&____starsPrestigeTimes_29), value);
	}

	inline static int32_t get_offset_of__txtTotalCoinOfThisPlanet_30() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtTotalCoinOfThisPlanet_30)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTotalCoinOfThisPlanet_30() const { return ____txtTotalCoinOfThisPlanet_30; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTotalCoinOfThisPlanet_30() { return &____txtTotalCoinOfThisPlanet_30; }
	inline void set__txtTotalCoinOfThisPlanet_30(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTotalCoinOfThisPlanet_30 = value;
		Il2CppCodeGenWriteBarrier((&____txtTotalCoinOfThisPlanet_30), value);
	}

	inline static int32_t get_offset_of__tmTotalCoinOfThisPlanet_31() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____tmTotalCoinOfThisPlanet_31)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get__tmTotalCoinOfThisPlanet_31() const { return ____tmTotalCoinOfThisPlanet_31; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of__tmTotalCoinOfThisPlanet_31() { return &____tmTotalCoinOfThisPlanet_31; }
	inline void set__tmTotalCoinOfThisPlanet_31(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		____tmTotalCoinOfThisPlanet_31 = value;
		Il2CppCodeGenWriteBarrier((&____tmTotalCoinOfThisPlanet_31), value);
	}

	inline static int32_t get_offset_of__panelCollectOfflineProfit_32() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____panelCollectOfflineProfit_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelCollectOfflineProfit_32() const { return ____panelCollectOfflineProfit_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelCollectOfflineProfit_32() { return &____panelCollectOfflineProfit_32; }
	inline void set__panelCollectOfflineProfit_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelCollectOfflineProfit_32 = value;
		Il2CppCodeGenWriteBarrier((&____panelCollectOfflineProfit_32), value);
	}

	inline static int32_t get_offset_of__txtOfflineProfitOfThisDistrict_33() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtOfflineProfitOfThisDistrict_33)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtOfflineProfitOfThisDistrict_33() const { return ____txtOfflineProfitOfThisDistrict_33; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtOfflineProfitOfThisDistrict_33() { return &____txtOfflineProfitOfThisDistrict_33; }
	inline void set__txtOfflineProfitOfThisDistrict_33(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtOfflineProfitOfThisDistrict_33 = value;
		Il2CppCodeGenWriteBarrier((&____txtOfflineProfitOfThisDistrict_33), value);
	}

	inline static int32_t get_offset_of__btnOpenBigMap_34() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____btnOpenBigMap_34)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnOpenBigMap_34() const { return ____btnOpenBigMap_34; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnOpenBigMap_34() { return &____btnOpenBigMap_34; }
	inline void set__btnOpenBigMap_34(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnOpenBigMap_34 = value;
		Il2CppCodeGenWriteBarrier((&____btnOpenBigMap_34), value);
	}

	inline static int32_t get_offset_of__btnCloseBigMap_35() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____btnCloseBigMap_35)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnCloseBigMap_35() const { return ____btnCloseBigMap_35; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnCloseBigMap_35() { return &____btnCloseBigMap_35; }
	inline void set__btnCloseBigMap_35(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnCloseBigMap_35 = value;
		Il2CppCodeGenWriteBarrier((&____btnCloseBigMap_35), value);
	}

	inline static int32_t get_offset_of__moneyCoin_36() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____moneyCoin_36)); }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * get__moneyCoin_36() const { return ____moneyCoin_36; }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF ** get_address_of__moneyCoin_36() { return &____moneyCoin_36; }
	inline void set__moneyCoin_36(MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * value)
	{
		____moneyCoin_36 = value;
		Il2CppCodeGenWriteBarrier((&____moneyCoin_36), value);
	}

	inline static int32_t get_offset_of_m_aryActivePlanetCoin_37() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryActivePlanetCoin_37)); }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* get_m_aryActivePlanetCoin_37() const { return ___m_aryActivePlanetCoin_37; }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A** get_address_of_m_aryActivePlanetCoin_37() { return &___m_aryActivePlanetCoin_37; }
	inline void set_m_aryActivePlanetCoin_37(MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* value)
	{
		___m_aryActivePlanetCoin_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryActivePlanetCoin_37), value);
	}

	inline static int32_t get_offset_of__moneyGreenCash_38() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____moneyGreenCash_38)); }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * get__moneyGreenCash_38() const { return ____moneyGreenCash_38; }
	inline MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF ** get_address_of__moneyGreenCash_38() { return &____moneyGreenCash_38; }
	inline void set__moneyGreenCash_38(MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF * value)
	{
		____moneyGreenCash_38 = value;
		Il2CppCodeGenWriteBarrier((&____moneyGreenCash_38), value);
	}

	inline static int32_t get_offset_of__txtDiamond_39() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtDiamond_39)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDiamond_39() const { return ____txtDiamond_39; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDiamond_39() { return &____txtDiamond_39; }
	inline void set__txtDiamond_39(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDiamond_39 = value;
		Il2CppCodeGenWriteBarrier((&____txtDiamond_39), value);
	}

	inline static int32_t get_offset_of__txtDiamond_Shadow_40() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtDiamond_Shadow_40)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDiamond_Shadow_40() const { return ____txtDiamond_Shadow_40; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDiamond_Shadow_40() { return &____txtDiamond_Shadow_40; }
	inline void set__txtDiamond_Shadow_40(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDiamond_Shadow_40 = value;
		Il2CppCodeGenWriteBarrier((&____txtDiamond_Shadow_40), value);
	}

	inline static int32_t get_offset_of__panelRaiseDetail_41() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____panelRaiseDetail_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelRaiseDetail_41() const { return ____panelRaiseDetail_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelRaiseDetail_41() { return &____panelRaiseDetail_41; }
	inline void set__panelRaiseDetail_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelRaiseDetail_41 = value;
		Il2CppCodeGenWriteBarrier((&____panelRaiseDetail_41), value);
	}

	inline static int32_t get_offset_of__txtRaiseDetail_42() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtRaiseDetail_42)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtRaiseDetail_42() const { return ____txtRaiseDetail_42; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtRaiseDetail_42() { return &____txtRaiseDetail_42; }
	inline void set__txtRaiseDetail_42(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtRaiseDetail_42 = value;
		Il2CppCodeGenWriteBarrier((&____txtRaiseDetail_42), value);
	}

	inline static int32_t get_offset_of__btnBuy_43() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____btnBuy_43)); }
	inline SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507 * get__btnBuy_43() const { return ____btnBuy_43; }
	inline SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507 ** get_address_of__btnBuy_43() { return &____btnBuy_43; }
	inline void set__btnBuy_43(SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507 * value)
	{
		____btnBuy_43 = value;
		Il2CppCodeGenWriteBarrier((&____btnBuy_43), value);
	}

	inline static int32_t get_offset_of__BigMap_44() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____BigMap_44)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__BigMap_44() const { return ____BigMap_44; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__BigMap_44() { return &____BigMap_44; }
	inline void set__BigMap_44(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____BigMap_44 = value;
		Il2CppCodeGenWriteBarrier((&____BigMap_44), value);
	}

	inline static int32_t get_offset_of__panelPrestige_45() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____panelPrestige_45)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelPrestige_45() const { return ____panelPrestige_45; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelPrestige_45() { return &____panelPrestige_45; }
	inline void set__panelPrestige_45(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelPrestige_45 = value;
		Il2CppCodeGenWriteBarrier((&____panelPrestige_45), value);
	}

	inline static int32_t get_offset_of__Prestige_46() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____Prestige_46)); }
	inline UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C * get__Prestige_46() const { return ____Prestige_46; }
	inline UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C ** get_address_of__Prestige_46() { return &____Prestige_46; }
	inline void set__Prestige_46(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C * value)
	{
		____Prestige_46 = value;
		Il2CppCodeGenWriteBarrier((&____Prestige_46), value);
	}

	inline static int32_t get_offset_of__txtNumOfRunningPlane_47() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtNumOfRunningPlane_47)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get__txtNumOfRunningPlane_47() const { return ____txtNumOfRunningPlane_47; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of__txtNumOfRunningPlane_47() { return &____txtNumOfRunningPlane_47; }
	inline void set__txtNumOfRunningPlane_47(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		____txtNumOfRunningPlane_47 = value;
		Il2CppCodeGenWriteBarrier((&____txtNumOfRunningPlane_47), value);
	}

	inline static int32_t get_offset_of__txtDPS_48() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____txtDPS_48)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDPS_48() const { return ____txtDPS_48; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDPS_48() { return &____txtDPS_48; }
	inline void set__txtDPS_48(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDPS_48 = value;
		Il2CppCodeGenWriteBarrier((&____txtDPS_48), value);
	}

	inline static int32_t get_offset_of__goRecycleBox_49() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____goRecycleBox_49)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goRecycleBox_49() const { return ____goRecycleBox_49; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goRecycleBox_49() { return &____goRecycleBox_49; }
	inline void set__goRecycleBox_49(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goRecycleBox_49 = value;
		Il2CppCodeGenWriteBarrier((&____goRecycleBox_49), value);
	}

	inline static int32_t get_offset_of_DISTANCE_UNIT_50() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___DISTANCE_UNIT_50)); }
	inline float get_DISTANCE_UNIT_50() const { return ___DISTANCE_UNIT_50; }
	inline float* get_address_of_DISTANCE_UNIT_50() { return &___DISTANCE_UNIT_50; }
	inline void set_DISTANCE_UNIT_50(float value)
	{
		___DISTANCE_UNIT_50 = value;
	}

	inline static int32_t get_offset_of_TURN_RADIUS_51() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___TURN_RADIUS_51)); }
	inline float get_TURN_RADIUS_51() const { return ___TURN_RADIUS_51; }
	inline float* get_address_of_TURN_RADIUS_51() { return &___TURN_RADIUS_51; }
	inline void set_TURN_RADIUS_51(float value)
	{
		___TURN_RADIUS_51 = value;
	}

	inline static int32_t get_offset_of_START_POS_52() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___START_POS_52)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_START_POS_52() const { return ___START_POS_52; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_START_POS_52() { return &___START_POS_52; }
	inline void set_START_POS_52(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___START_POS_52 = value;
	}

	inline static int32_t get_offset_of_LENGTH_OF_TURN_53() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___LENGTH_OF_TURN_53)); }
	inline float get_LENGTH_OF_TURN_53() const { return ___LENGTH_OF_TURN_53; }
	inline float* get_address_of_LENGTH_OF_TURN_53() { return &___LENGTH_OF_TURN_53; }
	inline void set_LENGTH_OF_TURN_53(float value)
	{
		___LENGTH_OF_TURN_53 = value;
	}

	inline static int32_t get_offset_of_PERIMETER_54() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___PERIMETER_54)); }
	inline float get_PERIMETER_54() const { return ___PERIMETER_54; }
	inline float* get_address_of_PERIMETER_54() { return &___PERIMETER_54; }
	inline void set_PERIMETER_54(float value)
	{
		___PERIMETER_54 = value;
	}

	inline static int32_t get_offset_of_MERGE_OFFSET_55() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___MERGE_OFFSET_55)); }
	inline float get_MERGE_OFFSET_55() const { return ___MERGE_OFFSET_55; }
	inline float* get_address_of_MERGE_OFFSET_55() { return &___MERGE_OFFSET_55; }
	inline void set_MERGE_OFFSET_55(float value)
	{
		___MERGE_OFFSET_55 = value;
	}

	inline static int32_t get_offset_of_MERGE_TIME_56() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___MERGE_TIME_56)); }
	inline float get_MERGE_TIME_56() const { return ___MERGE_TIME_56; }
	inline float* get_address_of_MERGE_TIME_56() { return &___MERGE_TIME_56; }
	inline void set_MERGE_TIME_56(float value)
	{
		___MERGE_TIME_56 = value;
	}

	inline static int32_t get_offset_of_MERGE_WAIT_TIME_57() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___MERGE_WAIT_TIME_57)); }
	inline float get_MERGE_WAIT_TIME_57() const { return ___MERGE_WAIT_TIME_57; }
	inline float* get_address_of_MERGE_WAIT_TIME_57() { return &___MERGE_WAIT_TIME_57; }
	inline void set_MERGE_WAIT_TIME_57(float value)
	{
		___MERGE_WAIT_TIME_57 = value;
	}

	inline static int32_t get_offset_of__containerRunningPlanes_58() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____containerRunningPlanes_58)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerRunningPlanes_58() const { return ____containerRunningPlanes_58; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerRunningPlanes_58() { return &____containerRunningPlanes_58; }
	inline void set__containerRunningPlanes_58(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerRunningPlanes_58 = value;
		Il2CppCodeGenWriteBarrier((&____containerRunningPlanes_58), value);
	}

	inline static int32_t get_offset_of_m_lstRunningPlanes_59() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_lstRunningPlanes_59)); }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * get_m_lstRunningPlanes_59() const { return ___m_lstRunningPlanes_59; }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 ** get_address_of_m_lstRunningPlanes_59() { return &___m_lstRunningPlanes_59; }
	inline void set_m_lstRunningPlanes_59(List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * value)
	{
		___m_lstRunningPlanes_59 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRunningPlanes_59), value);
	}

	inline static int32_t get_offset_of_m_aryPosThreshold_60() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryPosThreshold_60)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryPosThreshold_60() const { return ___m_aryPosThreshold_60; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryPosThreshold_60() { return &___m_aryPosThreshold_60; }
	inline void set_m_aryPosThreshold_60(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryPosThreshold_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPosThreshold_60), value);
	}

	inline static int32_t get_offset_of_m_aryStartPosOfEachSegment_61() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryStartPosOfEachSegment_61)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_aryStartPosOfEachSegment_61() const { return ___m_aryStartPosOfEachSegment_61; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_aryStartPosOfEachSegment_61() { return &___m_aryStartPosOfEachSegment_61; }
	inline void set_m_aryStartPosOfEachSegment_61(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_aryStartPosOfEachSegment_61 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryStartPosOfEachSegment_61), value);
	}

	inline static int32_t get_offset_of_m_aryCircleCenterOfTurn_62() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryCircleCenterOfTurn_62)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_aryCircleCenterOfTurn_62() const { return ___m_aryCircleCenterOfTurn_62; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_aryCircleCenterOfTurn_62() { return &___m_aryCircleCenterOfTurn_62; }
	inline void set_m_aryCircleCenterOfTurn_62(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_aryCircleCenterOfTurn_62 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCircleCenterOfTurn_62), value);
	}

	inline static int32_t get_offset_of_m_fMergeMoveSpeed_63() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fMergeMoveSpeed_63)); }
	inline float get_m_fMergeMoveSpeed_63() const { return ___m_fMergeMoveSpeed_63; }
	inline float* get_address_of_m_fMergeMoveSpeed_63() { return &___m_fMergeMoveSpeed_63; }
	inline void set_m_fMergeMoveSpeed_63(float value)
	{
		___m_fMergeMoveSpeed_63 = value;
	}

	inline static int32_t get_offset_of_m_aryLots_67() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryLots_67)); }
	inline LotU5BU5D_tDF39035F8B8E0E8A8915E650FAC90FE38BFD335F* get_m_aryLots_67() const { return ___m_aryLots_67; }
	inline LotU5BU5D_tDF39035F8B8E0E8A8915E650FAC90FE38BFD335F** get_address_of_m_aryLots_67() { return &___m_aryLots_67; }
	inline void set_m_aryLots_67(LotU5BU5D_tDF39035F8B8E0E8A8915E650FAC90FE38BFD335F* value)
	{
		___m_aryLots_67 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryLots_67), value);
	}

	inline static int32_t get_offset_of_m_aryLotsPositionPointsContainers_68() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryLotsPositionPointsContainers_68)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_aryLotsPositionPointsContainers_68() const { return ___m_aryLotsPositionPointsContainers_68; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_aryLotsPositionPointsContainers_68() { return &___m_aryLotsPositionPointsContainers_68; }
	inline void set_m_aryLotsPositionPointsContainers_68(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_aryLotsPositionPointsContainers_68 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryLotsPositionPointsContainers_68), value);
	}

	inline static int32_t get_offset_of_m_StartBelt_69() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_StartBelt_69)); }
	inline StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD * get_m_StartBelt_69() const { return ___m_StartBelt_69; }
	inline StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD ** get_address_of_m_StartBelt_69() { return &___m_StartBelt_69; }
	inline void set_m_StartBelt_69(StartBelt_t8C3AF2F8CA09525EC43A3EEBDFD40824C49D61CD * value)
	{
		___m_StartBelt_69 = value;
		Il2CppCodeGenWriteBarrier((&___m_StartBelt_69), value);
	}

	inline static int32_t get_offset_of_m_aryRoundTimeByLevel_70() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryRoundTimeByLevel_70)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryRoundTimeByLevel_70() const { return ___m_aryRoundTimeByLevel_70; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryRoundTimeByLevel_70() { return &___m_aryRoundTimeByLevel_70; }
	inline void set_m_aryRoundTimeByLevel_70(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryRoundTimeByLevel_70 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryRoundTimeByLevel_70), value);
	}

	inline static int32_t get_offset_of_m_lstAllPlanes_71() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_lstAllPlanes_71)); }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * get_m_lstAllPlanes_71() const { return ___m_lstAllPlanes_71; }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 ** get_address_of_m_lstAllPlanes_71() { return &___m_lstAllPlanes_71; }
	inline void set_m_lstAllPlanes_71(List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * value)
	{
		___m_lstAllPlanes_71 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstAllPlanes_71), value);
	}

	inline static int32_t get_offset_of_m_vecStartPos_72() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_vecStartPos_72)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecStartPos_72() const { return ___m_vecStartPos_72; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecStartPos_72() { return &___m_vecStartPos_72; }
	inline void set_m_vecStartPos_72(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecStartPos_72 = value;
	}

	inline static int32_t get_offset_of_m_vecNodeStartPos_73() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_vecNodeStartPos_73)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_vecNodeStartPos_73() const { return ___m_vecNodeStartPos_73; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_vecNodeStartPos_73() { return &___m_vecNodeStartPos_73; }
	inline void set_m_vecNodeStartPos_73(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_vecNodeStartPos_73 = value;
		Il2CppCodeGenWriteBarrier((&___m_vecNodeStartPos_73), value);
	}

	inline static int32_t get_offset_of_m_vecNodeCenterPos_74() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_vecNodeCenterPos_74)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_vecNodeCenterPos_74() const { return ___m_vecNodeCenterPos_74; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_vecNodeCenterPos_74() { return &___m_vecNodeCenterPos_74; }
	inline void set_m_vecNodeCenterPos_74(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_vecNodeCenterPos_74 = value;
		Il2CppCodeGenWriteBarrier((&___m_vecNodeCenterPos_74), value);
	}

	inline static int32_t get_offset_of_m_vecNodeEndPos_75() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_vecNodeEndPos_75)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_vecNodeEndPos_75() const { return ___m_vecNodeEndPos_75; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_vecNodeEndPos_75() { return &___m_vecNodeEndPos_75; }
	inline void set_m_vecNodeEndPos_75(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_vecNodeEndPos_75 = value;
		Il2CppCodeGenWriteBarrier((&___m_vecNodeEndPos_75), value);
	}

	inline static int32_t get_offset_of_m_fRadius_76() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fRadius_76)); }
	inline float get_m_fRadius_76() const { return ___m_fRadius_76; }
	inline float* get_address_of_m_fRadius_76() { return &___m_fRadius_76; }
	inline void set_m_fRadius_76(float value)
	{
		___m_fRadius_76 = value;
	}

	inline static int32_t get_offset_of_m_fVertLineLen_77() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fVertLineLen_77)); }
	inline float get_m_fVertLineLen_77() const { return ___m_fVertLineLen_77; }
	inline float* get_address_of_m_fVertLineLen_77() { return &___m_fVertLineLen_77; }
	inline void set_m_fVertLineLen_77(float value)
	{
		___m_fVertLineLen_77 = value;
	}

	inline static int32_t get_offset_of_m_fHoriLineLen_78() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fHoriLineLen_78)); }
	inline float get_m_fHoriLineLen_78() const { return ___m_fHoriLineLen_78; }
	inline float* get_address_of_m_fHoriLineLen_78() { return &___m_fHoriLineLen_78; }
	inline void set_m_fHoriLineLen_78(float value)
	{
		___m_fHoriLineLen_78 = value;
	}

	inline static int32_t get_offset_of_m_fRoundLen_79() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fRoundLen_79)); }
	inline float get_m_fRoundLen_79() const { return ___m_fRoundLen_79; }
	inline float* get_address_of_m_fRoundLen_79() { return &___m_fRoundLen_79; }
	inline void set_m_fRoundLen_79(float value)
	{
		___m_fRoundLen_79 = value;
	}

	inline static int32_t get_offset_of_m_fPosAdjustDistance_80() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fPosAdjustDistance_80)); }
	inline float get_m_fPosAdjustDistance_80() const { return ___m_fPosAdjustDistance_80; }
	inline float* get_address_of_m_fPosAdjustDistance_80() { return &___m_fPosAdjustDistance_80; }
	inline void set_m_fPosAdjustDistance_80(float value)
	{
		___m_fPosAdjustDistance_80 = value;
	}

	inline static int32_t get_offset_of_m_f10SecLoopElapse_82() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_f10SecLoopElapse_82)); }
	inline float get_m_f10SecLoopElapse_82() const { return ___m_f10SecLoopElapse_82; }
	inline float* get_address_of_m_f10SecLoopElapse_82() { return &___m_f10SecLoopElapse_82; }
	inline void set_m_f10SecLoopElapse_82(float value)
	{
		___m_f10SecLoopElapse_82 = value;
	}

	inline static int32_t get_offset_of_m_bIsVisible_83() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_bIsVisible_83)); }
	inline bool get_m_bIsVisible_83() const { return ___m_bIsVisible_83; }
	inline bool* get_address_of_m_bIsVisible_83() { return &___m_bIsVisible_83; }
	inline void set_m_bIsVisible_83(bool value)
	{
		___m_bIsVisible_83 = value;
	}

	inline static int32_t get_offset_of_lstTempPLanes_84() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___lstTempPLanes_84)); }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * get_lstTempPLanes_84() const { return ___lstTempPLanes_84; }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 ** get_address_of_lstTempPLanes_84() { return &___lstTempPLanes_84; }
	inline void set_lstTempPLanes_84(List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * value)
	{
		___lstTempPLanes_84 = value;
		Il2CppCodeGenWriteBarrier((&___lstTempPLanes_84), value);
	}

	inline static int32_t get_offset_of_m_fAccelerate_85() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fAccelerate_85)); }
	inline float get_m_fAccelerate_85() const { return ___m_fAccelerate_85; }
	inline float* get_address_of_m_fAccelerate_85() { return &___m_fAccelerate_85; }
	inline void set_m_fAccelerate_85(float value)
	{
		___m_fAccelerate_85 = value;
	}

	inline static int32_t get_offset_of_m_fPreAccerlateTimeLeft_86() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fPreAccerlateTimeLeft_86)); }
	inline float get_m_fPreAccerlateTimeLeft_86() const { return ___m_fPreAccerlateTimeLeft_86; }
	inline float* get_address_of_m_fPreAccerlateTimeLeft_86() { return &___m_fPreAccerlateTimeLeft_86; }
	inline void set_m_fPreAccerlateTimeLeft_86(float value)
	{
		___m_fPreAccerlateTimeLeft_86 = value;
	}

	inline static int32_t get_offset_of_m_fSpeedAccelerate_87() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fSpeedAccelerate_87)); }
	inline float get_m_fSpeedAccelerate_87() const { return ___m_fSpeedAccelerate_87; }
	inline float* get_address_of_m_fSpeedAccelerate_87() { return &___m_fSpeedAccelerate_87; }
	inline void set_m_fSpeedAccelerate_87(float value)
	{
		___m_fSpeedAccelerate_87 = value;
	}

	inline static int32_t get_offset_of_m_aryAccelerate_89() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_aryAccelerate_89)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryAccelerate_89() const { return ___m_aryAccelerate_89; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryAccelerate_89() { return &___m_aryAccelerate_89; }
	inline void set_m_aryAccelerate_89(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryAccelerate_89 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAccelerate_89), value);
	}

	inline static int32_t get_offset_of_m_bLostFocus_90() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_bLostFocus_90)); }
	inline bool get_m_bLostFocus_90() const { return ___m_bLostFocus_90; }
	inline bool* get_address_of_m_bLostFocus_90() { return &___m_bLostFocus_90; }
	inline void set_m_bLostFocus_90(bool value)
	{
		___m_bLostFocus_90 = value;
	}

	inline static int32_t get_offset_of__panelAds_91() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ____panelAds_91)); }
	inline RuntimeObject * get__panelAds_91() const { return ____panelAds_91; }
	inline RuntimeObject ** get_address_of__panelAds_91() { return &____panelAds_91; }
	inline void set__panelAds_91(RuntimeObject * value)
	{
		____panelAds_91 = value;
		Il2CppCodeGenWriteBarrier((&____panelAds_91), value);
	}

	inline static int32_t get_offset_of_m_fOfflineProfit_92() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fOfflineProfit_92)); }
	inline double get_m_fOfflineProfit_92() const { return ___m_fOfflineProfit_92; }
	inline double* get_address_of_m_fOfflineProfit_92() { return &___m_fOfflineProfit_92; }
	inline void set_m_fOfflineProfit_92(double value)
	{
		___m_fOfflineProfit_92 = value;
	}

	inline static int32_t get_offset_of_vecTempPosLeft_93() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___vecTempPosLeft_93)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPosLeft_93() const { return ___vecTempPosLeft_93; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPosLeft_93() { return &___vecTempPosLeft_93; }
	inline void set_vecTempPosLeft_93(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPosLeft_93 = value;
	}

	inline static int32_t get_offset_of_vecTempPosRight_94() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___vecTempPosRight_94)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPosRight_94() const { return ___vecTempPosRight_94; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPosRight_94() { return &___vecTempPosRight_94; }
	inline void set_vecTempPosRight_94(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPosRight_94 = value;
	}

	inline static int32_t get_offset_of_m_lstRunLocations_95() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_lstRunLocations_95)); }
	inline List_1_t1517B4D31CFE0D5A31B279B7CC226F6BA9FA9E43 * get_m_lstRunLocations_95() const { return ___m_lstRunLocations_95; }
	inline List_1_t1517B4D31CFE0D5A31B279B7CC226F6BA9FA9E43 ** get_address_of_m_lstRunLocations_95() { return &___m_lstRunLocations_95; }
	inline void set_m_lstRunLocations_95(List_1_t1517B4D31CFE0D5A31B279B7CC226F6BA9FA9E43 * value)
	{
		___m_lstRunLocations_95 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRunLocations_95), value);
	}

	inline static int32_t get_offset_of_m_dicLotPosOfLevel_96() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_dicLotPosOfLevel_96)); }
	inline Dictionary_2_t27BCF11A3E40FEB6B728C0EA39A63FAB1AE16D5C * get_m_dicLotPosOfLevel_96() const { return ___m_dicLotPosOfLevel_96; }
	inline Dictionary_2_t27BCF11A3E40FEB6B728C0EA39A63FAB1AE16D5C ** get_address_of_m_dicLotPosOfLevel_96() { return &___m_dicLotPosOfLevel_96; }
	inline void set_m_dicLotPosOfLevel_96(Dictionary_2_t27BCF11A3E40FEB6B728C0EA39A63FAB1AE16D5C * value)
	{
		___m_dicLotPosOfLevel_96 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicLotPosOfLevel_96), value);
	}

	inline static int32_t get_offset_of_selected_one_97() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___selected_one_97)); }
	inline sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949  get_selected_one_97() const { return ___selected_one_97; }
	inline sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949 * get_address_of_selected_one_97() { return &___selected_one_97; }
	inline void set_selected_one_97(sAutoRunLocation_t1B6EE8072FCC8B63AD96FF14493824FE7F1AC949  value)
	{
		___selected_one_97 = value;
	}

	inline static int32_t get_offset_of_m_dShowingNumber_98() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_dShowingNumber_98)); }
	inline double get_m_dShowingNumber_98() const { return ___m_dShowingNumber_98; }
	inline double* get_address_of_m_dShowingNumber_98() { return &___m_dShowingNumber_98; }
	inline void set_m_dShowingNumber_98(double value)
	{
		___m_dShowingNumber_98 = value;
	}

	inline static int32_t get_offset_of_m_dNumberChangeSpeed_99() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_dNumberChangeSpeed_99)); }
	inline double get_m_dNumberChangeSpeed_99() const { return ___m_dNumberChangeSpeed_99; }
	inline double* get_address_of_m_dNumberChangeSpeed_99() { return &___m_dNumberChangeSpeed_99; }
	inline void set_m_dNumberChangeSpeed_99(double value)
	{
		___m_dNumberChangeSpeed_99 = value;
	}

	inline static int32_t get_offset_of_m_fCoinChangeTime_100() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_fCoinChangeTime_100)); }
	inline float get_m_fCoinChangeTime_100() const { return ___m_fCoinChangeTime_100; }
	inline float* get_address_of_m_fCoinChangeTime_100() { return &___m_fCoinChangeTime_100; }
	inline void set_m_fCoinChangeTime_100(float value)
	{
		___m_fCoinChangeTime_100 = value;
	}

	inline static int32_t get_offset_of_m_dDestCoinNumber_101() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1, ___m_dDestCoinNumber_101)); }
	inline double get_m_dDestCoinNumber_101() const { return ___m_dDestCoinNumber_101; }
	inline double* get_address_of_m_dDestCoinNumber_101() { return &___m_dDestCoinNumber_101; }
	inline void set_m_dDestCoinNumber_101(double value)
	{
		___m_dDestCoinNumber_101 = value;
	}
};

struct Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields
{
public:
	// UnityEngine.Vector3 Main::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_18;
	// UnityEngine.Vector3 Main::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_19;
	// UnityEngine.Color Main::colorTemp
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___colorTemp_20;
	// Main Main::s_Instance
	Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1 * ___s_Instance_21;

public:
	inline static int32_t get_offset_of_vecTempPos_18() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields, ___vecTempPos_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_18() const { return ___vecTempPos_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_18() { return &___vecTempPos_18; }
	inline void set_vecTempPos_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_18 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_19() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields, ___vecTempScale_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_19() const { return ___vecTempScale_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_19() { return &___vecTempScale_19; }
	inline void set_vecTempScale_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_19 = value;
	}

	inline static int32_t get_offset_of_colorTemp_20() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields, ___colorTemp_20)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_colorTemp_20() const { return ___colorTemp_20; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_colorTemp_20() { return &___colorTemp_20; }
	inline void set_colorTemp_20(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___colorTemp_20 = value;
	}

	inline static int32_t get_offset_of_s_Instance_21() { return static_cast<int32_t>(offsetof(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields, ___s_Instance_21)); }
	inline Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1 * get_s_Instance_21() const { return ___s_Instance_21; }
	inline Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1 ** get_address_of_s_Instance_21() { return &___s_Instance_21; }
	inline void set_s_Instance_21(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1 * value)
	{
		___s_Instance_21 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAIN_T7BD87A0CB813F834A2F93006206A3CE79188D0C1_H
#ifndef MAPMANAGER_T76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_H
#define MAPMANAGER_T76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapManager
struct  MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image MapManager::_imgCPosCoinIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCPosCoinIcon_4;
	// UnityEngine.UI.Text MapManager::_txtCurPlanetName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurPlanetName_5;
	// UnityEngine.GameObject MapManager::_skeletonEffectCurTrack_Container
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____skeletonEffectCurTrack_Container_6;
	// Spine.Unity.SkeletonGraphic MapManager::_skeletonEffectCurTrack
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * ____skeletonEffectCurTrack_7;
	// UnityEngine.Color MapManager::m_colorPlanetUnlockBlack
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorPlanetUnlockBlack_8;
	// UnityEngine.Color MapManager::m_colorBlackWhite
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorBlackWhite_9;
	// UnityEngine.Color MapManager::m_colorOutlineNormal
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorOutlineNormal_10;
	// UnityEngine.Color MapManager::m_colorOutlineCurTrack
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorOutlineCurTrack_11;
	// UnityEngine.Material MapManager::m_matBlackWhite
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_matBlackWhite_12;
	// UnityEngine.Sprite MapManager::m_sprBlockBgCurTrack
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBlockBgCurTrack_13;
	// UnityEngine.Sprite MapManager::m_sprBlockBgNormal
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBlockBgNormal_14;
	// UnityEngine.Sprite MapManager::m_sprBlockBgCanNotUnlock
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBlockBgCanNotUnlock_15;
	// System.Single MapManager::m_fSlideTime
	float ___m_fSlideTime_16;
	// System.Single[] MapManager::m_aryTheMainlandCenters
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryTheMainlandCenters_17;
	// System.String[] MapManager::m_aryDistrictName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_aryDistrictName_21;
	// UnityEngine.Sprite MapManager::m_sprBg_Locked_Left
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBg_Locked_Left_22;
	// UnityEngine.Sprite MapManager::m_sprBg_Locked_Right
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBg_Locked_Right_23;
	// UnityEngine.Sprite MapManager::m_sprBgUnlocked_Left
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBgUnlocked_Left_24;
	// UnityEngine.Sprite MapManager::m_sprBgUnlocked_Right
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprBgUnlocked_Right_25;
	// MoneyCounter[] MapManager::m_aryMoneyCounters
	MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* ___m_aryMoneyCounters_26;
	// UnityEngine.UI.Image[] MapManager::m_aryVariousCoinIcons
	ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* ___m_aryVariousCoinIcons_27;
	// UnityEngine.UI.Button MapManager::_btnAdventure
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnAdventure_28;
	// UnityEngine.GameObject MapManager::_containerZengShouCounters
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerZengShouCounters_29;
	// UnityEngine.GameObject MapManager::_containerRecycledCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerRecycledCounter_30;
	// UnityEngine.GameObject MapManager::_panelZengShou
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelZengShou_31;
	// UnityEngine.GameObject MapManager::_panelPanelDetails
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelPanelDetails_32;
	// UnityEngine.UI.Text MapManager::_txtPanelDetailstitle
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPanelDetailstitle_33;
	// UIDistrict[] MapManager::m_aryUIDistricts
	UIDistrictU5BU5D_tDD148C70027D94D63D232B5C9FBB142D5EA721CB* ___m_aryUIDistricts_34;
	// UIPlanet[] MapManager::m_aryUIPlanets
	UIPlanetU5BU5D_t8587E1E90D0FB3095F0BBAA9DF7CE4A5A6A94298* ___m_aryUIPlanets_35;
	// UnityEngine.GameObject MapManager::_panelUnlockDistrict
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelUnlockDistrict_36;
	// UnityEngine.UI.Text MapManager::_txtUnlockDistrictCost
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtUnlockDistrictCost_37;
	// UnityEngine.UI.Image MapManager::_imgUnlockTrackPriceIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgUnlockTrackPriceIcon_38;
	// UnityEngine.GameObject MapManager::_panelUnlockPlanet
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelUnlockPlanet_39;
	// UnityEngine.UI.Text MapManager::_txtUnlockPlanetCost
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtUnlockPlanetCost_40;
	// UnityEngine.UI.Image MapManager::_imgUnlockPlanetPriceIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgUnlockPlanetPriceIcon_41;
	// UnityEngine.GameObject MapManager::_panelEntering
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelEntering_42;
	// UnityEngine.GameObject MapManager::_containerPlanets
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerPlanets_43;
	// UnityEngine.GameObject MapManager::_containerMainPlanets
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerMainPlanets_44;
	// UnityEngine.GameObject MapManager::_containerOtherPlanets
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerOtherPlanets_45;
	// UnityEngine.GameObject MapManager::_panelScienceTree
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelScienceTree_46;
	// UnityEngine.UI.Text MapManager::_txtCurPlanetAndTrack
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurPlanetAndTrack_47;
	// UnityEngine.UI.Text MapManager::_txtCurTrackLevel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurTrackLevel_48;
	// UnityEngine.GameObject[] MapManager::m_aryPlanetTitle
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryPlanetTitle_49;
	// UnityEngine.GameObject MapManager::_subpanelBatCollectOffline
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____subpanelBatCollectOffline_50;
	// UnityEngine.UI.Text MapManager::_txtBatOfflineGain
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtBatOfflineGain_51;
	// UnityEngine.UI.Text MapManager::_txtBatOfflineDetail
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtBatOfflineDetail_52;
	// UnityEngine.UI.Text MapManager::_txtBatOfflineTitle
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtBatOfflineTitle_53;
	// UnityEngine.GameObject MapManager::_goStarSky
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goStarSky_54;
	// System.Single MapManager::m_fStarSkyMoveSpeed
	float ___m_fStarSkyMoveSpeed_55;
	// Planet[] MapManager::m_aryPlanets
	PlanetU5BU5D_tFAD0DE71ED69B99F92FC1525DDAE53548F72893D* ___m_aryPlanets_56;
	// Planet MapManager::m_CurPlanet
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * ___m_CurPlanet_57;
	// District MapManager::m_CurDistrict
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * ___m_CurDistrict_58;
	// System.Int32 MapManager::m_nCurShowPlanetDetialIdOnUI
	int32_t ___m_nCurShowPlanetDetialIdOnUI_59;
	// System.Collections.Generic.Dictionary`2<System.String,UIZengShouCounter> MapManager::m_dicZengShouCounters
	Dictionary_2_tA42C789C20DBB051A4F863D661877067E6AE8081 * ___m_dicZengShouCounters_60;
	// UnityEngine.UI.Text[] MapManager::m_aryCoin0Value
	TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* ___m_aryCoin0Value_61;
	// UnityEngine.UI.Text[] MapManager::m_aryCoin1Value
	TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* ___m_aryCoin1Value_62;
	// UnityEngine.UI.Text[] MapManager::m_aryCoin2Value
	TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* ___m_aryCoin2Value_63;
	// UnityEngine.UI.Text[] MapManager::m_aryDiamondValue
	TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* ___m_aryDiamondValue_64;
	// UnityEngine.UI.Button MapManager::_btnCurEnterButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnCurEnterButton_65;
	// System.Single MapManager::m_fUpdatePlanetInfoLoopTimeElapse
	float ___m_fUpdatePlanetInfoLoopTimeElapse_66;
	// Planet MapManager::m_PlanetToPrestige
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * ___m_PlanetToPrestige_67;
	// District MapManager::m_DistrictToPrestige
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * ___m_DistrictToPrestige_68;
	// System.Boolean MapManager::m_bPlanetDetailPanelOpen
	bool ___m_bPlanetDetailPanelOpen_69;
	// District MapManager::m_districtToUnlock
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * ___m_districtToUnlock_70;
	// Planet MapManager::m_planetToUnlock
	Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * ___m_planetToUnlock_71;
	// System.Int32 MapManager::m_nToEnterRace_PlanetId
	int32_t ___m_nToEnterRace_PlanetId_72;
	// System.Int32 MapManager::m_nToEnterRace_DistrictId
	int32_t ___m_nToEnterRace_DistrictId_73;
	// System.Single MapManager::m_fPreEnterRaceLoopTime
	float ___m_fPreEnterRaceLoopTime_74;
	// System.Boolean MapManager::m_bStarSkyMoving
	bool ___m_bStarSkyMoving_75;
	// System.Boolean MapManager::m_bIsShowingZengShouPanel
	bool ___m_bIsShowingZengShouPanel_76;
	// System.Boolean MapManager::m_bTestShow
	bool ___m_bTestShow_77;
	// System.Boolean MapManager::m_bSliding
	bool ___m_bSliding_78;
	// System.Boolean MapManager::m_bBigMapShowing
	bool ___m_bBigMapShowing_79;
	// System.Single MapManager::m_fSlideSpeed
	float ___m_fSlideSpeed_80;
	// System.Single MapManager::m_fSlideA
	float ___m_fSlideA_81;
	// System.Single MapManager::m_fV0
	float ___m_fV0_82;
	// UnityEngine.GameObject MapManager::_goSVContent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goSVContent_83;
	// System.Single MapManager::m_fDestPos
	float ___m_fDestPos_84;
	// System.Single MapManager::m_fStartDragPos
	float ___m_fStartDragPos_85;
	// System.Single MapManager::m_fEndDragPos
	float ___m_fEndDragPos_86;
	// System.Boolean MapManager::m_bDragging
	bool ___m_bDragging_87;
	// System.Single MapManager::m_fMouseStartPos
	float ___m_fMouseStartPos_88;
	// System.Boolean MapManager::m_bClickProhibit
	bool ___m_bClickProhibit_89;
	// System.Single MapManager::m_fClickProhibitTimeLeft
	float ___m_fClickProhibitTimeLeft_90;
	// System.Int32 MapManager::m_nCurSelctedIndex
	int32_t ___m_nCurSelctedIndex_91;
	// UIMainLand[] MapManager::m_aryMainLands
	UIMainLandU5BU5D_tF159C8131B1BA4E2EEDF316C136F4BA6CE07B280* ___m_aryMainLands_92;

public:
	inline static int32_t get_offset_of__imgCPosCoinIcon_4() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____imgCPosCoinIcon_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCPosCoinIcon_4() const { return ____imgCPosCoinIcon_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCPosCoinIcon_4() { return &____imgCPosCoinIcon_4; }
	inline void set__imgCPosCoinIcon_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCPosCoinIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&____imgCPosCoinIcon_4), value);
	}

	inline static int32_t get_offset_of__txtCurPlanetName_5() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtCurPlanetName_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurPlanetName_5() const { return ____txtCurPlanetName_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurPlanetName_5() { return &____txtCurPlanetName_5; }
	inline void set__txtCurPlanetName_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurPlanetName_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurPlanetName_5), value);
	}

	inline static int32_t get_offset_of__skeletonEffectCurTrack_Container_6() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____skeletonEffectCurTrack_Container_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__skeletonEffectCurTrack_Container_6() const { return ____skeletonEffectCurTrack_Container_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__skeletonEffectCurTrack_Container_6() { return &____skeletonEffectCurTrack_Container_6; }
	inline void set__skeletonEffectCurTrack_Container_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____skeletonEffectCurTrack_Container_6 = value;
		Il2CppCodeGenWriteBarrier((&____skeletonEffectCurTrack_Container_6), value);
	}

	inline static int32_t get_offset_of__skeletonEffectCurTrack_7() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____skeletonEffectCurTrack_7)); }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * get__skeletonEffectCurTrack_7() const { return ____skeletonEffectCurTrack_7; }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A ** get_address_of__skeletonEffectCurTrack_7() { return &____skeletonEffectCurTrack_7; }
	inline void set__skeletonEffectCurTrack_7(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * value)
	{
		____skeletonEffectCurTrack_7 = value;
		Il2CppCodeGenWriteBarrier((&____skeletonEffectCurTrack_7), value);
	}

	inline static int32_t get_offset_of_m_colorPlanetUnlockBlack_8() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_colorPlanetUnlockBlack_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorPlanetUnlockBlack_8() const { return ___m_colorPlanetUnlockBlack_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorPlanetUnlockBlack_8() { return &___m_colorPlanetUnlockBlack_8; }
	inline void set_m_colorPlanetUnlockBlack_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorPlanetUnlockBlack_8 = value;
	}

	inline static int32_t get_offset_of_m_colorBlackWhite_9() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_colorBlackWhite_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorBlackWhite_9() const { return ___m_colorBlackWhite_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorBlackWhite_9() { return &___m_colorBlackWhite_9; }
	inline void set_m_colorBlackWhite_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorBlackWhite_9 = value;
	}

	inline static int32_t get_offset_of_m_colorOutlineNormal_10() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_colorOutlineNormal_10)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorOutlineNormal_10() const { return ___m_colorOutlineNormal_10; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorOutlineNormal_10() { return &___m_colorOutlineNormal_10; }
	inline void set_m_colorOutlineNormal_10(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorOutlineNormal_10 = value;
	}

	inline static int32_t get_offset_of_m_colorOutlineCurTrack_11() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_colorOutlineCurTrack_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorOutlineCurTrack_11() const { return ___m_colorOutlineCurTrack_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorOutlineCurTrack_11() { return &___m_colorOutlineCurTrack_11; }
	inline void set_m_colorOutlineCurTrack_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorOutlineCurTrack_11 = value;
	}

	inline static int32_t get_offset_of_m_matBlackWhite_12() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_matBlackWhite_12)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_matBlackWhite_12() const { return ___m_matBlackWhite_12; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_matBlackWhite_12() { return &___m_matBlackWhite_12; }
	inline void set_m_matBlackWhite_12(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_matBlackWhite_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_matBlackWhite_12), value);
	}

	inline static int32_t get_offset_of_m_sprBlockBgCurTrack_13() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBlockBgCurTrack_13)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBlockBgCurTrack_13() const { return ___m_sprBlockBgCurTrack_13; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBlockBgCurTrack_13() { return &___m_sprBlockBgCurTrack_13; }
	inline void set_m_sprBlockBgCurTrack_13(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBlockBgCurTrack_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBlockBgCurTrack_13), value);
	}

	inline static int32_t get_offset_of_m_sprBlockBgNormal_14() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBlockBgNormal_14)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBlockBgNormal_14() const { return ___m_sprBlockBgNormal_14; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBlockBgNormal_14() { return &___m_sprBlockBgNormal_14; }
	inline void set_m_sprBlockBgNormal_14(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBlockBgNormal_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBlockBgNormal_14), value);
	}

	inline static int32_t get_offset_of_m_sprBlockBgCanNotUnlock_15() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBlockBgCanNotUnlock_15)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBlockBgCanNotUnlock_15() const { return ___m_sprBlockBgCanNotUnlock_15; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBlockBgCanNotUnlock_15() { return &___m_sprBlockBgCanNotUnlock_15; }
	inline void set_m_sprBlockBgCanNotUnlock_15(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBlockBgCanNotUnlock_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBlockBgCanNotUnlock_15), value);
	}

	inline static int32_t get_offset_of_m_fSlideTime_16() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fSlideTime_16)); }
	inline float get_m_fSlideTime_16() const { return ___m_fSlideTime_16; }
	inline float* get_address_of_m_fSlideTime_16() { return &___m_fSlideTime_16; }
	inline void set_m_fSlideTime_16(float value)
	{
		___m_fSlideTime_16 = value;
	}

	inline static int32_t get_offset_of_m_aryTheMainlandCenters_17() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryTheMainlandCenters_17)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryTheMainlandCenters_17() const { return ___m_aryTheMainlandCenters_17; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryTheMainlandCenters_17() { return &___m_aryTheMainlandCenters_17; }
	inline void set_m_aryTheMainlandCenters_17(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryTheMainlandCenters_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryTheMainlandCenters_17), value);
	}

	inline static int32_t get_offset_of_m_aryDistrictName_21() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryDistrictName_21)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_aryDistrictName_21() const { return ___m_aryDistrictName_21; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_aryDistrictName_21() { return &___m_aryDistrictName_21; }
	inline void set_m_aryDistrictName_21(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_aryDistrictName_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryDistrictName_21), value);
	}

	inline static int32_t get_offset_of_m_sprBg_Locked_Left_22() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBg_Locked_Left_22)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBg_Locked_Left_22() const { return ___m_sprBg_Locked_Left_22; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBg_Locked_Left_22() { return &___m_sprBg_Locked_Left_22; }
	inline void set_m_sprBg_Locked_Left_22(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBg_Locked_Left_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBg_Locked_Left_22), value);
	}

	inline static int32_t get_offset_of_m_sprBg_Locked_Right_23() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBg_Locked_Right_23)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBg_Locked_Right_23() const { return ___m_sprBg_Locked_Right_23; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBg_Locked_Right_23() { return &___m_sprBg_Locked_Right_23; }
	inline void set_m_sprBg_Locked_Right_23(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBg_Locked_Right_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBg_Locked_Right_23), value);
	}

	inline static int32_t get_offset_of_m_sprBgUnlocked_Left_24() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBgUnlocked_Left_24)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBgUnlocked_Left_24() const { return ___m_sprBgUnlocked_Left_24; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBgUnlocked_Left_24() { return &___m_sprBgUnlocked_Left_24; }
	inline void set_m_sprBgUnlocked_Left_24(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBgUnlocked_Left_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBgUnlocked_Left_24), value);
	}

	inline static int32_t get_offset_of_m_sprBgUnlocked_Right_25() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_sprBgUnlocked_Right_25)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprBgUnlocked_Right_25() const { return ___m_sprBgUnlocked_Right_25; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprBgUnlocked_Right_25() { return &___m_sprBgUnlocked_Right_25; }
	inline void set_m_sprBgUnlocked_Right_25(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprBgUnlocked_Right_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprBgUnlocked_Right_25), value);
	}

	inline static int32_t get_offset_of_m_aryMoneyCounters_26() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryMoneyCounters_26)); }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* get_m_aryMoneyCounters_26() const { return ___m_aryMoneyCounters_26; }
	inline MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A** get_address_of_m_aryMoneyCounters_26() { return &___m_aryMoneyCounters_26; }
	inline void set_m_aryMoneyCounters_26(MoneyCounterU5BU5D_t6DD6D2DC5A638E59BF2B624387749501EFD7C29A* value)
	{
		___m_aryMoneyCounters_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMoneyCounters_26), value);
	}

	inline static int32_t get_offset_of_m_aryVariousCoinIcons_27() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryVariousCoinIcons_27)); }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* get_m_aryVariousCoinIcons_27() const { return ___m_aryVariousCoinIcons_27; }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D** get_address_of_m_aryVariousCoinIcons_27() { return &___m_aryVariousCoinIcons_27; }
	inline void set_m_aryVariousCoinIcons_27(ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* value)
	{
		___m_aryVariousCoinIcons_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryVariousCoinIcons_27), value);
	}

	inline static int32_t get_offset_of__btnAdventure_28() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____btnAdventure_28)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnAdventure_28() const { return ____btnAdventure_28; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnAdventure_28() { return &____btnAdventure_28; }
	inline void set__btnAdventure_28(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnAdventure_28 = value;
		Il2CppCodeGenWriteBarrier((&____btnAdventure_28), value);
	}

	inline static int32_t get_offset_of__containerZengShouCounters_29() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____containerZengShouCounters_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerZengShouCounters_29() const { return ____containerZengShouCounters_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerZengShouCounters_29() { return &____containerZengShouCounters_29; }
	inline void set__containerZengShouCounters_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerZengShouCounters_29 = value;
		Il2CppCodeGenWriteBarrier((&____containerZengShouCounters_29), value);
	}

	inline static int32_t get_offset_of__containerRecycledCounter_30() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____containerRecycledCounter_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerRecycledCounter_30() const { return ____containerRecycledCounter_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerRecycledCounter_30() { return &____containerRecycledCounter_30; }
	inline void set__containerRecycledCounter_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerRecycledCounter_30 = value;
		Il2CppCodeGenWriteBarrier((&____containerRecycledCounter_30), value);
	}

	inline static int32_t get_offset_of__panelZengShou_31() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____panelZengShou_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelZengShou_31() const { return ____panelZengShou_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelZengShou_31() { return &____panelZengShou_31; }
	inline void set__panelZengShou_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelZengShou_31 = value;
		Il2CppCodeGenWriteBarrier((&____panelZengShou_31), value);
	}

	inline static int32_t get_offset_of__panelPanelDetails_32() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____panelPanelDetails_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelPanelDetails_32() const { return ____panelPanelDetails_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelPanelDetails_32() { return &____panelPanelDetails_32; }
	inline void set__panelPanelDetails_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelPanelDetails_32 = value;
		Il2CppCodeGenWriteBarrier((&____panelPanelDetails_32), value);
	}

	inline static int32_t get_offset_of__txtPanelDetailstitle_33() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtPanelDetailstitle_33)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPanelDetailstitle_33() const { return ____txtPanelDetailstitle_33; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPanelDetailstitle_33() { return &____txtPanelDetailstitle_33; }
	inline void set__txtPanelDetailstitle_33(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPanelDetailstitle_33 = value;
		Il2CppCodeGenWriteBarrier((&____txtPanelDetailstitle_33), value);
	}

	inline static int32_t get_offset_of_m_aryUIDistricts_34() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryUIDistricts_34)); }
	inline UIDistrictU5BU5D_tDD148C70027D94D63D232B5C9FBB142D5EA721CB* get_m_aryUIDistricts_34() const { return ___m_aryUIDistricts_34; }
	inline UIDistrictU5BU5D_tDD148C70027D94D63D232B5C9FBB142D5EA721CB** get_address_of_m_aryUIDistricts_34() { return &___m_aryUIDistricts_34; }
	inline void set_m_aryUIDistricts_34(UIDistrictU5BU5D_tDD148C70027D94D63D232B5C9FBB142D5EA721CB* value)
	{
		___m_aryUIDistricts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUIDistricts_34), value);
	}

	inline static int32_t get_offset_of_m_aryUIPlanets_35() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryUIPlanets_35)); }
	inline UIPlanetU5BU5D_t8587E1E90D0FB3095F0BBAA9DF7CE4A5A6A94298* get_m_aryUIPlanets_35() const { return ___m_aryUIPlanets_35; }
	inline UIPlanetU5BU5D_t8587E1E90D0FB3095F0BBAA9DF7CE4A5A6A94298** get_address_of_m_aryUIPlanets_35() { return &___m_aryUIPlanets_35; }
	inline void set_m_aryUIPlanets_35(UIPlanetU5BU5D_t8587E1E90D0FB3095F0BBAA9DF7CE4A5A6A94298* value)
	{
		___m_aryUIPlanets_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUIPlanets_35), value);
	}

	inline static int32_t get_offset_of__panelUnlockDistrict_36() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____panelUnlockDistrict_36)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelUnlockDistrict_36() const { return ____panelUnlockDistrict_36; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelUnlockDistrict_36() { return &____panelUnlockDistrict_36; }
	inline void set__panelUnlockDistrict_36(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelUnlockDistrict_36 = value;
		Il2CppCodeGenWriteBarrier((&____panelUnlockDistrict_36), value);
	}

	inline static int32_t get_offset_of__txtUnlockDistrictCost_37() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtUnlockDistrictCost_37)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtUnlockDistrictCost_37() const { return ____txtUnlockDistrictCost_37; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtUnlockDistrictCost_37() { return &____txtUnlockDistrictCost_37; }
	inline void set__txtUnlockDistrictCost_37(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtUnlockDistrictCost_37 = value;
		Il2CppCodeGenWriteBarrier((&____txtUnlockDistrictCost_37), value);
	}

	inline static int32_t get_offset_of__imgUnlockTrackPriceIcon_38() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____imgUnlockTrackPriceIcon_38)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgUnlockTrackPriceIcon_38() const { return ____imgUnlockTrackPriceIcon_38; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgUnlockTrackPriceIcon_38() { return &____imgUnlockTrackPriceIcon_38; }
	inline void set__imgUnlockTrackPriceIcon_38(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgUnlockTrackPriceIcon_38 = value;
		Il2CppCodeGenWriteBarrier((&____imgUnlockTrackPriceIcon_38), value);
	}

	inline static int32_t get_offset_of__panelUnlockPlanet_39() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____panelUnlockPlanet_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelUnlockPlanet_39() const { return ____panelUnlockPlanet_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelUnlockPlanet_39() { return &____panelUnlockPlanet_39; }
	inline void set__panelUnlockPlanet_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelUnlockPlanet_39 = value;
		Il2CppCodeGenWriteBarrier((&____panelUnlockPlanet_39), value);
	}

	inline static int32_t get_offset_of__txtUnlockPlanetCost_40() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtUnlockPlanetCost_40)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtUnlockPlanetCost_40() const { return ____txtUnlockPlanetCost_40; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtUnlockPlanetCost_40() { return &____txtUnlockPlanetCost_40; }
	inline void set__txtUnlockPlanetCost_40(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtUnlockPlanetCost_40 = value;
		Il2CppCodeGenWriteBarrier((&____txtUnlockPlanetCost_40), value);
	}

	inline static int32_t get_offset_of__imgUnlockPlanetPriceIcon_41() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____imgUnlockPlanetPriceIcon_41)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgUnlockPlanetPriceIcon_41() const { return ____imgUnlockPlanetPriceIcon_41; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgUnlockPlanetPriceIcon_41() { return &____imgUnlockPlanetPriceIcon_41; }
	inline void set__imgUnlockPlanetPriceIcon_41(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgUnlockPlanetPriceIcon_41 = value;
		Il2CppCodeGenWriteBarrier((&____imgUnlockPlanetPriceIcon_41), value);
	}

	inline static int32_t get_offset_of__panelEntering_42() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____panelEntering_42)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelEntering_42() const { return ____panelEntering_42; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelEntering_42() { return &____panelEntering_42; }
	inline void set__panelEntering_42(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelEntering_42 = value;
		Il2CppCodeGenWriteBarrier((&____panelEntering_42), value);
	}

	inline static int32_t get_offset_of__containerPlanets_43() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____containerPlanets_43)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerPlanets_43() const { return ____containerPlanets_43; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerPlanets_43() { return &____containerPlanets_43; }
	inline void set__containerPlanets_43(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerPlanets_43 = value;
		Il2CppCodeGenWriteBarrier((&____containerPlanets_43), value);
	}

	inline static int32_t get_offset_of__containerMainPlanets_44() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____containerMainPlanets_44)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerMainPlanets_44() const { return ____containerMainPlanets_44; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerMainPlanets_44() { return &____containerMainPlanets_44; }
	inline void set__containerMainPlanets_44(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerMainPlanets_44 = value;
		Il2CppCodeGenWriteBarrier((&____containerMainPlanets_44), value);
	}

	inline static int32_t get_offset_of__containerOtherPlanets_45() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____containerOtherPlanets_45)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerOtherPlanets_45() const { return ____containerOtherPlanets_45; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerOtherPlanets_45() { return &____containerOtherPlanets_45; }
	inline void set__containerOtherPlanets_45(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerOtherPlanets_45 = value;
		Il2CppCodeGenWriteBarrier((&____containerOtherPlanets_45), value);
	}

	inline static int32_t get_offset_of__panelScienceTree_46() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____panelScienceTree_46)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelScienceTree_46() const { return ____panelScienceTree_46; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelScienceTree_46() { return &____panelScienceTree_46; }
	inline void set__panelScienceTree_46(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelScienceTree_46 = value;
		Il2CppCodeGenWriteBarrier((&____panelScienceTree_46), value);
	}

	inline static int32_t get_offset_of__txtCurPlanetAndTrack_47() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtCurPlanetAndTrack_47)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurPlanetAndTrack_47() const { return ____txtCurPlanetAndTrack_47; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurPlanetAndTrack_47() { return &____txtCurPlanetAndTrack_47; }
	inline void set__txtCurPlanetAndTrack_47(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurPlanetAndTrack_47 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurPlanetAndTrack_47), value);
	}

	inline static int32_t get_offset_of__txtCurTrackLevel_48() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtCurTrackLevel_48)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurTrackLevel_48() const { return ____txtCurTrackLevel_48; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurTrackLevel_48() { return &____txtCurTrackLevel_48; }
	inline void set__txtCurTrackLevel_48(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurTrackLevel_48 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurTrackLevel_48), value);
	}

	inline static int32_t get_offset_of_m_aryPlanetTitle_49() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryPlanetTitle_49)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryPlanetTitle_49() const { return ___m_aryPlanetTitle_49; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryPlanetTitle_49() { return &___m_aryPlanetTitle_49; }
	inline void set_m_aryPlanetTitle_49(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryPlanetTitle_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPlanetTitle_49), value);
	}

	inline static int32_t get_offset_of__subpanelBatCollectOffline_50() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____subpanelBatCollectOffline_50)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__subpanelBatCollectOffline_50() const { return ____subpanelBatCollectOffline_50; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__subpanelBatCollectOffline_50() { return &____subpanelBatCollectOffline_50; }
	inline void set__subpanelBatCollectOffline_50(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____subpanelBatCollectOffline_50 = value;
		Il2CppCodeGenWriteBarrier((&____subpanelBatCollectOffline_50), value);
	}

	inline static int32_t get_offset_of__txtBatOfflineGain_51() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtBatOfflineGain_51)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtBatOfflineGain_51() const { return ____txtBatOfflineGain_51; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtBatOfflineGain_51() { return &____txtBatOfflineGain_51; }
	inline void set__txtBatOfflineGain_51(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtBatOfflineGain_51 = value;
		Il2CppCodeGenWriteBarrier((&____txtBatOfflineGain_51), value);
	}

	inline static int32_t get_offset_of__txtBatOfflineDetail_52() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtBatOfflineDetail_52)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtBatOfflineDetail_52() const { return ____txtBatOfflineDetail_52; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtBatOfflineDetail_52() { return &____txtBatOfflineDetail_52; }
	inline void set__txtBatOfflineDetail_52(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtBatOfflineDetail_52 = value;
		Il2CppCodeGenWriteBarrier((&____txtBatOfflineDetail_52), value);
	}

	inline static int32_t get_offset_of__txtBatOfflineTitle_53() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____txtBatOfflineTitle_53)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtBatOfflineTitle_53() const { return ____txtBatOfflineTitle_53; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtBatOfflineTitle_53() { return &____txtBatOfflineTitle_53; }
	inline void set__txtBatOfflineTitle_53(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtBatOfflineTitle_53 = value;
		Il2CppCodeGenWriteBarrier((&____txtBatOfflineTitle_53), value);
	}

	inline static int32_t get_offset_of__goStarSky_54() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____goStarSky_54)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goStarSky_54() const { return ____goStarSky_54; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goStarSky_54() { return &____goStarSky_54; }
	inline void set__goStarSky_54(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goStarSky_54 = value;
		Il2CppCodeGenWriteBarrier((&____goStarSky_54), value);
	}

	inline static int32_t get_offset_of_m_fStarSkyMoveSpeed_55() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fStarSkyMoveSpeed_55)); }
	inline float get_m_fStarSkyMoveSpeed_55() const { return ___m_fStarSkyMoveSpeed_55; }
	inline float* get_address_of_m_fStarSkyMoveSpeed_55() { return &___m_fStarSkyMoveSpeed_55; }
	inline void set_m_fStarSkyMoveSpeed_55(float value)
	{
		___m_fStarSkyMoveSpeed_55 = value;
	}

	inline static int32_t get_offset_of_m_aryPlanets_56() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryPlanets_56)); }
	inline PlanetU5BU5D_tFAD0DE71ED69B99F92FC1525DDAE53548F72893D* get_m_aryPlanets_56() const { return ___m_aryPlanets_56; }
	inline PlanetU5BU5D_tFAD0DE71ED69B99F92FC1525DDAE53548F72893D** get_address_of_m_aryPlanets_56() { return &___m_aryPlanets_56; }
	inline void set_m_aryPlanets_56(PlanetU5BU5D_tFAD0DE71ED69B99F92FC1525DDAE53548F72893D* value)
	{
		___m_aryPlanets_56 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPlanets_56), value);
	}

	inline static int32_t get_offset_of_m_CurPlanet_57() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_CurPlanet_57)); }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * get_m_CurPlanet_57() const { return ___m_CurPlanet_57; }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B ** get_address_of_m_CurPlanet_57() { return &___m_CurPlanet_57; }
	inline void set_m_CurPlanet_57(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * value)
	{
		___m_CurPlanet_57 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurPlanet_57), value);
	}

	inline static int32_t get_offset_of_m_CurDistrict_58() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_CurDistrict_58)); }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * get_m_CurDistrict_58() const { return ___m_CurDistrict_58; }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A ** get_address_of_m_CurDistrict_58() { return &___m_CurDistrict_58; }
	inline void set_m_CurDistrict_58(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * value)
	{
		___m_CurDistrict_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurDistrict_58), value);
	}

	inline static int32_t get_offset_of_m_nCurShowPlanetDetialIdOnUI_59() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_nCurShowPlanetDetialIdOnUI_59)); }
	inline int32_t get_m_nCurShowPlanetDetialIdOnUI_59() const { return ___m_nCurShowPlanetDetialIdOnUI_59; }
	inline int32_t* get_address_of_m_nCurShowPlanetDetialIdOnUI_59() { return &___m_nCurShowPlanetDetialIdOnUI_59; }
	inline void set_m_nCurShowPlanetDetialIdOnUI_59(int32_t value)
	{
		___m_nCurShowPlanetDetialIdOnUI_59 = value;
	}

	inline static int32_t get_offset_of_m_dicZengShouCounters_60() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_dicZengShouCounters_60)); }
	inline Dictionary_2_tA42C789C20DBB051A4F863D661877067E6AE8081 * get_m_dicZengShouCounters_60() const { return ___m_dicZengShouCounters_60; }
	inline Dictionary_2_tA42C789C20DBB051A4F863D661877067E6AE8081 ** get_address_of_m_dicZengShouCounters_60() { return &___m_dicZengShouCounters_60; }
	inline void set_m_dicZengShouCounters_60(Dictionary_2_tA42C789C20DBB051A4F863D661877067E6AE8081 * value)
	{
		___m_dicZengShouCounters_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicZengShouCounters_60), value);
	}

	inline static int32_t get_offset_of_m_aryCoin0Value_61() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryCoin0Value_61)); }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* get_m_aryCoin0Value_61() const { return ___m_aryCoin0Value_61; }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188** get_address_of_m_aryCoin0Value_61() { return &___m_aryCoin0Value_61; }
	inline void set_m_aryCoin0Value_61(TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* value)
	{
		___m_aryCoin0Value_61 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin0Value_61), value);
	}

	inline static int32_t get_offset_of_m_aryCoin1Value_62() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryCoin1Value_62)); }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* get_m_aryCoin1Value_62() const { return ___m_aryCoin1Value_62; }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188** get_address_of_m_aryCoin1Value_62() { return &___m_aryCoin1Value_62; }
	inline void set_m_aryCoin1Value_62(TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* value)
	{
		___m_aryCoin1Value_62 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin1Value_62), value);
	}

	inline static int32_t get_offset_of_m_aryCoin2Value_63() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryCoin2Value_63)); }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* get_m_aryCoin2Value_63() const { return ___m_aryCoin2Value_63; }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188** get_address_of_m_aryCoin2Value_63() { return &___m_aryCoin2Value_63; }
	inline void set_m_aryCoin2Value_63(TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* value)
	{
		___m_aryCoin2Value_63 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoin2Value_63), value);
	}

	inline static int32_t get_offset_of_m_aryDiamondValue_64() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryDiamondValue_64)); }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* get_m_aryDiamondValue_64() const { return ___m_aryDiamondValue_64; }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188** get_address_of_m_aryDiamondValue_64() { return &___m_aryDiamondValue_64; }
	inline void set_m_aryDiamondValue_64(TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* value)
	{
		___m_aryDiamondValue_64 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryDiamondValue_64), value);
	}

	inline static int32_t get_offset_of__btnCurEnterButton_65() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____btnCurEnterButton_65)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnCurEnterButton_65() const { return ____btnCurEnterButton_65; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnCurEnterButton_65() { return &____btnCurEnterButton_65; }
	inline void set__btnCurEnterButton_65(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnCurEnterButton_65 = value;
		Il2CppCodeGenWriteBarrier((&____btnCurEnterButton_65), value);
	}

	inline static int32_t get_offset_of_m_fUpdatePlanetInfoLoopTimeElapse_66() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fUpdatePlanetInfoLoopTimeElapse_66)); }
	inline float get_m_fUpdatePlanetInfoLoopTimeElapse_66() const { return ___m_fUpdatePlanetInfoLoopTimeElapse_66; }
	inline float* get_address_of_m_fUpdatePlanetInfoLoopTimeElapse_66() { return &___m_fUpdatePlanetInfoLoopTimeElapse_66; }
	inline void set_m_fUpdatePlanetInfoLoopTimeElapse_66(float value)
	{
		___m_fUpdatePlanetInfoLoopTimeElapse_66 = value;
	}

	inline static int32_t get_offset_of_m_PlanetToPrestige_67() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_PlanetToPrestige_67)); }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * get_m_PlanetToPrestige_67() const { return ___m_PlanetToPrestige_67; }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B ** get_address_of_m_PlanetToPrestige_67() { return &___m_PlanetToPrestige_67; }
	inline void set_m_PlanetToPrestige_67(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * value)
	{
		___m_PlanetToPrestige_67 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlanetToPrestige_67), value);
	}

	inline static int32_t get_offset_of_m_DistrictToPrestige_68() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_DistrictToPrestige_68)); }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * get_m_DistrictToPrestige_68() const { return ___m_DistrictToPrestige_68; }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A ** get_address_of_m_DistrictToPrestige_68() { return &___m_DistrictToPrestige_68; }
	inline void set_m_DistrictToPrestige_68(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * value)
	{
		___m_DistrictToPrestige_68 = value;
		Il2CppCodeGenWriteBarrier((&___m_DistrictToPrestige_68), value);
	}

	inline static int32_t get_offset_of_m_bPlanetDetailPanelOpen_69() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bPlanetDetailPanelOpen_69)); }
	inline bool get_m_bPlanetDetailPanelOpen_69() const { return ___m_bPlanetDetailPanelOpen_69; }
	inline bool* get_address_of_m_bPlanetDetailPanelOpen_69() { return &___m_bPlanetDetailPanelOpen_69; }
	inline void set_m_bPlanetDetailPanelOpen_69(bool value)
	{
		___m_bPlanetDetailPanelOpen_69 = value;
	}

	inline static int32_t get_offset_of_m_districtToUnlock_70() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_districtToUnlock_70)); }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * get_m_districtToUnlock_70() const { return ___m_districtToUnlock_70; }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A ** get_address_of_m_districtToUnlock_70() { return &___m_districtToUnlock_70; }
	inline void set_m_districtToUnlock_70(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * value)
	{
		___m_districtToUnlock_70 = value;
		Il2CppCodeGenWriteBarrier((&___m_districtToUnlock_70), value);
	}

	inline static int32_t get_offset_of_m_planetToUnlock_71() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_planetToUnlock_71)); }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * get_m_planetToUnlock_71() const { return ___m_planetToUnlock_71; }
	inline Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B ** get_address_of_m_planetToUnlock_71() { return &___m_planetToUnlock_71; }
	inline void set_m_planetToUnlock_71(Planet_t898D8BC07B9E87CE1EEC734414E5AF9609023C3B * value)
	{
		___m_planetToUnlock_71 = value;
		Il2CppCodeGenWriteBarrier((&___m_planetToUnlock_71), value);
	}

	inline static int32_t get_offset_of_m_nToEnterRace_PlanetId_72() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_nToEnterRace_PlanetId_72)); }
	inline int32_t get_m_nToEnterRace_PlanetId_72() const { return ___m_nToEnterRace_PlanetId_72; }
	inline int32_t* get_address_of_m_nToEnterRace_PlanetId_72() { return &___m_nToEnterRace_PlanetId_72; }
	inline void set_m_nToEnterRace_PlanetId_72(int32_t value)
	{
		___m_nToEnterRace_PlanetId_72 = value;
	}

	inline static int32_t get_offset_of_m_nToEnterRace_DistrictId_73() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_nToEnterRace_DistrictId_73)); }
	inline int32_t get_m_nToEnterRace_DistrictId_73() const { return ___m_nToEnterRace_DistrictId_73; }
	inline int32_t* get_address_of_m_nToEnterRace_DistrictId_73() { return &___m_nToEnterRace_DistrictId_73; }
	inline void set_m_nToEnterRace_DistrictId_73(int32_t value)
	{
		___m_nToEnterRace_DistrictId_73 = value;
	}

	inline static int32_t get_offset_of_m_fPreEnterRaceLoopTime_74() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fPreEnterRaceLoopTime_74)); }
	inline float get_m_fPreEnterRaceLoopTime_74() const { return ___m_fPreEnterRaceLoopTime_74; }
	inline float* get_address_of_m_fPreEnterRaceLoopTime_74() { return &___m_fPreEnterRaceLoopTime_74; }
	inline void set_m_fPreEnterRaceLoopTime_74(float value)
	{
		___m_fPreEnterRaceLoopTime_74 = value;
	}

	inline static int32_t get_offset_of_m_bStarSkyMoving_75() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bStarSkyMoving_75)); }
	inline bool get_m_bStarSkyMoving_75() const { return ___m_bStarSkyMoving_75; }
	inline bool* get_address_of_m_bStarSkyMoving_75() { return &___m_bStarSkyMoving_75; }
	inline void set_m_bStarSkyMoving_75(bool value)
	{
		___m_bStarSkyMoving_75 = value;
	}

	inline static int32_t get_offset_of_m_bIsShowingZengShouPanel_76() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bIsShowingZengShouPanel_76)); }
	inline bool get_m_bIsShowingZengShouPanel_76() const { return ___m_bIsShowingZengShouPanel_76; }
	inline bool* get_address_of_m_bIsShowingZengShouPanel_76() { return &___m_bIsShowingZengShouPanel_76; }
	inline void set_m_bIsShowingZengShouPanel_76(bool value)
	{
		___m_bIsShowingZengShouPanel_76 = value;
	}

	inline static int32_t get_offset_of_m_bTestShow_77() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bTestShow_77)); }
	inline bool get_m_bTestShow_77() const { return ___m_bTestShow_77; }
	inline bool* get_address_of_m_bTestShow_77() { return &___m_bTestShow_77; }
	inline void set_m_bTestShow_77(bool value)
	{
		___m_bTestShow_77 = value;
	}

	inline static int32_t get_offset_of_m_bSliding_78() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bSliding_78)); }
	inline bool get_m_bSliding_78() const { return ___m_bSliding_78; }
	inline bool* get_address_of_m_bSliding_78() { return &___m_bSliding_78; }
	inline void set_m_bSliding_78(bool value)
	{
		___m_bSliding_78 = value;
	}

	inline static int32_t get_offset_of_m_bBigMapShowing_79() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bBigMapShowing_79)); }
	inline bool get_m_bBigMapShowing_79() const { return ___m_bBigMapShowing_79; }
	inline bool* get_address_of_m_bBigMapShowing_79() { return &___m_bBigMapShowing_79; }
	inline void set_m_bBigMapShowing_79(bool value)
	{
		___m_bBigMapShowing_79 = value;
	}

	inline static int32_t get_offset_of_m_fSlideSpeed_80() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fSlideSpeed_80)); }
	inline float get_m_fSlideSpeed_80() const { return ___m_fSlideSpeed_80; }
	inline float* get_address_of_m_fSlideSpeed_80() { return &___m_fSlideSpeed_80; }
	inline void set_m_fSlideSpeed_80(float value)
	{
		___m_fSlideSpeed_80 = value;
	}

	inline static int32_t get_offset_of_m_fSlideA_81() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fSlideA_81)); }
	inline float get_m_fSlideA_81() const { return ___m_fSlideA_81; }
	inline float* get_address_of_m_fSlideA_81() { return &___m_fSlideA_81; }
	inline void set_m_fSlideA_81(float value)
	{
		___m_fSlideA_81 = value;
	}

	inline static int32_t get_offset_of_m_fV0_82() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fV0_82)); }
	inline float get_m_fV0_82() const { return ___m_fV0_82; }
	inline float* get_address_of_m_fV0_82() { return &___m_fV0_82; }
	inline void set_m_fV0_82(float value)
	{
		___m_fV0_82 = value;
	}

	inline static int32_t get_offset_of__goSVContent_83() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ____goSVContent_83)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goSVContent_83() const { return ____goSVContent_83; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goSVContent_83() { return &____goSVContent_83; }
	inline void set__goSVContent_83(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goSVContent_83 = value;
		Il2CppCodeGenWriteBarrier((&____goSVContent_83), value);
	}

	inline static int32_t get_offset_of_m_fDestPos_84() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fDestPos_84)); }
	inline float get_m_fDestPos_84() const { return ___m_fDestPos_84; }
	inline float* get_address_of_m_fDestPos_84() { return &___m_fDestPos_84; }
	inline void set_m_fDestPos_84(float value)
	{
		___m_fDestPos_84 = value;
	}

	inline static int32_t get_offset_of_m_fStartDragPos_85() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fStartDragPos_85)); }
	inline float get_m_fStartDragPos_85() const { return ___m_fStartDragPos_85; }
	inline float* get_address_of_m_fStartDragPos_85() { return &___m_fStartDragPos_85; }
	inline void set_m_fStartDragPos_85(float value)
	{
		___m_fStartDragPos_85 = value;
	}

	inline static int32_t get_offset_of_m_fEndDragPos_86() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fEndDragPos_86)); }
	inline float get_m_fEndDragPos_86() const { return ___m_fEndDragPos_86; }
	inline float* get_address_of_m_fEndDragPos_86() { return &___m_fEndDragPos_86; }
	inline void set_m_fEndDragPos_86(float value)
	{
		___m_fEndDragPos_86 = value;
	}

	inline static int32_t get_offset_of_m_bDragging_87() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bDragging_87)); }
	inline bool get_m_bDragging_87() const { return ___m_bDragging_87; }
	inline bool* get_address_of_m_bDragging_87() { return &___m_bDragging_87; }
	inline void set_m_bDragging_87(bool value)
	{
		___m_bDragging_87 = value;
	}

	inline static int32_t get_offset_of_m_fMouseStartPos_88() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fMouseStartPos_88)); }
	inline float get_m_fMouseStartPos_88() const { return ___m_fMouseStartPos_88; }
	inline float* get_address_of_m_fMouseStartPos_88() { return &___m_fMouseStartPos_88; }
	inline void set_m_fMouseStartPos_88(float value)
	{
		___m_fMouseStartPos_88 = value;
	}

	inline static int32_t get_offset_of_m_bClickProhibit_89() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_bClickProhibit_89)); }
	inline bool get_m_bClickProhibit_89() const { return ___m_bClickProhibit_89; }
	inline bool* get_address_of_m_bClickProhibit_89() { return &___m_bClickProhibit_89; }
	inline void set_m_bClickProhibit_89(bool value)
	{
		___m_bClickProhibit_89 = value;
	}

	inline static int32_t get_offset_of_m_fClickProhibitTimeLeft_90() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_fClickProhibitTimeLeft_90)); }
	inline float get_m_fClickProhibitTimeLeft_90() const { return ___m_fClickProhibitTimeLeft_90; }
	inline float* get_address_of_m_fClickProhibitTimeLeft_90() { return &___m_fClickProhibitTimeLeft_90; }
	inline void set_m_fClickProhibitTimeLeft_90(float value)
	{
		___m_fClickProhibitTimeLeft_90 = value;
	}

	inline static int32_t get_offset_of_m_nCurSelctedIndex_91() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_nCurSelctedIndex_91)); }
	inline int32_t get_m_nCurSelctedIndex_91() const { return ___m_nCurSelctedIndex_91; }
	inline int32_t* get_address_of_m_nCurSelctedIndex_91() { return &___m_nCurSelctedIndex_91; }
	inline void set_m_nCurSelctedIndex_91(int32_t value)
	{
		___m_nCurSelctedIndex_91 = value;
	}

	inline static int32_t get_offset_of_m_aryMainLands_92() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C, ___m_aryMainLands_92)); }
	inline UIMainLandU5BU5D_tF159C8131B1BA4E2EEDF316C136F4BA6CE07B280* get_m_aryMainLands_92() const { return ___m_aryMainLands_92; }
	inline UIMainLandU5BU5D_tF159C8131B1BA4E2EEDF316C136F4BA6CE07B280** get_address_of_m_aryMainLands_92() { return &___m_aryMainLands_92; }
	inline void set_m_aryMainLands_92(UIMainLandU5BU5D_tF159C8131B1BA4E2EEDF316C136F4BA6CE07B280* value)
	{
		___m_aryMainLands_92 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryMainLands_92), value);
	}
};

struct MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields
{
public:
	// UnityEngine.Vector3 MapManager::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_18;
	// UnityEngine.Vector3 MapManager::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_19;
	// MapManager MapManager::s_Instance
	MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C * ___s_Instance_20;

public:
	inline static int32_t get_offset_of_vecTempPos_18() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields, ___vecTempPos_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_18() const { return ___vecTempPos_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_18() { return &___vecTempPos_18; }
	inline void set_vecTempPos_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_18 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_19() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields, ___vecTempScale_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_19() const { return ___vecTempScale_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_19() { return &___vecTempScale_19; }
	inline void set_vecTempScale_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_19 = value;
	}

	inline static int32_t get_offset_of_s_Instance_20() { return static_cast<int32_t>(offsetof(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields, ___s_Instance_20)); }
	inline MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C * get_s_Instance_20() const { return ___s_Instance_20; }
	inline MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C ** get_address_of_s_Instance_20() { return &___s_Instance_20; }
	inline void set_s_Instance_20(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C * value)
	{
		___s_Instance_20 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPMANAGER_T76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_H
#ifndef PAW_T9302465120C7C5579ADFF12950B7E6F59B4DF011_H
#define PAW_T9302465120C7C5579ADFF12950B7E6F59B4DF011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Paw
struct  Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// CFrameAnimationEffect Paw::_frameaniClick
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * ____frameaniClick_7;
	// BaseScale Paw::_basescalePaw
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____basescalePaw_8;
	// UnityEngine.UI.Image Paw::_imgPaw
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgPaw_9;
	// UnityEngine.SpriteRenderer Paw::_srPaw
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srPaw_10;
	// System.Single Paw::m_fCurAlpha
	float ___m_fCurAlpha_11;
	// System.Boolean Paw::m_bFading
	bool ___m_bFading_12;
	// System.Boolean Paw::m_bMoving
	bool ___m_bMoving_13;
	// UnityEngine.Vector3 Paw::m_vecStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecStartPos_14;
	// UnityEngine.Vector3 Paw::m_vecEndPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecEndPos_15;
	// UnityEngine.Vector3 Paw::m_vecMoveSpeed
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecMoveSpeed_16;
	// System.Single Paw::m_fWaitTime
	float ___m_fWaitTime_17;
	// System.Int32 Paw::m_nMoveStatus
	int32_t ___m_nMoveStatus_18;
	// System.Single Paw::m_fTimeElapse
	float ___m_fTimeElapse_19;
	// Paw/delegateMoveEnd Paw::eventMoveEnd
	delegateMoveEnd_t1F76CEA26063F0B11A971CAB2747FA1BFA8241F7 * ___eventMoveEnd_20;

public:
	inline static int32_t get_offset_of__frameaniClick_7() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ____frameaniClick_7)); }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * get__frameaniClick_7() const { return ____frameaniClick_7; }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 ** get_address_of__frameaniClick_7() { return &____frameaniClick_7; }
	inline void set__frameaniClick_7(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * value)
	{
		____frameaniClick_7 = value;
		Il2CppCodeGenWriteBarrier((&____frameaniClick_7), value);
	}

	inline static int32_t get_offset_of__basescalePaw_8() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ____basescalePaw_8)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__basescalePaw_8() const { return ____basescalePaw_8; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__basescalePaw_8() { return &____basescalePaw_8; }
	inline void set__basescalePaw_8(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____basescalePaw_8 = value;
		Il2CppCodeGenWriteBarrier((&____basescalePaw_8), value);
	}

	inline static int32_t get_offset_of__imgPaw_9() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ____imgPaw_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgPaw_9() const { return ____imgPaw_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgPaw_9() { return &____imgPaw_9; }
	inline void set__imgPaw_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgPaw_9 = value;
		Il2CppCodeGenWriteBarrier((&____imgPaw_9), value);
	}

	inline static int32_t get_offset_of__srPaw_10() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ____srPaw_10)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srPaw_10() const { return ____srPaw_10; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srPaw_10() { return &____srPaw_10; }
	inline void set__srPaw_10(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srPaw_10 = value;
		Il2CppCodeGenWriteBarrier((&____srPaw_10), value);
	}

	inline static int32_t get_offset_of_m_fCurAlpha_11() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_fCurAlpha_11)); }
	inline float get_m_fCurAlpha_11() const { return ___m_fCurAlpha_11; }
	inline float* get_address_of_m_fCurAlpha_11() { return &___m_fCurAlpha_11; }
	inline void set_m_fCurAlpha_11(float value)
	{
		___m_fCurAlpha_11 = value;
	}

	inline static int32_t get_offset_of_m_bFading_12() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_bFading_12)); }
	inline bool get_m_bFading_12() const { return ___m_bFading_12; }
	inline bool* get_address_of_m_bFading_12() { return &___m_bFading_12; }
	inline void set_m_bFading_12(bool value)
	{
		___m_bFading_12 = value;
	}

	inline static int32_t get_offset_of_m_bMoving_13() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_bMoving_13)); }
	inline bool get_m_bMoving_13() const { return ___m_bMoving_13; }
	inline bool* get_address_of_m_bMoving_13() { return &___m_bMoving_13; }
	inline void set_m_bMoving_13(bool value)
	{
		___m_bMoving_13 = value;
	}

	inline static int32_t get_offset_of_m_vecStartPos_14() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_vecStartPos_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecStartPos_14() const { return ___m_vecStartPos_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecStartPos_14() { return &___m_vecStartPos_14; }
	inline void set_m_vecStartPos_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecStartPos_14 = value;
	}

	inline static int32_t get_offset_of_m_vecEndPos_15() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_vecEndPos_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecEndPos_15() const { return ___m_vecEndPos_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecEndPos_15() { return &___m_vecEndPos_15; }
	inline void set_m_vecEndPos_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecEndPos_15 = value;
	}

	inline static int32_t get_offset_of_m_vecMoveSpeed_16() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_vecMoveSpeed_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecMoveSpeed_16() const { return ___m_vecMoveSpeed_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecMoveSpeed_16() { return &___m_vecMoveSpeed_16; }
	inline void set_m_vecMoveSpeed_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecMoveSpeed_16 = value;
	}

	inline static int32_t get_offset_of_m_fWaitTime_17() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_fWaitTime_17)); }
	inline float get_m_fWaitTime_17() const { return ___m_fWaitTime_17; }
	inline float* get_address_of_m_fWaitTime_17() { return &___m_fWaitTime_17; }
	inline void set_m_fWaitTime_17(float value)
	{
		___m_fWaitTime_17 = value;
	}

	inline static int32_t get_offset_of_m_nMoveStatus_18() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_nMoveStatus_18)); }
	inline int32_t get_m_nMoveStatus_18() const { return ___m_nMoveStatus_18; }
	inline int32_t* get_address_of_m_nMoveStatus_18() { return &___m_nMoveStatus_18; }
	inline void set_m_nMoveStatus_18(int32_t value)
	{
		___m_nMoveStatus_18 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_19() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___m_fTimeElapse_19)); }
	inline float get_m_fTimeElapse_19() const { return ___m_fTimeElapse_19; }
	inline float* get_address_of_m_fTimeElapse_19() { return &___m_fTimeElapse_19; }
	inline void set_m_fTimeElapse_19(float value)
	{
		___m_fTimeElapse_19 = value;
	}

	inline static int32_t get_offset_of_eventMoveEnd_20() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011, ___eventMoveEnd_20)); }
	inline delegateMoveEnd_t1F76CEA26063F0B11A971CAB2747FA1BFA8241F7 * get_eventMoveEnd_20() const { return ___eventMoveEnd_20; }
	inline delegateMoveEnd_t1F76CEA26063F0B11A971CAB2747FA1BFA8241F7 ** get_address_of_eventMoveEnd_20() { return &___eventMoveEnd_20; }
	inline void set_eventMoveEnd_20(delegateMoveEnd_t1F76CEA26063F0B11A971CAB2747FA1BFA8241F7 * value)
	{
		___eventMoveEnd_20 = value;
		Il2CppCodeGenWriteBarrier((&___eventMoveEnd_20), value);
	}
};

struct Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011_StaticFields
{
public:
	// UnityEngine.Color Paw::tempColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___tempColor_4;
	// UnityEngine.Vector3 Paw::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_5;
	// UnityEngine.Vector3 Paw::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_6;

public:
	inline static int32_t get_offset_of_tempColor_4() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011_StaticFields, ___tempColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_tempColor_4() const { return ___tempColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_tempColor_4() { return &___tempColor_4; }
	inline void set_tempColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___tempColor_4 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_5() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011_StaticFields, ___vecTempPos_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_5() const { return ___vecTempPos_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_5() { return &___vecTempPos_5; }
	inline void set_vecTempPos_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_5 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_6() { return static_cast<int32_t>(offsetof(Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011_StaticFields, ___vecTempScale_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_6() const { return ___vecTempScale_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_6() { return &___vecTempScale_6; }
	inline void set_vecTempScale_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAW_T9302465120C7C5579ADFF12950B7E6F59B4DF011_H
#ifndef RESOURCEMANAGER_TCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_H
#define RESOURCEMANAGER_TCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager
struct  ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Sprite ResourceManager::m_sprGreenCash
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprGreenCash_4;
	// UnityEngine.Sprite[] ResourceManager::m_aryCoinIcon_New
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryCoinIcon_New_5;
	// UnityEngine.Sprite[] ResourceManager::m_aryCoinIcon_Special_New
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryCoinIcon_Special_New_6;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileParking_0
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileParking_0_7;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileParking_1
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileParking_1_8;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileParking_2
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileParking_2_9;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileParking_3
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileParking_3_10;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileParking_4
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileParking_4_11;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileRunning_0
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileRunning_0_12;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileRunning_1
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileRunning_1_13;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileRunning_2
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileRunning_2_14;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileRunning_3
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileRunning_3_15;
	// UnityEngine.Sprite[] ResourceManager::m_aryAutomobileRunning_4
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryAutomobileRunning_4_16;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Sprite[]> ResourceManager::m_dicAutoSpritesParking
	Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E * ___m_dicAutoSpritesParking_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Sprite[]> ResourceManager::m_dicAutoSpritesRunning
	Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E * ___m_dicAutoSpritesRunning_18;
	// UnityEngine.Font ResourceManager::_font
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ____font_21;
	// UnityEngine.GameObject ResourceManager::m_preEnviromentMask
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preEnviromentMask_22;
	// UnityEngine.GameObject ResourceManager::m_preUIShoppinAndItemCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preUIShoppinAndItemCounter_23;
	// UnityEngine.Sprite[] ResourceManager::m_aryPlanetAvatar
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryPlanetAvatar_24;
	// UnityEngine.Sprite ResourceManager::m_sprArrow
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprArrow_25;
	// UnityEngine.Sprite[] ResourceManager::m_arySkillPointIcon
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_arySkillPointIcon_26;
	// UnityEngine.Sprite[] ResourceManager::m_aryCoinIcon
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryCoinIcon_27;
	// UnityEngine.GameObject ResourceManager::m_goRecycledPlanes
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goRecycledPlanes_28;
	// UnityEngine.Sprite[] ResourceManager::m_aryPlaneSprites
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryPlaneSprites_29;
	// UnityEngine.Sprite[] ResourceManager::m_aryParkingPlaneSprites
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryParkingPlaneSprites_30;
	// UnityEngine.GameObject ResourceManager::m_preZengShouCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preZengShouCounter_31;
	// UnityEngine.GameObject ResourceManager::m_prePlane
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_prePlane_32;
	// UnityEngine.GameObject ResourceManager::m_preJinBi
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preJinBi_33;
	// UnityEngine.GameObject ResourceManager::m_preRichTiaoZi
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preRichTiaoZi_34;
	// UnityEngine.GameObject ResourceManager::m_preVehicleCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preVehicleCounter_35;
	// UnityEngine.Color[] ResourceManager::m_aryTrailColor
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___m_aryTrailColor_36;
	// UnityEngine.GameObject ResourceManager::m_preSkill
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preSkill_37;
	// UnityEngine.GameObject ResourceManager::m_preFlyingCoin
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preFlyingCoin_38;
	// UnityEngine.GameObject ResourceManager::m_preScienceTreeConfig
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preScienceTreeConfig_39;
	// UnityEngine.GameObject ResourceManager::m_preScienceLeaf
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preScienceLeaf_40;
	// UnityEngine.Sprite[] ResourceManager::m_aryItemIcon
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryItemIcon_41;
	// UnityEngine.Sprite[] ResourceManager::m_aryCoinRaiseItemIcon
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryCoinRaiseItemIcon_42;
	// UnityEngine.Sprite[] ResourceManager::m_aryCoinDiamondItemIcon
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___m_aryCoinDiamondItemIcon_43;
	// UnityEngine.Sprite ResourceManager::m_sprWatchAdIcon
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprWatchAdIcon_44;
	// System.Collections.Generic.List`1<Plane> ResourceManager::m_lstRecycledPlanes
	List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * ___m_lstRecycledPlanes_46;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ResourceManager::m_lstRecycledJinBi
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___m_lstRecycledJinBi_47;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ResourceManager::m_lstRecycledRichTiaoZi
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___m_lstRecycledRichTiaoZi_48;
	// UnityEngine.GameObject ResourceManager::m_preUiItem
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preUiItem_49;
	// System.Collections.Generic.List`1<UIItemInBag> ResourceManager::m_lstRecylcedUiItems
	List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB * ___m_lstRecylcedUiItems_50;
	// UnityEngine.GameObject ResourceManager::_containerRecycledUIItems
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerRecycledUIItems_51;
	// UnityEngine.GameObject ResourceManager::m_goRecycedVehicleCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goRecycedVehicleCounter_53;
	// UnityEngine.GameObject ResourceManager::m_goRecycedResearchCounter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goRecycedResearchCounter_54;
	// System.Collections.Generic.List`1<UIFlyingCoin> ResourceManager::m_lstRecycledFlyingCoin
	List_1_tB36688DD81A4ADEF07F2EB8C42C12B19153C0C95 * ___m_lstRecycledFlyingCoin_55;
	// UnityEngine.GameObject ResourceManager::m_preTrail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preTrail_56;

public:
	inline static int32_t get_offset_of_m_sprGreenCash_4() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_sprGreenCash_4)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprGreenCash_4() const { return ___m_sprGreenCash_4; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprGreenCash_4() { return &___m_sprGreenCash_4; }
	inline void set_m_sprGreenCash_4(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprGreenCash_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprGreenCash_4), value);
	}

	inline static int32_t get_offset_of_m_aryCoinIcon_New_5() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryCoinIcon_New_5)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryCoinIcon_New_5() const { return ___m_aryCoinIcon_New_5; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryCoinIcon_New_5() { return &___m_aryCoinIcon_New_5; }
	inline void set_m_aryCoinIcon_New_5(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryCoinIcon_New_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoinIcon_New_5), value);
	}

	inline static int32_t get_offset_of_m_aryCoinIcon_Special_New_6() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryCoinIcon_Special_New_6)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryCoinIcon_Special_New_6() const { return ___m_aryCoinIcon_Special_New_6; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryCoinIcon_Special_New_6() { return &___m_aryCoinIcon_Special_New_6; }
	inline void set_m_aryCoinIcon_Special_New_6(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryCoinIcon_Special_New_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoinIcon_Special_New_6), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileParking_0_7() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileParking_0_7)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileParking_0_7() const { return ___m_aryAutomobileParking_0_7; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileParking_0_7() { return &___m_aryAutomobileParking_0_7; }
	inline void set_m_aryAutomobileParking_0_7(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileParking_0_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileParking_0_7), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileParking_1_8() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileParking_1_8)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileParking_1_8() const { return ___m_aryAutomobileParking_1_8; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileParking_1_8() { return &___m_aryAutomobileParking_1_8; }
	inline void set_m_aryAutomobileParking_1_8(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileParking_1_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileParking_1_8), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileParking_2_9() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileParking_2_9)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileParking_2_9() const { return ___m_aryAutomobileParking_2_9; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileParking_2_9() { return &___m_aryAutomobileParking_2_9; }
	inline void set_m_aryAutomobileParking_2_9(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileParking_2_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileParking_2_9), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileParking_3_10() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileParking_3_10)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileParking_3_10() const { return ___m_aryAutomobileParking_3_10; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileParking_3_10() { return &___m_aryAutomobileParking_3_10; }
	inline void set_m_aryAutomobileParking_3_10(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileParking_3_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileParking_3_10), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileParking_4_11() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileParking_4_11)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileParking_4_11() const { return ___m_aryAutomobileParking_4_11; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileParking_4_11() { return &___m_aryAutomobileParking_4_11; }
	inline void set_m_aryAutomobileParking_4_11(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileParking_4_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileParking_4_11), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileRunning_0_12() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileRunning_0_12)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileRunning_0_12() const { return ___m_aryAutomobileRunning_0_12; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileRunning_0_12() { return &___m_aryAutomobileRunning_0_12; }
	inline void set_m_aryAutomobileRunning_0_12(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileRunning_0_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileRunning_0_12), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileRunning_1_13() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileRunning_1_13)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileRunning_1_13() const { return ___m_aryAutomobileRunning_1_13; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileRunning_1_13() { return &___m_aryAutomobileRunning_1_13; }
	inline void set_m_aryAutomobileRunning_1_13(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileRunning_1_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileRunning_1_13), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileRunning_2_14() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileRunning_2_14)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileRunning_2_14() const { return ___m_aryAutomobileRunning_2_14; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileRunning_2_14() { return &___m_aryAutomobileRunning_2_14; }
	inline void set_m_aryAutomobileRunning_2_14(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileRunning_2_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileRunning_2_14), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileRunning_3_15() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileRunning_3_15)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileRunning_3_15() const { return ___m_aryAutomobileRunning_3_15; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileRunning_3_15() { return &___m_aryAutomobileRunning_3_15; }
	inline void set_m_aryAutomobileRunning_3_15(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileRunning_3_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileRunning_3_15), value);
	}

	inline static int32_t get_offset_of_m_aryAutomobileRunning_4_16() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryAutomobileRunning_4_16)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryAutomobileRunning_4_16() const { return ___m_aryAutomobileRunning_4_16; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryAutomobileRunning_4_16() { return &___m_aryAutomobileRunning_4_16; }
	inline void set_m_aryAutomobileRunning_4_16(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryAutomobileRunning_4_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryAutomobileRunning_4_16), value);
	}

	inline static int32_t get_offset_of_m_dicAutoSpritesParking_17() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_dicAutoSpritesParking_17)); }
	inline Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E * get_m_dicAutoSpritesParking_17() const { return ___m_dicAutoSpritesParking_17; }
	inline Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E ** get_address_of_m_dicAutoSpritesParking_17() { return &___m_dicAutoSpritesParking_17; }
	inline void set_m_dicAutoSpritesParking_17(Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E * value)
	{
		___m_dicAutoSpritesParking_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAutoSpritesParking_17), value);
	}

	inline static int32_t get_offset_of_m_dicAutoSpritesRunning_18() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_dicAutoSpritesRunning_18)); }
	inline Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E * get_m_dicAutoSpritesRunning_18() const { return ___m_dicAutoSpritesRunning_18; }
	inline Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E ** get_address_of_m_dicAutoSpritesRunning_18() { return &___m_dicAutoSpritesRunning_18; }
	inline void set_m_dicAutoSpritesRunning_18(Dictionary_2_tA081F7CB9715C6E2E1A9836A7C70323171E03D0E * value)
	{
		___m_dicAutoSpritesRunning_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicAutoSpritesRunning_18), value);
	}

	inline static int32_t get_offset_of__font_21() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ____font_21)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get__font_21() const { return ____font_21; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of__font_21() { return &____font_21; }
	inline void set__font_21(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		____font_21 = value;
		Il2CppCodeGenWriteBarrier((&____font_21), value);
	}

	inline static int32_t get_offset_of_m_preEnviromentMask_22() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preEnviromentMask_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preEnviromentMask_22() const { return ___m_preEnviromentMask_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preEnviromentMask_22() { return &___m_preEnviromentMask_22; }
	inline void set_m_preEnviromentMask_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preEnviromentMask_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_preEnviromentMask_22), value);
	}

	inline static int32_t get_offset_of_m_preUIShoppinAndItemCounter_23() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preUIShoppinAndItemCounter_23)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preUIShoppinAndItemCounter_23() const { return ___m_preUIShoppinAndItemCounter_23; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preUIShoppinAndItemCounter_23() { return &___m_preUIShoppinAndItemCounter_23; }
	inline void set_m_preUIShoppinAndItemCounter_23(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preUIShoppinAndItemCounter_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_preUIShoppinAndItemCounter_23), value);
	}

	inline static int32_t get_offset_of_m_aryPlanetAvatar_24() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryPlanetAvatar_24)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryPlanetAvatar_24() const { return ___m_aryPlanetAvatar_24; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryPlanetAvatar_24() { return &___m_aryPlanetAvatar_24; }
	inline void set_m_aryPlanetAvatar_24(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryPlanetAvatar_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPlanetAvatar_24), value);
	}

	inline static int32_t get_offset_of_m_sprArrow_25() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_sprArrow_25)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprArrow_25() const { return ___m_sprArrow_25; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprArrow_25() { return &___m_sprArrow_25; }
	inline void set_m_sprArrow_25(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprArrow_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprArrow_25), value);
	}

	inline static int32_t get_offset_of_m_arySkillPointIcon_26() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_arySkillPointIcon_26)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_arySkillPointIcon_26() const { return ___m_arySkillPointIcon_26; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_arySkillPointIcon_26() { return &___m_arySkillPointIcon_26; }
	inline void set_m_arySkillPointIcon_26(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_arySkillPointIcon_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySkillPointIcon_26), value);
	}

	inline static int32_t get_offset_of_m_aryCoinIcon_27() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryCoinIcon_27)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryCoinIcon_27() const { return ___m_aryCoinIcon_27; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryCoinIcon_27() { return &___m_aryCoinIcon_27; }
	inline void set_m_aryCoinIcon_27(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryCoinIcon_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoinIcon_27), value);
	}

	inline static int32_t get_offset_of_m_goRecycledPlanes_28() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_goRecycledPlanes_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goRecycledPlanes_28() const { return ___m_goRecycledPlanes_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goRecycledPlanes_28() { return &___m_goRecycledPlanes_28; }
	inline void set_m_goRecycledPlanes_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goRecycledPlanes_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_goRecycledPlanes_28), value);
	}

	inline static int32_t get_offset_of_m_aryPlaneSprites_29() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryPlaneSprites_29)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryPlaneSprites_29() const { return ___m_aryPlaneSprites_29; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryPlaneSprites_29() { return &___m_aryPlaneSprites_29; }
	inline void set_m_aryPlaneSprites_29(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryPlaneSprites_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryPlaneSprites_29), value);
	}

	inline static int32_t get_offset_of_m_aryParkingPlaneSprites_30() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryParkingPlaneSprites_30)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryParkingPlaneSprites_30() const { return ___m_aryParkingPlaneSprites_30; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryParkingPlaneSprites_30() { return &___m_aryParkingPlaneSprites_30; }
	inline void set_m_aryParkingPlaneSprites_30(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryParkingPlaneSprites_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryParkingPlaneSprites_30), value);
	}

	inline static int32_t get_offset_of_m_preZengShouCounter_31() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preZengShouCounter_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preZengShouCounter_31() const { return ___m_preZengShouCounter_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preZengShouCounter_31() { return &___m_preZengShouCounter_31; }
	inline void set_m_preZengShouCounter_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preZengShouCounter_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_preZengShouCounter_31), value);
	}

	inline static int32_t get_offset_of_m_prePlane_32() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_prePlane_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_prePlane_32() const { return ___m_prePlane_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_prePlane_32() { return &___m_prePlane_32; }
	inline void set_m_prePlane_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_prePlane_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_prePlane_32), value);
	}

	inline static int32_t get_offset_of_m_preJinBi_33() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preJinBi_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preJinBi_33() const { return ___m_preJinBi_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preJinBi_33() { return &___m_preJinBi_33; }
	inline void set_m_preJinBi_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preJinBi_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_preJinBi_33), value);
	}

	inline static int32_t get_offset_of_m_preRichTiaoZi_34() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preRichTiaoZi_34)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preRichTiaoZi_34() const { return ___m_preRichTiaoZi_34; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preRichTiaoZi_34() { return &___m_preRichTiaoZi_34; }
	inline void set_m_preRichTiaoZi_34(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preRichTiaoZi_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_preRichTiaoZi_34), value);
	}

	inline static int32_t get_offset_of_m_preVehicleCounter_35() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preVehicleCounter_35)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preVehicleCounter_35() const { return ___m_preVehicleCounter_35; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preVehicleCounter_35() { return &___m_preVehicleCounter_35; }
	inline void set_m_preVehicleCounter_35(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preVehicleCounter_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_preVehicleCounter_35), value);
	}

	inline static int32_t get_offset_of_m_aryTrailColor_36() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryTrailColor_36)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_m_aryTrailColor_36() const { return ___m_aryTrailColor_36; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_m_aryTrailColor_36() { return &___m_aryTrailColor_36; }
	inline void set_m_aryTrailColor_36(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___m_aryTrailColor_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryTrailColor_36), value);
	}

	inline static int32_t get_offset_of_m_preSkill_37() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preSkill_37)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preSkill_37() const { return ___m_preSkill_37; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preSkill_37() { return &___m_preSkill_37; }
	inline void set_m_preSkill_37(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preSkill_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_preSkill_37), value);
	}

	inline static int32_t get_offset_of_m_preFlyingCoin_38() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preFlyingCoin_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preFlyingCoin_38() const { return ___m_preFlyingCoin_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preFlyingCoin_38() { return &___m_preFlyingCoin_38; }
	inline void set_m_preFlyingCoin_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preFlyingCoin_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_preFlyingCoin_38), value);
	}

	inline static int32_t get_offset_of_m_preScienceTreeConfig_39() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preScienceTreeConfig_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preScienceTreeConfig_39() const { return ___m_preScienceTreeConfig_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preScienceTreeConfig_39() { return &___m_preScienceTreeConfig_39; }
	inline void set_m_preScienceTreeConfig_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preScienceTreeConfig_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_preScienceTreeConfig_39), value);
	}

	inline static int32_t get_offset_of_m_preScienceLeaf_40() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preScienceLeaf_40)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preScienceLeaf_40() const { return ___m_preScienceLeaf_40; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preScienceLeaf_40() { return &___m_preScienceLeaf_40; }
	inline void set_m_preScienceLeaf_40(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preScienceLeaf_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_preScienceLeaf_40), value);
	}

	inline static int32_t get_offset_of_m_aryItemIcon_41() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryItemIcon_41)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryItemIcon_41() const { return ___m_aryItemIcon_41; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryItemIcon_41() { return &___m_aryItemIcon_41; }
	inline void set_m_aryItemIcon_41(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryItemIcon_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryItemIcon_41), value);
	}

	inline static int32_t get_offset_of_m_aryCoinRaiseItemIcon_42() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryCoinRaiseItemIcon_42)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryCoinRaiseItemIcon_42() const { return ___m_aryCoinRaiseItemIcon_42; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryCoinRaiseItemIcon_42() { return &___m_aryCoinRaiseItemIcon_42; }
	inline void set_m_aryCoinRaiseItemIcon_42(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryCoinRaiseItemIcon_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoinRaiseItemIcon_42), value);
	}

	inline static int32_t get_offset_of_m_aryCoinDiamondItemIcon_43() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_aryCoinDiamondItemIcon_43)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_m_aryCoinDiamondItemIcon_43() const { return ___m_aryCoinDiamondItemIcon_43; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_m_aryCoinDiamondItemIcon_43() { return &___m_aryCoinDiamondItemIcon_43; }
	inline void set_m_aryCoinDiamondItemIcon_43(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___m_aryCoinDiamondItemIcon_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryCoinDiamondItemIcon_43), value);
	}

	inline static int32_t get_offset_of_m_sprWatchAdIcon_44() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_sprWatchAdIcon_44)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprWatchAdIcon_44() const { return ___m_sprWatchAdIcon_44; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprWatchAdIcon_44() { return &___m_sprWatchAdIcon_44; }
	inline void set_m_sprWatchAdIcon_44(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprWatchAdIcon_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprWatchAdIcon_44), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledPlanes_46() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_lstRecycledPlanes_46)); }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * get_m_lstRecycledPlanes_46() const { return ___m_lstRecycledPlanes_46; }
	inline List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 ** get_address_of_m_lstRecycledPlanes_46() { return &___m_lstRecycledPlanes_46; }
	inline void set_m_lstRecycledPlanes_46(List_1_t47760497A2262813AF37CFF6B714399A5DE17D22 * value)
	{
		___m_lstRecycledPlanes_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledPlanes_46), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledJinBi_47() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_lstRecycledJinBi_47)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_m_lstRecycledJinBi_47() const { return ___m_lstRecycledJinBi_47; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_m_lstRecycledJinBi_47() { return &___m_lstRecycledJinBi_47; }
	inline void set_m_lstRecycledJinBi_47(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___m_lstRecycledJinBi_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledJinBi_47), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledRichTiaoZi_48() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_lstRecycledRichTiaoZi_48)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_m_lstRecycledRichTiaoZi_48() const { return ___m_lstRecycledRichTiaoZi_48; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_m_lstRecycledRichTiaoZi_48() { return &___m_lstRecycledRichTiaoZi_48; }
	inline void set_m_lstRecycledRichTiaoZi_48(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___m_lstRecycledRichTiaoZi_48 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledRichTiaoZi_48), value);
	}

	inline static int32_t get_offset_of_m_preUiItem_49() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preUiItem_49)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preUiItem_49() const { return ___m_preUiItem_49; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preUiItem_49() { return &___m_preUiItem_49; }
	inline void set_m_preUiItem_49(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preUiItem_49 = value;
		Il2CppCodeGenWriteBarrier((&___m_preUiItem_49), value);
	}

	inline static int32_t get_offset_of_m_lstRecylcedUiItems_50() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_lstRecylcedUiItems_50)); }
	inline List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB * get_m_lstRecylcedUiItems_50() const { return ___m_lstRecylcedUiItems_50; }
	inline List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB ** get_address_of_m_lstRecylcedUiItems_50() { return &___m_lstRecylcedUiItems_50; }
	inline void set_m_lstRecylcedUiItems_50(List_1_tD70D201312B91DD24869D84B42069A85F6A07FDB * value)
	{
		___m_lstRecylcedUiItems_50 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecylcedUiItems_50), value);
	}

	inline static int32_t get_offset_of__containerRecycledUIItems_51() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ____containerRecycledUIItems_51)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerRecycledUIItems_51() const { return ____containerRecycledUIItems_51; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerRecycledUIItems_51() { return &____containerRecycledUIItems_51; }
	inline void set__containerRecycledUIItems_51(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerRecycledUIItems_51 = value;
		Il2CppCodeGenWriteBarrier((&____containerRecycledUIItems_51), value);
	}

	inline static int32_t get_offset_of_m_goRecycedVehicleCounter_53() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_goRecycedVehicleCounter_53)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goRecycedVehicleCounter_53() const { return ___m_goRecycedVehicleCounter_53; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goRecycedVehicleCounter_53() { return &___m_goRecycedVehicleCounter_53; }
	inline void set_m_goRecycedVehicleCounter_53(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goRecycedVehicleCounter_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_goRecycedVehicleCounter_53), value);
	}

	inline static int32_t get_offset_of_m_goRecycedResearchCounter_54() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_goRecycedResearchCounter_54)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goRecycedResearchCounter_54() const { return ___m_goRecycedResearchCounter_54; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goRecycedResearchCounter_54() { return &___m_goRecycedResearchCounter_54; }
	inline void set_m_goRecycedResearchCounter_54(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goRecycedResearchCounter_54 = value;
		Il2CppCodeGenWriteBarrier((&___m_goRecycedResearchCounter_54), value);
	}

	inline static int32_t get_offset_of_m_lstRecycledFlyingCoin_55() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_lstRecycledFlyingCoin_55)); }
	inline List_1_tB36688DD81A4ADEF07F2EB8C42C12B19153C0C95 * get_m_lstRecycledFlyingCoin_55() const { return ___m_lstRecycledFlyingCoin_55; }
	inline List_1_tB36688DD81A4ADEF07F2EB8C42C12B19153C0C95 ** get_address_of_m_lstRecycledFlyingCoin_55() { return &___m_lstRecycledFlyingCoin_55; }
	inline void set_m_lstRecycledFlyingCoin_55(List_1_tB36688DD81A4ADEF07F2EB8C42C12B19153C0C95 * value)
	{
		___m_lstRecycledFlyingCoin_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstRecycledFlyingCoin_55), value);
	}

	inline static int32_t get_offset_of_m_preTrail_56() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78, ___m_preTrail_56)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preTrail_56() const { return ___m_preTrail_56; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preTrail_56() { return &___m_preTrail_56; }
	inline void set_m_preTrail_56(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preTrail_56 = value;
		Il2CppCodeGenWriteBarrier((&___m_preTrail_56), value);
	}
};

struct ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields
{
public:
	// ResourceManager ResourceManager::s_Instance
	ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 * ___s_Instance_19;
	// UnityEngine.Vector3 ResourceManager::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_20;
	// System.Int32 ResourceManager::s_TrailColorIndex
	int32_t ___s_TrailColorIndex_45;

public:
	inline static int32_t get_offset_of_s_Instance_19() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields, ___s_Instance_19)); }
	inline ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 * get_s_Instance_19() const { return ___s_Instance_19; }
	inline ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 ** get_address_of_s_Instance_19() { return &___s_Instance_19; }
	inline void set_s_Instance_19(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 * value)
	{
		___s_Instance_19 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_19), value);
	}

	inline static int32_t get_offset_of_vecTempScale_20() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields, ___vecTempScale_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_20() const { return ___vecTempScale_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_20() { return &___vecTempScale_20; }
	inline void set_vecTempScale_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_20 = value;
	}

	inline static int32_t get_offset_of_s_TrailColorIndex_45() { return static_cast<int32_t>(offsetof(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields, ___s_TrailColorIndex_45)); }
	inline int32_t get_s_TrailColorIndex_45() const { return ___s_TrailColorIndex_45; }
	inline int32_t* get_address_of_s_TrailColorIndex_45() { return &___s_TrailColorIndex_45; }
	inline void set_s_TrailColorIndex_45(int32_t value)
	{
		___s_TrailColorIndex_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCEMANAGER_TCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_H
#ifndef SNOWFLAKE_T5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_H
#define SNOWFLAKE_T5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SnowFlake
struct  SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SpriteRenderer SnowFlake::_srMain
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srMain_7;
	// System.Single SnowFlake::m_fDropEndPosY
	float ___m_fDropEndPosY_8;
	// System.Single SnowFlake::m_fAlphaChangePerFrame
	float ___m_fAlphaChangePerFrame_9;
	// System.Single SnowFlake::m_fBeginFadePosY
	float ___m_fBeginFadePosY_10;
	// System.Single SnowFlake::m_fCurAngle
	float ___m_fCurAngle_11;
	// System.Single SnowFlake::m_fStartPosX
	float ___m_fStartPosX_12;
	// System.Single SnowFlake::m_fStartPosY
	float ___m_fStartPosY_13;

public:
	inline static int32_t get_offset_of__srMain_7() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ____srMain_7)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srMain_7() const { return ____srMain_7; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srMain_7() { return &____srMain_7; }
	inline void set__srMain_7(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srMain_7 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_7), value);
	}

	inline static int32_t get_offset_of_m_fDropEndPosY_8() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ___m_fDropEndPosY_8)); }
	inline float get_m_fDropEndPosY_8() const { return ___m_fDropEndPosY_8; }
	inline float* get_address_of_m_fDropEndPosY_8() { return &___m_fDropEndPosY_8; }
	inline void set_m_fDropEndPosY_8(float value)
	{
		___m_fDropEndPosY_8 = value;
	}

	inline static int32_t get_offset_of_m_fAlphaChangePerFrame_9() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ___m_fAlphaChangePerFrame_9)); }
	inline float get_m_fAlphaChangePerFrame_9() const { return ___m_fAlphaChangePerFrame_9; }
	inline float* get_address_of_m_fAlphaChangePerFrame_9() { return &___m_fAlphaChangePerFrame_9; }
	inline void set_m_fAlphaChangePerFrame_9(float value)
	{
		___m_fAlphaChangePerFrame_9 = value;
	}

	inline static int32_t get_offset_of_m_fBeginFadePosY_10() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ___m_fBeginFadePosY_10)); }
	inline float get_m_fBeginFadePosY_10() const { return ___m_fBeginFadePosY_10; }
	inline float* get_address_of_m_fBeginFadePosY_10() { return &___m_fBeginFadePosY_10; }
	inline void set_m_fBeginFadePosY_10(float value)
	{
		___m_fBeginFadePosY_10 = value;
	}

	inline static int32_t get_offset_of_m_fCurAngle_11() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ___m_fCurAngle_11)); }
	inline float get_m_fCurAngle_11() const { return ___m_fCurAngle_11; }
	inline float* get_address_of_m_fCurAngle_11() { return &___m_fCurAngle_11; }
	inline void set_m_fCurAngle_11(float value)
	{
		___m_fCurAngle_11 = value;
	}

	inline static int32_t get_offset_of_m_fStartPosX_12() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ___m_fStartPosX_12)); }
	inline float get_m_fStartPosX_12() const { return ___m_fStartPosX_12; }
	inline float* get_address_of_m_fStartPosX_12() { return &___m_fStartPosX_12; }
	inline void set_m_fStartPosX_12(float value)
	{
		___m_fStartPosX_12 = value;
	}

	inline static int32_t get_offset_of_m_fStartPosY_13() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ___m_fStartPosY_13)); }
	inline float get_m_fStartPosY_13() const { return ___m_fStartPosY_13; }
	inline float* get_address_of_m_fStartPosY_13() { return &___m_fStartPosY_13; }
	inline void set_m_fStartPosY_13(float value)
	{
		___m_fStartPosY_13 = value;
	}
};

struct SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_StaticFields
{
public:
	// UnityEngine.Vector3 SnowFlake::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_4;
	// UnityEngine.Vector3 SnowFlake::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_5;
	// UnityEngine.Color SnowFlake::colorTemp
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___colorTemp_6;

public:
	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_StaticFields, ___vecTempPos_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_StaticFields, ___vecTempScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_5 = value;
	}

	inline static int32_t get_offset_of_colorTemp_6() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_StaticFields, ___colorTemp_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_colorTemp_6() const { return ___colorTemp_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_colorTemp_6() { return &___colorTemp_6; }
	inline void set_colorTemp_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___colorTemp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SNOWFLAKE_T5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_H
#ifndef TANGECHE_T8AB5138CD649482ABE434C2571453BE2A5AF0455_H
#define TANGECHE_T8AB5138CD649482ABE434C2571453BE2A5AF0455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TanGeChe
struct  TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 TanGeChe::m_nFreshGuideStatus
	int32_t ___m_nFreshGuideStatus_4;
	// UnityEngine.UI.VerticalLayoutGroup TanGeChe::_vlg
	VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 * ____vlg_5;
	// UnityEngine.GameObject TanGeChe::_goFreeFlag
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goFreeFlag_6;
	// UnityEngine.GameObject TanGeChe::_goBtnTanGeChe
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goBtnTanGeChe_7;
	// UnityEngine.UI.Image TanGeChe::_imgPriceOff
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgPriceOff_8;
	// UnityEngine.Vector2 TanGeChe::m_vecPriceOffStartPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecPriceOffStartPos_9;
	// UnityEngine.Vector2 TanGeChe::m_vecPriceOffSummitPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecPriceOffSummitPos_10;
	// UnityEngine.Vector2 TanGeChe::m_vecPriceOffEndPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecPriceOffEndPos_11;
	// System.Single TanGeChe::m_fPriceOffAniTime
	float ___m_fPriceOffAniTime_12;
	// UnityEngine.Vector2 TanGeChe::m_vecPriceOffAniSpeed
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecPriceOffAniSpeed_13;
	// System.Single TanGeChe::m_fPriceOffAniA
	float ___m_fPriceOffAniA_14;
	// System.Single TanGeChe::m_fPriceOffAniV0
	float ___m_fPriceOffAniV0_15;
	// UnityEngine.UI.Text TanGeChe::_txtRecommendPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtRecommendPrice_19;
	// UnityEngine.UI.Image TanGeChe::_imgRecommendAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgRecommendAvatar_20;
	// UnityEngine.UI.Image TanGeChe::_imgRecommendCoinIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgRecommendCoinIcon_21;
	// UnityEngine.GameObject TanGeChe::_panelRecommendDetail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelRecommendDetail_22;
	// UnityEngine.UI.Text TanGeChe::_txtRecommendDetail
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtRecommendDetail_23;
	// UnityEngine.UI.Toggle TanGeChe::_toggleNoPlane
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ____toggleNoPlane_24;
	// UnityEngine.UI.Toggle TanGeChe::_toggleNoDrop
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ____toggleNoDrop_25;
	// UnityEngine.UI.InputField TanGeChe::_inputTrackLevel
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ____inputTrackLevel_26;
	// UnityEngine.GameObject TanGeChe::_goCounterList
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goCounterList_28;
	// System.Collections.Generic.List`1<UIVehicleCounter> TanGeChe::m_lstVehicleCounters
	List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * ___m_lstVehicleCounters_29;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UIVehicleCounter>> TanGeChe::m_dicVehicleCounters
	Dictionary_2_t23E80E3DB14B3FC8A2D8537F3FCAC9C8780F1654 * ___m_dicVehicleCounters_30;
	// UnityEngine.GameObject TanGeChe::_panelTanGeChe
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelTanGeChe_31;
	// System.Single TanGeChe::m_fRealTimeDiscount
	float ___m_fRealTimeDiscount_32;
	// UnityEngine.Sprite TanGeChe::m_sprJianYingTemp
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprJianYingTemp_33;
	// UnityEngine.Sprite TanGeChe::m_sprLockedBuyButton
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprLockedBuyButton_34;
	// UnityEngine.Sprite TanGeChe::m_spUnlockedBuyButton
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_spUnlockedBuyButton_35;
	// UnityEngine.Color TanGeChe::m_colorUnlocked
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorUnlocked_36;
	// UnityEngine.Color TanGeChe::m_colorLocked
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorLocked_37;
	// System.Int32 TanGeChe::m_nRecommendLowerAmount
	int32_t ___m_nRecommendLowerAmount_38;
	// System.Int32 TanGeChe::m_nRecommendHigherAmount
	int32_t ___m_nRecommendHigherAmount_39;
	// System.Collections.Generic.List`1<UIVehicleCounter> TanGeChe::lstTempShowingCounters
	List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * ___lstTempShowingCounters_40;
	// System.Collections.Generic.List`1<UIVehicleCounter> TanGeChe::lstTempShowingUnlockedCounters
	List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * ___lstTempShowingUnlockedCounters_41;
	// System.Collections.Generic.Dictionary`2<System.Int32,UIResearchCounterContainer> TanGeChe::m_dicShareUICountersContainer
	Dictionary_2_tC10E085FDFF6C3AB8005D16563E90234EF657A79 * ___m_dicShareUICountersContainer_42;
	// System.Int32 TanGeChe::m_nCurWatchAdsFreeCounterIndex
	int32_t ___m_nCurWatchAdsFreeCounterIndex_43;
	// System.Boolean TanGeChe::m_bIsVisible
	bool ___m_bIsVisible_44;
	// System.Single TanGeChe::m_fRandomEventInterval
	float ___m_fRandomEventInterval_45;
	// System.Single TanGeChe::m_fRandomEventTimeElapse
	float ___m_fRandomEventTimeElapse_46;
	// System.Boolean TanGeChe::m_bRandomEventProcessing
	bool ___m_bRandomEventProcessing_47;
	// System.Int32 TanGeChe::s_nRecommendCount
	int32_t ___s_nRecommendCount_48;
	// District/sLevelAndCost TanGeChe::selected_one
	sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA  ___selected_one_49;
	// System.Boolean TanGeChe::m_bNoPlane
	bool ___m_bNoPlane_50;
	// System.Boolean TanGeChe::m_bNoDrop
	bool ___m_bNoDrop_51;
	// System.Int32 TanGeChe::m_nPriceOffAnimationStatus
	int32_t ___m_nPriceOffAnimationStatus_52;

public:
	inline static int32_t get_offset_of_m_nFreshGuideStatus_4() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_nFreshGuideStatus_4)); }
	inline int32_t get_m_nFreshGuideStatus_4() const { return ___m_nFreshGuideStatus_4; }
	inline int32_t* get_address_of_m_nFreshGuideStatus_4() { return &___m_nFreshGuideStatus_4; }
	inline void set_m_nFreshGuideStatus_4(int32_t value)
	{
		___m_nFreshGuideStatus_4 = value;
	}

	inline static int32_t get_offset_of__vlg_5() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____vlg_5)); }
	inline VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 * get__vlg_5() const { return ____vlg_5; }
	inline VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 ** get_address_of__vlg_5() { return &____vlg_5; }
	inline void set__vlg_5(VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 * value)
	{
		____vlg_5 = value;
		Il2CppCodeGenWriteBarrier((&____vlg_5), value);
	}

	inline static int32_t get_offset_of__goFreeFlag_6() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____goFreeFlag_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goFreeFlag_6() const { return ____goFreeFlag_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goFreeFlag_6() { return &____goFreeFlag_6; }
	inline void set__goFreeFlag_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goFreeFlag_6 = value;
		Il2CppCodeGenWriteBarrier((&____goFreeFlag_6), value);
	}

	inline static int32_t get_offset_of__goBtnTanGeChe_7() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____goBtnTanGeChe_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goBtnTanGeChe_7() const { return ____goBtnTanGeChe_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goBtnTanGeChe_7() { return &____goBtnTanGeChe_7; }
	inline void set__goBtnTanGeChe_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goBtnTanGeChe_7 = value;
		Il2CppCodeGenWriteBarrier((&____goBtnTanGeChe_7), value);
	}

	inline static int32_t get_offset_of__imgPriceOff_8() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____imgPriceOff_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgPriceOff_8() const { return ____imgPriceOff_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgPriceOff_8() { return &____imgPriceOff_8; }
	inline void set__imgPriceOff_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgPriceOff_8 = value;
		Il2CppCodeGenWriteBarrier((&____imgPriceOff_8), value);
	}

	inline static int32_t get_offset_of_m_vecPriceOffStartPos_9() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_vecPriceOffStartPos_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecPriceOffStartPos_9() const { return ___m_vecPriceOffStartPos_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecPriceOffStartPos_9() { return &___m_vecPriceOffStartPos_9; }
	inline void set_m_vecPriceOffStartPos_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecPriceOffStartPos_9 = value;
	}

	inline static int32_t get_offset_of_m_vecPriceOffSummitPos_10() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_vecPriceOffSummitPos_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecPriceOffSummitPos_10() const { return ___m_vecPriceOffSummitPos_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecPriceOffSummitPos_10() { return &___m_vecPriceOffSummitPos_10; }
	inline void set_m_vecPriceOffSummitPos_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecPriceOffSummitPos_10 = value;
	}

	inline static int32_t get_offset_of_m_vecPriceOffEndPos_11() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_vecPriceOffEndPos_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecPriceOffEndPos_11() const { return ___m_vecPriceOffEndPos_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecPriceOffEndPos_11() { return &___m_vecPriceOffEndPos_11; }
	inline void set_m_vecPriceOffEndPos_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecPriceOffEndPos_11 = value;
	}

	inline static int32_t get_offset_of_m_fPriceOffAniTime_12() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_fPriceOffAniTime_12)); }
	inline float get_m_fPriceOffAniTime_12() const { return ___m_fPriceOffAniTime_12; }
	inline float* get_address_of_m_fPriceOffAniTime_12() { return &___m_fPriceOffAniTime_12; }
	inline void set_m_fPriceOffAniTime_12(float value)
	{
		___m_fPriceOffAniTime_12 = value;
	}

	inline static int32_t get_offset_of_m_vecPriceOffAniSpeed_13() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_vecPriceOffAniSpeed_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecPriceOffAniSpeed_13() const { return ___m_vecPriceOffAniSpeed_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecPriceOffAniSpeed_13() { return &___m_vecPriceOffAniSpeed_13; }
	inline void set_m_vecPriceOffAniSpeed_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecPriceOffAniSpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_fPriceOffAniA_14() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_fPriceOffAniA_14)); }
	inline float get_m_fPriceOffAniA_14() const { return ___m_fPriceOffAniA_14; }
	inline float* get_address_of_m_fPriceOffAniA_14() { return &___m_fPriceOffAniA_14; }
	inline void set_m_fPriceOffAniA_14(float value)
	{
		___m_fPriceOffAniA_14 = value;
	}

	inline static int32_t get_offset_of_m_fPriceOffAniV0_15() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_fPriceOffAniV0_15)); }
	inline float get_m_fPriceOffAniV0_15() const { return ___m_fPriceOffAniV0_15; }
	inline float* get_address_of_m_fPriceOffAniV0_15() { return &___m_fPriceOffAniV0_15; }
	inline void set_m_fPriceOffAniV0_15(float value)
	{
		___m_fPriceOffAniV0_15 = value;
	}

	inline static int32_t get_offset_of__txtRecommendPrice_19() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____txtRecommendPrice_19)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtRecommendPrice_19() const { return ____txtRecommendPrice_19; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtRecommendPrice_19() { return &____txtRecommendPrice_19; }
	inline void set__txtRecommendPrice_19(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtRecommendPrice_19 = value;
		Il2CppCodeGenWriteBarrier((&____txtRecommendPrice_19), value);
	}

	inline static int32_t get_offset_of__imgRecommendAvatar_20() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____imgRecommendAvatar_20)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgRecommendAvatar_20() const { return ____imgRecommendAvatar_20; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgRecommendAvatar_20() { return &____imgRecommendAvatar_20; }
	inline void set__imgRecommendAvatar_20(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgRecommendAvatar_20 = value;
		Il2CppCodeGenWriteBarrier((&____imgRecommendAvatar_20), value);
	}

	inline static int32_t get_offset_of__imgRecommendCoinIcon_21() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____imgRecommendCoinIcon_21)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgRecommendCoinIcon_21() const { return ____imgRecommendCoinIcon_21; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgRecommendCoinIcon_21() { return &____imgRecommendCoinIcon_21; }
	inline void set__imgRecommendCoinIcon_21(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgRecommendCoinIcon_21 = value;
		Il2CppCodeGenWriteBarrier((&____imgRecommendCoinIcon_21), value);
	}

	inline static int32_t get_offset_of__panelRecommendDetail_22() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____panelRecommendDetail_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelRecommendDetail_22() const { return ____panelRecommendDetail_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelRecommendDetail_22() { return &____panelRecommendDetail_22; }
	inline void set__panelRecommendDetail_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelRecommendDetail_22 = value;
		Il2CppCodeGenWriteBarrier((&____panelRecommendDetail_22), value);
	}

	inline static int32_t get_offset_of__txtRecommendDetail_23() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____txtRecommendDetail_23)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtRecommendDetail_23() const { return ____txtRecommendDetail_23; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtRecommendDetail_23() { return &____txtRecommendDetail_23; }
	inline void set__txtRecommendDetail_23(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtRecommendDetail_23 = value;
		Il2CppCodeGenWriteBarrier((&____txtRecommendDetail_23), value);
	}

	inline static int32_t get_offset_of__toggleNoPlane_24() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____toggleNoPlane_24)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get__toggleNoPlane_24() const { return ____toggleNoPlane_24; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of__toggleNoPlane_24() { return &____toggleNoPlane_24; }
	inline void set__toggleNoPlane_24(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		____toggleNoPlane_24 = value;
		Il2CppCodeGenWriteBarrier((&____toggleNoPlane_24), value);
	}

	inline static int32_t get_offset_of__toggleNoDrop_25() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____toggleNoDrop_25)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get__toggleNoDrop_25() const { return ____toggleNoDrop_25; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of__toggleNoDrop_25() { return &____toggleNoDrop_25; }
	inline void set__toggleNoDrop_25(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		____toggleNoDrop_25 = value;
		Il2CppCodeGenWriteBarrier((&____toggleNoDrop_25), value);
	}

	inline static int32_t get_offset_of__inputTrackLevel_26() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____inputTrackLevel_26)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get__inputTrackLevel_26() const { return ____inputTrackLevel_26; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of__inputTrackLevel_26() { return &____inputTrackLevel_26; }
	inline void set__inputTrackLevel_26(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		____inputTrackLevel_26 = value;
		Il2CppCodeGenWriteBarrier((&____inputTrackLevel_26), value);
	}

	inline static int32_t get_offset_of__goCounterList_28() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____goCounterList_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goCounterList_28() const { return ____goCounterList_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goCounterList_28() { return &____goCounterList_28; }
	inline void set__goCounterList_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goCounterList_28 = value;
		Il2CppCodeGenWriteBarrier((&____goCounterList_28), value);
	}

	inline static int32_t get_offset_of_m_lstVehicleCounters_29() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_lstVehicleCounters_29)); }
	inline List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * get_m_lstVehicleCounters_29() const { return ___m_lstVehicleCounters_29; }
	inline List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C ** get_address_of_m_lstVehicleCounters_29() { return &___m_lstVehicleCounters_29; }
	inline void set_m_lstVehicleCounters_29(List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * value)
	{
		___m_lstVehicleCounters_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstVehicleCounters_29), value);
	}

	inline static int32_t get_offset_of_m_dicVehicleCounters_30() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_dicVehicleCounters_30)); }
	inline Dictionary_2_t23E80E3DB14B3FC8A2D8537F3FCAC9C8780F1654 * get_m_dicVehicleCounters_30() const { return ___m_dicVehicleCounters_30; }
	inline Dictionary_2_t23E80E3DB14B3FC8A2D8537F3FCAC9C8780F1654 ** get_address_of_m_dicVehicleCounters_30() { return &___m_dicVehicleCounters_30; }
	inline void set_m_dicVehicleCounters_30(Dictionary_2_t23E80E3DB14B3FC8A2D8537F3FCAC9C8780F1654 * value)
	{
		___m_dicVehicleCounters_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicVehicleCounters_30), value);
	}

	inline static int32_t get_offset_of__panelTanGeChe_31() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ____panelTanGeChe_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelTanGeChe_31() const { return ____panelTanGeChe_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelTanGeChe_31() { return &____panelTanGeChe_31; }
	inline void set__panelTanGeChe_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelTanGeChe_31 = value;
		Il2CppCodeGenWriteBarrier((&____panelTanGeChe_31), value);
	}

	inline static int32_t get_offset_of_m_fRealTimeDiscount_32() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_fRealTimeDiscount_32)); }
	inline float get_m_fRealTimeDiscount_32() const { return ___m_fRealTimeDiscount_32; }
	inline float* get_address_of_m_fRealTimeDiscount_32() { return &___m_fRealTimeDiscount_32; }
	inline void set_m_fRealTimeDiscount_32(float value)
	{
		___m_fRealTimeDiscount_32 = value;
	}

	inline static int32_t get_offset_of_m_sprJianYingTemp_33() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_sprJianYingTemp_33)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprJianYingTemp_33() const { return ___m_sprJianYingTemp_33; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprJianYingTemp_33() { return &___m_sprJianYingTemp_33; }
	inline void set_m_sprJianYingTemp_33(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprJianYingTemp_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprJianYingTemp_33), value);
	}

	inline static int32_t get_offset_of_m_sprLockedBuyButton_34() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_sprLockedBuyButton_34)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprLockedBuyButton_34() const { return ___m_sprLockedBuyButton_34; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprLockedBuyButton_34() { return &___m_sprLockedBuyButton_34; }
	inline void set_m_sprLockedBuyButton_34(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprLockedBuyButton_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprLockedBuyButton_34), value);
	}

	inline static int32_t get_offset_of_m_spUnlockedBuyButton_35() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_spUnlockedBuyButton_35)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_spUnlockedBuyButton_35() const { return ___m_spUnlockedBuyButton_35; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_spUnlockedBuyButton_35() { return &___m_spUnlockedBuyButton_35; }
	inline void set_m_spUnlockedBuyButton_35(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_spUnlockedBuyButton_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_spUnlockedBuyButton_35), value);
	}

	inline static int32_t get_offset_of_m_colorUnlocked_36() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_colorUnlocked_36)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorUnlocked_36() const { return ___m_colorUnlocked_36; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorUnlocked_36() { return &___m_colorUnlocked_36; }
	inline void set_m_colorUnlocked_36(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorUnlocked_36 = value;
	}

	inline static int32_t get_offset_of_m_colorLocked_37() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_colorLocked_37)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorLocked_37() const { return ___m_colorLocked_37; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorLocked_37() { return &___m_colorLocked_37; }
	inline void set_m_colorLocked_37(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorLocked_37 = value;
	}

	inline static int32_t get_offset_of_m_nRecommendLowerAmount_38() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_nRecommendLowerAmount_38)); }
	inline int32_t get_m_nRecommendLowerAmount_38() const { return ___m_nRecommendLowerAmount_38; }
	inline int32_t* get_address_of_m_nRecommendLowerAmount_38() { return &___m_nRecommendLowerAmount_38; }
	inline void set_m_nRecommendLowerAmount_38(int32_t value)
	{
		___m_nRecommendLowerAmount_38 = value;
	}

	inline static int32_t get_offset_of_m_nRecommendHigherAmount_39() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_nRecommendHigherAmount_39)); }
	inline int32_t get_m_nRecommendHigherAmount_39() const { return ___m_nRecommendHigherAmount_39; }
	inline int32_t* get_address_of_m_nRecommendHigherAmount_39() { return &___m_nRecommendHigherAmount_39; }
	inline void set_m_nRecommendHigherAmount_39(int32_t value)
	{
		___m_nRecommendHigherAmount_39 = value;
	}

	inline static int32_t get_offset_of_lstTempShowingCounters_40() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___lstTempShowingCounters_40)); }
	inline List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * get_lstTempShowingCounters_40() const { return ___lstTempShowingCounters_40; }
	inline List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C ** get_address_of_lstTempShowingCounters_40() { return &___lstTempShowingCounters_40; }
	inline void set_lstTempShowingCounters_40(List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * value)
	{
		___lstTempShowingCounters_40 = value;
		Il2CppCodeGenWriteBarrier((&___lstTempShowingCounters_40), value);
	}

	inline static int32_t get_offset_of_lstTempShowingUnlockedCounters_41() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___lstTempShowingUnlockedCounters_41)); }
	inline List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * get_lstTempShowingUnlockedCounters_41() const { return ___lstTempShowingUnlockedCounters_41; }
	inline List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C ** get_address_of_lstTempShowingUnlockedCounters_41() { return &___lstTempShowingUnlockedCounters_41; }
	inline void set_lstTempShowingUnlockedCounters_41(List_1_tEC028986D5AC83E9D6B7CD26117BCCF63B08C54C * value)
	{
		___lstTempShowingUnlockedCounters_41 = value;
		Il2CppCodeGenWriteBarrier((&___lstTempShowingUnlockedCounters_41), value);
	}

	inline static int32_t get_offset_of_m_dicShareUICountersContainer_42() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_dicShareUICountersContainer_42)); }
	inline Dictionary_2_tC10E085FDFF6C3AB8005D16563E90234EF657A79 * get_m_dicShareUICountersContainer_42() const { return ___m_dicShareUICountersContainer_42; }
	inline Dictionary_2_tC10E085FDFF6C3AB8005D16563E90234EF657A79 ** get_address_of_m_dicShareUICountersContainer_42() { return &___m_dicShareUICountersContainer_42; }
	inline void set_m_dicShareUICountersContainer_42(Dictionary_2_tC10E085FDFF6C3AB8005D16563E90234EF657A79 * value)
	{
		___m_dicShareUICountersContainer_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_dicShareUICountersContainer_42), value);
	}

	inline static int32_t get_offset_of_m_nCurWatchAdsFreeCounterIndex_43() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_nCurWatchAdsFreeCounterIndex_43)); }
	inline int32_t get_m_nCurWatchAdsFreeCounterIndex_43() const { return ___m_nCurWatchAdsFreeCounterIndex_43; }
	inline int32_t* get_address_of_m_nCurWatchAdsFreeCounterIndex_43() { return &___m_nCurWatchAdsFreeCounterIndex_43; }
	inline void set_m_nCurWatchAdsFreeCounterIndex_43(int32_t value)
	{
		___m_nCurWatchAdsFreeCounterIndex_43 = value;
	}

	inline static int32_t get_offset_of_m_bIsVisible_44() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_bIsVisible_44)); }
	inline bool get_m_bIsVisible_44() const { return ___m_bIsVisible_44; }
	inline bool* get_address_of_m_bIsVisible_44() { return &___m_bIsVisible_44; }
	inline void set_m_bIsVisible_44(bool value)
	{
		___m_bIsVisible_44 = value;
	}

	inline static int32_t get_offset_of_m_fRandomEventInterval_45() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_fRandomEventInterval_45)); }
	inline float get_m_fRandomEventInterval_45() const { return ___m_fRandomEventInterval_45; }
	inline float* get_address_of_m_fRandomEventInterval_45() { return &___m_fRandomEventInterval_45; }
	inline void set_m_fRandomEventInterval_45(float value)
	{
		___m_fRandomEventInterval_45 = value;
	}

	inline static int32_t get_offset_of_m_fRandomEventTimeElapse_46() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_fRandomEventTimeElapse_46)); }
	inline float get_m_fRandomEventTimeElapse_46() const { return ___m_fRandomEventTimeElapse_46; }
	inline float* get_address_of_m_fRandomEventTimeElapse_46() { return &___m_fRandomEventTimeElapse_46; }
	inline void set_m_fRandomEventTimeElapse_46(float value)
	{
		___m_fRandomEventTimeElapse_46 = value;
	}

	inline static int32_t get_offset_of_m_bRandomEventProcessing_47() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_bRandomEventProcessing_47)); }
	inline bool get_m_bRandomEventProcessing_47() const { return ___m_bRandomEventProcessing_47; }
	inline bool* get_address_of_m_bRandomEventProcessing_47() { return &___m_bRandomEventProcessing_47; }
	inline void set_m_bRandomEventProcessing_47(bool value)
	{
		___m_bRandomEventProcessing_47 = value;
	}

	inline static int32_t get_offset_of_s_nRecommendCount_48() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___s_nRecommendCount_48)); }
	inline int32_t get_s_nRecommendCount_48() const { return ___s_nRecommendCount_48; }
	inline int32_t* get_address_of_s_nRecommendCount_48() { return &___s_nRecommendCount_48; }
	inline void set_s_nRecommendCount_48(int32_t value)
	{
		___s_nRecommendCount_48 = value;
	}

	inline static int32_t get_offset_of_selected_one_49() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___selected_one_49)); }
	inline sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA  get_selected_one_49() const { return ___selected_one_49; }
	inline sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA * get_address_of_selected_one_49() { return &___selected_one_49; }
	inline void set_selected_one_49(sLevelAndCost_t54392C7967F904A78CC7723B1259304E036E0EBA  value)
	{
		___selected_one_49 = value;
	}

	inline static int32_t get_offset_of_m_bNoPlane_50() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_bNoPlane_50)); }
	inline bool get_m_bNoPlane_50() const { return ___m_bNoPlane_50; }
	inline bool* get_address_of_m_bNoPlane_50() { return &___m_bNoPlane_50; }
	inline void set_m_bNoPlane_50(bool value)
	{
		___m_bNoPlane_50 = value;
	}

	inline static int32_t get_offset_of_m_bNoDrop_51() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_bNoDrop_51)); }
	inline bool get_m_bNoDrop_51() const { return ___m_bNoDrop_51; }
	inline bool* get_address_of_m_bNoDrop_51() { return &___m_bNoDrop_51; }
	inline void set_m_bNoDrop_51(bool value)
	{
		___m_bNoDrop_51 = value;
	}

	inline static int32_t get_offset_of_m_nPriceOffAnimationStatus_52() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455, ___m_nPriceOffAnimationStatus_52)); }
	inline int32_t get_m_nPriceOffAnimationStatus_52() const { return ___m_nPriceOffAnimationStatus_52; }
	inline int32_t* get_address_of_m_nPriceOffAnimationStatus_52() { return &___m_nPriceOffAnimationStatus_52; }
	inline void set_m_nPriceOffAnimationStatus_52(int32_t value)
	{
		___m_nPriceOffAnimationStatus_52 = value;
	}
};

struct TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields
{
public:
	// UnityEngine.Vector3 TanGeChe::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_16;
	// UnityEngine.Vector3 TanGeChe::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_17;
	// UnityEngine.Color TanGeChe::colorTemp
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___colorTemp_18;
	// TanGeChe TanGeChe::s_Instance
	TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 * ___s_Instance_27;

public:
	inline static int32_t get_offset_of_vecTempScale_16() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields, ___vecTempScale_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_16() const { return ___vecTempScale_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_16() { return &___vecTempScale_16; }
	inline void set_vecTempScale_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_16 = value;
	}

	inline static int32_t get_offset_of_vecTempPos_17() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields, ___vecTempPos_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_17() const { return ___vecTempPos_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_17() { return &___vecTempPos_17; }
	inline void set_vecTempPos_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_17 = value;
	}

	inline static int32_t get_offset_of_colorTemp_18() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields, ___colorTemp_18)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_colorTemp_18() const { return ___colorTemp_18; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_colorTemp_18() { return &___colorTemp_18; }
	inline void set_colorTemp_18(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___colorTemp_18 = value;
	}

	inline static int32_t get_offset_of_s_Instance_27() { return static_cast<int32_t>(offsetof(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields, ___s_Instance_27)); }
	inline TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 * get_s_Instance_27() const { return ___s_Instance_27; }
	inline TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 ** get_address_of_s_Instance_27() { return &___s_Instance_27; }
	inline void set_s_Instance_27(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 * value)
	{
		___s_Instance_27 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TANGECHE_T8AB5138CD649482ABE434C2571453BE2A5AF0455_H
#ifndef UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#define UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject UIManager::_goStart
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goStart_5;
	// UnityEngine.GameObject UIManager::_goStart_Admin
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goStart_Admin_6;
	// UnityEngine.GameObject UIManager::_goEnd
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goEnd_7;
	// UnityEngine.GameObject UIManager::_goMiddleLeft
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goMiddleLeft_8;
	// UnityEngine.GameObject UIManager::_goMiddleRight
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goMiddleRight_9;
	// UnityEngine.GameObject[] UIManager::m_aryVariousUI
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryVariousUI_10;
	// UnityEngine.GameObject[] UIManager::m_aryVariousUI_Center
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryVariousUI_Center_11;
	// UnityEngine.GameObject[] UIManager::m_aryVariousUI_Bottom
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryVariousUI_Bottom_12;
	// UnityEngine.Vector3 UIManager::vecTempInitPosOfTopButtons
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempInitPosOfTopButtons_13;
	// UnityEngine.Vector3 UIManager::m_vecScreenBottom_World
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecScreenBottom_World_17;
	// UnityEngine.Vector3 UIManager::m_vecBottomButtonsInitPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecBottomButtonsInitPos_18;
	// UnityEngine.Vector3 UIManager::m_vecGainAndCollect
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecGainAndCollect_19;
	// UnityEngine.Vector3 UIManager::m_vecAdventureAndScience
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecAdventureAndScience_20;
	// UnityEngine.Vector3 UIManager::m_vecSubContainerForBigMap
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecSubContainerForBigMap_21;
	// System.Single UIManager::m_fOffsetToScreenBottom_World
	float ___m_fOffsetToScreenBottom_World_22;
	// UnityEngine.GameObject UIManager::_uiTestKnob
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiTestKnob_23;
	// UnityEngine.GameObject UIManager::_uiTopButtons
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiTopButtons_24;
	// UnityEngine.GameObject UIManager::_uiBottomButtons
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiBottomButtons_25;
	// UnityEngine.GameObject UIManager::_uiDPS
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiDPS_26;
	// UnityEngine.GameObject UIManager::_uiCPosButtons
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiCPosButtons_27;
	// UnityEngine.GameObject UIManager::_uiGainAndCollect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiGainAndCollect_28;
	// UnityEngine.GameObject UIManager::_uiAdventureAndScience
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiAdventureAndScience_29;
	// UnityEngine.GameObject UIManager::_uiSubContainerForBigMap
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiSubContainerForBigMap_30;
	// UnityEngine.GameObject UIManager::_uiSubContainerForBigMap_Content
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiSubContainerForBigMap_Content_31;
	// UnityEngine.Canvas UIManager::_canvasMain
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ____canvasMain_32;
	// UnityEngine.GameObject UIManager::_UI
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____UI_33;
	// UnityEngine.UI.CanvasScaler UIManager::_canvasScaler
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564 * ____canvasScaler_34;
	// UnityEngine.Vector2 UIManager::m_vecCoinFlySeesion0EndPosRangeX
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecCoinFlySeesion0EndPosRangeX_35;
	// UnityEngine.Vector2 UIManager::m_vecCoinFlySeesion0EndPosRangeY
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecCoinFlySeesion0EndPosRangeY_36;
	// UnityEngine.Vector2 UIManager::m_vecCoinFlyStartPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecCoinFlyStartPos_37;
	// UnityEngine.Vector2 UIManager::m_vecCoinFlyEndPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecCoinFlyEndPos_38;
	// System.Single UIManager::m_fSession0Time
	float ___m_fSession0Time_39;
	// System.Single UIManager::m_fSession1Time
	float ___m_fSession1Time_40;
	// UnityEngine.GameObject UIManager::_containerFlyingCoins
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerFlyingCoins_41;
	// UnityEngine.GameObject[] UIManager::m_arySomeCtrlsDueToXingKong
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_arySomeCtrlsDueToXingKong_42;
	// CFrameAnimationEffect UIManager::_goEffectClick
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * ____goEffectClick_43;
	// UnityEngine.Vector2[] UIManager::m_aryUICtrlPos_1920_1080
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_aryUICtrlPos_1920_1080_44;
	// UnityEngine.Vector2[] UIManager::m_aryUICtrlPos_2436_1125
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_aryUICtrlPos_2436_1125_45;
	// UnityEngine.GameObject[] UIManager::m_aryUICtrl
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryUICtrl_46;
	// UnityEngine.Vector2[] UIManager::m_aryResolution
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_aryResolution_47;

public:
	inline static int32_t get_offset_of__goStart_5() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____goStart_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goStart_5() const { return ____goStart_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goStart_5() { return &____goStart_5; }
	inline void set__goStart_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goStart_5 = value;
		Il2CppCodeGenWriteBarrier((&____goStart_5), value);
	}

	inline static int32_t get_offset_of__goStart_Admin_6() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____goStart_Admin_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goStart_Admin_6() const { return ____goStart_Admin_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goStart_Admin_6() { return &____goStart_Admin_6; }
	inline void set__goStart_Admin_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goStart_Admin_6 = value;
		Il2CppCodeGenWriteBarrier((&____goStart_Admin_6), value);
	}

	inline static int32_t get_offset_of__goEnd_7() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____goEnd_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goEnd_7() const { return ____goEnd_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goEnd_7() { return &____goEnd_7; }
	inline void set__goEnd_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goEnd_7 = value;
		Il2CppCodeGenWriteBarrier((&____goEnd_7), value);
	}

	inline static int32_t get_offset_of__goMiddleLeft_8() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____goMiddleLeft_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goMiddleLeft_8() const { return ____goMiddleLeft_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goMiddleLeft_8() { return &____goMiddleLeft_8; }
	inline void set__goMiddleLeft_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goMiddleLeft_8 = value;
		Il2CppCodeGenWriteBarrier((&____goMiddleLeft_8), value);
	}

	inline static int32_t get_offset_of__goMiddleRight_9() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____goMiddleRight_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goMiddleRight_9() const { return ____goMiddleRight_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goMiddleRight_9() { return &____goMiddleRight_9; }
	inline void set__goMiddleRight_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goMiddleRight_9 = value;
		Il2CppCodeGenWriteBarrier((&____goMiddleRight_9), value);
	}

	inline static int32_t get_offset_of_m_aryVariousUI_10() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryVariousUI_10)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryVariousUI_10() const { return ___m_aryVariousUI_10; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryVariousUI_10() { return &___m_aryVariousUI_10; }
	inline void set_m_aryVariousUI_10(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryVariousUI_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryVariousUI_10), value);
	}

	inline static int32_t get_offset_of_m_aryVariousUI_Center_11() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryVariousUI_Center_11)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryVariousUI_Center_11() const { return ___m_aryVariousUI_Center_11; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryVariousUI_Center_11() { return &___m_aryVariousUI_Center_11; }
	inline void set_m_aryVariousUI_Center_11(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryVariousUI_Center_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryVariousUI_Center_11), value);
	}

	inline static int32_t get_offset_of_m_aryVariousUI_Bottom_12() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryVariousUI_Bottom_12)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryVariousUI_Bottom_12() const { return ___m_aryVariousUI_Bottom_12; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryVariousUI_Bottom_12() { return &___m_aryVariousUI_Bottom_12; }
	inline void set_m_aryVariousUI_Bottom_12(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryVariousUI_Bottom_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryVariousUI_Bottom_12), value);
	}

	inline static int32_t get_offset_of_vecTempInitPosOfTopButtons_13() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___vecTempInitPosOfTopButtons_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempInitPosOfTopButtons_13() const { return ___vecTempInitPosOfTopButtons_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempInitPosOfTopButtons_13() { return &___vecTempInitPosOfTopButtons_13; }
	inline void set_vecTempInitPosOfTopButtons_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempInitPosOfTopButtons_13 = value;
	}

	inline static int32_t get_offset_of_m_vecScreenBottom_World_17() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecScreenBottom_World_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecScreenBottom_World_17() const { return ___m_vecScreenBottom_World_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecScreenBottom_World_17() { return &___m_vecScreenBottom_World_17; }
	inline void set_m_vecScreenBottom_World_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecScreenBottom_World_17 = value;
	}

	inline static int32_t get_offset_of_m_vecBottomButtonsInitPos_18() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecBottomButtonsInitPos_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecBottomButtonsInitPos_18() const { return ___m_vecBottomButtonsInitPos_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecBottomButtonsInitPos_18() { return &___m_vecBottomButtonsInitPos_18; }
	inline void set_m_vecBottomButtonsInitPos_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecBottomButtonsInitPos_18 = value;
	}

	inline static int32_t get_offset_of_m_vecGainAndCollect_19() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecGainAndCollect_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecGainAndCollect_19() const { return ___m_vecGainAndCollect_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecGainAndCollect_19() { return &___m_vecGainAndCollect_19; }
	inline void set_m_vecGainAndCollect_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecGainAndCollect_19 = value;
	}

	inline static int32_t get_offset_of_m_vecAdventureAndScience_20() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecAdventureAndScience_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecAdventureAndScience_20() const { return ___m_vecAdventureAndScience_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecAdventureAndScience_20() { return &___m_vecAdventureAndScience_20; }
	inline void set_m_vecAdventureAndScience_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecAdventureAndScience_20 = value;
	}

	inline static int32_t get_offset_of_m_vecSubContainerForBigMap_21() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecSubContainerForBigMap_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecSubContainerForBigMap_21() const { return ___m_vecSubContainerForBigMap_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecSubContainerForBigMap_21() { return &___m_vecSubContainerForBigMap_21; }
	inline void set_m_vecSubContainerForBigMap_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecSubContainerForBigMap_21 = value;
	}

	inline static int32_t get_offset_of_m_fOffsetToScreenBottom_World_22() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_fOffsetToScreenBottom_World_22)); }
	inline float get_m_fOffsetToScreenBottom_World_22() const { return ___m_fOffsetToScreenBottom_World_22; }
	inline float* get_address_of_m_fOffsetToScreenBottom_World_22() { return &___m_fOffsetToScreenBottom_World_22; }
	inline void set_m_fOffsetToScreenBottom_World_22(float value)
	{
		___m_fOffsetToScreenBottom_World_22 = value;
	}

	inline static int32_t get_offset_of__uiTestKnob_23() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiTestKnob_23)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiTestKnob_23() const { return ____uiTestKnob_23; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiTestKnob_23() { return &____uiTestKnob_23; }
	inline void set__uiTestKnob_23(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiTestKnob_23 = value;
		Il2CppCodeGenWriteBarrier((&____uiTestKnob_23), value);
	}

	inline static int32_t get_offset_of__uiTopButtons_24() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiTopButtons_24)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiTopButtons_24() const { return ____uiTopButtons_24; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiTopButtons_24() { return &____uiTopButtons_24; }
	inline void set__uiTopButtons_24(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiTopButtons_24 = value;
		Il2CppCodeGenWriteBarrier((&____uiTopButtons_24), value);
	}

	inline static int32_t get_offset_of__uiBottomButtons_25() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiBottomButtons_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiBottomButtons_25() const { return ____uiBottomButtons_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiBottomButtons_25() { return &____uiBottomButtons_25; }
	inline void set__uiBottomButtons_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiBottomButtons_25 = value;
		Il2CppCodeGenWriteBarrier((&____uiBottomButtons_25), value);
	}

	inline static int32_t get_offset_of__uiDPS_26() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiDPS_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiDPS_26() const { return ____uiDPS_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiDPS_26() { return &____uiDPS_26; }
	inline void set__uiDPS_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiDPS_26 = value;
		Il2CppCodeGenWriteBarrier((&____uiDPS_26), value);
	}

	inline static int32_t get_offset_of__uiCPosButtons_27() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiCPosButtons_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiCPosButtons_27() const { return ____uiCPosButtons_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiCPosButtons_27() { return &____uiCPosButtons_27; }
	inline void set__uiCPosButtons_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiCPosButtons_27 = value;
		Il2CppCodeGenWriteBarrier((&____uiCPosButtons_27), value);
	}

	inline static int32_t get_offset_of__uiGainAndCollect_28() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiGainAndCollect_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiGainAndCollect_28() const { return ____uiGainAndCollect_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiGainAndCollect_28() { return &____uiGainAndCollect_28; }
	inline void set__uiGainAndCollect_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiGainAndCollect_28 = value;
		Il2CppCodeGenWriteBarrier((&____uiGainAndCollect_28), value);
	}

	inline static int32_t get_offset_of__uiAdventureAndScience_29() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiAdventureAndScience_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiAdventureAndScience_29() const { return ____uiAdventureAndScience_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiAdventureAndScience_29() { return &____uiAdventureAndScience_29; }
	inline void set__uiAdventureAndScience_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiAdventureAndScience_29 = value;
		Il2CppCodeGenWriteBarrier((&____uiAdventureAndScience_29), value);
	}

	inline static int32_t get_offset_of__uiSubContainerForBigMap_30() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiSubContainerForBigMap_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiSubContainerForBigMap_30() const { return ____uiSubContainerForBigMap_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiSubContainerForBigMap_30() { return &____uiSubContainerForBigMap_30; }
	inline void set__uiSubContainerForBigMap_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiSubContainerForBigMap_30 = value;
		Il2CppCodeGenWriteBarrier((&____uiSubContainerForBigMap_30), value);
	}

	inline static int32_t get_offset_of__uiSubContainerForBigMap_Content_31() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiSubContainerForBigMap_Content_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiSubContainerForBigMap_Content_31() const { return ____uiSubContainerForBigMap_Content_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiSubContainerForBigMap_Content_31() { return &____uiSubContainerForBigMap_Content_31; }
	inline void set__uiSubContainerForBigMap_Content_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiSubContainerForBigMap_Content_31 = value;
		Il2CppCodeGenWriteBarrier((&____uiSubContainerForBigMap_Content_31), value);
	}

	inline static int32_t get_offset_of__canvasMain_32() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____canvasMain_32)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get__canvasMain_32() const { return ____canvasMain_32; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of__canvasMain_32() { return &____canvasMain_32; }
	inline void set__canvasMain_32(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		____canvasMain_32 = value;
		Il2CppCodeGenWriteBarrier((&____canvasMain_32), value);
	}

	inline static int32_t get_offset_of__UI_33() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____UI_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__UI_33() const { return ____UI_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__UI_33() { return &____UI_33; }
	inline void set__UI_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____UI_33 = value;
		Il2CppCodeGenWriteBarrier((&____UI_33), value);
	}

	inline static int32_t get_offset_of__canvasScaler_34() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____canvasScaler_34)); }
	inline CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564 * get__canvasScaler_34() const { return ____canvasScaler_34; }
	inline CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564 ** get_address_of__canvasScaler_34() { return &____canvasScaler_34; }
	inline void set__canvasScaler_34(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564 * value)
	{
		____canvasScaler_34 = value;
		Il2CppCodeGenWriteBarrier((&____canvasScaler_34), value);
	}

	inline static int32_t get_offset_of_m_vecCoinFlySeesion0EndPosRangeX_35() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecCoinFlySeesion0EndPosRangeX_35)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecCoinFlySeesion0EndPosRangeX_35() const { return ___m_vecCoinFlySeesion0EndPosRangeX_35; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecCoinFlySeesion0EndPosRangeX_35() { return &___m_vecCoinFlySeesion0EndPosRangeX_35; }
	inline void set_m_vecCoinFlySeesion0EndPosRangeX_35(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecCoinFlySeesion0EndPosRangeX_35 = value;
	}

	inline static int32_t get_offset_of_m_vecCoinFlySeesion0EndPosRangeY_36() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecCoinFlySeesion0EndPosRangeY_36)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecCoinFlySeesion0EndPosRangeY_36() const { return ___m_vecCoinFlySeesion0EndPosRangeY_36; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecCoinFlySeesion0EndPosRangeY_36() { return &___m_vecCoinFlySeesion0EndPosRangeY_36; }
	inline void set_m_vecCoinFlySeesion0EndPosRangeY_36(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecCoinFlySeesion0EndPosRangeY_36 = value;
	}

	inline static int32_t get_offset_of_m_vecCoinFlyStartPos_37() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecCoinFlyStartPos_37)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecCoinFlyStartPos_37() const { return ___m_vecCoinFlyStartPos_37; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecCoinFlyStartPos_37() { return &___m_vecCoinFlyStartPos_37; }
	inline void set_m_vecCoinFlyStartPos_37(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecCoinFlyStartPos_37 = value;
	}

	inline static int32_t get_offset_of_m_vecCoinFlyEndPos_38() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecCoinFlyEndPos_38)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecCoinFlyEndPos_38() const { return ___m_vecCoinFlyEndPos_38; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecCoinFlyEndPos_38() { return &___m_vecCoinFlyEndPos_38; }
	inline void set_m_vecCoinFlyEndPos_38(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecCoinFlyEndPos_38 = value;
	}

	inline static int32_t get_offset_of_m_fSession0Time_39() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_fSession0Time_39)); }
	inline float get_m_fSession0Time_39() const { return ___m_fSession0Time_39; }
	inline float* get_address_of_m_fSession0Time_39() { return &___m_fSession0Time_39; }
	inline void set_m_fSession0Time_39(float value)
	{
		___m_fSession0Time_39 = value;
	}

	inline static int32_t get_offset_of_m_fSession1Time_40() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_fSession1Time_40)); }
	inline float get_m_fSession1Time_40() const { return ___m_fSession1Time_40; }
	inline float* get_address_of_m_fSession1Time_40() { return &___m_fSession1Time_40; }
	inline void set_m_fSession1Time_40(float value)
	{
		___m_fSession1Time_40 = value;
	}

	inline static int32_t get_offset_of__containerFlyingCoins_41() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____containerFlyingCoins_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerFlyingCoins_41() const { return ____containerFlyingCoins_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerFlyingCoins_41() { return &____containerFlyingCoins_41; }
	inline void set__containerFlyingCoins_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerFlyingCoins_41 = value;
		Il2CppCodeGenWriteBarrier((&____containerFlyingCoins_41), value);
	}

	inline static int32_t get_offset_of_m_arySomeCtrlsDueToXingKong_42() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_arySomeCtrlsDueToXingKong_42)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_arySomeCtrlsDueToXingKong_42() const { return ___m_arySomeCtrlsDueToXingKong_42; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_arySomeCtrlsDueToXingKong_42() { return &___m_arySomeCtrlsDueToXingKong_42; }
	inline void set_m_arySomeCtrlsDueToXingKong_42(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_arySomeCtrlsDueToXingKong_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySomeCtrlsDueToXingKong_42), value);
	}

	inline static int32_t get_offset_of__goEffectClick_43() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____goEffectClick_43)); }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * get__goEffectClick_43() const { return ____goEffectClick_43; }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 ** get_address_of__goEffectClick_43() { return &____goEffectClick_43; }
	inline void set__goEffectClick_43(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * value)
	{
		____goEffectClick_43 = value;
		Il2CppCodeGenWriteBarrier((&____goEffectClick_43), value);
	}

	inline static int32_t get_offset_of_m_aryUICtrlPos_1920_1080_44() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryUICtrlPos_1920_1080_44)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_aryUICtrlPos_1920_1080_44() const { return ___m_aryUICtrlPos_1920_1080_44; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_aryUICtrlPos_1920_1080_44() { return &___m_aryUICtrlPos_1920_1080_44; }
	inline void set_m_aryUICtrlPos_1920_1080_44(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_aryUICtrlPos_1920_1080_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUICtrlPos_1920_1080_44), value);
	}

	inline static int32_t get_offset_of_m_aryUICtrlPos_2436_1125_45() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryUICtrlPos_2436_1125_45)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_aryUICtrlPos_2436_1125_45() const { return ___m_aryUICtrlPos_2436_1125_45; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_aryUICtrlPos_2436_1125_45() { return &___m_aryUICtrlPos_2436_1125_45; }
	inline void set_m_aryUICtrlPos_2436_1125_45(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_aryUICtrlPos_2436_1125_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUICtrlPos_2436_1125_45), value);
	}

	inline static int32_t get_offset_of_m_aryUICtrl_46() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryUICtrl_46)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryUICtrl_46() const { return ___m_aryUICtrl_46; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryUICtrl_46() { return &___m_aryUICtrl_46; }
	inline void set_m_aryUICtrl_46(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryUICtrl_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUICtrl_46), value);
	}

	inline static int32_t get_offset_of_m_aryResolution_47() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryResolution_47)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_aryResolution_47() const { return ___m_aryResolution_47; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_aryResolution_47() { return &___m_aryResolution_47; }
	inline void set_m_aryResolution_47(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_aryResolution_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryResolution_47), value);
	}
};

struct UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields
{
public:
	// UIManager UIManager::s_Instance
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * ___s_Instance_4;
	// UnityEngine.Vector3 UIManager::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_15;
	// UnityEngine.Vector3 UIManager::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_16;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields, ___s_Instance_4)); }
	inline UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * get_s_Instance_4() const { return ___s_Instance_4; }
	inline UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_vecTempPos_15() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields, ___vecTempPos_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_15() const { return ___vecTempPos_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_15() { return &___vecTempPos_15; }
	inline void set_vecTempPos_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_15 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_16() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields, ___vecTempScale_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_16() const { return ___vecTempScale_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_16() { return &___vecTempScale_16; }
	inline void set_vecTempScale_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#ifndef UIZENGSHOUCOUNTER_T35FB691FBD7229E775905E79FA0CA9E20992CA95_H
#define UIZENGSHOUCOUNTER_T35FB691FBD7229E775905E79FA0CA9E20992CA95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIZengShouCounter
struct  UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text UIZengShouCounter::_txtPlanetAndDistrict
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPlanetAndDistrict_4;
	// UnityEngine.UI.Image UIZengShouCounter::_imgPlanetAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgPlanetAvatar_5;
	// UnityEngine.UI.Text UIZengShouCounter::_txtPrestigeRaise
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrestigeRaise_6;
	// UnityEngine.UI.Text UIZengShouCounter::_txtDistrictRaise
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDistrictRaise_7;
	// UnityEngine.UI.Image UIZengShouCounter::_imgCoinIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCoinIcon_8;
	// UnityEngine.UI.Text UIZengShouCounter::_txtOfflineDps
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtOfflineDps_9;
	// UnityEngine.UI.Text UIZengShouCounter::_txtCurPrestigeTimes
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurPrestigeTimes_10;
	// System.Int32 UIZengShouCounter::m_nPlanetId
	int32_t ___m_nPlanetId_11;
	// System.Int32 UIZengShouCounter::m_nDistrictId
	int32_t ___m_nDistrictId_12;

public:
	inline static int32_t get_offset_of__txtPlanetAndDistrict_4() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____txtPlanetAndDistrict_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPlanetAndDistrict_4() const { return ____txtPlanetAndDistrict_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPlanetAndDistrict_4() { return &____txtPlanetAndDistrict_4; }
	inline void set__txtPlanetAndDistrict_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPlanetAndDistrict_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlanetAndDistrict_4), value);
	}

	inline static int32_t get_offset_of__imgPlanetAvatar_5() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____imgPlanetAvatar_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgPlanetAvatar_5() const { return ____imgPlanetAvatar_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgPlanetAvatar_5() { return &____imgPlanetAvatar_5; }
	inline void set__imgPlanetAvatar_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgPlanetAvatar_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgPlanetAvatar_5), value);
	}

	inline static int32_t get_offset_of__txtPrestigeRaise_6() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____txtPrestigeRaise_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrestigeRaise_6() const { return ____txtPrestigeRaise_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrestigeRaise_6() { return &____txtPrestigeRaise_6; }
	inline void set__txtPrestigeRaise_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrestigeRaise_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrestigeRaise_6), value);
	}

	inline static int32_t get_offset_of__txtDistrictRaise_7() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____txtDistrictRaise_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDistrictRaise_7() const { return ____txtDistrictRaise_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDistrictRaise_7() { return &____txtDistrictRaise_7; }
	inline void set__txtDistrictRaise_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDistrictRaise_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtDistrictRaise_7), value);
	}

	inline static int32_t get_offset_of__imgCoinIcon_8() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____imgCoinIcon_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCoinIcon_8() const { return ____imgCoinIcon_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCoinIcon_8() { return &____imgCoinIcon_8; }
	inline void set__imgCoinIcon_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCoinIcon_8 = value;
		Il2CppCodeGenWriteBarrier((&____imgCoinIcon_8), value);
	}

	inline static int32_t get_offset_of__txtOfflineDps_9() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____txtOfflineDps_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtOfflineDps_9() const { return ____txtOfflineDps_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtOfflineDps_9() { return &____txtOfflineDps_9; }
	inline void set__txtOfflineDps_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtOfflineDps_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtOfflineDps_9), value);
	}

	inline static int32_t get_offset_of__txtCurPrestigeTimes_10() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____txtCurPrestigeTimes_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurPrestigeTimes_10() const { return ____txtCurPrestigeTimes_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurPrestigeTimes_10() { return &____txtCurPrestigeTimes_10; }
	inline void set__txtCurPrestigeTimes_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurPrestigeTimes_10 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurPrestigeTimes_10), value);
	}

	inline static int32_t get_offset_of_m_nPlanetId_11() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ___m_nPlanetId_11)); }
	inline int32_t get_m_nPlanetId_11() const { return ___m_nPlanetId_11; }
	inline int32_t* get_address_of_m_nPlanetId_11() { return &___m_nPlanetId_11; }
	inline void set_m_nPlanetId_11(int32_t value)
	{
		___m_nPlanetId_11 = value;
	}

	inline static int32_t get_offset_of_m_nDistrictId_12() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ___m_nDistrictId_12)); }
	inline int32_t get_m_nDistrictId_12() const { return ___m_nDistrictId_12; }
	inline int32_t* get_address_of_m_nDistrictId_12() { return &___m_nDistrictId_12; }
	inline void set_m_nDistrictId_12(int32_t value)
	{
		___m_nDistrictId_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIZENGSHOUCOUNTER_T35FB691FBD7229E775905E79FA0CA9E20992CA95_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef UNLOCKNEWPLANEMANAGER_T402DEC9E426B680014E232E712D172F77F23341B_H
#define UNLOCKNEWPLANEMANAGER_T402DEC9E426B680014E232E712D172F77F23341B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnlockNewPlaneManager
struct  UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text UnlockNewPlaneManager::_txtTapToExit
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTapToExit_5;
	// UnityEngine.UI.Button UnlockNewPlaneManager::_btnClose
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnClose_6;
	// Spine.Unity.SkeletonGraphic UnlockNewPlaneManager::_skeTwoAuto
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * ____skeTwoAuto_7;
	// Spine.Unity.SkeletonGraphic UnlockNewPlaneManager::_skeNewAuto
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * ____skeNewAuto_8;
	// Spine.Unity.SkeletonGraphic UnlockNewPlaneManager::_skeEffect1
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * ____skeEffect1_9;
	// Spine.Unity.SkeletonGraphic UnlockNewPlaneManager::_skeEffect2
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * ____skeEffect2_10;
	// UnityEngine.GameObject UnlockNewPlaneManager::m_containerEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerEffect_11;
	// UnityEngine.GameObject UnlockNewPlaneManager::m_containerLighning
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerLighning_12;
	// UnityEngine.GameObject UnlockNewPlaneManager::m_containerFoGuang
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerFoGuang_13;
	// UnityEngine.GameObject UnlockNewPlaneManager::m_preFoGuang
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preFoGuang_14;
	// UnityEngine.GameObject UnlockNewPlaneManager::_containerAll
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerAll_17;
	// UnityEngine.SpriteRenderer UnlockNewPlaneManager::_srPlane1
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srPlane1_18;
	// UnityEngine.SpriteRenderer UnlockNewPlaneManager::_srPlane2
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srPlane2_19;
	// UnityEngine.SpriteRenderer UnlockNewPlaneManager::_srNewPlane
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srNewPlane_20;
	// System.Single[] UnlockNewPlaneManager::m_arySessionTime
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_arySessionTime_21;
	// UnityEngine.Vector2 UnlockNewPlaneManager::m_vecPlane1StartPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecPlane1StartPos_22;
	// UnityEngine.Vector2 UnlockNewPlaneManager::m_vecPlane2StartPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecPlane2StartPos_23;
	// System.Single UnlockNewPlaneManager::m_fCrashDistance
	float ___m_fCrashDistance_24;
	// System.Single UnlockNewPlaneManager::m_fBackSpeed
	float ___m_fBackSpeed_25;
	// System.Single UnlockNewPlaneManager::m_fBackDistance
	float ___m_fBackDistance_26;
	// System.Single UnlockNewPlaneManager::m_fCrashA
	float ___m_fCrashA_27;
	// System.Single UnlockNewPlaneManager::m_fCrashSpeed
	float ___m_fCrashSpeed_28;
	// System.Int32 UnlockNewPlaneManager::m_nStatus
	int32_t ___m_nStatus_29;
	// UnityEngine.GameObject UnlockNewPlaneManager::_goLight
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goLight_30;
	// BaseScale UnlockNewPlaneManager::_baseScaleNewPlane
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____baseScaleNewPlane_31;
	// UnityEngine.GameObject UnlockNewPlaneManager::_containerExplosionEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerExplosionEffect_32;
	// UnityEngine.GameObject UnlockNewPlaneManager::_effectFoGuang
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____effectFoGuang_33;
	// UnityEngine.GameObject[] UnlockNewPlaneManager::_aryContainerFirework
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ____aryContainerFirework_34;
	// UnityEngine.Sprite UnlockNewPlaneManager::m_sprTest
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprTest_35;
	// System.Int32 UnlockNewPlaneManager::m_nCurLevel
	int32_t ___m_nCurLevel_36;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnlockNewPlaneManager::m_lstEffects
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___m_lstEffects_37;
	// System.Single UnlockNewPlaneManager::m_fTimeElapse
	float ___m_fTimeElapse_38;
	// System.Single UnlockNewPlaneManager::m_fMovement
	float ___m_fMovement_39;
	// System.Int32 UnlockNewPlaneManager::m_nCount
	int32_t ___m_nCount_40;

public:
	inline static int32_t get_offset_of__txtTapToExit_5() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____txtTapToExit_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTapToExit_5() const { return ____txtTapToExit_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTapToExit_5() { return &____txtTapToExit_5; }
	inline void set__txtTapToExit_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTapToExit_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtTapToExit_5), value);
	}

	inline static int32_t get_offset_of__btnClose_6() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____btnClose_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnClose_6() const { return ____btnClose_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnClose_6() { return &____btnClose_6; }
	inline void set__btnClose_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnClose_6 = value;
		Il2CppCodeGenWriteBarrier((&____btnClose_6), value);
	}

	inline static int32_t get_offset_of__skeTwoAuto_7() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____skeTwoAuto_7)); }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * get__skeTwoAuto_7() const { return ____skeTwoAuto_7; }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A ** get_address_of__skeTwoAuto_7() { return &____skeTwoAuto_7; }
	inline void set__skeTwoAuto_7(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * value)
	{
		____skeTwoAuto_7 = value;
		Il2CppCodeGenWriteBarrier((&____skeTwoAuto_7), value);
	}

	inline static int32_t get_offset_of__skeNewAuto_8() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____skeNewAuto_8)); }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * get__skeNewAuto_8() const { return ____skeNewAuto_8; }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A ** get_address_of__skeNewAuto_8() { return &____skeNewAuto_8; }
	inline void set__skeNewAuto_8(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * value)
	{
		____skeNewAuto_8 = value;
		Il2CppCodeGenWriteBarrier((&____skeNewAuto_8), value);
	}

	inline static int32_t get_offset_of__skeEffect1_9() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____skeEffect1_9)); }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * get__skeEffect1_9() const { return ____skeEffect1_9; }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A ** get_address_of__skeEffect1_9() { return &____skeEffect1_9; }
	inline void set__skeEffect1_9(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * value)
	{
		____skeEffect1_9 = value;
		Il2CppCodeGenWriteBarrier((&____skeEffect1_9), value);
	}

	inline static int32_t get_offset_of__skeEffect2_10() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____skeEffect2_10)); }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * get__skeEffect2_10() const { return ____skeEffect2_10; }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A ** get_address_of__skeEffect2_10() { return &____skeEffect2_10; }
	inline void set__skeEffect2_10(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * value)
	{
		____skeEffect2_10 = value;
		Il2CppCodeGenWriteBarrier((&____skeEffect2_10), value);
	}

	inline static int32_t get_offset_of_m_containerEffect_11() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_containerEffect_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerEffect_11() const { return ___m_containerEffect_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerEffect_11() { return &___m_containerEffect_11; }
	inline void set_m_containerEffect_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerEffect_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerEffect_11), value);
	}

	inline static int32_t get_offset_of_m_containerLighning_12() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_containerLighning_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerLighning_12() const { return ___m_containerLighning_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerLighning_12() { return &___m_containerLighning_12; }
	inline void set_m_containerLighning_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerLighning_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerLighning_12), value);
	}

	inline static int32_t get_offset_of_m_containerFoGuang_13() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_containerFoGuang_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerFoGuang_13() const { return ___m_containerFoGuang_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerFoGuang_13() { return &___m_containerFoGuang_13; }
	inline void set_m_containerFoGuang_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerFoGuang_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerFoGuang_13), value);
	}

	inline static int32_t get_offset_of_m_preFoGuang_14() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_preFoGuang_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preFoGuang_14() const { return ___m_preFoGuang_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preFoGuang_14() { return &___m_preFoGuang_14; }
	inline void set_m_preFoGuang_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preFoGuang_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_preFoGuang_14), value);
	}

	inline static int32_t get_offset_of__containerAll_17() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____containerAll_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerAll_17() const { return ____containerAll_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerAll_17() { return &____containerAll_17; }
	inline void set__containerAll_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerAll_17 = value;
		Il2CppCodeGenWriteBarrier((&____containerAll_17), value);
	}

	inline static int32_t get_offset_of__srPlane1_18() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____srPlane1_18)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srPlane1_18() const { return ____srPlane1_18; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srPlane1_18() { return &____srPlane1_18; }
	inline void set__srPlane1_18(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srPlane1_18 = value;
		Il2CppCodeGenWriteBarrier((&____srPlane1_18), value);
	}

	inline static int32_t get_offset_of__srPlane2_19() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____srPlane2_19)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srPlane2_19() const { return ____srPlane2_19; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srPlane2_19() { return &____srPlane2_19; }
	inline void set__srPlane2_19(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srPlane2_19 = value;
		Il2CppCodeGenWriteBarrier((&____srPlane2_19), value);
	}

	inline static int32_t get_offset_of__srNewPlane_20() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____srNewPlane_20)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srNewPlane_20() const { return ____srNewPlane_20; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srNewPlane_20() { return &____srNewPlane_20; }
	inline void set__srNewPlane_20(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srNewPlane_20 = value;
		Il2CppCodeGenWriteBarrier((&____srNewPlane_20), value);
	}

	inline static int32_t get_offset_of_m_arySessionTime_21() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_arySessionTime_21)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_arySessionTime_21() const { return ___m_arySessionTime_21; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_arySessionTime_21() { return &___m_arySessionTime_21; }
	inline void set_m_arySessionTime_21(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_arySessionTime_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySessionTime_21), value);
	}

	inline static int32_t get_offset_of_m_vecPlane1StartPos_22() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_vecPlane1StartPos_22)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecPlane1StartPos_22() const { return ___m_vecPlane1StartPos_22; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecPlane1StartPos_22() { return &___m_vecPlane1StartPos_22; }
	inline void set_m_vecPlane1StartPos_22(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecPlane1StartPos_22 = value;
	}

	inline static int32_t get_offset_of_m_vecPlane2StartPos_23() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_vecPlane2StartPos_23)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecPlane2StartPos_23() const { return ___m_vecPlane2StartPos_23; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecPlane2StartPos_23() { return &___m_vecPlane2StartPos_23; }
	inline void set_m_vecPlane2StartPos_23(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecPlane2StartPos_23 = value;
	}

	inline static int32_t get_offset_of_m_fCrashDistance_24() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fCrashDistance_24)); }
	inline float get_m_fCrashDistance_24() const { return ___m_fCrashDistance_24; }
	inline float* get_address_of_m_fCrashDistance_24() { return &___m_fCrashDistance_24; }
	inline void set_m_fCrashDistance_24(float value)
	{
		___m_fCrashDistance_24 = value;
	}

	inline static int32_t get_offset_of_m_fBackSpeed_25() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fBackSpeed_25)); }
	inline float get_m_fBackSpeed_25() const { return ___m_fBackSpeed_25; }
	inline float* get_address_of_m_fBackSpeed_25() { return &___m_fBackSpeed_25; }
	inline void set_m_fBackSpeed_25(float value)
	{
		___m_fBackSpeed_25 = value;
	}

	inline static int32_t get_offset_of_m_fBackDistance_26() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fBackDistance_26)); }
	inline float get_m_fBackDistance_26() const { return ___m_fBackDistance_26; }
	inline float* get_address_of_m_fBackDistance_26() { return &___m_fBackDistance_26; }
	inline void set_m_fBackDistance_26(float value)
	{
		___m_fBackDistance_26 = value;
	}

	inline static int32_t get_offset_of_m_fCrashA_27() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fCrashA_27)); }
	inline float get_m_fCrashA_27() const { return ___m_fCrashA_27; }
	inline float* get_address_of_m_fCrashA_27() { return &___m_fCrashA_27; }
	inline void set_m_fCrashA_27(float value)
	{
		___m_fCrashA_27 = value;
	}

	inline static int32_t get_offset_of_m_fCrashSpeed_28() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fCrashSpeed_28)); }
	inline float get_m_fCrashSpeed_28() const { return ___m_fCrashSpeed_28; }
	inline float* get_address_of_m_fCrashSpeed_28() { return &___m_fCrashSpeed_28; }
	inline void set_m_fCrashSpeed_28(float value)
	{
		___m_fCrashSpeed_28 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_29() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_nStatus_29)); }
	inline int32_t get_m_nStatus_29() const { return ___m_nStatus_29; }
	inline int32_t* get_address_of_m_nStatus_29() { return &___m_nStatus_29; }
	inline void set_m_nStatus_29(int32_t value)
	{
		___m_nStatus_29 = value;
	}

	inline static int32_t get_offset_of__goLight_30() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____goLight_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goLight_30() const { return ____goLight_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goLight_30() { return &____goLight_30; }
	inline void set__goLight_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goLight_30 = value;
		Il2CppCodeGenWriteBarrier((&____goLight_30), value);
	}

	inline static int32_t get_offset_of__baseScaleNewPlane_31() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____baseScaleNewPlane_31)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__baseScaleNewPlane_31() const { return ____baseScaleNewPlane_31; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__baseScaleNewPlane_31() { return &____baseScaleNewPlane_31; }
	inline void set__baseScaleNewPlane_31(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____baseScaleNewPlane_31 = value;
		Il2CppCodeGenWriteBarrier((&____baseScaleNewPlane_31), value);
	}

	inline static int32_t get_offset_of__containerExplosionEffect_32() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____containerExplosionEffect_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerExplosionEffect_32() const { return ____containerExplosionEffect_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerExplosionEffect_32() { return &____containerExplosionEffect_32; }
	inline void set__containerExplosionEffect_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerExplosionEffect_32 = value;
		Il2CppCodeGenWriteBarrier((&____containerExplosionEffect_32), value);
	}

	inline static int32_t get_offset_of__effectFoGuang_33() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____effectFoGuang_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__effectFoGuang_33() const { return ____effectFoGuang_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__effectFoGuang_33() { return &____effectFoGuang_33; }
	inline void set__effectFoGuang_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____effectFoGuang_33 = value;
		Il2CppCodeGenWriteBarrier((&____effectFoGuang_33), value);
	}

	inline static int32_t get_offset_of__aryContainerFirework_34() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____aryContainerFirework_34)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get__aryContainerFirework_34() const { return ____aryContainerFirework_34; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of__aryContainerFirework_34() { return &____aryContainerFirework_34; }
	inline void set__aryContainerFirework_34(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		____aryContainerFirework_34 = value;
		Il2CppCodeGenWriteBarrier((&____aryContainerFirework_34), value);
	}

	inline static int32_t get_offset_of_m_sprTest_35() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_sprTest_35)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprTest_35() const { return ___m_sprTest_35; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprTest_35() { return &___m_sprTest_35; }
	inline void set_m_sprTest_35(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprTest_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprTest_35), value);
	}

	inline static int32_t get_offset_of_m_nCurLevel_36() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_nCurLevel_36)); }
	inline int32_t get_m_nCurLevel_36() const { return ___m_nCurLevel_36; }
	inline int32_t* get_address_of_m_nCurLevel_36() { return &___m_nCurLevel_36; }
	inline void set_m_nCurLevel_36(int32_t value)
	{
		___m_nCurLevel_36 = value;
	}

	inline static int32_t get_offset_of_m_lstEffects_37() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_lstEffects_37)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_m_lstEffects_37() const { return ___m_lstEffects_37; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_m_lstEffects_37() { return &___m_lstEffects_37; }
	inline void set_m_lstEffects_37(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___m_lstEffects_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstEffects_37), value);
	}

	inline static int32_t get_offset_of_m_fTimeElapse_38() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fTimeElapse_38)); }
	inline float get_m_fTimeElapse_38() const { return ___m_fTimeElapse_38; }
	inline float* get_address_of_m_fTimeElapse_38() { return &___m_fTimeElapse_38; }
	inline void set_m_fTimeElapse_38(float value)
	{
		___m_fTimeElapse_38 = value;
	}

	inline static int32_t get_offset_of_m_fMovement_39() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fMovement_39)); }
	inline float get_m_fMovement_39() const { return ___m_fMovement_39; }
	inline float* get_address_of_m_fMovement_39() { return &___m_fMovement_39; }
	inline void set_m_fMovement_39(float value)
	{
		___m_fMovement_39 = value;
	}

	inline static int32_t get_offset_of_m_nCount_40() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_nCount_40)); }
	inline int32_t get_m_nCount_40() const { return ___m_nCount_40; }
	inline int32_t* get_address_of_m_nCount_40() { return &___m_nCount_40; }
	inline void set_m_nCount_40(int32_t value)
	{
		___m_nCount_40 = value;
	}
};

struct UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields
{
public:
	// UnlockNewPlaneManager UnlockNewPlaneManager::s_Instance
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * ___s_Instance_4;
	// UnityEngine.Vector3 UnlockNewPlaneManager::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_15;
	// UnityEngine.Vector3 UnlockNewPlaneManager::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_16;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields, ___s_Instance_4)); }
	inline UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * get_s_Instance_4() const { return ___s_Instance_4; }
	inline UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_vecTempPos_15() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields, ___vecTempPos_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_15() const { return ___vecTempPos_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_15() { return &___vecTempPos_15; }
	inline void set_vecTempPos_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_15 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_16() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields, ___vecTempScale_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_16() const { return ___vecTempScale_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_16() { return &___vecTempScale_16; }
	inline void set_vecTempScale_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNLOCKNEWPLANEMANAGER_T402DEC9E426B680014E232E712D172F77F23341B_H
#ifndef WEATHERMANAGER_T77BC50C3104A8AAC79F5FEA210AC31B904ECD196_H
#define WEATHERMANAGER_T77BC50C3104A8AAC79F5FEA210AC31B904ECD196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WeatherManager
struct  WeatherManager_t77BC50C3104A8AAC79F5FEA210AC31B904ECD196  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject WeatherManager::m_preSnowFlake
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preSnowFlake_4;

public:
	inline static int32_t get_offset_of_m_preSnowFlake_4() { return static_cast<int32_t>(offsetof(WeatherManager_t77BC50C3104A8AAC79F5FEA210AC31B904ECD196, ___m_preSnowFlake_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preSnowFlake_4() const { return ___m_preSnowFlake_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preSnowFlake_4() { return &___m_preSnowFlake_4; }
	inline void set_m_preSnowFlake_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preSnowFlake_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_preSnowFlake_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEATHERMANAGER_T77BC50C3104A8AAC79F5FEA210AC31B904ECD196_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#define SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Navigation_5)); }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Colors_7)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_SpriteState_8)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_TargetGraphic_11)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CanvasGroupCache_17)); }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_List_4)); }
	inline List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#ifndef BUTTON_T1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_H
#define BUTTON_T1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button
struct  Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * ___m_OnClick_18;

public:
	inline static int32_t get_offset_of_m_OnClick_18() { return static_cast<int32_t>(offsetof(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B, ___m_OnClick_18)); }
	inline ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * get_m_OnClick_18() const { return ___m_OnClick_18; }
	inline ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB ** get_address_of_m_OnClick_18() { return &___m_OnClick_18; }
	inline void set_m_OnClick_18(ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * value)
	{
		___m_OnClick_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnClick_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef SKELETONGRAPHIC_T5BE1CA256FF1DA1D464015B2E074DFB6B910106A_H
#define SKELETONGRAPHIC_T5BE1CA256FF1DA1D464015B2E074DFB6B910106A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonGraphic
struct  SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// Spine.Unity.SkeletonDataAsset Spine.Unity.SkeletonGraphic::skeletonDataAsset
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * ___skeletonDataAsset_30;
	// System.String Spine.Unity.SkeletonGraphic::initialSkinName
	String_t* ___initialSkinName_31;
	// System.Boolean Spine.Unity.SkeletonGraphic::initialFlipX
	bool ___initialFlipX_32;
	// System.Boolean Spine.Unity.SkeletonGraphic::initialFlipY
	bool ___initialFlipY_33;
	// System.String Spine.Unity.SkeletonGraphic::startingAnimation
	String_t* ___startingAnimation_34;
	// System.Boolean Spine.Unity.SkeletonGraphic::startingLoop
	bool ___startingLoop_35;
	// System.Single Spine.Unity.SkeletonGraphic::timeScale
	float ___timeScale_36;
	// System.Boolean Spine.Unity.SkeletonGraphic::freeze
	bool ___freeze_37;
	// System.Boolean Spine.Unity.SkeletonGraphic::unscaledTime
	bool ___unscaledTime_38;
	// UnityEngine.Texture Spine.Unity.SkeletonGraphic::overrideTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___overrideTexture_39;
	// Spine.Skeleton Spine.Unity.SkeletonGraphic::skeleton
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * ___skeleton_40;
	// Spine.AnimationState Spine.Unity.SkeletonGraphic::state
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * ___state_41;
	// Spine.Unity.MeshGenerator Spine.Unity.SkeletonGraphic::meshGenerator
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * ___meshGenerator_42;
	// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh> Spine.Unity.SkeletonGraphic::meshBuffers
	DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC * ___meshBuffers_43;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.SkeletonGraphic::currentInstructions
	SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * ___currentInstructions_44;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateLocal
	UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * ___UpdateLocal_45;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateWorld
	UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * ___UpdateWorld_46;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateComplete
	UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * ___UpdateComplete_47;
	// Spine.Unity.MeshGeneratorDelegate Spine.Unity.SkeletonGraphic::OnPostProcessVertices
	MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4 * ___OnPostProcessVertices_48;

public:
	inline static int32_t get_offset_of_skeletonDataAsset_30() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___skeletonDataAsset_30)); }
	inline SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * get_skeletonDataAsset_30() const { return ___skeletonDataAsset_30; }
	inline SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 ** get_address_of_skeletonDataAsset_30() { return &___skeletonDataAsset_30; }
	inline void set_skeletonDataAsset_30(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * value)
	{
		___skeletonDataAsset_30 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_30), value);
	}

	inline static int32_t get_offset_of_initialSkinName_31() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___initialSkinName_31)); }
	inline String_t* get_initialSkinName_31() const { return ___initialSkinName_31; }
	inline String_t** get_address_of_initialSkinName_31() { return &___initialSkinName_31; }
	inline void set_initialSkinName_31(String_t* value)
	{
		___initialSkinName_31 = value;
		Il2CppCodeGenWriteBarrier((&___initialSkinName_31), value);
	}

	inline static int32_t get_offset_of_initialFlipX_32() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___initialFlipX_32)); }
	inline bool get_initialFlipX_32() const { return ___initialFlipX_32; }
	inline bool* get_address_of_initialFlipX_32() { return &___initialFlipX_32; }
	inline void set_initialFlipX_32(bool value)
	{
		___initialFlipX_32 = value;
	}

	inline static int32_t get_offset_of_initialFlipY_33() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___initialFlipY_33)); }
	inline bool get_initialFlipY_33() const { return ___initialFlipY_33; }
	inline bool* get_address_of_initialFlipY_33() { return &___initialFlipY_33; }
	inline void set_initialFlipY_33(bool value)
	{
		___initialFlipY_33 = value;
	}

	inline static int32_t get_offset_of_startingAnimation_34() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___startingAnimation_34)); }
	inline String_t* get_startingAnimation_34() const { return ___startingAnimation_34; }
	inline String_t** get_address_of_startingAnimation_34() { return &___startingAnimation_34; }
	inline void set_startingAnimation_34(String_t* value)
	{
		___startingAnimation_34 = value;
		Il2CppCodeGenWriteBarrier((&___startingAnimation_34), value);
	}

	inline static int32_t get_offset_of_startingLoop_35() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___startingLoop_35)); }
	inline bool get_startingLoop_35() const { return ___startingLoop_35; }
	inline bool* get_address_of_startingLoop_35() { return &___startingLoop_35; }
	inline void set_startingLoop_35(bool value)
	{
		___startingLoop_35 = value;
	}

	inline static int32_t get_offset_of_timeScale_36() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___timeScale_36)); }
	inline float get_timeScale_36() const { return ___timeScale_36; }
	inline float* get_address_of_timeScale_36() { return &___timeScale_36; }
	inline void set_timeScale_36(float value)
	{
		___timeScale_36 = value;
	}

	inline static int32_t get_offset_of_freeze_37() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___freeze_37)); }
	inline bool get_freeze_37() const { return ___freeze_37; }
	inline bool* get_address_of_freeze_37() { return &___freeze_37; }
	inline void set_freeze_37(bool value)
	{
		___freeze_37 = value;
	}

	inline static int32_t get_offset_of_unscaledTime_38() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___unscaledTime_38)); }
	inline bool get_unscaledTime_38() const { return ___unscaledTime_38; }
	inline bool* get_address_of_unscaledTime_38() { return &___unscaledTime_38; }
	inline void set_unscaledTime_38(bool value)
	{
		___unscaledTime_38 = value;
	}

	inline static int32_t get_offset_of_overrideTexture_39() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___overrideTexture_39)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_overrideTexture_39() const { return ___overrideTexture_39; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_overrideTexture_39() { return &___overrideTexture_39; }
	inline void set_overrideTexture_39(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___overrideTexture_39 = value;
		Il2CppCodeGenWriteBarrier((&___overrideTexture_39), value);
	}

	inline static int32_t get_offset_of_skeleton_40() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___skeleton_40)); }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * get_skeleton_40() const { return ___skeleton_40; }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 ** get_address_of_skeleton_40() { return &___skeleton_40; }
	inline void set_skeleton_40(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * value)
	{
		___skeleton_40 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_40), value);
	}

	inline static int32_t get_offset_of_state_41() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___state_41)); }
	inline AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * get_state_41() const { return ___state_41; }
	inline AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 ** get_address_of_state_41() { return &___state_41; }
	inline void set_state_41(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * value)
	{
		___state_41 = value;
		Il2CppCodeGenWriteBarrier((&___state_41), value);
	}

	inline static int32_t get_offset_of_meshGenerator_42() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___meshGenerator_42)); }
	inline MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * get_meshGenerator_42() const { return ___meshGenerator_42; }
	inline MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 ** get_address_of_meshGenerator_42() { return &___meshGenerator_42; }
	inline void set_meshGenerator_42(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * value)
	{
		___meshGenerator_42 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_42), value);
	}

	inline static int32_t get_offset_of_meshBuffers_43() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___meshBuffers_43)); }
	inline DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC * get_meshBuffers_43() const { return ___meshBuffers_43; }
	inline DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC ** get_address_of_meshBuffers_43() { return &___meshBuffers_43; }
	inline void set_meshBuffers_43(DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC * value)
	{
		___meshBuffers_43 = value;
		Il2CppCodeGenWriteBarrier((&___meshBuffers_43), value);
	}

	inline static int32_t get_offset_of_currentInstructions_44() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___currentInstructions_44)); }
	inline SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * get_currentInstructions_44() const { return ___currentInstructions_44; }
	inline SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A ** get_address_of_currentInstructions_44() { return &___currentInstructions_44; }
	inline void set_currentInstructions_44(SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * value)
	{
		___currentInstructions_44 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_44), value);
	}

	inline static int32_t get_offset_of_UpdateLocal_45() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___UpdateLocal_45)); }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * get_UpdateLocal_45() const { return ___UpdateLocal_45; }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 ** get_address_of_UpdateLocal_45() { return &___UpdateLocal_45; }
	inline void set_UpdateLocal_45(UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * value)
	{
		___UpdateLocal_45 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateLocal_45), value);
	}

	inline static int32_t get_offset_of_UpdateWorld_46() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___UpdateWorld_46)); }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * get_UpdateWorld_46() const { return ___UpdateWorld_46; }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 ** get_address_of_UpdateWorld_46() { return &___UpdateWorld_46; }
	inline void set_UpdateWorld_46(UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * value)
	{
		___UpdateWorld_46 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateWorld_46), value);
	}

	inline static int32_t get_offset_of_UpdateComplete_47() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___UpdateComplete_47)); }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * get_UpdateComplete_47() const { return ___UpdateComplete_47; }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 ** get_address_of_UpdateComplete_47() { return &___UpdateComplete_47; }
	inline void set_UpdateComplete_47(UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * value)
	{
		___UpdateComplete_47 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateComplete_47), value);
	}

	inline static int32_t get_offset_of_OnPostProcessVertices_48() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___OnPostProcessVertices_48)); }
	inline MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4 * get_OnPostProcessVertices_48() const { return ___OnPostProcessVertices_48; }
	inline MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4 ** get_address_of_OnPostProcessVertices_48() { return &___OnPostProcessVertices_48; }
	inline void set_OnPostProcessVertices_48(MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4 * value)
	{
		___OnPostProcessVertices_48 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostProcessVertices_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGRAPHIC_T5BE1CA256FF1DA1D464015B2E074DFB6B910106A_H
#ifndef IMAGE_T18FED07D8646917E1C563745518CF3DD57FF0B3E_H
#define IMAGE_T18FED07D8646917E1C563745518CF3DD57FF0B3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_Sprite_31;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_OverrideSprite_32;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_33;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_34;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_35;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_36;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_37;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_38;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_39;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_40;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_41;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_42;

public:
	inline static int32_t get_offset_of_m_Sprite_31() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Sprite_31)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_Sprite_31() const { return ___m_Sprite_31; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_Sprite_31() { return &___m_Sprite_31; }
	inline void set_m_Sprite_31(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_Sprite_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_31), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_32() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_OverrideSprite_32)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_OverrideSprite_32() const { return ___m_OverrideSprite_32; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_OverrideSprite_32() { return &___m_OverrideSprite_32; }
	inline void set_m_OverrideSprite_32(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_OverrideSprite_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_32), value);
	}

	inline static int32_t get_offset_of_m_Type_33() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Type_33)); }
	inline int32_t get_m_Type_33() const { return ___m_Type_33; }
	inline int32_t* get_address_of_m_Type_33() { return &___m_Type_33; }
	inline void set_m_Type_33(int32_t value)
	{
		___m_Type_33 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_34() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_PreserveAspect_34)); }
	inline bool get_m_PreserveAspect_34() const { return ___m_PreserveAspect_34; }
	inline bool* get_address_of_m_PreserveAspect_34() { return &___m_PreserveAspect_34; }
	inline void set_m_PreserveAspect_34(bool value)
	{
		___m_PreserveAspect_34 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_35() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillCenter_35)); }
	inline bool get_m_FillCenter_35() const { return ___m_FillCenter_35; }
	inline bool* get_address_of_m_FillCenter_35() { return &___m_FillCenter_35; }
	inline void set_m_FillCenter_35(bool value)
	{
		___m_FillCenter_35 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_36() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillMethod_36)); }
	inline int32_t get_m_FillMethod_36() const { return ___m_FillMethod_36; }
	inline int32_t* get_address_of_m_FillMethod_36() { return &___m_FillMethod_36; }
	inline void set_m_FillMethod_36(int32_t value)
	{
		___m_FillMethod_36 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_37() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillAmount_37)); }
	inline float get_m_FillAmount_37() const { return ___m_FillAmount_37; }
	inline float* get_address_of_m_FillAmount_37() { return &___m_FillAmount_37; }
	inline void set_m_FillAmount_37(float value)
	{
		___m_FillAmount_37 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_38() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillClockwise_38)); }
	inline bool get_m_FillClockwise_38() const { return ___m_FillClockwise_38; }
	inline bool* get_address_of_m_FillClockwise_38() { return &___m_FillClockwise_38; }
	inline void set_m_FillClockwise_38(bool value)
	{
		___m_FillClockwise_38 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_39() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillOrigin_39)); }
	inline int32_t get_m_FillOrigin_39() const { return ___m_FillOrigin_39; }
	inline int32_t* get_address_of_m_FillOrigin_39() { return &___m_FillOrigin_39; }
	inline void set_m_FillOrigin_39(int32_t value)
	{
		___m_FillOrigin_39 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_40() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_AlphaHitTestMinimumThreshold_40)); }
	inline float get_m_AlphaHitTestMinimumThreshold_40() const { return ___m_AlphaHitTestMinimumThreshold_40; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_40() { return &___m_AlphaHitTestMinimumThreshold_40; }
	inline void set_m_AlphaHitTestMinimumThreshold_40(float value)
	{
		___m_AlphaHitTestMinimumThreshold_40 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_41() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Tracked_41)); }
	inline bool get_m_Tracked_41() const { return ___m_Tracked_41; }
	inline bool* get_address_of_m_Tracked_41() { return &___m_Tracked_41; }
	inline void set_m_Tracked_41(bool value)
	{
		___m_Tracked_41 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_42() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_UseSpriteMesh_42)); }
	inline bool get_m_UseSpriteMesh_42() const { return ___m_UseSpriteMesh_42; }
	inline bool* get_address_of_m_UseSpriteMesh_42() { return &___m_UseSpriteMesh_42; }
	inline void set_m_UseSpriteMesh_42(bool value)
	{
		___m_UseSpriteMesh_42 = value;
	}
};

struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_ETC1DefaultUI_30;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_VertScratch_43;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_UVScratch_44;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Xy_45;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Uv_46;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * ___m_TrackedTexturelessImages_47;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_48;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.UI.Image::<>f__mg$cache0
	Action_1_t148D4FE58B48D51DD45913A7B6EAA61E30D4B285 * ___U3CU3Ef__mgU24cache0_49;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_30() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_ETC1DefaultUI_30)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_ETC1DefaultUI_30() const { return ___s_ETC1DefaultUI_30; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_ETC1DefaultUI_30() { return &___s_ETC1DefaultUI_30; }
	inline void set_s_ETC1DefaultUI_30(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_ETC1DefaultUI_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_30), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_43() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_VertScratch_43)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_VertScratch_43() const { return ___s_VertScratch_43; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_VertScratch_43() { return &___s_VertScratch_43; }
	inline void set_s_VertScratch_43(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_VertScratch_43 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_43), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_44() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_UVScratch_44)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_UVScratch_44() const { return ___s_UVScratch_44; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_UVScratch_44() { return &___s_UVScratch_44; }
	inline void set_s_UVScratch_44(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_UVScratch_44 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_44), value);
	}

	inline static int32_t get_offset_of_s_Xy_45() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Xy_45)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Xy_45() const { return ___s_Xy_45; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Xy_45() { return &___s_Xy_45; }
	inline void set_s_Xy_45(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Xy_45 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_45), value);
	}

	inline static int32_t get_offset_of_s_Uv_46() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Uv_46)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Uv_46() const { return ___s_Uv_46; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Uv_46() { return &___s_Uv_46; }
	inline void set_s_Uv_46(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Uv_46 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_46), value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_47() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___m_TrackedTexturelessImages_47)); }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * get_m_TrackedTexturelessImages_47() const { return ___m_TrackedTexturelessImages_47; }
	inline List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED ** get_address_of_m_TrackedTexturelessImages_47() { return &___m_TrackedTexturelessImages_47; }
	inline void set_m_TrackedTexturelessImages_47(List_1_t5E6CEE165340A9D74D8BD47B8E6F422DFB7744ED * value)
	{
		___m_TrackedTexturelessImages_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedTexturelessImages_47), value);
	}

	inline static int32_t get_offset_of_s_Initialized_48() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Initialized_48)); }
	inline bool get_s_Initialized_48() const { return ___s_Initialized_48; }
	inline bool* get_address_of_s_Initialized_48() { return &___s_Initialized_48; }
	inline void set_s_Initialized_48(bool value)
	{
		___s_Initialized_48 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_49() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___U3CU3Ef__mgU24cache0_49)); }
	inline Action_1_t148D4FE58B48D51DD45913A7B6EAA61E30D4B285 * get_U3CU3Ef__mgU24cache0_49() const { return ___U3CU3Ef__mgU24cache0_49; }
	inline Action_1_t148D4FE58B48D51DD45913A7B6EAA61E30D4B285 ** get_address_of_U3CU3Ef__mgU24cache0_49() { return &___U3CU3Ef__mgU24cache0_49; }
	inline void set_U3CU3Ef__mgU24cache0_49(Action_1_t148D4FE58B48D51DD45913A7B6EAA61E30D4B285 * value)
	{
		___U3CU3Ef__mgU24cache0_49 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_49), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T18FED07D8646917E1C563745518CF3DD57FF0B3E_H
#ifndef TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#define TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * ___m_FontData_30;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_31;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCache_32;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCacheForLayout_33;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_35;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ___m_TempVerts_36;

public:
	inline static int32_t get_offset_of_m_FontData_30() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_FontData_30)); }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * get_m_FontData_30() const { return ___m_FontData_30; }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 ** get_address_of_m_FontData_30() { return &___m_FontData_30; }
	inline void set_m_FontData_30(FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * value)
	{
		___m_FontData_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_30), value);
	}

	inline static int32_t get_offset_of_m_Text_31() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_Text_31)); }
	inline String_t* get_m_Text_31() const { return ___m_Text_31; }
	inline String_t** get_address_of_m_Text_31() { return &___m_Text_31; }
	inline void set_m_Text_31(String_t* value)
	{
		___m_Text_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_31), value);
	}

	inline static int32_t get_offset_of_m_TextCache_32() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCache_32)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCache_32() const { return ___m_TextCache_32; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCache_32() { return &___m_TextCache_32; }
	inline void set_m_TextCache_32(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCache_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_32), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_33() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCacheForLayout_33)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCacheForLayout_33() const { return ___m_TextCacheForLayout_33; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCacheForLayout_33() { return &___m_TextCacheForLayout_33; }
	inline void set_m_TextCacheForLayout_33(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCacheForLayout_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_33), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_35() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_DisableFontTextureRebuiltCallback_35)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_35() const { return ___m_DisableFontTextureRebuiltCallback_35; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_35() { return &___m_DisableFontTextureRebuiltCallback_35; }
	inline void set_m_DisableFontTextureRebuiltCallback_35(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_35 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}
};

struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultText_34;

public:
	inline static int32_t get_offset_of_s_DefaultText_34() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields, ___s_DefaultText_34)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultText_34() const { return ___s_DefaultText_34; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultText_34() { return &___s_DefaultText_34; }
	inline void set_s_DefaultText_34(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultText_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// Spine.Unity.AtlasAsset[]
struct AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 * m_Items[1];

public:
	inline AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * m_Items[1];

public:
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mEF511C369E0CA9462FD3427DFC2375E81469570F_gshared (RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);

// System.Void Main::OpenPrestigePanel_ZengShou(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Main_OpenPrestigePanel_ZengShou_m1A6346E4B366764F942EE013F77E2F380DF5964D (Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1 * __this, int32_t ___nPlanetId0, int32_t ___nDistrictId1, const RuntimeMethod* method);
// DataManager/sPlanetConfig DataManager::GetPlanetConfigById(System.Int32)
extern "C" IL2CPP_METHOD_ATTR sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9  DataManager_GetPlanetConfigById_mBB062C792E37346E8635E8BF1A7ED1C870C94D4D (DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * __this, int32_t ___nPlanetId0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// DataManager/sTrackConfig DataManager::GetTrackConfigById(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B  DataManager_GetTrackConfigById_mFF61E5525974EFE2594D2ADC56EA393B3A694DCF (DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * __this, int32_t ___nPlanetId0, int32_t ___nTrackId1, const RuntimeMethod* method);
// UnityEngine.Sprite ResourceManager::GetCoinSpriteByPlanetId_New(System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ResourceManager_GetCoinSpriteByPlanetId_New_mA8E49C19B3A27526DAE9C3B6140A8A237266A2C4 (ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 * __this, int32_t ___nPlanetId0, bool ___bSpecial1, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C" IL2CPP_METHOD_ATTR void Image_set_sprite_m51F205B44430C8FF8BB0AF1BA7D825978EE663F9 (Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * __this, Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Void UnlockNewPlaneManager::End_NewVersion()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_End_NewVersion_m354AD213F4DFE5EDE856637A70D10D0377C818A7 (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool p0, const RuntimeMethod* method);
// System.Void AudioManager::PlaySE(AudioManager/eSE)
extern "C" IL2CPP_METHOD_ATTR void AudioManager_PlaySE_mA5A9A2C6078BFB432154F7F99206A7D3ED927BB7 (AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * __this, int32_t ___id0, const RuntimeMethod* method);
// District MapManager::GetCurDistrict()
extern "C" IL2CPP_METHOD_ATTR District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * MapManager_GetCurDistrict_m5F4C8E620A238B23E917E7E313F7DED947BF737F (MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C * __this, const RuntimeMethod* method);
// System.Int32 District::GetLevel()
extern "C" IL2CPP_METHOD_ATTR int32_t District_GetLevel_m74A14A6273413718D7993D33A89AFA013CFB3AC5 (District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * __this, const RuntimeMethod* method);
// System.Void FreshGuide::BeginMask(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void FreshGuide_BeginMask_m0C87D78F4B9C75F23D88B57DB1FEA62D6734D270 (FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * __this, float ___fStartPosX0, float ___fStartPosY1, float ___fStartRadius2, float ___fEndPosX3, float ___fEndPosY4, float ___fEndRadius5, float ___fTime6, const RuntimeMethod* method);
// System.Void Paw::SetIsUI(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Paw_SetIsUI_m6518AE99C9586A4ADB3E32C150F7352C987DD1B1 (Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * __this, bool ___bUI0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void Transform_SetParent_mFAF9209CAB6A864552074BA065D740924A4BF979 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, bool p0, const RuntimeMethod* method);
// System.Void Paw::EndMove()
extern "C" IL2CPP_METHOD_ATTR void Paw_EndMove_m1FAF5AD5E82BCB5D08AB4268AB9D0E7BBE1290A6 (Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void Paw::SetAlpha(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Paw_SetAlpha_m0550AA68D9E16E509E75B21196CFDDF85D8B8088 (Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * __this, float ___fAlpha0, const RuntimeMethod* method);
// System.Void DataManager::SaveMyData(System.String,System.Double)
extern "C" IL2CPP_METHOD_ATTR void DataManager_SaveMyData_m101FE62BB9088DA52B2A0983AD51D05730A62ACE (DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * __this, String_t* ___szKey0, double ___dValue1, const RuntimeMethod* method);
// Spine.AnimationState Spine.Unity.SkeletonGraphic::get_AnimationState()
extern "C" IL2CPP_METHOD_ATTR AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * SkeletonGraphic_get_AnimationState_mD7077640DED8C192BD58C5537D923D31A02FB6DB (SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * __this, const RuntimeMethod* method);
// Spine.TrackEntry Spine.AnimationState::SetAnimation(System.Int32,System.String,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * AnimationState_SetAnimation_m262C3BEE1026DA4F16FE5AA5B86D8E2C93322CFB (AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * __this, int32_t ___trackIndex0, String_t* ___animationName1, bool ___loop2, const RuntimeMethod* method);
// System.Int32 District::GetId()
extern "C" IL2CPP_METHOD_ATTR int32_t District_GetId_mCACC834000F73A0DFCD78CF5CFC1DC18CFA07F33 (District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * __this, const RuntimeMethod* method);
// UnityEngine.Sprite ResourceManager::GetParkingPlaneSpriteByLevel(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ResourceManager_GetParkingPlaneSpriteByLevel_m6B1EA9A59CC4B952F991793A5B3090BE220ECC15 (ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 * __this, int32_t ___nTrackId0, int32_t ___nLevel1, const RuntimeMethod* method);
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C" IL2CPP_METHOD_ATTR Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * Sprite_get_texture_mA1FF8462BBB398DC8B3F0F92B2AB41BDA6AF69A5 (Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void Material_SetTexture_mAA0F00FACFE40CFE4BE28A11162E5EEFCC5F5A61 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, String_t* p0, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * p1, const RuntimeMethod* method);
// System.Void AudioManager::PlaySE_New(AudioManager/eSe_New)
extern "C" IL2CPP_METHOD_ATTR void AudioManager_PlaySE_New_m31C5B9BA7E9757771D61B5A2096048B75F5D76DE (AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * __this, int32_t ___eType0, const RuntimeMethod* method);
// System.Void UIManager::SetUiVisible(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void UIManager_SetUiVisible_m451F5413573F04BF2E06B37FC1F80B3CBA3ADD76 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, bool ___bVisible0, const RuntimeMethod* method);
// System.Void UnlockNewPlaneManager::ClearSomething()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_ClearSomething_mC8779E9BDFB3C60832AF9F7835BDA8B0AD914FCC (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method);
// System.Void UnlockNewPlaneManager::Loop_NewVersion()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_Loop_NewVersion_mFC19BCACA343BEA3A1A473D122BB84DE40868327 (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
inline void List_1__ctor_m70B8A20433AEEDEB942CD3EEC229497AB693E9D6 (List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * p0, const RuntimeMethod* method)
{
	return ((  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mEF511C369E0CA9462FD3427DFC2375E81469570F_gshared)(p0, method);
}
// !!0 UnityEngine.GameObject::GetComponent<SnowFlake>()
inline SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70 * GameObject_GetComponent_TisSnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_m4B69970D1C279F5DF4E82E0549BA7738FB538EF7 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method)
{
	return ((  SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70 * (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m41E09C4CA476451FE275573062956CED105CB79A_gshared)(__this, method);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIZengShouCounter::Start()
extern "C" IL2CPP_METHOD_ATTR void UIZengShouCounter_Start_m7F0F55587B2AE042B31298E6C996C1D968AA99BD (UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UIZengShouCounter::Update()
extern "C" IL2CPP_METHOD_ATTR void UIZengShouCounter_Update_m766F29DAF4474606DC20456593E69E1A424D80CF (UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Int32 UIZengShouCounter::GetPlanetId()
extern "C" IL2CPP_METHOD_ATTR int32_t UIZengShouCounter_GetPlanetId_mB0A3541DE66D5CD1D628D950AF880E685ED008F7 (UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_nPlanetId_11();
		return L_0;
	}
}
// System.Int32 UIZengShouCounter::GetDistrictId()
extern "C" IL2CPP_METHOD_ATTR int32_t UIZengShouCounter_GetDistrictId_m9D65B6DBE6274439D88993CE224B8239894E0807 (UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_nDistrictId_12();
		return L_0;
	}
}
// System.Void UIZengShouCounter::OnClick_ZengShou()
extern "C" IL2CPP_METHOD_ATTR void UIZengShouCounter_OnClick_ZengShou_mBF06A58B9F4504B81DF059C063C1967D070335D0 (UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIZengShouCounter_OnClick_ZengShou_mBF06A58B9F4504B81DF059C063C1967D070335D0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_il2cpp_TypeInfo_var);
		Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1 * L_0 = ((Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_StaticFields*)il2cpp_codegen_static_fields_for(Main_t7BD87A0CB813F834A2F93006206A3CE79188D0C1_il2cpp_TypeInfo_var))->get_s_Instance_21();
		int32_t L_1 = __this->get_m_nPlanetId_11();
		int32_t L_2 = __this->get_m_nDistrictId_12();
		NullCheck(L_0);
		Main_OpenPrestigePanel_ZengShou_m1A6346E4B366764F942EE013F77E2F380DF5964D(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIZengShouCounter::SetPlanetIdAndDistrictId(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UIZengShouCounter_SetPlanetIdAndDistrictId_m9CCB17E056A92CDCAEAAA29AE78F0063FCFE680F (UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95 * __this, int32_t ___nPlanetId0, int32_t ___nDistrictId1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIZengShouCounter_SetPlanetIdAndDistrictId_m9CCB17E056A92CDCAEAAA29AE78F0063FCFE680F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9  V_0;
	memset(&V_0, 0, sizeof(V_0));
	sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___nPlanetId0;
		__this->set_m_nPlanetId_11(L_0);
		int32_t L_1 = ___nDistrictId1;
		__this->set_m_nDistrictId_12(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_il2cpp_TypeInfo_var);
		DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * L_2 = ((DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields*)il2cpp_codegen_static_fields_for(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_il2cpp_TypeInfo_var))->get_s_Instance_4();
		int32_t L_3 = ___nPlanetId0;
		NullCheck(L_2);
		sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9  L_4 = DataManager_GetPlanetConfigById_mBB062C792E37346E8635E8BF1A7ED1C870C94D4D(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = ___nPlanetId0;
		switch (L_5)
		{
			case 0:
			{
				goto IL_002e;
			}
			case 1:
			{
				goto IL_0041;
			}
			case 2:
			{
				goto IL_0054;
			}
		}
	}
	{
		goto IL_0065;
	}

IL_002e:
	{
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_6 = __this->get__txtPlanetAndDistrict_4();
		sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9  L_7 = V_0;
		String_t* L_8 = L_7.get_szName_0();
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_8);
		goto IL_0065;
	}

IL_0041:
	{
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_9 = __this->get__txtPlanetAndDistrict_4();
		sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9  L_10 = V_0;
		String_t* L_11 = L_10.get_szName_0();
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, L_11);
		goto IL_0065;
	}

IL_0054:
	{
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_12 = __this->get__txtPlanetAndDistrict_4();
		sPlanetConfig_tB3752904FD33E8B33195C2F67744EF265D750AA9  L_13 = V_0;
		String_t* L_14 = L_13.get_szName_0();
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_14);
	}

IL_0065:
	{
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_15 = __this->get__txtPlanetAndDistrict_4();
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_16 = L_15;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(72 /* System.String UnityEngine.UI.Text::get_text() */, L_16);
		String_t* L_18 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_17, _stringLiteral099600A10A944114AAC406D136B625FB416DD779, /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_16, L_18);
		IL2CPP_RUNTIME_CLASS_INIT(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_il2cpp_TypeInfo_var);
		DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * L_19 = ((DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields*)il2cpp_codegen_static_fields_for(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_il2cpp_TypeInfo_var))->get_s_Instance_4();
		int32_t L_20 = ___nPlanetId0;
		int32_t L_21 = __this->get_m_nDistrictId_12();
		NullCheck(L_19);
		sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B  L_22 = DataManager_GetTrackConfigById_mFF61E5525974EFE2594D2ADC56EA393B3A694DCF(L_19, L_20, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_23 = __this->get__txtPlanetAndDistrict_4();
		sTrackConfig_tC4F21E8F5FB966041FF5D2421A3AB04E318A005B  L_24 = V_1;
		String_t* L_25 = L_24.get_szName_1();
		NullCheck(L_23);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_23, L_25);
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_26 = __this->get__imgCoinIcon_8();
		IL2CPP_RUNTIME_CLASS_INIT(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_il2cpp_TypeInfo_var);
		ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 * L_27 = ((ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields*)il2cpp_codegen_static_fields_for(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_il2cpp_TypeInfo_var))->get_s_Instance_19();
		int32_t L_28 = ___nPlanetId0;
		NullCheck(L_27);
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_29 = ResourceManager_GetCoinSpriteByPlanetId_New_mA8E49C19B3A27526DAE9C3B6140A8A237266A2C4(L_27, L_28, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_26);
		Image_set_sprite_m51F205B44430C8FF8BB0AF1BA7D825978EE663F9(L_26, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIZengShouCounter::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UIZengShouCounter__ctor_m944E0446522F8609D2F34EB59A50B700C8837435 (UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnlockNewPlaneManager::Awake()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_Awake_m4544CEBF76793762E75BC39B5C02292718040688 (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnlockNewPlaneManager_Awake_m4544CEBF76793762E75BC39B5C02292718040688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var);
		((UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields*)il2cpp_codegen_static_fields_for(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var))->set_s_Instance_4(__this);
		return;
	}
}
// System.Void UnlockNewPlaneManager::Start()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_Start_m0B4FF6FC0E1C8CD33C9A1B63E9EADA0E45C9A67E (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_m_fBackDistance_26();
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_1 = __this->get_m_arySessionTime_21();
		NullCheck(L_1);
		int32_t L_2 = 1;
		float L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		__this->set_m_fBackSpeed_25(((float)((float)L_0/(float)L_3)));
		float L_4 = __this->get_m_fCrashDistance_24();
		V_0 = L_4;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_5 = __this->get_m_arySessionTime_21();
		NullCheck(L_5);
		int32_t L_6 = 2;
		float L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		float L_8 = V_0;
		float L_9 = V_1;
		float L_10 = V_1;
		__this->set_m_fCrashA_27(((float)((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_8))/(float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_10)))));
		return;
	}
}
// System.Void UnlockNewPlaneManager::Reset()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_Reset_mF4C176A32A9F2D92BB2FF8F1D158283FC8380AAC (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_nCurLevel_36(1);
		return;
	}
}
// System.Void UnlockNewPlaneManager::SetCurLevel(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_SetCurLevel_m4AF3100A9F6BACD3E7A6B5129C025515EC5AECEB (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, int32_t ___nLevel0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nLevel0;
		__this->set_m_nCurLevel_36(L_0);
		return;
	}
}
// System.Void UnlockNewPlaneManager::Loop_NewVersion()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_Loop_NewVersion_mFC19BCACA343BEA3A1A473D122BB84DE40868327 (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnlockNewPlaneManager_Loop_NewVersion_mFC19BCACA343BEA3A1A473D122BB84DE40868327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_m_nStatus_29();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		float L_1 = __this->get_m_fTimeElapse_38();
		float L_2 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		__this->set_m_fTimeElapse_38(((float)il2cpp_codegen_add((float)L_1, (float)L_2)));
		float L_3 = __this->get_m_fTimeElapse_38();
		if ((!(((float)L_3) > ((float)(5.0f)))))
		{
			goto IL_002f;
		}
	}
	{
		UnlockNewPlaneManager_End_NewVersion_m354AD213F4DFE5EDE856637A70D10D0377C818A7(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		float L_4 = __this->get_m_fTimeElapse_38();
		if ((!(((float)L_4) >= ((float)(2.0f)))))
		{
			goto IL_004d;
		}
	}
	{
		Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * L_5 = __this->get__btnClose_6();
		NullCheck(L_5);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_6 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_6, (bool)1, /*hidden argument*/NULL);
	}

IL_004d:
	{
		int32_t L_7 = __this->get_m_nStatus_29();
		if (L_7)
		{
			goto IL_0074;
		}
	}
	{
		float L_8 = __this->get_m_fTimeElapse_38();
		if ((!(((float)L_8) >= ((float)(1.5f)))))
		{
			goto IL_0074;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_il2cpp_TypeInfo_var);
		AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * L_9 = ((AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields*)il2cpp_codegen_static_fields_for(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_il2cpp_TypeInfo_var))->get_s_Instance_4();
		NullCheck(L_9);
		AudioManager_PlaySE_mA5A9A2C6078BFB432154F7F99206A7D3ED927BB7(L_9, 7, /*hidden argument*/NULL);
		__this->set_m_nStatus_29(1);
	}

IL_0074:
	{
		return;
	}
}
// System.Void UnlockNewPlaneManager::OnClickButton_Exit()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_OnClickButton_Exit_mB31A25F5009D6072661601B78924C4097CDF2A89 (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method)
{
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get__containerAll_17();
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnlockNewPlaneManager::End_NewVersion()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_End_NewVersion_m354AD213F4DFE5EDE856637A70D10D0377C818A7 (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnlockNewPlaneManager_End_NewVersion_m354AD213F4DFE5EDE856637A70D10D0377C818A7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get__containerAll_17();
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, (bool)0, /*hidden argument*/NULL);
		__this->set_m_nStatus_29((-1));
		IL2CPP_RUNTIME_CLASS_INIT(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_il2cpp_TypeInfo_var);
		MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C * L_1 = ((MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields*)il2cpp_codegen_static_fields_for(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_il2cpp_TypeInfo_var))->get_s_Instance_20();
		NullCheck(L_1);
		District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * L_2 = MapManager_GetCurDistrict_m5F4C8E620A238B23E917E7E313F7DED947BF737F(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = District_GetLevel_m74A14A6273413718D7993D33A89AFA013CFB3AC5(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)6))))
		{
			goto IL_01f3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_il2cpp_TypeInfo_var);
		TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 * L_4 = ((TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields*)il2cpp_codegen_static_fields_for(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_il2cpp_TypeInfo_var))->get_s_Instance_27();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_m_nFreshGuideStatus_4();
		if (L_5)
		{
			goto IL_01f3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var);
		FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * L_6 = ((FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields*)il2cpp_codegen_static_fields_for(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var))->get_s_Instance_25();
		NullCheck(L_6);
		FreshGuide_BeginMask_m0C87D78F4B9C75F23D88B57DB1FEA62D6734D270(L_6, (0.62f), (0.25f), (0.3f), (0.62f), (0.22f), (0.09f), (1.0f), /*hidden argument*/NULL);
		FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * L_7 = ((FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields*)il2cpp_codegen_static_fields_for(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var))->get_s_Instance_25();
		NullCheck(L_7);
		Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * L_8 = L_7->get__paw_27();
		NullCheck(L_8);
		Paw_SetIsUI_m6518AE99C9586A4ADB3E32C150F7352C987DD1B1(L_8, (bool)1, /*hidden argument*/NULL);
		FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * L_9 = ((FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields*)il2cpp_codegen_static_fields_for(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var))->get_s_Instance_25();
		NullCheck(L_9);
		Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * L_10 = L_9->get__paw_27();
		NullCheck(L_10);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_il2cpp_TypeInfo_var);
		TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 * L_12 = ((TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields*)il2cpp_codegen_static_fields_for(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_il2cpp_TypeInfo_var))->get_s_Instance_27();
		NullCheck(L_12);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_13 = L_12->get__goBtnTanGeChe_7();
		NullCheck(L_13);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_SetParent_mFAF9209CAB6A864552074BA065D740924A4BF979(L_11, L_14, /*hidden argument*/NULL);
		FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * L_15 = ((FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields*)il2cpp_codegen_static_fields_for(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var))->get_s_Instance_25();
		NullCheck(L_15);
		Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * L_16 = L_15->get__paw_27();
		NullCheck(L_16);
		BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * L_17 = L_16->get__basescalePaw_8();
		NullCheck(L_17);
		Behaviour_set_enabled_m9755D3B17D7022D23D1E4C618BD9A6B66A5ADC6B(L_17, (bool)1, /*hidden argument*/NULL);
		FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * L_18 = ((FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields*)il2cpp_codegen_static_fields_for(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var))->get_s_Instance_25();
		NullCheck(L_18);
		Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * L_19 = L_18->get__paw_27();
		NullCheck(L_19);
		Paw_EndMove_m1FAF5AD5E82BCB5D08AB4268AB9D0E7BBE1290A6(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var);
		(((UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields*)il2cpp_codegen_static_fields_for(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var))->get_address_of_vecTempPos_15())->set_x_2((30.0f));
		(((UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields*)il2cpp_codegen_static_fields_for(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var))->get_address_of_vecTempPos_15())->set_y_3((-100.0f));
		(((UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields*)il2cpp_codegen_static_fields_for(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var))->get_address_of_vecTempPos_15())->set_z_4((0.0f));
		FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * L_20 = ((FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields*)il2cpp_codegen_static_fields_for(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var))->get_s_Instance_25();
		NullCheck(L_20);
		Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * L_21 = L_20->get__paw_27();
		NullCheck(L_21);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_22 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_21, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_23 = ((UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields*)il2cpp_codegen_static_fields_for(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var))->get_vecTempPos_15();
		NullCheck(L_22);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_22, L_23, /*hidden argument*/NULL);
		(((UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields*)il2cpp_codegen_static_fields_for(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var))->get_address_of_vecTempScale_16())->set_x_2((0.7f));
		(((UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields*)il2cpp_codegen_static_fields_for(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var))->get_address_of_vecTempScale_16())->set_y_3((0.7f));
		FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * L_24 = ((FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields*)il2cpp_codegen_static_fields_for(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var))->get_s_Instance_25();
		NullCheck(L_24);
		Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * L_25 = L_24->get__paw_27();
		NullCheck(L_25);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_26 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_25, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_27 = ((UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields*)il2cpp_codegen_static_fields_for(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var))->get_vecTempScale_16();
		NullCheck(L_26);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_26, L_27, /*hidden argument*/NULL);
		FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * L_28 = ((FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields*)il2cpp_codegen_static_fields_for(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var))->get_s_Instance_25();
		NullCheck(L_28);
		CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * L_29 = L_28->get__faClick_28();
		NullCheck(L_29);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_30 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_29, /*hidden argument*/NULL);
		TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 * L_31 = ((TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields*)il2cpp_codegen_static_fields_for(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_il2cpp_TypeInfo_var))->get_s_Instance_27();
		NullCheck(L_31);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_32 = L_31->get__goBtnTanGeChe_7();
		NullCheck(L_32);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_33 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_SetParent_mFAF9209CAB6A864552074BA065D740924A4BF979(L_30, L_33, /*hidden argument*/NULL);
		FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * L_34 = ((FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields*)il2cpp_codegen_static_fields_for(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var))->get_s_Instance_25();
		NullCheck(L_34);
		CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * L_35 = L_34->get__faClick_28();
		NullCheck(L_35);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_36 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_35, /*hidden argument*/NULL);
		FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * L_37 = ((FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields*)il2cpp_codegen_static_fields_for(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var))->get_s_Instance_25();
		NullCheck(L_37);
		Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * L_38 = L_37->get__paw_27();
		NullCheck(L_38);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_39 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_40 = Transform_get_localPosition_m812D43318E05BDCB78310EB7308785A13D85EFD8(L_39, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_set_localPosition_m275F5550DD939F83AFEB5E8D681131172E2E1728(L_36, L_40, /*hidden argument*/NULL);
		FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * L_41 = ((FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields*)il2cpp_codegen_static_fields_for(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var))->get_s_Instance_25();
		NullCheck(L_41);
		Paw_t9302465120C7C5579ADFF12950B7E6F59B4DF011 * L_42 = L_41->get__paw_27();
		NullCheck(L_42);
		Paw_SetAlpha_m0550AA68D9E16E509E75B21196CFDDF85D8B8088(L_42, (1.0f), /*hidden argument*/NULL);
		(((UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields*)il2cpp_codegen_static_fields_for(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var))->get_address_of_vecTempScale_16())->set_x_2((0.8f));
		(((UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields*)il2cpp_codegen_static_fields_for(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var))->get_address_of_vecTempScale_16())->set_y_3((0.8f));
		FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3 * L_43 = ((FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_StaticFields*)il2cpp_codegen_static_fields_for(FreshGuide_t840B865F5BBA1373DC91F27C5939E88FD6095DD3_il2cpp_TypeInfo_var))->get_s_Instance_25();
		NullCheck(L_43);
		CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * L_44 = L_43->get__faClick_28();
		NullCheck(L_44);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_45 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_44, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_46 = ((UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields*)il2cpp_codegen_static_fields_for(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_il2cpp_TypeInfo_var))->get_vecTempScale_16();
		NullCheck(L_45);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_45, L_46, /*hidden argument*/NULL);
		TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 * L_47 = ((TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields*)il2cpp_codegen_static_fields_for(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_il2cpp_TypeInfo_var))->get_s_Instance_27();
		NullCheck(L_47);
		L_47->set_m_nFreshGuideStatus_4(1);
		IL2CPP_RUNTIME_CLASS_INIT(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_il2cpp_TypeInfo_var);
		DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30 * L_48 = ((DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_StaticFields*)il2cpp_codegen_static_fields_for(DataManager_tE86B5403AB8D1E0344B6B0D41A7FCAE957A3EB30_il2cpp_TypeInfo_var))->get_s_Instance_4();
		TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455 * L_49 = ((TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_StaticFields*)il2cpp_codegen_static_fields_for(TanGeChe_t8AB5138CD649482ABE434C2571453BE2A5AF0455_il2cpp_TypeInfo_var))->get_s_Instance_27();
		NullCheck(L_49);
		int32_t L_50 = L_49->get_m_nFreshGuideStatus_4();
		NullCheck(L_48);
		DataManager_SaveMyData_m101FE62BB9088DA52B2A0983AD51D05730A62ACE(L_48, _stringLiteralA80854C74FC8CD3D0E53284034962DAFBE6B500F, (((double)((double)L_50))), /*hidden argument*/NULL);
	}

IL_01f3:
	{
		return;
	}
}
// System.Void UnlockNewPlaneManager::Begin(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_Begin_m099DE7F42069707FED56E3B0C7E9A76DE85E2178 (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, int32_t ___nNewLevel0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnlockNewPlaneManager_Begin_m099DE7F42069707FED56E3B0C7E9A76DE85E2178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_0 = __this->get__txtTapToExit_5();
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_1, (bool)1, /*hidden argument*/NULL);
		Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * L_2 = __this->get__btnClose_6();
		NullCheck(L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_3, (bool)0, /*hidden argument*/NULL);
		__this->set_m_nStatus_29(0);
		__this->set_m_fTimeElapse_38((0.0f));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_4 = __this->get__containerAll_17();
		NullCheck(L_4);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_4, (bool)1, /*hidden argument*/NULL);
		SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * L_5 = __this->get__skeTwoAuto_7();
		NullCheck(L_5);
		AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * L_6 = SkeletonGraphic_get_AnimationState_mD7077640DED8C192BD58C5537D923D31A02FB6DB(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		AnimationState_SetAnimation_m262C3BEE1026DA4F16FE5AA5B86D8E2C93322CFB(L_6, 0, _stringLiteral309784E462C9CE80EC50F54AFDFB1E3B3AB31858, (bool)0, /*hidden argument*/NULL);
		SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * L_7 = __this->get__skeNewAuto_8();
		NullCheck(L_7);
		AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * L_8 = SkeletonGraphic_get_AnimationState_mD7077640DED8C192BD58C5537D923D31A02FB6DB(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		AnimationState_SetAnimation_m262C3BEE1026DA4F16FE5AA5B86D8E2C93322CFB(L_8, 0, _stringLiteral309784E462C9CE80EC50F54AFDFB1E3B3AB31858, (bool)0, /*hidden argument*/NULL);
		SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * L_9 = __this->get__skeEffect1_9();
		NullCheck(L_9);
		AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * L_10 = SkeletonGraphic_get_AnimationState_mD7077640DED8C192BD58C5537D923D31A02FB6DB(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		AnimationState_SetAnimation_m262C3BEE1026DA4F16FE5AA5B86D8E2C93322CFB(L_10, 0, _stringLiteral309784E462C9CE80EC50F54AFDFB1E3B3AB31858, (bool)0, /*hidden argument*/NULL);
		SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * L_11 = __this->get__skeEffect2_10();
		NullCheck(L_11);
		AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * L_12 = SkeletonGraphic_get_AnimationState_mD7077640DED8C192BD58C5537D923D31A02FB6DB(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		AnimationState_SetAnimation_m262C3BEE1026DA4F16FE5AA5B86D8E2C93322CFB(L_12, 0, _stringLiteral309784E462C9CE80EC50F54AFDFB1E3B3AB31858, (bool)0, /*hidden argument*/NULL);
		SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * L_13 = __this->get__skeTwoAuto_7();
		NullCheck(L_13);
		SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * L_14 = L_13->get_skeletonDataAsset_30();
		NullCheck(L_14);
		AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182* L_15 = L_14->get_atlasAssets_4();
		NullCheck(L_15);
		int32_t L_16 = 0;
		AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 * L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* L_18 = L_17->get_materials_5();
		NullCheck(L_18);
		int32_t L_19 = 0;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		IL2CPP_RUNTIME_CLASS_INIT(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_il2cpp_TypeInfo_var);
		ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 * L_21 = ((ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields*)il2cpp_codegen_static_fields_for(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_il2cpp_TypeInfo_var))->get_s_Instance_19();
		IL2CPP_RUNTIME_CLASS_INIT(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_il2cpp_TypeInfo_var);
		MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C * L_22 = ((MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields*)il2cpp_codegen_static_fields_for(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_il2cpp_TypeInfo_var))->get_s_Instance_20();
		NullCheck(L_22);
		District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * L_23 = MapManager_GetCurDistrict_m5F4C8E620A238B23E917E7E313F7DED947BF737F(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		int32_t L_24 = District_GetId_mCACC834000F73A0DFCD78CF5CFC1DC18CFA07F33(L_23, /*hidden argument*/NULL);
		int32_t L_25 = ___nNewLevel0;
		NullCheck(L_21);
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_26 = ResourceManager_GetParkingPlaneSpriteByLevel_m6B1EA9A59CC4B952F991793A5B3090BE220ECC15(L_21, L_24, ((int32_t)il2cpp_codegen_subtract((int32_t)L_25, (int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_26);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_27 = Sprite_get_texture_mA1FF8462BBB398DC8B3F0F92B2AB41BDA6AF69A5(L_26, /*hidden argument*/NULL);
		NullCheck(L_20);
		Material_SetTexture_mAA0F00FACFE40CFE4BE28A11162E5EEFCC5F5A61(L_20, _stringLiteralC510EA100EEE1C261FE63B56E1F3390BFB85F481, L_27, /*hidden argument*/NULL);
		SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * L_28 = __this->get__skeNewAuto_8();
		NullCheck(L_28);
		SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * L_29 = L_28->get_skeletonDataAsset_30();
		NullCheck(L_29);
		AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182* L_30 = L_29->get_atlasAssets_4();
		NullCheck(L_30);
		int32_t L_31 = 0;
		AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 * L_32 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_32);
		MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* L_33 = L_32->get_materials_5();
		NullCheck(L_33);
		int32_t L_34 = 0;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_35 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78 * L_36 = ((ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_StaticFields*)il2cpp_codegen_static_fields_for(ResourceManager_tCFA24EF3B73D3018EECF116BB1928AA7F1C08F78_il2cpp_TypeInfo_var))->get_s_Instance_19();
		MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C * L_37 = ((MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_StaticFields*)il2cpp_codegen_static_fields_for(MapManager_t76B6F439D4A039A5A7E58A9FFDDA6A4974CEF33C_il2cpp_TypeInfo_var))->get_s_Instance_20();
		NullCheck(L_37);
		District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * L_38 = MapManager_GetCurDistrict_m5F4C8E620A238B23E917E7E313F7DED947BF737F(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		int32_t L_39 = District_GetId_mCACC834000F73A0DFCD78CF5CFC1DC18CFA07F33(L_38, /*hidden argument*/NULL);
		int32_t L_40 = ___nNewLevel0;
		NullCheck(L_36);
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_41 = ResourceManager_GetParkingPlaneSpriteByLevel_m6B1EA9A59CC4B952F991793A5B3090BE220ECC15(L_36, L_39, L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_42 = Sprite_get_texture_mA1FF8462BBB398DC8B3F0F92B2AB41BDA6AF69A5(L_41, /*hidden argument*/NULL);
		NullCheck(L_35);
		Material_SetTexture_mAA0F00FACFE40CFE4BE28A11162E5EEFCC5F5A61(L_35, _stringLiteralC510EA100EEE1C261FE63B56E1F3390BFB85F481, L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_il2cpp_TypeInfo_var);
		AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * L_43 = ((AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields*)il2cpp_codegen_static_fields_for(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_il2cpp_TypeInfo_var))->get_s_Instance_4();
		NullCheck(L_43);
		AudioManager_PlaySE_New_m31C5B9BA7E9757771D61B5A2096048B75F5D76DE(L_43, ((int32_t)28), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnlockNewPlaneManager::End()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_End_m29520DC54380DF192A61130906FE5C2F713377F0 (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnlockNewPlaneManager_End_m29520DC54380DF192A61130906FE5C2F713377F0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_nStatus_29((-1));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get__containerAll_17();
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var);
		UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * L_1 = ((UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields*)il2cpp_codegen_static_fields_for(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var))->get_s_Instance_4();
		NullCheck(L_1);
		UIManager_SetUiVisible_m451F5413573F04BF2E06B37FC1F80B3CBA3ADD76(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnlockNewPlaneManager::ClearSomething()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_ClearSomething_mC8779E9BDFB3C60832AF9F7835BDA8B0AD914FCC (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_fMovement_39((0.0f));
		__this->set_m_fTimeElapse_38((0.0f));
		return;
	}
}
// System.Void UnlockNewPlaneManager::SetStatus(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_SetStatus_mECC26ADCD7BE7B54FB31C6465FC4BD4CF6D7884A (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, int32_t ___nStatus0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nStatus0;
		__this->set_m_nStatus_29(L_0);
		UnlockNewPlaneManager_ClearSomething_mC8779E9BDFB3C60832AF9F7835BDA8B0AD914FCC(__this, /*hidden argument*/NULL);
		__this->set_m_nCount_40(0);
		return;
	}
}
// System.Void UnlockNewPlaneManager::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_FixedUpdate_m8BD3CE8F06E1C25DC255B2E64DAEF59C487E2C30 (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * V_2 = NULL;
	int32_t V_3 = 0;
	{
		return;
	}
}
// System.Void UnlockNewPlaneManager::Update()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager_Update_mBEE7ED1A8521A6BBFA370222A3B8D42469B2188F (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method)
{
	{
		UnlockNewPlaneManager_Loop_NewVersion_mFC19BCACA343BEA3A1A473D122BB84DE40868327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnlockNewPlaneManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager__ctor_mEDBECCBFF99FAF208045C5296CDEC9A1B9EAECAD (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnlockNewPlaneManager__ctor_mEDBECCBFF99FAF208045C5296CDEC9A1B9EAECAD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_nStatus_29((-1));
		__this->set_m_nCurLevel_36(1);
		List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * L_0 = (List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 *)il2cpp_codegen_object_new(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650_il2cpp_TypeInfo_var);
		List_1__ctor_m70B8A20433AEEDEB942CD3EEC229497AB693E9D6(L_0, /*hidden argument*/List_1__ctor_m70B8A20433AEEDEB942CD3EEC229497AB693E9D6_RuntimeMethod_var);
		__this->set_m_lstEffects_37(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnlockNewPlaneManager::.cctor()
extern "C" IL2CPP_METHOD_ATTR void UnlockNewPlaneManager__cctor_m5AE77FF8136B6A155FEB3D1BFC942F102E5FB2AB (const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WeatherManager::Start()
extern "C" IL2CPP_METHOD_ATTR void WeatherManager_Start_mB8C7EE9248F45AA1BA5B563CB92AA78069B542D5 (WeatherManager_t77BC50C3104A8AAC79F5FEA210AC31B904ECD196 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void WeatherManager::Update()
extern "C" IL2CPP_METHOD_ATTR void WeatherManager_Update_m366F394746013983C3F67ACF1D896B1A11FEF377 (WeatherManager_t77BC50C3104A8AAC79F5FEA210AC31B904ECD196 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// SnowFlake WeatherManager::NewSnowFlake()
extern "C" IL2CPP_METHOD_ATTR SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70 * WeatherManager_NewSnowFlake_mFB9DF3C7E9501BDF52756311194B8765379D12E7 (WeatherManager_t77BC50C3104A8AAC79F5FEA210AC31B904ECD196 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeatherManager_NewSnowFlake_mFB9DF3C7E9501BDF52756311194B8765379D12E7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_m_preSnowFlake_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E(L_0, /*hidden argument*/Object_Instantiate_TisGameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_m598037C6F246E67DB3E38DFBB1F44D4D9921A85E_RuntimeMethod_var);
		NullCheck(L_1);
		SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70 * L_2 = GameObject_GetComponent_TisSnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_m4B69970D1C279F5DF4E82E0549BA7738FB538EF7(L_1, /*hidden argument*/GameObject_GetComponent_TisSnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_m4B69970D1C279F5DF4E82E0549BA7738FB538EF7_RuntimeMethod_var);
		return L_2;
	}
}
// System.Void WeatherManager::DeleteSnowFlake(SnowFlake)
extern "C" IL2CPP_METHOD_ATTR void WeatherManager_DeleteSnowFlake_mD97E3FFB7ED3A0AA60B8DC14AF9A186B0DF274DC (WeatherManager_t77BC50C3104A8AAC79F5FEA210AC31B904ECD196 * __this, SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70 * ___snow0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeatherManager_DeleteSnowFlake_mD97E3FFB7ED3A0AA60B8DC14AF9A186B0DF274DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70 * L_0 = ___snow0;
		NullCheck(L_0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WeatherManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WeatherManager__ctor_mB199C0A0D35B41F6495D80EA0B1B4A9CF6290A0A (WeatherManager_t77BC50C3104A8AAC79F5FEA210AC31B904ECD196 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void dou::.ctor()
extern "C" IL2CPP_METHOD_ATTR void dou__ctor_m22E6F2979020B9E23104DFECF2295BAB10393714 (dou_t6892A900BE5AC1C3D8B4DFBA12DC4445953E1EFD * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
