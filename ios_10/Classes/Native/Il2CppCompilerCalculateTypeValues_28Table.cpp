﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Admin
struct Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60;
// BaseRotate
struct BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377;
// BaseScale
struct BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063;
// CFrameAnimationEffect
struct CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10;
// District
struct District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A;
// ResearchCounter
struct ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD;
// SceneUiButton/OnClickEventHandler
struct OnClickEventHandler_t0FB1C8529204CFF772DD0008958C94A36A9ECB86;
// SharpJson.Lexer
struct Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122;
// Skill
struct Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF;
// Spine.Animation
struct Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482;
// Spine.AnimationState
struct AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0;
// Spine.AnimationState/TrackEntryDelegate
struct TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F;
// Spine.AnimationState/TrackEntryEventDelegate
struct TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE;
// Spine.AnimationStateData
struct AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C;
// Spine.AtlasPage
struct AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453;
// Spine.Atlas[]
struct AtlasU5BU5D_t077A4AE39606D7409AA99289C7AF0F14D48CC4DE;
// Spine.BoneData
struct BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F;
// Spine.Event
struct Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0;
// Spine.EventData
struct EventData_t34B08BE715A229D34906F96784419D669DE4869E;
// Spine.EventQueue
struct EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77;
// Spine.Event[]
struct EventU5BU5D_t318C0E0DC13E422E4E9A16FC8F841192027531C2;
// Spine.ExposedList`1<Spine.Bone>
struct ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D;
// Spine.ExposedList`1<Spine.Event>
struct ExposedList_1_t0883A674779C02F9CCBF02B511F843FBBD8CEE67;
// Spine.ExposedList`1<Spine.Timeline>
struct ExposedList_1_t153A965FDE7C7725701651562F0365BA79B57C19;
// Spine.ExposedList`1<Spine.TrackEntry>
struct ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99;
// Spine.ExposedList`1<System.Int32>
struct ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960;
// Spine.ExposedList`1<System.Single>
struct ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F;
// Spine.Pool`1<Spine.TrackEntry>
struct Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF;
// Spine.Skeleton
struct Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782;
// Spine.SkeletonData
struct SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20;
// Spine.SlotData
struct SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738;
// Spine.TextureLoader
struct TextureLoader_tB46FAA9CECE7DB7128F41ED10989314497364810;
// Spine.TrackEntry
struct TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276;
// Spine.Unity.SkeletonGraphic
struct SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A;
// Spine.VertexAttachment
struct VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631;
// SpineSkeletonFlipBehaviour
struct SpineSkeletonFlipBehaviour_t881D1AD276451B7A71EDB55CC0849B6F8E71EB5C;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Spine.AnimationStateData/AnimationPair,System.Single>
struct Dictionary_2_tA91CB3374A841ABDB78CF1560A50D9698B89B95C;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E;
// System.Collections.Generic.List`1<Spine.AtlasPage>
struct List_1_t78A0D289677B15C409C6933C6DD0AF07DDFDC06C;
// System.Collections.Generic.List`1<Spine.AtlasRegion>
struct List_1_t9A4AB41337F5672F3A305537420A5D664BA7120C;
// System.Collections.Generic.List`1<Spine.EventQueue/EventQueueEntry>
struct List_1_t180114C151E81B2D3C61133023D4DFD72AEC82A0;
// System.Collections.Generic.List`1<UIShoppinAndItemCounter>
struct List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t64BA96BFC713F221050385E91C868CE455C245D6;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.Single[][]
struct SingleU5BU5DU5BU5D_tDE1FB3BBC65F7A632E44302B85B7F7193EF48839;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UIAdventureProfitCounter[]
struct UIAdventureProfitCounterU5BU5D_tD2A07412D3D7D4B0C324E1F6A451DF229A11CABA;
// UIBigMapTrackInfo[]
struct UIBigMapTrackInfoU5BU5D_t62B8E0E3DB4206E3629B57C37ED5293BCB2975E2;
// UIBlock
struct UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B;
// UIProgressBar
struct UIProgressBar_t2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96;
// UIStars
struct UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB;
// UnityEngine.UI.CanvasScaler
struct CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188;
// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef JSONDECODER_T0C1C57352FB78AB1682A11F1E1E229F2B152DA62_H
#define JSONDECODER_T0C1C57352FB78AB1682A11F1E1E229F2B152DA62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpJson.JsonDecoder
struct  JsonDecoder_t0C1C57352FB78AB1682A11F1E1E229F2B152DA62  : public RuntimeObject
{
public:
	// System.String SharpJson.JsonDecoder::<errorMessage>k__BackingField
	String_t* ___U3CerrorMessageU3Ek__BackingField_0;
	// System.Boolean SharpJson.JsonDecoder::<parseNumbersAsFloat>k__BackingField
	bool ___U3CparseNumbersAsFloatU3Ek__BackingField_1;
	// SharpJson.Lexer SharpJson.JsonDecoder::lexer
	Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122 * ___lexer_2;

public:
	inline static int32_t get_offset_of_U3CerrorMessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonDecoder_t0C1C57352FB78AB1682A11F1E1E229F2B152DA62, ___U3CerrorMessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CerrorMessageU3Ek__BackingField_0() const { return ___U3CerrorMessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CerrorMessageU3Ek__BackingField_0() { return &___U3CerrorMessageU3Ek__BackingField_0; }
	inline void set_U3CerrorMessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CerrorMessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorMessageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonDecoder_t0C1C57352FB78AB1682A11F1E1E229F2B152DA62, ___U3CparseNumbersAsFloatU3Ek__BackingField_1)); }
	inline bool get_U3CparseNumbersAsFloatU3Ek__BackingField_1() const { return ___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return &___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline void set_U3CparseNumbersAsFloatU3Ek__BackingField_1(bool value)
	{
		___U3CparseNumbersAsFloatU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_lexer_2() { return static_cast<int32_t>(offsetof(JsonDecoder_t0C1C57352FB78AB1682A11F1E1E229F2B152DA62, ___lexer_2)); }
	inline Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122 * get_lexer_2() const { return ___lexer_2; }
	inline Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122 ** get_address_of_lexer_2() { return &___lexer_2; }
	inline void set_lexer_2(Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122 * value)
	{
		___lexer_2 = value;
		Il2CppCodeGenWriteBarrier((&___lexer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDECODER_T0C1C57352FB78AB1682A11F1E1E229F2B152DA62_H
#ifndef LEXER_T93D12C38ED5395C9FCB0C20C62C208E046DE1122_H
#define LEXER_T93D12C38ED5395C9FCB0C20C62C208E046DE1122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpJson.Lexer
struct  Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122  : public RuntimeObject
{
public:
	// System.Int32 SharpJson.Lexer::<lineNumber>k__BackingField
	int32_t ___U3ClineNumberU3Ek__BackingField_0;
	// System.Boolean SharpJson.Lexer::<parseNumbersAsFloat>k__BackingField
	bool ___U3CparseNumbersAsFloatU3Ek__BackingField_1;
	// System.Char[] SharpJson.Lexer::json
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___json_2;
	// System.Int32 SharpJson.Lexer::index
	int32_t ___index_3;
	// System.Boolean SharpJson.Lexer::success
	bool ___success_4;
	// System.Char[] SharpJson.Lexer::stringBuffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___stringBuffer_5;

public:
	inline static int32_t get_offset_of_U3ClineNumberU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122, ___U3ClineNumberU3Ek__BackingField_0)); }
	inline int32_t get_U3ClineNumberU3Ek__BackingField_0() const { return ___U3ClineNumberU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3ClineNumberU3Ek__BackingField_0() { return &___U3ClineNumberU3Ek__BackingField_0; }
	inline void set_U3ClineNumberU3Ek__BackingField_0(int32_t value)
	{
		___U3ClineNumberU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122, ___U3CparseNumbersAsFloatU3Ek__BackingField_1)); }
	inline bool get_U3CparseNumbersAsFloatU3Ek__BackingField_1() const { return ___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return &___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline void set_U3CparseNumbersAsFloatU3Ek__BackingField_1(bool value)
	{
		___U3CparseNumbersAsFloatU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_json_2() { return static_cast<int32_t>(offsetof(Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122, ___json_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_json_2() const { return ___json_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_json_2() { return &___json_2; }
	inline void set_json_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___json_2 = value;
		Il2CppCodeGenWriteBarrier((&___json_2), value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_success_4() { return static_cast<int32_t>(offsetof(Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122, ___success_4)); }
	inline bool get_success_4() const { return ___success_4; }
	inline bool* get_address_of_success_4() { return &___success_4; }
	inline void set_success_4(bool value)
	{
		___success_4 = value;
	}

	inline static int32_t get_offset_of_stringBuffer_5() { return static_cast<int32_t>(offsetof(Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122, ___stringBuffer_5)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_stringBuffer_5() const { return ___stringBuffer_5; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_stringBuffer_5() { return &___stringBuffer_5; }
	inline void set_stringBuffer_5(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___stringBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___stringBuffer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXER_T93D12C38ED5395C9FCB0C20C62C208E046DE1122_H
#ifndef ANIMATION_T7943402608B3C2228DDB4C2496C65B87CFD5B482_H
#define ANIMATION_T7943402608B3C2228DDB4C2496C65B87CFD5B482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Animation
struct  Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482  : public RuntimeObject
{
public:
	// Spine.ExposedList`1<Spine.Timeline> Spine.Animation::timelines
	ExposedList_1_t153A965FDE7C7725701651562F0365BA79B57C19 * ___timelines_0;
	// System.Single Spine.Animation::duration
	float ___duration_1;
	// System.String Spine.Animation::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_timelines_0() { return static_cast<int32_t>(offsetof(Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482, ___timelines_0)); }
	inline ExposedList_1_t153A965FDE7C7725701651562F0365BA79B57C19 * get_timelines_0() const { return ___timelines_0; }
	inline ExposedList_1_t153A965FDE7C7725701651562F0365BA79B57C19 ** get_address_of_timelines_0() { return &___timelines_0; }
	inline void set_timelines_0(ExposedList_1_t153A965FDE7C7725701651562F0365BA79B57C19 * value)
	{
		___timelines_0 = value;
		Il2CppCodeGenWriteBarrier((&___timelines_0), value);
	}

	inline static int32_t get_offset_of_duration_1() { return static_cast<int32_t>(offsetof(Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482, ___duration_1)); }
	inline float get_duration_1() const { return ___duration_1; }
	inline float* get_address_of_duration_1() { return &___duration_1; }
	inline void set_duration_1(float value)
	{
		___duration_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_T7943402608B3C2228DDB4C2496C65B87CFD5B482_H
#ifndef ANIMATIONSTATE_T8C505E02BE0DB4858362AA74C02FB30085250DE0_H
#define ANIMATIONSTATE_T8C505E02BE0DB4858362AA74C02FB30085250DE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState
struct  AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0  : public RuntimeObject
{
public:
	// Spine.AnimationStateData Spine.AnimationState::data
	AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C * ___data_5;
	// Spine.Pool`1<Spine.TrackEntry> Spine.AnimationState::trackEntryPool
	Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF * ___trackEntryPool_6;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.AnimationState::tracks
	ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * ___tracks_7;
	// Spine.ExposedList`1<Spine.Event> Spine.AnimationState::events
	ExposedList_1_t0883A674779C02F9CCBF02B511F843FBBD8CEE67 * ___events_8;
	// Spine.EventQueue Spine.AnimationState::queue
	EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77 * ___queue_9;
	// System.Collections.Generic.HashSet`1<System.Int32> Spine.AnimationState::propertyIDs
	HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * ___propertyIDs_10;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.AnimationState::mixingTo
	ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * ___mixingTo_11;
	// System.Boolean Spine.AnimationState::animationsChanged
	bool ___animationsChanged_12;
	// System.Single Spine.AnimationState::timeScale
	float ___timeScale_13;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Start
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Start_14;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Interrupt
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Interrupt_15;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::End
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___End_16;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Dispose
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Dispose_17;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Complete
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Complete_18;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.AnimationState::Event
	TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE * ___Event_19;

public:
	inline static int32_t get_offset_of_data_5() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___data_5)); }
	inline AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C * get_data_5() const { return ___data_5; }
	inline AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C ** get_address_of_data_5() { return &___data_5; }
	inline void set_data_5(AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C * value)
	{
		___data_5 = value;
		Il2CppCodeGenWriteBarrier((&___data_5), value);
	}

	inline static int32_t get_offset_of_trackEntryPool_6() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___trackEntryPool_6)); }
	inline Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF * get_trackEntryPool_6() const { return ___trackEntryPool_6; }
	inline Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF ** get_address_of_trackEntryPool_6() { return &___trackEntryPool_6; }
	inline void set_trackEntryPool_6(Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF * value)
	{
		___trackEntryPool_6 = value;
		Il2CppCodeGenWriteBarrier((&___trackEntryPool_6), value);
	}

	inline static int32_t get_offset_of_tracks_7() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___tracks_7)); }
	inline ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * get_tracks_7() const { return ___tracks_7; }
	inline ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 ** get_address_of_tracks_7() { return &___tracks_7; }
	inline void set_tracks_7(ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * value)
	{
		___tracks_7 = value;
		Il2CppCodeGenWriteBarrier((&___tracks_7), value);
	}

	inline static int32_t get_offset_of_events_8() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___events_8)); }
	inline ExposedList_1_t0883A674779C02F9CCBF02B511F843FBBD8CEE67 * get_events_8() const { return ___events_8; }
	inline ExposedList_1_t0883A674779C02F9CCBF02B511F843FBBD8CEE67 ** get_address_of_events_8() { return &___events_8; }
	inline void set_events_8(ExposedList_1_t0883A674779C02F9CCBF02B511F843FBBD8CEE67 * value)
	{
		___events_8 = value;
		Il2CppCodeGenWriteBarrier((&___events_8), value);
	}

	inline static int32_t get_offset_of_queue_9() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___queue_9)); }
	inline EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77 * get_queue_9() const { return ___queue_9; }
	inline EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77 ** get_address_of_queue_9() { return &___queue_9; }
	inline void set_queue_9(EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77 * value)
	{
		___queue_9 = value;
		Il2CppCodeGenWriteBarrier((&___queue_9), value);
	}

	inline static int32_t get_offset_of_propertyIDs_10() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___propertyIDs_10)); }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * get_propertyIDs_10() const { return ___propertyIDs_10; }
	inline HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E ** get_address_of_propertyIDs_10() { return &___propertyIDs_10; }
	inline void set_propertyIDs_10(HashSet_1_tD16423F193A61077DD5FE7C8517877716AAFF11E * value)
	{
		___propertyIDs_10 = value;
		Il2CppCodeGenWriteBarrier((&___propertyIDs_10), value);
	}

	inline static int32_t get_offset_of_mixingTo_11() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___mixingTo_11)); }
	inline ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * get_mixingTo_11() const { return ___mixingTo_11; }
	inline ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 ** get_address_of_mixingTo_11() { return &___mixingTo_11; }
	inline void set_mixingTo_11(ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * value)
	{
		___mixingTo_11 = value;
		Il2CppCodeGenWriteBarrier((&___mixingTo_11), value);
	}

	inline static int32_t get_offset_of_animationsChanged_12() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___animationsChanged_12)); }
	inline bool get_animationsChanged_12() const { return ___animationsChanged_12; }
	inline bool* get_address_of_animationsChanged_12() { return &___animationsChanged_12; }
	inline void set_animationsChanged_12(bool value)
	{
		___animationsChanged_12 = value;
	}

	inline static int32_t get_offset_of_timeScale_13() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___timeScale_13)); }
	inline float get_timeScale_13() const { return ___timeScale_13; }
	inline float* get_address_of_timeScale_13() { return &___timeScale_13; }
	inline void set_timeScale_13(float value)
	{
		___timeScale_13 = value;
	}

	inline static int32_t get_offset_of_Start_14() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___Start_14)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Start_14() const { return ___Start_14; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Start_14() { return &___Start_14; }
	inline void set_Start_14(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Start_14 = value;
		Il2CppCodeGenWriteBarrier((&___Start_14), value);
	}

	inline static int32_t get_offset_of_Interrupt_15() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___Interrupt_15)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Interrupt_15() const { return ___Interrupt_15; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Interrupt_15() { return &___Interrupt_15; }
	inline void set_Interrupt_15(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Interrupt_15 = value;
		Il2CppCodeGenWriteBarrier((&___Interrupt_15), value);
	}

	inline static int32_t get_offset_of_End_16() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___End_16)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_End_16() const { return ___End_16; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_End_16() { return &___End_16; }
	inline void set_End_16(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___End_16 = value;
		Il2CppCodeGenWriteBarrier((&___End_16), value);
	}

	inline static int32_t get_offset_of_Dispose_17() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___Dispose_17)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Dispose_17() const { return ___Dispose_17; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Dispose_17() { return &___Dispose_17; }
	inline void set_Dispose_17(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Dispose_17 = value;
		Il2CppCodeGenWriteBarrier((&___Dispose_17), value);
	}

	inline static int32_t get_offset_of_Complete_18() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___Complete_18)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Complete_18() const { return ___Complete_18; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Complete_18() { return &___Complete_18; }
	inline void set_Complete_18(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Complete_18 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_18), value);
	}

	inline static int32_t get_offset_of_Event_19() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0, ___Event_19)); }
	inline TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE * get_Event_19() const { return ___Event_19; }
	inline TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE ** get_address_of_Event_19() { return &___Event_19; }
	inline void set_Event_19(TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE * value)
	{
		___Event_19 = value;
		Il2CppCodeGenWriteBarrier((&___Event_19), value);
	}
};

struct AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0_StaticFields
{
public:
	// Spine.Animation Spine.AnimationState::EmptyAnimation
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * ___EmptyAnimation_0;

public:
	inline static int32_t get_offset_of_EmptyAnimation_0() { return static_cast<int32_t>(offsetof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0_StaticFields, ___EmptyAnimation_0)); }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * get_EmptyAnimation_0() const { return ___EmptyAnimation_0; }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 ** get_address_of_EmptyAnimation_0() { return &___EmptyAnimation_0; }
	inline void set_EmptyAnimation_0(Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * value)
	{
		___EmptyAnimation_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyAnimation_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATE_T8C505E02BE0DB4858362AA74C02FB30085250DE0_H
#ifndef ANIMATIONSTATEDATA_T1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C_H
#define ANIMATIONSTATEDATA_T1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationStateData
struct  AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C  : public RuntimeObject
{
public:
	// Spine.SkeletonData Spine.AnimationStateData::skeletonData
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 * ___skeletonData_0;
	// System.Collections.Generic.Dictionary`2<Spine.AnimationStateData/AnimationPair,System.Single> Spine.AnimationStateData::animationToMixTime
	Dictionary_2_tA91CB3374A841ABDB78CF1560A50D9698B89B95C * ___animationToMixTime_1;
	// System.Single Spine.AnimationStateData::defaultMix
	float ___defaultMix_2;

public:
	inline static int32_t get_offset_of_skeletonData_0() { return static_cast<int32_t>(offsetof(AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C, ___skeletonData_0)); }
	inline SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 * get_skeletonData_0() const { return ___skeletonData_0; }
	inline SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 ** get_address_of_skeletonData_0() { return &___skeletonData_0; }
	inline void set_skeletonData_0(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 * value)
	{
		___skeletonData_0 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonData_0), value);
	}

	inline static int32_t get_offset_of_animationToMixTime_1() { return static_cast<int32_t>(offsetof(AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C, ___animationToMixTime_1)); }
	inline Dictionary_2_tA91CB3374A841ABDB78CF1560A50D9698B89B95C * get_animationToMixTime_1() const { return ___animationToMixTime_1; }
	inline Dictionary_2_tA91CB3374A841ABDB78CF1560A50D9698B89B95C ** get_address_of_animationToMixTime_1() { return &___animationToMixTime_1; }
	inline void set_animationToMixTime_1(Dictionary_2_tA91CB3374A841ABDB78CF1560A50D9698B89B95C * value)
	{
		___animationToMixTime_1 = value;
		Il2CppCodeGenWriteBarrier((&___animationToMixTime_1), value);
	}

	inline static int32_t get_offset_of_defaultMix_2() { return static_cast<int32_t>(offsetof(AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C, ___defaultMix_2)); }
	inline float get_defaultMix_2() const { return ___defaultMix_2; }
	inline float* get_address_of_defaultMix_2() { return &___defaultMix_2; }
	inline void set_defaultMix_2(float value)
	{
		___defaultMix_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATEDATA_T1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C_H
#ifndef ANIMATIONPAIRCOMPARER_T65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE_H
#define ANIMATIONPAIRCOMPARER_T65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationStateData/AnimationPairComparer
struct  AnimationPairComparer_t65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE  : public RuntimeObject
{
public:

public:
};

struct AnimationPairComparer_t65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE_StaticFields
{
public:
	// Spine.AnimationStateData/AnimationPairComparer Spine.AnimationStateData/AnimationPairComparer::Instance
	AnimationPairComparer_t65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(AnimationPairComparer_t65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE_StaticFields, ___Instance_0)); }
	inline AnimationPairComparer_t65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE * get_Instance_0() const { return ___Instance_0; }
	inline AnimationPairComparer_t65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(AnimationPairComparer_t65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPAIRCOMPARER_T65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE_H
#ifndef ATLAS_T59791993EEB5BEAEAF07A47B141C848194676CE4_H
#define ATLAS_T59791993EEB5BEAEAF07A47B141C848194676CE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Atlas
struct  Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Spine.AtlasPage> Spine.Atlas::pages
	List_1_t78A0D289677B15C409C6933C6DD0AF07DDFDC06C * ___pages_0;
	// System.Collections.Generic.List`1<Spine.AtlasRegion> Spine.Atlas::regions
	List_1_t9A4AB41337F5672F3A305537420A5D664BA7120C * ___regions_1;
	// Spine.TextureLoader Spine.Atlas::textureLoader
	RuntimeObject* ___textureLoader_2;

public:
	inline static int32_t get_offset_of_pages_0() { return static_cast<int32_t>(offsetof(Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4, ___pages_0)); }
	inline List_1_t78A0D289677B15C409C6933C6DD0AF07DDFDC06C * get_pages_0() const { return ___pages_0; }
	inline List_1_t78A0D289677B15C409C6933C6DD0AF07DDFDC06C ** get_address_of_pages_0() { return &___pages_0; }
	inline void set_pages_0(List_1_t78A0D289677B15C409C6933C6DD0AF07DDFDC06C * value)
	{
		___pages_0 = value;
		Il2CppCodeGenWriteBarrier((&___pages_0), value);
	}

	inline static int32_t get_offset_of_regions_1() { return static_cast<int32_t>(offsetof(Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4, ___regions_1)); }
	inline List_1_t9A4AB41337F5672F3A305537420A5D664BA7120C * get_regions_1() const { return ___regions_1; }
	inline List_1_t9A4AB41337F5672F3A305537420A5D664BA7120C ** get_address_of_regions_1() { return &___regions_1; }
	inline void set_regions_1(List_1_t9A4AB41337F5672F3A305537420A5D664BA7120C * value)
	{
		___regions_1 = value;
		Il2CppCodeGenWriteBarrier((&___regions_1), value);
	}

	inline static int32_t get_offset_of_textureLoader_2() { return static_cast<int32_t>(offsetof(Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4, ___textureLoader_2)); }
	inline RuntimeObject* get_textureLoader_2() const { return ___textureLoader_2; }
	inline RuntimeObject** get_address_of_textureLoader_2() { return &___textureLoader_2; }
	inline void set_textureLoader_2(RuntimeObject* value)
	{
		___textureLoader_2 = value;
		Il2CppCodeGenWriteBarrier((&___textureLoader_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLAS_T59791993EEB5BEAEAF07A47B141C848194676CE4_H
#ifndef ATLASATTACHMENTLOADER_T38697609F0F3ABFC7F18657ADD40BFAA6131163D_H
#define ATLASATTACHMENTLOADER_T38697609F0F3ABFC7F18657ADD40BFAA6131163D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AtlasAttachmentLoader
struct  AtlasAttachmentLoader_t38697609F0F3ABFC7F18657ADD40BFAA6131163D  : public RuntimeObject
{
public:
	// Spine.Atlas[] Spine.AtlasAttachmentLoader::atlasArray
	AtlasU5BU5D_t077A4AE39606D7409AA99289C7AF0F14D48CC4DE* ___atlasArray_0;

public:
	inline static int32_t get_offset_of_atlasArray_0() { return static_cast<int32_t>(offsetof(AtlasAttachmentLoader_t38697609F0F3ABFC7F18657ADD40BFAA6131163D, ___atlasArray_0)); }
	inline AtlasU5BU5D_t077A4AE39606D7409AA99289C7AF0F14D48CC4DE* get_atlasArray_0() const { return ___atlasArray_0; }
	inline AtlasU5BU5D_t077A4AE39606D7409AA99289C7AF0F14D48CC4DE** get_address_of_atlasArray_0() { return &___atlasArray_0; }
	inline void set_atlasArray_0(AtlasU5BU5D_t077A4AE39606D7409AA99289C7AF0F14D48CC4DE* value)
	{
		___atlasArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASATTACHMENTLOADER_T38697609F0F3ABFC7F18657ADD40BFAA6131163D_H
#ifndef ATLASREGION_TDC32F693E116CBB7C9DD128D1D9A168BA00AEF30_H
#define ATLASREGION_TDC32F693E116CBB7C9DD128D1D9A168BA00AEF30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AtlasRegion
struct  AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30  : public RuntimeObject
{
public:
	// Spine.AtlasPage Spine.AtlasRegion::page
	AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453 * ___page_0;
	// System.String Spine.AtlasRegion::name
	String_t* ___name_1;
	// System.Int32 Spine.AtlasRegion::x
	int32_t ___x_2;
	// System.Int32 Spine.AtlasRegion::y
	int32_t ___y_3;
	// System.Int32 Spine.AtlasRegion::width
	int32_t ___width_4;
	// System.Int32 Spine.AtlasRegion::height
	int32_t ___height_5;
	// System.Single Spine.AtlasRegion::u
	float ___u_6;
	// System.Single Spine.AtlasRegion::v
	float ___v_7;
	// System.Single Spine.AtlasRegion::u2
	float ___u2_8;
	// System.Single Spine.AtlasRegion::v2
	float ___v2_9;
	// System.Single Spine.AtlasRegion::offsetX
	float ___offsetX_10;
	// System.Single Spine.AtlasRegion::offsetY
	float ___offsetY_11;
	// System.Int32 Spine.AtlasRegion::originalWidth
	int32_t ___originalWidth_12;
	// System.Int32 Spine.AtlasRegion::originalHeight
	int32_t ___originalHeight_13;
	// System.Int32 Spine.AtlasRegion::index
	int32_t ___index_14;
	// System.Boolean Spine.AtlasRegion::rotate
	bool ___rotate_15;
	// System.Int32[] Spine.AtlasRegion::splits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___splits_16;
	// System.Int32[] Spine.AtlasRegion::pads
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___pads_17;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___page_0)); }
	inline AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453 * get_page_0() const { return ___page_0; }
	inline AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453 ** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453 * value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier((&___page_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___x_2)); }
	inline int32_t get_x_2() const { return ___x_2; }
	inline int32_t* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(int32_t value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___y_3)); }
	inline int32_t get_y_3() const { return ___y_3; }
	inline int32_t* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(int32_t value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___width_4)); }
	inline int32_t get_width_4() const { return ___width_4; }
	inline int32_t* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(int32_t value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_height_5() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___height_5)); }
	inline int32_t get_height_5() const { return ___height_5; }
	inline int32_t* get_address_of_height_5() { return &___height_5; }
	inline void set_height_5(int32_t value)
	{
		___height_5 = value;
	}

	inline static int32_t get_offset_of_u_6() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___u_6)); }
	inline float get_u_6() const { return ___u_6; }
	inline float* get_address_of_u_6() { return &___u_6; }
	inline void set_u_6(float value)
	{
		___u_6 = value;
	}

	inline static int32_t get_offset_of_v_7() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___v_7)); }
	inline float get_v_7() const { return ___v_7; }
	inline float* get_address_of_v_7() { return &___v_7; }
	inline void set_v_7(float value)
	{
		___v_7 = value;
	}

	inline static int32_t get_offset_of_u2_8() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___u2_8)); }
	inline float get_u2_8() const { return ___u2_8; }
	inline float* get_address_of_u2_8() { return &___u2_8; }
	inline void set_u2_8(float value)
	{
		___u2_8 = value;
	}

	inline static int32_t get_offset_of_v2_9() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___v2_9)); }
	inline float get_v2_9() const { return ___v2_9; }
	inline float* get_address_of_v2_9() { return &___v2_9; }
	inline void set_v2_9(float value)
	{
		___v2_9 = value;
	}

	inline static int32_t get_offset_of_offsetX_10() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___offsetX_10)); }
	inline float get_offsetX_10() const { return ___offsetX_10; }
	inline float* get_address_of_offsetX_10() { return &___offsetX_10; }
	inline void set_offsetX_10(float value)
	{
		___offsetX_10 = value;
	}

	inline static int32_t get_offset_of_offsetY_11() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___offsetY_11)); }
	inline float get_offsetY_11() const { return ___offsetY_11; }
	inline float* get_address_of_offsetY_11() { return &___offsetY_11; }
	inline void set_offsetY_11(float value)
	{
		___offsetY_11 = value;
	}

	inline static int32_t get_offset_of_originalWidth_12() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___originalWidth_12)); }
	inline int32_t get_originalWidth_12() const { return ___originalWidth_12; }
	inline int32_t* get_address_of_originalWidth_12() { return &___originalWidth_12; }
	inline void set_originalWidth_12(int32_t value)
	{
		___originalWidth_12 = value;
	}

	inline static int32_t get_offset_of_originalHeight_13() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___originalHeight_13)); }
	inline int32_t get_originalHeight_13() const { return ___originalHeight_13; }
	inline int32_t* get_address_of_originalHeight_13() { return &___originalHeight_13; }
	inline void set_originalHeight_13(int32_t value)
	{
		___originalHeight_13 = value;
	}

	inline static int32_t get_offset_of_index_14() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___index_14)); }
	inline int32_t get_index_14() const { return ___index_14; }
	inline int32_t* get_address_of_index_14() { return &___index_14; }
	inline void set_index_14(int32_t value)
	{
		___index_14 = value;
	}

	inline static int32_t get_offset_of_rotate_15() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___rotate_15)); }
	inline bool get_rotate_15() const { return ___rotate_15; }
	inline bool* get_address_of_rotate_15() { return &___rotate_15; }
	inline void set_rotate_15(bool value)
	{
		___rotate_15 = value;
	}

	inline static int32_t get_offset_of_splits_16() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___splits_16)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_splits_16() const { return ___splits_16; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_splits_16() { return &___splits_16; }
	inline void set_splits_16(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___splits_16 = value;
		Il2CppCodeGenWriteBarrier((&___splits_16), value);
	}

	inline static int32_t get_offset_of_pads_17() { return static_cast<int32_t>(offsetof(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30, ___pads_17)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_pads_17() const { return ___pads_17; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_pads_17() { return &___pads_17; }
	inline void set_pads_17(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___pads_17 = value;
		Il2CppCodeGenWriteBarrier((&___pads_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASREGION_TDC32F693E116CBB7C9DD128D1D9A168BA00AEF30_H
#ifndef ATTACHMENT_TCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92_H
#define ATTACHMENT_TCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Attachment
struct  Attachment_tCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92  : public RuntimeObject
{
public:
	// System.String Spine.Attachment::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Attachment_tCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENT_TCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92_H
#ifndef ATTACHMENTTIMELINE_T8770B00FFFE84AE5CFA264C26D56C15BEF924A2A_H
#define ATTACHMENTTIMELINE_T8770B00FFFE84AE5CFA264C26D56C15BEF924A2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AttachmentTimeline
struct  AttachmentTimeline_t8770B00FFFE84AE5CFA264C26D56C15BEF924A2A  : public RuntimeObject
{
public:
	// System.Int32 Spine.AttachmentTimeline::slotIndex
	int32_t ___slotIndex_0;
	// System.Single[] Spine.AttachmentTimeline::frames
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___frames_1;
	// System.String[] Spine.AttachmentTimeline::attachmentNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___attachmentNames_2;

public:
	inline static int32_t get_offset_of_slotIndex_0() { return static_cast<int32_t>(offsetof(AttachmentTimeline_t8770B00FFFE84AE5CFA264C26D56C15BEF924A2A, ___slotIndex_0)); }
	inline int32_t get_slotIndex_0() const { return ___slotIndex_0; }
	inline int32_t* get_address_of_slotIndex_0() { return &___slotIndex_0; }
	inline void set_slotIndex_0(int32_t value)
	{
		___slotIndex_0 = value;
	}

	inline static int32_t get_offset_of_frames_1() { return static_cast<int32_t>(offsetof(AttachmentTimeline_t8770B00FFFE84AE5CFA264C26D56C15BEF924A2A, ___frames_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_frames_1() const { return ___frames_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_frames_1() { return &___frames_1; }
	inline void set_frames_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___frames_1 = value;
		Il2CppCodeGenWriteBarrier((&___frames_1), value);
	}

	inline static int32_t get_offset_of_attachmentNames_2() { return static_cast<int32_t>(offsetof(AttachmentTimeline_t8770B00FFFE84AE5CFA264C26D56C15BEF924A2A, ___attachmentNames_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_attachmentNames_2() const { return ___attachmentNames_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_attachmentNames_2() { return &___attachmentNames_2; }
	inline void set_attachmentNames_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___attachmentNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentNames_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTTIMELINE_T8770B00FFFE84AE5CFA264C26D56C15BEF924A2A_H
#ifndef BONE_T97C936A57AA46DB1B135D3853C7223DE467A799E_H
#define BONE_T97C936A57AA46DB1B135D3853C7223DE467A799E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Bone
struct  Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E  : public RuntimeObject
{
public:
	// Spine.BoneData Spine.Bone::data
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * ___data_1;
	// Spine.Skeleton Spine.Bone::skeleton
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * ___skeleton_2;
	// Spine.Bone Spine.Bone::parent
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * ___parent_3;
	// Spine.ExposedList`1<Spine.Bone> Spine.Bone::children
	ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * ___children_4;
	// System.Single Spine.Bone::x
	float ___x_5;
	// System.Single Spine.Bone::y
	float ___y_6;
	// System.Single Spine.Bone::rotation
	float ___rotation_7;
	// System.Single Spine.Bone::scaleX
	float ___scaleX_8;
	// System.Single Spine.Bone::scaleY
	float ___scaleY_9;
	// System.Single Spine.Bone::shearX
	float ___shearX_10;
	// System.Single Spine.Bone::shearY
	float ___shearY_11;
	// System.Single Spine.Bone::ax
	float ___ax_12;
	// System.Single Spine.Bone::ay
	float ___ay_13;
	// System.Single Spine.Bone::arotation
	float ___arotation_14;
	// System.Single Spine.Bone::ascaleX
	float ___ascaleX_15;
	// System.Single Spine.Bone::ascaleY
	float ___ascaleY_16;
	// System.Single Spine.Bone::ashearX
	float ___ashearX_17;
	// System.Single Spine.Bone::ashearY
	float ___ashearY_18;
	// System.Boolean Spine.Bone::appliedValid
	bool ___appliedValid_19;
	// System.Single Spine.Bone::a
	float ___a_20;
	// System.Single Spine.Bone::b
	float ___b_21;
	// System.Single Spine.Bone::worldX
	float ___worldX_22;
	// System.Single Spine.Bone::c
	float ___c_23;
	// System.Single Spine.Bone::d
	float ___d_24;
	// System.Single Spine.Bone::worldY
	float ___worldY_25;
	// System.Boolean Spine.Bone::sorted
	bool ___sorted_26;

public:
	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___data_1)); }
	inline BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * get_data_1() const { return ___data_1; }
	inline BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_skeleton_2() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___skeleton_2)); }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * get_skeleton_2() const { return ___skeleton_2; }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 ** get_address_of_skeleton_2() { return &___skeleton_2; }
	inline void set_skeleton_2(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * value)
	{
		___skeleton_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_2), value);
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___parent_3)); }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * get_parent_3() const { return ___parent_3; }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((&___parent_3), value);
	}

	inline static int32_t get_offset_of_children_4() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___children_4)); }
	inline ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * get_children_4() const { return ___children_4; }
	inline ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D ** get_address_of_children_4() { return &___children_4; }
	inline void set_children_4(ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * value)
	{
		___children_4 = value;
		Il2CppCodeGenWriteBarrier((&___children_4), value);
	}

	inline static int32_t get_offset_of_x_5() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___x_5)); }
	inline float get_x_5() const { return ___x_5; }
	inline float* get_address_of_x_5() { return &___x_5; }
	inline void set_x_5(float value)
	{
		___x_5 = value;
	}

	inline static int32_t get_offset_of_y_6() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___y_6)); }
	inline float get_y_6() const { return ___y_6; }
	inline float* get_address_of_y_6() { return &___y_6; }
	inline void set_y_6(float value)
	{
		___y_6 = value;
	}

	inline static int32_t get_offset_of_rotation_7() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___rotation_7)); }
	inline float get_rotation_7() const { return ___rotation_7; }
	inline float* get_address_of_rotation_7() { return &___rotation_7; }
	inline void set_rotation_7(float value)
	{
		___rotation_7 = value;
	}

	inline static int32_t get_offset_of_scaleX_8() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___scaleX_8)); }
	inline float get_scaleX_8() const { return ___scaleX_8; }
	inline float* get_address_of_scaleX_8() { return &___scaleX_8; }
	inline void set_scaleX_8(float value)
	{
		___scaleX_8 = value;
	}

	inline static int32_t get_offset_of_scaleY_9() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___scaleY_9)); }
	inline float get_scaleY_9() const { return ___scaleY_9; }
	inline float* get_address_of_scaleY_9() { return &___scaleY_9; }
	inline void set_scaleY_9(float value)
	{
		___scaleY_9 = value;
	}

	inline static int32_t get_offset_of_shearX_10() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___shearX_10)); }
	inline float get_shearX_10() const { return ___shearX_10; }
	inline float* get_address_of_shearX_10() { return &___shearX_10; }
	inline void set_shearX_10(float value)
	{
		___shearX_10 = value;
	}

	inline static int32_t get_offset_of_shearY_11() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___shearY_11)); }
	inline float get_shearY_11() const { return ___shearY_11; }
	inline float* get_address_of_shearY_11() { return &___shearY_11; }
	inline void set_shearY_11(float value)
	{
		___shearY_11 = value;
	}

	inline static int32_t get_offset_of_ax_12() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___ax_12)); }
	inline float get_ax_12() const { return ___ax_12; }
	inline float* get_address_of_ax_12() { return &___ax_12; }
	inline void set_ax_12(float value)
	{
		___ax_12 = value;
	}

	inline static int32_t get_offset_of_ay_13() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___ay_13)); }
	inline float get_ay_13() const { return ___ay_13; }
	inline float* get_address_of_ay_13() { return &___ay_13; }
	inline void set_ay_13(float value)
	{
		___ay_13 = value;
	}

	inline static int32_t get_offset_of_arotation_14() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___arotation_14)); }
	inline float get_arotation_14() const { return ___arotation_14; }
	inline float* get_address_of_arotation_14() { return &___arotation_14; }
	inline void set_arotation_14(float value)
	{
		___arotation_14 = value;
	}

	inline static int32_t get_offset_of_ascaleX_15() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___ascaleX_15)); }
	inline float get_ascaleX_15() const { return ___ascaleX_15; }
	inline float* get_address_of_ascaleX_15() { return &___ascaleX_15; }
	inline void set_ascaleX_15(float value)
	{
		___ascaleX_15 = value;
	}

	inline static int32_t get_offset_of_ascaleY_16() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___ascaleY_16)); }
	inline float get_ascaleY_16() const { return ___ascaleY_16; }
	inline float* get_address_of_ascaleY_16() { return &___ascaleY_16; }
	inline void set_ascaleY_16(float value)
	{
		___ascaleY_16 = value;
	}

	inline static int32_t get_offset_of_ashearX_17() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___ashearX_17)); }
	inline float get_ashearX_17() const { return ___ashearX_17; }
	inline float* get_address_of_ashearX_17() { return &___ashearX_17; }
	inline void set_ashearX_17(float value)
	{
		___ashearX_17 = value;
	}

	inline static int32_t get_offset_of_ashearY_18() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___ashearY_18)); }
	inline float get_ashearY_18() const { return ___ashearY_18; }
	inline float* get_address_of_ashearY_18() { return &___ashearY_18; }
	inline void set_ashearY_18(float value)
	{
		___ashearY_18 = value;
	}

	inline static int32_t get_offset_of_appliedValid_19() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___appliedValid_19)); }
	inline bool get_appliedValid_19() const { return ___appliedValid_19; }
	inline bool* get_address_of_appliedValid_19() { return &___appliedValid_19; }
	inline void set_appliedValid_19(bool value)
	{
		___appliedValid_19 = value;
	}

	inline static int32_t get_offset_of_a_20() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___a_20)); }
	inline float get_a_20() const { return ___a_20; }
	inline float* get_address_of_a_20() { return &___a_20; }
	inline void set_a_20(float value)
	{
		___a_20 = value;
	}

	inline static int32_t get_offset_of_b_21() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___b_21)); }
	inline float get_b_21() const { return ___b_21; }
	inline float* get_address_of_b_21() { return &___b_21; }
	inline void set_b_21(float value)
	{
		___b_21 = value;
	}

	inline static int32_t get_offset_of_worldX_22() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___worldX_22)); }
	inline float get_worldX_22() const { return ___worldX_22; }
	inline float* get_address_of_worldX_22() { return &___worldX_22; }
	inline void set_worldX_22(float value)
	{
		___worldX_22 = value;
	}

	inline static int32_t get_offset_of_c_23() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___c_23)); }
	inline float get_c_23() const { return ___c_23; }
	inline float* get_address_of_c_23() { return &___c_23; }
	inline void set_c_23(float value)
	{
		___c_23 = value;
	}

	inline static int32_t get_offset_of_d_24() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___d_24)); }
	inline float get_d_24() const { return ___d_24; }
	inline float* get_address_of_d_24() { return &___d_24; }
	inline void set_d_24(float value)
	{
		___d_24 = value;
	}

	inline static int32_t get_offset_of_worldY_25() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___worldY_25)); }
	inline float get_worldY_25() const { return ___worldY_25; }
	inline float* get_address_of_worldY_25() { return &___worldY_25; }
	inline void set_worldY_25(float value)
	{
		___worldY_25 = value;
	}

	inline static int32_t get_offset_of_sorted_26() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E, ___sorted_26)); }
	inline bool get_sorted_26() const { return ___sorted_26; }
	inline bool* get_address_of_sorted_26() { return &___sorted_26; }
	inline void set_sorted_26(bool value)
	{
		___sorted_26 = value;
	}
};

struct Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E_StaticFields
{
public:
	// System.Boolean Spine.Bone::yDown
	bool ___yDown_0;

public:
	inline static int32_t get_offset_of_yDown_0() { return static_cast<int32_t>(offsetof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E_StaticFields, ___yDown_0)); }
	inline bool get_yDown_0() const { return ___yDown_0; }
	inline bool* get_address_of_yDown_0() { return &___yDown_0; }
	inline void set_yDown_0(bool value)
	{
		___yDown_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONE_T97C936A57AA46DB1B135D3853C7223DE467A799E_H
#ifndef CURVETIMELINE_TF597BE88014A4EA50D23D6D5394E7786C19A153E_H
#define CURVETIMELINE_TF597BE88014A4EA50D23D6D5394E7786C19A153E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.CurveTimeline
struct  CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E  : public RuntimeObject
{
public:
	// System.Single[] Spine.CurveTimeline::curves
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___curves_4;

public:
	inline static int32_t get_offset_of_curves_4() { return static_cast<int32_t>(offsetof(CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E, ___curves_4)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_curves_4() const { return ___curves_4; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_curves_4() { return &___curves_4; }
	inline void set_curves_4(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___curves_4 = value;
		Il2CppCodeGenWriteBarrier((&___curves_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVETIMELINE_TF597BE88014A4EA50D23D6D5394E7786C19A153E_H
#ifndef DRAWORDERTIMELINE_T4604A14ED147349C259089CA32C00D526E32A98A_H
#define DRAWORDERTIMELINE_T4604A14ED147349C259089CA32C00D526E32A98A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.DrawOrderTimeline
struct  DrawOrderTimeline_t4604A14ED147349C259089CA32C00D526E32A98A  : public RuntimeObject
{
public:
	// System.Single[] Spine.DrawOrderTimeline::frames
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___frames_0;
	// System.Int32[][] Spine.DrawOrderTimeline::drawOrders
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___drawOrders_1;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(DrawOrderTimeline_t4604A14ED147349C259089CA32C00D526E32A98A, ___frames_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_frames_0() const { return ___frames_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}

	inline static int32_t get_offset_of_drawOrders_1() { return static_cast<int32_t>(offsetof(DrawOrderTimeline_t4604A14ED147349C259089CA32C00D526E32A98A, ___drawOrders_1)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_drawOrders_1() const { return ___drawOrders_1; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_drawOrders_1() { return &___drawOrders_1; }
	inline void set_drawOrders_1(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___drawOrders_1 = value;
		Il2CppCodeGenWriteBarrier((&___drawOrders_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWORDERTIMELINE_T4604A14ED147349C259089CA32C00D526E32A98A_H
#ifndef EVENT_TC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0_H
#define EVENT_TC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Event
struct  Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0  : public RuntimeObject
{
public:
	// Spine.EventData Spine.Event::data
	EventData_t34B08BE715A229D34906F96784419D669DE4869E * ___data_0;
	// System.Single Spine.Event::time
	float ___time_1;
	// System.Int32 Spine.Event::intValue
	int32_t ___intValue_2;
	// System.Single Spine.Event::floatValue
	float ___floatValue_3;
	// System.String Spine.Event::stringValue
	String_t* ___stringValue_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0, ___data_0)); }
	inline EventData_t34B08BE715A229D34906F96784419D669DE4869E * get_data_0() const { return ___data_0; }
	inline EventData_t34B08BE715A229D34906F96784419D669DE4869E ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(EventData_t34B08BE715A229D34906F96784419D669DE4869E * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0, ___time_1)); }
	inline float get_time_1() const { return ___time_1; }
	inline float* get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(float value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_intValue_2() { return static_cast<int32_t>(offsetof(Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0, ___intValue_2)); }
	inline int32_t get_intValue_2() const { return ___intValue_2; }
	inline int32_t* get_address_of_intValue_2() { return &___intValue_2; }
	inline void set_intValue_2(int32_t value)
	{
		___intValue_2 = value;
	}

	inline static int32_t get_offset_of_floatValue_3() { return static_cast<int32_t>(offsetof(Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0, ___floatValue_3)); }
	inline float get_floatValue_3() const { return ___floatValue_3; }
	inline float* get_address_of_floatValue_3() { return &___floatValue_3; }
	inline void set_floatValue_3(float value)
	{
		___floatValue_3 = value;
	}

	inline static int32_t get_offset_of_stringValue_4() { return static_cast<int32_t>(offsetof(Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0, ___stringValue_4)); }
	inline String_t* get_stringValue_4() const { return ___stringValue_4; }
	inline String_t** get_address_of_stringValue_4() { return &___stringValue_4; }
	inline void set_stringValue_4(String_t* value)
	{
		___stringValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENT_TC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0_H
#ifndef EVENTDATA_T34B08BE715A229D34906F96784419D669DE4869E_H
#define EVENTDATA_T34B08BE715A229D34906F96784419D669DE4869E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventData
struct  EventData_t34B08BE715A229D34906F96784419D669DE4869E  : public RuntimeObject
{
public:
	// System.String Spine.EventData::name
	String_t* ___name_0;
	// System.Int32 Spine.EventData::<Int>k__BackingField
	int32_t ___U3CIntU3Ek__BackingField_1;
	// System.Single Spine.EventData::<Float>k__BackingField
	float ___U3CFloatU3Ek__BackingField_2;
	// System.String Spine.EventData::<String>k__BackingField
	String_t* ___U3CStringU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(EventData_t34B08BE715A229D34906F96784419D669DE4869E, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_U3CIntU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EventData_t34B08BE715A229D34906F96784419D669DE4869E, ___U3CIntU3Ek__BackingField_1)); }
	inline int32_t get_U3CIntU3Ek__BackingField_1() const { return ___U3CIntU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIntU3Ek__BackingField_1() { return &___U3CIntU3Ek__BackingField_1; }
	inline void set_U3CIntU3Ek__BackingField_1(int32_t value)
	{
		___U3CIntU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CFloatU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EventData_t34B08BE715A229D34906F96784419D669DE4869E, ___U3CFloatU3Ek__BackingField_2)); }
	inline float get_U3CFloatU3Ek__BackingField_2() const { return ___U3CFloatU3Ek__BackingField_2; }
	inline float* get_address_of_U3CFloatU3Ek__BackingField_2() { return &___U3CFloatU3Ek__BackingField_2; }
	inline void set_U3CFloatU3Ek__BackingField_2(float value)
	{
		___U3CFloatU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CStringU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EventData_t34B08BE715A229D34906F96784419D669DE4869E, ___U3CStringU3Ek__BackingField_3)); }
	inline String_t* get_U3CStringU3Ek__BackingField_3() const { return ___U3CStringU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CStringU3Ek__BackingField_3() { return &___U3CStringU3Ek__BackingField_3; }
	inline void set_U3CStringU3Ek__BackingField_3(String_t* value)
	{
		___U3CStringU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStringU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDATA_T34B08BE715A229D34906F96784419D669DE4869E_H
#ifndef EVENTQUEUE_TD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77_H
#define EVENTQUEUE_TD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventQueue
struct  EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Spine.EventQueue/EventQueueEntry> Spine.EventQueue::eventQueueEntries
	List_1_t180114C151E81B2D3C61133023D4DFD72AEC82A0 * ___eventQueueEntries_0;
	// System.Boolean Spine.EventQueue::drainDisabled
	bool ___drainDisabled_1;
	// Spine.AnimationState Spine.EventQueue::state
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * ___state_2;
	// Spine.Pool`1<Spine.TrackEntry> Spine.EventQueue::trackEntryPool
	Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF * ___trackEntryPool_3;
	// System.Action Spine.EventQueue::AnimationsChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___AnimationsChanged_4;

public:
	inline static int32_t get_offset_of_eventQueueEntries_0() { return static_cast<int32_t>(offsetof(EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77, ___eventQueueEntries_0)); }
	inline List_1_t180114C151E81B2D3C61133023D4DFD72AEC82A0 * get_eventQueueEntries_0() const { return ___eventQueueEntries_0; }
	inline List_1_t180114C151E81B2D3C61133023D4DFD72AEC82A0 ** get_address_of_eventQueueEntries_0() { return &___eventQueueEntries_0; }
	inline void set_eventQueueEntries_0(List_1_t180114C151E81B2D3C61133023D4DFD72AEC82A0 * value)
	{
		___eventQueueEntries_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventQueueEntries_0), value);
	}

	inline static int32_t get_offset_of_drainDisabled_1() { return static_cast<int32_t>(offsetof(EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77, ___drainDisabled_1)); }
	inline bool get_drainDisabled_1() const { return ___drainDisabled_1; }
	inline bool* get_address_of_drainDisabled_1() { return &___drainDisabled_1; }
	inline void set_drainDisabled_1(bool value)
	{
		___drainDisabled_1 = value;
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77, ___state_2)); }
	inline AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * get_state_2() const { return ___state_2; }
	inline AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 ** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier((&___state_2), value);
	}

	inline static int32_t get_offset_of_trackEntryPool_3() { return static_cast<int32_t>(offsetof(EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77, ___trackEntryPool_3)); }
	inline Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF * get_trackEntryPool_3() const { return ___trackEntryPool_3; }
	inline Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF ** get_address_of_trackEntryPool_3() { return &___trackEntryPool_3; }
	inline void set_trackEntryPool_3(Pool_1_tD20979205852E77F68A0D201074CF4EC84FE3CDF * value)
	{
		___trackEntryPool_3 = value;
		Il2CppCodeGenWriteBarrier((&___trackEntryPool_3), value);
	}

	inline static int32_t get_offset_of_AnimationsChanged_4() { return static_cast<int32_t>(offsetof(EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77, ___AnimationsChanged_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_AnimationsChanged_4() const { return ___AnimationsChanged_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_AnimationsChanged_4() { return &___AnimationsChanged_4; }
	inline void set_AnimationsChanged_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___AnimationsChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationsChanged_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTQUEUE_TD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77_H
#ifndef EVENTTIMELINE_TBF409E0B4D5D35510A43CFE2F4C660A552F69AAF_H
#define EVENTTIMELINE_TBF409E0B4D5D35510A43CFE2F4C660A552F69AAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventTimeline
struct  EventTimeline_tBF409E0B4D5D35510A43CFE2F4C660A552F69AAF  : public RuntimeObject
{
public:
	// System.Single[] Spine.EventTimeline::frames
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___frames_0;
	// Spine.Event[] Spine.EventTimeline::events
	EventU5BU5D_t318C0E0DC13E422E4E9A16FC8F841192027531C2* ___events_1;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(EventTimeline_tBF409E0B4D5D35510A43CFE2F4C660A552F69AAF, ___frames_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_frames_0() const { return ___frames_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}

	inline static int32_t get_offset_of_events_1() { return static_cast<int32_t>(offsetof(EventTimeline_tBF409E0B4D5D35510A43CFE2F4C660A552F69AAF, ___events_1)); }
	inline EventU5BU5D_t318C0E0DC13E422E4E9A16FC8F841192027531C2* get_events_1() const { return ___events_1; }
	inline EventU5BU5D_t318C0E0DC13E422E4E9A16FC8F841192027531C2** get_address_of_events_1() { return &___events_1; }
	inline void set_events_1(EventU5BU5D_t318C0E0DC13E422E4E9A16FC8F841192027531C2* value)
	{
		___events_1 = value;
		Il2CppCodeGenWriteBarrier((&___events_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTIMELINE_TBF409E0B4D5D35510A43CFE2F4C660A552F69AAF_H
#ifndef TRACKENTRY_TC06EF190EA88E3477B5CDDC2AABB2F01B6B12276_H
#define TRACKENTRY_TC06EF190EA88E3477B5CDDC2AABB2F01B6B12276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TrackEntry
struct  TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276  : public RuntimeObject
{
public:
	// Spine.Animation Spine.TrackEntry::animation
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * ___animation_0;
	// Spine.TrackEntry Spine.TrackEntry::next
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * ___next_1;
	// Spine.TrackEntry Spine.TrackEntry::mixingFrom
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * ___mixingFrom_2;
	// System.Int32 Spine.TrackEntry::trackIndex
	int32_t ___trackIndex_3;
	// System.Boolean Spine.TrackEntry::loop
	bool ___loop_4;
	// System.Single Spine.TrackEntry::eventThreshold
	float ___eventThreshold_5;
	// System.Single Spine.TrackEntry::attachmentThreshold
	float ___attachmentThreshold_6;
	// System.Single Spine.TrackEntry::drawOrderThreshold
	float ___drawOrderThreshold_7;
	// System.Single Spine.TrackEntry::animationStart
	float ___animationStart_8;
	// System.Single Spine.TrackEntry::animationEnd
	float ___animationEnd_9;
	// System.Single Spine.TrackEntry::animationLast
	float ___animationLast_10;
	// System.Single Spine.TrackEntry::nextAnimationLast
	float ___nextAnimationLast_11;
	// System.Single Spine.TrackEntry::delay
	float ___delay_12;
	// System.Single Spine.TrackEntry::trackTime
	float ___trackTime_13;
	// System.Single Spine.TrackEntry::trackLast
	float ___trackLast_14;
	// System.Single Spine.TrackEntry::nextTrackLast
	float ___nextTrackLast_15;
	// System.Single Spine.TrackEntry::trackEnd
	float ___trackEnd_16;
	// System.Single Spine.TrackEntry::timeScale
	float ___timeScale_17;
	// System.Single Spine.TrackEntry::alpha
	float ___alpha_18;
	// System.Single Spine.TrackEntry::mixTime
	float ___mixTime_19;
	// System.Single Spine.TrackEntry::mixDuration
	float ___mixDuration_20;
	// System.Single Spine.TrackEntry::interruptAlpha
	float ___interruptAlpha_21;
	// System.Single Spine.TrackEntry::totalAlpha
	float ___totalAlpha_22;
	// Spine.ExposedList`1<System.Int32> Spine.TrackEntry::timelineData
	ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * ___timelineData_23;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.TrackEntry::timelineDipMix
	ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * ___timelineDipMix_24;
	// Spine.ExposedList`1<System.Single> Spine.TrackEntry::timelinesRotation
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___timelinesRotation_25;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Start
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Start_26;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Interrupt
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Interrupt_27;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::End
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___End_28;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Dispose
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Dispose_29;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Complete
	TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * ___Complete_30;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.TrackEntry::Event
	TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE * ___Event_31;

public:
	inline static int32_t get_offset_of_animation_0() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___animation_0)); }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * get_animation_0() const { return ___animation_0; }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 ** get_address_of_animation_0() { return &___animation_0; }
	inline void set_animation_0(Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * value)
	{
		___animation_0 = value;
		Il2CppCodeGenWriteBarrier((&___animation_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___next_1)); }
	inline TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * get_next_1() const { return ___next_1; }
	inline TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((&___next_1), value);
	}

	inline static int32_t get_offset_of_mixingFrom_2() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___mixingFrom_2)); }
	inline TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * get_mixingFrom_2() const { return ___mixingFrom_2; }
	inline TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 ** get_address_of_mixingFrom_2() { return &___mixingFrom_2; }
	inline void set_mixingFrom_2(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * value)
	{
		___mixingFrom_2 = value;
		Il2CppCodeGenWriteBarrier((&___mixingFrom_2), value);
	}

	inline static int32_t get_offset_of_trackIndex_3() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___trackIndex_3)); }
	inline int32_t get_trackIndex_3() const { return ___trackIndex_3; }
	inline int32_t* get_address_of_trackIndex_3() { return &___trackIndex_3; }
	inline void set_trackIndex_3(int32_t value)
	{
		___trackIndex_3 = value;
	}

	inline static int32_t get_offset_of_loop_4() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___loop_4)); }
	inline bool get_loop_4() const { return ___loop_4; }
	inline bool* get_address_of_loop_4() { return &___loop_4; }
	inline void set_loop_4(bool value)
	{
		___loop_4 = value;
	}

	inline static int32_t get_offset_of_eventThreshold_5() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___eventThreshold_5)); }
	inline float get_eventThreshold_5() const { return ___eventThreshold_5; }
	inline float* get_address_of_eventThreshold_5() { return &___eventThreshold_5; }
	inline void set_eventThreshold_5(float value)
	{
		___eventThreshold_5 = value;
	}

	inline static int32_t get_offset_of_attachmentThreshold_6() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___attachmentThreshold_6)); }
	inline float get_attachmentThreshold_6() const { return ___attachmentThreshold_6; }
	inline float* get_address_of_attachmentThreshold_6() { return &___attachmentThreshold_6; }
	inline void set_attachmentThreshold_6(float value)
	{
		___attachmentThreshold_6 = value;
	}

	inline static int32_t get_offset_of_drawOrderThreshold_7() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___drawOrderThreshold_7)); }
	inline float get_drawOrderThreshold_7() const { return ___drawOrderThreshold_7; }
	inline float* get_address_of_drawOrderThreshold_7() { return &___drawOrderThreshold_7; }
	inline void set_drawOrderThreshold_7(float value)
	{
		___drawOrderThreshold_7 = value;
	}

	inline static int32_t get_offset_of_animationStart_8() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___animationStart_8)); }
	inline float get_animationStart_8() const { return ___animationStart_8; }
	inline float* get_address_of_animationStart_8() { return &___animationStart_8; }
	inline void set_animationStart_8(float value)
	{
		___animationStart_8 = value;
	}

	inline static int32_t get_offset_of_animationEnd_9() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___animationEnd_9)); }
	inline float get_animationEnd_9() const { return ___animationEnd_9; }
	inline float* get_address_of_animationEnd_9() { return &___animationEnd_9; }
	inline void set_animationEnd_9(float value)
	{
		___animationEnd_9 = value;
	}

	inline static int32_t get_offset_of_animationLast_10() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___animationLast_10)); }
	inline float get_animationLast_10() const { return ___animationLast_10; }
	inline float* get_address_of_animationLast_10() { return &___animationLast_10; }
	inline void set_animationLast_10(float value)
	{
		___animationLast_10 = value;
	}

	inline static int32_t get_offset_of_nextAnimationLast_11() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___nextAnimationLast_11)); }
	inline float get_nextAnimationLast_11() const { return ___nextAnimationLast_11; }
	inline float* get_address_of_nextAnimationLast_11() { return &___nextAnimationLast_11; }
	inline void set_nextAnimationLast_11(float value)
	{
		___nextAnimationLast_11 = value;
	}

	inline static int32_t get_offset_of_delay_12() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___delay_12)); }
	inline float get_delay_12() const { return ___delay_12; }
	inline float* get_address_of_delay_12() { return &___delay_12; }
	inline void set_delay_12(float value)
	{
		___delay_12 = value;
	}

	inline static int32_t get_offset_of_trackTime_13() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___trackTime_13)); }
	inline float get_trackTime_13() const { return ___trackTime_13; }
	inline float* get_address_of_trackTime_13() { return &___trackTime_13; }
	inline void set_trackTime_13(float value)
	{
		___trackTime_13 = value;
	}

	inline static int32_t get_offset_of_trackLast_14() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___trackLast_14)); }
	inline float get_trackLast_14() const { return ___trackLast_14; }
	inline float* get_address_of_trackLast_14() { return &___trackLast_14; }
	inline void set_trackLast_14(float value)
	{
		___trackLast_14 = value;
	}

	inline static int32_t get_offset_of_nextTrackLast_15() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___nextTrackLast_15)); }
	inline float get_nextTrackLast_15() const { return ___nextTrackLast_15; }
	inline float* get_address_of_nextTrackLast_15() { return &___nextTrackLast_15; }
	inline void set_nextTrackLast_15(float value)
	{
		___nextTrackLast_15 = value;
	}

	inline static int32_t get_offset_of_trackEnd_16() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___trackEnd_16)); }
	inline float get_trackEnd_16() const { return ___trackEnd_16; }
	inline float* get_address_of_trackEnd_16() { return &___trackEnd_16; }
	inline void set_trackEnd_16(float value)
	{
		___trackEnd_16 = value;
	}

	inline static int32_t get_offset_of_timeScale_17() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___timeScale_17)); }
	inline float get_timeScale_17() const { return ___timeScale_17; }
	inline float* get_address_of_timeScale_17() { return &___timeScale_17; }
	inline void set_timeScale_17(float value)
	{
		___timeScale_17 = value;
	}

	inline static int32_t get_offset_of_alpha_18() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___alpha_18)); }
	inline float get_alpha_18() const { return ___alpha_18; }
	inline float* get_address_of_alpha_18() { return &___alpha_18; }
	inline void set_alpha_18(float value)
	{
		___alpha_18 = value;
	}

	inline static int32_t get_offset_of_mixTime_19() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___mixTime_19)); }
	inline float get_mixTime_19() const { return ___mixTime_19; }
	inline float* get_address_of_mixTime_19() { return &___mixTime_19; }
	inline void set_mixTime_19(float value)
	{
		___mixTime_19 = value;
	}

	inline static int32_t get_offset_of_mixDuration_20() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___mixDuration_20)); }
	inline float get_mixDuration_20() const { return ___mixDuration_20; }
	inline float* get_address_of_mixDuration_20() { return &___mixDuration_20; }
	inline void set_mixDuration_20(float value)
	{
		___mixDuration_20 = value;
	}

	inline static int32_t get_offset_of_interruptAlpha_21() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___interruptAlpha_21)); }
	inline float get_interruptAlpha_21() const { return ___interruptAlpha_21; }
	inline float* get_address_of_interruptAlpha_21() { return &___interruptAlpha_21; }
	inline void set_interruptAlpha_21(float value)
	{
		___interruptAlpha_21 = value;
	}

	inline static int32_t get_offset_of_totalAlpha_22() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___totalAlpha_22)); }
	inline float get_totalAlpha_22() const { return ___totalAlpha_22; }
	inline float* get_address_of_totalAlpha_22() { return &___totalAlpha_22; }
	inline void set_totalAlpha_22(float value)
	{
		___totalAlpha_22 = value;
	}

	inline static int32_t get_offset_of_timelineData_23() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___timelineData_23)); }
	inline ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * get_timelineData_23() const { return ___timelineData_23; }
	inline ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 ** get_address_of_timelineData_23() { return &___timelineData_23; }
	inline void set_timelineData_23(ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * value)
	{
		___timelineData_23 = value;
		Il2CppCodeGenWriteBarrier((&___timelineData_23), value);
	}

	inline static int32_t get_offset_of_timelineDipMix_24() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___timelineDipMix_24)); }
	inline ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * get_timelineDipMix_24() const { return ___timelineDipMix_24; }
	inline ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 ** get_address_of_timelineDipMix_24() { return &___timelineDipMix_24; }
	inline void set_timelineDipMix_24(ExposedList_1_t3AB9F2ED97737EA95E06E11E4A5EF60154D01D99 * value)
	{
		___timelineDipMix_24 = value;
		Il2CppCodeGenWriteBarrier((&___timelineDipMix_24), value);
	}

	inline static int32_t get_offset_of_timelinesRotation_25() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___timelinesRotation_25)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_timelinesRotation_25() const { return ___timelinesRotation_25; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_timelinesRotation_25() { return &___timelinesRotation_25; }
	inline void set_timelinesRotation_25(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___timelinesRotation_25 = value;
		Il2CppCodeGenWriteBarrier((&___timelinesRotation_25), value);
	}

	inline static int32_t get_offset_of_Start_26() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___Start_26)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Start_26() const { return ___Start_26; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Start_26() { return &___Start_26; }
	inline void set_Start_26(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Start_26 = value;
		Il2CppCodeGenWriteBarrier((&___Start_26), value);
	}

	inline static int32_t get_offset_of_Interrupt_27() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___Interrupt_27)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Interrupt_27() const { return ___Interrupt_27; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Interrupt_27() { return &___Interrupt_27; }
	inline void set_Interrupt_27(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Interrupt_27 = value;
		Il2CppCodeGenWriteBarrier((&___Interrupt_27), value);
	}

	inline static int32_t get_offset_of_End_28() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___End_28)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_End_28() const { return ___End_28; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_End_28() { return &___End_28; }
	inline void set_End_28(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___End_28 = value;
		Il2CppCodeGenWriteBarrier((&___End_28), value);
	}

	inline static int32_t get_offset_of_Dispose_29() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___Dispose_29)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Dispose_29() const { return ___Dispose_29; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Dispose_29() { return &___Dispose_29; }
	inline void set_Dispose_29(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Dispose_29 = value;
		Il2CppCodeGenWriteBarrier((&___Dispose_29), value);
	}

	inline static int32_t get_offset_of_Complete_30() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___Complete_30)); }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * get_Complete_30() const { return ___Complete_30; }
	inline TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F ** get_address_of_Complete_30() { return &___Complete_30; }
	inline void set_Complete_30(TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F * value)
	{
		___Complete_30 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_30), value);
	}

	inline static int32_t get_offset_of_Event_31() { return static_cast<int32_t>(offsetof(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276, ___Event_31)); }
	inline TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE * get_Event_31() const { return ___Event_31; }
	inline TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE ** get_address_of_Event_31() { return &___Event_31; }
	inline void set_Event_31(TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE * value)
	{
		___Event_31 = value;
		Il2CppCodeGenWriteBarrier((&___Event_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRY_TC06EF190EA88E3477B5CDDC2AABB2F01B6B12276_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef PLAYABLEBEHAVIOUR_T5F4AA32E735199182CC5F57D426D27BE8ABA8F01_H
#define PLAYABLEBEHAVIOUR_T5F4AA32E735199182CC5F57D426D27BE8ABA8F01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T5F4AA32E735199182CC5F57D426D27BE8ABA8F01_H
#ifndef SAUTOMOBILECONFIG_T0F7305B3147D29DC77AFD945BD716711D8271B20_H
#define SAUTOMOBILECONFIG_T0F7305B3147D29DC77AFD945BD716711D8271B20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/sAutomobileConfig
struct  sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20 
{
public:
	// System.Int32 DataManager/sAutomobileConfig::nId
	int32_t ___nId_0;
	// System.Int32 DataManager/sAutomobileConfig::nLevel
	int32_t ___nLevel_1;
	// System.Int32 DataManager/sAutomobileConfig::nResId
	int32_t ___nResId_2;
	// System.String DataManager/sAutomobileConfig::szName
	String_t* ___szName_3;
	// System.Int32 DataManager/sAutomobileConfig::eCoinType
	int32_t ___eCoinType_4;
	// System.Double DataManager/sAutomobileConfig::nStartPrice_CoinValue
	double ___nStartPrice_CoinValue_5;
	// System.Single DataManager/sAutomobileConfig::fPriceRaisePercentPerBuy
	float ___fPriceRaisePercentPerBuy_6;
	// System.Int32 DataManager/sAutomobileConfig::nPriceDiamond
	int32_t ___nPriceDiamond_7;
	// System.Int32 DataManager/sAutomobileConfig::nCanUnlockLevel
	int32_t ___nCanUnlockLevel_8;
	// System.String DataManager/sAutomobileConfig::szSuitableTrackId
	String_t* ___szSuitableTrackId_9;
	// System.Double DataManager/sAutomobileConfig::nBaseGain
	double ___nBaseGain_10;
	// System.Single DataManager/sAutomobileConfig::fBaseSpeed
	float ___fBaseSpeed_11;
	// System.Int32 DataManager/sAutomobileConfig::nDropLevel
	int32_t ___nDropLevel_12;
	// System.Int32 DataManager/sAutomobileConfig::nDropInterval
	int32_t ___nDropInterval_13;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_nLevel_1() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nLevel_1)); }
	inline int32_t get_nLevel_1() const { return ___nLevel_1; }
	inline int32_t* get_address_of_nLevel_1() { return &___nLevel_1; }
	inline void set_nLevel_1(int32_t value)
	{
		___nLevel_1 = value;
	}

	inline static int32_t get_offset_of_nResId_2() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nResId_2)); }
	inline int32_t get_nResId_2() const { return ___nResId_2; }
	inline int32_t* get_address_of_nResId_2() { return &___nResId_2; }
	inline void set_nResId_2(int32_t value)
	{
		___nResId_2 = value;
	}

	inline static int32_t get_offset_of_szName_3() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___szName_3)); }
	inline String_t* get_szName_3() const { return ___szName_3; }
	inline String_t** get_address_of_szName_3() { return &___szName_3; }
	inline void set_szName_3(String_t* value)
	{
		___szName_3 = value;
		Il2CppCodeGenWriteBarrier((&___szName_3), value);
	}

	inline static int32_t get_offset_of_eCoinType_4() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___eCoinType_4)); }
	inline int32_t get_eCoinType_4() const { return ___eCoinType_4; }
	inline int32_t* get_address_of_eCoinType_4() { return &___eCoinType_4; }
	inline void set_eCoinType_4(int32_t value)
	{
		___eCoinType_4 = value;
	}

	inline static int32_t get_offset_of_nStartPrice_CoinValue_5() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nStartPrice_CoinValue_5)); }
	inline double get_nStartPrice_CoinValue_5() const { return ___nStartPrice_CoinValue_5; }
	inline double* get_address_of_nStartPrice_CoinValue_5() { return &___nStartPrice_CoinValue_5; }
	inline void set_nStartPrice_CoinValue_5(double value)
	{
		___nStartPrice_CoinValue_5 = value;
	}

	inline static int32_t get_offset_of_fPriceRaisePercentPerBuy_6() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___fPriceRaisePercentPerBuy_6)); }
	inline float get_fPriceRaisePercentPerBuy_6() const { return ___fPriceRaisePercentPerBuy_6; }
	inline float* get_address_of_fPriceRaisePercentPerBuy_6() { return &___fPriceRaisePercentPerBuy_6; }
	inline void set_fPriceRaisePercentPerBuy_6(float value)
	{
		___fPriceRaisePercentPerBuy_6 = value;
	}

	inline static int32_t get_offset_of_nPriceDiamond_7() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nPriceDiamond_7)); }
	inline int32_t get_nPriceDiamond_7() const { return ___nPriceDiamond_7; }
	inline int32_t* get_address_of_nPriceDiamond_7() { return &___nPriceDiamond_7; }
	inline void set_nPriceDiamond_7(int32_t value)
	{
		___nPriceDiamond_7 = value;
	}

	inline static int32_t get_offset_of_nCanUnlockLevel_8() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nCanUnlockLevel_8)); }
	inline int32_t get_nCanUnlockLevel_8() const { return ___nCanUnlockLevel_8; }
	inline int32_t* get_address_of_nCanUnlockLevel_8() { return &___nCanUnlockLevel_8; }
	inline void set_nCanUnlockLevel_8(int32_t value)
	{
		___nCanUnlockLevel_8 = value;
	}

	inline static int32_t get_offset_of_szSuitableTrackId_9() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___szSuitableTrackId_9)); }
	inline String_t* get_szSuitableTrackId_9() const { return ___szSuitableTrackId_9; }
	inline String_t** get_address_of_szSuitableTrackId_9() { return &___szSuitableTrackId_9; }
	inline void set_szSuitableTrackId_9(String_t* value)
	{
		___szSuitableTrackId_9 = value;
		Il2CppCodeGenWriteBarrier((&___szSuitableTrackId_9), value);
	}

	inline static int32_t get_offset_of_nBaseGain_10() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nBaseGain_10)); }
	inline double get_nBaseGain_10() const { return ___nBaseGain_10; }
	inline double* get_address_of_nBaseGain_10() { return &___nBaseGain_10; }
	inline void set_nBaseGain_10(double value)
	{
		___nBaseGain_10 = value;
	}

	inline static int32_t get_offset_of_fBaseSpeed_11() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___fBaseSpeed_11)); }
	inline float get_fBaseSpeed_11() const { return ___fBaseSpeed_11; }
	inline float* get_address_of_fBaseSpeed_11() { return &___fBaseSpeed_11; }
	inline void set_fBaseSpeed_11(float value)
	{
		___fBaseSpeed_11 = value;
	}

	inline static int32_t get_offset_of_nDropLevel_12() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nDropLevel_12)); }
	inline int32_t get_nDropLevel_12() const { return ___nDropLevel_12; }
	inline int32_t* get_address_of_nDropLevel_12() { return &___nDropLevel_12; }
	inline void set_nDropLevel_12(int32_t value)
	{
		___nDropLevel_12 = value;
	}

	inline static int32_t get_offset_of_nDropInterval_13() { return static_cast<int32_t>(offsetof(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20, ___nDropInterval_13)); }
	inline int32_t get_nDropInterval_13() const { return ___nDropInterval_13; }
	inline int32_t* get_address_of_nDropInterval_13() { return &___nDropInterval_13; }
	inline void set_nDropInterval_13(int32_t value)
	{
		___nDropInterval_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DataManager/sAutomobileConfig
struct sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20_marshaled_pinvoke
{
	int32_t ___nId_0;
	int32_t ___nLevel_1;
	int32_t ___nResId_2;
	char* ___szName_3;
	int32_t ___eCoinType_4;
	double ___nStartPrice_CoinValue_5;
	float ___fPriceRaisePercentPerBuy_6;
	int32_t ___nPriceDiamond_7;
	int32_t ___nCanUnlockLevel_8;
	char* ___szSuitableTrackId_9;
	double ___nBaseGain_10;
	float ___fBaseSpeed_11;
	int32_t ___nDropLevel_12;
	int32_t ___nDropInterval_13;
};
// Native definition for COM marshalling of DataManager/sAutomobileConfig
struct sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20_marshaled_com
{
	int32_t ___nId_0;
	int32_t ___nLevel_1;
	int32_t ___nResId_2;
	Il2CppChar* ___szName_3;
	int32_t ___eCoinType_4;
	double ___nStartPrice_CoinValue_5;
	float ___fPriceRaisePercentPerBuy_6;
	int32_t ___nPriceDiamond_7;
	int32_t ___nCanUnlockLevel_8;
	Il2CppChar* ___szSuitableTrackId_9;
	double ___nBaseGain_10;
	float ___fBaseSpeed_11;
	int32_t ___nDropLevel_12;
	int32_t ___nDropInterval_13;
};
#endif // SAUTOMOBILECONFIG_T0F7305B3147D29DC77AFD945BD716711D8271B20_H
#ifndef SITEMCONFIG_T9C277270FC8FC12356FC8BA5BD234112769E425C_H
#define SITEMCONFIG_T9C277270FC8FC12356FC8BA5BD234112769E425C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemSystem/sItemConfig
struct  sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C 
{
public:
	// System.Int32 ItemSystem/sItemConfig::nId
	int32_t ___nId_0;
	// System.String ItemSystem/sItemConfig::szName
	String_t* ___szName_1;
	// System.Int32 ItemSystem/sItemConfig::nType
	int32_t ___nType_2;
	// System.Int32 ItemSystem/sItemConfig::nResId
	int32_t ___nResId_3;
	// System.String ItemSystem/sItemConfig::szParams
	String_t* ___szParams_4;
	// System.Int32 ItemSystem/sItemConfig::nDuration
	int32_t ___nDuration_5;
	// System.String ItemSystem/sItemConfig::szDesc
	String_t* ___szDesc_6;
	// System.Int32[] ItemSystem/sItemConfig::aryIntParams
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___aryIntParams_7;
	// System.Single[] ItemSystem/sItemConfig::aryFloatParams
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___aryFloatParams_8;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_szName_1() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___szName_1)); }
	inline String_t* get_szName_1() const { return ___szName_1; }
	inline String_t** get_address_of_szName_1() { return &___szName_1; }
	inline void set_szName_1(String_t* value)
	{
		___szName_1 = value;
		Il2CppCodeGenWriteBarrier((&___szName_1), value);
	}

	inline static int32_t get_offset_of_nType_2() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___nType_2)); }
	inline int32_t get_nType_2() const { return ___nType_2; }
	inline int32_t* get_address_of_nType_2() { return &___nType_2; }
	inline void set_nType_2(int32_t value)
	{
		___nType_2 = value;
	}

	inline static int32_t get_offset_of_nResId_3() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___nResId_3)); }
	inline int32_t get_nResId_3() const { return ___nResId_3; }
	inline int32_t* get_address_of_nResId_3() { return &___nResId_3; }
	inline void set_nResId_3(int32_t value)
	{
		___nResId_3 = value;
	}

	inline static int32_t get_offset_of_szParams_4() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___szParams_4)); }
	inline String_t* get_szParams_4() const { return ___szParams_4; }
	inline String_t** get_address_of_szParams_4() { return &___szParams_4; }
	inline void set_szParams_4(String_t* value)
	{
		___szParams_4 = value;
		Il2CppCodeGenWriteBarrier((&___szParams_4), value);
	}

	inline static int32_t get_offset_of_nDuration_5() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___nDuration_5)); }
	inline int32_t get_nDuration_5() const { return ___nDuration_5; }
	inline int32_t* get_address_of_nDuration_5() { return &___nDuration_5; }
	inline void set_nDuration_5(int32_t value)
	{
		___nDuration_5 = value;
	}

	inline static int32_t get_offset_of_szDesc_6() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___szDesc_6)); }
	inline String_t* get_szDesc_6() const { return ___szDesc_6; }
	inline String_t** get_address_of_szDesc_6() { return &___szDesc_6; }
	inline void set_szDesc_6(String_t* value)
	{
		___szDesc_6 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_6), value);
	}

	inline static int32_t get_offset_of_aryIntParams_7() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___aryIntParams_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_aryIntParams_7() const { return ___aryIntParams_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_aryIntParams_7() { return &___aryIntParams_7; }
	inline void set_aryIntParams_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___aryIntParams_7 = value;
		Il2CppCodeGenWriteBarrier((&___aryIntParams_7), value);
	}

	inline static int32_t get_offset_of_aryFloatParams_8() { return static_cast<int32_t>(offsetof(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C, ___aryFloatParams_8)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_aryFloatParams_8() const { return ___aryFloatParams_8; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_aryFloatParams_8() { return &___aryFloatParams_8; }
	inline void set_aryFloatParams_8(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___aryFloatParams_8 = value;
		Il2CppCodeGenWriteBarrier((&___aryFloatParams_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ItemSystem/sItemConfig
struct sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C_marshaled_pinvoke
{
	int32_t ___nId_0;
	char* ___szName_1;
	int32_t ___nType_2;
	int32_t ___nResId_3;
	char* ___szParams_4;
	int32_t ___nDuration_5;
	char* ___szDesc_6;
	int32_t* ___aryIntParams_7;
	float* ___aryFloatParams_8;
};
// Native definition for COM marshalling of ItemSystem/sItemConfig
struct sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C_marshaled_com
{
	int32_t ___nId_0;
	Il2CppChar* ___szName_1;
	int32_t ___nType_2;
	int32_t ___nResId_3;
	Il2CppChar* ___szParams_4;
	int32_t ___nDuration_5;
	Il2CppChar* ___szDesc_6;
	int32_t* ___aryIntParams_7;
	float* ___aryFloatParams_8;
};
#endif // SITEMCONFIG_T9C277270FC8FC12356FC8BA5BD234112769E425C_H
#ifndef ANIMATIONPAIR_TE3B0769553A08F90C7CCAE7F8D9011DFE541D349_H
#define ANIMATIONPAIR_TE3B0769553A08F90C7CCAE7F8D9011DFE541D349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationStateData/AnimationPair
struct  AnimationPair_tE3B0769553A08F90C7CCAE7F8D9011DFE541D349 
{
public:
	// Spine.Animation Spine.AnimationStateData/AnimationPair::a1
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * ___a1_0;
	// Spine.Animation Spine.AnimationStateData/AnimationPair::a2
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * ___a2_1;

public:
	inline static int32_t get_offset_of_a1_0() { return static_cast<int32_t>(offsetof(AnimationPair_tE3B0769553A08F90C7CCAE7F8D9011DFE541D349, ___a1_0)); }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * get_a1_0() const { return ___a1_0; }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 ** get_address_of_a1_0() { return &___a1_0; }
	inline void set_a1_0(Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * value)
	{
		___a1_0 = value;
		Il2CppCodeGenWriteBarrier((&___a1_0), value);
	}

	inline static int32_t get_offset_of_a2_1() { return static_cast<int32_t>(offsetof(AnimationPair_tE3B0769553A08F90C7CCAE7F8D9011DFE541D349, ___a2_1)); }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * get_a2_1() const { return ___a2_1; }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 ** get_address_of_a2_1() { return &___a2_1; }
	inline void set_a2_1(Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * value)
	{
		___a2_1 = value;
		Il2CppCodeGenWriteBarrier((&___a2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.AnimationStateData/AnimationPair
struct AnimationPair_tE3B0769553A08F90C7CCAE7F8D9011DFE541D349_marshaled_pinvoke
{
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * ___a1_0;
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * ___a2_1;
};
// Native definition for COM marshalling of Spine.AnimationStateData/AnimationPair
struct AnimationPair_tE3B0769553A08F90C7CCAE7F8D9011DFE541D349_marshaled_com
{
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * ___a1_0;
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * ___a2_1;
};
#endif // ANIMATIONPAIR_TE3B0769553A08F90C7CCAE7F8D9011DFE541D349_H
#ifndef COLORTIMELINE_T3C9AD36709A96F0AE371BB3B60F339089D32707F_H
#define COLORTIMELINE_T3C9AD36709A96F0AE371BB3B60F339089D32707F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ColorTimeline
struct  ColorTimeline_t3C9AD36709A96F0AE371BB3B60F339089D32707F  : public CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E
{
public:
	// System.Int32 Spine.ColorTimeline::slotIndex
	int32_t ___slotIndex_15;
	// System.Single[] Spine.ColorTimeline::frames
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___frames_16;

public:
	inline static int32_t get_offset_of_slotIndex_15() { return static_cast<int32_t>(offsetof(ColorTimeline_t3C9AD36709A96F0AE371BB3B60F339089D32707F, ___slotIndex_15)); }
	inline int32_t get_slotIndex_15() const { return ___slotIndex_15; }
	inline int32_t* get_address_of_slotIndex_15() { return &___slotIndex_15; }
	inline void set_slotIndex_15(int32_t value)
	{
		___slotIndex_15 = value;
	}

	inline static int32_t get_offset_of_frames_16() { return static_cast<int32_t>(offsetof(ColorTimeline_t3C9AD36709A96F0AE371BB3B60F339089D32707F, ___frames_16)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_frames_16() const { return ___frames_16; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_frames_16() { return &___frames_16; }
	inline void set_frames_16(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___frames_16 = value;
		Il2CppCodeGenWriteBarrier((&___frames_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTIMELINE_T3C9AD36709A96F0AE371BB3B60F339089D32707F_H
#ifndef DEFORMTIMELINE_TABB31F393685EF4297F9372BBA5D31828B76BAB2_H
#define DEFORMTIMELINE_TABB31F393685EF4297F9372BBA5D31828B76BAB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.DeformTimeline
struct  DeformTimeline_tABB31F393685EF4297F9372BBA5D31828B76BAB2  : public CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E
{
public:
	// System.Int32 Spine.DeformTimeline::slotIndex
	int32_t ___slotIndex_5;
	// System.Single[] Spine.DeformTimeline::frames
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___frames_6;
	// System.Single[][] Spine.DeformTimeline::frameVertices
	SingleU5BU5DU5BU5D_tDE1FB3BBC65F7A632E44302B85B7F7193EF48839* ___frameVertices_7;
	// Spine.VertexAttachment Spine.DeformTimeline::attachment
	VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631 * ___attachment_8;

public:
	inline static int32_t get_offset_of_slotIndex_5() { return static_cast<int32_t>(offsetof(DeformTimeline_tABB31F393685EF4297F9372BBA5D31828B76BAB2, ___slotIndex_5)); }
	inline int32_t get_slotIndex_5() const { return ___slotIndex_5; }
	inline int32_t* get_address_of_slotIndex_5() { return &___slotIndex_5; }
	inline void set_slotIndex_5(int32_t value)
	{
		___slotIndex_5 = value;
	}

	inline static int32_t get_offset_of_frames_6() { return static_cast<int32_t>(offsetof(DeformTimeline_tABB31F393685EF4297F9372BBA5D31828B76BAB2, ___frames_6)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_frames_6() const { return ___frames_6; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_frames_6() { return &___frames_6; }
	inline void set_frames_6(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___frames_6 = value;
		Il2CppCodeGenWriteBarrier((&___frames_6), value);
	}

	inline static int32_t get_offset_of_frameVertices_7() { return static_cast<int32_t>(offsetof(DeformTimeline_tABB31F393685EF4297F9372BBA5D31828B76BAB2, ___frameVertices_7)); }
	inline SingleU5BU5DU5BU5D_tDE1FB3BBC65F7A632E44302B85B7F7193EF48839* get_frameVertices_7() const { return ___frameVertices_7; }
	inline SingleU5BU5DU5BU5D_tDE1FB3BBC65F7A632E44302B85B7F7193EF48839** get_address_of_frameVertices_7() { return &___frameVertices_7; }
	inline void set_frameVertices_7(SingleU5BU5DU5BU5D_tDE1FB3BBC65F7A632E44302B85B7F7193EF48839* value)
	{
		___frameVertices_7 = value;
		Il2CppCodeGenWriteBarrier((&___frameVertices_7), value);
	}

	inline static int32_t get_offset_of_attachment_8() { return static_cast<int32_t>(offsetof(DeformTimeline_tABB31F393685EF4297F9372BBA5D31828B76BAB2, ___attachment_8)); }
	inline VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631 * get_attachment_8() const { return ___attachment_8; }
	inline VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631 ** get_address_of_attachment_8() { return &___attachment_8; }
	inline void set_attachment_8(VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631 * value)
	{
		___attachment_8 = value;
		Il2CppCodeGenWriteBarrier((&___attachment_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFORMTIMELINE_TABB31F393685EF4297F9372BBA5D31828B76BAB2_H
#ifndef IKCONSTRAINTTIMELINE_T804DB850BFAB66F35538D64EAB52B792E4A61689_H
#define IKCONSTRAINTTIMELINE_T804DB850BFAB66F35538D64EAB52B792E4A61689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.IkConstraintTimeline
struct  IkConstraintTimeline_t804DB850BFAB66F35538D64EAB52B792E4A61689  : public CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E
{
public:
	// System.Int32 Spine.IkConstraintTimeline::ikConstraintIndex
	int32_t ___ikConstraintIndex_11;
	// System.Single[] Spine.IkConstraintTimeline::frames
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___frames_12;

public:
	inline static int32_t get_offset_of_ikConstraintIndex_11() { return static_cast<int32_t>(offsetof(IkConstraintTimeline_t804DB850BFAB66F35538D64EAB52B792E4A61689, ___ikConstraintIndex_11)); }
	inline int32_t get_ikConstraintIndex_11() const { return ___ikConstraintIndex_11; }
	inline int32_t* get_address_of_ikConstraintIndex_11() { return &___ikConstraintIndex_11; }
	inline void set_ikConstraintIndex_11(int32_t value)
	{
		___ikConstraintIndex_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(IkConstraintTimeline_t804DB850BFAB66F35538D64EAB52B792E4A61689, ___frames_12)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_frames_12() const { return ___frames_12; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___frames_12 = value;
		Il2CppCodeGenWriteBarrier((&___frames_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCONSTRAINTTIMELINE_T804DB850BFAB66F35538D64EAB52B792E4A61689_H
#ifndef PATHCONSTRAINTMIXTIMELINE_T5612530CC2D6D7B270B68BD20BA063FB32934020_H
#define PATHCONSTRAINTMIXTIMELINE_T5612530CC2D6D7B270B68BD20BA063FB32934020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintMixTimeline
struct  PathConstraintMixTimeline_t5612530CC2D6D7B270B68BD20BA063FB32934020  : public CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E
{
public:
	// System.Int32 Spine.PathConstraintMixTimeline::pathConstraintIndex
	int32_t ___pathConstraintIndex_11;
	// System.Single[] Spine.PathConstraintMixTimeline::frames
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___frames_12;

public:
	inline static int32_t get_offset_of_pathConstraintIndex_11() { return static_cast<int32_t>(offsetof(PathConstraintMixTimeline_t5612530CC2D6D7B270B68BD20BA063FB32934020, ___pathConstraintIndex_11)); }
	inline int32_t get_pathConstraintIndex_11() const { return ___pathConstraintIndex_11; }
	inline int32_t* get_address_of_pathConstraintIndex_11() { return &___pathConstraintIndex_11; }
	inline void set_pathConstraintIndex_11(int32_t value)
	{
		___pathConstraintIndex_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(PathConstraintMixTimeline_t5612530CC2D6D7B270B68BD20BA063FB32934020, ___frames_12)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_frames_12() const { return ___frames_12; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___frames_12 = value;
		Il2CppCodeGenWriteBarrier((&___frames_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTMIXTIMELINE_T5612530CC2D6D7B270B68BD20BA063FB32934020_H
#ifndef PATHCONSTRAINTPOSITIONTIMELINE_T067633A6B9AE48EF83FE73BBC66EA1DC47D7DACE_H
#define PATHCONSTRAINTPOSITIONTIMELINE_T067633A6B9AE48EF83FE73BBC66EA1DC47D7DACE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintPositionTimeline
struct  PathConstraintPositionTimeline_t067633A6B9AE48EF83FE73BBC66EA1DC47D7DACE  : public CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E
{
public:
	// System.Int32 Spine.PathConstraintPositionTimeline::pathConstraintIndex
	int32_t ___pathConstraintIndex_9;
	// System.Single[] Spine.PathConstraintPositionTimeline::frames
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___frames_10;

public:
	inline static int32_t get_offset_of_pathConstraintIndex_9() { return static_cast<int32_t>(offsetof(PathConstraintPositionTimeline_t067633A6B9AE48EF83FE73BBC66EA1DC47D7DACE, ___pathConstraintIndex_9)); }
	inline int32_t get_pathConstraintIndex_9() const { return ___pathConstraintIndex_9; }
	inline int32_t* get_address_of_pathConstraintIndex_9() { return &___pathConstraintIndex_9; }
	inline void set_pathConstraintIndex_9(int32_t value)
	{
		___pathConstraintIndex_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(PathConstraintPositionTimeline_t067633A6B9AE48EF83FE73BBC66EA1DC47D7DACE, ___frames_10)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_frames_10() const { return ___frames_10; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___frames_10 = value;
		Il2CppCodeGenWriteBarrier((&___frames_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTPOSITIONTIMELINE_T067633A6B9AE48EF83FE73BBC66EA1DC47D7DACE_H
#ifndef POINTATTACHMENT_T8F365CFDF058CDC5566E801DE1DAFE637F194D1B_H
#define POINTATTACHMENT_T8F365CFDF058CDC5566E801DE1DAFE637F194D1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PointAttachment
struct  PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B  : public Attachment_tCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92
{
public:
	// System.Single Spine.PointAttachment::x
	float ___x_1;
	// System.Single Spine.PointAttachment::y
	float ___y_2;
	// System.Single Spine.PointAttachment::rotation
	float ___rotation_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_rotation_3() { return static_cast<int32_t>(offsetof(PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B, ___rotation_3)); }
	inline float get_rotation_3() const { return ___rotation_3; }
	inline float* get_address_of_rotation_3() { return &___rotation_3; }
	inline void set_rotation_3(float value)
	{
		___rotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTATTACHMENT_T8F365CFDF058CDC5566E801DE1DAFE637F194D1B_H
#ifndef REGIONATTACHMENT_TB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC_H
#define REGIONATTACHMENT_TB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.RegionAttachment
struct  RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC  : public Attachment_tCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92
{
public:
	// System.Single Spine.RegionAttachment::x
	float ___x_9;
	// System.Single Spine.RegionAttachment::y
	float ___y_10;
	// System.Single Spine.RegionAttachment::rotation
	float ___rotation_11;
	// System.Single Spine.RegionAttachment::scaleX
	float ___scaleX_12;
	// System.Single Spine.RegionAttachment::scaleY
	float ___scaleY_13;
	// System.Single Spine.RegionAttachment::width
	float ___width_14;
	// System.Single Spine.RegionAttachment::height
	float ___height_15;
	// System.Single Spine.RegionAttachment::regionOffsetX
	float ___regionOffsetX_16;
	// System.Single Spine.RegionAttachment::regionOffsetY
	float ___regionOffsetY_17;
	// System.Single Spine.RegionAttachment::regionWidth
	float ___regionWidth_18;
	// System.Single Spine.RegionAttachment::regionHeight
	float ___regionHeight_19;
	// System.Single Spine.RegionAttachment::regionOriginalWidth
	float ___regionOriginalWidth_20;
	// System.Single Spine.RegionAttachment::regionOriginalHeight
	float ___regionOriginalHeight_21;
	// System.Single[] Spine.RegionAttachment::offset
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___offset_22;
	// System.Single[] Spine.RegionAttachment::uvs
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___uvs_23;
	// System.Single Spine.RegionAttachment::r
	float ___r_24;
	// System.Single Spine.RegionAttachment::g
	float ___g_25;
	// System.Single Spine.RegionAttachment::b
	float ___b_26;
	// System.Single Spine.RegionAttachment::a
	float ___a_27;
	// System.String Spine.RegionAttachment::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_28;
	// System.Object Spine.RegionAttachment::<RendererObject>k__BackingField
	RuntimeObject * ___U3CRendererObjectU3Ek__BackingField_29;

public:
	inline static int32_t get_offset_of_x_9() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___x_9)); }
	inline float get_x_9() const { return ___x_9; }
	inline float* get_address_of_x_9() { return &___x_9; }
	inline void set_x_9(float value)
	{
		___x_9 = value;
	}

	inline static int32_t get_offset_of_y_10() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___y_10)); }
	inline float get_y_10() const { return ___y_10; }
	inline float* get_address_of_y_10() { return &___y_10; }
	inline void set_y_10(float value)
	{
		___y_10 = value;
	}

	inline static int32_t get_offset_of_rotation_11() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___rotation_11)); }
	inline float get_rotation_11() const { return ___rotation_11; }
	inline float* get_address_of_rotation_11() { return &___rotation_11; }
	inline void set_rotation_11(float value)
	{
		___rotation_11 = value;
	}

	inline static int32_t get_offset_of_scaleX_12() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___scaleX_12)); }
	inline float get_scaleX_12() const { return ___scaleX_12; }
	inline float* get_address_of_scaleX_12() { return &___scaleX_12; }
	inline void set_scaleX_12(float value)
	{
		___scaleX_12 = value;
	}

	inline static int32_t get_offset_of_scaleY_13() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___scaleY_13)); }
	inline float get_scaleY_13() const { return ___scaleY_13; }
	inline float* get_address_of_scaleY_13() { return &___scaleY_13; }
	inline void set_scaleY_13(float value)
	{
		___scaleY_13 = value;
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___width_14)); }
	inline float get_width_14() const { return ___width_14; }
	inline float* get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(float value)
	{
		___width_14 = value;
	}

	inline static int32_t get_offset_of_height_15() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___height_15)); }
	inline float get_height_15() const { return ___height_15; }
	inline float* get_address_of_height_15() { return &___height_15; }
	inline void set_height_15(float value)
	{
		___height_15 = value;
	}

	inline static int32_t get_offset_of_regionOffsetX_16() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___regionOffsetX_16)); }
	inline float get_regionOffsetX_16() const { return ___regionOffsetX_16; }
	inline float* get_address_of_regionOffsetX_16() { return &___regionOffsetX_16; }
	inline void set_regionOffsetX_16(float value)
	{
		___regionOffsetX_16 = value;
	}

	inline static int32_t get_offset_of_regionOffsetY_17() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___regionOffsetY_17)); }
	inline float get_regionOffsetY_17() const { return ___regionOffsetY_17; }
	inline float* get_address_of_regionOffsetY_17() { return &___regionOffsetY_17; }
	inline void set_regionOffsetY_17(float value)
	{
		___regionOffsetY_17 = value;
	}

	inline static int32_t get_offset_of_regionWidth_18() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___regionWidth_18)); }
	inline float get_regionWidth_18() const { return ___regionWidth_18; }
	inline float* get_address_of_regionWidth_18() { return &___regionWidth_18; }
	inline void set_regionWidth_18(float value)
	{
		___regionWidth_18 = value;
	}

	inline static int32_t get_offset_of_regionHeight_19() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___regionHeight_19)); }
	inline float get_regionHeight_19() const { return ___regionHeight_19; }
	inline float* get_address_of_regionHeight_19() { return &___regionHeight_19; }
	inline void set_regionHeight_19(float value)
	{
		___regionHeight_19 = value;
	}

	inline static int32_t get_offset_of_regionOriginalWidth_20() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___regionOriginalWidth_20)); }
	inline float get_regionOriginalWidth_20() const { return ___regionOriginalWidth_20; }
	inline float* get_address_of_regionOriginalWidth_20() { return &___regionOriginalWidth_20; }
	inline void set_regionOriginalWidth_20(float value)
	{
		___regionOriginalWidth_20 = value;
	}

	inline static int32_t get_offset_of_regionOriginalHeight_21() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___regionOriginalHeight_21)); }
	inline float get_regionOriginalHeight_21() const { return ___regionOriginalHeight_21; }
	inline float* get_address_of_regionOriginalHeight_21() { return &___regionOriginalHeight_21; }
	inline void set_regionOriginalHeight_21(float value)
	{
		___regionOriginalHeight_21 = value;
	}

	inline static int32_t get_offset_of_offset_22() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___offset_22)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_offset_22() const { return ___offset_22; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_offset_22() { return &___offset_22; }
	inline void set_offset_22(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___offset_22 = value;
		Il2CppCodeGenWriteBarrier((&___offset_22), value);
	}

	inline static int32_t get_offset_of_uvs_23() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___uvs_23)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_uvs_23() const { return ___uvs_23; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_uvs_23() { return &___uvs_23; }
	inline void set_uvs_23(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___uvs_23 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_23), value);
	}

	inline static int32_t get_offset_of_r_24() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___r_24)); }
	inline float get_r_24() const { return ___r_24; }
	inline float* get_address_of_r_24() { return &___r_24; }
	inline void set_r_24(float value)
	{
		___r_24 = value;
	}

	inline static int32_t get_offset_of_g_25() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___g_25)); }
	inline float get_g_25() const { return ___g_25; }
	inline float* get_address_of_g_25() { return &___g_25; }
	inline void set_g_25(float value)
	{
		___g_25 = value;
	}

	inline static int32_t get_offset_of_b_26() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___b_26)); }
	inline float get_b_26() const { return ___b_26; }
	inline float* get_address_of_b_26() { return &___b_26; }
	inline void set_b_26(float value)
	{
		___b_26 = value;
	}

	inline static int32_t get_offset_of_a_27() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___a_27)); }
	inline float get_a_27() const { return ___a_27; }
	inline float* get_address_of_a_27() { return &___a_27; }
	inline void set_a_27(float value)
	{
		___a_27 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___U3CPathU3Ek__BackingField_28)); }
	inline String_t* get_U3CPathU3Ek__BackingField_28() const { return ___U3CPathU3Ek__BackingField_28; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_28() { return &___U3CPathU3Ek__BackingField_28; }
	inline void set_U3CPathU3Ek__BackingField_28(String_t* value)
	{
		___U3CPathU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CRendererObjectU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC, ___U3CRendererObjectU3Ek__BackingField_29)); }
	inline RuntimeObject * get_U3CRendererObjectU3Ek__BackingField_29() const { return ___U3CRendererObjectU3Ek__BackingField_29; }
	inline RuntimeObject ** get_address_of_U3CRendererObjectU3Ek__BackingField_29() { return &___U3CRendererObjectU3Ek__BackingField_29; }
	inline void set_U3CRendererObjectU3Ek__BackingField_29(RuntimeObject * value)
	{
		___U3CRendererObjectU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRendererObjectU3Ek__BackingField_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONATTACHMENT_TB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC_H
#ifndef ROTATETIMELINE_T7A18C0AE969708B70CCAC154518EC5FD81ABFCF1_H
#define ROTATETIMELINE_T7A18C0AE969708B70CCAC154518EC5FD81ABFCF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.RotateTimeline
struct  RotateTimeline_t7A18C0AE969708B70CCAC154518EC5FD81ABFCF1  : public CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E
{
public:
	// System.Int32 Spine.RotateTimeline::boneIndex
	int32_t ___boneIndex_9;
	// System.Single[] Spine.RotateTimeline::frames
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___frames_10;

public:
	inline static int32_t get_offset_of_boneIndex_9() { return static_cast<int32_t>(offsetof(RotateTimeline_t7A18C0AE969708B70CCAC154518EC5FD81ABFCF1, ___boneIndex_9)); }
	inline int32_t get_boneIndex_9() const { return ___boneIndex_9; }
	inline int32_t* get_address_of_boneIndex_9() { return &___boneIndex_9; }
	inline void set_boneIndex_9(int32_t value)
	{
		___boneIndex_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(RotateTimeline_t7A18C0AE969708B70CCAC154518EC5FD81ABFCF1, ___frames_10)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_frames_10() const { return ___frames_10; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___frames_10 = value;
		Il2CppCodeGenWriteBarrier((&___frames_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATETIMELINE_T7A18C0AE969708B70CCAC154518EC5FD81ABFCF1_H
#ifndef TRANSFORMCONSTRAINTTIMELINE_T96F58F0669906A6E5EE9956621786488E145FD56_H
#define TRANSFORMCONSTRAINTTIMELINE_T96F58F0669906A6E5EE9956621786488E145FD56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformConstraintTimeline
struct  TransformConstraintTimeline_t96F58F0669906A6E5EE9956621786488E145FD56  : public CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E
{
public:
	// System.Int32 Spine.TransformConstraintTimeline::transformConstraintIndex
	int32_t ___transformConstraintIndex_15;
	// System.Single[] Spine.TransformConstraintTimeline::frames
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___frames_16;

public:
	inline static int32_t get_offset_of_transformConstraintIndex_15() { return static_cast<int32_t>(offsetof(TransformConstraintTimeline_t96F58F0669906A6E5EE9956621786488E145FD56, ___transformConstraintIndex_15)); }
	inline int32_t get_transformConstraintIndex_15() const { return ___transformConstraintIndex_15; }
	inline int32_t* get_address_of_transformConstraintIndex_15() { return &___transformConstraintIndex_15; }
	inline void set_transformConstraintIndex_15(int32_t value)
	{
		___transformConstraintIndex_15 = value;
	}

	inline static int32_t get_offset_of_frames_16() { return static_cast<int32_t>(offsetof(TransformConstraintTimeline_t96F58F0669906A6E5EE9956621786488E145FD56, ___frames_16)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_frames_16() const { return ___frames_16; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_frames_16() { return &___frames_16; }
	inline void set_frames_16(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___frames_16 = value;
		Il2CppCodeGenWriteBarrier((&___frames_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONSTRAINTTIMELINE_T96F58F0669906A6E5EE9956621786488E145FD56_H
#ifndef TRANSLATETIMELINE_T35CE691D668D75F2779A2D495380C7F6E563FF88_H
#define TRANSLATETIMELINE_T35CE691D668D75F2779A2D495380C7F6E563FF88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TranslateTimeline
struct  TranslateTimeline_t35CE691D668D75F2779A2D495380C7F6E563FF88  : public CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E
{
public:
	// System.Int32 Spine.TranslateTimeline::boneIndex
	int32_t ___boneIndex_11;
	// System.Single[] Spine.TranslateTimeline::frames
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___frames_12;

public:
	inline static int32_t get_offset_of_boneIndex_11() { return static_cast<int32_t>(offsetof(TranslateTimeline_t35CE691D668D75F2779A2D495380C7F6E563FF88, ___boneIndex_11)); }
	inline int32_t get_boneIndex_11() const { return ___boneIndex_11; }
	inline int32_t* get_address_of_boneIndex_11() { return &___boneIndex_11; }
	inline void set_boneIndex_11(int32_t value)
	{
		___boneIndex_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(TranslateTimeline_t35CE691D668D75F2779A2D495380C7F6E563FF88, ___frames_12)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_frames_12() const { return ___frames_12; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___frames_12 = value;
		Il2CppCodeGenWriteBarrier((&___frames_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATETIMELINE_T35CE691D668D75F2779A2D495380C7F6E563FF88_H
#ifndef TWOCOLORTIMELINE_T69FAE8EC5AB6A7E367BA935157DF7B1EDE94F11E_H
#define TWOCOLORTIMELINE_T69FAE8EC5AB6A7E367BA935157DF7B1EDE94F11E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TwoColorTimeline
struct  TwoColorTimeline_t69FAE8EC5AB6A7E367BA935157DF7B1EDE94F11E  : public CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E
{
public:
	// System.Int32 Spine.TwoColorTimeline::slotIndex
	int32_t ___slotIndex_21;
	// System.Single[] Spine.TwoColorTimeline::frames
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___frames_22;

public:
	inline static int32_t get_offset_of_slotIndex_21() { return static_cast<int32_t>(offsetof(TwoColorTimeline_t69FAE8EC5AB6A7E367BA935157DF7B1EDE94F11E, ___slotIndex_21)); }
	inline int32_t get_slotIndex_21() const { return ___slotIndex_21; }
	inline int32_t* get_address_of_slotIndex_21() { return &___slotIndex_21; }
	inline void set_slotIndex_21(int32_t value)
	{
		___slotIndex_21 = value;
	}

	inline static int32_t get_offset_of_frames_22() { return static_cast<int32_t>(offsetof(TwoColorTimeline_t69FAE8EC5AB6A7E367BA935157DF7B1EDE94F11E, ___frames_22)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_frames_22() const { return ___frames_22; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_frames_22() { return &___frames_22; }
	inline void set_frames_22(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___frames_22 = value;
		Il2CppCodeGenWriteBarrier((&___frames_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOCOLORTIMELINE_T69FAE8EC5AB6A7E367BA935157DF7B1EDE94F11E_H
#ifndef VERTEXATTACHMENT_T3FD7C7E4270CEF2803E8C524DD6D4E91DB769631_H
#define VERTEXATTACHMENT_T3FD7C7E4270CEF2803E8C524DD6D4E91DB769631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.VertexAttachment
struct  VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631  : public Attachment_tCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92
{
public:
	// System.Int32 Spine.VertexAttachment::id
	int32_t ___id_3;
	// System.Int32[] Spine.VertexAttachment::bones
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___bones_4;
	// System.Single[] Spine.VertexAttachment::vertices
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___vertices_5;
	// System.Int32 Spine.VertexAttachment::worldVerticesLength
	int32_t ___worldVerticesLength_6;

public:
	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631, ___id_3)); }
	inline int32_t get_id_3() const { return ___id_3; }
	inline int32_t* get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(int32_t value)
	{
		___id_3 = value;
	}

	inline static int32_t get_offset_of_bones_4() { return static_cast<int32_t>(offsetof(VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631, ___bones_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_bones_4() const { return ___bones_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_bones_4() { return &___bones_4; }
	inline void set_bones_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___bones_4 = value;
		Il2CppCodeGenWriteBarrier((&___bones_4), value);
	}

	inline static int32_t get_offset_of_vertices_5() { return static_cast<int32_t>(offsetof(VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631, ___vertices_5)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_vertices_5() const { return ___vertices_5; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_vertices_5() { return &___vertices_5; }
	inline void set_vertices_5(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___vertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_5), value);
	}

	inline static int32_t get_offset_of_worldVerticesLength_6() { return static_cast<int32_t>(offsetof(VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631, ___worldVerticesLength_6)); }
	inline int32_t get_worldVerticesLength_6() const { return ___worldVerticesLength_6; }
	inline int32_t* get_address_of_worldVerticesLength_6() { return &___worldVerticesLength_6; }
	inline void set_worldVerticesLength_6(int32_t value)
	{
		___worldVerticesLength_6 = value;
	}
};

struct VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631_StaticFields
{
public:
	// System.Int32 Spine.VertexAttachment::nextID
	int32_t ___nextID_1;
	// System.Object Spine.VertexAttachment::nextIdLock
	RuntimeObject * ___nextIdLock_2;

public:
	inline static int32_t get_offset_of_nextID_1() { return static_cast<int32_t>(offsetof(VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631_StaticFields, ___nextID_1)); }
	inline int32_t get_nextID_1() const { return ___nextID_1; }
	inline int32_t* get_address_of_nextID_1() { return &___nextID_1; }
	inline void set_nextID_1(int32_t value)
	{
		___nextID_1 = value;
	}

	inline static int32_t get_offset_of_nextIdLock_2() { return static_cast<int32_t>(offsetof(VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631_StaticFields, ___nextIdLock_2)); }
	inline RuntimeObject * get_nextIdLock_2() const { return ___nextIdLock_2; }
	inline RuntimeObject ** get_address_of_nextIdLock_2() { return &___nextIdLock_2; }
	inline void set_nextIdLock_2(RuntimeObject * value)
	{
		___nextIdLock_2 = value;
		Il2CppCodeGenWriteBarrier((&___nextIdLock_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXATTACHMENT_T3FD7C7E4270CEF2803E8C524DD6D4E91DB769631_H
#ifndef SPINESKELETONFLIPBEHAVIOUR_T881D1AD276451B7A71EDB55CC0849B6F8E71EB5C_H
#define SPINESKELETONFLIPBEHAVIOUR_T881D1AD276451B7A71EDB55CC0849B6F8E71EB5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpineSkeletonFlipBehaviour
struct  SpineSkeletonFlipBehaviour_t881D1AD276451B7A71EDB55CC0849B6F8E71EB5C  : public PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01
{
public:
	// System.Boolean SpineSkeletonFlipBehaviour::flipX
	bool ___flipX_0;
	// System.Boolean SpineSkeletonFlipBehaviour::flipY
	bool ___flipY_1;

public:
	inline static int32_t get_offset_of_flipX_0() { return static_cast<int32_t>(offsetof(SpineSkeletonFlipBehaviour_t881D1AD276451B7A71EDB55CC0849B6F8E71EB5C, ___flipX_0)); }
	inline bool get_flipX_0() const { return ___flipX_0; }
	inline bool* get_address_of_flipX_0() { return &___flipX_0; }
	inline void set_flipX_0(bool value)
	{
		___flipX_0 = value;
	}

	inline static int32_t get_offset_of_flipY_1() { return static_cast<int32_t>(offsetof(SpineSkeletonFlipBehaviour_t881D1AD276451B7A71EDB55CC0849B6F8E71EB5C, ___flipY_1)); }
	inline bool get_flipY_1() const { return ___flipY_1; }
	inline bool* get_address_of_flipY_1() { return &___flipY_1; }
	inline void set_flipY_1(bool value)
	{
		___flipY_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESKELETONFLIPBEHAVIOUR_T881D1AD276451B7A71EDB55CC0849B6F8E71EB5C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#define SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_HighlightedSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_PressedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_DisabledSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef EADMINFUNCTYPE_TF7B1C80396FD2B29BD0659D720064CE0F64EC460_H
#define EADMINFUNCTYPE_TF7B1C80396FD2B29BD0659D720064CE0F64EC460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdministratorManager/eAdminFuncType
struct  eAdminFuncType_tF7B1C80396FD2B29BD0659D720064CE0F64EC460 
{
public:
	// System.Int32 AdministratorManager/eAdminFuncType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eAdminFuncType_tF7B1C80396FD2B29BD0659D720064CE0F64EC460, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EADMINFUNCTYPE_TF7B1C80396FD2B29BD0659D720064CE0F64EC460_H
#ifndef EADVENTUREPROFITTYPE_TD199C31799F1616902B9916C0195AE7BBE3D60CC_H
#define EADVENTUREPROFITTYPE_TD199C31799F1616902B9916C0195AE7BBE3D60CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdventureManager/eAdventureProfitType
struct  eAdventureProfitType_tD199C31799F1616902B9916C0195AE7BBE3D60CC 
{
public:
	// System.Int32 AdventureManager/eAdventureProfitType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eAdventureProfitType_tD199C31799F1616902B9916C0195AE7BBE3D60CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EADVENTUREPROFITTYPE_TD199C31799F1616902B9916C0195AE7BBE3D60CC_H
#ifndef EMONEYTYPE_T9A22FBEEECC2803A11A7756315481457B389F4C1_H
#define EMONEYTYPE_T9A22FBEEECC2803A11A7756315481457B389F4C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataManager/eMoneyType
struct  eMoneyType_t9A22FBEEECC2803A11A7756315481457B389F4C1 
{
public:
	// System.Int32 DataManager/eMoneyType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eMoneyType_t9A22FBEEECC2803A11A7756315481457B389F4C1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMONEYTYPE_T9A22FBEEECC2803A11A7756315481457B389F4C1_H
#ifndef EDISTRICTSTATUS_TA21EDB657F7184F24E00E5C52D26254A33458EC8_H
#define EDISTRICTSTATUS_TA21EDB657F7184F24E00E5C52D26254A33458EC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapManager/eDistrictStatus
struct  eDistrictStatus_tA21EDB657F7184F24E00E5C52D26254A33458EC8 
{
public:
	// System.Int32 MapManager/eDistrictStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eDistrictStatus_tA21EDB657F7184F24E00E5C52D26254A33458EC8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDISTRICTSTATUS_TA21EDB657F7184F24E00E5C52D26254A33458EC8_H
#ifndef TOKEN_TF9F61D56B1A37EACCC9FDBF91EDD4606F3440BA0_H
#define TOKEN_TF9F61D56B1A37EACCC9FDBF91EDD4606F3440BA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpJson.Lexer/Token
struct  Token_tF9F61D56B1A37EACCC9FDBF91EDD4606F3440BA0 
{
public:
	// System.Int32 SharpJson.Lexer/Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_tF9F61D56B1A37EACCC9FDBF91EDD4606F3440BA0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_TF9F61D56B1A37EACCC9FDBF91EDD4606F3440BA0_H
#ifndef EITEMTYPE_T796F7E98F0ED3F4BC872A243ECB26EBF2A5A25A4_H
#define EITEMTYPE_T796F7E98F0ED3F4BC872A243ECB26EBF2A5A25A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppinMall/eItemType
struct  eItemType_t796F7E98F0ED3F4BC872A243ECB26EBF2A5A25A4 
{
public:
	// System.Int32 ShoppinMall/eItemType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eItemType_t796F7E98F0ED3F4BC872A243ECB26EBF2A5A25A4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EITEMTYPE_T796F7E98F0ED3F4BC872A243ECB26EBF2A5A25A4_H
#ifndef EPRICESUBTYPE_T452224C5C4361AD927ABAAB951C185B7D2801B5D_H
#define EPRICESUBTYPE_T452224C5C4361AD927ABAAB951C185B7D2801B5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppinMall/ePriceSubType
struct  ePriceSubType_t452224C5C4361AD927ABAAB951C185B7D2801B5D 
{
public:
	// System.Int32 ShoppinMall/ePriceSubType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ePriceSubType_t452224C5C4361AD927ABAAB951C185B7D2801B5D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPRICESUBTYPE_T452224C5C4361AD927ABAAB951C185B7D2801B5D_H
#ifndef EPRICETYPE_T2A0ABDBAB8B516FAA133FC73FC2F9C0F3B46E4EA_H
#define EPRICETYPE_T2A0ABDBAB8B516FAA133FC73FC2F9C0F3B46E4EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppinMall/ePriceType
struct  ePriceType_t2A0ABDBAB8B516FAA133FC73FC2F9C0F3B46E4EA 
{
public:
	// System.Int32 ShoppinMall/ePriceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ePriceType_t2A0ABDBAB8B516FAA133FC73FC2F9C0F3B46E4EA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EPRICETYPE_T2A0ABDBAB8B516FAA133FC73FC2F9C0F3B46E4EA_H
#ifndef SSHOPPINGMALLITEMCONFIG_T77C43AC55058BC53A189F0EEC0D09D27D9B0D058_H
#define SSHOPPINGMALLITEMCONFIG_T77C43AC55058BC53A189F0EEC0D09D27D9B0D058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppinMall/sShoppingMallItemConfig
struct  sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058 
{
public:
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nId
	int32_t ___nId_0;
	// ItemSystem/sItemConfig ShoppinMall/sShoppingMallItemConfig::itemConfig
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C  ___itemConfig_1;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nPriceType1
	int32_t ___nPriceType1_2;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nPriceSubType1
	int32_t ___nPriceSubType1_3;
	// System.Double ShoppinMall/sShoppingMallItemConfig::nPrice1
	double ___nPrice1_4;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nNum1
	int32_t ___nNum1_5;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nPriceType2
	int32_t ___nPriceType2_6;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nPriceSubType2
	int32_t ___nPriceSubType2_7;
	// System.Double ShoppinMall/sShoppingMallItemConfig::nPrice2
	double ___nPrice2_8;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nNum2
	int32_t ___nNum2_9;
	// System.Single ShoppinMall/sShoppingMallItemConfig::fRisePricePercent
	float ___fRisePricePercent_10;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nNum
	int32_t ___nNum_11;
	// System.Int32 ShoppinMall/sShoppingMallItemConfig::nOn
	int32_t ___nOn_12;
	// System.String ShoppinMall/sShoppingMallItemConfig::szDesc
	String_t* ___szDesc_13;
	// System.String ShoppinMall/sShoppingMallItemConfig::szIconId
	String_t* ___szIconId_14;
	// System.Single ShoppinMall/sShoppingMallItemConfig::fPriceRiseEachBuy
	float ___fPriceRiseEachBuy_15;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_itemConfig_1() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___itemConfig_1)); }
	inline sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C  get_itemConfig_1() const { return ___itemConfig_1; }
	inline sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C * get_address_of_itemConfig_1() { return &___itemConfig_1; }
	inline void set_itemConfig_1(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C  value)
	{
		___itemConfig_1 = value;
	}

	inline static int32_t get_offset_of_nPriceType1_2() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nPriceType1_2)); }
	inline int32_t get_nPriceType1_2() const { return ___nPriceType1_2; }
	inline int32_t* get_address_of_nPriceType1_2() { return &___nPriceType1_2; }
	inline void set_nPriceType1_2(int32_t value)
	{
		___nPriceType1_2 = value;
	}

	inline static int32_t get_offset_of_nPriceSubType1_3() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nPriceSubType1_3)); }
	inline int32_t get_nPriceSubType1_3() const { return ___nPriceSubType1_3; }
	inline int32_t* get_address_of_nPriceSubType1_3() { return &___nPriceSubType1_3; }
	inline void set_nPriceSubType1_3(int32_t value)
	{
		___nPriceSubType1_3 = value;
	}

	inline static int32_t get_offset_of_nPrice1_4() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nPrice1_4)); }
	inline double get_nPrice1_4() const { return ___nPrice1_4; }
	inline double* get_address_of_nPrice1_4() { return &___nPrice1_4; }
	inline void set_nPrice1_4(double value)
	{
		___nPrice1_4 = value;
	}

	inline static int32_t get_offset_of_nNum1_5() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nNum1_5)); }
	inline int32_t get_nNum1_5() const { return ___nNum1_5; }
	inline int32_t* get_address_of_nNum1_5() { return &___nNum1_5; }
	inline void set_nNum1_5(int32_t value)
	{
		___nNum1_5 = value;
	}

	inline static int32_t get_offset_of_nPriceType2_6() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nPriceType2_6)); }
	inline int32_t get_nPriceType2_6() const { return ___nPriceType2_6; }
	inline int32_t* get_address_of_nPriceType2_6() { return &___nPriceType2_6; }
	inline void set_nPriceType2_6(int32_t value)
	{
		___nPriceType2_6 = value;
	}

	inline static int32_t get_offset_of_nPriceSubType2_7() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nPriceSubType2_7)); }
	inline int32_t get_nPriceSubType2_7() const { return ___nPriceSubType2_7; }
	inline int32_t* get_address_of_nPriceSubType2_7() { return &___nPriceSubType2_7; }
	inline void set_nPriceSubType2_7(int32_t value)
	{
		___nPriceSubType2_7 = value;
	}

	inline static int32_t get_offset_of_nPrice2_8() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nPrice2_8)); }
	inline double get_nPrice2_8() const { return ___nPrice2_8; }
	inline double* get_address_of_nPrice2_8() { return &___nPrice2_8; }
	inline void set_nPrice2_8(double value)
	{
		___nPrice2_8 = value;
	}

	inline static int32_t get_offset_of_nNum2_9() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nNum2_9)); }
	inline int32_t get_nNum2_9() const { return ___nNum2_9; }
	inline int32_t* get_address_of_nNum2_9() { return &___nNum2_9; }
	inline void set_nNum2_9(int32_t value)
	{
		___nNum2_9 = value;
	}

	inline static int32_t get_offset_of_fRisePricePercent_10() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___fRisePricePercent_10)); }
	inline float get_fRisePricePercent_10() const { return ___fRisePricePercent_10; }
	inline float* get_address_of_fRisePricePercent_10() { return &___fRisePricePercent_10; }
	inline void set_fRisePricePercent_10(float value)
	{
		___fRisePricePercent_10 = value;
	}

	inline static int32_t get_offset_of_nNum_11() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nNum_11)); }
	inline int32_t get_nNum_11() const { return ___nNum_11; }
	inline int32_t* get_address_of_nNum_11() { return &___nNum_11; }
	inline void set_nNum_11(int32_t value)
	{
		___nNum_11 = value;
	}

	inline static int32_t get_offset_of_nOn_12() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___nOn_12)); }
	inline int32_t get_nOn_12() const { return ___nOn_12; }
	inline int32_t* get_address_of_nOn_12() { return &___nOn_12; }
	inline void set_nOn_12(int32_t value)
	{
		___nOn_12 = value;
	}

	inline static int32_t get_offset_of_szDesc_13() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___szDesc_13)); }
	inline String_t* get_szDesc_13() const { return ___szDesc_13; }
	inline String_t** get_address_of_szDesc_13() { return &___szDesc_13; }
	inline void set_szDesc_13(String_t* value)
	{
		___szDesc_13 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_13), value);
	}

	inline static int32_t get_offset_of_szIconId_14() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___szIconId_14)); }
	inline String_t* get_szIconId_14() const { return ___szIconId_14; }
	inline String_t** get_address_of_szIconId_14() { return &___szIconId_14; }
	inline void set_szIconId_14(String_t* value)
	{
		___szIconId_14 = value;
		Il2CppCodeGenWriteBarrier((&___szIconId_14), value);
	}

	inline static int32_t get_offset_of_fPriceRiseEachBuy_15() { return static_cast<int32_t>(offsetof(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058, ___fPriceRiseEachBuy_15)); }
	inline float get_fPriceRiseEachBuy_15() const { return ___fPriceRiseEachBuy_15; }
	inline float* get_address_of_fPriceRiseEachBuy_15() { return &___fPriceRiseEachBuy_15; }
	inline void set_fPriceRiseEachBuy_15(float value)
	{
		___fPriceRiseEachBuy_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ShoppinMall/sShoppingMallItemConfig
struct sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058_marshaled_pinvoke
{
	int32_t ___nId_0;
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C_marshaled_pinvoke ___itemConfig_1;
	int32_t ___nPriceType1_2;
	int32_t ___nPriceSubType1_3;
	double ___nPrice1_4;
	int32_t ___nNum1_5;
	int32_t ___nPriceType2_6;
	int32_t ___nPriceSubType2_7;
	double ___nPrice2_8;
	int32_t ___nNum2_9;
	float ___fRisePricePercent_10;
	int32_t ___nNum_11;
	int32_t ___nOn_12;
	char* ___szDesc_13;
	char* ___szIconId_14;
	float ___fPriceRiseEachBuy_15;
};
// Native definition for COM marshalling of ShoppinMall/sShoppingMallItemConfig
struct sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058_marshaled_com
{
	int32_t ___nId_0;
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C_marshaled_com ___itemConfig_1;
	int32_t ___nPriceType1_2;
	int32_t ___nPriceSubType1_3;
	double ___nPrice1_4;
	int32_t ___nNum1_5;
	int32_t ___nPriceType2_6;
	int32_t ___nPriceSubType2_7;
	double ___nPrice2_8;
	int32_t ___nNum2_9;
	float ___fRisePricePercent_10;
	int32_t ___nNum_11;
	int32_t ___nOn_12;
	Il2CppChar* ___szDesc_13;
	Il2CppChar* ___szIconId_14;
	float ___fPriceRiseEachBuy_15;
};
#endif // SSHOPPINGMALLITEMCONFIG_T77C43AC55058BC53A189F0EEC0D09D27D9B0D058_H
#ifndef ESKILLTYPE_T6B98D3D74E24B3B140959305730C30677538C578_H
#define ESKILLTYPE_T6B98D3D74E24B3B140959305730C30677538C578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkillManager/eSkillType
struct  eSkillType_t6B98D3D74E24B3B140959305730C30677538C578 
{
public:
	// System.Int32 SkillManager/eSkillType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eSkillType_t6B98D3D74E24B3B140959305730C30677538C578, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESKILLTYPE_T6B98D3D74E24B3B140959305730C30677538C578_H
#ifndef ATTACHMENTTYPE_T1F679C7DFF0FA3CEDB51DD23B2EA3B4F1EFA90A8_H
#define ATTACHMENTTYPE_T1F679C7DFF0FA3CEDB51DD23B2EA3B4F1EFA90A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AttachmentType
struct  AttachmentType_t1F679C7DFF0FA3CEDB51DD23B2EA3B4F1EFA90A8 
{
public:
	// System.Int32 Spine.AttachmentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttachmentType_t1F679C7DFF0FA3CEDB51DD23B2EA3B4F1EFA90A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTTYPE_T1F679C7DFF0FA3CEDB51DD23B2EA3B4F1EFA90A8_H
#ifndef BLENDMODE_T36C159CFDBBD6C33AFD2B2392A2EA71889DDD0E1_H
#define BLENDMODE_T36C159CFDBBD6C33AFD2B2392A2EA71889DDD0E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BlendMode
struct  BlendMode_t36C159CFDBBD6C33AFD2B2392A2EA71889DDD0E1 
{
public:
	// System.Int32 Spine.BlendMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlendMode_t36C159CFDBBD6C33AFD2B2392A2EA71889DDD0E1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDMODE_T36C159CFDBBD6C33AFD2B2392A2EA71889DDD0E1_H
#ifndef BOUNDINGBOXATTACHMENT_TE2A96EC447B439870D918C9F72B9093D49707931_H
#define BOUNDINGBOXATTACHMENT_TE2A96EC447B439870D918C9F72B9093D49707931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BoundingBoxAttachment
struct  BoundingBoxAttachment_tE2A96EC447B439870D918C9F72B9093D49707931  : public VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXATTACHMENT_TE2A96EC447B439870D918C9F72B9093D49707931_H
#ifndef CLIPPINGATTACHMENT_TB72A005F3E80A08C9BF2D903BBD1D951342AB490_H
#define CLIPPINGATTACHMENT_TB72A005F3E80A08C9BF2D903BBD1D951342AB490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ClippingAttachment
struct  ClippingAttachment_tB72A005F3E80A08C9BF2D903BBD1D951342AB490  : public VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631
{
public:
	// Spine.SlotData Spine.ClippingAttachment::endSlot
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738 * ___endSlot_7;

public:
	inline static int32_t get_offset_of_endSlot_7() { return static_cast<int32_t>(offsetof(ClippingAttachment_tB72A005F3E80A08C9BF2D903BBD1D951342AB490, ___endSlot_7)); }
	inline SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738 * get_endSlot_7() const { return ___endSlot_7; }
	inline SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738 ** get_address_of_endSlot_7() { return &___endSlot_7; }
	inline void set_endSlot_7(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738 * value)
	{
		___endSlot_7 = value;
		Il2CppCodeGenWriteBarrier((&___endSlot_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPINGATTACHMENT_TB72A005F3E80A08C9BF2D903BBD1D951342AB490_H
#ifndef EVENTTYPE_TD27E3F173F4CA745D9BDD0CD35BF292009CB585F_H
#define EVENTTYPE_TD27E3F173F4CA745D9BDD0CD35BF292009CB585F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventQueue/EventType
struct  EventType_tD27E3F173F4CA745D9BDD0CD35BF292009CB585F 
{
public:
	// System.Int32 Spine.EventQueue/EventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventType_tD27E3F173F4CA745D9BDD0CD35BF292009CB585F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_TD27E3F173F4CA745D9BDD0CD35BF292009CB585F_H
#ifndef FORMAT_TB01BD241CBCEF4B723D11A239A0A1BD9CC5AFF4D_H
#define FORMAT_TB01BD241CBCEF4B723D11A239A0A1BD9CC5AFF4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Format
struct  Format_tB01BD241CBCEF4B723D11A239A0A1BD9CC5AFF4D 
{
public:
	// System.Int32 Spine.Format::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Format_tB01BD241CBCEF4B723D11A239A0A1BD9CC5AFF4D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMAT_TB01BD241CBCEF4B723D11A239A0A1BD9CC5AFF4D_H
#ifndef MESHATTACHMENT_TC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392_H
#define MESHATTACHMENT_TC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MeshAttachment
struct  MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392  : public VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631
{
public:
	// System.Single Spine.MeshAttachment::regionOffsetX
	float ___regionOffsetX_7;
	// System.Single Spine.MeshAttachment::regionOffsetY
	float ___regionOffsetY_8;
	// System.Single Spine.MeshAttachment::regionWidth
	float ___regionWidth_9;
	// System.Single Spine.MeshAttachment::regionHeight
	float ___regionHeight_10;
	// System.Single Spine.MeshAttachment::regionOriginalWidth
	float ___regionOriginalWidth_11;
	// System.Single Spine.MeshAttachment::regionOriginalHeight
	float ___regionOriginalHeight_12;
	// Spine.MeshAttachment Spine.MeshAttachment::parentMesh
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392 * ___parentMesh_13;
	// System.Single[] Spine.MeshAttachment::uvs
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___uvs_14;
	// System.Single[] Spine.MeshAttachment::regionUVs
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___regionUVs_15;
	// System.Int32[] Spine.MeshAttachment::triangles
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___triangles_16;
	// System.Single Spine.MeshAttachment::r
	float ___r_17;
	// System.Single Spine.MeshAttachment::g
	float ___g_18;
	// System.Single Spine.MeshAttachment::b
	float ___b_19;
	// System.Single Spine.MeshAttachment::a
	float ___a_20;
	// System.Int32 Spine.MeshAttachment::hulllength
	int32_t ___hulllength_21;
	// System.Boolean Spine.MeshAttachment::inheritDeform
	bool ___inheritDeform_22;
	// System.String Spine.MeshAttachment::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_23;
	// System.Object Spine.MeshAttachment::<RendererObject>k__BackingField
	RuntimeObject * ___U3CRendererObjectU3Ek__BackingField_24;
	// System.Single Spine.MeshAttachment::<RegionU>k__BackingField
	float ___U3CRegionUU3Ek__BackingField_25;
	// System.Single Spine.MeshAttachment::<RegionV>k__BackingField
	float ___U3CRegionVU3Ek__BackingField_26;
	// System.Single Spine.MeshAttachment::<RegionU2>k__BackingField
	float ___U3CRegionU2U3Ek__BackingField_27;
	// System.Single Spine.MeshAttachment::<RegionV2>k__BackingField
	float ___U3CRegionV2U3Ek__BackingField_28;
	// System.Boolean Spine.MeshAttachment::<RegionRotate>k__BackingField
	bool ___U3CRegionRotateU3Ek__BackingField_29;
	// System.Int32[] Spine.MeshAttachment::<Edges>k__BackingField
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___U3CEdgesU3Ek__BackingField_30;
	// System.Single Spine.MeshAttachment::<Width>k__BackingField
	float ___U3CWidthU3Ek__BackingField_31;
	// System.Single Spine.MeshAttachment::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of_regionOffsetX_7() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___regionOffsetX_7)); }
	inline float get_regionOffsetX_7() const { return ___regionOffsetX_7; }
	inline float* get_address_of_regionOffsetX_7() { return &___regionOffsetX_7; }
	inline void set_regionOffsetX_7(float value)
	{
		___regionOffsetX_7 = value;
	}

	inline static int32_t get_offset_of_regionOffsetY_8() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___regionOffsetY_8)); }
	inline float get_regionOffsetY_8() const { return ___regionOffsetY_8; }
	inline float* get_address_of_regionOffsetY_8() { return &___regionOffsetY_8; }
	inline void set_regionOffsetY_8(float value)
	{
		___regionOffsetY_8 = value;
	}

	inline static int32_t get_offset_of_regionWidth_9() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___regionWidth_9)); }
	inline float get_regionWidth_9() const { return ___regionWidth_9; }
	inline float* get_address_of_regionWidth_9() { return &___regionWidth_9; }
	inline void set_regionWidth_9(float value)
	{
		___regionWidth_9 = value;
	}

	inline static int32_t get_offset_of_regionHeight_10() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___regionHeight_10)); }
	inline float get_regionHeight_10() const { return ___regionHeight_10; }
	inline float* get_address_of_regionHeight_10() { return &___regionHeight_10; }
	inline void set_regionHeight_10(float value)
	{
		___regionHeight_10 = value;
	}

	inline static int32_t get_offset_of_regionOriginalWidth_11() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___regionOriginalWidth_11)); }
	inline float get_regionOriginalWidth_11() const { return ___regionOriginalWidth_11; }
	inline float* get_address_of_regionOriginalWidth_11() { return &___regionOriginalWidth_11; }
	inline void set_regionOriginalWidth_11(float value)
	{
		___regionOriginalWidth_11 = value;
	}

	inline static int32_t get_offset_of_regionOriginalHeight_12() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___regionOriginalHeight_12)); }
	inline float get_regionOriginalHeight_12() const { return ___regionOriginalHeight_12; }
	inline float* get_address_of_regionOriginalHeight_12() { return &___regionOriginalHeight_12; }
	inline void set_regionOriginalHeight_12(float value)
	{
		___regionOriginalHeight_12 = value;
	}

	inline static int32_t get_offset_of_parentMesh_13() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___parentMesh_13)); }
	inline MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392 * get_parentMesh_13() const { return ___parentMesh_13; }
	inline MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392 ** get_address_of_parentMesh_13() { return &___parentMesh_13; }
	inline void set_parentMesh_13(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392 * value)
	{
		___parentMesh_13 = value;
		Il2CppCodeGenWriteBarrier((&___parentMesh_13), value);
	}

	inline static int32_t get_offset_of_uvs_14() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___uvs_14)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_uvs_14() const { return ___uvs_14; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_uvs_14() { return &___uvs_14; }
	inline void set_uvs_14(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___uvs_14 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_14), value);
	}

	inline static int32_t get_offset_of_regionUVs_15() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___regionUVs_15)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_regionUVs_15() const { return ___regionUVs_15; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_regionUVs_15() { return &___regionUVs_15; }
	inline void set_regionUVs_15(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___regionUVs_15 = value;
		Il2CppCodeGenWriteBarrier((&___regionUVs_15), value);
	}

	inline static int32_t get_offset_of_triangles_16() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___triangles_16)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_triangles_16() const { return ___triangles_16; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_triangles_16() { return &___triangles_16; }
	inline void set_triangles_16(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___triangles_16 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_16), value);
	}

	inline static int32_t get_offset_of_r_17() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___r_17)); }
	inline float get_r_17() const { return ___r_17; }
	inline float* get_address_of_r_17() { return &___r_17; }
	inline void set_r_17(float value)
	{
		___r_17 = value;
	}

	inline static int32_t get_offset_of_g_18() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___g_18)); }
	inline float get_g_18() const { return ___g_18; }
	inline float* get_address_of_g_18() { return &___g_18; }
	inline void set_g_18(float value)
	{
		___g_18 = value;
	}

	inline static int32_t get_offset_of_b_19() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___b_19)); }
	inline float get_b_19() const { return ___b_19; }
	inline float* get_address_of_b_19() { return &___b_19; }
	inline void set_b_19(float value)
	{
		___b_19 = value;
	}

	inline static int32_t get_offset_of_a_20() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___a_20)); }
	inline float get_a_20() const { return ___a_20; }
	inline float* get_address_of_a_20() { return &___a_20; }
	inline void set_a_20(float value)
	{
		___a_20 = value;
	}

	inline static int32_t get_offset_of_hulllength_21() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___hulllength_21)); }
	inline int32_t get_hulllength_21() const { return ___hulllength_21; }
	inline int32_t* get_address_of_hulllength_21() { return &___hulllength_21; }
	inline void set_hulllength_21(int32_t value)
	{
		___hulllength_21 = value;
	}

	inline static int32_t get_offset_of_inheritDeform_22() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___inheritDeform_22)); }
	inline bool get_inheritDeform_22() const { return ___inheritDeform_22; }
	inline bool* get_address_of_inheritDeform_22() { return &___inheritDeform_22; }
	inline void set_inheritDeform_22(bool value)
	{
		___inheritDeform_22 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___U3CPathU3Ek__BackingField_23)); }
	inline String_t* get_U3CPathU3Ek__BackingField_23() const { return ___U3CPathU3Ek__BackingField_23; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_23() { return &___U3CPathU3Ek__BackingField_23; }
	inline void set_U3CPathU3Ek__BackingField_23(String_t* value)
	{
		___U3CPathU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CRendererObjectU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___U3CRendererObjectU3Ek__BackingField_24)); }
	inline RuntimeObject * get_U3CRendererObjectU3Ek__BackingField_24() const { return ___U3CRendererObjectU3Ek__BackingField_24; }
	inline RuntimeObject ** get_address_of_U3CRendererObjectU3Ek__BackingField_24() { return &___U3CRendererObjectU3Ek__BackingField_24; }
	inline void set_U3CRendererObjectU3Ek__BackingField_24(RuntimeObject * value)
	{
		___U3CRendererObjectU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRendererObjectU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_U3CRegionUU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___U3CRegionUU3Ek__BackingField_25)); }
	inline float get_U3CRegionUU3Ek__BackingField_25() const { return ___U3CRegionUU3Ek__BackingField_25; }
	inline float* get_address_of_U3CRegionUU3Ek__BackingField_25() { return &___U3CRegionUU3Ek__BackingField_25; }
	inline void set_U3CRegionUU3Ek__BackingField_25(float value)
	{
		___U3CRegionUU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CRegionVU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___U3CRegionVU3Ek__BackingField_26)); }
	inline float get_U3CRegionVU3Ek__BackingField_26() const { return ___U3CRegionVU3Ek__BackingField_26; }
	inline float* get_address_of_U3CRegionVU3Ek__BackingField_26() { return &___U3CRegionVU3Ek__BackingField_26; }
	inline void set_U3CRegionVU3Ek__BackingField_26(float value)
	{
		___U3CRegionVU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CRegionU2U3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___U3CRegionU2U3Ek__BackingField_27)); }
	inline float get_U3CRegionU2U3Ek__BackingField_27() const { return ___U3CRegionU2U3Ek__BackingField_27; }
	inline float* get_address_of_U3CRegionU2U3Ek__BackingField_27() { return &___U3CRegionU2U3Ek__BackingField_27; }
	inline void set_U3CRegionU2U3Ek__BackingField_27(float value)
	{
		___U3CRegionU2U3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CRegionV2U3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___U3CRegionV2U3Ek__BackingField_28)); }
	inline float get_U3CRegionV2U3Ek__BackingField_28() const { return ___U3CRegionV2U3Ek__BackingField_28; }
	inline float* get_address_of_U3CRegionV2U3Ek__BackingField_28() { return &___U3CRegionV2U3Ek__BackingField_28; }
	inline void set_U3CRegionV2U3Ek__BackingField_28(float value)
	{
		___U3CRegionV2U3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CRegionRotateU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___U3CRegionRotateU3Ek__BackingField_29)); }
	inline bool get_U3CRegionRotateU3Ek__BackingField_29() const { return ___U3CRegionRotateU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CRegionRotateU3Ek__BackingField_29() { return &___U3CRegionRotateU3Ek__BackingField_29; }
	inline void set_U3CRegionRotateU3Ek__BackingField_29(bool value)
	{
		___U3CRegionRotateU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CEdgesU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___U3CEdgesU3Ek__BackingField_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_U3CEdgesU3Ek__BackingField_30() const { return ___U3CEdgesU3Ek__BackingField_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_U3CEdgesU3Ek__BackingField_30() { return &___U3CEdgesU3Ek__BackingField_30; }
	inline void set_U3CEdgesU3Ek__BackingField_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___U3CEdgesU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEdgesU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___U3CWidthU3Ek__BackingField_31)); }
	inline float get_U3CWidthU3Ek__BackingField_31() const { return ___U3CWidthU3Ek__BackingField_31; }
	inline float* get_address_of_U3CWidthU3Ek__BackingField_31() { return &___U3CWidthU3Ek__BackingField_31; }
	inline void set_U3CWidthU3Ek__BackingField_31(float value)
	{
		___U3CWidthU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392, ___U3CHeightU3Ek__BackingField_32)); }
	inline float get_U3CHeightU3Ek__BackingField_32() const { return ___U3CHeightU3Ek__BackingField_32; }
	inline float* get_address_of_U3CHeightU3Ek__BackingField_32() { return &___U3CHeightU3Ek__BackingField_32; }
	inline void set_U3CHeightU3Ek__BackingField_32(float value)
	{
		___U3CHeightU3Ek__BackingField_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHATTACHMENT_TC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392_H
#ifndef MIXDIRECTION_TE1DA6601177FB68DD7DB387CD2C818140069C900_H
#define MIXDIRECTION_TE1DA6601177FB68DD7DB387CD2C818140069C900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MixDirection
struct  MixDirection_tE1DA6601177FB68DD7DB387CD2C818140069C900 
{
public:
	// System.Int32 Spine.MixDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MixDirection_tE1DA6601177FB68DD7DB387CD2C818140069C900, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXDIRECTION_TE1DA6601177FB68DD7DB387CD2C818140069C900_H
#ifndef MIXPOSE_T65B6F539ED15C820349F7845BA4977ADFC03C1F5_H
#define MIXPOSE_T65B6F539ED15C820349F7845BA4977ADFC03C1F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MixPose
struct  MixPose_t65B6F539ED15C820349F7845BA4977ADFC03C1F5 
{
public:
	// System.Int32 Spine.MixPose::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MixPose_t65B6F539ED15C820349F7845BA4977ADFC03C1F5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXPOSE_T65B6F539ED15C820349F7845BA4977ADFC03C1F5_H
#ifndef PATHATTACHMENT_T7B1C838F67212A550EEF16DDA94F9DA8C9ED1733_H
#define PATHATTACHMENT_T7B1C838F67212A550EEF16DDA94F9DA8C9ED1733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathAttachment
struct  PathAttachment_t7B1C838F67212A550EEF16DDA94F9DA8C9ED1733  : public VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631
{
public:
	// System.Single[] Spine.PathAttachment::lengths
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___lengths_7;
	// System.Boolean Spine.PathAttachment::closed
	bool ___closed_8;
	// System.Boolean Spine.PathAttachment::constantSpeed
	bool ___constantSpeed_9;

public:
	inline static int32_t get_offset_of_lengths_7() { return static_cast<int32_t>(offsetof(PathAttachment_t7B1C838F67212A550EEF16DDA94F9DA8C9ED1733, ___lengths_7)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_lengths_7() const { return ___lengths_7; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_lengths_7() { return &___lengths_7; }
	inline void set_lengths_7(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___lengths_7 = value;
		Il2CppCodeGenWriteBarrier((&___lengths_7), value);
	}

	inline static int32_t get_offset_of_closed_8() { return static_cast<int32_t>(offsetof(PathAttachment_t7B1C838F67212A550EEF16DDA94F9DA8C9ED1733, ___closed_8)); }
	inline bool get_closed_8() const { return ___closed_8; }
	inline bool* get_address_of_closed_8() { return &___closed_8; }
	inline void set_closed_8(bool value)
	{
		___closed_8 = value;
	}

	inline static int32_t get_offset_of_constantSpeed_9() { return static_cast<int32_t>(offsetof(PathAttachment_t7B1C838F67212A550EEF16DDA94F9DA8C9ED1733, ___constantSpeed_9)); }
	inline bool get_constantSpeed_9() const { return ___constantSpeed_9; }
	inline bool* get_address_of_constantSpeed_9() { return &___constantSpeed_9; }
	inline void set_constantSpeed_9(bool value)
	{
		___constantSpeed_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHATTACHMENT_T7B1C838F67212A550EEF16DDA94F9DA8C9ED1733_H
#ifndef PATHCONSTRAINTSPACINGTIMELINE_TDEA29E775F5ECAB1F8E09235AFEDBD614CF83796_H
#define PATHCONSTRAINTSPACINGTIMELINE_TDEA29E775F5ECAB1F8E09235AFEDBD614CF83796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintSpacingTimeline
struct  PathConstraintSpacingTimeline_tDEA29E775F5ECAB1F8E09235AFEDBD614CF83796  : public PathConstraintPositionTimeline_t067633A6B9AE48EF83FE73BBC66EA1DC47D7DACE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTSPACINGTIMELINE_TDEA29E775F5ECAB1F8E09235AFEDBD614CF83796_H
#ifndef SCALETIMELINE_T1011924EB2AF4FB783DF13AFF3DBF05DB117DEF9_H
#define SCALETIMELINE_T1011924EB2AF4FB783DF13AFF3DBF05DB117DEF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ScaleTimeline
struct  ScaleTimeline_t1011924EB2AF4FB783DF13AFF3DBF05DB117DEF9  : public TranslateTimeline_t35CE691D668D75F2779A2D495380C7F6E563FF88
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALETIMELINE_T1011924EB2AF4FB783DF13AFF3DBF05DB117DEF9_H
#ifndef SHEARTIMELINE_T35FAC5DCDDF9CBE1989938FDDFE4A5E3A33005C1_H
#define SHEARTIMELINE_T35FAC5DCDDF9CBE1989938FDDFE4A5E3A33005C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ShearTimeline
struct  ShearTimeline_t35FAC5DCDDF9CBE1989938FDDFE4A5E3A33005C1  : public TranslateTimeline_t35CE691D668D75F2779A2D495380C7F6E563FF88
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHEARTIMELINE_T35FAC5DCDDF9CBE1989938FDDFE4A5E3A33005C1_H
#ifndef TEXTUREFILTER_T8E51CE4FC4F71CCF888F884595E4A2A780879B68_H
#define TEXTUREFILTER_T8E51CE4FC4F71CCF888F884595E4A2A780879B68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TextureFilter
struct  TextureFilter_t8E51CE4FC4F71CCF888F884595E4A2A780879B68 
{
public:
	// System.Int32 Spine.TextureFilter::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFilter_t8E51CE4FC4F71CCF888F884595E4A2A780879B68, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFILTER_T8E51CE4FC4F71CCF888F884595E4A2A780879B68_H
#ifndef TEXTUREWRAP_TAB8018DC28DE360488DA2417277C798FED128617_H
#define TEXTUREWRAP_TAB8018DC28DE360488DA2417277C798FED128617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TextureWrap
struct  TextureWrap_tAB8018DC28DE360488DA2417277C798FED128617 
{
public:
	// System.Int32 Spine.TextureWrap::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureWrap_tAB8018DC28DE360488DA2417277C798FED128617, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAP_TAB8018DC28DE360488DA2417277C798FED128617_H
#ifndef TIMELINETYPE_T1FD5B4020300DAA50249E7BAB7328A7FEFC68141_H
#define TIMELINETYPE_T1FD5B4020300DAA50249E7BAB7328A7FEFC68141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TimelineType
struct  TimelineType_t1FD5B4020300DAA50249E7BAB7328A7FEFC68141 
{
public:
	// System.Int32 Spine.TimelineType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TimelineType_t1FD5B4020300DAA50249E7BAB7328A7FEFC68141, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINETYPE_T1FD5B4020300DAA50249E7BAB7328A7FEFC68141_H
#ifndef TRANSFORMMODE_T468F038B8F71E82224721942A1009106EB740ED4_H
#define TRANSFORMMODE_T468F038B8F71E82224721942A1009106EB740ED4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformMode
struct  TransformMode_t468F038B8F71E82224721942A1009106EB740ED4 
{
public:
	// System.Int32 Spine.TransformMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransformMode_t468F038B8F71E82224721942A1009106EB740ED4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMMODE_T468F038B8F71E82224721942A1009106EB740ED4_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef EUIID_TF5176DFF3CB5188E73B7887069C38DF36A4CF137_H
#define EUIID_TF5176DFF3CB5188E73B7887069C38DF36A4CF137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager/eUiId
struct  eUiId_tF5176DFF3CB5188E73B7887069C38DF36A4CF137 
{
public:
	// System.Int32 UIManager/eUiId::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eUiId_tF5176DFF3CB5188E73B7887069C38DF36A4CF137, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EUIID_TF5176DFF3CB5188E73B7887069C38DF36A4CF137_H
#ifndef ECALLBACKEVENTTYPE_TC080966288865EC90905EB86CDA59D5E3F5C38FA_H
#define ECALLBACKEVENTTYPE_TC080966288865EC90905EB86CDA59D5E3F5C38FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIMsgBox/eCallBackEventType
struct  eCallBackEventType_tC080966288865EC90905EB86CDA59D5E3F5C38FA 
{
public:
	// System.Int32 UIMsgBox/eCallBackEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eCallBackEventType_tC080966288865EC90905EB86CDA59D5E3F5C38FA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECALLBACKEVENTTYPE_TC080966288865EC90905EB86CDA59D5E3F5C38FA_H
#ifndef EUIPLANETTYPE_TFEF94613B677EB7537CEDB6472F772D9D8100A8E_H
#define EUIPLANETTYPE_TFEF94613B677EB7537CEDB6472F772D9D8100A8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPlanet/eUIPlanetType
struct  eUIPlanetType_tFEF94613B677EB7537CEDB6472F772D9D8100A8E 
{
public:
	// System.Int32 UIPlanet/eUIPlanetType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(eUIPlanetType_tFEF94613B677EB7537CEDB6472F772D9D8100A8E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EUIPLANETTYPE_TFEF94613B677EB7537CEDB6472F772D9D8100A8E_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#define COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_NormalColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_HighlightedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_PressedColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_DisabledColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA_H
#ifndef MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#define MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T93F92BD50B147AE38D82BA33FA77FD247A59FE26_H
#ifndef SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#define SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_tF089B96B46A592693753CBF23C52A3887632D210 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_tF089B96B46A592693753CBF23C52A3887632D210, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_TF089B96B46A592693753CBF23C52A3887632D210_H
#ifndef TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#define TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_tA9261C608B54C52324084A0B080E7A3E0548A181 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tA9261C608B54C52324084A0B080E7A3E0548A181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_TA9261C608B54C52324084A0B080E7A3E0548A181_H
#ifndef SADMINCONFIG_TCD81360114F98AE183656DD00005E18A400A4827_H
#define SADMINCONFIG_TCD81360114F98AE183656DD00005E18A400A4827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdministratorManager/sAdminConfig
struct  sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827 
{
public:
	// System.Int32 AdministratorManager/sAdminConfig::nId
	int32_t ___nId_0;
	// System.String AdministratorManager/sAdminConfig::szName
	String_t* ___szName_1;
	// System.String AdministratorManager/sAdminConfig::szQuality
	String_t* ___szQuality_2;
	// System.Int32 AdministratorManager/sAdminConfig::nQuality
	int32_t ___nQuality_3;
	// System.String AdministratorManager/sAdminConfig::szDesc
	String_t* ___szDesc_4;
	// AdministratorManager/eAdminFuncType AdministratorManager/sAdminConfig::eType
	int32_t ___eType_5;
	// System.Single AdministratorManager/sAdminConfig::fValue
	float ___fValue_6;
	// System.Int32 AdministratorManager/sAdminConfig::nDuration
	int32_t ___nDuration_7;
	// System.Int32 AdministratorManager/sAdminConfig::nColddown
	int32_t ___nColddown_8;
	// System.Int32 AdministratorManager/sAdminConfig::nProbabilityWeight
	int32_t ___nProbabilityWeight_9;

public:
	inline static int32_t get_offset_of_nId_0() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___nId_0)); }
	inline int32_t get_nId_0() const { return ___nId_0; }
	inline int32_t* get_address_of_nId_0() { return &___nId_0; }
	inline void set_nId_0(int32_t value)
	{
		___nId_0 = value;
	}

	inline static int32_t get_offset_of_szName_1() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___szName_1)); }
	inline String_t* get_szName_1() const { return ___szName_1; }
	inline String_t** get_address_of_szName_1() { return &___szName_1; }
	inline void set_szName_1(String_t* value)
	{
		___szName_1 = value;
		Il2CppCodeGenWriteBarrier((&___szName_1), value);
	}

	inline static int32_t get_offset_of_szQuality_2() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___szQuality_2)); }
	inline String_t* get_szQuality_2() const { return ___szQuality_2; }
	inline String_t** get_address_of_szQuality_2() { return &___szQuality_2; }
	inline void set_szQuality_2(String_t* value)
	{
		___szQuality_2 = value;
		Il2CppCodeGenWriteBarrier((&___szQuality_2), value);
	}

	inline static int32_t get_offset_of_nQuality_3() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___nQuality_3)); }
	inline int32_t get_nQuality_3() const { return ___nQuality_3; }
	inline int32_t* get_address_of_nQuality_3() { return &___nQuality_3; }
	inline void set_nQuality_3(int32_t value)
	{
		___nQuality_3 = value;
	}

	inline static int32_t get_offset_of_szDesc_4() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___szDesc_4)); }
	inline String_t* get_szDesc_4() const { return ___szDesc_4; }
	inline String_t** get_address_of_szDesc_4() { return &___szDesc_4; }
	inline void set_szDesc_4(String_t* value)
	{
		___szDesc_4 = value;
		Il2CppCodeGenWriteBarrier((&___szDesc_4), value);
	}

	inline static int32_t get_offset_of_eType_5() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___eType_5)); }
	inline int32_t get_eType_5() const { return ___eType_5; }
	inline int32_t* get_address_of_eType_5() { return &___eType_5; }
	inline void set_eType_5(int32_t value)
	{
		___eType_5 = value;
	}

	inline static int32_t get_offset_of_fValue_6() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___fValue_6)); }
	inline float get_fValue_6() const { return ___fValue_6; }
	inline float* get_address_of_fValue_6() { return &___fValue_6; }
	inline void set_fValue_6(float value)
	{
		___fValue_6 = value;
	}

	inline static int32_t get_offset_of_nDuration_7() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___nDuration_7)); }
	inline int32_t get_nDuration_7() const { return ___nDuration_7; }
	inline int32_t* get_address_of_nDuration_7() { return &___nDuration_7; }
	inline void set_nDuration_7(int32_t value)
	{
		___nDuration_7 = value;
	}

	inline static int32_t get_offset_of_nColddown_8() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___nColddown_8)); }
	inline int32_t get_nColddown_8() const { return ___nColddown_8; }
	inline int32_t* get_address_of_nColddown_8() { return &___nColddown_8; }
	inline void set_nColddown_8(int32_t value)
	{
		___nColddown_8 = value;
	}

	inline static int32_t get_offset_of_nProbabilityWeight_9() { return static_cast<int32_t>(offsetof(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827, ___nProbabilityWeight_9)); }
	inline int32_t get_nProbabilityWeight_9() const { return ___nProbabilityWeight_9; }
	inline int32_t* get_address_of_nProbabilityWeight_9() { return &___nProbabilityWeight_9; }
	inline void set_nProbabilityWeight_9(int32_t value)
	{
		___nProbabilityWeight_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of AdministratorManager/sAdminConfig
struct sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827_marshaled_pinvoke
{
	int32_t ___nId_0;
	char* ___szName_1;
	char* ___szQuality_2;
	int32_t ___nQuality_3;
	char* ___szDesc_4;
	int32_t ___eType_5;
	float ___fValue_6;
	int32_t ___nDuration_7;
	int32_t ___nColddown_8;
	int32_t ___nProbabilityWeight_9;
};
// Native definition for COM marshalling of AdministratorManager/sAdminConfig
struct sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827_marshaled_com
{
	int32_t ___nId_0;
	Il2CppChar* ___szName_1;
	Il2CppChar* ___szQuality_2;
	int32_t ___nQuality_3;
	Il2CppChar* ___szDesc_4;
	int32_t ___eType_5;
	float ___fValue_6;
	int32_t ___nDuration_7;
	int32_t ___nColddown_8;
	int32_t ___nProbabilityWeight_9;
};
#endif // SADMINCONFIG_TCD81360114F98AE183656DD00005E18A400A4827_H
#ifndef ATLASPAGE_T38CC63272BC1C1FADD75F3D26DCE8E988D1A4453_H
#define ATLASPAGE_T38CC63272BC1C1FADD75F3D26DCE8E988D1A4453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AtlasPage
struct  AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453  : public RuntimeObject
{
public:
	// System.String Spine.AtlasPage::name
	String_t* ___name_0;
	// Spine.Format Spine.AtlasPage::format
	int32_t ___format_1;
	// Spine.TextureFilter Spine.AtlasPage::minFilter
	int32_t ___minFilter_2;
	// Spine.TextureFilter Spine.AtlasPage::magFilter
	int32_t ___magFilter_3;
	// Spine.TextureWrap Spine.AtlasPage::uWrap
	int32_t ___uWrap_4;
	// Spine.TextureWrap Spine.AtlasPage::vWrap
	int32_t ___vWrap_5;
	// System.Object Spine.AtlasPage::rendererObject
	RuntimeObject * ___rendererObject_6;
	// System.Int32 Spine.AtlasPage::width
	int32_t ___width_7;
	// System.Int32 Spine.AtlasPage::height
	int32_t ___height_8;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_format_1() { return static_cast<int32_t>(offsetof(AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453, ___format_1)); }
	inline int32_t get_format_1() const { return ___format_1; }
	inline int32_t* get_address_of_format_1() { return &___format_1; }
	inline void set_format_1(int32_t value)
	{
		___format_1 = value;
	}

	inline static int32_t get_offset_of_minFilter_2() { return static_cast<int32_t>(offsetof(AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453, ___minFilter_2)); }
	inline int32_t get_minFilter_2() const { return ___minFilter_2; }
	inline int32_t* get_address_of_minFilter_2() { return &___minFilter_2; }
	inline void set_minFilter_2(int32_t value)
	{
		___minFilter_2 = value;
	}

	inline static int32_t get_offset_of_magFilter_3() { return static_cast<int32_t>(offsetof(AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453, ___magFilter_3)); }
	inline int32_t get_magFilter_3() const { return ___magFilter_3; }
	inline int32_t* get_address_of_magFilter_3() { return &___magFilter_3; }
	inline void set_magFilter_3(int32_t value)
	{
		___magFilter_3 = value;
	}

	inline static int32_t get_offset_of_uWrap_4() { return static_cast<int32_t>(offsetof(AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453, ___uWrap_4)); }
	inline int32_t get_uWrap_4() const { return ___uWrap_4; }
	inline int32_t* get_address_of_uWrap_4() { return &___uWrap_4; }
	inline void set_uWrap_4(int32_t value)
	{
		___uWrap_4 = value;
	}

	inline static int32_t get_offset_of_vWrap_5() { return static_cast<int32_t>(offsetof(AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453, ___vWrap_5)); }
	inline int32_t get_vWrap_5() const { return ___vWrap_5; }
	inline int32_t* get_address_of_vWrap_5() { return &___vWrap_5; }
	inline void set_vWrap_5(int32_t value)
	{
		___vWrap_5 = value;
	}

	inline static int32_t get_offset_of_rendererObject_6() { return static_cast<int32_t>(offsetof(AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453, ___rendererObject_6)); }
	inline RuntimeObject * get_rendererObject_6() const { return ___rendererObject_6; }
	inline RuntimeObject ** get_address_of_rendererObject_6() { return &___rendererObject_6; }
	inline void set_rendererObject_6(RuntimeObject * value)
	{
		___rendererObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___rendererObject_6), value);
	}

	inline static int32_t get_offset_of_width_7() { return static_cast<int32_t>(offsetof(AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453, ___width_7)); }
	inline int32_t get_width_7() const { return ___width_7; }
	inline int32_t* get_address_of_width_7() { return &___width_7; }
	inline void set_width_7(int32_t value)
	{
		___width_7 = value;
	}

	inline static int32_t get_offset_of_height_8() { return static_cast<int32_t>(offsetof(AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453, ___height_8)); }
	inline int32_t get_height_8() const { return ___height_8; }
	inline int32_t* get_address_of_height_8() { return &___height_8; }
	inline void set_height_8(int32_t value)
	{
		___height_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASPAGE_T38CC63272BC1C1FADD75F3D26DCE8E988D1A4453_H
#ifndef BONEDATA_T0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F_H
#define BONEDATA_T0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BoneData
struct  BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F  : public RuntimeObject
{
public:
	// System.Int32 Spine.BoneData::index
	int32_t ___index_0;
	// System.String Spine.BoneData::name
	String_t* ___name_1;
	// Spine.BoneData Spine.BoneData::parent
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * ___parent_2;
	// System.Single Spine.BoneData::length
	float ___length_3;
	// System.Single Spine.BoneData::x
	float ___x_4;
	// System.Single Spine.BoneData::y
	float ___y_5;
	// System.Single Spine.BoneData::rotation
	float ___rotation_6;
	// System.Single Spine.BoneData::scaleX
	float ___scaleX_7;
	// System.Single Spine.BoneData::scaleY
	float ___scaleY_8;
	// System.Single Spine.BoneData::shearX
	float ___shearX_9;
	// System.Single Spine.BoneData::shearY
	float ___shearY_10;
	// Spine.TransformMode Spine.BoneData::transformMode
	int32_t ___transformMode_11;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F, ___parent_2)); }
	inline BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * get_parent_2() const { return ___parent_2; }
	inline BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___parent_2), value);
	}

	inline static int32_t get_offset_of_length_3() { return static_cast<int32_t>(offsetof(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F, ___length_3)); }
	inline float get_length_3() const { return ___length_3; }
	inline float* get_address_of_length_3() { return &___length_3; }
	inline void set_length_3(float value)
	{
		___length_3 = value;
	}

	inline static int32_t get_offset_of_x_4() { return static_cast<int32_t>(offsetof(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F, ___x_4)); }
	inline float get_x_4() const { return ___x_4; }
	inline float* get_address_of_x_4() { return &___x_4; }
	inline void set_x_4(float value)
	{
		___x_4 = value;
	}

	inline static int32_t get_offset_of_y_5() { return static_cast<int32_t>(offsetof(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F, ___y_5)); }
	inline float get_y_5() const { return ___y_5; }
	inline float* get_address_of_y_5() { return &___y_5; }
	inline void set_y_5(float value)
	{
		___y_5 = value;
	}

	inline static int32_t get_offset_of_rotation_6() { return static_cast<int32_t>(offsetof(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F, ___rotation_6)); }
	inline float get_rotation_6() const { return ___rotation_6; }
	inline float* get_address_of_rotation_6() { return &___rotation_6; }
	inline void set_rotation_6(float value)
	{
		___rotation_6 = value;
	}

	inline static int32_t get_offset_of_scaleX_7() { return static_cast<int32_t>(offsetof(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F, ___scaleX_7)); }
	inline float get_scaleX_7() const { return ___scaleX_7; }
	inline float* get_address_of_scaleX_7() { return &___scaleX_7; }
	inline void set_scaleX_7(float value)
	{
		___scaleX_7 = value;
	}

	inline static int32_t get_offset_of_scaleY_8() { return static_cast<int32_t>(offsetof(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F, ___scaleY_8)); }
	inline float get_scaleY_8() const { return ___scaleY_8; }
	inline float* get_address_of_scaleY_8() { return &___scaleY_8; }
	inline void set_scaleY_8(float value)
	{
		___scaleY_8 = value;
	}

	inline static int32_t get_offset_of_shearX_9() { return static_cast<int32_t>(offsetof(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F, ___shearX_9)); }
	inline float get_shearX_9() const { return ___shearX_9; }
	inline float* get_address_of_shearX_9() { return &___shearX_9; }
	inline void set_shearX_9(float value)
	{
		___shearX_9 = value;
	}

	inline static int32_t get_offset_of_shearY_10() { return static_cast<int32_t>(offsetof(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F, ___shearY_10)); }
	inline float get_shearY_10() const { return ___shearY_10; }
	inline float* get_address_of_shearY_10() { return &___shearY_10; }
	inline void set_shearY_10(float value)
	{
		___shearY_10 = value;
	}

	inline static int32_t get_offset_of_transformMode_11() { return static_cast<int32_t>(offsetof(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F, ___transformMode_11)); }
	inline int32_t get_transformMode_11() const { return ___transformMode_11; }
	inline int32_t* get_address_of_transformMode_11() { return &___transformMode_11; }
	inline void set_transformMode_11(int32_t value)
	{
		___transformMode_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEDATA_T0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F_H
#ifndef EVENTQUEUEENTRY_T18A087AFF39EE175D3F8CF4897B97ABC9E8720CB_H
#define EVENTQUEUEENTRY_T18A087AFF39EE175D3F8CF4897B97ABC9E8720CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventQueue/EventQueueEntry
struct  EventQueueEntry_t18A087AFF39EE175D3F8CF4897B97ABC9E8720CB 
{
public:
	// Spine.EventQueue/EventType Spine.EventQueue/EventQueueEntry::type
	int32_t ___type_0;
	// Spine.TrackEntry Spine.EventQueue/EventQueueEntry::entry
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * ___entry_1;
	// Spine.Event Spine.EventQueue/EventQueueEntry::e
	Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0 * ___e_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(EventQueueEntry_t18A087AFF39EE175D3F8CF4897B97ABC9E8720CB, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_entry_1() { return static_cast<int32_t>(offsetof(EventQueueEntry_t18A087AFF39EE175D3F8CF4897B97ABC9E8720CB, ___entry_1)); }
	inline TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * get_entry_1() const { return ___entry_1; }
	inline TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 ** get_address_of_entry_1() { return &___entry_1; }
	inline void set_entry_1(TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * value)
	{
		___entry_1 = value;
		Il2CppCodeGenWriteBarrier((&___entry_1), value);
	}

	inline static int32_t get_offset_of_e_2() { return static_cast<int32_t>(offsetof(EventQueueEntry_t18A087AFF39EE175D3F8CF4897B97ABC9E8720CB, ___e_2)); }
	inline Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0 * get_e_2() const { return ___e_2; }
	inline Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0 ** get_address_of_e_2() { return &___e_2; }
	inline void set_e_2(Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0 * value)
	{
		___e_2 = value;
		Il2CppCodeGenWriteBarrier((&___e_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.EventQueue/EventQueueEntry
struct EventQueueEntry_t18A087AFF39EE175D3F8CF4897B97ABC9E8720CB_marshaled_pinvoke
{
	int32_t ___type_0;
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * ___entry_1;
	Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0 * ___e_2;
};
// Native definition for COM marshalling of Spine.EventQueue/EventQueueEntry
struct EventQueueEntry_t18A087AFF39EE175D3F8CF4897B97ABC9E8720CB_marshaled_com
{
	int32_t ___type_0;
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276 * ___entry_1;
	Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0 * ___e_2;
};
#endif // EVENTQUEUEENTRY_T18A087AFF39EE175D3F8CF4897B97ABC9E8720CB_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#define NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnUp_1)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnDown_2)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnLeft_3)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnRight_4)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T761250C05C09773B75F5E0D52DDCBBFE60288A07_H
#ifndef DELEGATEMOVEEND_T1F76CEA26063F0B11A971CAB2747FA1BFA8241F7_H
#define DELEGATEMOVEEND_T1F76CEA26063F0B11A971CAB2747FA1BFA8241F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Paw/delegateMoveEnd
struct  delegateMoveEnd_t1F76CEA26063F0B11A971CAB2747FA1BFA8241F7  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEMOVEEND_T1F76CEA26063F0B11A971CAB2747FA1BFA8241F7_H
#ifndef ONCLICKEVENTHANDLER_T0FB1C8529204CFF772DD0008958C94A36A9ECB86_H
#define ONCLICKEVENTHANDLER_T0FB1C8529204CFF772DD0008958C94A36A9ECB86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneUiButton/OnClickEventHandler
struct  OnClickEventHandler_t0FB1C8529204CFF772DD0008958C94A36A9ECB86  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKEVENTHANDLER_T0FB1C8529204CFF772DD0008958C94A36A9ECB86_H
#ifndef TRACKENTRYDELEGATE_TE909F250DB1ADC27B3DF4A52297A94DC79C8825F_H
#define TRACKENTRYDELEGATE_TE909F250DB1ADC27B3DF4A52297A94DC79C8825F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState/TrackEntryDelegate
struct  TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRYDELEGATE_TE909F250DB1ADC27B3DF4A52297A94DC79C8825F_H
#ifndef TRACKENTRYEVENTDELEGATE_TED21C03D5D861C22FCC6214B387C9046174FA4DE_H
#define TRACKENTRYEVENTDELEGATE_TED21C03D5D861C22FCC6214B387C9046174FA4DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState/TrackEntryEventDelegate
struct  TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRYEVENTDELEGATE_TED21C03D5D861C22FCC6214B387C9046174FA4DE_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef PLAYABLEASSET_T28B670EFE526C0D383A1C5A5AE2A150424E989AD_H
#define PLAYABLEASSET_T28B670EFE526C0D383A1C5A5AE2A150424E989AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T28B670EFE526C0D383A1C5A5AE2A150424E989AD_H
#ifndef SPINESKELETONFLIPCLIP_TD9E93640501997B27D28C96B18FBFE6E817D73F6_H
#define SPINESKELETONFLIPCLIP_TD9E93640501997B27D28C96B18FBFE6E817D73F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpineSkeletonFlipClip
struct  SpineSkeletonFlipClip_tD9E93640501997B27D28C96B18FBFE6E817D73F6  : public PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD
{
public:
	// SpineSkeletonFlipBehaviour SpineSkeletonFlipClip::template
	SpineSkeletonFlipBehaviour_t881D1AD276451B7A71EDB55CC0849B6F8E71EB5C * ___template_4;

public:
	inline static int32_t get_offset_of_template_4() { return static_cast<int32_t>(offsetof(SpineSkeletonFlipClip_tD9E93640501997B27D28C96B18FBFE6E817D73F6, ___template_4)); }
	inline SpineSkeletonFlipBehaviour_t881D1AD276451B7A71EDB55CC0849B6F8E71EB5C * get_template_4() const { return ___template_4; }
	inline SpineSkeletonFlipBehaviour_t881D1AD276451B7A71EDB55CC0849B6F8E71EB5C ** get_address_of_template_4() { return &___template_4; }
	inline void set_template_4(SpineSkeletonFlipBehaviour_t881D1AD276451B7A71EDB55CC0849B6F8E71EB5C * value)
	{
		___template_4 = value;
		Il2CppCodeGenWriteBarrier((&___template_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESKELETONFLIPCLIP_TD9E93640501997B27D28C96B18FBFE6E817D73F6_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef MONEYCOUNTER_TD573C40FE0104CF84E8397A6D579A61ADB80B5AF_H
#define MONEYCOUNTER_TD573C40FE0104CF84E8397A6D579A61ADB80B5AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoneyCounter
struct  MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image MoneyCounter::_imgIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgIcon_4;
	// UnityEngine.UI.Text MoneyCounter::_txtValue
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtValue_5;

public:
	inline static int32_t get_offset_of__imgIcon_4() { return static_cast<int32_t>(offsetof(MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF, ____imgIcon_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgIcon_4() const { return ____imgIcon_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgIcon_4() { return &____imgIcon_4; }
	inline void set__imgIcon_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&____imgIcon_4), value);
	}

	inline static int32_t get_offset_of__txtValue_5() { return static_cast<int32_t>(offsetof(MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF, ____txtValue_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtValue_5() const { return ____txtValue_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtValue_5() { return &____txtValue_5; }
	inline void set__txtValue_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtValue_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtValue_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONEYCOUNTER_TD573C40FE0104CF84E8397A6D579A61ADB80B5AF_H
#ifndef SCENEUIBUTTON_T111438FEF8993259A9F0F2221A4AD4CE08C48507_H
#define SCENEUIBUTTON_T111438FEF8993259A9F0F2221A4AD4CE08C48507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneUiButton
struct  SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// SceneUiButton/OnClickEventHandler SceneUiButton::OnClickEvent
	OnClickEventHandler_t0FB1C8529204CFF772DD0008958C94A36A9ECB86 * ___OnClickEvent_4;

public:
	inline static int32_t get_offset_of_OnClickEvent_4() { return static_cast<int32_t>(offsetof(SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507, ___OnClickEvent_4)); }
	inline OnClickEventHandler_t0FB1C8529204CFF772DD0008958C94A36A9ECB86 * get_OnClickEvent_4() const { return ___OnClickEvent_4; }
	inline OnClickEventHandler_t0FB1C8529204CFF772DD0008958C94A36A9ECB86 ** get_address_of_OnClickEvent_4() { return &___OnClickEvent_4; }
	inline void set_OnClickEvent_4(OnClickEventHandler_t0FB1C8529204CFF772DD0008958C94A36A9ECB86 * value)
	{
		___OnClickEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnClickEvent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEUIBUTTON_T111438FEF8993259A9F0F2221A4AD4CE08C48507_H
#ifndef SCENEUIPRICE_TDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2_H
#define SCENEUIPRICE_TDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneUiPrice
struct  SceneUiPrice_tDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SpriteRenderer SceneUiPrice::_srMoneyTypeIcon
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srMoneyTypeIcon_4;
	// UnityEngine.TextMesh SceneUiPrice::_tmValue
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ____tmValue_5;

public:
	inline static int32_t get_offset_of__srMoneyTypeIcon_4() { return static_cast<int32_t>(offsetof(SceneUiPrice_tDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2, ____srMoneyTypeIcon_4)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srMoneyTypeIcon_4() const { return ____srMoneyTypeIcon_4; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srMoneyTypeIcon_4() { return &____srMoneyTypeIcon_4; }
	inline void set__srMoneyTypeIcon_4(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srMoneyTypeIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&____srMoneyTypeIcon_4), value);
	}

	inline static int32_t get_offset_of__tmValue_5() { return static_cast<int32_t>(offsetof(SceneUiPrice_tDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2, ____tmValue_5)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get__tmValue_5() const { return ____tmValue_5; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of__tmValue_5() { return &____tmValue_5; }
	inline void set__tmValue_5(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		____tmValue_5 = value;
		Il2CppCodeGenWriteBarrier((&____tmValue_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEUIPRICE_TDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2_H
#ifndef SCENEUISKILLBUTTON_T9E9A95B19B33D952277A3910F275A4C2BA2F5A13_H
#define SCENEUISKILLBUTTON_T9E9A95B19B33D952277A3910F275A4C2BA2F5A13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneUiSkillButton
struct  SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh SceneUiSkillButton::_txtStatus
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ____txtStatus_4;
	// UnityEngine.TextMesh SceneUiSkillButton::_txtLeftTime
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ____txtLeftTime_5;
	// UnityEngine.TextMesh SceneUiSkillButton::_txtLevel
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ____txtLevel_6;
	// UnityEngine.TextMesh SceneUiSkillButton::_txtCurValue
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ____txtCurValue_7;
	// SkillManager/eSkillType SceneUiSkillButton::m_eType
	int32_t ___m_eType_8;
	// System.Int32 SceneUiSkillButton::m_nDuration
	int32_t ___m_nDuration_9;
	// System.Int32 SceneUiSkillButton::m_nColdDown
	int32_t ___m_nColdDown_10;
	// System.Single SceneUiSkillButton::m_fColdDownCount
	float ___m_fColdDownCount_11;
	// System.Single SceneUiSkillButton::m_DurationCount
	float ___m_DurationCount_12;
	// System.Single SceneUiSkillButton::m_fTimeElapse
	float ___m_fTimeElapse_13;
	// Skill SceneUiSkillButton::m_BoundSkill
	Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF * ___m_BoundSkill_14;

public:
	inline static int32_t get_offset_of__txtStatus_4() { return static_cast<int32_t>(offsetof(SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13, ____txtStatus_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get__txtStatus_4() const { return ____txtStatus_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of__txtStatus_4() { return &____txtStatus_4; }
	inline void set__txtStatus_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		____txtStatus_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtStatus_4), value);
	}

	inline static int32_t get_offset_of__txtLeftTime_5() { return static_cast<int32_t>(offsetof(SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13, ____txtLeftTime_5)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get__txtLeftTime_5() const { return ____txtLeftTime_5; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of__txtLeftTime_5() { return &____txtLeftTime_5; }
	inline void set__txtLeftTime_5(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		____txtLeftTime_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtLeftTime_5), value);
	}

	inline static int32_t get_offset_of__txtLevel_6() { return static_cast<int32_t>(offsetof(SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13, ____txtLevel_6)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get__txtLevel_6() const { return ____txtLevel_6; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of__txtLevel_6() { return &____txtLevel_6; }
	inline void set__txtLevel_6(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		____txtLevel_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtLevel_6), value);
	}

	inline static int32_t get_offset_of__txtCurValue_7() { return static_cast<int32_t>(offsetof(SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13, ____txtCurValue_7)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get__txtCurValue_7() const { return ____txtCurValue_7; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of__txtCurValue_7() { return &____txtCurValue_7; }
	inline void set__txtCurValue_7(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		____txtCurValue_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurValue_7), value);
	}

	inline static int32_t get_offset_of_m_eType_8() { return static_cast<int32_t>(offsetof(SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13, ___m_eType_8)); }
	inline int32_t get_m_eType_8() const { return ___m_eType_8; }
	inline int32_t* get_address_of_m_eType_8() { return &___m_eType_8; }
	inline void set_m_eType_8(int32_t value)
	{
		___m_eType_8 = value;
	}

	inline static int32_t get_offset_of_m_nDuration_9() { return static_cast<int32_t>(offsetof(SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13, ___m_nDuration_9)); }
	inline int32_t get_m_nDuration_9() const { return ___m_nDuration_9; }
	inline int32_t* get_address_of_m_nDuration_9() { return &___m_nDuration_9; }
	inline void set_m_nDuration_9(int32_t value)
	{
		___m_nDuration_9 = value;
	}

	inline static int32_t get_offset_of_m_nColdDown_10() { return static_cast<int32_t>(offsetof(SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13, ___m_nColdDown_10)); }
	inline int32_t get_m_nColdDown_10() const { return ___m_nColdDown_10; }
	inline int32_t* get_address_of_m_nColdDown_10() { return &___m_nColdDown_10; }
	inline void set_m_nColdDown_10(int32_t value)
	{
		___m_nColdDown_10 = value;
	}

	inline static int32_t get_offset_of_m_fColdDownCount_11() { return static_cast<int32_t>(offsetof(SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13, ___m_fColdDownCount_11)); }
	inline float get_m_fColdDownCount_11() const { return ___m_fColdDownCount_11; }
	inline float* get_address_of_m_fColdDownCount_11() { return &___m_fColdDownCount_11; }
	inline void set_m_fColdDownCount_11(float value)
	{
		___m_fColdDownCount_11 = value;
	}

	inline static int32_t get_offset_of_m_DurationCount_12() { return static_cast<int32_t>(offsetof(SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13, ___m_DurationCount_12)); }
	inline float get_m_DurationCount_12() const { return ___m_DurationCount_12; }
	inline float* get_address_of_m_DurationCount_12() { return &___m_DurationCount_12; }
	inline void set_m_DurationCount_12(float value)
	{
		___m_DurationCount_12 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_13() { return static_cast<int32_t>(offsetof(SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13, ___m_fTimeElapse_13)); }
	inline float get_m_fTimeElapse_13() const { return ___m_fTimeElapse_13; }
	inline float* get_address_of_m_fTimeElapse_13() { return &___m_fTimeElapse_13; }
	inline void set_m_fTimeElapse_13(float value)
	{
		___m_fTimeElapse_13 = value;
	}

	inline static int32_t get_offset_of_m_BoundSkill_14() { return static_cast<int32_t>(offsetof(SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13, ___m_BoundSkill_14)); }
	inline Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF * get_m_BoundSkill_14() const { return ___m_BoundSkill_14; }
	inline Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF ** get_address_of_m_BoundSkill_14() { return &___m_BoundSkill_14; }
	inline void set_m_BoundSkill_14(Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF * value)
	{
		___m_BoundSkill_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundSkill_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEUISKILLBUTTON_T9E9A95B19B33D952277A3910F275A4C2BA2F5A13_H
#ifndef SHOPPINMALLTYPECONTAINER_T47FF2B55688E7E26C694A9F2B02FE76CE9E37555_H
#define SHOPPINMALLTYPECONTAINER_T47FF2B55688E7E26C694A9F2B02FE76CE9E37555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShoppinMallTypeContainer
struct  ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject ShoppinMallTypeContainer::m_goItemsContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_goItemsContainer_6;
	// UnityEngine.Vector2 ShoppinMallTypeContainer::m_vecStartPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecStartPos_7;
	// System.Int32 ShoppinMallTypeContainer::m_nCurNum
	int32_t ___m_nCurNum_8;
	// System.Int32 ShoppinMallTypeContainer::m_nRow
	int32_t ___m_nRow_9;
	// System.Collections.Generic.List`1<UIShoppinAndItemCounter> ShoppinMallTypeContainer::m_lstItems
	List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * ___m_lstItems_10;
	// UnityEngine.UI.Text ShoppinMallTypeContainer::_txtTypeName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTypeName_11;

public:
	inline static int32_t get_offset_of_m_goItemsContainer_6() { return static_cast<int32_t>(offsetof(ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555, ___m_goItemsContainer_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_goItemsContainer_6() const { return ___m_goItemsContainer_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_goItemsContainer_6() { return &___m_goItemsContainer_6; }
	inline void set_m_goItemsContainer_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_goItemsContainer_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_goItemsContainer_6), value);
	}

	inline static int32_t get_offset_of_m_vecStartPos_7() { return static_cast<int32_t>(offsetof(ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555, ___m_vecStartPos_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecStartPos_7() const { return ___m_vecStartPos_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecStartPos_7() { return &___m_vecStartPos_7; }
	inline void set_m_vecStartPos_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecStartPos_7 = value;
	}

	inline static int32_t get_offset_of_m_nCurNum_8() { return static_cast<int32_t>(offsetof(ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555, ___m_nCurNum_8)); }
	inline int32_t get_m_nCurNum_8() const { return ___m_nCurNum_8; }
	inline int32_t* get_address_of_m_nCurNum_8() { return &___m_nCurNum_8; }
	inline void set_m_nCurNum_8(int32_t value)
	{
		___m_nCurNum_8 = value;
	}

	inline static int32_t get_offset_of_m_nRow_9() { return static_cast<int32_t>(offsetof(ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555, ___m_nRow_9)); }
	inline int32_t get_m_nRow_9() const { return ___m_nRow_9; }
	inline int32_t* get_address_of_m_nRow_9() { return &___m_nRow_9; }
	inline void set_m_nRow_9(int32_t value)
	{
		___m_nRow_9 = value;
	}

	inline static int32_t get_offset_of_m_lstItems_10() { return static_cast<int32_t>(offsetof(ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555, ___m_lstItems_10)); }
	inline List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * get_m_lstItems_10() const { return ___m_lstItems_10; }
	inline List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B ** get_address_of_m_lstItems_10() { return &___m_lstItems_10; }
	inline void set_m_lstItems_10(List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * value)
	{
		___m_lstItems_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstItems_10), value);
	}

	inline static int32_t get_offset_of__txtTypeName_11() { return static_cast<int32_t>(offsetof(ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555, ____txtTypeName_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTypeName_11() const { return ____txtTypeName_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTypeName_11() { return &____txtTypeName_11; }
	inline void set__txtTypeName_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTypeName_11 = value;
		Il2CppCodeGenWriteBarrier((&____txtTypeName_11), value);
	}
};

struct ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555_StaticFields
{
public:
	// UnityEngine.Vector3 ShoppinMallTypeContainer::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_4;
	// UnityEngine.Vector3 ShoppinMallTypeContainer::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_5;

public:
	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555_StaticFields, ___vecTempPos_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555_StaticFields, ___vecTempScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOPPINMALLTYPECONTAINER_T47FF2B55688E7E26C694A9F2B02FE76CE9E37555_H
#ifndef SNOWFLAKE_T5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_H
#define SNOWFLAKE_T5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SnowFlake
struct  SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SpriteRenderer SnowFlake::_srMain
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srMain_7;
	// System.Single SnowFlake::m_fDropEndPosY
	float ___m_fDropEndPosY_8;
	// System.Single SnowFlake::m_fAlphaChangePerFrame
	float ___m_fAlphaChangePerFrame_9;
	// System.Single SnowFlake::m_fBeginFadePosY
	float ___m_fBeginFadePosY_10;
	// System.Single SnowFlake::m_fCurAngle
	float ___m_fCurAngle_11;
	// System.Single SnowFlake::m_fStartPosX
	float ___m_fStartPosX_12;
	// System.Single SnowFlake::m_fStartPosY
	float ___m_fStartPosY_13;

public:
	inline static int32_t get_offset_of__srMain_7() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ____srMain_7)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srMain_7() const { return ____srMain_7; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srMain_7() { return &____srMain_7; }
	inline void set__srMain_7(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srMain_7 = value;
		Il2CppCodeGenWriteBarrier((&____srMain_7), value);
	}

	inline static int32_t get_offset_of_m_fDropEndPosY_8() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ___m_fDropEndPosY_8)); }
	inline float get_m_fDropEndPosY_8() const { return ___m_fDropEndPosY_8; }
	inline float* get_address_of_m_fDropEndPosY_8() { return &___m_fDropEndPosY_8; }
	inline void set_m_fDropEndPosY_8(float value)
	{
		___m_fDropEndPosY_8 = value;
	}

	inline static int32_t get_offset_of_m_fAlphaChangePerFrame_9() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ___m_fAlphaChangePerFrame_9)); }
	inline float get_m_fAlphaChangePerFrame_9() const { return ___m_fAlphaChangePerFrame_9; }
	inline float* get_address_of_m_fAlphaChangePerFrame_9() { return &___m_fAlphaChangePerFrame_9; }
	inline void set_m_fAlphaChangePerFrame_9(float value)
	{
		___m_fAlphaChangePerFrame_9 = value;
	}

	inline static int32_t get_offset_of_m_fBeginFadePosY_10() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ___m_fBeginFadePosY_10)); }
	inline float get_m_fBeginFadePosY_10() const { return ___m_fBeginFadePosY_10; }
	inline float* get_address_of_m_fBeginFadePosY_10() { return &___m_fBeginFadePosY_10; }
	inline void set_m_fBeginFadePosY_10(float value)
	{
		___m_fBeginFadePosY_10 = value;
	}

	inline static int32_t get_offset_of_m_fCurAngle_11() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ___m_fCurAngle_11)); }
	inline float get_m_fCurAngle_11() const { return ___m_fCurAngle_11; }
	inline float* get_address_of_m_fCurAngle_11() { return &___m_fCurAngle_11; }
	inline void set_m_fCurAngle_11(float value)
	{
		___m_fCurAngle_11 = value;
	}

	inline static int32_t get_offset_of_m_fStartPosX_12() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ___m_fStartPosX_12)); }
	inline float get_m_fStartPosX_12() const { return ___m_fStartPosX_12; }
	inline float* get_address_of_m_fStartPosX_12() { return &___m_fStartPosX_12; }
	inline void set_m_fStartPosX_12(float value)
	{
		___m_fStartPosX_12 = value;
	}

	inline static int32_t get_offset_of_m_fStartPosY_13() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70, ___m_fStartPosY_13)); }
	inline float get_m_fStartPosY_13() const { return ___m_fStartPosY_13; }
	inline float* get_address_of_m_fStartPosY_13() { return &___m_fStartPosY_13; }
	inline void set_m_fStartPosY_13(float value)
	{
		___m_fStartPosY_13 = value;
	}
};

struct SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_StaticFields
{
public:
	// UnityEngine.Vector3 SnowFlake::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_4;
	// UnityEngine.Vector3 SnowFlake::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_5;
	// UnityEngine.Color SnowFlake::colorTemp
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___colorTemp_6;

public:
	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_StaticFields, ___vecTempPos_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_StaticFields, ___vecTempScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_5 = value;
	}

	inline static int32_t get_offset_of_colorTemp_6() { return static_cast<int32_t>(offsetof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_StaticFields, ___colorTemp_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_colorTemp_6() const { return ___colorTemp_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_colorTemp_6() { return &___colorTemp_6; }
	inline void set_colorTemp_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___colorTemp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SNOWFLAKE_T5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_H
#ifndef UIADMINISTRATORCOUNTER_TB0779CA5FD21487575278D829A0AEC52F698DA78_H
#define UIADMINISTRATORCOUNTER_TB0779CA5FD21487575278D829A0AEC52F698DA78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAdministratorCounter
struct  UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text UIAdministratorCounter::_txtName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtName_4;
	// UnityEngine.UI.Text UIAdministratorCounter::_txtQuality
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtQuality_5;
	// UnityEngine.UI.Text UIAdministratorCounter::_txtDuration
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDuration_6;
	// UnityEngine.UI.Text UIAdministratorCounter::_txtDesc
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDesc_7;
	// UnityEngine.UI.Button UIAdministratorCounter::_btnUseOrCancel
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnUseOrCancel_8;
	// UnityEngine.UI.Button UIAdministratorCounter::_btnSell
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnSell_9;
	// UnityEngine.UI.Button UIAdministratorCounter::_btnCancel
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnCancel_10;
	// UnityEngine.UI.Image UIAdministratorCounter::_imgAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgAvatar_11;
	// UnityEngine.UI.Image UIAdministratorCounter::_imgBg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgBg_12;
	// UnityEngine.UI.Image UIAdministratorCounter::_imgSkillIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgSkillIcon_13;
	// UnityEngine.UI.Text UIAdministratorCounter::_txtOperateTitle
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtOperateTitle_14;
	// UIProgressBar UIAdministratorCounter::_progressbarColdDown
	UIProgressBar_t2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96 * ____progressbarColdDown_15;
	// AdministratorManager/sAdminConfig UIAdministratorCounter::m_Config
	sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827  ___m_Config_16;
	// Admin UIAdministratorCounter::m_BoundAdmin
	Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60 * ___m_BoundAdmin_17;
	// System.Boolean UIAdministratorCounter::m_bUsing
	bool ___m_bUsing_18;
	// UnityEngine.Sprite UIAdministratorCounter::m_sprSmallAvatar
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprSmallAvatar_19;

public:
	inline static int32_t get_offset_of__txtName_4() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ____txtName_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtName_4() const { return ____txtName_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtName_4() { return &____txtName_4; }
	inline void set__txtName_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtName_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtName_4), value);
	}

	inline static int32_t get_offset_of__txtQuality_5() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ____txtQuality_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtQuality_5() const { return ____txtQuality_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtQuality_5() { return &____txtQuality_5; }
	inline void set__txtQuality_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtQuality_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtQuality_5), value);
	}

	inline static int32_t get_offset_of__txtDuration_6() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ____txtDuration_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDuration_6() const { return ____txtDuration_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDuration_6() { return &____txtDuration_6; }
	inline void set__txtDuration_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDuration_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtDuration_6), value);
	}

	inline static int32_t get_offset_of__txtDesc_7() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ____txtDesc_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDesc_7() const { return ____txtDesc_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDesc_7() { return &____txtDesc_7; }
	inline void set__txtDesc_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDesc_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtDesc_7), value);
	}

	inline static int32_t get_offset_of__btnUseOrCancel_8() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ____btnUseOrCancel_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnUseOrCancel_8() const { return ____btnUseOrCancel_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnUseOrCancel_8() { return &____btnUseOrCancel_8; }
	inline void set__btnUseOrCancel_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnUseOrCancel_8 = value;
		Il2CppCodeGenWriteBarrier((&____btnUseOrCancel_8), value);
	}

	inline static int32_t get_offset_of__btnSell_9() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ____btnSell_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnSell_9() const { return ____btnSell_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnSell_9() { return &____btnSell_9; }
	inline void set__btnSell_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnSell_9 = value;
		Il2CppCodeGenWriteBarrier((&____btnSell_9), value);
	}

	inline static int32_t get_offset_of__btnCancel_10() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ____btnCancel_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnCancel_10() const { return ____btnCancel_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnCancel_10() { return &____btnCancel_10; }
	inline void set__btnCancel_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnCancel_10 = value;
		Il2CppCodeGenWriteBarrier((&____btnCancel_10), value);
	}

	inline static int32_t get_offset_of__imgAvatar_11() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ____imgAvatar_11)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgAvatar_11() const { return ____imgAvatar_11; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgAvatar_11() { return &____imgAvatar_11; }
	inline void set__imgAvatar_11(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgAvatar_11 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatar_11), value);
	}

	inline static int32_t get_offset_of__imgBg_12() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ____imgBg_12)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgBg_12() const { return ____imgBg_12; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgBg_12() { return &____imgBg_12; }
	inline void set__imgBg_12(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgBg_12 = value;
		Il2CppCodeGenWriteBarrier((&____imgBg_12), value);
	}

	inline static int32_t get_offset_of__imgSkillIcon_13() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ____imgSkillIcon_13)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgSkillIcon_13() const { return ____imgSkillIcon_13; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgSkillIcon_13() { return &____imgSkillIcon_13; }
	inline void set__imgSkillIcon_13(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgSkillIcon_13 = value;
		Il2CppCodeGenWriteBarrier((&____imgSkillIcon_13), value);
	}

	inline static int32_t get_offset_of__txtOperateTitle_14() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ____txtOperateTitle_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtOperateTitle_14() const { return ____txtOperateTitle_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtOperateTitle_14() { return &____txtOperateTitle_14; }
	inline void set__txtOperateTitle_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtOperateTitle_14 = value;
		Il2CppCodeGenWriteBarrier((&____txtOperateTitle_14), value);
	}

	inline static int32_t get_offset_of__progressbarColdDown_15() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ____progressbarColdDown_15)); }
	inline UIProgressBar_t2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96 * get__progressbarColdDown_15() const { return ____progressbarColdDown_15; }
	inline UIProgressBar_t2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96 ** get_address_of__progressbarColdDown_15() { return &____progressbarColdDown_15; }
	inline void set__progressbarColdDown_15(UIProgressBar_t2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96 * value)
	{
		____progressbarColdDown_15 = value;
		Il2CppCodeGenWriteBarrier((&____progressbarColdDown_15), value);
	}

	inline static int32_t get_offset_of_m_Config_16() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ___m_Config_16)); }
	inline sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827  get_m_Config_16() const { return ___m_Config_16; }
	inline sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827 * get_address_of_m_Config_16() { return &___m_Config_16; }
	inline void set_m_Config_16(sAdminConfig_tCD81360114F98AE183656DD00005E18A400A4827  value)
	{
		___m_Config_16 = value;
	}

	inline static int32_t get_offset_of_m_BoundAdmin_17() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ___m_BoundAdmin_17)); }
	inline Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60 * get_m_BoundAdmin_17() const { return ___m_BoundAdmin_17; }
	inline Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60 ** get_address_of_m_BoundAdmin_17() { return &___m_BoundAdmin_17; }
	inline void set_m_BoundAdmin_17(Admin_t32563B36AF9D92E819A9499DA78CEA80663DDF60 * value)
	{
		___m_BoundAdmin_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundAdmin_17), value);
	}

	inline static int32_t get_offset_of_m_bUsing_18() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ___m_bUsing_18)); }
	inline bool get_m_bUsing_18() const { return ___m_bUsing_18; }
	inline bool* get_address_of_m_bUsing_18() { return &___m_bUsing_18; }
	inline void set_m_bUsing_18(bool value)
	{
		___m_bUsing_18 = value;
	}

	inline static int32_t get_offset_of_m_sprSmallAvatar_19() { return static_cast<int32_t>(offsetof(UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78, ___m_sprSmallAvatar_19)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprSmallAvatar_19() const { return ___m_sprSmallAvatar_19; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprSmallAvatar_19() { return &___m_sprSmallAvatar_19; }
	inline void set_m_sprSmallAvatar_19(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprSmallAvatar_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprSmallAvatar_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIADMINISTRATORCOUNTER_TB0779CA5FD21487575278D829A0AEC52F698DA78_H
#ifndef UIADVENTURECOUNTER_TE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C_H
#define UIADVENTURECOUNTER_TE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAdventureCounter
struct  UIAdventureCounter_tE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UIAdventureCounter::m_nDuration
	int32_t ___m_nDuration_4;
	// UIAdventureProfitCounter[] UIAdventureCounter::m_aryProfitList
	UIAdventureProfitCounterU5BU5D_tD2A07412D3D7D4B0C324E1F6A451DF229A11CABA* ___m_aryProfitList_5;

public:
	inline static int32_t get_offset_of_m_nDuration_4() { return static_cast<int32_t>(offsetof(UIAdventureCounter_tE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C, ___m_nDuration_4)); }
	inline int32_t get_m_nDuration_4() const { return ___m_nDuration_4; }
	inline int32_t* get_address_of_m_nDuration_4() { return &___m_nDuration_4; }
	inline void set_m_nDuration_4(int32_t value)
	{
		___m_nDuration_4 = value;
	}

	inline static int32_t get_offset_of_m_aryProfitList_5() { return static_cast<int32_t>(offsetof(UIAdventureCounter_tE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C, ___m_aryProfitList_5)); }
	inline UIAdventureProfitCounterU5BU5D_tD2A07412D3D7D4B0C324E1F6A451DF229A11CABA* get_m_aryProfitList_5() const { return ___m_aryProfitList_5; }
	inline UIAdventureProfitCounterU5BU5D_tD2A07412D3D7D4B0C324E1F6A451DF229A11CABA** get_address_of_m_aryProfitList_5() { return &___m_aryProfitList_5; }
	inline void set_m_aryProfitList_5(UIAdventureProfitCounterU5BU5D_tD2A07412D3D7D4B0C324E1F6A451DF229A11CABA* value)
	{
		___m_aryProfitList_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryProfitList_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIADVENTURECOUNTER_TE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C_H
#ifndef UIADVENTUREPROFITCOUNTER_T6EDEEB336BA9A03676339D2FA1C2F30612613B8D_H
#define UIADVENTUREPROFITCOUNTER_T6EDEEB336BA9A03676339D2FA1C2F30612613B8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAdventureProfitCounter
struct  UIAdventureProfitCounter_t6EDEEB336BA9A03676339D2FA1C2F30612613B8D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// AdventureManager/eAdventureProfitType UIAdventureProfitCounter::m_eProfitType
	int32_t ___m_eProfitType_4;
	// System.Int32 UIAdventureProfitCounter::m_nItemId
	int32_t ___m_nItemId_5;
	// System.Int32 UIAdventureProfitCounter::m_nValue0
	int32_t ___m_nValue0_6;
	// System.String UIAdventureProfitCounter::m_szName
	String_t* ___m_szName_7;

public:
	inline static int32_t get_offset_of_m_eProfitType_4() { return static_cast<int32_t>(offsetof(UIAdventureProfitCounter_t6EDEEB336BA9A03676339D2FA1C2F30612613B8D, ___m_eProfitType_4)); }
	inline int32_t get_m_eProfitType_4() const { return ___m_eProfitType_4; }
	inline int32_t* get_address_of_m_eProfitType_4() { return &___m_eProfitType_4; }
	inline void set_m_eProfitType_4(int32_t value)
	{
		___m_eProfitType_4 = value;
	}

	inline static int32_t get_offset_of_m_nItemId_5() { return static_cast<int32_t>(offsetof(UIAdventureProfitCounter_t6EDEEB336BA9A03676339D2FA1C2F30612613B8D, ___m_nItemId_5)); }
	inline int32_t get_m_nItemId_5() const { return ___m_nItemId_5; }
	inline int32_t* get_address_of_m_nItemId_5() { return &___m_nItemId_5; }
	inline void set_m_nItemId_5(int32_t value)
	{
		___m_nItemId_5 = value;
	}

	inline static int32_t get_offset_of_m_nValue0_6() { return static_cast<int32_t>(offsetof(UIAdventureProfitCounter_t6EDEEB336BA9A03676339D2FA1C2F30612613B8D, ___m_nValue0_6)); }
	inline int32_t get_m_nValue0_6() const { return ___m_nValue0_6; }
	inline int32_t* get_address_of_m_nValue0_6() { return &___m_nValue0_6; }
	inline void set_m_nValue0_6(int32_t value)
	{
		___m_nValue0_6 = value;
	}

	inline static int32_t get_offset_of_m_szName_7() { return static_cast<int32_t>(offsetof(UIAdventureProfitCounter_t6EDEEB336BA9A03676339D2FA1C2F30612613B8D, ___m_szName_7)); }
	inline String_t* get_m_szName_7() const { return ___m_szName_7; }
	inline String_t** get_address_of_m_szName_7() { return &___m_szName_7; }
	inline void set_m_szName_7(String_t* value)
	{
		___m_szName_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_szName_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIADVENTUREPROFITCOUNTER_T6EDEEB336BA9A03676339D2FA1C2F30612613B8D_H
#ifndef UIADVENTUREREWARDSHOW_TC98CDF0A1478D1585512C351DF8B6106FCB4C93F_H
#define UIADVENTUREREWARDSHOW_TC98CDF0A1478D1585512C351DF8B6106FCB4C93F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAdventureRewardShow
struct  UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text UIAdventureRewardShow::_txtName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtName_4;
	// UnityEngine.UI.Text UIAdventureRewardShow::_txtNum
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtNum_5;
	// UnityEngine.UI.Image UIAdventureRewardShow::_imgAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgAvatar_6;

public:
	inline static int32_t get_offset_of__txtName_4() { return static_cast<int32_t>(offsetof(UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F, ____txtName_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtName_4() const { return ____txtName_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtName_4() { return &____txtName_4; }
	inline void set__txtName_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtName_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtName_4), value);
	}

	inline static int32_t get_offset_of__txtNum_5() { return static_cast<int32_t>(offsetof(UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F, ____txtNum_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtNum_5() const { return ____txtNum_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtNum_5() { return &____txtNum_5; }
	inline void set__txtNum_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtNum_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtNum_5), value);
	}

	inline static int32_t get_offset_of__imgAvatar_6() { return static_cast<int32_t>(offsetof(UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F, ____imgAvatar_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgAvatar_6() const { return ____imgAvatar_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgAvatar_6() { return &____imgAvatar_6; }
	inline void set__imgAvatar_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgAvatar_6 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatar_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIADVENTUREREWARDSHOW_TC98CDF0A1478D1585512C351DF8B6106FCB4C93F_H
#ifndef UIBIGMAPTRACKINFO_TD1567A742038A1B490AA47873BC98DF75421C69E_H
#define UIBIGMAPTRACKINFO_TD1567A742038A1B490AA47873BC98DF75421C69E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBigMapTrackInfo
struct  UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject UIBigMapTrackInfo::_containerEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerEffect_4;
	// UnityEngine.GameObject UIBigMapTrackInfo::_goBlock
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goBlock_5;
	// UnityEngine.UI.Image UIBigMapTrackInfo::_imgBlock_bg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgBlock_bg_6;
	// UnityEngine.UI.Image UIBigMapTrackInfo::_imgBlock_Main
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgBlock_Main_7;
	// UnityEngine.GameObject UIBigMapTrackInfo::_containerMainContent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerMainContent_8;
	// UnityEngine.UI.Image UIBigMapTrackInfo::_imgBlock
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgBlock_9;
	// UIBlock UIBigMapTrackInfo::_block
	UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B * ____block_10;
	// UnityEngine.GameObject UIBigMapTrackInfo::_Content
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____Content_11;
	// UnityEngine.UI.Image UIBigMapTrackInfo::_imgQuestionMark
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgQuestionMark_12;
	// UnityEngine.GameObject UIBigMapTrackInfo::_containerUnlockPrice
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerUnlockPrice_13;
	// UnityEngine.GameObject UIBigMapTrackInfo::_containerUnLockPrice
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerUnLockPrice_14;
	// UnityEngine.GameObject UIBigMapTrackInfo::_containerProfitAndTime
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerProfitAndTime_15;
	// UnityEngine.GameObject UIBigMapTrackInfo::_txtIsCurDistrict
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____txtIsCurDistrict_16;
	// UnityEngine.GameObject UIBigMapTrackInfo::_subcontainerProfitAndTime
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____subcontainerProfitAndTime_17;
	// UIStars UIBigMapTrackInfo::_starsPrestige
	UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * ____starsPrestige_18;
	// UnityEngine.UI.Text UIBigMapTrackInfo::_txtTrackName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTrackName_19;
	// UnityEngine.UI.Text UIBigMapTrackInfo::_txtCurOfflineGain
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurOfflineGain_20;
	// UnityEngine.UI.Text UIBigMapTrackInfo::_txtAdsProfitLeftTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtAdsProfitLeftTime_21;
	// UnityEngine.UI.Button UIBigMapTrackInfo::_imgLock
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____imgLock_22;
	// UnityEngine.UI.Text UIBigMapTrackInfo::_txtUnlockPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtUnlockPrice_23;
	// UnityEngine.UI.Image UIBigMapTrackInfo::_imgUnlockCoinType
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgUnlockCoinType_24;
	// UnityEngine.UI.Image UIBigMapTrackInfo::_imgProfitCoinType
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgProfitCoinType_25;
	// UnityEngine.UI.Image UIBigMapTrackInfo::_imgDataBg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgDataBg_26;
	// UnityEngine.UI.Button UIBigMapTrackInfo::_btnEnter
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnEnter_27;
	// District UIBigMapTrackInfo::m_District
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * ___m_District_28;
	// MapManager/eDistrictStatus UIBigMapTrackInfo::m_eStatus
	int32_t ___m_eStatus_29;
	// System.Boolean UIBigMapTrackInfo::m_bCanUnlock
	bool ___m_bCanUnlock_30;
	// System.Boolean UIBigMapTrackInfo::m_bLocked
	bool ___m_bLocked_31;
	// System.Single UIBigMapTrackInfo::m_fBtnEnterShowTime
	float ___m_fBtnEnterShowTime_32;
	// System.Boolean UIBigMapTrackInfo::m_bClickMe
	bool ___m_bClickMe_33;

public:
	inline static int32_t get_offset_of__containerEffect_4() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____containerEffect_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerEffect_4() const { return ____containerEffect_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerEffect_4() { return &____containerEffect_4; }
	inline void set__containerEffect_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerEffect_4 = value;
		Il2CppCodeGenWriteBarrier((&____containerEffect_4), value);
	}

	inline static int32_t get_offset_of__goBlock_5() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____goBlock_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goBlock_5() const { return ____goBlock_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goBlock_5() { return &____goBlock_5; }
	inline void set__goBlock_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goBlock_5 = value;
		Il2CppCodeGenWriteBarrier((&____goBlock_5), value);
	}

	inline static int32_t get_offset_of__imgBlock_bg_6() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____imgBlock_bg_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgBlock_bg_6() const { return ____imgBlock_bg_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgBlock_bg_6() { return &____imgBlock_bg_6; }
	inline void set__imgBlock_bg_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgBlock_bg_6 = value;
		Il2CppCodeGenWriteBarrier((&____imgBlock_bg_6), value);
	}

	inline static int32_t get_offset_of__imgBlock_Main_7() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____imgBlock_Main_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgBlock_Main_7() const { return ____imgBlock_Main_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgBlock_Main_7() { return &____imgBlock_Main_7; }
	inline void set__imgBlock_Main_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgBlock_Main_7 = value;
		Il2CppCodeGenWriteBarrier((&____imgBlock_Main_7), value);
	}

	inline static int32_t get_offset_of__containerMainContent_8() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____containerMainContent_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerMainContent_8() const { return ____containerMainContent_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerMainContent_8() { return &____containerMainContent_8; }
	inline void set__containerMainContent_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerMainContent_8 = value;
		Il2CppCodeGenWriteBarrier((&____containerMainContent_8), value);
	}

	inline static int32_t get_offset_of__imgBlock_9() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____imgBlock_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgBlock_9() const { return ____imgBlock_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgBlock_9() { return &____imgBlock_9; }
	inline void set__imgBlock_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgBlock_9 = value;
		Il2CppCodeGenWriteBarrier((&____imgBlock_9), value);
	}

	inline static int32_t get_offset_of__block_10() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____block_10)); }
	inline UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B * get__block_10() const { return ____block_10; }
	inline UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B ** get_address_of__block_10() { return &____block_10; }
	inline void set__block_10(UIBlock_t34D109BECBD5E8B392500CB078B388ADB954326B * value)
	{
		____block_10 = value;
		Il2CppCodeGenWriteBarrier((&____block_10), value);
	}

	inline static int32_t get_offset_of__Content_11() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____Content_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__Content_11() const { return ____Content_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__Content_11() { return &____Content_11; }
	inline void set__Content_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____Content_11 = value;
		Il2CppCodeGenWriteBarrier((&____Content_11), value);
	}

	inline static int32_t get_offset_of__imgQuestionMark_12() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____imgQuestionMark_12)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgQuestionMark_12() const { return ____imgQuestionMark_12; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgQuestionMark_12() { return &____imgQuestionMark_12; }
	inline void set__imgQuestionMark_12(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgQuestionMark_12 = value;
		Il2CppCodeGenWriteBarrier((&____imgQuestionMark_12), value);
	}

	inline static int32_t get_offset_of__containerUnlockPrice_13() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____containerUnlockPrice_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerUnlockPrice_13() const { return ____containerUnlockPrice_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerUnlockPrice_13() { return &____containerUnlockPrice_13; }
	inline void set__containerUnlockPrice_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerUnlockPrice_13 = value;
		Il2CppCodeGenWriteBarrier((&____containerUnlockPrice_13), value);
	}

	inline static int32_t get_offset_of__containerUnLockPrice_14() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____containerUnLockPrice_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerUnLockPrice_14() const { return ____containerUnLockPrice_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerUnLockPrice_14() { return &____containerUnLockPrice_14; }
	inline void set__containerUnLockPrice_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerUnLockPrice_14 = value;
		Il2CppCodeGenWriteBarrier((&____containerUnLockPrice_14), value);
	}

	inline static int32_t get_offset_of__containerProfitAndTime_15() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____containerProfitAndTime_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerProfitAndTime_15() const { return ____containerProfitAndTime_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerProfitAndTime_15() { return &____containerProfitAndTime_15; }
	inline void set__containerProfitAndTime_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerProfitAndTime_15 = value;
		Il2CppCodeGenWriteBarrier((&____containerProfitAndTime_15), value);
	}

	inline static int32_t get_offset_of__txtIsCurDistrict_16() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____txtIsCurDistrict_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__txtIsCurDistrict_16() const { return ____txtIsCurDistrict_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__txtIsCurDistrict_16() { return &____txtIsCurDistrict_16; }
	inline void set__txtIsCurDistrict_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____txtIsCurDistrict_16 = value;
		Il2CppCodeGenWriteBarrier((&____txtIsCurDistrict_16), value);
	}

	inline static int32_t get_offset_of__subcontainerProfitAndTime_17() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____subcontainerProfitAndTime_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__subcontainerProfitAndTime_17() const { return ____subcontainerProfitAndTime_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__subcontainerProfitAndTime_17() { return &____subcontainerProfitAndTime_17; }
	inline void set__subcontainerProfitAndTime_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____subcontainerProfitAndTime_17 = value;
		Il2CppCodeGenWriteBarrier((&____subcontainerProfitAndTime_17), value);
	}

	inline static int32_t get_offset_of__starsPrestige_18() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____starsPrestige_18)); }
	inline UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * get__starsPrestige_18() const { return ____starsPrestige_18; }
	inline UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B ** get_address_of__starsPrestige_18() { return &____starsPrestige_18; }
	inline void set__starsPrestige_18(UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * value)
	{
		____starsPrestige_18 = value;
		Il2CppCodeGenWriteBarrier((&____starsPrestige_18), value);
	}

	inline static int32_t get_offset_of__txtTrackName_19() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____txtTrackName_19)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTrackName_19() const { return ____txtTrackName_19; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTrackName_19() { return &____txtTrackName_19; }
	inline void set__txtTrackName_19(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTrackName_19 = value;
		Il2CppCodeGenWriteBarrier((&____txtTrackName_19), value);
	}

	inline static int32_t get_offset_of__txtCurOfflineGain_20() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____txtCurOfflineGain_20)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurOfflineGain_20() const { return ____txtCurOfflineGain_20; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurOfflineGain_20() { return &____txtCurOfflineGain_20; }
	inline void set__txtCurOfflineGain_20(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurOfflineGain_20 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurOfflineGain_20), value);
	}

	inline static int32_t get_offset_of__txtAdsProfitLeftTime_21() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____txtAdsProfitLeftTime_21)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtAdsProfitLeftTime_21() const { return ____txtAdsProfitLeftTime_21; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtAdsProfitLeftTime_21() { return &____txtAdsProfitLeftTime_21; }
	inline void set__txtAdsProfitLeftTime_21(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtAdsProfitLeftTime_21 = value;
		Il2CppCodeGenWriteBarrier((&____txtAdsProfitLeftTime_21), value);
	}

	inline static int32_t get_offset_of__imgLock_22() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____imgLock_22)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__imgLock_22() const { return ____imgLock_22; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__imgLock_22() { return &____imgLock_22; }
	inline void set__imgLock_22(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____imgLock_22 = value;
		Il2CppCodeGenWriteBarrier((&____imgLock_22), value);
	}

	inline static int32_t get_offset_of__txtUnlockPrice_23() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____txtUnlockPrice_23)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtUnlockPrice_23() const { return ____txtUnlockPrice_23; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtUnlockPrice_23() { return &____txtUnlockPrice_23; }
	inline void set__txtUnlockPrice_23(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtUnlockPrice_23 = value;
		Il2CppCodeGenWriteBarrier((&____txtUnlockPrice_23), value);
	}

	inline static int32_t get_offset_of__imgUnlockCoinType_24() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____imgUnlockCoinType_24)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgUnlockCoinType_24() const { return ____imgUnlockCoinType_24; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgUnlockCoinType_24() { return &____imgUnlockCoinType_24; }
	inline void set__imgUnlockCoinType_24(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgUnlockCoinType_24 = value;
		Il2CppCodeGenWriteBarrier((&____imgUnlockCoinType_24), value);
	}

	inline static int32_t get_offset_of__imgProfitCoinType_25() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____imgProfitCoinType_25)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgProfitCoinType_25() const { return ____imgProfitCoinType_25; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgProfitCoinType_25() { return &____imgProfitCoinType_25; }
	inline void set__imgProfitCoinType_25(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgProfitCoinType_25 = value;
		Il2CppCodeGenWriteBarrier((&____imgProfitCoinType_25), value);
	}

	inline static int32_t get_offset_of__imgDataBg_26() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____imgDataBg_26)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgDataBg_26() const { return ____imgDataBg_26; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgDataBg_26() { return &____imgDataBg_26; }
	inline void set__imgDataBg_26(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgDataBg_26 = value;
		Il2CppCodeGenWriteBarrier((&____imgDataBg_26), value);
	}

	inline static int32_t get_offset_of__btnEnter_27() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ____btnEnter_27)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnEnter_27() const { return ____btnEnter_27; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnEnter_27() { return &____btnEnter_27; }
	inline void set__btnEnter_27(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnEnter_27 = value;
		Il2CppCodeGenWriteBarrier((&____btnEnter_27), value);
	}

	inline static int32_t get_offset_of_m_District_28() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ___m_District_28)); }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * get_m_District_28() const { return ___m_District_28; }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A ** get_address_of_m_District_28() { return &___m_District_28; }
	inline void set_m_District_28(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * value)
	{
		___m_District_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_District_28), value);
	}

	inline static int32_t get_offset_of_m_eStatus_29() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ___m_eStatus_29)); }
	inline int32_t get_m_eStatus_29() const { return ___m_eStatus_29; }
	inline int32_t* get_address_of_m_eStatus_29() { return &___m_eStatus_29; }
	inline void set_m_eStatus_29(int32_t value)
	{
		___m_eStatus_29 = value;
	}

	inline static int32_t get_offset_of_m_bCanUnlock_30() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ___m_bCanUnlock_30)); }
	inline bool get_m_bCanUnlock_30() const { return ___m_bCanUnlock_30; }
	inline bool* get_address_of_m_bCanUnlock_30() { return &___m_bCanUnlock_30; }
	inline void set_m_bCanUnlock_30(bool value)
	{
		___m_bCanUnlock_30 = value;
	}

	inline static int32_t get_offset_of_m_bLocked_31() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ___m_bLocked_31)); }
	inline bool get_m_bLocked_31() const { return ___m_bLocked_31; }
	inline bool* get_address_of_m_bLocked_31() { return &___m_bLocked_31; }
	inline void set_m_bLocked_31(bool value)
	{
		___m_bLocked_31 = value;
	}

	inline static int32_t get_offset_of_m_fBtnEnterShowTime_32() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ___m_fBtnEnterShowTime_32)); }
	inline float get_m_fBtnEnterShowTime_32() const { return ___m_fBtnEnterShowTime_32; }
	inline float* get_address_of_m_fBtnEnterShowTime_32() { return &___m_fBtnEnterShowTime_32; }
	inline void set_m_fBtnEnterShowTime_32(float value)
	{
		___m_fBtnEnterShowTime_32 = value;
	}

	inline static int32_t get_offset_of_m_bClickMe_33() { return static_cast<int32_t>(offsetof(UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E, ___m_bClickMe_33)); }
	inline bool get_m_bClickMe_33() const { return ___m_bClickMe_33; }
	inline bool* get_address_of_m_bClickMe_33() { return &___m_bClickMe_33; }
	inline void set_m_bClickMe_33(bool value)
	{
		___m_bClickMe_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBIGMAPTRACKINFO_TD1567A742038A1B490AA47873BC98DF75421C69E_H
#ifndef UIBUFFCOUNTER_T2E5C624662362FB38268B941481E453A4FAD4E26_H
#define UIBUFFCOUNTER_T2E5C624662362FB38268B941481E453A4FAD4E26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIBuffCounter
struct  UIBuffCounter_t2E5C624662362FB38268B941481E453A4FAD4E26  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text UIBuffCounter::_txtValue
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtValue_4;
	// UnityEngine.UI.Image UIBuffCounter::_imgIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgIcon_5;

public:
	inline static int32_t get_offset_of__txtValue_4() { return static_cast<int32_t>(offsetof(UIBuffCounter_t2E5C624662362FB38268B941481E453A4FAD4E26, ____txtValue_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtValue_4() const { return ____txtValue_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtValue_4() { return &____txtValue_4; }
	inline void set__txtValue_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtValue_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtValue_4), value);
	}

	inline static int32_t get_offset_of__imgIcon_5() { return static_cast<int32_t>(offsetof(UIBuffCounter_t2E5C624662362FB38268B941481E453A4FAD4E26, ____imgIcon_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgIcon_5() const { return ____imgIcon_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgIcon_5() { return &____imgIcon_5; }
	inline void set__imgIcon_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgIcon_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUFFCOUNTER_T2E5C624662362FB38268B941481E453A4FAD4E26_H
#ifndef UIDISTRICT_TD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7_H
#define UIDISTRICT_TD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDistrict
struct  UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject UIDistrict::_lock
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lock_4;
	// System.Boolean UIDistrict::m_bLocked
	bool ___m_bLocked_5;
	// System.Boolean UIDistrict::m_bCanUnlock
	bool ___m_bCanUnlock_6;
	// District UIDistrict::m_District
	District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * ___m_District_7;
	// UnityEngine.GameObject UIDistrict::_goWenHao
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goWenHao_8;
	// UnityEngine.UI.Text UIDistrict::_txtUnlockPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtUnlockPrice_9;
	// UnityEngine.UI.Text UIDistrict::_txtTrackName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTrackName_10;
	// UnityEngine.GameObject UIDistrict::_goPrice
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goPrice_11;
	// UnityEngine.UI.Image UIDistrict::_imgMoneyIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgMoneyIcon_12;
	// UnityEngine.UI.Image UIDistrict::_imgMoneyIcon_ShouYi
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgMoneyIcon_ShouYi_13;
	// UnityEngine.UI.Text UIDistrict::_txtTiSheng
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTiSheng_14;
	// UnityEngine.UI.Text UIDistrict::_txtShouYi
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtShouYi_15;
	// UnityEngine.GameObject UIDistrict::_contaainerShouYi
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____contaainerShouYi_16;
	// UnityEngine.GameObject UIDistrict::_containerTiShengAndShouYi
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerTiShengAndShouYi_17;
	// UnityEngine.UI.Image UIDistrict::_imgBg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgBg_18;
	// UnityEngine.UI.Text UIDistrict::_txtIsCurDistrict
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtIsCurDistrict_19;
	// UnityEngine.UI.Button UIDistrict::_btnEnter
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnEnter_20;
	// System.Single UIDistrict::m_fEnterButtonShowTime
	float ___m_fEnterButtonShowTime_21;

public:
	inline static int32_t get_offset_of__lock_4() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____lock_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lock_4() const { return ____lock_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lock_4() { return &____lock_4; }
	inline void set__lock_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lock_4 = value;
		Il2CppCodeGenWriteBarrier((&____lock_4), value);
	}

	inline static int32_t get_offset_of_m_bLocked_5() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ___m_bLocked_5)); }
	inline bool get_m_bLocked_5() const { return ___m_bLocked_5; }
	inline bool* get_address_of_m_bLocked_5() { return &___m_bLocked_5; }
	inline void set_m_bLocked_5(bool value)
	{
		___m_bLocked_5 = value;
	}

	inline static int32_t get_offset_of_m_bCanUnlock_6() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ___m_bCanUnlock_6)); }
	inline bool get_m_bCanUnlock_6() const { return ___m_bCanUnlock_6; }
	inline bool* get_address_of_m_bCanUnlock_6() { return &___m_bCanUnlock_6; }
	inline void set_m_bCanUnlock_6(bool value)
	{
		___m_bCanUnlock_6 = value;
	}

	inline static int32_t get_offset_of_m_District_7() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ___m_District_7)); }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * get_m_District_7() const { return ___m_District_7; }
	inline District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A ** get_address_of_m_District_7() { return &___m_District_7; }
	inline void set_m_District_7(District_t773B8BE5DDC4F45983ECB3226E97FF6FEFACC19A * value)
	{
		___m_District_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_District_7), value);
	}

	inline static int32_t get_offset_of__goWenHao_8() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____goWenHao_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goWenHao_8() const { return ____goWenHao_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goWenHao_8() { return &____goWenHao_8; }
	inline void set__goWenHao_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goWenHao_8 = value;
		Il2CppCodeGenWriteBarrier((&____goWenHao_8), value);
	}

	inline static int32_t get_offset_of__txtUnlockPrice_9() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____txtUnlockPrice_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtUnlockPrice_9() const { return ____txtUnlockPrice_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtUnlockPrice_9() { return &____txtUnlockPrice_9; }
	inline void set__txtUnlockPrice_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtUnlockPrice_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtUnlockPrice_9), value);
	}

	inline static int32_t get_offset_of__txtTrackName_10() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____txtTrackName_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTrackName_10() const { return ____txtTrackName_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTrackName_10() { return &____txtTrackName_10; }
	inline void set__txtTrackName_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTrackName_10 = value;
		Il2CppCodeGenWriteBarrier((&____txtTrackName_10), value);
	}

	inline static int32_t get_offset_of__goPrice_11() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____goPrice_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goPrice_11() const { return ____goPrice_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goPrice_11() { return &____goPrice_11; }
	inline void set__goPrice_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goPrice_11 = value;
		Il2CppCodeGenWriteBarrier((&____goPrice_11), value);
	}

	inline static int32_t get_offset_of__imgMoneyIcon_12() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____imgMoneyIcon_12)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgMoneyIcon_12() const { return ____imgMoneyIcon_12; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgMoneyIcon_12() { return &____imgMoneyIcon_12; }
	inline void set__imgMoneyIcon_12(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgMoneyIcon_12 = value;
		Il2CppCodeGenWriteBarrier((&____imgMoneyIcon_12), value);
	}

	inline static int32_t get_offset_of__imgMoneyIcon_ShouYi_13() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____imgMoneyIcon_ShouYi_13)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgMoneyIcon_ShouYi_13() const { return ____imgMoneyIcon_ShouYi_13; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgMoneyIcon_ShouYi_13() { return &____imgMoneyIcon_ShouYi_13; }
	inline void set__imgMoneyIcon_ShouYi_13(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgMoneyIcon_ShouYi_13 = value;
		Il2CppCodeGenWriteBarrier((&____imgMoneyIcon_ShouYi_13), value);
	}

	inline static int32_t get_offset_of__txtTiSheng_14() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____txtTiSheng_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTiSheng_14() const { return ____txtTiSheng_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTiSheng_14() { return &____txtTiSheng_14; }
	inline void set__txtTiSheng_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTiSheng_14 = value;
		Il2CppCodeGenWriteBarrier((&____txtTiSheng_14), value);
	}

	inline static int32_t get_offset_of__txtShouYi_15() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____txtShouYi_15)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtShouYi_15() const { return ____txtShouYi_15; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtShouYi_15() { return &____txtShouYi_15; }
	inline void set__txtShouYi_15(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtShouYi_15 = value;
		Il2CppCodeGenWriteBarrier((&____txtShouYi_15), value);
	}

	inline static int32_t get_offset_of__contaainerShouYi_16() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____contaainerShouYi_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__contaainerShouYi_16() const { return ____contaainerShouYi_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__contaainerShouYi_16() { return &____contaainerShouYi_16; }
	inline void set__contaainerShouYi_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____contaainerShouYi_16 = value;
		Il2CppCodeGenWriteBarrier((&____contaainerShouYi_16), value);
	}

	inline static int32_t get_offset_of__containerTiShengAndShouYi_17() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____containerTiShengAndShouYi_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerTiShengAndShouYi_17() const { return ____containerTiShengAndShouYi_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerTiShengAndShouYi_17() { return &____containerTiShengAndShouYi_17; }
	inline void set__containerTiShengAndShouYi_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerTiShengAndShouYi_17 = value;
		Il2CppCodeGenWriteBarrier((&____containerTiShengAndShouYi_17), value);
	}

	inline static int32_t get_offset_of__imgBg_18() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____imgBg_18)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgBg_18() const { return ____imgBg_18; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgBg_18() { return &____imgBg_18; }
	inline void set__imgBg_18(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgBg_18 = value;
		Il2CppCodeGenWriteBarrier((&____imgBg_18), value);
	}

	inline static int32_t get_offset_of__txtIsCurDistrict_19() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____txtIsCurDistrict_19)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtIsCurDistrict_19() const { return ____txtIsCurDistrict_19; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtIsCurDistrict_19() { return &____txtIsCurDistrict_19; }
	inline void set__txtIsCurDistrict_19(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtIsCurDistrict_19 = value;
		Il2CppCodeGenWriteBarrier((&____txtIsCurDistrict_19), value);
	}

	inline static int32_t get_offset_of__btnEnter_20() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ____btnEnter_20)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnEnter_20() const { return ____btnEnter_20; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnEnter_20() { return &____btnEnter_20; }
	inline void set__btnEnter_20(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnEnter_20 = value;
		Il2CppCodeGenWriteBarrier((&____btnEnter_20), value);
	}

	inline static int32_t get_offset_of_m_fEnterButtonShowTime_21() { return static_cast<int32_t>(offsetof(UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7, ___m_fEnterButtonShowTime_21)); }
	inline float get_m_fEnterButtonShowTime_21() const { return ___m_fEnterButtonShowTime_21; }
	inline float* get_address_of_m_fEnterButtonShowTime_21() { return &___m_fEnterButtonShowTime_21; }
	inline void set_m_fEnterButtonShowTime_21(float value)
	{
		___m_fEnterButtonShowTime_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDISTRICT_TD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7_H
#ifndef UIFLYINGCOIN_T8763DFF8DEB3473D5330B0517A0D33B063DBE0D2_H
#define UIFLYINGCOIN_T8763DFF8DEB3473D5330B0517A0D33B063DBE0D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIFlyingCoin
struct  UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 UIFlyingCoin::m_vecStartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecStartPos_5;
	// UnityEngine.Vector3 UIFlyingCoin::m_vecEndPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecEndPos_6;
	// System.Single UIFlyingCoin::m_fSession0_Time
	float ___m_fSession0_Time_7;
	// System.Single UIFlyingCoin::m_fSession1_Time
	float ___m_fSession1_Time_8;
	// UnityEngine.Vector2 UIFlyingCoin::m_vecSession0End
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecSession0End_9;
	// UnityEngine.Vector2 UIFlyingCoin::m_vecSession0_A
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecSession0_A_10;
	// UnityEngine.Vector2 UIFlyingCoin::m_vecSession1_A
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecSession1_A_11;
	// UnityEngine.Vector2 UIFlyingCoin::m_vecVelocity
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecVelocity_12;
	// System.Int32 UIFlyingCoin::m_nStatus
	int32_t ___m_nStatus_13;
	// UnityEngine.UI.Image UIFlyingCoin::_imgMain
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgMain_15;

public:
	inline static int32_t get_offset_of_m_vecStartPos_5() { return static_cast<int32_t>(offsetof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2, ___m_vecStartPos_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecStartPos_5() const { return ___m_vecStartPos_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecStartPos_5() { return &___m_vecStartPos_5; }
	inline void set_m_vecStartPos_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecStartPos_5 = value;
	}

	inline static int32_t get_offset_of_m_vecEndPos_6() { return static_cast<int32_t>(offsetof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2, ___m_vecEndPos_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecEndPos_6() const { return ___m_vecEndPos_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecEndPos_6() { return &___m_vecEndPos_6; }
	inline void set_m_vecEndPos_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecEndPos_6 = value;
	}

	inline static int32_t get_offset_of_m_fSession0_Time_7() { return static_cast<int32_t>(offsetof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2, ___m_fSession0_Time_7)); }
	inline float get_m_fSession0_Time_7() const { return ___m_fSession0_Time_7; }
	inline float* get_address_of_m_fSession0_Time_7() { return &___m_fSession0_Time_7; }
	inline void set_m_fSession0_Time_7(float value)
	{
		___m_fSession0_Time_7 = value;
	}

	inline static int32_t get_offset_of_m_fSession1_Time_8() { return static_cast<int32_t>(offsetof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2, ___m_fSession1_Time_8)); }
	inline float get_m_fSession1_Time_8() const { return ___m_fSession1_Time_8; }
	inline float* get_address_of_m_fSession1_Time_8() { return &___m_fSession1_Time_8; }
	inline void set_m_fSession1_Time_8(float value)
	{
		___m_fSession1_Time_8 = value;
	}

	inline static int32_t get_offset_of_m_vecSession0End_9() { return static_cast<int32_t>(offsetof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2, ___m_vecSession0End_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecSession0End_9() const { return ___m_vecSession0End_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecSession0End_9() { return &___m_vecSession0End_9; }
	inline void set_m_vecSession0End_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecSession0End_9 = value;
	}

	inline static int32_t get_offset_of_m_vecSession0_A_10() { return static_cast<int32_t>(offsetof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2, ___m_vecSession0_A_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecSession0_A_10() const { return ___m_vecSession0_A_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecSession0_A_10() { return &___m_vecSession0_A_10; }
	inline void set_m_vecSession0_A_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecSession0_A_10 = value;
	}

	inline static int32_t get_offset_of_m_vecSession1_A_11() { return static_cast<int32_t>(offsetof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2, ___m_vecSession1_A_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecSession1_A_11() const { return ___m_vecSession1_A_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecSession1_A_11() { return &___m_vecSession1_A_11; }
	inline void set_m_vecSession1_A_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecSession1_A_11 = value;
	}

	inline static int32_t get_offset_of_m_vecVelocity_12() { return static_cast<int32_t>(offsetof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2, ___m_vecVelocity_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecVelocity_12() const { return ___m_vecVelocity_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecVelocity_12() { return &___m_vecVelocity_12; }
	inline void set_m_vecVelocity_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecVelocity_12 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_13() { return static_cast<int32_t>(offsetof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2, ___m_nStatus_13)); }
	inline int32_t get_m_nStatus_13() const { return ___m_nStatus_13; }
	inline int32_t* get_address_of_m_nStatus_13() { return &___m_nStatus_13; }
	inline void set_m_nStatus_13(int32_t value)
	{
		___m_nStatus_13 = value;
	}

	inline static int32_t get_offset_of__imgMain_15() { return static_cast<int32_t>(offsetof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2, ____imgMain_15)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgMain_15() const { return ____imgMain_15; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgMain_15() { return &____imgMain_15; }
	inline void set__imgMain_15(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgMain_15 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_15), value);
	}
};

struct UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2_StaticFields
{
public:
	// UnityEngine.Vector3 UIFlyingCoin::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_4;
	// System.Int32 UIFlyingCoin::s_ShitCount
	int32_t ___s_ShitCount_14;

public:
	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2_StaticFields, ___vecTempPos_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_s_ShitCount_14() { return static_cast<int32_t>(offsetof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2_StaticFields, ___s_ShitCount_14)); }
	inline int32_t get_s_ShitCount_14() const { return ___s_ShitCount_14; }
	inline int32_t* get_address_of_s_ShitCount_14() { return &___s_ShitCount_14; }
	inline void set_s_ShitCount_14(int32_t value)
	{
		___s_ShitCount_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIFLYINGCOIN_T8763DFF8DEB3473D5330B0517A0D33B063DBE0D2_H
#ifndef UIITEM_T14D41FF84CDBA85F0EC99C29A710A1D348A61957_H
#define UIITEM_T14D41FF84CDBA85F0EC99C29A710A1D348A61957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIItem
struct  UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean UIItem::m_bInBag
	bool ___m_bInBag_4;
	// UnityEngine.GameObject UIItem::_panelCounting
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____panelCounting_5;
	// UnityEngine.UI.Text UIItem::_txtName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtName_6;
	// UnityEngine.UI.Text UIItem::_txtDesc
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDesc_7;
	// UnityEngine.UI.Text UIItem::_txtValue
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtValue_8;
	// UnityEngine.UI.Text UIItem::_txtPrice_Coin
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrice_Coin_9;
	// UnityEngine.UI.Text UIItem::_txtPrice_Diamond
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrice_Diamond_10;
	// UnityEngine.UI.Text UIItem::_txtPrice1
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrice1_11;
	// UnityEngine.UI.Text UIItem::_txtPrice2
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrice2_12;
	// UnityEngine.UI.Image UIItem::_imgMoneyIcon1
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgMoneyIcon1_13;
	// UnityEngine.UI.Image UIItem::_imgMoneyIcon2
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgMoneyIcon2_14;
	// UnityEngine.GameObject UIItem::_goPrice1
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goPrice1_15;
	// UnityEngine.GameObject UIItem::_goPrice2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goPrice2_16;
	// UnityEngine.UI.Text UIItem::_txtNum
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtNum_17;
	// UnityEngine.UI.Text UIItem::_txtLeftTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtLeftTime_18;
	// UnityEngine.UI.Button UIItem::_btnUse
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnUse_19;
	// ShoppinMall/eItemType UIItem::m_eItemType
	int32_t ___m_eItemType_20;
	// ShoppinMall/ePriceType UIItem::m_ePriceType
	int32_t ___m_ePriceType_21;
	// ShoppinMall/ePriceSubType UIItem::m_ePriceSubType
	int32_t ___m_ePriceSubType_22;
	// System.Int32 UIItem::m_nItemId
	int32_t ___m_nItemId_23;
	// System.Double UIItem::m_nPrice_Coin
	double ___m_nPrice_Coin_24;
	// System.Single UIItem::m_fCoinPriceRiseAfterBuy
	float ___m_fCoinPriceRiseAfterBuy_25;
	// System.Double UIItem::m_nPrice_Diamond
	double ___m_nPrice_Diamond_26;
	// System.Int32 UIItem::m_nValue0
	int32_t ___m_nValue0_27;
	// System.Int32 UIItem::m_nDuration
	int32_t ___m_nDuration_28;
	// System.Single UIItem::m_fStartTime
	float ___m_fStartTime_29;
	// ShoppinMall/sShoppingMallItemConfig UIItem::m_Config
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  ___m_Config_30;
	// ItemSystem/sItemConfig UIItem::m_BagItemConfig
	sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C  ___m_BagItemConfig_31;
	// System.Int32 UIItem::m_nNum
	int32_t ___m_nNum_32;
	// System.Int32[] UIItem::m_aryIntParams
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_aryIntParams_33;
	// System.Single[] UIItem::m_aryFloatParams
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_aryFloatParams_34;
	// System.Int32 UIItem::m_nLeftTime
	int32_t ___m_nLeftTime_35;
	// ShoppinMall/ePriceType UIItem::m_eToBuy_MoneyType
	int32_t ___m_eToBuy_MoneyType_36;
	// System.Int32 UIItem::m_nToBuy_SubMoneyType
	int32_t ___m_nToBuy_SubMoneyType_37;
	// System.Double UIItem::m_nToBuy_Price
	double ___m_nToBuy_Price_38;
	// System.Int32 UIItem::m_nToBuy_Num
	int32_t ___m_nToBuy_Num_39;
	// ShoppinMall/sShoppingMallItemConfig UIItem::m_ToBuyConfig
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  ___m_ToBuyConfig_40;

public:
	inline static int32_t get_offset_of_m_bInBag_4() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_bInBag_4)); }
	inline bool get_m_bInBag_4() const { return ___m_bInBag_4; }
	inline bool* get_address_of_m_bInBag_4() { return &___m_bInBag_4; }
	inline void set_m_bInBag_4(bool value)
	{
		___m_bInBag_4 = value;
	}

	inline static int32_t get_offset_of__panelCounting_5() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____panelCounting_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__panelCounting_5() const { return ____panelCounting_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__panelCounting_5() { return &____panelCounting_5; }
	inline void set__panelCounting_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____panelCounting_5 = value;
		Il2CppCodeGenWriteBarrier((&____panelCounting_5), value);
	}

	inline static int32_t get_offset_of__txtName_6() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____txtName_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtName_6() const { return ____txtName_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtName_6() { return &____txtName_6; }
	inline void set__txtName_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtName_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtName_6), value);
	}

	inline static int32_t get_offset_of__txtDesc_7() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____txtDesc_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDesc_7() const { return ____txtDesc_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDesc_7() { return &____txtDesc_7; }
	inline void set__txtDesc_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDesc_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtDesc_7), value);
	}

	inline static int32_t get_offset_of__txtValue_8() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____txtValue_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtValue_8() const { return ____txtValue_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtValue_8() { return &____txtValue_8; }
	inline void set__txtValue_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtValue_8 = value;
		Il2CppCodeGenWriteBarrier((&____txtValue_8), value);
	}

	inline static int32_t get_offset_of__txtPrice_Coin_9() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____txtPrice_Coin_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrice_Coin_9() const { return ____txtPrice_Coin_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrice_Coin_9() { return &____txtPrice_Coin_9; }
	inline void set__txtPrice_Coin_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrice_Coin_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrice_Coin_9), value);
	}

	inline static int32_t get_offset_of__txtPrice_Diamond_10() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____txtPrice_Diamond_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrice_Diamond_10() const { return ____txtPrice_Diamond_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrice_Diamond_10() { return &____txtPrice_Diamond_10; }
	inline void set__txtPrice_Diamond_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrice_Diamond_10 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrice_Diamond_10), value);
	}

	inline static int32_t get_offset_of__txtPrice1_11() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____txtPrice1_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrice1_11() const { return ____txtPrice1_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrice1_11() { return &____txtPrice1_11; }
	inline void set__txtPrice1_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrice1_11 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrice1_11), value);
	}

	inline static int32_t get_offset_of__txtPrice2_12() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____txtPrice2_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrice2_12() const { return ____txtPrice2_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrice2_12() { return &____txtPrice2_12; }
	inline void set__txtPrice2_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrice2_12 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrice2_12), value);
	}

	inline static int32_t get_offset_of__imgMoneyIcon1_13() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____imgMoneyIcon1_13)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgMoneyIcon1_13() const { return ____imgMoneyIcon1_13; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgMoneyIcon1_13() { return &____imgMoneyIcon1_13; }
	inline void set__imgMoneyIcon1_13(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgMoneyIcon1_13 = value;
		Il2CppCodeGenWriteBarrier((&____imgMoneyIcon1_13), value);
	}

	inline static int32_t get_offset_of__imgMoneyIcon2_14() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____imgMoneyIcon2_14)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgMoneyIcon2_14() const { return ____imgMoneyIcon2_14; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgMoneyIcon2_14() { return &____imgMoneyIcon2_14; }
	inline void set__imgMoneyIcon2_14(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgMoneyIcon2_14 = value;
		Il2CppCodeGenWriteBarrier((&____imgMoneyIcon2_14), value);
	}

	inline static int32_t get_offset_of__goPrice1_15() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____goPrice1_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goPrice1_15() const { return ____goPrice1_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goPrice1_15() { return &____goPrice1_15; }
	inline void set__goPrice1_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goPrice1_15 = value;
		Il2CppCodeGenWriteBarrier((&____goPrice1_15), value);
	}

	inline static int32_t get_offset_of__goPrice2_16() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____goPrice2_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goPrice2_16() const { return ____goPrice2_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goPrice2_16() { return &____goPrice2_16; }
	inline void set__goPrice2_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goPrice2_16 = value;
		Il2CppCodeGenWriteBarrier((&____goPrice2_16), value);
	}

	inline static int32_t get_offset_of__txtNum_17() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____txtNum_17)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtNum_17() const { return ____txtNum_17; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtNum_17() { return &____txtNum_17; }
	inline void set__txtNum_17(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtNum_17 = value;
		Il2CppCodeGenWriteBarrier((&____txtNum_17), value);
	}

	inline static int32_t get_offset_of__txtLeftTime_18() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____txtLeftTime_18)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtLeftTime_18() const { return ____txtLeftTime_18; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtLeftTime_18() { return &____txtLeftTime_18; }
	inline void set__txtLeftTime_18(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtLeftTime_18 = value;
		Il2CppCodeGenWriteBarrier((&____txtLeftTime_18), value);
	}

	inline static int32_t get_offset_of__btnUse_19() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ____btnUse_19)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnUse_19() const { return ____btnUse_19; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnUse_19() { return &____btnUse_19; }
	inline void set__btnUse_19(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnUse_19 = value;
		Il2CppCodeGenWriteBarrier((&____btnUse_19), value);
	}

	inline static int32_t get_offset_of_m_eItemType_20() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_eItemType_20)); }
	inline int32_t get_m_eItemType_20() const { return ___m_eItemType_20; }
	inline int32_t* get_address_of_m_eItemType_20() { return &___m_eItemType_20; }
	inline void set_m_eItemType_20(int32_t value)
	{
		___m_eItemType_20 = value;
	}

	inline static int32_t get_offset_of_m_ePriceType_21() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_ePriceType_21)); }
	inline int32_t get_m_ePriceType_21() const { return ___m_ePriceType_21; }
	inline int32_t* get_address_of_m_ePriceType_21() { return &___m_ePriceType_21; }
	inline void set_m_ePriceType_21(int32_t value)
	{
		___m_ePriceType_21 = value;
	}

	inline static int32_t get_offset_of_m_ePriceSubType_22() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_ePriceSubType_22)); }
	inline int32_t get_m_ePriceSubType_22() const { return ___m_ePriceSubType_22; }
	inline int32_t* get_address_of_m_ePriceSubType_22() { return &___m_ePriceSubType_22; }
	inline void set_m_ePriceSubType_22(int32_t value)
	{
		___m_ePriceSubType_22 = value;
	}

	inline static int32_t get_offset_of_m_nItemId_23() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_nItemId_23)); }
	inline int32_t get_m_nItemId_23() const { return ___m_nItemId_23; }
	inline int32_t* get_address_of_m_nItemId_23() { return &___m_nItemId_23; }
	inline void set_m_nItemId_23(int32_t value)
	{
		___m_nItemId_23 = value;
	}

	inline static int32_t get_offset_of_m_nPrice_Coin_24() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_nPrice_Coin_24)); }
	inline double get_m_nPrice_Coin_24() const { return ___m_nPrice_Coin_24; }
	inline double* get_address_of_m_nPrice_Coin_24() { return &___m_nPrice_Coin_24; }
	inline void set_m_nPrice_Coin_24(double value)
	{
		___m_nPrice_Coin_24 = value;
	}

	inline static int32_t get_offset_of_m_fCoinPriceRiseAfterBuy_25() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_fCoinPriceRiseAfterBuy_25)); }
	inline float get_m_fCoinPriceRiseAfterBuy_25() const { return ___m_fCoinPriceRiseAfterBuy_25; }
	inline float* get_address_of_m_fCoinPriceRiseAfterBuy_25() { return &___m_fCoinPriceRiseAfterBuy_25; }
	inline void set_m_fCoinPriceRiseAfterBuy_25(float value)
	{
		___m_fCoinPriceRiseAfterBuy_25 = value;
	}

	inline static int32_t get_offset_of_m_nPrice_Diamond_26() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_nPrice_Diamond_26)); }
	inline double get_m_nPrice_Diamond_26() const { return ___m_nPrice_Diamond_26; }
	inline double* get_address_of_m_nPrice_Diamond_26() { return &___m_nPrice_Diamond_26; }
	inline void set_m_nPrice_Diamond_26(double value)
	{
		___m_nPrice_Diamond_26 = value;
	}

	inline static int32_t get_offset_of_m_nValue0_27() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_nValue0_27)); }
	inline int32_t get_m_nValue0_27() const { return ___m_nValue0_27; }
	inline int32_t* get_address_of_m_nValue0_27() { return &___m_nValue0_27; }
	inline void set_m_nValue0_27(int32_t value)
	{
		___m_nValue0_27 = value;
	}

	inline static int32_t get_offset_of_m_nDuration_28() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_nDuration_28)); }
	inline int32_t get_m_nDuration_28() const { return ___m_nDuration_28; }
	inline int32_t* get_address_of_m_nDuration_28() { return &___m_nDuration_28; }
	inline void set_m_nDuration_28(int32_t value)
	{
		___m_nDuration_28 = value;
	}

	inline static int32_t get_offset_of_m_fStartTime_29() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_fStartTime_29)); }
	inline float get_m_fStartTime_29() const { return ___m_fStartTime_29; }
	inline float* get_address_of_m_fStartTime_29() { return &___m_fStartTime_29; }
	inline void set_m_fStartTime_29(float value)
	{
		___m_fStartTime_29 = value;
	}

	inline static int32_t get_offset_of_m_Config_30() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_Config_30)); }
	inline sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  get_m_Config_30() const { return ___m_Config_30; }
	inline sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058 * get_address_of_m_Config_30() { return &___m_Config_30; }
	inline void set_m_Config_30(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  value)
	{
		___m_Config_30 = value;
	}

	inline static int32_t get_offset_of_m_BagItemConfig_31() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_BagItemConfig_31)); }
	inline sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C  get_m_BagItemConfig_31() const { return ___m_BagItemConfig_31; }
	inline sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C * get_address_of_m_BagItemConfig_31() { return &___m_BagItemConfig_31; }
	inline void set_m_BagItemConfig_31(sItemConfig_t9C277270FC8FC12356FC8BA5BD234112769E425C  value)
	{
		___m_BagItemConfig_31 = value;
	}

	inline static int32_t get_offset_of_m_nNum_32() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_nNum_32)); }
	inline int32_t get_m_nNum_32() const { return ___m_nNum_32; }
	inline int32_t* get_address_of_m_nNum_32() { return &___m_nNum_32; }
	inline void set_m_nNum_32(int32_t value)
	{
		___m_nNum_32 = value;
	}

	inline static int32_t get_offset_of_m_aryIntParams_33() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_aryIntParams_33)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_aryIntParams_33() const { return ___m_aryIntParams_33; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_aryIntParams_33() { return &___m_aryIntParams_33; }
	inline void set_m_aryIntParams_33(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_aryIntParams_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryIntParams_33), value);
	}

	inline static int32_t get_offset_of_m_aryFloatParams_34() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_aryFloatParams_34)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_aryFloatParams_34() const { return ___m_aryFloatParams_34; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_aryFloatParams_34() { return &___m_aryFloatParams_34; }
	inline void set_m_aryFloatParams_34(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_aryFloatParams_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryFloatParams_34), value);
	}

	inline static int32_t get_offset_of_m_nLeftTime_35() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_nLeftTime_35)); }
	inline int32_t get_m_nLeftTime_35() const { return ___m_nLeftTime_35; }
	inline int32_t* get_address_of_m_nLeftTime_35() { return &___m_nLeftTime_35; }
	inline void set_m_nLeftTime_35(int32_t value)
	{
		___m_nLeftTime_35 = value;
	}

	inline static int32_t get_offset_of_m_eToBuy_MoneyType_36() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_eToBuy_MoneyType_36)); }
	inline int32_t get_m_eToBuy_MoneyType_36() const { return ___m_eToBuy_MoneyType_36; }
	inline int32_t* get_address_of_m_eToBuy_MoneyType_36() { return &___m_eToBuy_MoneyType_36; }
	inline void set_m_eToBuy_MoneyType_36(int32_t value)
	{
		___m_eToBuy_MoneyType_36 = value;
	}

	inline static int32_t get_offset_of_m_nToBuy_SubMoneyType_37() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_nToBuy_SubMoneyType_37)); }
	inline int32_t get_m_nToBuy_SubMoneyType_37() const { return ___m_nToBuy_SubMoneyType_37; }
	inline int32_t* get_address_of_m_nToBuy_SubMoneyType_37() { return &___m_nToBuy_SubMoneyType_37; }
	inline void set_m_nToBuy_SubMoneyType_37(int32_t value)
	{
		___m_nToBuy_SubMoneyType_37 = value;
	}

	inline static int32_t get_offset_of_m_nToBuy_Price_38() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_nToBuy_Price_38)); }
	inline double get_m_nToBuy_Price_38() const { return ___m_nToBuy_Price_38; }
	inline double* get_address_of_m_nToBuy_Price_38() { return &___m_nToBuy_Price_38; }
	inline void set_m_nToBuy_Price_38(double value)
	{
		___m_nToBuy_Price_38 = value;
	}

	inline static int32_t get_offset_of_m_nToBuy_Num_39() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_nToBuy_Num_39)); }
	inline int32_t get_m_nToBuy_Num_39() const { return ___m_nToBuy_Num_39; }
	inline int32_t* get_address_of_m_nToBuy_Num_39() { return &___m_nToBuy_Num_39; }
	inline void set_m_nToBuy_Num_39(int32_t value)
	{
		___m_nToBuy_Num_39 = value;
	}

	inline static int32_t get_offset_of_m_ToBuyConfig_40() { return static_cast<int32_t>(offsetof(UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957, ___m_ToBuyConfig_40)); }
	inline sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  get_m_ToBuyConfig_40() const { return ___m_ToBuyConfig_40; }
	inline sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058 * get_address_of_m_ToBuyConfig_40() { return &___m_ToBuyConfig_40; }
	inline void set_m_ToBuyConfig_40(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  value)
	{
		___m_ToBuyConfig_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIITEM_T14D41FF84CDBA85F0EC99C29A710A1D348A61957_H
#ifndef UIITEMBAGCONTAINER_T1A192CE450A13125409D3FD893ED1E4BF6B213B6_H
#define UIITEMBAGCONTAINER_T1A192CE450A13125409D3FD893ED1E4BF6B213B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIItemBagContainer
struct  UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject UIItemBagContainer::m_containerTemp
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerTemp_6;
	// UnityEngine.UI.VerticalLayoutGroup UIItemBagContainer::_vlg
	VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 * ____vlg_7;
	// System.Single UIItemBagContainer::m_fHoriGap
	float ___m_fHoriGap_8;
	// System.Single UIItemBagContainer::m_fVertGap
	float ___m_fVertGap_9;
	// System.Collections.Generic.List`1<UIShoppinAndItemCounter> UIItemBagContainer::m_lstItems
	List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * ___m_lstItems_10;

public:
	inline static int32_t get_offset_of_m_containerTemp_6() { return static_cast<int32_t>(offsetof(UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6, ___m_containerTemp_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerTemp_6() const { return ___m_containerTemp_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerTemp_6() { return &___m_containerTemp_6; }
	inline void set_m_containerTemp_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerTemp_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerTemp_6), value);
	}

	inline static int32_t get_offset_of__vlg_7() { return static_cast<int32_t>(offsetof(UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6, ____vlg_7)); }
	inline VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 * get__vlg_7() const { return ____vlg_7; }
	inline VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 ** get_address_of__vlg_7() { return &____vlg_7; }
	inline void set__vlg_7(VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11 * value)
	{
		____vlg_7 = value;
		Il2CppCodeGenWriteBarrier((&____vlg_7), value);
	}

	inline static int32_t get_offset_of_m_fHoriGap_8() { return static_cast<int32_t>(offsetof(UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6, ___m_fHoriGap_8)); }
	inline float get_m_fHoriGap_8() const { return ___m_fHoriGap_8; }
	inline float* get_address_of_m_fHoriGap_8() { return &___m_fHoriGap_8; }
	inline void set_m_fHoriGap_8(float value)
	{
		___m_fHoriGap_8 = value;
	}

	inline static int32_t get_offset_of_m_fVertGap_9() { return static_cast<int32_t>(offsetof(UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6, ___m_fVertGap_9)); }
	inline float get_m_fVertGap_9() const { return ___m_fVertGap_9; }
	inline float* get_address_of_m_fVertGap_9() { return &___m_fVertGap_9; }
	inline void set_m_fVertGap_9(float value)
	{
		___m_fVertGap_9 = value;
	}

	inline static int32_t get_offset_of_m_lstItems_10() { return static_cast<int32_t>(offsetof(UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6, ___m_lstItems_10)); }
	inline List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * get_m_lstItems_10() const { return ___m_lstItems_10; }
	inline List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B ** get_address_of_m_lstItems_10() { return &___m_lstItems_10; }
	inline void set_m_lstItems_10(List_1_tD61F2E73BA9B661D01DA67EC40E292A6F5C2354B * value)
	{
		___m_lstItems_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstItems_10), value);
	}
};

struct UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6_StaticFields
{
public:
	// UnityEngine.Vector3 UIItemBagContainer::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_4;
	// UnityEngine.Vector3 UIItemBagContainer::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_5;

public:
	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6_StaticFields, ___vecTempPos_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6_StaticFields, ___vecTempScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIITEMBAGCONTAINER_T1A192CE450A13125409D3FD893ED1E4BF6B213B6_H
#ifndef UIITEMINBAG_T7F21A3C7BDBB6A681598F874C027E613F87CADE6_H
#define UIITEMINBAG_T7F21A3C7BDBB6A681598F874C027E613F87CADE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIItemInBag
struct  UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text UIItemInBag::_txtFuncValue
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtFuncValue_4;
	// UnityEngine.UI.Text UIItemInBag::_txtLeftTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtLeftTime_5;
	// UnityEngine.UI.Image UIItemInBag::_imgAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgAvatar_6;
	// System.Int32 UIItemInBag::m_nItemId
	int32_t ___m_nItemId_7;
	// System.Int32 UIItemInBag::m_nValue0
	int32_t ___m_nValue0_8;
	// System.Int32 UIItemInBag::m_nDuration
	int32_t ___m_nDuration_9;
	// System.DateTime UIItemInBag::m_StartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_StartTime_10;

public:
	inline static int32_t get_offset_of__txtFuncValue_4() { return static_cast<int32_t>(offsetof(UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6, ____txtFuncValue_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtFuncValue_4() const { return ____txtFuncValue_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtFuncValue_4() { return &____txtFuncValue_4; }
	inline void set__txtFuncValue_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtFuncValue_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtFuncValue_4), value);
	}

	inline static int32_t get_offset_of__txtLeftTime_5() { return static_cast<int32_t>(offsetof(UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6, ____txtLeftTime_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtLeftTime_5() const { return ____txtLeftTime_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtLeftTime_5() { return &____txtLeftTime_5; }
	inline void set__txtLeftTime_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtLeftTime_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtLeftTime_5), value);
	}

	inline static int32_t get_offset_of__imgAvatar_6() { return static_cast<int32_t>(offsetof(UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6, ____imgAvatar_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgAvatar_6() const { return ____imgAvatar_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgAvatar_6() { return &____imgAvatar_6; }
	inline void set__imgAvatar_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgAvatar_6 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatar_6), value);
	}

	inline static int32_t get_offset_of_m_nItemId_7() { return static_cast<int32_t>(offsetof(UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6, ___m_nItemId_7)); }
	inline int32_t get_m_nItemId_7() const { return ___m_nItemId_7; }
	inline int32_t* get_address_of_m_nItemId_7() { return &___m_nItemId_7; }
	inline void set_m_nItemId_7(int32_t value)
	{
		___m_nItemId_7 = value;
	}

	inline static int32_t get_offset_of_m_nValue0_8() { return static_cast<int32_t>(offsetof(UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6, ___m_nValue0_8)); }
	inline int32_t get_m_nValue0_8() const { return ___m_nValue0_8; }
	inline int32_t* get_address_of_m_nValue0_8() { return &___m_nValue0_8; }
	inline void set_m_nValue0_8(int32_t value)
	{
		___m_nValue0_8 = value;
	}

	inline static int32_t get_offset_of_m_nDuration_9() { return static_cast<int32_t>(offsetof(UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6, ___m_nDuration_9)); }
	inline int32_t get_m_nDuration_9() const { return ___m_nDuration_9; }
	inline int32_t* get_address_of_m_nDuration_9() { return &___m_nDuration_9; }
	inline void set_m_nDuration_9(int32_t value)
	{
		___m_nDuration_9 = value;
	}

	inline static int32_t get_offset_of_m_StartTime_10() { return static_cast<int32_t>(offsetof(UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6, ___m_StartTime_10)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_StartTime_10() const { return ___m_StartTime_10; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_StartTime_10() { return &___m_StartTime_10; }
	inline void set_m_StartTime_10(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_StartTime_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIITEMINBAG_T7F21A3C7BDBB6A681598F874C027E613F87CADE6_H
#ifndef UIMAINLAND_T6619E1A1161085203FE4F2B87F1CDC289144D7CF_H
#define UIMAINLAND_T6619E1A1161085203FE4F2B87F1CDC289144D7CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIMainLand
struct  UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UIBigMapTrackInfo[] UIMainLand::m_aryUITrakcs
	UIBigMapTrackInfoU5BU5D_t62B8E0E3DB4206E3629B57C37ED5293BCB2975E2* ___m_aryUITrakcs_4;
	// UnityEngine.GameObject UIMainLand::_containerTrakcs
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerTrakcs_5;
	// UnityEngine.UI.Image UIMainLand::_imgMain
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgMain_6;
	// UnityEngine.GameObject UIMainLand::_containerUnlockPrice
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerUnlockPrice_7;
	// UnityEngine.UI.Text UIMainLand::_txtUnlockPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtUnlockPrice_8;
	// UnityEngine.UI.Image UIMainLand::_imgUnlockIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgUnlockIcon_9;
	// System.Boolean UIMainLand::m_bLocked
	bool ___m_bLocked_10;
	// System.Int32 UIMainLand::m_nPlanetId
	int32_t ___m_nPlanetId_11;
	// UnityEngine.UI.Button UIMainLand::_btnClickMe
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnClickMe_12;
	// System.Boolean UIMainLand::m_bClickMe
	bool ___m_bClickMe_13;

public:
	inline static int32_t get_offset_of_m_aryUITrakcs_4() { return static_cast<int32_t>(offsetof(UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF, ___m_aryUITrakcs_4)); }
	inline UIBigMapTrackInfoU5BU5D_t62B8E0E3DB4206E3629B57C37ED5293BCB2975E2* get_m_aryUITrakcs_4() const { return ___m_aryUITrakcs_4; }
	inline UIBigMapTrackInfoU5BU5D_t62B8E0E3DB4206E3629B57C37ED5293BCB2975E2** get_address_of_m_aryUITrakcs_4() { return &___m_aryUITrakcs_4; }
	inline void set_m_aryUITrakcs_4(UIBigMapTrackInfoU5BU5D_t62B8E0E3DB4206E3629B57C37ED5293BCB2975E2* value)
	{
		___m_aryUITrakcs_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUITrakcs_4), value);
	}

	inline static int32_t get_offset_of__containerTrakcs_5() { return static_cast<int32_t>(offsetof(UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF, ____containerTrakcs_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerTrakcs_5() const { return ____containerTrakcs_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerTrakcs_5() { return &____containerTrakcs_5; }
	inline void set__containerTrakcs_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerTrakcs_5 = value;
		Il2CppCodeGenWriteBarrier((&____containerTrakcs_5), value);
	}

	inline static int32_t get_offset_of__imgMain_6() { return static_cast<int32_t>(offsetof(UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF, ____imgMain_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgMain_6() const { return ____imgMain_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgMain_6() { return &____imgMain_6; }
	inline void set__imgMain_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgMain_6 = value;
		Il2CppCodeGenWriteBarrier((&____imgMain_6), value);
	}

	inline static int32_t get_offset_of__containerUnlockPrice_7() { return static_cast<int32_t>(offsetof(UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF, ____containerUnlockPrice_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerUnlockPrice_7() const { return ____containerUnlockPrice_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerUnlockPrice_7() { return &____containerUnlockPrice_7; }
	inline void set__containerUnlockPrice_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerUnlockPrice_7 = value;
		Il2CppCodeGenWriteBarrier((&____containerUnlockPrice_7), value);
	}

	inline static int32_t get_offset_of__txtUnlockPrice_8() { return static_cast<int32_t>(offsetof(UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF, ____txtUnlockPrice_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtUnlockPrice_8() const { return ____txtUnlockPrice_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtUnlockPrice_8() { return &____txtUnlockPrice_8; }
	inline void set__txtUnlockPrice_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtUnlockPrice_8 = value;
		Il2CppCodeGenWriteBarrier((&____txtUnlockPrice_8), value);
	}

	inline static int32_t get_offset_of__imgUnlockIcon_9() { return static_cast<int32_t>(offsetof(UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF, ____imgUnlockIcon_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgUnlockIcon_9() const { return ____imgUnlockIcon_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgUnlockIcon_9() { return &____imgUnlockIcon_9; }
	inline void set__imgUnlockIcon_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgUnlockIcon_9 = value;
		Il2CppCodeGenWriteBarrier((&____imgUnlockIcon_9), value);
	}

	inline static int32_t get_offset_of_m_bLocked_10() { return static_cast<int32_t>(offsetof(UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF, ___m_bLocked_10)); }
	inline bool get_m_bLocked_10() const { return ___m_bLocked_10; }
	inline bool* get_address_of_m_bLocked_10() { return &___m_bLocked_10; }
	inline void set_m_bLocked_10(bool value)
	{
		___m_bLocked_10 = value;
	}

	inline static int32_t get_offset_of_m_nPlanetId_11() { return static_cast<int32_t>(offsetof(UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF, ___m_nPlanetId_11)); }
	inline int32_t get_m_nPlanetId_11() const { return ___m_nPlanetId_11; }
	inline int32_t* get_address_of_m_nPlanetId_11() { return &___m_nPlanetId_11; }
	inline void set_m_nPlanetId_11(int32_t value)
	{
		___m_nPlanetId_11 = value;
	}

	inline static int32_t get_offset_of__btnClickMe_12() { return static_cast<int32_t>(offsetof(UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF, ____btnClickMe_12)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnClickMe_12() const { return ____btnClickMe_12; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnClickMe_12() { return &____btnClickMe_12; }
	inline void set__btnClickMe_12(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnClickMe_12 = value;
		Il2CppCodeGenWriteBarrier((&____btnClickMe_12), value);
	}

	inline static int32_t get_offset_of_m_bClickMe_13() { return static_cast<int32_t>(offsetof(UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF, ___m_bClickMe_13)); }
	inline bool get_m_bClickMe_13() const { return ___m_bClickMe_13; }
	inline bool* get_address_of_m_bClickMe_13() { return &___m_bClickMe_13; }
	inline void set_m_bClickMe_13(bool value)
	{
		___m_bClickMe_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMAINLAND_T6619E1A1161085203FE4F2B87F1CDC289144D7CF_H
#ifndef UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#define UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject UIManager::_goStart
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goStart_5;
	// UnityEngine.GameObject UIManager::_goStart_Admin
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goStart_Admin_6;
	// UnityEngine.GameObject UIManager::_goEnd
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goEnd_7;
	// UnityEngine.GameObject UIManager::_goMiddleLeft
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goMiddleLeft_8;
	// UnityEngine.GameObject UIManager::_goMiddleRight
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goMiddleRight_9;
	// UnityEngine.GameObject[] UIManager::m_aryVariousUI
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryVariousUI_10;
	// UnityEngine.GameObject[] UIManager::m_aryVariousUI_Center
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryVariousUI_Center_11;
	// UnityEngine.GameObject[] UIManager::m_aryVariousUI_Bottom
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryVariousUI_Bottom_12;
	// UnityEngine.Vector3 UIManager::vecTempInitPosOfTopButtons
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempInitPosOfTopButtons_13;
	// UnityEngine.Vector3 UIManager::m_vecScreenBottom_World
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecScreenBottom_World_17;
	// UnityEngine.Vector3 UIManager::m_vecBottomButtonsInitPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecBottomButtonsInitPos_18;
	// UnityEngine.Vector3 UIManager::m_vecGainAndCollect
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecGainAndCollect_19;
	// UnityEngine.Vector3 UIManager::m_vecAdventureAndScience
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecAdventureAndScience_20;
	// UnityEngine.Vector3 UIManager::m_vecSubContainerForBigMap
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_vecSubContainerForBigMap_21;
	// System.Single UIManager::m_fOffsetToScreenBottom_World
	float ___m_fOffsetToScreenBottom_World_22;
	// UnityEngine.GameObject UIManager::_uiTestKnob
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiTestKnob_23;
	// UnityEngine.GameObject UIManager::_uiTopButtons
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiTopButtons_24;
	// UnityEngine.GameObject UIManager::_uiBottomButtons
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiBottomButtons_25;
	// UnityEngine.GameObject UIManager::_uiDPS
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiDPS_26;
	// UnityEngine.GameObject UIManager::_uiCPosButtons
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiCPosButtons_27;
	// UnityEngine.GameObject UIManager::_uiGainAndCollect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiGainAndCollect_28;
	// UnityEngine.GameObject UIManager::_uiAdventureAndScience
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiAdventureAndScience_29;
	// UnityEngine.GameObject UIManager::_uiSubContainerForBigMap
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiSubContainerForBigMap_30;
	// UnityEngine.GameObject UIManager::_uiSubContainerForBigMap_Content
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____uiSubContainerForBigMap_Content_31;
	// UnityEngine.Canvas UIManager::_canvasMain
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ____canvasMain_32;
	// UnityEngine.GameObject UIManager::_UI
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____UI_33;
	// UnityEngine.UI.CanvasScaler UIManager::_canvasScaler
	CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564 * ____canvasScaler_34;
	// UnityEngine.Vector2 UIManager::m_vecCoinFlySeesion0EndPosRangeX
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecCoinFlySeesion0EndPosRangeX_35;
	// UnityEngine.Vector2 UIManager::m_vecCoinFlySeesion0EndPosRangeY
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecCoinFlySeesion0EndPosRangeY_36;
	// UnityEngine.Vector2 UIManager::m_vecCoinFlyStartPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecCoinFlyStartPos_37;
	// UnityEngine.Vector2 UIManager::m_vecCoinFlyEndPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecCoinFlyEndPos_38;
	// System.Single UIManager::m_fSession0Time
	float ___m_fSession0Time_39;
	// System.Single UIManager::m_fSession1Time
	float ___m_fSession1Time_40;
	// UnityEngine.GameObject UIManager::_containerFlyingCoins
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerFlyingCoins_41;
	// UnityEngine.GameObject[] UIManager::m_arySomeCtrlsDueToXingKong
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_arySomeCtrlsDueToXingKong_42;
	// CFrameAnimationEffect UIManager::_goEffectClick
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * ____goEffectClick_43;
	// UnityEngine.Vector2[] UIManager::m_aryUICtrlPos_1920_1080
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_aryUICtrlPos_1920_1080_44;
	// UnityEngine.Vector2[] UIManager::m_aryUICtrlPos_2436_1125
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_aryUICtrlPos_2436_1125_45;
	// UnityEngine.GameObject[] UIManager::m_aryUICtrl
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryUICtrl_46;
	// UnityEngine.Vector2[] UIManager::m_aryResolution
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_aryResolution_47;

public:
	inline static int32_t get_offset_of__goStart_5() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____goStart_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goStart_5() const { return ____goStart_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goStart_5() { return &____goStart_5; }
	inline void set__goStart_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goStart_5 = value;
		Il2CppCodeGenWriteBarrier((&____goStart_5), value);
	}

	inline static int32_t get_offset_of__goStart_Admin_6() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____goStart_Admin_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goStart_Admin_6() const { return ____goStart_Admin_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goStart_Admin_6() { return &____goStart_Admin_6; }
	inline void set__goStart_Admin_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goStart_Admin_6 = value;
		Il2CppCodeGenWriteBarrier((&____goStart_Admin_6), value);
	}

	inline static int32_t get_offset_of__goEnd_7() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____goEnd_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goEnd_7() const { return ____goEnd_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goEnd_7() { return &____goEnd_7; }
	inline void set__goEnd_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goEnd_7 = value;
		Il2CppCodeGenWriteBarrier((&____goEnd_7), value);
	}

	inline static int32_t get_offset_of__goMiddleLeft_8() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____goMiddleLeft_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goMiddleLeft_8() const { return ____goMiddleLeft_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goMiddleLeft_8() { return &____goMiddleLeft_8; }
	inline void set__goMiddleLeft_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goMiddleLeft_8 = value;
		Il2CppCodeGenWriteBarrier((&____goMiddleLeft_8), value);
	}

	inline static int32_t get_offset_of__goMiddleRight_9() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____goMiddleRight_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goMiddleRight_9() const { return ____goMiddleRight_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goMiddleRight_9() { return &____goMiddleRight_9; }
	inline void set__goMiddleRight_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goMiddleRight_9 = value;
		Il2CppCodeGenWriteBarrier((&____goMiddleRight_9), value);
	}

	inline static int32_t get_offset_of_m_aryVariousUI_10() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryVariousUI_10)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryVariousUI_10() const { return ___m_aryVariousUI_10; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryVariousUI_10() { return &___m_aryVariousUI_10; }
	inline void set_m_aryVariousUI_10(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryVariousUI_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryVariousUI_10), value);
	}

	inline static int32_t get_offset_of_m_aryVariousUI_Center_11() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryVariousUI_Center_11)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryVariousUI_Center_11() const { return ___m_aryVariousUI_Center_11; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryVariousUI_Center_11() { return &___m_aryVariousUI_Center_11; }
	inline void set_m_aryVariousUI_Center_11(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryVariousUI_Center_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryVariousUI_Center_11), value);
	}

	inline static int32_t get_offset_of_m_aryVariousUI_Bottom_12() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryVariousUI_Bottom_12)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryVariousUI_Bottom_12() const { return ___m_aryVariousUI_Bottom_12; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryVariousUI_Bottom_12() { return &___m_aryVariousUI_Bottom_12; }
	inline void set_m_aryVariousUI_Bottom_12(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryVariousUI_Bottom_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryVariousUI_Bottom_12), value);
	}

	inline static int32_t get_offset_of_vecTempInitPosOfTopButtons_13() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___vecTempInitPosOfTopButtons_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempInitPosOfTopButtons_13() const { return ___vecTempInitPosOfTopButtons_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempInitPosOfTopButtons_13() { return &___vecTempInitPosOfTopButtons_13; }
	inline void set_vecTempInitPosOfTopButtons_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempInitPosOfTopButtons_13 = value;
	}

	inline static int32_t get_offset_of_m_vecScreenBottom_World_17() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecScreenBottom_World_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecScreenBottom_World_17() const { return ___m_vecScreenBottom_World_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecScreenBottom_World_17() { return &___m_vecScreenBottom_World_17; }
	inline void set_m_vecScreenBottom_World_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecScreenBottom_World_17 = value;
	}

	inline static int32_t get_offset_of_m_vecBottomButtonsInitPos_18() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecBottomButtonsInitPos_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecBottomButtonsInitPos_18() const { return ___m_vecBottomButtonsInitPos_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecBottomButtonsInitPos_18() { return &___m_vecBottomButtonsInitPos_18; }
	inline void set_m_vecBottomButtonsInitPos_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecBottomButtonsInitPos_18 = value;
	}

	inline static int32_t get_offset_of_m_vecGainAndCollect_19() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecGainAndCollect_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecGainAndCollect_19() const { return ___m_vecGainAndCollect_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecGainAndCollect_19() { return &___m_vecGainAndCollect_19; }
	inline void set_m_vecGainAndCollect_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecGainAndCollect_19 = value;
	}

	inline static int32_t get_offset_of_m_vecAdventureAndScience_20() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecAdventureAndScience_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecAdventureAndScience_20() const { return ___m_vecAdventureAndScience_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecAdventureAndScience_20() { return &___m_vecAdventureAndScience_20; }
	inline void set_m_vecAdventureAndScience_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecAdventureAndScience_20 = value;
	}

	inline static int32_t get_offset_of_m_vecSubContainerForBigMap_21() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecSubContainerForBigMap_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_vecSubContainerForBigMap_21() const { return ___m_vecSubContainerForBigMap_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_vecSubContainerForBigMap_21() { return &___m_vecSubContainerForBigMap_21; }
	inline void set_m_vecSubContainerForBigMap_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_vecSubContainerForBigMap_21 = value;
	}

	inline static int32_t get_offset_of_m_fOffsetToScreenBottom_World_22() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_fOffsetToScreenBottom_World_22)); }
	inline float get_m_fOffsetToScreenBottom_World_22() const { return ___m_fOffsetToScreenBottom_World_22; }
	inline float* get_address_of_m_fOffsetToScreenBottom_World_22() { return &___m_fOffsetToScreenBottom_World_22; }
	inline void set_m_fOffsetToScreenBottom_World_22(float value)
	{
		___m_fOffsetToScreenBottom_World_22 = value;
	}

	inline static int32_t get_offset_of__uiTestKnob_23() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiTestKnob_23)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiTestKnob_23() const { return ____uiTestKnob_23; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiTestKnob_23() { return &____uiTestKnob_23; }
	inline void set__uiTestKnob_23(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiTestKnob_23 = value;
		Il2CppCodeGenWriteBarrier((&____uiTestKnob_23), value);
	}

	inline static int32_t get_offset_of__uiTopButtons_24() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiTopButtons_24)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiTopButtons_24() const { return ____uiTopButtons_24; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiTopButtons_24() { return &____uiTopButtons_24; }
	inline void set__uiTopButtons_24(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiTopButtons_24 = value;
		Il2CppCodeGenWriteBarrier((&____uiTopButtons_24), value);
	}

	inline static int32_t get_offset_of__uiBottomButtons_25() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiBottomButtons_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiBottomButtons_25() const { return ____uiBottomButtons_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiBottomButtons_25() { return &____uiBottomButtons_25; }
	inline void set__uiBottomButtons_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiBottomButtons_25 = value;
		Il2CppCodeGenWriteBarrier((&____uiBottomButtons_25), value);
	}

	inline static int32_t get_offset_of__uiDPS_26() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiDPS_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiDPS_26() const { return ____uiDPS_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiDPS_26() { return &____uiDPS_26; }
	inline void set__uiDPS_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiDPS_26 = value;
		Il2CppCodeGenWriteBarrier((&____uiDPS_26), value);
	}

	inline static int32_t get_offset_of__uiCPosButtons_27() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiCPosButtons_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiCPosButtons_27() const { return ____uiCPosButtons_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiCPosButtons_27() { return &____uiCPosButtons_27; }
	inline void set__uiCPosButtons_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiCPosButtons_27 = value;
		Il2CppCodeGenWriteBarrier((&____uiCPosButtons_27), value);
	}

	inline static int32_t get_offset_of__uiGainAndCollect_28() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiGainAndCollect_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiGainAndCollect_28() const { return ____uiGainAndCollect_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiGainAndCollect_28() { return &____uiGainAndCollect_28; }
	inline void set__uiGainAndCollect_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiGainAndCollect_28 = value;
		Il2CppCodeGenWriteBarrier((&____uiGainAndCollect_28), value);
	}

	inline static int32_t get_offset_of__uiAdventureAndScience_29() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiAdventureAndScience_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiAdventureAndScience_29() const { return ____uiAdventureAndScience_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiAdventureAndScience_29() { return &____uiAdventureAndScience_29; }
	inline void set__uiAdventureAndScience_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiAdventureAndScience_29 = value;
		Il2CppCodeGenWriteBarrier((&____uiAdventureAndScience_29), value);
	}

	inline static int32_t get_offset_of__uiSubContainerForBigMap_30() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiSubContainerForBigMap_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiSubContainerForBigMap_30() const { return ____uiSubContainerForBigMap_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiSubContainerForBigMap_30() { return &____uiSubContainerForBigMap_30; }
	inline void set__uiSubContainerForBigMap_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiSubContainerForBigMap_30 = value;
		Il2CppCodeGenWriteBarrier((&____uiSubContainerForBigMap_30), value);
	}

	inline static int32_t get_offset_of__uiSubContainerForBigMap_Content_31() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____uiSubContainerForBigMap_Content_31)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__uiSubContainerForBigMap_Content_31() const { return ____uiSubContainerForBigMap_Content_31; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__uiSubContainerForBigMap_Content_31() { return &____uiSubContainerForBigMap_Content_31; }
	inline void set__uiSubContainerForBigMap_Content_31(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____uiSubContainerForBigMap_Content_31 = value;
		Il2CppCodeGenWriteBarrier((&____uiSubContainerForBigMap_Content_31), value);
	}

	inline static int32_t get_offset_of__canvasMain_32() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____canvasMain_32)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get__canvasMain_32() const { return ____canvasMain_32; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of__canvasMain_32() { return &____canvasMain_32; }
	inline void set__canvasMain_32(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		____canvasMain_32 = value;
		Il2CppCodeGenWriteBarrier((&____canvasMain_32), value);
	}

	inline static int32_t get_offset_of__UI_33() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____UI_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__UI_33() const { return ____UI_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__UI_33() { return &____UI_33; }
	inline void set__UI_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____UI_33 = value;
		Il2CppCodeGenWriteBarrier((&____UI_33), value);
	}

	inline static int32_t get_offset_of__canvasScaler_34() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____canvasScaler_34)); }
	inline CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564 * get__canvasScaler_34() const { return ____canvasScaler_34; }
	inline CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564 ** get_address_of__canvasScaler_34() { return &____canvasScaler_34; }
	inline void set__canvasScaler_34(CanvasScaler_t304BA6F47EDB7402EBA405DD36CA7D6ADF723564 * value)
	{
		____canvasScaler_34 = value;
		Il2CppCodeGenWriteBarrier((&____canvasScaler_34), value);
	}

	inline static int32_t get_offset_of_m_vecCoinFlySeesion0EndPosRangeX_35() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecCoinFlySeesion0EndPosRangeX_35)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecCoinFlySeesion0EndPosRangeX_35() const { return ___m_vecCoinFlySeesion0EndPosRangeX_35; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecCoinFlySeesion0EndPosRangeX_35() { return &___m_vecCoinFlySeesion0EndPosRangeX_35; }
	inline void set_m_vecCoinFlySeesion0EndPosRangeX_35(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecCoinFlySeesion0EndPosRangeX_35 = value;
	}

	inline static int32_t get_offset_of_m_vecCoinFlySeesion0EndPosRangeY_36() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecCoinFlySeesion0EndPosRangeY_36)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecCoinFlySeesion0EndPosRangeY_36() const { return ___m_vecCoinFlySeesion0EndPosRangeY_36; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecCoinFlySeesion0EndPosRangeY_36() { return &___m_vecCoinFlySeesion0EndPosRangeY_36; }
	inline void set_m_vecCoinFlySeesion0EndPosRangeY_36(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecCoinFlySeesion0EndPosRangeY_36 = value;
	}

	inline static int32_t get_offset_of_m_vecCoinFlyStartPos_37() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecCoinFlyStartPos_37)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecCoinFlyStartPos_37() const { return ___m_vecCoinFlyStartPos_37; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecCoinFlyStartPos_37() { return &___m_vecCoinFlyStartPos_37; }
	inline void set_m_vecCoinFlyStartPos_37(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecCoinFlyStartPos_37 = value;
	}

	inline static int32_t get_offset_of_m_vecCoinFlyEndPos_38() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_vecCoinFlyEndPos_38)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecCoinFlyEndPos_38() const { return ___m_vecCoinFlyEndPos_38; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecCoinFlyEndPos_38() { return &___m_vecCoinFlyEndPos_38; }
	inline void set_m_vecCoinFlyEndPos_38(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecCoinFlyEndPos_38 = value;
	}

	inline static int32_t get_offset_of_m_fSession0Time_39() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_fSession0Time_39)); }
	inline float get_m_fSession0Time_39() const { return ___m_fSession0Time_39; }
	inline float* get_address_of_m_fSession0Time_39() { return &___m_fSession0Time_39; }
	inline void set_m_fSession0Time_39(float value)
	{
		___m_fSession0Time_39 = value;
	}

	inline static int32_t get_offset_of_m_fSession1Time_40() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_fSession1Time_40)); }
	inline float get_m_fSession1Time_40() const { return ___m_fSession1Time_40; }
	inline float* get_address_of_m_fSession1Time_40() { return &___m_fSession1Time_40; }
	inline void set_m_fSession1Time_40(float value)
	{
		___m_fSession1Time_40 = value;
	}

	inline static int32_t get_offset_of__containerFlyingCoins_41() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____containerFlyingCoins_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerFlyingCoins_41() const { return ____containerFlyingCoins_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerFlyingCoins_41() { return &____containerFlyingCoins_41; }
	inline void set__containerFlyingCoins_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerFlyingCoins_41 = value;
		Il2CppCodeGenWriteBarrier((&____containerFlyingCoins_41), value);
	}

	inline static int32_t get_offset_of_m_arySomeCtrlsDueToXingKong_42() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_arySomeCtrlsDueToXingKong_42)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_arySomeCtrlsDueToXingKong_42() const { return ___m_arySomeCtrlsDueToXingKong_42; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_arySomeCtrlsDueToXingKong_42() { return &___m_arySomeCtrlsDueToXingKong_42; }
	inline void set_m_arySomeCtrlsDueToXingKong_42(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_arySomeCtrlsDueToXingKong_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySomeCtrlsDueToXingKong_42), value);
	}

	inline static int32_t get_offset_of__goEffectClick_43() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ____goEffectClick_43)); }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * get__goEffectClick_43() const { return ____goEffectClick_43; }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 ** get_address_of__goEffectClick_43() { return &____goEffectClick_43; }
	inline void set__goEffectClick_43(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * value)
	{
		____goEffectClick_43 = value;
		Il2CppCodeGenWriteBarrier((&____goEffectClick_43), value);
	}

	inline static int32_t get_offset_of_m_aryUICtrlPos_1920_1080_44() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryUICtrlPos_1920_1080_44)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_aryUICtrlPos_1920_1080_44() const { return ___m_aryUICtrlPos_1920_1080_44; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_aryUICtrlPos_1920_1080_44() { return &___m_aryUICtrlPos_1920_1080_44; }
	inline void set_m_aryUICtrlPos_1920_1080_44(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_aryUICtrlPos_1920_1080_44 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUICtrlPos_1920_1080_44), value);
	}

	inline static int32_t get_offset_of_m_aryUICtrlPos_2436_1125_45() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryUICtrlPos_2436_1125_45)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_aryUICtrlPos_2436_1125_45() const { return ___m_aryUICtrlPos_2436_1125_45; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_aryUICtrlPos_2436_1125_45() { return &___m_aryUICtrlPos_2436_1125_45; }
	inline void set_m_aryUICtrlPos_2436_1125_45(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_aryUICtrlPos_2436_1125_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUICtrlPos_2436_1125_45), value);
	}

	inline static int32_t get_offset_of_m_aryUICtrl_46() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryUICtrl_46)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryUICtrl_46() const { return ___m_aryUICtrl_46; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryUICtrl_46() { return &___m_aryUICtrl_46; }
	inline void set_m_aryUICtrl_46(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryUICtrl_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryUICtrl_46), value);
	}

	inline static int32_t get_offset_of_m_aryResolution_47() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_aryResolution_47)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_aryResolution_47() const { return ___m_aryResolution_47; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_aryResolution_47() { return &___m_aryResolution_47; }
	inline void set_m_aryResolution_47(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_aryResolution_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryResolution_47), value);
	}
};

struct UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields
{
public:
	// UIManager UIManager::s_Instance
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * ___s_Instance_4;
	// UnityEngine.Vector3 UIManager::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_15;
	// UnityEngine.Vector3 UIManager::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_16;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields, ___s_Instance_4)); }
	inline UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * get_s_Instance_4() const { return ___s_Instance_4; }
	inline UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_vecTempPos_15() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields, ___vecTempPos_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_15() const { return ___vecTempPos_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_15() { return &___vecTempPos_15; }
	inline void set_vecTempPos_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_15 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_16() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields, ___vecTempScale_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_16() const { return ___vecTempScale_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_16() { return &___vecTempScale_16; }
	inline void set_vecTempScale_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#ifndef UIMSGBOX_TFE0DEA91E7249781144314FA42C7521ADEC5E0AA_H
#define UIMSGBOX_TFE0DEA91E7249781144314FA42C7521ADEC5E0AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIMsgBox
struct  UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UIMsgBox/eCallBackEventType UIMsgBox::m_eType
	int32_t ___m_eType_4;
	// UnityEngine.GameObject UIMsgBox::_goContainerMain
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goContainerMain_6;
	// UnityEngine.GameObject UIMsgBox::_containerOnlyOk
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerOnlyOk_7;
	// UnityEngine.GameObject UIMsgBox::_containerYesNo
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerYesNo_8;
	// UnityEngine.UI.Text UIMsgBox::_txtMsgOnlyOk
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtMsgOnlyOk_9;
	// UnityEngine.UI.Text UIMsgBox::_txtMsgYesNo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtMsgYesNo_10;
	// System.Single UIMsgBox::m_fTimeElapse
	float ___m_fTimeElapse_11;
	// ResearchCounter UIMsgBox::_BoundResearchCounter
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * ____BoundResearchCounter_12;

public:
	inline static int32_t get_offset_of_m_eType_4() { return static_cast<int32_t>(offsetof(UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA, ___m_eType_4)); }
	inline int32_t get_m_eType_4() const { return ___m_eType_4; }
	inline int32_t* get_address_of_m_eType_4() { return &___m_eType_4; }
	inline void set_m_eType_4(int32_t value)
	{
		___m_eType_4 = value;
	}

	inline static int32_t get_offset_of__goContainerMain_6() { return static_cast<int32_t>(offsetof(UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA, ____goContainerMain_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goContainerMain_6() const { return ____goContainerMain_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goContainerMain_6() { return &____goContainerMain_6; }
	inline void set__goContainerMain_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goContainerMain_6 = value;
		Il2CppCodeGenWriteBarrier((&____goContainerMain_6), value);
	}

	inline static int32_t get_offset_of__containerOnlyOk_7() { return static_cast<int32_t>(offsetof(UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA, ____containerOnlyOk_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerOnlyOk_7() const { return ____containerOnlyOk_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerOnlyOk_7() { return &____containerOnlyOk_7; }
	inline void set__containerOnlyOk_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerOnlyOk_7 = value;
		Il2CppCodeGenWriteBarrier((&____containerOnlyOk_7), value);
	}

	inline static int32_t get_offset_of__containerYesNo_8() { return static_cast<int32_t>(offsetof(UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA, ____containerYesNo_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerYesNo_8() const { return ____containerYesNo_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerYesNo_8() { return &____containerYesNo_8; }
	inline void set__containerYesNo_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerYesNo_8 = value;
		Il2CppCodeGenWriteBarrier((&____containerYesNo_8), value);
	}

	inline static int32_t get_offset_of__txtMsgOnlyOk_9() { return static_cast<int32_t>(offsetof(UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA, ____txtMsgOnlyOk_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtMsgOnlyOk_9() const { return ____txtMsgOnlyOk_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtMsgOnlyOk_9() { return &____txtMsgOnlyOk_9; }
	inline void set__txtMsgOnlyOk_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtMsgOnlyOk_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtMsgOnlyOk_9), value);
	}

	inline static int32_t get_offset_of__txtMsgYesNo_10() { return static_cast<int32_t>(offsetof(UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA, ____txtMsgYesNo_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtMsgYesNo_10() const { return ____txtMsgYesNo_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtMsgYesNo_10() { return &____txtMsgYesNo_10; }
	inline void set__txtMsgYesNo_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtMsgYesNo_10 = value;
		Il2CppCodeGenWriteBarrier((&____txtMsgYesNo_10), value);
	}

	inline static int32_t get_offset_of_m_fTimeElapse_11() { return static_cast<int32_t>(offsetof(UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA, ___m_fTimeElapse_11)); }
	inline float get_m_fTimeElapse_11() const { return ___m_fTimeElapse_11; }
	inline float* get_address_of_m_fTimeElapse_11() { return &___m_fTimeElapse_11; }
	inline void set_m_fTimeElapse_11(float value)
	{
		___m_fTimeElapse_11 = value;
	}

	inline static int32_t get_offset_of__BoundResearchCounter_12() { return static_cast<int32_t>(offsetof(UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA, ____BoundResearchCounter_12)); }
	inline ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * get__BoundResearchCounter_12() const { return ____BoundResearchCounter_12; }
	inline ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD ** get_address_of__BoundResearchCounter_12() { return &____BoundResearchCounter_12; }
	inline void set__BoundResearchCounter_12(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * value)
	{
		____BoundResearchCounter_12 = value;
		Il2CppCodeGenWriteBarrier((&____BoundResearchCounter_12), value);
	}
};

struct UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA_StaticFields
{
public:
	// UIMsgBox UIMsgBox::s_Instance
	UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA * ___s_Instance_5;

public:
	inline static int32_t get_offset_of_s_Instance_5() { return static_cast<int32_t>(offsetof(UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA_StaticFields, ___s_Instance_5)); }
	inline UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA * get_s_Instance_5() const { return ___s_Instance_5; }
	inline UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA ** get_address_of_s_Instance_5() { return &___s_Instance_5; }
	inline void set_s_Instance_5(UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA * value)
	{
		___s_Instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMSGBOX_TFE0DEA91E7249781144314FA42C7521ADEC5E0AA_H
#ifndef UIPLANET_TA72D11E981757CAB07BA519432A793E2BF92E38A_H
#define UIPLANET_TA72D11E981757CAB07BA519432A793E2BF92E38A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPlanet
struct  UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject UIPlanet::_lock
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lock_4;
	// System.Boolean UIPlanet::m_bLocked
	bool ___m_bLocked_5;
	// System.Int32 UIPlanet::m_nPlanetId
	int32_t ___m_nPlanetId_6;
	// UIPlanet/eUIPlanetType UIPlanet::m_ePlanetType
	int32_t ___m_ePlanetType_7;
	// UnityEngine.UI.Text UIPlanet::_txtUnlockPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtUnlockPrice_8;
	// UnityEngine.UI.Text UIPlanet::_txtPlanetName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPlanetName_9;
	// UnityEngine.GameObject UIPlanet::_goLock
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goLock_10;
	// UnityEngine.GameObject UIPlanet::_goPrice
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goPrice_11;
	// UnityEngine.GameObject UIPlanet::_goButtonMain
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goButtonMain_12;
	// BaseRotate UIPlanet::_BaseRotate
	BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377 * ____BaseRotate_13;

public:
	inline static int32_t get_offset_of__lock_4() { return static_cast<int32_t>(offsetof(UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A, ____lock_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lock_4() const { return ____lock_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lock_4() { return &____lock_4; }
	inline void set__lock_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lock_4 = value;
		Il2CppCodeGenWriteBarrier((&____lock_4), value);
	}

	inline static int32_t get_offset_of_m_bLocked_5() { return static_cast<int32_t>(offsetof(UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A, ___m_bLocked_5)); }
	inline bool get_m_bLocked_5() const { return ___m_bLocked_5; }
	inline bool* get_address_of_m_bLocked_5() { return &___m_bLocked_5; }
	inline void set_m_bLocked_5(bool value)
	{
		___m_bLocked_5 = value;
	}

	inline static int32_t get_offset_of_m_nPlanetId_6() { return static_cast<int32_t>(offsetof(UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A, ___m_nPlanetId_6)); }
	inline int32_t get_m_nPlanetId_6() const { return ___m_nPlanetId_6; }
	inline int32_t* get_address_of_m_nPlanetId_6() { return &___m_nPlanetId_6; }
	inline void set_m_nPlanetId_6(int32_t value)
	{
		___m_nPlanetId_6 = value;
	}

	inline static int32_t get_offset_of_m_ePlanetType_7() { return static_cast<int32_t>(offsetof(UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A, ___m_ePlanetType_7)); }
	inline int32_t get_m_ePlanetType_7() const { return ___m_ePlanetType_7; }
	inline int32_t* get_address_of_m_ePlanetType_7() { return &___m_ePlanetType_7; }
	inline void set_m_ePlanetType_7(int32_t value)
	{
		___m_ePlanetType_7 = value;
	}

	inline static int32_t get_offset_of__txtUnlockPrice_8() { return static_cast<int32_t>(offsetof(UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A, ____txtUnlockPrice_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtUnlockPrice_8() const { return ____txtUnlockPrice_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtUnlockPrice_8() { return &____txtUnlockPrice_8; }
	inline void set__txtUnlockPrice_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtUnlockPrice_8 = value;
		Il2CppCodeGenWriteBarrier((&____txtUnlockPrice_8), value);
	}

	inline static int32_t get_offset_of__txtPlanetName_9() { return static_cast<int32_t>(offsetof(UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A, ____txtPlanetName_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPlanetName_9() const { return ____txtPlanetName_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPlanetName_9() { return &____txtPlanetName_9; }
	inline void set__txtPlanetName_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPlanetName_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlanetName_9), value);
	}

	inline static int32_t get_offset_of__goLock_10() { return static_cast<int32_t>(offsetof(UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A, ____goLock_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goLock_10() const { return ____goLock_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goLock_10() { return &____goLock_10; }
	inline void set__goLock_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goLock_10 = value;
		Il2CppCodeGenWriteBarrier((&____goLock_10), value);
	}

	inline static int32_t get_offset_of__goPrice_11() { return static_cast<int32_t>(offsetof(UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A, ____goPrice_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goPrice_11() const { return ____goPrice_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goPrice_11() { return &____goPrice_11; }
	inline void set__goPrice_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goPrice_11 = value;
		Il2CppCodeGenWriteBarrier((&____goPrice_11), value);
	}

	inline static int32_t get_offset_of__goButtonMain_12() { return static_cast<int32_t>(offsetof(UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A, ____goButtonMain_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goButtonMain_12() const { return ____goButtonMain_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goButtonMain_12() { return &____goButtonMain_12; }
	inline void set__goButtonMain_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goButtonMain_12 = value;
		Il2CppCodeGenWriteBarrier((&____goButtonMain_12), value);
	}

	inline static int32_t get_offset_of__BaseRotate_13() { return static_cast<int32_t>(offsetof(UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A, ____BaseRotate_13)); }
	inline BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377 * get__BaseRotate_13() const { return ____BaseRotate_13; }
	inline BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377 ** get_address_of__BaseRotate_13() { return &____BaseRotate_13; }
	inline void set__BaseRotate_13(BaseRotate_t66CD927C20FD5B40C08A788B6578D220A0B11377 * value)
	{
		____BaseRotate_13 = value;
		Il2CppCodeGenWriteBarrier((&____BaseRotate_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPLANET_TA72D11E981757CAB07BA519432A793E2BF92E38A_H
#ifndef UIPRESTIGE_T2BDF11E128EBC48DB6215DE038F0501D99AC566C_H
#define UIPRESTIGE_T2BDF11E128EBC48DB6215DE038F0501D99AC566C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPrestige
struct  UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Color UIPrestige::m_colorCoinNotEnough
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorCoinNotEnough_5;
	// UnityEngine.Color UIPrestige::m_colorGray
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_colorGray_6;
	// UnityEngine.UI.Text UIPrestige::_txtCurGainTimes
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurGainTimes_7;
	// UnityEngine.UI.Text UIPrestige::_txtNextGainTimes
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtNextGainTimes_8;
	// UnityEngine.UI.Text UIPrestige::_txtCost
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCost_9;
	// UnityEngine.UI.Button UIPrestige::_btnDoPrestige
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnDoPrestige_10;
	// UnityEngine.UI.Image UIPrestige::_imgDoPrestige_Image
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgDoPrestige_Image_11;
	// UnityEngine.UI.Text UIPrestige::_txtDoPrestige_Text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDoPrestige_Text_12;
	// UnityEngine.UI.Text UIPrestige::_txtCoinKeep
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCoinKeep_13;
	// UnityEngine.UI.Image UIPrestige::_imgCostCoinIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCostCoinIcon_14;
	// UnityEngine.UI.Image UIPrestige::_imgKeepCoinIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgKeepCoinIcon_15;
	// UnityEngine.UI.Text UIPrestige::_txtKeepSkillPoint
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtKeepSkillPoint_16;
	// UnityEngine.UI.Image UIPrestige::_imgKeepSkillPointIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgKeepSkillPointIcon_17;
	// UnityEngine.UI.Text UIPrestige::_txtKeepDiamond
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtKeepDiamond_18;
	// UIStars UIPrestige::_starsCur
	UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * ____starsCur_19;
	// UIStars UIPrestige::_starsNext
	UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * ____starsNext_20;
	// UnityEngine.GameObject UIPrestige::m_containerCostAndKeep
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerCostAndKeep_21;
	// System.Boolean UIPrestige::m_bCoinNotEnough
	bool ___m_bCoinNotEnough_22;
	// System.Double UIPrestige::m_dRealPrestigeCoin
	double ___m_dRealPrestigeCoin_23;

public:
	inline static int32_t get_offset_of_m_colorCoinNotEnough_5() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ___m_colorCoinNotEnough_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorCoinNotEnough_5() const { return ___m_colorCoinNotEnough_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorCoinNotEnough_5() { return &___m_colorCoinNotEnough_5; }
	inline void set_m_colorCoinNotEnough_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorCoinNotEnough_5 = value;
	}

	inline static int32_t get_offset_of_m_colorGray_6() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ___m_colorGray_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_colorGray_6() const { return ___m_colorGray_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_colorGray_6() { return &___m_colorGray_6; }
	inline void set_m_colorGray_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_colorGray_6 = value;
	}

	inline static int32_t get_offset_of__txtCurGainTimes_7() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____txtCurGainTimes_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurGainTimes_7() const { return ____txtCurGainTimes_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurGainTimes_7() { return &____txtCurGainTimes_7; }
	inline void set__txtCurGainTimes_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurGainTimes_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurGainTimes_7), value);
	}

	inline static int32_t get_offset_of__txtNextGainTimes_8() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____txtNextGainTimes_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtNextGainTimes_8() const { return ____txtNextGainTimes_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtNextGainTimes_8() { return &____txtNextGainTimes_8; }
	inline void set__txtNextGainTimes_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtNextGainTimes_8 = value;
		Il2CppCodeGenWriteBarrier((&____txtNextGainTimes_8), value);
	}

	inline static int32_t get_offset_of__txtCost_9() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____txtCost_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCost_9() const { return ____txtCost_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCost_9() { return &____txtCost_9; }
	inline void set__txtCost_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCost_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtCost_9), value);
	}

	inline static int32_t get_offset_of__btnDoPrestige_10() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____btnDoPrestige_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnDoPrestige_10() const { return ____btnDoPrestige_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnDoPrestige_10() { return &____btnDoPrestige_10; }
	inline void set__btnDoPrestige_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnDoPrestige_10 = value;
		Il2CppCodeGenWriteBarrier((&____btnDoPrestige_10), value);
	}

	inline static int32_t get_offset_of__imgDoPrestige_Image_11() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____imgDoPrestige_Image_11)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgDoPrestige_Image_11() const { return ____imgDoPrestige_Image_11; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgDoPrestige_Image_11() { return &____imgDoPrestige_Image_11; }
	inline void set__imgDoPrestige_Image_11(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgDoPrestige_Image_11 = value;
		Il2CppCodeGenWriteBarrier((&____imgDoPrestige_Image_11), value);
	}

	inline static int32_t get_offset_of__txtDoPrestige_Text_12() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____txtDoPrestige_Text_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDoPrestige_Text_12() const { return ____txtDoPrestige_Text_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDoPrestige_Text_12() { return &____txtDoPrestige_Text_12; }
	inline void set__txtDoPrestige_Text_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDoPrestige_Text_12 = value;
		Il2CppCodeGenWriteBarrier((&____txtDoPrestige_Text_12), value);
	}

	inline static int32_t get_offset_of__txtCoinKeep_13() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____txtCoinKeep_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCoinKeep_13() const { return ____txtCoinKeep_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCoinKeep_13() { return &____txtCoinKeep_13; }
	inline void set__txtCoinKeep_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCoinKeep_13 = value;
		Il2CppCodeGenWriteBarrier((&____txtCoinKeep_13), value);
	}

	inline static int32_t get_offset_of__imgCostCoinIcon_14() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____imgCostCoinIcon_14)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCostCoinIcon_14() const { return ____imgCostCoinIcon_14; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCostCoinIcon_14() { return &____imgCostCoinIcon_14; }
	inline void set__imgCostCoinIcon_14(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCostCoinIcon_14 = value;
		Il2CppCodeGenWriteBarrier((&____imgCostCoinIcon_14), value);
	}

	inline static int32_t get_offset_of__imgKeepCoinIcon_15() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____imgKeepCoinIcon_15)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgKeepCoinIcon_15() const { return ____imgKeepCoinIcon_15; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgKeepCoinIcon_15() { return &____imgKeepCoinIcon_15; }
	inline void set__imgKeepCoinIcon_15(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgKeepCoinIcon_15 = value;
		Il2CppCodeGenWriteBarrier((&____imgKeepCoinIcon_15), value);
	}

	inline static int32_t get_offset_of__txtKeepSkillPoint_16() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____txtKeepSkillPoint_16)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtKeepSkillPoint_16() const { return ____txtKeepSkillPoint_16; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtKeepSkillPoint_16() { return &____txtKeepSkillPoint_16; }
	inline void set__txtKeepSkillPoint_16(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtKeepSkillPoint_16 = value;
		Il2CppCodeGenWriteBarrier((&____txtKeepSkillPoint_16), value);
	}

	inline static int32_t get_offset_of__imgKeepSkillPointIcon_17() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____imgKeepSkillPointIcon_17)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgKeepSkillPointIcon_17() const { return ____imgKeepSkillPointIcon_17; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgKeepSkillPointIcon_17() { return &____imgKeepSkillPointIcon_17; }
	inline void set__imgKeepSkillPointIcon_17(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgKeepSkillPointIcon_17 = value;
		Il2CppCodeGenWriteBarrier((&____imgKeepSkillPointIcon_17), value);
	}

	inline static int32_t get_offset_of__txtKeepDiamond_18() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____txtKeepDiamond_18)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtKeepDiamond_18() const { return ____txtKeepDiamond_18; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtKeepDiamond_18() { return &____txtKeepDiamond_18; }
	inline void set__txtKeepDiamond_18(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtKeepDiamond_18 = value;
		Il2CppCodeGenWriteBarrier((&____txtKeepDiamond_18), value);
	}

	inline static int32_t get_offset_of__starsCur_19() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____starsCur_19)); }
	inline UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * get__starsCur_19() const { return ____starsCur_19; }
	inline UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B ** get_address_of__starsCur_19() { return &____starsCur_19; }
	inline void set__starsCur_19(UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * value)
	{
		____starsCur_19 = value;
		Il2CppCodeGenWriteBarrier((&____starsCur_19), value);
	}

	inline static int32_t get_offset_of__starsNext_20() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ____starsNext_20)); }
	inline UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * get__starsNext_20() const { return ____starsNext_20; }
	inline UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B ** get_address_of__starsNext_20() { return &____starsNext_20; }
	inline void set__starsNext_20(UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B * value)
	{
		____starsNext_20 = value;
		Il2CppCodeGenWriteBarrier((&____starsNext_20), value);
	}

	inline static int32_t get_offset_of_m_containerCostAndKeep_21() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ___m_containerCostAndKeep_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerCostAndKeep_21() const { return ___m_containerCostAndKeep_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerCostAndKeep_21() { return &___m_containerCostAndKeep_21; }
	inline void set_m_containerCostAndKeep_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerCostAndKeep_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerCostAndKeep_21), value);
	}

	inline static int32_t get_offset_of_m_bCoinNotEnough_22() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ___m_bCoinNotEnough_22)); }
	inline bool get_m_bCoinNotEnough_22() const { return ___m_bCoinNotEnough_22; }
	inline bool* get_address_of_m_bCoinNotEnough_22() { return &___m_bCoinNotEnough_22; }
	inline void set_m_bCoinNotEnough_22(bool value)
	{
		___m_bCoinNotEnough_22 = value;
	}

	inline static int32_t get_offset_of_m_dRealPrestigeCoin_23() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C, ___m_dRealPrestigeCoin_23)); }
	inline double get_m_dRealPrestigeCoin_23() const { return ___m_dRealPrestigeCoin_23; }
	inline double* get_address_of_m_dRealPrestigeCoin_23() { return &___m_dRealPrestigeCoin_23; }
	inline void set_m_dRealPrestigeCoin_23(double value)
	{
		___m_dRealPrestigeCoin_23 = value;
	}
};

struct UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C_StaticFields
{
public:
	// UIPrestige UIPrestige::s_Instance
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C_StaticFields, ___s_Instance_4)); }
	inline UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C * get_s_Instance_4() const { return ___s_Instance_4; }
	inline UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPRESTIGE_T2BDF11E128EBC48DB6215DE038F0501D99AC566C_H
#ifndef UIPROGRESSBAR_T2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96_H
#define UIPROGRESSBAR_T2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIProgressBar
struct  UIProgressBar_t2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image UIProgressBar::_imgBar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgBar_4;
	// UnityEngine.UI.Text UIProgressBar::_txtContent
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtContent_5;

public:
	inline static int32_t get_offset_of__imgBar_4() { return static_cast<int32_t>(offsetof(UIProgressBar_t2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96, ____imgBar_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgBar_4() const { return ____imgBar_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgBar_4() { return &____imgBar_4; }
	inline void set__imgBar_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgBar_4 = value;
		Il2CppCodeGenWriteBarrier((&____imgBar_4), value);
	}

	inline static int32_t get_offset_of__txtContent_5() { return static_cast<int32_t>(offsetof(UIProgressBar_t2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96, ____txtContent_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtContent_5() const { return ____txtContent_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtContent_5() { return &____txtContent_5; }
	inline void set__txtContent_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtContent_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtContent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPROGRESSBAR_T2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96_H
#ifndef UISHOPPINANDITEMCOUNTER_TC16F00AAEA618662EE5AD9105A70A000C6AB9169_H
#define UISHOPPINANDITEMCOUNTER_TC16F00AAEA618662EE5AD9105A70A000C6AB9169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIShoppinAndItemCounter
struct  UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button UIShoppinAndItemCounter::_btnBuy
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnBuy_6;
	// UnityEngine.UI.Button UIShoppinAndItemCounter::_btnUse
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnUse_7;
	// UnityEngine.GameObject UIShoppinAndItemCounter::_containerOneButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerOneButton_8;
	// UnityEngine.GameObject UIShoppinAndItemCounter::_containerTwoButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerTwoButton_9;
	// UnityEngine.GameObject UIShoppinAndItemCounter::_containerItemUsingStatus
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerItemUsingStatus_10;
	// UnityEngine.UI.Image UIShoppinAndItemCounter::_imgCoinIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCoinIcon_11;
	// UnityEngine.UI.Image UIShoppinAndItemCounter::_imgGreenCashIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgGreenCashIcon_12;
	// UnityEngine.UI.Text UIShoppinAndItemCounter::_txtLeftTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtLeftTime_13;
	// UnityEngine.UI.Text UIShoppinAndItemCounter::_txtDuration
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDuration_14;
	// UnityEngine.UI.Text UIShoppinAndItemCounter::_txtValue
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtValue_15;
	// UnityEngine.UI.Text UIShoppinAndItemCounter::_txtNum
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtNum_16;
	// UnityEngine.UI.Text[] UIShoppinAndItemCounter::_aryTxtPriceDiamond
	TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* ____aryTxtPriceDiamond_17;
	// UnityEngine.UI.Text UIShoppinAndItemCounter::_txtPriceCoin
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPriceCoin_18;
	// UnityEngine.UI.Text UIShoppinAndItemCounter::_txtPriceDiamond
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPriceDiamond_19;
	// UnityEngine.UI.Image UIShoppinAndItemCounter::_imgAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgAvatar_20;
	// UnityEngine.UI.Image UIShoppinAndItemCounter::_imgProgressBarFill
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgProgressBarFill_21;
	// System.Single UIShoppinAndItemCounter::m_fValue
	float ___m_fValue_22;
	// System.Double UIShoppinAndItemCounter::m_dCoinPrice
	double ___m_dCoinPrice_23;
	// System.Int32 UIShoppinAndItemCounter::m_nDiamondPrice
	int32_t ___m_nDiamondPrice_24;
	// System.Int32 UIShoppinAndItemCounter::m_nDuration
	int32_t ___m_nDuration_25;
	// System.Int32 UIShoppinAndItemCounter::m_nItemType
	int32_t ___m_nItemType_26;
	// System.Int32 UIShoppinAndItemCounter::m_nItemSubType
	int32_t ___m_nItemSubType_27;
	// System.String UIShoppinAndItemCounter::m_szFuncDest
	String_t* ___m_szFuncDest_28;
	// System.Boolean UIShoppinAndItemCounter::m_bItemInBag
	bool ___m_bItemInBag_29;
	// System.Int32 UIShoppinAndItemCounter::m_nNum
	int32_t ___m_nNum_30;
	// System.Int32 UIShoppinAndItemCounter::m_nItemId
	int32_t ___m_nItemId_31;
	// System.DateTime UIShoppinAndItemCounter::m_dateStartTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dateStartTime_32;
	// System.Boolean UIShoppinAndItemCounter::m_bWorking
	bool ___m_bWorking_33;
	// ShoppinMall/sShoppingMallItemConfig UIShoppinAndItemCounter::m_Config
	sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  ___m_Config_34;
	// ShoppinMall/eItemType UIShoppinAndItemCounter::m_eItemType
	int32_t ___m_eItemType_35;
	// System.Single UIShoppinAndItemCounter::m_fTimeElapse
	float ___m_fTimeElapse_36;

public:
	inline static int32_t get_offset_of__btnBuy_6() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____btnBuy_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnBuy_6() const { return ____btnBuy_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnBuy_6() { return &____btnBuy_6; }
	inline void set__btnBuy_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnBuy_6 = value;
		Il2CppCodeGenWriteBarrier((&____btnBuy_6), value);
	}

	inline static int32_t get_offset_of__btnUse_7() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____btnUse_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnUse_7() const { return ____btnUse_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnUse_7() { return &____btnUse_7; }
	inline void set__btnUse_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnUse_7 = value;
		Il2CppCodeGenWriteBarrier((&____btnUse_7), value);
	}

	inline static int32_t get_offset_of__containerOneButton_8() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____containerOneButton_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerOneButton_8() const { return ____containerOneButton_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerOneButton_8() { return &____containerOneButton_8; }
	inline void set__containerOneButton_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerOneButton_8 = value;
		Il2CppCodeGenWriteBarrier((&____containerOneButton_8), value);
	}

	inline static int32_t get_offset_of__containerTwoButton_9() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____containerTwoButton_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerTwoButton_9() const { return ____containerTwoButton_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerTwoButton_9() { return &____containerTwoButton_9; }
	inline void set__containerTwoButton_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerTwoButton_9 = value;
		Il2CppCodeGenWriteBarrier((&____containerTwoButton_9), value);
	}

	inline static int32_t get_offset_of__containerItemUsingStatus_10() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____containerItemUsingStatus_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerItemUsingStatus_10() const { return ____containerItemUsingStatus_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerItemUsingStatus_10() { return &____containerItemUsingStatus_10; }
	inline void set__containerItemUsingStatus_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerItemUsingStatus_10 = value;
		Il2CppCodeGenWriteBarrier((&____containerItemUsingStatus_10), value);
	}

	inline static int32_t get_offset_of__imgCoinIcon_11() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____imgCoinIcon_11)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCoinIcon_11() const { return ____imgCoinIcon_11; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCoinIcon_11() { return &____imgCoinIcon_11; }
	inline void set__imgCoinIcon_11(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCoinIcon_11 = value;
		Il2CppCodeGenWriteBarrier((&____imgCoinIcon_11), value);
	}

	inline static int32_t get_offset_of__imgGreenCashIcon_12() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____imgGreenCashIcon_12)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgGreenCashIcon_12() const { return ____imgGreenCashIcon_12; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgGreenCashIcon_12() { return &____imgGreenCashIcon_12; }
	inline void set__imgGreenCashIcon_12(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgGreenCashIcon_12 = value;
		Il2CppCodeGenWriteBarrier((&____imgGreenCashIcon_12), value);
	}

	inline static int32_t get_offset_of__txtLeftTime_13() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____txtLeftTime_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtLeftTime_13() const { return ____txtLeftTime_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtLeftTime_13() { return &____txtLeftTime_13; }
	inline void set__txtLeftTime_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtLeftTime_13 = value;
		Il2CppCodeGenWriteBarrier((&____txtLeftTime_13), value);
	}

	inline static int32_t get_offset_of__txtDuration_14() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____txtDuration_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDuration_14() const { return ____txtDuration_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDuration_14() { return &____txtDuration_14; }
	inline void set__txtDuration_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDuration_14 = value;
		Il2CppCodeGenWriteBarrier((&____txtDuration_14), value);
	}

	inline static int32_t get_offset_of__txtValue_15() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____txtValue_15)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtValue_15() const { return ____txtValue_15; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtValue_15() { return &____txtValue_15; }
	inline void set__txtValue_15(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtValue_15 = value;
		Il2CppCodeGenWriteBarrier((&____txtValue_15), value);
	}

	inline static int32_t get_offset_of__txtNum_16() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____txtNum_16)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtNum_16() const { return ____txtNum_16; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtNum_16() { return &____txtNum_16; }
	inline void set__txtNum_16(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtNum_16 = value;
		Il2CppCodeGenWriteBarrier((&____txtNum_16), value);
	}

	inline static int32_t get_offset_of__aryTxtPriceDiamond_17() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____aryTxtPriceDiamond_17)); }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* get__aryTxtPriceDiamond_17() const { return ____aryTxtPriceDiamond_17; }
	inline TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188** get_address_of__aryTxtPriceDiamond_17() { return &____aryTxtPriceDiamond_17; }
	inline void set__aryTxtPriceDiamond_17(TextU5BU5D_t8855BE16E29F8F98FBC7FDDADA9705F9259A1188* value)
	{
		____aryTxtPriceDiamond_17 = value;
		Il2CppCodeGenWriteBarrier((&____aryTxtPriceDiamond_17), value);
	}

	inline static int32_t get_offset_of__txtPriceCoin_18() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____txtPriceCoin_18)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPriceCoin_18() const { return ____txtPriceCoin_18; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPriceCoin_18() { return &____txtPriceCoin_18; }
	inline void set__txtPriceCoin_18(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPriceCoin_18 = value;
		Il2CppCodeGenWriteBarrier((&____txtPriceCoin_18), value);
	}

	inline static int32_t get_offset_of__txtPriceDiamond_19() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____txtPriceDiamond_19)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPriceDiamond_19() const { return ____txtPriceDiamond_19; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPriceDiamond_19() { return &____txtPriceDiamond_19; }
	inline void set__txtPriceDiamond_19(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPriceDiamond_19 = value;
		Il2CppCodeGenWriteBarrier((&____txtPriceDiamond_19), value);
	}

	inline static int32_t get_offset_of__imgAvatar_20() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____imgAvatar_20)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgAvatar_20() const { return ____imgAvatar_20; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgAvatar_20() { return &____imgAvatar_20; }
	inline void set__imgAvatar_20(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgAvatar_20 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatar_20), value);
	}

	inline static int32_t get_offset_of__imgProgressBarFill_21() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ____imgProgressBarFill_21)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgProgressBarFill_21() const { return ____imgProgressBarFill_21; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgProgressBarFill_21() { return &____imgProgressBarFill_21; }
	inline void set__imgProgressBarFill_21(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgProgressBarFill_21 = value;
		Il2CppCodeGenWriteBarrier((&____imgProgressBarFill_21), value);
	}

	inline static int32_t get_offset_of_m_fValue_22() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_fValue_22)); }
	inline float get_m_fValue_22() const { return ___m_fValue_22; }
	inline float* get_address_of_m_fValue_22() { return &___m_fValue_22; }
	inline void set_m_fValue_22(float value)
	{
		___m_fValue_22 = value;
	}

	inline static int32_t get_offset_of_m_dCoinPrice_23() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_dCoinPrice_23)); }
	inline double get_m_dCoinPrice_23() const { return ___m_dCoinPrice_23; }
	inline double* get_address_of_m_dCoinPrice_23() { return &___m_dCoinPrice_23; }
	inline void set_m_dCoinPrice_23(double value)
	{
		___m_dCoinPrice_23 = value;
	}

	inline static int32_t get_offset_of_m_nDiamondPrice_24() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_nDiamondPrice_24)); }
	inline int32_t get_m_nDiamondPrice_24() const { return ___m_nDiamondPrice_24; }
	inline int32_t* get_address_of_m_nDiamondPrice_24() { return &___m_nDiamondPrice_24; }
	inline void set_m_nDiamondPrice_24(int32_t value)
	{
		___m_nDiamondPrice_24 = value;
	}

	inline static int32_t get_offset_of_m_nDuration_25() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_nDuration_25)); }
	inline int32_t get_m_nDuration_25() const { return ___m_nDuration_25; }
	inline int32_t* get_address_of_m_nDuration_25() { return &___m_nDuration_25; }
	inline void set_m_nDuration_25(int32_t value)
	{
		___m_nDuration_25 = value;
	}

	inline static int32_t get_offset_of_m_nItemType_26() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_nItemType_26)); }
	inline int32_t get_m_nItemType_26() const { return ___m_nItemType_26; }
	inline int32_t* get_address_of_m_nItemType_26() { return &___m_nItemType_26; }
	inline void set_m_nItemType_26(int32_t value)
	{
		___m_nItemType_26 = value;
	}

	inline static int32_t get_offset_of_m_nItemSubType_27() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_nItemSubType_27)); }
	inline int32_t get_m_nItemSubType_27() const { return ___m_nItemSubType_27; }
	inline int32_t* get_address_of_m_nItemSubType_27() { return &___m_nItemSubType_27; }
	inline void set_m_nItemSubType_27(int32_t value)
	{
		___m_nItemSubType_27 = value;
	}

	inline static int32_t get_offset_of_m_szFuncDest_28() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_szFuncDest_28)); }
	inline String_t* get_m_szFuncDest_28() const { return ___m_szFuncDest_28; }
	inline String_t** get_address_of_m_szFuncDest_28() { return &___m_szFuncDest_28; }
	inline void set_m_szFuncDest_28(String_t* value)
	{
		___m_szFuncDest_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_szFuncDest_28), value);
	}

	inline static int32_t get_offset_of_m_bItemInBag_29() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_bItemInBag_29)); }
	inline bool get_m_bItemInBag_29() const { return ___m_bItemInBag_29; }
	inline bool* get_address_of_m_bItemInBag_29() { return &___m_bItemInBag_29; }
	inline void set_m_bItemInBag_29(bool value)
	{
		___m_bItemInBag_29 = value;
	}

	inline static int32_t get_offset_of_m_nNum_30() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_nNum_30)); }
	inline int32_t get_m_nNum_30() const { return ___m_nNum_30; }
	inline int32_t* get_address_of_m_nNum_30() { return &___m_nNum_30; }
	inline void set_m_nNum_30(int32_t value)
	{
		___m_nNum_30 = value;
	}

	inline static int32_t get_offset_of_m_nItemId_31() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_nItemId_31)); }
	inline int32_t get_m_nItemId_31() const { return ___m_nItemId_31; }
	inline int32_t* get_address_of_m_nItemId_31() { return &___m_nItemId_31; }
	inline void set_m_nItemId_31(int32_t value)
	{
		___m_nItemId_31 = value;
	}

	inline static int32_t get_offset_of_m_dateStartTime_32() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_dateStartTime_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dateStartTime_32() const { return ___m_dateStartTime_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dateStartTime_32() { return &___m_dateStartTime_32; }
	inline void set_m_dateStartTime_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dateStartTime_32 = value;
	}

	inline static int32_t get_offset_of_m_bWorking_33() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_bWorking_33)); }
	inline bool get_m_bWorking_33() const { return ___m_bWorking_33; }
	inline bool* get_address_of_m_bWorking_33() { return &___m_bWorking_33; }
	inline void set_m_bWorking_33(bool value)
	{
		___m_bWorking_33 = value;
	}

	inline static int32_t get_offset_of_m_Config_34() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_Config_34)); }
	inline sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  get_m_Config_34() const { return ___m_Config_34; }
	inline sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058 * get_address_of_m_Config_34() { return &___m_Config_34; }
	inline void set_m_Config_34(sShoppingMallItemConfig_t77C43AC55058BC53A189F0EEC0D09D27D9B0D058  value)
	{
		___m_Config_34 = value;
	}

	inline static int32_t get_offset_of_m_eItemType_35() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_eItemType_35)); }
	inline int32_t get_m_eItemType_35() const { return ___m_eItemType_35; }
	inline int32_t* get_address_of_m_eItemType_35() { return &___m_eItemType_35; }
	inline void set_m_eItemType_35(int32_t value)
	{
		___m_eItemType_35 = value;
	}

	inline static int32_t get_offset_of_m_fTimeElapse_36() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169, ___m_fTimeElapse_36)); }
	inline float get_m_fTimeElapse_36() const { return ___m_fTimeElapse_36; }
	inline float* get_address_of_m_fTimeElapse_36() { return &___m_fTimeElapse_36; }
	inline void set_m_fTimeElapse_36(float value)
	{
		___m_fTimeElapse_36 = value;
	}
};

struct UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169_StaticFields
{
public:
	// UnityEngine.Vector3 UIShoppinAndItemCounter::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_4;
	// UnityEngine.Vector3 UIShoppinAndItemCounter::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_5;

public:
	inline static int32_t get_offset_of_vecTempPos_4() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169_StaticFields, ___vecTempPos_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_4() const { return ___vecTempPos_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_4() { return &___vecTempPos_4; }
	inline void set_vecTempPos_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_4 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_5() { return static_cast<int32_t>(offsetof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169_StaticFields, ___vecTempScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_5() const { return ___vecTempScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_5() { return &___vecTempScale_5; }
	inline void set_vecTempScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISHOPPINANDITEMCOUNTER_TC16F00AAEA618662EE5AD9105A70A000C6AB9169_H
#ifndef UISKILLCASTCOUNTER_T247CA0C3EB0CEA299A1BA446A51261D42690D324_H
#define UISKILLCASTCOUNTER_T247CA0C3EB0CEA299A1BA446A51261D42690D324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISkillCastCounter
struct  UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text UISkillCastCounter::_txtLeftTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtLeftTime_4;
	// UnityEngine.UI.Text UISkillCastCounter::_txtValue
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtValue_5;
	// UnityEngine.UI.Image UISkillCastCounter::_imgProgress
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgProgress_6;
	// UnityEngine.UI.Image UISkillCastCounter::_imgColdDownMask
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgColdDownMask_7;
	// CFrameAnimationEffect UISkillCastCounter::_effectLight
	CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * ____effectLight_8;
	// SkillManager/eSkillType UISkillCastCounter::m_eType
	int32_t ___m_eType_9;
	// Skill UISkillCastCounter::m_BoundSkill
	Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF * ___m_BoundSkill_10;
	// BaseScale UISkillCastCounter::_baseScale
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____baseScale_11;

public:
	inline static int32_t get_offset_of__txtLeftTime_4() { return static_cast<int32_t>(offsetof(UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324, ____txtLeftTime_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtLeftTime_4() const { return ____txtLeftTime_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtLeftTime_4() { return &____txtLeftTime_4; }
	inline void set__txtLeftTime_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtLeftTime_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtLeftTime_4), value);
	}

	inline static int32_t get_offset_of__txtValue_5() { return static_cast<int32_t>(offsetof(UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324, ____txtValue_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtValue_5() const { return ____txtValue_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtValue_5() { return &____txtValue_5; }
	inline void set__txtValue_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtValue_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtValue_5), value);
	}

	inline static int32_t get_offset_of__imgProgress_6() { return static_cast<int32_t>(offsetof(UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324, ____imgProgress_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgProgress_6() const { return ____imgProgress_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgProgress_6() { return &____imgProgress_6; }
	inline void set__imgProgress_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgProgress_6 = value;
		Il2CppCodeGenWriteBarrier((&____imgProgress_6), value);
	}

	inline static int32_t get_offset_of__imgColdDownMask_7() { return static_cast<int32_t>(offsetof(UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324, ____imgColdDownMask_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgColdDownMask_7() const { return ____imgColdDownMask_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgColdDownMask_7() { return &____imgColdDownMask_7; }
	inline void set__imgColdDownMask_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgColdDownMask_7 = value;
		Il2CppCodeGenWriteBarrier((&____imgColdDownMask_7), value);
	}

	inline static int32_t get_offset_of__effectLight_8() { return static_cast<int32_t>(offsetof(UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324, ____effectLight_8)); }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * get__effectLight_8() const { return ____effectLight_8; }
	inline CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 ** get_address_of__effectLight_8() { return &____effectLight_8; }
	inline void set__effectLight_8(CFrameAnimationEffect_tB09C367E8E6954F89BDD8004C69990C722AC2E10 * value)
	{
		____effectLight_8 = value;
		Il2CppCodeGenWriteBarrier((&____effectLight_8), value);
	}

	inline static int32_t get_offset_of_m_eType_9() { return static_cast<int32_t>(offsetof(UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324, ___m_eType_9)); }
	inline int32_t get_m_eType_9() const { return ___m_eType_9; }
	inline int32_t* get_address_of_m_eType_9() { return &___m_eType_9; }
	inline void set_m_eType_9(int32_t value)
	{
		___m_eType_9 = value;
	}

	inline static int32_t get_offset_of_m_BoundSkill_10() { return static_cast<int32_t>(offsetof(UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324, ___m_BoundSkill_10)); }
	inline Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF * get_m_BoundSkill_10() const { return ___m_BoundSkill_10; }
	inline Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF ** get_address_of_m_BoundSkill_10() { return &___m_BoundSkill_10; }
	inline void set_m_BoundSkill_10(Skill_t073059782D8C94AF6552F03A570459CA6FBEB2DF * value)
	{
		___m_BoundSkill_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundSkill_10), value);
	}

	inline static int32_t get_offset_of__baseScale_11() { return static_cast<int32_t>(offsetof(UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324, ____baseScale_11)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__baseScale_11() const { return ____baseScale_11; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__baseScale_11() { return &____baseScale_11; }
	inline void set__baseScale_11(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____baseScale_11 = value;
		Il2CppCodeGenWriteBarrier((&____baseScale_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISKILLCASTCOUNTER_T247CA0C3EB0CEA299A1BA446A51261D42690D324_H
#ifndef UISTARS_T895A62A22BCDD8808F59819C10C25D2A4095DC5B_H
#define UISTARS_T895A62A22BCDD8808F59819C10C25D2A4095DC5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIStars
struct  UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UIStars::m_nStarType
	int32_t ___m_nStarType_4;
	// UnityEngine.Sprite UIStars::m_sprStarNormal
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprStarNormal_5;
	// UnityEngine.Sprite UIStars::m_sprStarEmpty
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprStarEmpty_6;
	// UnityEngine.UI.Image UIStars::_imgGrayStar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgGrayStar_7;
	// UnityEngine.UI.Image[] UIStars::m_aryType0Stars
	ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* ___m_aryType0Stars_8;
	// UnityEngine.GameObject[] UIStars::m_aryType1StarsContainer
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___m_aryType1StarsContainer_9;

public:
	inline static int32_t get_offset_of_m_nStarType_4() { return static_cast<int32_t>(offsetof(UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B, ___m_nStarType_4)); }
	inline int32_t get_m_nStarType_4() const { return ___m_nStarType_4; }
	inline int32_t* get_address_of_m_nStarType_4() { return &___m_nStarType_4; }
	inline void set_m_nStarType_4(int32_t value)
	{
		___m_nStarType_4 = value;
	}

	inline static int32_t get_offset_of_m_sprStarNormal_5() { return static_cast<int32_t>(offsetof(UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B, ___m_sprStarNormal_5)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprStarNormal_5() const { return ___m_sprStarNormal_5; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprStarNormal_5() { return &___m_sprStarNormal_5; }
	inline void set_m_sprStarNormal_5(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprStarNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprStarNormal_5), value);
	}

	inline static int32_t get_offset_of_m_sprStarEmpty_6() { return static_cast<int32_t>(offsetof(UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B, ___m_sprStarEmpty_6)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprStarEmpty_6() const { return ___m_sprStarEmpty_6; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprStarEmpty_6() { return &___m_sprStarEmpty_6; }
	inline void set_m_sprStarEmpty_6(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprStarEmpty_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprStarEmpty_6), value);
	}

	inline static int32_t get_offset_of__imgGrayStar_7() { return static_cast<int32_t>(offsetof(UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B, ____imgGrayStar_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgGrayStar_7() const { return ____imgGrayStar_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgGrayStar_7() { return &____imgGrayStar_7; }
	inline void set__imgGrayStar_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgGrayStar_7 = value;
		Il2CppCodeGenWriteBarrier((&____imgGrayStar_7), value);
	}

	inline static int32_t get_offset_of_m_aryType0Stars_8() { return static_cast<int32_t>(offsetof(UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B, ___m_aryType0Stars_8)); }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* get_m_aryType0Stars_8() const { return ___m_aryType0Stars_8; }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D** get_address_of_m_aryType0Stars_8() { return &___m_aryType0Stars_8; }
	inline void set_m_aryType0Stars_8(ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* value)
	{
		___m_aryType0Stars_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryType0Stars_8), value);
	}

	inline static int32_t get_offset_of_m_aryType1StarsContainer_9() { return static_cast<int32_t>(offsetof(UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B, ___m_aryType1StarsContainer_9)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_m_aryType1StarsContainer_9() const { return ___m_aryType1StarsContainer_9; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_m_aryType1StarsContainer_9() { return &___m_aryType1StarsContainer_9; }
	inline void set_m_aryType1StarsContainer_9(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___m_aryType1StarsContainer_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_aryType1StarsContainer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISTARS_T895A62A22BCDD8808F59819C10C25D2A4095DC5B_H
#ifndef UIVEHICLECOUNTER_TC0C616D4332173796831C3199722C223A8D33D46_H
#define UIVEHICLECOUNTER_TC0C616D4332173796831C3199722C223A8D33D46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIVehicleCounter
struct  UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject UIVehicleCounter::_containerPriceOff
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerPriceOff_5;
	// UnityEngine.UI.Text UIVehicleCounter::_txtPriceOff
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPriceOff_6;
	// UnityEngine.UI.Text UIVehicleCounter::_txtQuestionMark0
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtQuestionMark0_7;
	// UnityEngine.UI.Text UIVehicleCounter::_txtQuestionMark1
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtQuestionMark1_8;
	// UnityEngine.GameObject UIVehicleCounter::_containerUnlockLevelAvatar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerUnlockLevelAvatar_9;
	// UnityEngine.UI.Image UIVehicleCounter::_imgUnlockLevelAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgUnlockLevelAvatar_10;
	// UnityEngine.UI.Image UIVehicleCounter::_imgLock
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgLock_11;
	// UnityEngine.UI.Text UIVehicleCounter::_txtTitleSpeed
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTitleSpeed_12;
	// UnityEngine.UI.Text UIVehicleCounter::_txtTitleGain
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTitleGain_13;
	// UnityEngine.UI.Text UIVehicleCounter::_txtLevel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtLevel_14;
	// UnityEngine.UI.Image UIVehicleCounter::_imgAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgAvatar_15;
	// UnityEngine.UI.Text UIVehicleCounter::_txtSpeed
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtSpeed_16;
	// UnityEngine.UI.Text UIVehicleCounter::_txtGain
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtGain_17;
	// UnityEngine.UI.Text UIVehicleCounter::_txtPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrice_18;
	// UnityEngine.UI.Text UIVehicleCounter::_txtInitialPrice
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtInitialPrice_19;
	// UnityEngine.UI.Text UIVehicleCounter::_txtSkillDiscount
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtSkillDiscount_20;
	// UnityEngine.UI.Text UIVehicleCounter::_txtScienceDiscount
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtScienceDiscount_21;
	// UnityEngine.UI.Text UIVehicleCounter::_txtName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtName_22;
	// UnityEngine.UI.Text UIVehicleCounter::_txtUnLockLevel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtUnLockLevel_23;
	// UnityEngine.UI.Image UIVehicleCounter::_imgProgressBar_Gain
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgProgressBar_Gain_24;
	// UnityEngine.UI.Image UIVehicleCounter::_imgProgressBar_Speed
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgProgressBar_Speed_25;
	// UnityEngine.UI.Image UIVehicleCounter::_imgBtnBuy
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgBtnBuy_26;
	// UnityEngine.UI.Image UIVehicleCounter::_imgCoinType
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCoinType_27;
	// UnityEngine.GameObject UIVehicleCounter::_containerGainAndSpeed
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerGainAndSpeed_28;
	// UnityEngine.GameObject UIVehicleCounter::_containerDiscount
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerDiscount_29;
	// UnityEngine.GameObject UIVehicleCounter::_containerPrice
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerPrice_30;
	// UnityEngine.UI.Button UIVehicleCounter::_btnBuy
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnBuy_31;
	// UnityEngine.GameObject UIVehicleCounter::_containerResearchLock
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerResearchLock_32;
	// System.Int32 UIVehicleCounter::m_nVehicleLevel
	int32_t ___m_nVehicleLevel_33;
	// System.Double UIVehicleCounter::m_nPrice
	double ___m_nPrice_34;
	// System.Int32 UIVehicleCounter::m_nDiamondPrice
	int32_t ___m_nDiamondPrice_35;
	// System.Boolean UIVehicleCounter::m_bWatchAdFree
	bool ___m_bWatchAdFree_36;
	// DataManager/eMoneyType UIVehicleCounter::m_eMoneyType
	int32_t ___m_eMoneyType_37;
	// UnityEngine.Sprite UIVehicleCounter::m_sprJianYingTemp
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprJianYingTemp_38;
	// DataManager/sAutomobileConfig UIVehicleCounter::m_Config
	sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20  ___m_Config_39;
	// System.Boolean UIVehicleCounter::m_bUnlocked
	bool ___m_bUnlocked_40;
	// ResearchCounter UIVehicleCounter::m_BoundResearchCounter
	ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * ___m_BoundResearchCounter_41;

public:
	inline static int32_t get_offset_of__containerPriceOff_5() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____containerPriceOff_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerPriceOff_5() const { return ____containerPriceOff_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerPriceOff_5() { return &____containerPriceOff_5; }
	inline void set__containerPriceOff_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerPriceOff_5 = value;
		Il2CppCodeGenWriteBarrier((&____containerPriceOff_5), value);
	}

	inline static int32_t get_offset_of__txtPriceOff_6() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtPriceOff_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPriceOff_6() const { return ____txtPriceOff_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPriceOff_6() { return &____txtPriceOff_6; }
	inline void set__txtPriceOff_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPriceOff_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtPriceOff_6), value);
	}

	inline static int32_t get_offset_of__txtQuestionMark0_7() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtQuestionMark0_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtQuestionMark0_7() const { return ____txtQuestionMark0_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtQuestionMark0_7() { return &____txtQuestionMark0_7; }
	inline void set__txtQuestionMark0_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtQuestionMark0_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtQuestionMark0_7), value);
	}

	inline static int32_t get_offset_of__txtQuestionMark1_8() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtQuestionMark1_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtQuestionMark1_8() const { return ____txtQuestionMark1_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtQuestionMark1_8() { return &____txtQuestionMark1_8; }
	inline void set__txtQuestionMark1_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtQuestionMark1_8 = value;
		Il2CppCodeGenWriteBarrier((&____txtQuestionMark1_8), value);
	}

	inline static int32_t get_offset_of__containerUnlockLevelAvatar_9() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____containerUnlockLevelAvatar_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerUnlockLevelAvatar_9() const { return ____containerUnlockLevelAvatar_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerUnlockLevelAvatar_9() { return &____containerUnlockLevelAvatar_9; }
	inline void set__containerUnlockLevelAvatar_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerUnlockLevelAvatar_9 = value;
		Il2CppCodeGenWriteBarrier((&____containerUnlockLevelAvatar_9), value);
	}

	inline static int32_t get_offset_of__imgUnlockLevelAvatar_10() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____imgUnlockLevelAvatar_10)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgUnlockLevelAvatar_10() const { return ____imgUnlockLevelAvatar_10; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgUnlockLevelAvatar_10() { return &____imgUnlockLevelAvatar_10; }
	inline void set__imgUnlockLevelAvatar_10(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgUnlockLevelAvatar_10 = value;
		Il2CppCodeGenWriteBarrier((&____imgUnlockLevelAvatar_10), value);
	}

	inline static int32_t get_offset_of__imgLock_11() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____imgLock_11)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgLock_11() const { return ____imgLock_11; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgLock_11() { return &____imgLock_11; }
	inline void set__imgLock_11(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgLock_11 = value;
		Il2CppCodeGenWriteBarrier((&____imgLock_11), value);
	}

	inline static int32_t get_offset_of__txtTitleSpeed_12() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtTitleSpeed_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTitleSpeed_12() const { return ____txtTitleSpeed_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTitleSpeed_12() { return &____txtTitleSpeed_12; }
	inline void set__txtTitleSpeed_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTitleSpeed_12 = value;
		Il2CppCodeGenWriteBarrier((&____txtTitleSpeed_12), value);
	}

	inline static int32_t get_offset_of__txtTitleGain_13() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtTitleGain_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTitleGain_13() const { return ____txtTitleGain_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTitleGain_13() { return &____txtTitleGain_13; }
	inline void set__txtTitleGain_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTitleGain_13 = value;
		Il2CppCodeGenWriteBarrier((&____txtTitleGain_13), value);
	}

	inline static int32_t get_offset_of__txtLevel_14() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtLevel_14)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtLevel_14() const { return ____txtLevel_14; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtLevel_14() { return &____txtLevel_14; }
	inline void set__txtLevel_14(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtLevel_14 = value;
		Il2CppCodeGenWriteBarrier((&____txtLevel_14), value);
	}

	inline static int32_t get_offset_of__imgAvatar_15() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____imgAvatar_15)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgAvatar_15() const { return ____imgAvatar_15; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgAvatar_15() { return &____imgAvatar_15; }
	inline void set__imgAvatar_15(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgAvatar_15 = value;
		Il2CppCodeGenWriteBarrier((&____imgAvatar_15), value);
	}

	inline static int32_t get_offset_of__txtSpeed_16() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtSpeed_16)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtSpeed_16() const { return ____txtSpeed_16; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtSpeed_16() { return &____txtSpeed_16; }
	inline void set__txtSpeed_16(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtSpeed_16 = value;
		Il2CppCodeGenWriteBarrier((&____txtSpeed_16), value);
	}

	inline static int32_t get_offset_of__txtGain_17() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtGain_17)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtGain_17() const { return ____txtGain_17; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtGain_17() { return &____txtGain_17; }
	inline void set__txtGain_17(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtGain_17 = value;
		Il2CppCodeGenWriteBarrier((&____txtGain_17), value);
	}

	inline static int32_t get_offset_of__txtPrice_18() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtPrice_18)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrice_18() const { return ____txtPrice_18; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrice_18() { return &____txtPrice_18; }
	inline void set__txtPrice_18(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrice_18 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrice_18), value);
	}

	inline static int32_t get_offset_of__txtInitialPrice_19() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtInitialPrice_19)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtInitialPrice_19() const { return ____txtInitialPrice_19; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtInitialPrice_19() { return &____txtInitialPrice_19; }
	inline void set__txtInitialPrice_19(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtInitialPrice_19 = value;
		Il2CppCodeGenWriteBarrier((&____txtInitialPrice_19), value);
	}

	inline static int32_t get_offset_of__txtSkillDiscount_20() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtSkillDiscount_20)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtSkillDiscount_20() const { return ____txtSkillDiscount_20; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtSkillDiscount_20() { return &____txtSkillDiscount_20; }
	inline void set__txtSkillDiscount_20(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtSkillDiscount_20 = value;
		Il2CppCodeGenWriteBarrier((&____txtSkillDiscount_20), value);
	}

	inline static int32_t get_offset_of__txtScienceDiscount_21() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtScienceDiscount_21)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtScienceDiscount_21() const { return ____txtScienceDiscount_21; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtScienceDiscount_21() { return &____txtScienceDiscount_21; }
	inline void set__txtScienceDiscount_21(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtScienceDiscount_21 = value;
		Il2CppCodeGenWriteBarrier((&____txtScienceDiscount_21), value);
	}

	inline static int32_t get_offset_of__txtName_22() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtName_22)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtName_22() const { return ____txtName_22; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtName_22() { return &____txtName_22; }
	inline void set__txtName_22(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtName_22 = value;
		Il2CppCodeGenWriteBarrier((&____txtName_22), value);
	}

	inline static int32_t get_offset_of__txtUnLockLevel_23() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____txtUnLockLevel_23)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtUnLockLevel_23() const { return ____txtUnLockLevel_23; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtUnLockLevel_23() { return &____txtUnLockLevel_23; }
	inline void set__txtUnLockLevel_23(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtUnLockLevel_23 = value;
		Il2CppCodeGenWriteBarrier((&____txtUnLockLevel_23), value);
	}

	inline static int32_t get_offset_of__imgProgressBar_Gain_24() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____imgProgressBar_Gain_24)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgProgressBar_Gain_24() const { return ____imgProgressBar_Gain_24; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgProgressBar_Gain_24() { return &____imgProgressBar_Gain_24; }
	inline void set__imgProgressBar_Gain_24(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgProgressBar_Gain_24 = value;
		Il2CppCodeGenWriteBarrier((&____imgProgressBar_Gain_24), value);
	}

	inline static int32_t get_offset_of__imgProgressBar_Speed_25() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____imgProgressBar_Speed_25)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgProgressBar_Speed_25() const { return ____imgProgressBar_Speed_25; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgProgressBar_Speed_25() { return &____imgProgressBar_Speed_25; }
	inline void set__imgProgressBar_Speed_25(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgProgressBar_Speed_25 = value;
		Il2CppCodeGenWriteBarrier((&____imgProgressBar_Speed_25), value);
	}

	inline static int32_t get_offset_of__imgBtnBuy_26() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____imgBtnBuy_26)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgBtnBuy_26() const { return ____imgBtnBuy_26; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgBtnBuy_26() { return &____imgBtnBuy_26; }
	inline void set__imgBtnBuy_26(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgBtnBuy_26 = value;
		Il2CppCodeGenWriteBarrier((&____imgBtnBuy_26), value);
	}

	inline static int32_t get_offset_of__imgCoinType_27() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____imgCoinType_27)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCoinType_27() const { return ____imgCoinType_27; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCoinType_27() { return &____imgCoinType_27; }
	inline void set__imgCoinType_27(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCoinType_27 = value;
		Il2CppCodeGenWriteBarrier((&____imgCoinType_27), value);
	}

	inline static int32_t get_offset_of__containerGainAndSpeed_28() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____containerGainAndSpeed_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerGainAndSpeed_28() const { return ____containerGainAndSpeed_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerGainAndSpeed_28() { return &____containerGainAndSpeed_28; }
	inline void set__containerGainAndSpeed_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerGainAndSpeed_28 = value;
		Il2CppCodeGenWriteBarrier((&____containerGainAndSpeed_28), value);
	}

	inline static int32_t get_offset_of__containerDiscount_29() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____containerDiscount_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerDiscount_29() const { return ____containerDiscount_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerDiscount_29() { return &____containerDiscount_29; }
	inline void set__containerDiscount_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerDiscount_29 = value;
		Il2CppCodeGenWriteBarrier((&____containerDiscount_29), value);
	}

	inline static int32_t get_offset_of__containerPrice_30() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____containerPrice_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerPrice_30() const { return ____containerPrice_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerPrice_30() { return &____containerPrice_30; }
	inline void set__containerPrice_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerPrice_30 = value;
		Il2CppCodeGenWriteBarrier((&____containerPrice_30), value);
	}

	inline static int32_t get_offset_of__btnBuy_31() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____btnBuy_31)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnBuy_31() const { return ____btnBuy_31; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnBuy_31() { return &____btnBuy_31; }
	inline void set__btnBuy_31(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnBuy_31 = value;
		Il2CppCodeGenWriteBarrier((&____btnBuy_31), value);
	}

	inline static int32_t get_offset_of__containerResearchLock_32() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ____containerResearchLock_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerResearchLock_32() const { return ____containerResearchLock_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerResearchLock_32() { return &____containerResearchLock_32; }
	inline void set__containerResearchLock_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerResearchLock_32 = value;
		Il2CppCodeGenWriteBarrier((&____containerResearchLock_32), value);
	}

	inline static int32_t get_offset_of_m_nVehicleLevel_33() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ___m_nVehicleLevel_33)); }
	inline int32_t get_m_nVehicleLevel_33() const { return ___m_nVehicleLevel_33; }
	inline int32_t* get_address_of_m_nVehicleLevel_33() { return &___m_nVehicleLevel_33; }
	inline void set_m_nVehicleLevel_33(int32_t value)
	{
		___m_nVehicleLevel_33 = value;
	}

	inline static int32_t get_offset_of_m_nPrice_34() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ___m_nPrice_34)); }
	inline double get_m_nPrice_34() const { return ___m_nPrice_34; }
	inline double* get_address_of_m_nPrice_34() { return &___m_nPrice_34; }
	inline void set_m_nPrice_34(double value)
	{
		___m_nPrice_34 = value;
	}

	inline static int32_t get_offset_of_m_nDiamondPrice_35() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ___m_nDiamondPrice_35)); }
	inline int32_t get_m_nDiamondPrice_35() const { return ___m_nDiamondPrice_35; }
	inline int32_t* get_address_of_m_nDiamondPrice_35() { return &___m_nDiamondPrice_35; }
	inline void set_m_nDiamondPrice_35(int32_t value)
	{
		___m_nDiamondPrice_35 = value;
	}

	inline static int32_t get_offset_of_m_bWatchAdFree_36() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ___m_bWatchAdFree_36)); }
	inline bool get_m_bWatchAdFree_36() const { return ___m_bWatchAdFree_36; }
	inline bool* get_address_of_m_bWatchAdFree_36() { return &___m_bWatchAdFree_36; }
	inline void set_m_bWatchAdFree_36(bool value)
	{
		___m_bWatchAdFree_36 = value;
	}

	inline static int32_t get_offset_of_m_eMoneyType_37() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ___m_eMoneyType_37)); }
	inline int32_t get_m_eMoneyType_37() const { return ___m_eMoneyType_37; }
	inline int32_t* get_address_of_m_eMoneyType_37() { return &___m_eMoneyType_37; }
	inline void set_m_eMoneyType_37(int32_t value)
	{
		___m_eMoneyType_37 = value;
	}

	inline static int32_t get_offset_of_m_sprJianYingTemp_38() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ___m_sprJianYingTemp_38)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprJianYingTemp_38() const { return ___m_sprJianYingTemp_38; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprJianYingTemp_38() { return &___m_sprJianYingTemp_38; }
	inline void set_m_sprJianYingTemp_38(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprJianYingTemp_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprJianYingTemp_38), value);
	}

	inline static int32_t get_offset_of_m_Config_39() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ___m_Config_39)); }
	inline sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20  get_m_Config_39() const { return ___m_Config_39; }
	inline sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20 * get_address_of_m_Config_39() { return &___m_Config_39; }
	inline void set_m_Config_39(sAutomobileConfig_t0F7305B3147D29DC77AFD945BD716711D8271B20  value)
	{
		___m_Config_39 = value;
	}

	inline static int32_t get_offset_of_m_bUnlocked_40() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ___m_bUnlocked_40)); }
	inline bool get_m_bUnlocked_40() const { return ___m_bUnlocked_40; }
	inline bool* get_address_of_m_bUnlocked_40() { return &___m_bUnlocked_40; }
	inline void set_m_bUnlocked_40(bool value)
	{
		___m_bUnlocked_40 = value;
	}

	inline static int32_t get_offset_of_m_BoundResearchCounter_41() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46, ___m_BoundResearchCounter_41)); }
	inline ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * get_m_BoundResearchCounter_41() const { return ___m_BoundResearchCounter_41; }
	inline ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD ** get_address_of_m_BoundResearchCounter_41() { return &___m_BoundResearchCounter_41; }
	inline void set_m_BoundResearchCounter_41(ResearchCounter_t192EF04A2372C9EAD586F86C352CE0742E98AAAD * value)
	{
		___m_BoundResearchCounter_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundResearchCounter_41), value);
	}
};

struct UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46_StaticFields
{
public:
	// UnityEngine.Color UIVehicleCounter::colorTemp
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___colorTemp_4;

public:
	inline static int32_t get_offset_of_colorTemp_4() { return static_cast<int32_t>(offsetof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46_StaticFields, ___colorTemp_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_colorTemp_4() const { return ___colorTemp_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_colorTemp_4() { return &___colorTemp_4; }
	inline void set_colorTemp_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___colorTemp_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVEHICLECOUNTER_TC0C616D4332173796831C3199722C223A8D33D46_H
#ifndef UIZENGSHOUCOUNTER_T35FB691FBD7229E775905E79FA0CA9E20992CA95_H
#define UIZENGSHOUCOUNTER_T35FB691FBD7229E775905E79FA0CA9E20992CA95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIZengShouCounter
struct  UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text UIZengShouCounter::_txtPlanetAndDistrict
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPlanetAndDistrict_4;
	// UnityEngine.UI.Image UIZengShouCounter::_imgPlanetAvatar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgPlanetAvatar_5;
	// UnityEngine.UI.Text UIZengShouCounter::_txtPrestigeRaise
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtPrestigeRaise_6;
	// UnityEngine.UI.Text UIZengShouCounter::_txtDistrictRaise
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtDistrictRaise_7;
	// UnityEngine.UI.Image UIZengShouCounter::_imgCoinIcon
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____imgCoinIcon_8;
	// UnityEngine.UI.Text UIZengShouCounter::_txtOfflineDps
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtOfflineDps_9;
	// UnityEngine.UI.Text UIZengShouCounter::_txtCurPrestigeTimes
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtCurPrestigeTimes_10;
	// System.Int32 UIZengShouCounter::m_nPlanetId
	int32_t ___m_nPlanetId_11;
	// System.Int32 UIZengShouCounter::m_nDistrictId
	int32_t ___m_nDistrictId_12;

public:
	inline static int32_t get_offset_of__txtPlanetAndDistrict_4() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____txtPlanetAndDistrict_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPlanetAndDistrict_4() const { return ____txtPlanetAndDistrict_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPlanetAndDistrict_4() { return &____txtPlanetAndDistrict_4; }
	inline void set__txtPlanetAndDistrict_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPlanetAndDistrict_4 = value;
		Il2CppCodeGenWriteBarrier((&____txtPlanetAndDistrict_4), value);
	}

	inline static int32_t get_offset_of__imgPlanetAvatar_5() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____imgPlanetAvatar_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgPlanetAvatar_5() const { return ____imgPlanetAvatar_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgPlanetAvatar_5() { return &____imgPlanetAvatar_5; }
	inline void set__imgPlanetAvatar_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgPlanetAvatar_5 = value;
		Il2CppCodeGenWriteBarrier((&____imgPlanetAvatar_5), value);
	}

	inline static int32_t get_offset_of__txtPrestigeRaise_6() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____txtPrestigeRaise_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtPrestigeRaise_6() const { return ____txtPrestigeRaise_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtPrestigeRaise_6() { return &____txtPrestigeRaise_6; }
	inline void set__txtPrestigeRaise_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtPrestigeRaise_6 = value;
		Il2CppCodeGenWriteBarrier((&____txtPrestigeRaise_6), value);
	}

	inline static int32_t get_offset_of__txtDistrictRaise_7() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____txtDistrictRaise_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtDistrictRaise_7() const { return ____txtDistrictRaise_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtDistrictRaise_7() { return &____txtDistrictRaise_7; }
	inline void set__txtDistrictRaise_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtDistrictRaise_7 = value;
		Il2CppCodeGenWriteBarrier((&____txtDistrictRaise_7), value);
	}

	inline static int32_t get_offset_of__imgCoinIcon_8() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____imgCoinIcon_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__imgCoinIcon_8() const { return ____imgCoinIcon_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__imgCoinIcon_8() { return &____imgCoinIcon_8; }
	inline void set__imgCoinIcon_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____imgCoinIcon_8 = value;
		Il2CppCodeGenWriteBarrier((&____imgCoinIcon_8), value);
	}

	inline static int32_t get_offset_of__txtOfflineDps_9() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____txtOfflineDps_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtOfflineDps_9() const { return ____txtOfflineDps_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtOfflineDps_9() { return &____txtOfflineDps_9; }
	inline void set__txtOfflineDps_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtOfflineDps_9 = value;
		Il2CppCodeGenWriteBarrier((&____txtOfflineDps_9), value);
	}

	inline static int32_t get_offset_of__txtCurPrestigeTimes_10() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ____txtCurPrestigeTimes_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtCurPrestigeTimes_10() const { return ____txtCurPrestigeTimes_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtCurPrestigeTimes_10() { return &____txtCurPrestigeTimes_10; }
	inline void set__txtCurPrestigeTimes_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtCurPrestigeTimes_10 = value;
		Il2CppCodeGenWriteBarrier((&____txtCurPrestigeTimes_10), value);
	}

	inline static int32_t get_offset_of_m_nPlanetId_11() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ___m_nPlanetId_11)); }
	inline int32_t get_m_nPlanetId_11() const { return ___m_nPlanetId_11; }
	inline int32_t* get_address_of_m_nPlanetId_11() { return &___m_nPlanetId_11; }
	inline void set_m_nPlanetId_11(int32_t value)
	{
		___m_nPlanetId_11 = value;
	}

	inline static int32_t get_offset_of_m_nDistrictId_12() { return static_cast<int32_t>(offsetof(UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95, ___m_nDistrictId_12)); }
	inline int32_t get_m_nDistrictId_12() const { return ___m_nDistrictId_12; }
	inline int32_t* get_address_of_m_nDistrictId_12() { return &___m_nDistrictId_12; }
	inline void set_m_nDistrictId_12(int32_t value)
	{
		___m_nDistrictId_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIZENGSHOUCOUNTER_T35FB691FBD7229E775905E79FA0CA9E20992CA95_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef UNLOCKNEWPLANEMANAGER_T402DEC9E426B680014E232E712D172F77F23341B_H
#define UNLOCKNEWPLANEMANAGER_T402DEC9E426B680014E232E712D172F77F23341B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnlockNewPlaneManager
struct  UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text UnlockNewPlaneManager::_txtTapToExit
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____txtTapToExit_5;
	// UnityEngine.UI.Button UnlockNewPlaneManager::_btnClose
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____btnClose_6;
	// Spine.Unity.SkeletonGraphic UnlockNewPlaneManager::_skeTwoAuto
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * ____skeTwoAuto_7;
	// Spine.Unity.SkeletonGraphic UnlockNewPlaneManager::_skeNewAuto
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * ____skeNewAuto_8;
	// Spine.Unity.SkeletonGraphic UnlockNewPlaneManager::_skeEffect1
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * ____skeEffect1_9;
	// Spine.Unity.SkeletonGraphic UnlockNewPlaneManager::_skeEffect2
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * ____skeEffect2_10;
	// UnityEngine.GameObject UnlockNewPlaneManager::m_containerEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerEffect_11;
	// UnityEngine.GameObject UnlockNewPlaneManager::m_containerLighning
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerLighning_12;
	// UnityEngine.GameObject UnlockNewPlaneManager::m_containerFoGuang
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_containerFoGuang_13;
	// UnityEngine.GameObject UnlockNewPlaneManager::m_preFoGuang
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preFoGuang_14;
	// UnityEngine.GameObject UnlockNewPlaneManager::_containerAll
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerAll_17;
	// UnityEngine.SpriteRenderer UnlockNewPlaneManager::_srPlane1
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srPlane1_18;
	// UnityEngine.SpriteRenderer UnlockNewPlaneManager::_srPlane2
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srPlane2_19;
	// UnityEngine.SpriteRenderer UnlockNewPlaneManager::_srNewPlane
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____srNewPlane_20;
	// System.Single[] UnlockNewPlaneManager::m_arySessionTime
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_arySessionTime_21;
	// UnityEngine.Vector2 UnlockNewPlaneManager::m_vecPlane1StartPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecPlane1StartPos_22;
	// UnityEngine.Vector2 UnlockNewPlaneManager::m_vecPlane2StartPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_vecPlane2StartPos_23;
	// System.Single UnlockNewPlaneManager::m_fCrashDistance
	float ___m_fCrashDistance_24;
	// System.Single UnlockNewPlaneManager::m_fBackSpeed
	float ___m_fBackSpeed_25;
	// System.Single UnlockNewPlaneManager::m_fBackDistance
	float ___m_fBackDistance_26;
	// System.Single UnlockNewPlaneManager::m_fCrashA
	float ___m_fCrashA_27;
	// System.Single UnlockNewPlaneManager::m_fCrashSpeed
	float ___m_fCrashSpeed_28;
	// System.Int32 UnlockNewPlaneManager::m_nStatus
	int32_t ___m_nStatus_29;
	// UnityEngine.GameObject UnlockNewPlaneManager::_goLight
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____goLight_30;
	// BaseScale UnlockNewPlaneManager::_baseScaleNewPlane
	BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * ____baseScaleNewPlane_31;
	// UnityEngine.GameObject UnlockNewPlaneManager::_containerExplosionEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____containerExplosionEffect_32;
	// UnityEngine.GameObject UnlockNewPlaneManager::_effectFoGuang
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____effectFoGuang_33;
	// UnityEngine.GameObject[] UnlockNewPlaneManager::_aryContainerFirework
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ____aryContainerFirework_34;
	// UnityEngine.Sprite UnlockNewPlaneManager::m_sprTest
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_sprTest_35;
	// System.Int32 UnlockNewPlaneManager::m_nCurLevel
	int32_t ___m_nCurLevel_36;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnlockNewPlaneManager::m_lstEffects
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___m_lstEffects_37;
	// System.Single UnlockNewPlaneManager::m_fTimeElapse
	float ___m_fTimeElapse_38;
	// System.Single UnlockNewPlaneManager::m_fMovement
	float ___m_fMovement_39;
	// System.Int32 UnlockNewPlaneManager::m_nCount
	int32_t ___m_nCount_40;

public:
	inline static int32_t get_offset_of__txtTapToExit_5() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____txtTapToExit_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__txtTapToExit_5() const { return ____txtTapToExit_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__txtTapToExit_5() { return &____txtTapToExit_5; }
	inline void set__txtTapToExit_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____txtTapToExit_5 = value;
		Il2CppCodeGenWriteBarrier((&____txtTapToExit_5), value);
	}

	inline static int32_t get_offset_of__btnClose_6() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____btnClose_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__btnClose_6() const { return ____btnClose_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__btnClose_6() { return &____btnClose_6; }
	inline void set__btnClose_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____btnClose_6 = value;
		Il2CppCodeGenWriteBarrier((&____btnClose_6), value);
	}

	inline static int32_t get_offset_of__skeTwoAuto_7() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____skeTwoAuto_7)); }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * get__skeTwoAuto_7() const { return ____skeTwoAuto_7; }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A ** get_address_of__skeTwoAuto_7() { return &____skeTwoAuto_7; }
	inline void set__skeTwoAuto_7(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * value)
	{
		____skeTwoAuto_7 = value;
		Il2CppCodeGenWriteBarrier((&____skeTwoAuto_7), value);
	}

	inline static int32_t get_offset_of__skeNewAuto_8() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____skeNewAuto_8)); }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * get__skeNewAuto_8() const { return ____skeNewAuto_8; }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A ** get_address_of__skeNewAuto_8() { return &____skeNewAuto_8; }
	inline void set__skeNewAuto_8(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * value)
	{
		____skeNewAuto_8 = value;
		Il2CppCodeGenWriteBarrier((&____skeNewAuto_8), value);
	}

	inline static int32_t get_offset_of__skeEffect1_9() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____skeEffect1_9)); }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * get__skeEffect1_9() const { return ____skeEffect1_9; }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A ** get_address_of__skeEffect1_9() { return &____skeEffect1_9; }
	inline void set__skeEffect1_9(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * value)
	{
		____skeEffect1_9 = value;
		Il2CppCodeGenWriteBarrier((&____skeEffect1_9), value);
	}

	inline static int32_t get_offset_of__skeEffect2_10() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____skeEffect2_10)); }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * get__skeEffect2_10() const { return ____skeEffect2_10; }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A ** get_address_of__skeEffect2_10() { return &____skeEffect2_10; }
	inline void set__skeEffect2_10(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * value)
	{
		____skeEffect2_10 = value;
		Il2CppCodeGenWriteBarrier((&____skeEffect2_10), value);
	}

	inline static int32_t get_offset_of_m_containerEffect_11() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_containerEffect_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerEffect_11() const { return ___m_containerEffect_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerEffect_11() { return &___m_containerEffect_11; }
	inline void set_m_containerEffect_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerEffect_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerEffect_11), value);
	}

	inline static int32_t get_offset_of_m_containerLighning_12() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_containerLighning_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerLighning_12() const { return ___m_containerLighning_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerLighning_12() { return &___m_containerLighning_12; }
	inline void set_m_containerLighning_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerLighning_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerLighning_12), value);
	}

	inline static int32_t get_offset_of_m_containerFoGuang_13() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_containerFoGuang_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_containerFoGuang_13() const { return ___m_containerFoGuang_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_containerFoGuang_13() { return &___m_containerFoGuang_13; }
	inline void set_m_containerFoGuang_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_containerFoGuang_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_containerFoGuang_13), value);
	}

	inline static int32_t get_offset_of_m_preFoGuang_14() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_preFoGuang_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preFoGuang_14() const { return ___m_preFoGuang_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preFoGuang_14() { return &___m_preFoGuang_14; }
	inline void set_m_preFoGuang_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preFoGuang_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_preFoGuang_14), value);
	}

	inline static int32_t get_offset_of__containerAll_17() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____containerAll_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerAll_17() const { return ____containerAll_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerAll_17() { return &____containerAll_17; }
	inline void set__containerAll_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerAll_17 = value;
		Il2CppCodeGenWriteBarrier((&____containerAll_17), value);
	}

	inline static int32_t get_offset_of__srPlane1_18() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____srPlane1_18)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srPlane1_18() const { return ____srPlane1_18; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srPlane1_18() { return &____srPlane1_18; }
	inline void set__srPlane1_18(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srPlane1_18 = value;
		Il2CppCodeGenWriteBarrier((&____srPlane1_18), value);
	}

	inline static int32_t get_offset_of__srPlane2_19() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____srPlane2_19)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srPlane2_19() const { return ____srPlane2_19; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srPlane2_19() { return &____srPlane2_19; }
	inline void set__srPlane2_19(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srPlane2_19 = value;
		Il2CppCodeGenWriteBarrier((&____srPlane2_19), value);
	}

	inline static int32_t get_offset_of__srNewPlane_20() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____srNewPlane_20)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__srNewPlane_20() const { return ____srNewPlane_20; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__srNewPlane_20() { return &____srNewPlane_20; }
	inline void set__srNewPlane_20(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____srNewPlane_20 = value;
		Il2CppCodeGenWriteBarrier((&____srNewPlane_20), value);
	}

	inline static int32_t get_offset_of_m_arySessionTime_21() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_arySessionTime_21)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_arySessionTime_21() const { return ___m_arySessionTime_21; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_arySessionTime_21() { return &___m_arySessionTime_21; }
	inline void set_m_arySessionTime_21(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_arySessionTime_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_arySessionTime_21), value);
	}

	inline static int32_t get_offset_of_m_vecPlane1StartPos_22() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_vecPlane1StartPos_22)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecPlane1StartPos_22() const { return ___m_vecPlane1StartPos_22; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecPlane1StartPos_22() { return &___m_vecPlane1StartPos_22; }
	inline void set_m_vecPlane1StartPos_22(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecPlane1StartPos_22 = value;
	}

	inline static int32_t get_offset_of_m_vecPlane2StartPos_23() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_vecPlane2StartPos_23)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_vecPlane2StartPos_23() const { return ___m_vecPlane2StartPos_23; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_vecPlane2StartPos_23() { return &___m_vecPlane2StartPos_23; }
	inline void set_m_vecPlane2StartPos_23(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_vecPlane2StartPos_23 = value;
	}

	inline static int32_t get_offset_of_m_fCrashDistance_24() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fCrashDistance_24)); }
	inline float get_m_fCrashDistance_24() const { return ___m_fCrashDistance_24; }
	inline float* get_address_of_m_fCrashDistance_24() { return &___m_fCrashDistance_24; }
	inline void set_m_fCrashDistance_24(float value)
	{
		___m_fCrashDistance_24 = value;
	}

	inline static int32_t get_offset_of_m_fBackSpeed_25() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fBackSpeed_25)); }
	inline float get_m_fBackSpeed_25() const { return ___m_fBackSpeed_25; }
	inline float* get_address_of_m_fBackSpeed_25() { return &___m_fBackSpeed_25; }
	inline void set_m_fBackSpeed_25(float value)
	{
		___m_fBackSpeed_25 = value;
	}

	inline static int32_t get_offset_of_m_fBackDistance_26() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fBackDistance_26)); }
	inline float get_m_fBackDistance_26() const { return ___m_fBackDistance_26; }
	inline float* get_address_of_m_fBackDistance_26() { return &___m_fBackDistance_26; }
	inline void set_m_fBackDistance_26(float value)
	{
		___m_fBackDistance_26 = value;
	}

	inline static int32_t get_offset_of_m_fCrashA_27() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fCrashA_27)); }
	inline float get_m_fCrashA_27() const { return ___m_fCrashA_27; }
	inline float* get_address_of_m_fCrashA_27() { return &___m_fCrashA_27; }
	inline void set_m_fCrashA_27(float value)
	{
		___m_fCrashA_27 = value;
	}

	inline static int32_t get_offset_of_m_fCrashSpeed_28() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fCrashSpeed_28)); }
	inline float get_m_fCrashSpeed_28() const { return ___m_fCrashSpeed_28; }
	inline float* get_address_of_m_fCrashSpeed_28() { return &___m_fCrashSpeed_28; }
	inline void set_m_fCrashSpeed_28(float value)
	{
		___m_fCrashSpeed_28 = value;
	}

	inline static int32_t get_offset_of_m_nStatus_29() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_nStatus_29)); }
	inline int32_t get_m_nStatus_29() const { return ___m_nStatus_29; }
	inline int32_t* get_address_of_m_nStatus_29() { return &___m_nStatus_29; }
	inline void set_m_nStatus_29(int32_t value)
	{
		___m_nStatus_29 = value;
	}

	inline static int32_t get_offset_of__goLight_30() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____goLight_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__goLight_30() const { return ____goLight_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__goLight_30() { return &____goLight_30; }
	inline void set__goLight_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____goLight_30 = value;
		Il2CppCodeGenWriteBarrier((&____goLight_30), value);
	}

	inline static int32_t get_offset_of__baseScaleNewPlane_31() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____baseScaleNewPlane_31)); }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * get__baseScaleNewPlane_31() const { return ____baseScaleNewPlane_31; }
	inline BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 ** get_address_of__baseScaleNewPlane_31() { return &____baseScaleNewPlane_31; }
	inline void set__baseScaleNewPlane_31(BaseScale_tA9DBA1A65C015253341E09F34B301CB704B44063 * value)
	{
		____baseScaleNewPlane_31 = value;
		Il2CppCodeGenWriteBarrier((&____baseScaleNewPlane_31), value);
	}

	inline static int32_t get_offset_of__containerExplosionEffect_32() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____containerExplosionEffect_32)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__containerExplosionEffect_32() const { return ____containerExplosionEffect_32; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__containerExplosionEffect_32() { return &____containerExplosionEffect_32; }
	inline void set__containerExplosionEffect_32(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____containerExplosionEffect_32 = value;
		Il2CppCodeGenWriteBarrier((&____containerExplosionEffect_32), value);
	}

	inline static int32_t get_offset_of__effectFoGuang_33() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____effectFoGuang_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__effectFoGuang_33() const { return ____effectFoGuang_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__effectFoGuang_33() { return &____effectFoGuang_33; }
	inline void set__effectFoGuang_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____effectFoGuang_33 = value;
		Il2CppCodeGenWriteBarrier((&____effectFoGuang_33), value);
	}

	inline static int32_t get_offset_of__aryContainerFirework_34() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ____aryContainerFirework_34)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get__aryContainerFirework_34() const { return ____aryContainerFirework_34; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of__aryContainerFirework_34() { return &____aryContainerFirework_34; }
	inline void set__aryContainerFirework_34(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		____aryContainerFirework_34 = value;
		Il2CppCodeGenWriteBarrier((&____aryContainerFirework_34), value);
	}

	inline static int32_t get_offset_of_m_sprTest_35() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_sprTest_35)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_sprTest_35() const { return ___m_sprTest_35; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_sprTest_35() { return &___m_sprTest_35; }
	inline void set_m_sprTest_35(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_sprTest_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_sprTest_35), value);
	}

	inline static int32_t get_offset_of_m_nCurLevel_36() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_nCurLevel_36)); }
	inline int32_t get_m_nCurLevel_36() const { return ___m_nCurLevel_36; }
	inline int32_t* get_address_of_m_nCurLevel_36() { return &___m_nCurLevel_36; }
	inline void set_m_nCurLevel_36(int32_t value)
	{
		___m_nCurLevel_36 = value;
	}

	inline static int32_t get_offset_of_m_lstEffects_37() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_lstEffects_37)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_m_lstEffects_37() const { return ___m_lstEffects_37; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_m_lstEffects_37() { return &___m_lstEffects_37; }
	inline void set_m_lstEffects_37(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___m_lstEffects_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_lstEffects_37), value);
	}

	inline static int32_t get_offset_of_m_fTimeElapse_38() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fTimeElapse_38)); }
	inline float get_m_fTimeElapse_38() const { return ___m_fTimeElapse_38; }
	inline float* get_address_of_m_fTimeElapse_38() { return &___m_fTimeElapse_38; }
	inline void set_m_fTimeElapse_38(float value)
	{
		___m_fTimeElapse_38 = value;
	}

	inline static int32_t get_offset_of_m_fMovement_39() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_fMovement_39)); }
	inline float get_m_fMovement_39() const { return ___m_fMovement_39; }
	inline float* get_address_of_m_fMovement_39() { return &___m_fMovement_39; }
	inline void set_m_fMovement_39(float value)
	{
		___m_fMovement_39 = value;
	}

	inline static int32_t get_offset_of_m_nCount_40() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B, ___m_nCount_40)); }
	inline int32_t get_m_nCount_40() const { return ___m_nCount_40; }
	inline int32_t* get_address_of_m_nCount_40() { return &___m_nCount_40; }
	inline void set_m_nCount_40(int32_t value)
	{
		___m_nCount_40 = value;
	}
};

struct UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields
{
public:
	// UnlockNewPlaneManager UnlockNewPlaneManager::s_Instance
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * ___s_Instance_4;
	// UnityEngine.Vector3 UnlockNewPlaneManager::vecTempPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempPos_15;
	// UnityEngine.Vector3 UnlockNewPlaneManager::vecTempScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___vecTempScale_16;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields, ___s_Instance_4)); }
	inline UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * get_s_Instance_4() const { return ___s_Instance_4; }
	inline UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}

	inline static int32_t get_offset_of_vecTempPos_15() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields, ___vecTempPos_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempPos_15() const { return ___vecTempPos_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempPos_15() { return &___vecTempPos_15; }
	inline void set_vecTempPos_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempPos_15 = value;
	}

	inline static int32_t get_offset_of_vecTempScale_16() { return static_cast<int32_t>(offsetof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields, ___vecTempScale_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_vecTempScale_16() const { return ___vecTempScale_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_vecTempScale_16() { return &___vecTempScale_16; }
	inline void set_vecTempScale_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___vecTempScale_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNLOCKNEWPLANEMANAGER_T402DEC9E426B680014E232E712D172F77F23341B_H
#ifndef WEATHERMANAGER_T77BC50C3104A8AAC79F5FEA210AC31B904ECD196_H
#define WEATHERMANAGER_T77BC50C3104A8AAC79F5FEA210AC31B904ECD196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WeatherManager
struct  WeatherManager_t77BC50C3104A8AAC79F5FEA210AC31B904ECD196  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject WeatherManager::m_preSnowFlake
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_preSnowFlake_4;

public:
	inline static int32_t get_offset_of_m_preSnowFlake_4() { return static_cast<int32_t>(offsetof(WeatherManager_t77BC50C3104A8AAC79F5FEA210AC31B904ECD196, ___m_preSnowFlake_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_preSnowFlake_4() const { return ___m_preSnowFlake_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_preSnowFlake_4() { return &___m_preSnowFlake_4; }
	inline void set_m_preSnowFlake_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_preSnowFlake_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_preSnowFlake_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEATHERMANAGER_T77BC50C3104A8AAC79F5FEA210AC31B904ECD196_H
#ifndef SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#define SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Navigation_5)); }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Colors_7)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_SpriteState_8)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_TargetGraphic_11)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CanvasGroupCache_17)); }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t64BA96BFC713F221050385E91C868CE455C245D6 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t64BA96BFC713F221050385E91C868CE455C245D6 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_List_4)); }
	inline List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_tC6550F4D86CF67D987B6B46F46941B36D02A9680 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_TAA9065030FE0468018DEC880302F95FEA9C0133A_H
#ifndef BUTTON_T1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_H
#define BUTTON_T1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button
struct  Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * ___m_OnClick_18;

public:
	inline static int32_t get_offset_of_m_OnClick_18() { return static_cast<int32_t>(offsetof(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B, ___m_OnClick_18)); }
	inline ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * get_m_OnClick_18() const { return ___m_OnClick_18; }
	inline ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB ** get_address_of_m_OnClick_18() { return &___m_OnClick_18; }
	inline void set_m_OnClick_18(ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * value)
	{
		___m_OnClick_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnClick_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_H
#ifndef UICUBERTREEBUTTON_T85CFE701D7D8BFA0A50BE98BF38F04256B696C68_H
#define UICUBERTREEBUTTON_T85CFE701D7D8BFA0A50BE98BF38F04256B696C68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICuberTreeButton
struct  UICuberTreeButton_t85CFE701D7D8BFA0A50BE98BF38F04256B696C68  : public Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B
{
public:
	// System.Boolean UICuberTreeButton::m_bPlayDefaultClickAudio
	bool ___m_bPlayDefaultClickAudio_19;
	// UnityEngine.AudioSource UICuberTreeButton::_audioClickMe
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ____audioClickMe_20;
	// System.Single UICuberTreeButton::m_fShit
	float ___m_fShit_21;

public:
	inline static int32_t get_offset_of_m_bPlayDefaultClickAudio_19() { return static_cast<int32_t>(offsetof(UICuberTreeButton_t85CFE701D7D8BFA0A50BE98BF38F04256B696C68, ___m_bPlayDefaultClickAudio_19)); }
	inline bool get_m_bPlayDefaultClickAudio_19() const { return ___m_bPlayDefaultClickAudio_19; }
	inline bool* get_address_of_m_bPlayDefaultClickAudio_19() { return &___m_bPlayDefaultClickAudio_19; }
	inline void set_m_bPlayDefaultClickAudio_19(bool value)
	{
		___m_bPlayDefaultClickAudio_19 = value;
	}

	inline static int32_t get_offset_of__audioClickMe_20() { return static_cast<int32_t>(offsetof(UICuberTreeButton_t85CFE701D7D8BFA0A50BE98BF38F04256B696C68, ____audioClickMe_20)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get__audioClickMe_20() const { return ____audioClickMe_20; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of__audioClickMe_20() { return &____audioClickMe_20; }
	inline void set__audioClickMe_20(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		____audioClickMe_20 = value;
		Il2CppCodeGenWriteBarrier((&____audioClickMe_20), value);
	}

	inline static int32_t get_offset_of_m_fShit_21() { return static_cast<int32_t>(offsetof(UICuberTreeButton_t85CFE701D7D8BFA0A50BE98BF38F04256B696C68, ___m_fShit_21)); }
	inline float get_m_fShit_21() const { return ___m_fShit_21; }
	inline float* get_address_of_m_fShit_21() { return &___m_fShit_21; }
	inline void set_m_fShit_21(float value)
	{
		___m_fShit_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICUBERTREEBUTTON_T85CFE701D7D8BFA0A50BE98BF38F04256B696C68_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (delegateMoveEnd_t1F76CEA26063F0B11A971CAB2747FA1BFA8241F7), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6), -1, sizeof(UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2801[7] = 
{
	UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6_StaticFields::get_offset_of_vecTempPos_4(),
	UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6_StaticFields::get_offset_of_vecTempScale_5(),
	UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6::get_offset_of_m_containerTemp_6(),
	UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6::get_offset_of__vlg_7(),
	UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6::get_offset_of_m_fHoriGap_8(),
	UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6::get_offset_of_m_fVertGap_9(),
	UIItemBagContainer_t1A192CE450A13125409D3FD893ED1E4BF6B213B6::get_offset_of_m_lstItems_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[2] = 
{
	MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF::get_offset_of__imgIcon_4(),
	MoneyCounter_tD573C40FE0104CF84E8397A6D579A61ADB80B5AF::get_offset_of__txtValue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2803[1] = 
{
	SceneUiButton_t111438FEF8993259A9F0F2221A4AD4CE08C48507::get_offset_of_OnClickEvent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (OnClickEventHandler_t0FB1C8529204CFF772DD0008958C94A36A9ECB86), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (SceneUiPrice_tDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[2] = 
{
	SceneUiPrice_tDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2::get_offset_of__srMoneyTypeIcon_4(),
	SceneUiPrice_tDB576D2BE2EF300BC483CC86EE35EEDDEDBC9AC2::get_offset_of__tmValue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[11] = 
{
	SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13::get_offset_of__txtStatus_4(),
	SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13::get_offset_of__txtLeftTime_5(),
	SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13::get_offset_of__txtLevel_6(),
	SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13::get_offset_of__txtCurValue_7(),
	SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13::get_offset_of_m_eType_8(),
	SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13::get_offset_of_m_nDuration_9(),
	SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13::get_offset_of_m_nColdDown_10(),
	SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13::get_offset_of_m_fColdDownCount_11(),
	SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13::get_offset_of_m_DurationCount_12(),
	SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13::get_offset_of_m_fTimeElapse_13(),
	SceneUiSkillButton_t9E9A95B19B33D952277A3910F275A4C2BA2F5A13::get_offset_of_m_BoundSkill_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555), -1, sizeof(ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2807[8] = 
{
	ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555_StaticFields::get_offset_of_vecTempPos_4(),
	ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555_StaticFields::get_offset_of_vecTempScale_5(),
	ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555::get_offset_of_m_goItemsContainer_6(),
	ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555::get_offset_of_m_vecStartPos_7(),
	ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555::get_offset_of_m_nCurNum_8(),
	ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555::get_offset_of_m_nRow_9(),
	ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555::get_offset_of_m_lstItems_10(),
	ShoppinMallTypeContainer_t47FF2B55688E7E26C694A9F2B02FE76CE9E37555::get_offset_of__txtTypeName_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2808[16] = 
{
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of__txtName_4(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of__txtQuality_5(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of__txtDuration_6(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of__txtDesc_7(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of__btnUseOrCancel_8(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of__btnSell_9(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of__btnCancel_10(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of__imgAvatar_11(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of__imgBg_12(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of__imgSkillIcon_13(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of__txtOperateTitle_14(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of__progressbarColdDown_15(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of_m_Config_16(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of_m_BoundAdmin_17(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of_m_bUsing_18(),
	UIAdministratorCounter_tB0779CA5FD21487575278D829A0AEC52F698DA78::get_offset_of_m_sprSmallAvatar_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (UIAdventureCounter_tE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[2] = 
{
	UIAdventureCounter_tE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C::get_offset_of_m_nDuration_4(),
	UIAdventureCounter_tE0C6DFFE3A3304286F70E3CEA3F16DE53C8CD28C::get_offset_of_m_aryProfitList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (UIAdventureProfitCounter_t6EDEEB336BA9A03676339D2FA1C2F30612613B8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[4] = 
{
	UIAdventureProfitCounter_t6EDEEB336BA9A03676339D2FA1C2F30612613B8D::get_offset_of_m_eProfitType_4(),
	UIAdventureProfitCounter_t6EDEEB336BA9A03676339D2FA1C2F30612613B8D::get_offset_of_m_nItemId_5(),
	UIAdventureProfitCounter_t6EDEEB336BA9A03676339D2FA1C2F30612613B8D::get_offset_of_m_nValue0_6(),
	UIAdventureProfitCounter_t6EDEEB336BA9A03676339D2FA1C2F30612613B8D::get_offset_of_m_szName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[3] = 
{
	UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F::get_offset_of__txtName_4(),
	UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F::get_offset_of__txtNum_5(),
	UIAdventureRewardShow_tC98CDF0A1478D1585512C351DF8B6106FCB4C93F::get_offset_of__imgAvatar_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (UICuberTreeButton_t85CFE701D7D8BFA0A50BE98BF38F04256B696C68), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[3] = 
{
	UICuberTreeButton_t85CFE701D7D8BFA0A50BE98BF38F04256B696C68::get_offset_of_m_bPlayDefaultClickAudio_19(),
	UICuberTreeButton_t85CFE701D7D8BFA0A50BE98BF38F04256B696C68::get_offset_of__audioClickMe_20(),
	UICuberTreeButton_t85CFE701D7D8BFA0A50BE98BF38F04256B696C68::get_offset_of_m_fShit_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[30] = 
{
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__containerEffect_4(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__goBlock_5(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__imgBlock_bg_6(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__imgBlock_Main_7(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__containerMainContent_8(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__imgBlock_9(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__block_10(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__Content_11(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__imgQuestionMark_12(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__containerUnlockPrice_13(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__containerUnLockPrice_14(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__containerProfitAndTime_15(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__txtIsCurDistrict_16(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__subcontainerProfitAndTime_17(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__starsPrestige_18(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__txtTrackName_19(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__txtCurOfflineGain_20(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__txtAdsProfitLeftTime_21(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__imgLock_22(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__txtUnlockPrice_23(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__imgUnlockCoinType_24(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__imgProfitCoinType_25(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__imgDataBg_26(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of__btnEnter_27(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of_m_District_28(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of_m_eStatus_29(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of_m_bCanUnlock_30(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of_m_bLocked_31(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of_m_fBtnEnterShowTime_32(),
	UIBigMapTrackInfo_tD1567A742038A1B490AA47873BC98DF75421C69E::get_offset_of_m_bClickMe_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (UIBuffCounter_t2E5C624662362FB38268B941481E453A4FAD4E26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[2] = 
{
	UIBuffCounter_t2E5C624662362FB38268B941481E453A4FAD4E26::get_offset_of__txtValue_4(),
	UIBuffCounter_t2E5C624662362FB38268B941481E453A4FAD4E26::get_offset_of__imgIcon_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[18] = 
{
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__lock_4(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of_m_bLocked_5(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of_m_bCanUnlock_6(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of_m_District_7(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__goWenHao_8(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__txtUnlockPrice_9(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__txtTrackName_10(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__goPrice_11(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__imgMoneyIcon_12(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__imgMoneyIcon_ShouYi_13(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__txtTiSheng_14(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__txtShouYi_15(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__contaainerShouYi_16(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__containerTiShengAndShouYi_17(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__imgBg_18(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__txtIsCurDistrict_19(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of__btnEnter_20(),
	UIDistrict_tD97DBD1AB5F38BFA0D3874612FB7B7CF5BA4CCB7::get_offset_of_m_fEnterButtonShowTime_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2), -1, sizeof(UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2816[12] = 
{
	UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2_StaticFields::get_offset_of_vecTempPos_4(),
	UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2::get_offset_of_m_vecStartPos_5(),
	UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2::get_offset_of_m_vecEndPos_6(),
	UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2::get_offset_of_m_fSession0_Time_7(),
	UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2::get_offset_of_m_fSession1_Time_8(),
	UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2::get_offset_of_m_vecSession0End_9(),
	UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2::get_offset_of_m_vecSession0_A_10(),
	UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2::get_offset_of_m_vecSession1_A_11(),
	UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2::get_offset_of_m_vecVelocity_12(),
	UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2::get_offset_of_m_nStatus_13(),
	UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2_StaticFields::get_offset_of_s_ShitCount_14(),
	UIFlyingCoin_t8763DFF8DEB3473D5330B0517A0D33B063DBE0D2::get_offset_of__imgMain_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2817[37] = 
{
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_bInBag_4(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__panelCounting_5(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__txtName_6(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__txtDesc_7(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__txtValue_8(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__txtPrice_Coin_9(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__txtPrice_Diamond_10(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__txtPrice1_11(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__txtPrice2_12(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__imgMoneyIcon1_13(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__imgMoneyIcon2_14(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__goPrice1_15(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__goPrice2_16(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__txtNum_17(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__txtLeftTime_18(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of__btnUse_19(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_eItemType_20(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_ePriceType_21(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_ePriceSubType_22(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_nItemId_23(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_nPrice_Coin_24(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_fCoinPriceRiseAfterBuy_25(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_nPrice_Diamond_26(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_nValue0_27(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_nDuration_28(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_fStartTime_29(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_Config_30(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_BagItemConfig_31(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_nNum_32(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_aryIntParams_33(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_aryFloatParams_34(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_nLeftTime_35(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_eToBuy_MoneyType_36(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_nToBuy_SubMoneyType_37(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_nToBuy_Price_38(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_nToBuy_Num_39(),
	UIItem_t14D41FF84CDBA85F0EC99C29A710A1D348A61957::get_offset_of_m_ToBuyConfig_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2818[7] = 
{
	UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6::get_offset_of__txtFuncValue_4(),
	UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6::get_offset_of__txtLeftTime_5(),
	UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6::get_offset_of__imgAvatar_6(),
	UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6::get_offset_of_m_nItemId_7(),
	UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6::get_offset_of_m_nValue0_8(),
	UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6::get_offset_of_m_nDuration_9(),
	UIItemInBag_t7F21A3C7BDBB6A681598F874C027E613F87CADE6::get_offset_of_m_StartTime_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2819[10] = 
{
	UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF::get_offset_of_m_aryUITrakcs_4(),
	UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF::get_offset_of__containerTrakcs_5(),
	UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF::get_offset_of__imgMain_6(),
	UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF::get_offset_of__containerUnlockPrice_7(),
	UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF::get_offset_of__txtUnlockPrice_8(),
	UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF::get_offset_of__imgUnlockIcon_9(),
	UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF::get_offset_of_m_bLocked_10(),
	UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF::get_offset_of_m_nPlanetId_11(),
	UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF::get_offset_of__btnClickMe_12(),
	UIMainLand_t6619E1A1161085203FE4F2B87F1CDC289144D7CF::get_offset_of_m_bClickMe_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C), -1, sizeof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2820[44] = 
{
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields::get_offset_of_s_Instance_4(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__goStart_5(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__goStart_Admin_6(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__goEnd_7(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__goMiddleLeft_8(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__goMiddleRight_9(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_aryVariousUI_10(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_aryVariousUI_Center_11(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_aryVariousUI_Bottom_12(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_vecTempInitPosOfTopButtons_13(),
	0,
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields::get_offset_of_vecTempPos_15(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields::get_offset_of_vecTempScale_16(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_vecScreenBottom_World_17(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_vecBottomButtonsInitPos_18(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_vecGainAndCollect_19(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_vecAdventureAndScience_20(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_vecSubContainerForBigMap_21(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_fOffsetToScreenBottom_World_22(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__uiTestKnob_23(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__uiTopButtons_24(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__uiBottomButtons_25(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__uiDPS_26(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__uiCPosButtons_27(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__uiGainAndCollect_28(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__uiAdventureAndScience_29(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__uiSubContainerForBigMap_30(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__uiSubContainerForBigMap_Content_31(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__canvasMain_32(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__UI_33(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__canvasScaler_34(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_vecCoinFlySeesion0EndPosRangeX_35(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_vecCoinFlySeesion0EndPosRangeY_36(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_vecCoinFlyStartPos_37(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_vecCoinFlyEndPos_38(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_fSession0Time_39(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_fSession1Time_40(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__containerFlyingCoins_41(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_arySomeCtrlsDueToXingKong_42(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of__goEffectClick_43(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_aryUICtrlPos_1920_1080_44(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_aryUICtrlPos_2436_1125_45(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_aryUICtrl_46(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_aryResolution_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (eUiId_tF5176DFF3CB5188E73B7887069C38DF36A4CF137)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2821[10] = 
{
	eUiId_tF5176DFF3CB5188E73B7887069C38DF36A4CF137::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA), -1, sizeof(UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2822[9] = 
{
	UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA::get_offset_of_m_eType_4(),
	UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA_StaticFields::get_offset_of_s_Instance_5(),
	UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA::get_offset_of__goContainerMain_6(),
	UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA::get_offset_of__containerOnlyOk_7(),
	UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA::get_offset_of__containerYesNo_8(),
	UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA::get_offset_of__txtMsgOnlyOk_9(),
	UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA::get_offset_of__txtMsgYesNo_10(),
	UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA::get_offset_of_m_fTimeElapse_11(),
	UIMsgBox_tFE0DEA91E7249781144314FA42C7521ADEC5E0AA::get_offset_of__BoundResearchCounter_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (eCallBackEventType_tC080966288865EC90905EB86CDA59D5E3F5C38FA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2823[4] = 
{
	eCallBackEventType_tC080966288865EC90905EB86CDA59D5E3F5C38FA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2824[10] = 
{
	UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A::get_offset_of__lock_4(),
	UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A::get_offset_of_m_bLocked_5(),
	UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A::get_offset_of_m_nPlanetId_6(),
	UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A::get_offset_of_m_ePlanetType_7(),
	UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A::get_offset_of__txtUnlockPrice_8(),
	UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A::get_offset_of__txtPlanetName_9(),
	UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A::get_offset_of__goLock_10(),
	UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A::get_offset_of__goPrice_11(),
	UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A::get_offset_of__goButtonMain_12(),
	UIPlanet_tA72D11E981757CAB07BA519432A793E2BF92E38A::get_offset_of__BaseRotate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (eUIPlanetType_tFEF94613B677EB7537CEDB6472F772D9D8100A8E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2825[5] = 
{
	eUIPlanetType_tFEF94613B677EB7537CEDB6472F772D9D8100A8E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C), -1, sizeof(UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2826[20] = 
{
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C_StaticFields::get_offset_of_s_Instance_4(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of_m_colorCoinNotEnough_5(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of_m_colorGray_6(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__txtCurGainTimes_7(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__txtNextGainTimes_8(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__txtCost_9(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__btnDoPrestige_10(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__imgDoPrestige_Image_11(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__txtDoPrestige_Text_12(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__txtCoinKeep_13(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__imgCostCoinIcon_14(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__imgKeepCoinIcon_15(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__txtKeepSkillPoint_16(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__imgKeepSkillPointIcon_17(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__txtKeepDiamond_18(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__starsCur_19(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of__starsNext_20(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of_m_containerCostAndKeep_21(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of_m_bCoinNotEnough_22(),
	UIPrestige_t2BDF11E128EBC48DB6215DE038F0501D99AC566C::get_offset_of_m_dRealPrestigeCoin_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (UIProgressBar_t2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[2] = 
{
	UIProgressBar_t2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96::get_offset_of__imgBar_4(),
	UIProgressBar_t2FF6D8395FB020C8D2EC33F23235E7BF2FE38B96::get_offset_of__txtContent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169), -1, sizeof(UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2828[33] = 
{
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169_StaticFields::get_offset_of_vecTempPos_4(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169_StaticFields::get_offset_of_vecTempScale_5(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__btnBuy_6(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__btnUse_7(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__containerOneButton_8(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__containerTwoButton_9(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__containerItemUsingStatus_10(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__imgCoinIcon_11(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__imgGreenCashIcon_12(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__txtLeftTime_13(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__txtDuration_14(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__txtValue_15(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__txtNum_16(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__aryTxtPriceDiamond_17(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__txtPriceCoin_18(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__txtPriceDiamond_19(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__imgAvatar_20(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of__imgProgressBarFill_21(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_fValue_22(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_dCoinPrice_23(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_nDiamondPrice_24(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_nDuration_25(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_nItemType_26(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_nItemSubType_27(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_szFuncDest_28(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_bItemInBag_29(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_nNum_30(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_nItemId_31(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_dateStartTime_32(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_bWorking_33(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_Config_34(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_eItemType_35(),
	UIShoppinAndItemCounter_tC16F00AAEA618662EE5AD9105A70A000C6AB9169::get_offset_of_m_fTimeElapse_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[8] = 
{
	UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324::get_offset_of__txtLeftTime_4(),
	UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324::get_offset_of__txtValue_5(),
	UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324::get_offset_of__imgProgress_6(),
	UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324::get_offset_of__imgColdDownMask_7(),
	UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324::get_offset_of__effectLight_8(),
	UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324::get_offset_of_m_eType_9(),
	UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324::get_offset_of_m_BoundSkill_10(),
	UISkillCastCounter_t247CA0C3EB0CEA299A1BA446A51261D42690D324::get_offset_of__baseScale_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[6] = 
{
	UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B::get_offset_of_m_nStarType_4(),
	UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B::get_offset_of_m_sprStarNormal_5(),
	UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B::get_offset_of_m_sprStarEmpty_6(),
	UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B::get_offset_of__imgGrayStar_7(),
	UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B::get_offset_of_m_aryType0Stars_8(),
	UIStars_t895A62A22BCDD8808F59819C10C25D2A4095DC5B::get_offset_of_m_aryType1StarsContainer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46), -1, sizeof(UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2831[38] = 
{
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46_StaticFields::get_offset_of_colorTemp_4(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__containerPriceOff_5(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtPriceOff_6(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtQuestionMark0_7(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtQuestionMark1_8(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__containerUnlockLevelAvatar_9(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__imgUnlockLevelAvatar_10(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__imgLock_11(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtTitleSpeed_12(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtTitleGain_13(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtLevel_14(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__imgAvatar_15(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtSpeed_16(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtGain_17(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtPrice_18(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtInitialPrice_19(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtSkillDiscount_20(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtScienceDiscount_21(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtName_22(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__txtUnLockLevel_23(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__imgProgressBar_Gain_24(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__imgProgressBar_Speed_25(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__imgBtnBuy_26(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__imgCoinType_27(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__containerGainAndSpeed_28(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__containerDiscount_29(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__containerPrice_30(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__btnBuy_31(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of__containerResearchLock_32(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of_m_nVehicleLevel_33(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of_m_nPrice_34(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of_m_nDiamondPrice_35(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of_m_bWatchAdFree_36(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of_m_eMoneyType_37(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of_m_sprJianYingTemp_38(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of_m_Config_39(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of_m_bUnlocked_40(),
	UIVehicleCounter_tC0C616D4332173796831C3199722C223A8D33D46::get_offset_of_m_BoundResearchCounter_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[9] = 
{
	UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95::get_offset_of__txtPlanetAndDistrict_4(),
	UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95::get_offset_of__imgPlanetAvatar_5(),
	UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95::get_offset_of__txtPrestigeRaise_6(),
	UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95::get_offset_of__txtDistrictRaise_7(),
	UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95::get_offset_of__imgCoinIcon_8(),
	UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95::get_offset_of__txtOfflineDps_9(),
	UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95::get_offset_of__txtCurPrestigeTimes_10(),
	UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95::get_offset_of_m_nPlanetId_11(),
	UIZengShouCounter_t35FB691FBD7229E775905E79FA0CA9E20992CA95::get_offset_of_m_nDistrictId_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B), -1, sizeof(UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2833[37] = 
{
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields::get_offset_of_s_Instance_4(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__txtTapToExit_5(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__btnClose_6(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__skeTwoAuto_7(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__skeNewAuto_8(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__skeEffect1_9(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__skeEffect2_10(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_containerEffect_11(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_containerLighning_12(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_containerFoGuang_13(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_preFoGuang_14(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields::get_offset_of_vecTempPos_15(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B_StaticFields::get_offset_of_vecTempScale_16(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__containerAll_17(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__srPlane1_18(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__srPlane2_19(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__srNewPlane_20(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_arySessionTime_21(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_vecPlane1StartPos_22(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_vecPlane2StartPos_23(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_fCrashDistance_24(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_fBackSpeed_25(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_fBackDistance_26(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_fCrashA_27(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_fCrashSpeed_28(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_nStatus_29(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__goLight_30(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__baseScaleNewPlane_31(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__containerExplosionEffect_32(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__effectFoGuang_33(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of__aryContainerFirework_34(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_sprTest_35(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_nCurLevel_36(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_lstEffects_37(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_fTimeElapse_38(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_fMovement_39(),
	UnlockNewPlaneManager_t402DEC9E426B680014E232E712D172F77F23341B::get_offset_of_m_nCount_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70), -1, sizeof(SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2834[10] = 
{
	SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_StaticFields::get_offset_of_vecTempPos_4(),
	SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_StaticFields::get_offset_of_vecTempScale_5(),
	SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70_StaticFields::get_offset_of_colorTemp_6(),
	SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70::get_offset_of__srMain_7(),
	SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70::get_offset_of_m_fDropEndPosY_8(),
	SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70::get_offset_of_m_fAlphaChangePerFrame_9(),
	SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70::get_offset_of_m_fBeginFadePosY_10(),
	SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70::get_offset_of_m_fCurAngle_11(),
	SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70::get_offset_of_m_fStartPosX_12(),
	SnowFlake_t5AAB54B222D115B6FCC34BC7FD10E4A79C622E70::get_offset_of_m_fStartPosY_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (WeatherManager_t77BC50C3104A8AAC79F5FEA210AC31B904ECD196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2835[1] = 
{
	WeatherManager_t77BC50C3104A8AAC79F5FEA210AC31B904ECD196::get_offset_of_m_preSnowFlake_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (SpineSkeletonFlipBehaviour_t881D1AD276451B7A71EDB55CC0849B6F8E71EB5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2836[2] = 
{
	SpineSkeletonFlipBehaviour_t881D1AD276451B7A71EDB55CC0849B6F8E71EB5C::get_offset_of_flipX_0(),
	SpineSkeletonFlipBehaviour_t881D1AD276451B7A71EDB55CC0849B6F8E71EB5C::get_offset_of_flipY_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (SpineSkeletonFlipClip_tD9E93640501997B27D28C96B18FBFE6E817D73F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[1] = 
{
	SpineSkeletonFlipClip_tD9E93640501997B27D28C96B18FBFE6E817D73F6::get_offset_of_template_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2838[6] = 
{
	Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122::get_offset_of_U3ClineNumberU3Ek__BackingField_0(),
	Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122::get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1(),
	Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122::get_offset_of_json_2(),
	Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122::get_offset_of_index_3(),
	Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122::get_offset_of_success_4(),
	Lexer_t93D12C38ED5395C9FCB0C20C62C208E046DE1122::get_offset_of_stringBuffer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (Token_tF9F61D56B1A37EACCC9FDBF91EDD4606F3440BA0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2839[13] = 
{
	Token_tF9F61D56B1A37EACCC9FDBF91EDD4606F3440BA0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (JsonDecoder_t0C1C57352FB78AB1682A11F1E1E229F2B152DA62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2840[3] = 
{
	JsonDecoder_t0C1C57352FB78AB1682A11F1E1E229F2B152DA62::get_offset_of_U3CerrorMessageU3Ek__BackingField_0(),
	JsonDecoder_t0C1C57352FB78AB1682A11F1E1E229F2B152DA62::get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1(),
	JsonDecoder_t0C1C57352FB78AB1682A11F1E1E229F2B152DA62::get_offset_of_lexer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2841[3] = 
{
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482::get_offset_of_timelines_0(),
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482::get_offset_of_duration_1(),
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (MixPose_t65B6F539ED15C820349F7845BA4977ADFC03C1F5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2843[4] = 
{
	MixPose_t65B6F539ED15C820349F7845BA4977ADFC03C1F5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (MixDirection_tE1DA6601177FB68DD7DB387CD2C818140069C900)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2844[3] = 
{
	MixDirection_tE1DA6601177FB68DD7DB387CD2C818140069C900::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (TimelineType_t1FD5B4020300DAA50249E7BAB7328A7FEFC68141)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2845[16] = 
{
	TimelineType_t1FD5B4020300DAA50249E7BAB7328A7FEFC68141::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[5] = 
{
	0,
	0,
	0,
	0,
	CurveTimeline_tF597BE88014A4EA50D23D6D5394E7786C19A153E::get_offset_of_curves_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (RotateTimeline_t7A18C0AE969708B70CCAC154518EC5FD81ABFCF1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2847[6] = 
{
	0,
	0,
	0,
	0,
	RotateTimeline_t7A18C0AE969708B70CCAC154518EC5FD81ABFCF1::get_offset_of_boneIndex_9(),
	RotateTimeline_t7A18C0AE969708B70CCAC154518EC5FD81ABFCF1::get_offset_of_frames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (TranslateTimeline_t35CE691D668D75F2779A2D495380C7F6E563FF88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2848[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TranslateTimeline_t35CE691D668D75F2779A2D495380C7F6E563FF88::get_offset_of_boneIndex_11(),
	TranslateTimeline_t35CE691D668D75F2779A2D495380C7F6E563FF88::get_offset_of_frames_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (ScaleTimeline_t1011924EB2AF4FB783DF13AFF3DBF05DB117DEF9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (ShearTimeline_t35FAC5DCDDF9CBE1989938FDDFE4A5E3A33005C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (ColorTimeline_t3C9AD36709A96F0AE371BB3B60F339089D32707F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2851[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ColorTimeline_t3C9AD36709A96F0AE371BB3B60F339089D32707F::get_offset_of_slotIndex_15(),
	ColorTimeline_t3C9AD36709A96F0AE371BB3B60F339089D32707F::get_offset_of_frames_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (TwoColorTimeline_t69FAE8EC5AB6A7E367BA935157DF7B1EDE94F11E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2852[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TwoColorTimeline_t69FAE8EC5AB6A7E367BA935157DF7B1EDE94F11E::get_offset_of_slotIndex_21(),
	TwoColorTimeline_t69FAE8EC5AB6A7E367BA935157DF7B1EDE94F11E::get_offset_of_frames_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (AttachmentTimeline_t8770B00FFFE84AE5CFA264C26D56C15BEF924A2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2853[3] = 
{
	AttachmentTimeline_t8770B00FFFE84AE5CFA264C26D56C15BEF924A2A::get_offset_of_slotIndex_0(),
	AttachmentTimeline_t8770B00FFFE84AE5CFA264C26D56C15BEF924A2A::get_offset_of_frames_1(),
	AttachmentTimeline_t8770B00FFFE84AE5CFA264C26D56C15BEF924A2A::get_offset_of_attachmentNames_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (DeformTimeline_tABB31F393685EF4297F9372BBA5D31828B76BAB2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2854[4] = 
{
	DeformTimeline_tABB31F393685EF4297F9372BBA5D31828B76BAB2::get_offset_of_slotIndex_5(),
	DeformTimeline_tABB31F393685EF4297F9372BBA5D31828B76BAB2::get_offset_of_frames_6(),
	DeformTimeline_tABB31F393685EF4297F9372BBA5D31828B76BAB2::get_offset_of_frameVertices_7(),
	DeformTimeline_tABB31F393685EF4297F9372BBA5D31828B76BAB2::get_offset_of_attachment_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (EventTimeline_tBF409E0B4D5D35510A43CFE2F4C660A552F69AAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2855[2] = 
{
	EventTimeline_tBF409E0B4D5D35510A43CFE2F4C660A552F69AAF::get_offset_of_frames_0(),
	EventTimeline_tBF409E0B4D5D35510A43CFE2F4C660A552F69AAF::get_offset_of_events_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (DrawOrderTimeline_t4604A14ED147349C259089CA32C00D526E32A98A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[2] = 
{
	DrawOrderTimeline_t4604A14ED147349C259089CA32C00D526E32A98A::get_offset_of_frames_0(),
	DrawOrderTimeline_t4604A14ED147349C259089CA32C00D526E32A98A::get_offset_of_drawOrders_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (IkConstraintTimeline_t804DB850BFAB66F35538D64EAB52B792E4A61689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2857[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	IkConstraintTimeline_t804DB850BFAB66F35538D64EAB52B792E4A61689::get_offset_of_ikConstraintIndex_11(),
	IkConstraintTimeline_t804DB850BFAB66F35538D64EAB52B792E4A61689::get_offset_of_frames_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (TransformConstraintTimeline_t96F58F0669906A6E5EE9956621786488E145FD56), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2858[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TransformConstraintTimeline_t96F58F0669906A6E5EE9956621786488E145FD56::get_offset_of_transformConstraintIndex_15(),
	TransformConstraintTimeline_t96F58F0669906A6E5EE9956621786488E145FD56::get_offset_of_frames_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (PathConstraintPositionTimeline_t067633A6B9AE48EF83FE73BBC66EA1DC47D7DACE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2859[6] = 
{
	0,
	0,
	0,
	0,
	PathConstraintPositionTimeline_t067633A6B9AE48EF83FE73BBC66EA1DC47D7DACE::get_offset_of_pathConstraintIndex_9(),
	PathConstraintPositionTimeline_t067633A6B9AE48EF83FE73BBC66EA1DC47D7DACE::get_offset_of_frames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (PathConstraintSpacingTimeline_tDEA29E775F5ECAB1F8E09235AFEDBD614CF83796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (PathConstraintMixTimeline_t5612530CC2D6D7B270B68BD20BA063FB32934020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	PathConstraintMixTimeline_t5612530CC2D6D7B270B68BD20BA063FB32934020::get_offset_of_pathConstraintIndex_11(),
	PathConstraintMixTimeline_t5612530CC2D6D7B270B68BD20BA063FB32934020::get_offset_of_frames_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0), -1, sizeof(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2862[20] = 
{
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0_StaticFields::get_offset_of_EmptyAnimation_0(),
	0,
	0,
	0,
	0,
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_data_5(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_trackEntryPool_6(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_tracks_7(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_events_8(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_queue_9(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_propertyIDs_10(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_mixingTo_11(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_animationsChanged_12(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_timeScale_13(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_Start_14(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_Interrupt_15(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_End_16(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_Dispose_17(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_Complete_18(),
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0::get_offset_of_Event_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (TrackEntryDelegate_tE909F250DB1ADC27B3DF4A52297A94DC79C8825F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (TrackEntryEventDelegate_tED21C03D5D861C22FCC6214B387C9046174FA4DE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2865[32] = 
{
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_animation_0(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_next_1(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_mixingFrom_2(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_trackIndex_3(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_loop_4(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_eventThreshold_5(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_attachmentThreshold_6(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_drawOrderThreshold_7(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_animationStart_8(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_animationEnd_9(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_animationLast_10(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_nextAnimationLast_11(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_delay_12(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_trackTime_13(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_trackLast_14(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_nextTrackLast_15(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_trackEnd_16(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_timeScale_17(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_alpha_18(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_mixTime_19(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_mixDuration_20(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_interruptAlpha_21(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_totalAlpha_22(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_timelineData_23(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_timelineDipMix_24(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_timelinesRotation_25(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_Start_26(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_Interrupt_27(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_End_28(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_Dispose_29(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_Complete_30(),
	TrackEntry_tC06EF190EA88E3477B5CDDC2AABB2F01B6B12276::get_offset_of_Event_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[5] = 
{
	EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77::get_offset_of_eventQueueEntries_0(),
	EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77::get_offset_of_drainDisabled_1(),
	EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77::get_offset_of_state_2(),
	EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77::get_offset_of_trackEntryPool_3(),
	EventQueue_tD4F4CC04E5EB5E56DD504B0578F4D94C07D81D77::get_offset_of_AnimationsChanged_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (EventQueueEntry_t18A087AFF39EE175D3F8CF4897B97ABC9E8720CB)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[3] = 
{
	EventQueueEntry_t18A087AFF39EE175D3F8CF4897B97ABC9E8720CB::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventQueueEntry_t18A087AFF39EE175D3F8CF4897B97ABC9E8720CB::get_offset_of_entry_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventQueueEntry_t18A087AFF39EE175D3F8CF4897B97ABC9E8720CB::get_offset_of_e_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (EventType_tD27E3F173F4CA745D9BDD0CD35BF292009CB585F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2868[7] = 
{
	EventType_tD27E3F173F4CA745D9BDD0CD35BF292009CB585F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[3] = 
{
	AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C::get_offset_of_skeletonData_0(),
	AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C::get_offset_of_animationToMixTime_1(),
	AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C::get_offset_of_defaultMix_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (AnimationPair_tE3B0769553A08F90C7CCAE7F8D9011DFE541D349)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[2] = 
{
	AnimationPair_tE3B0769553A08F90C7CCAE7F8D9011DFE541D349::get_offset_of_a1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationPair_tE3B0769553A08F90C7CCAE7F8D9011DFE541D349::get_offset_of_a2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (AnimationPairComparer_t65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE), -1, sizeof(AnimationPairComparer_t65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2873[1] = 
{
	AnimationPairComparer_t65910C9A5CB659F4F2FD1BC204B3B6DCDE7E72AE_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[3] = 
{
	Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4::get_offset_of_pages_0(),
	Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4::get_offset_of_regions_1(),
	Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4::get_offset_of_textureLoader_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (Format_tB01BD241CBCEF4B723D11A239A0A1BD9CC5AFF4D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2875[8] = 
{
	Format_tB01BD241CBCEF4B723D11A239A0A1BD9CC5AFF4D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (TextureFilter_t8E51CE4FC4F71CCF888F884595E4A2A780879B68)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2876[8] = 
{
	TextureFilter_t8E51CE4FC4F71CCF888F884595E4A2A780879B68::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (TextureWrap_tAB8018DC28DE360488DA2417277C798FED128617)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2877[4] = 
{
	TextureWrap_tAB8018DC28DE360488DA2417277C798FED128617::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[9] = 
{
	AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453::get_offset_of_name_0(),
	AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453::get_offset_of_format_1(),
	AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453::get_offset_of_minFilter_2(),
	AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453::get_offset_of_magFilter_3(),
	AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453::get_offset_of_uWrap_4(),
	AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453::get_offset_of_vWrap_5(),
	AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453::get_offset_of_rendererObject_6(),
	AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453::get_offset_of_width_7(),
	AtlasPage_t38CC63272BC1C1FADD75F3D26DCE8E988D1A4453::get_offset_of_height_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[18] = 
{
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_page_0(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_name_1(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_x_2(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_y_3(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_width_4(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_height_5(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_u_6(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_v_7(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_u2_8(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_v2_9(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_offsetX_10(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_offsetY_11(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_originalWidth_12(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_originalHeight_13(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_index_14(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_rotate_15(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_splits_16(),
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30::get_offset_of_pads_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (AtlasAttachmentLoader_t38697609F0F3ABFC7F18657ADD40BFAA6131163D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2881[1] = 
{
	AtlasAttachmentLoader_t38697609F0F3ABFC7F18657ADD40BFAA6131163D::get_offset_of_atlasArray_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (Attachment_tCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2882[1] = 
{
	Attachment_tCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (AttachmentType_t1F679C7DFF0FA3CEDB51DD23B2EA3B4F1EFA90A8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2885[8] = 
{
	AttachmentType_t1F679C7DFF0FA3CEDB51DD23B2EA3B4F1EFA90A8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (BoundingBoxAttachment_tE2A96EC447B439870D918C9F72B9093D49707931), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (ClippingAttachment_tB72A005F3E80A08C9BF2D903BBD1D951342AB490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2887[1] = 
{
	ClippingAttachment_tB72A005F3E80A08C9BF2D903BBD1D951342AB490::get_offset_of_endSlot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[26] = 
{
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_regionOffsetX_7(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_regionOffsetY_8(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_regionWidth_9(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_regionHeight_10(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_regionOriginalWidth_11(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_regionOriginalHeight_12(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_parentMesh_13(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_uvs_14(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_regionUVs_15(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_triangles_16(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_r_17(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_g_18(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_b_19(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_a_20(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_hulllength_21(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_inheritDeform_22(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_U3CPathU3Ek__BackingField_23(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_U3CRendererObjectU3Ek__BackingField_24(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_U3CRegionUU3Ek__BackingField_25(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_U3CRegionVU3Ek__BackingField_26(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_U3CRegionU2U3Ek__BackingField_27(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_U3CRegionV2U3Ek__BackingField_28(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_U3CRegionRotateU3Ek__BackingField_29(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_U3CEdgesU3Ek__BackingField_30(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_U3CWidthU3Ek__BackingField_31(),
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392::get_offset_of_U3CHeightU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (PathAttachment_t7B1C838F67212A550EEF16DDA94F9DA8C9ED1733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2889[3] = 
{
	PathAttachment_t7B1C838F67212A550EEF16DDA94F9DA8C9ED1733::get_offset_of_lengths_7(),
	PathAttachment_t7B1C838F67212A550EEF16DDA94F9DA8C9ED1733::get_offset_of_closed_8(),
	PathAttachment_t7B1C838F67212A550EEF16DDA94F9DA8C9ED1733::get_offset_of_constantSpeed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[3] = 
{
	PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B::get_offset_of_x_1(),
	PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B::get_offset_of_y_2(),
	PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B::get_offset_of_rotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2891[29] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_x_9(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_y_10(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_rotation_11(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_scaleX_12(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_scaleY_13(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_width_14(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_height_15(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_regionOffsetX_16(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_regionOffsetY_17(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_regionWidth_18(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_regionHeight_19(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_regionOriginalWidth_20(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_regionOriginalHeight_21(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_offset_22(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_uvs_23(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_r_24(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_g_25(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_b_26(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_a_27(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_U3CPathU3Ek__BackingField_28(),
	RegionAttachment_tB39DEF9B637BEB578030B3FFD1CEE51973BAFEEC::get_offset_of_U3CRendererObjectU3Ek__BackingField_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631), -1, sizeof(VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2892[6] = 
{
	VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631_StaticFields::get_offset_of_nextID_1(),
	VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631_StaticFields::get_offset_of_nextIdLock_2(),
	VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631::get_offset_of_id_3(),
	VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631::get_offset_of_bones_4(),
	VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631::get_offset_of_vertices_5(),
	VertexAttachment_t3FD7C7E4270CEF2803E8C524DD6D4E91DB769631::get_offset_of_worldVerticesLength_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (BlendMode_t36C159CFDBBD6C33AFD2B2392A2EA71889DDD0E1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2893[5] = 
{
	BlendMode_t36C159CFDBBD6C33AFD2B2392A2EA71889DDD0E1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E), -1, sizeof(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2894[27] = 
{
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E_StaticFields::get_offset_of_yDown_0(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_data_1(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_skeleton_2(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_parent_3(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_children_4(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_x_5(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_y_6(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_rotation_7(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_scaleX_8(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_scaleY_9(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_shearX_10(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_shearY_11(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_ax_12(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_ay_13(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_arotation_14(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_ascaleX_15(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_ascaleY_16(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_ashearX_17(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_ashearY_18(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_appliedValid_19(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_a_20(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_b_21(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_worldX_22(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_c_23(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_d_24(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_worldY_25(),
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E::get_offset_of_sorted_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2895[12] = 
{
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F::get_offset_of_index_0(),
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F::get_offset_of_name_1(),
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F::get_offset_of_parent_2(),
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F::get_offset_of_length_3(),
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F::get_offset_of_x_4(),
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F::get_offset_of_y_5(),
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F::get_offset_of_rotation_6(),
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F::get_offset_of_scaleX_7(),
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F::get_offset_of_scaleY_8(),
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F::get_offset_of_shearX_9(),
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F::get_offset_of_shearY_10(),
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F::get_offset_of_transformMode_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (TransformMode_t468F038B8F71E82224721942A1009106EB740ED4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2896[6] = 
{
	TransformMode_t468F038B8F71E82224721942A1009106EB740ED4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2897[5] = 
{
	Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0::get_offset_of_data_0(),
	Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0::get_offset_of_time_1(),
	Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0::get_offset_of_intValue_2(),
	Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0::get_offset_of_floatValue_3(),
	Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0::get_offset_of_stringValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (EventData_t34B08BE715A229D34906F96784419D669DE4869E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2898[4] = 
{
	EventData_t34B08BE715A229D34906F96784419D669DE4869E::get_offset_of_name_0(),
	EventData_t34B08BE715A229D34906F96784419D669DE4869E::get_offset_of_U3CIntU3Ek__BackingField_1(),
	EventData_t34B08BE715A229D34906F96784419D669DE4869E::get_offset_of_U3CFloatU3Ek__BackingField_2(),
	EventData_t34B08BE715A229D34906F96784419D669DE4869E::get_offset_of_U3CStringU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2899[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
