﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Spine.Animation
struct Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482;
// Spine.AnimationState
struct AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0;
// Spine.AnimationStateData
struct AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C;
// Spine.Atlas
struct Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4;
// Spine.AtlasRegion
struct AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30;
// Spine.Attachment
struct Attachment_tCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92;
// Spine.AttachmentLoader
struct AttachmentLoader_t4ADC428394E503BC70091FE2A953C9BB743744B6;
// Spine.Bone
struct Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E;
// Spine.BoneData
struct BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F;
// Spine.BoundingBoxAttachment
struct BoundingBoxAttachment_tE2A96EC447B439870D918C9F72B9093D49707931;
// Spine.ClippingAttachment
struct ClippingAttachment_tB72A005F3E80A08C9BF2D903BBD1D951342AB490;
// Spine.Event
struct Event_tC2DFE0AAE240359A9B81EC314D02F75AF6E30EA0;
// Spine.EventData
struct EventData_t34B08BE715A229D34906F96784419D669DE4869E;
// Spine.ExposedList`1<Spine.Animation>
struct ExposedList_1_t96091D01C54C896967DD0ADE17658ED3963868D7;
// Spine.ExposedList`1<Spine.Attachment>
struct ExposedList_1_tE8852F51C99129DA2CBB62736EDCC23FB9604F74;
// Spine.ExposedList`1<Spine.Bone>
struct ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D;
// Spine.ExposedList`1<Spine.BoneData>
struct ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899;
// Spine.ExposedList`1<Spine.BoundingBoxAttachment>
struct ExposedList_1_t606BD1E3EAA809DF858495E0E8DE51FBA010D210;
// Spine.ExposedList`1<Spine.EventData>
struct ExposedList_1_tE97D9108C86EA6DAE531F84FB41144E092FB3F69;
// Spine.ExposedList`1<Spine.ExposedList`1<System.Int32>>
struct ExposedList_1_tA2B5C15C080DBB9671A5521E5A131BF5D0611DC3;
// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>>
struct ExposedList_1_t9581BAC884D675A26267860B1FF48E5CC396C29D;
// Spine.ExposedList`1<Spine.IUpdatable>
struct ExposedList_1_t0567DA28C0F68E726B4130E04EBA18ABAAF63D1E;
// Spine.ExposedList`1<Spine.IkConstraint>
struct ExposedList_1_t1A9F3DCF9F866E2D4DE392BB6ABC75F9178B6F61;
// Spine.ExposedList`1<Spine.IkConstraintData>
struct ExposedList_1_tD5F36B29DF6FF4D72C58216E45C800B549C65E12;
// Spine.ExposedList`1<Spine.PathConstraint>
struct ExposedList_1_t8C85350CD7E4838C9E1AFB8184A81C721B94675B;
// Spine.ExposedList`1<Spine.PathConstraintData>
struct ExposedList_1_t9CB1A4F354891A5A8DCFA3327FA1BC84B43FAC52;
// Spine.ExposedList`1<Spine.Polygon>
struct ExposedList_1_t2F106219E665F6208C2FB3B19D3451B206277EE9;
// Spine.ExposedList`1<Spine.Skin>
struct ExposedList_1_t997F8F436E9E68E760B5A3629BF3652425FC0D08;
// Spine.ExposedList`1<Spine.Slot>
struct ExposedList_1_t4B1D0FC5A5AB0104620A337E1E1B8DA2164330D9;
// Spine.ExposedList`1<Spine.SlotData>
struct ExposedList_1_tCC329C3B6D6C1CBF59931DE72BF0C1F508ED3D58;
// Spine.ExposedList`1<Spine.TransformConstraint>
struct ExposedList_1_t4435BD427EE73EEE2B9867878692B1E8963D3811;
// Spine.ExposedList`1<Spine.TransformConstraintData>
struct ExposedList_1_tC1E8D61EFB149631739A083EC35EC9D95950CFEA;
// Spine.ExposedList`1<Spine.Unity.SubmeshInstruction>
struct ExposedList_1_tF0ACB19929B7EFBF260E9588A963EC02D1EF6571;
// Spine.ExposedList`1<System.Boolean>
struct ExposedList_1_tAF5D637957B8D720F397F5305984F4DEAA6D8B69;
// Spine.ExposedList`1<System.Int32>
struct ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960;
// Spine.ExposedList`1<System.Single>
struct ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F;
// Spine.ExposedList`1<UnityEngine.Color32>
struct ExposedList_1_tE4914E468AA30F682A54A8A684D2EE51E644F58A;
// Spine.ExposedList`1<UnityEngine.Material>
struct ExposedList_1_tF74661C2F8E20C6B8429AFE8DBE767C1B055095F;
// Spine.ExposedList`1<UnityEngine.Vector2>
struct ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C;
// Spine.ExposedList`1<UnityEngine.Vector3>
struct ExposedList_1_tC4D3977D6C3902E2345F2E4F5C1EB9A7D4CC8CCF;
// Spine.IkConstraintData
struct IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C;
// Spine.MeshAttachment
struct MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392;
// Spine.PathConstraintData
struct PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827;
// Spine.PointAttachment
struct PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B;
// Spine.Pool`1<Spine.ExposedList`1<System.Int32>>
struct Pool_1_t8B778139132F28F6ABAE16BE9D4E0CBA7AA4C50B;
// Spine.Pool`1<Spine.ExposedList`1<System.Single>>
struct Pool_1_t1685D1DC5C6E6B864C29696C73DB5A637C8967D1;
// Spine.Skeleton
struct Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782;
// Spine.SkeletonClipping
struct SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5;
// Spine.SkeletonData
struct SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20;
// Spine.Skin
struct Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3;
// Spine.Slot
struct Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51;
// Spine.SlotData
struct SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738;
// Spine.TransformConstraintData
struct TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8;
// Spine.TransformMode[]
struct TransformModeU5BU5D_t4191A1BDFB015DC5355A575DDBE8B20CC00F0CB3;
// Spine.Triangulator
struct Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC;
// Spine.Unity.AnimationReferenceAsset
struct AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB;
// Spine.Unity.AtlasAsset
struct AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47;
// Spine.Unity.AtlasAsset[]
struct AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182;
// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh>
struct DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC;
// Spine.Unity.ISkeletonAnimation
struct ISkeletonAnimation_tDF41FB227A23465F8A2D0F38B5E02D6B26BE6F60;
// Spine.Unity.MeshGenerator
struct MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157;
// Spine.Unity.MeshGeneratorDelegate
struct MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4;
// Spine.Unity.MeshRendererBuffers
struct MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107;
// Spine.Unity.Playables.SpineAnimationStateBehaviour
struct SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57;
// Spine.Unity.Playables.SpineEventDelegate
struct SpineEventDelegate_tE33FEE8B9A419F03B4559C77F6CA2E23F1AD8FC7;
// Spine.Unity.Playables.SpinePlayableHandleBase
struct SpinePlayableHandleBase_tB5F200A1F15E5F638384F2B4B26C076C4E4810AC;
// Spine.Unity.SkeletonAnimation
struct SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D;
// Spine.Unity.SkeletonAnimator/MecanimTranslator
struct MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1;
// Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos[]
struct ClipInfosU5BU5D_tC874B18B2FBA9F9DE751EFB44564F4A8DD94CF37;
// Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode[]
struct MixModeU5BU5D_t28548C79DF95F6D1F3B52866B08499B0EFDC0910;
// Spine.Unity.SkeletonDataAsset
struct SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5;
// Spine.Unity.SkeletonGraphic
struct SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A;
// Spine.Unity.SkeletonRenderer
struct SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03;
// Spine.Unity.SkeletonRenderer/InstructionDelegate
struct InstructionDelegate_t5D855F63CA53EA174005317091099D0BAA478F54;
// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate
struct SkeletonRendererDelegate_tF51A39DB9B998C96CEEE3C8F0F55FB65070EBBF7;
// Spine.Unity.SkeletonRendererInstruction
struct SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A;
// Spine.Unity.SkeletonUtility
struct SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0;
// Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate
struct SkeletonUtilityDelegate_t40A11BA492FE4C516EBAF8D63834116D5865092B;
// Spine.Unity.SkeletonUtilityBone
struct SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39;
// Spine.Unity.UpdateBonesDelegate
struct UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513;
// System.Action`3<UnityEngine.Timeline.TimelineClip,UnityEngine.GameObject,UnityEngine.Playables.Playable>
struct Action_3_t94294D297455C1B4C4B6ACA47BC2B4019ECE97CB;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,System.String>
struct Dictionary_2_tE1AD6DF5433910BA9C3CE74FBEA2DF0456202D27;
// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,UnityEngine.PolygonCollider2D>
struct Dictionary_2_tF77553BC41651B08657E2A18F051DC632966797D;
// System.Collections.Generic.Dictionary`2<Spine.Skin/AttachmentKeyTuple,Spine.Attachment>
struct Dictionary_2_tB466C12C0D76C8DDE825C7FB828D002CA6E4F943;
// System.Collections.Generic.Dictionary`2<Spine.Slot,UnityEngine.Material>
struct Dictionary_2_t162335B8709163DD53742586B4CCB3D1D7F824B1;
// System.Collections.Generic.Dictionary`2<System.Int32,Spine.Animation>
struct Dictionary_2_t9EC5A030DCF7A83F350091E7207E8A0CB9825F43;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute>
struct Dictionary_2_t58C9CA6651216CBA1E51B3DE5F86E81AB30077B1;
// System.Collections.Generic.Dictionary`2<UnityEngine.AnimationClip,System.Int32>
struct Dictionary_2_tCC3E0800B3394E0A2AB78BDA00108CAFD03D05F4;
// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material>
struct Dictionary_2_t87B6F3EB6208E12A08960AA208E68480EA2C7926;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Timeline.TrackAsset>
struct IEnumerable_1_t121E17B1B8EA085B72ACE0C1AAB08EF16D87BD5D;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t7B82AA0F8B96BAAA21E36DDF7A1FE4348BDDBE95;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.AnimationClip>
struct IEqualityComparer_1_t76A93A8E91FD0BB0B6CE8B9FE2F5B99A689BE33C;
// System.Collections.Generic.List`1<Spine.Animation>
struct List_1_tDE08A2AA48EDC4BAF1D96E78046A75F98F7FA7DD;
// System.Collections.Generic.List`1<Spine.BoneData>
struct List_1_tAB2206C3DA0E2861C635E880865AF7DC16BC4CC5;
// System.Collections.Generic.List`1<Spine.SkeletonJson/LinkedMesh>
struct List_1_t5E2E60F21138E85F870ACD01F967CE1376B031C5;
// System.Collections.Generic.List`1<Spine.Slot>
struct List_1_t92152F4927754904CFC1D788B0E4A24816D37E1A;
// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityBone>
struct List_1_t4B4918D0C123A9AB1B8B4BEBD5E6D7932B435CBB;
// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityConstraint>
struct List_1_tFF497566FC0F9D6C9AC5D51FFB9904C090AACEEB;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo>
struct List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.ScriptableObject>
struct List_1_t803BD2FB729584A0A796EBF33774257912427B4E;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip>
struct List_1_tBE0C2267D3E6C51CE882E2B6B95F8E67011B1376;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Comparison`1<UnityEngine.Timeline.TimelineClip>
struct Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationClip
struct AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD;
// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD;
// UnityEngine.TextAsset
struct TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Timeline.TimelineClip[]
struct TimelineClipU5BU5D_t54DF64E1454792297ECC9A75D1E33DB9293334A3;
// UnityEngine.Timeline.TrackAsset[]
struct TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;

struct Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 ;
struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D ;
struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef IKCONSTRAINT_TAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E_H
#define IKCONSTRAINT_TAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.IkConstraint
struct  IkConstraint_tAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E  : public RuntimeObject
{
public:
	// Spine.IkConstraintData Spine.IkConstraint::data
	IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.IkConstraint::bones
	ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * ___bones_1;
	// Spine.Bone Spine.IkConstraint::target
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * ___target_2;
	// System.Single Spine.IkConstraint::mix
	float ___mix_3;
	// System.Int32 Spine.IkConstraint::bendDirection
	int32_t ___bendDirection_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(IkConstraint_tAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E, ___data_0)); }
	inline IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C * get_data_0() const { return ___data_0; }
	inline IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(IkConstraint_tAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E, ___bones_1)); }
	inline ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(IkConstraint_tAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E, ___target_2)); }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * get_target_2() const { return ___target_2; }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_mix_3() { return static_cast<int32_t>(offsetof(IkConstraint_tAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E, ___mix_3)); }
	inline float get_mix_3() const { return ___mix_3; }
	inline float* get_address_of_mix_3() { return &___mix_3; }
	inline void set_mix_3(float value)
	{
		___mix_3 = value;
	}

	inline static int32_t get_offset_of_bendDirection_4() { return static_cast<int32_t>(offsetof(IkConstraint_tAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E, ___bendDirection_4)); }
	inline int32_t get_bendDirection_4() const { return ___bendDirection_4; }
	inline int32_t* get_address_of_bendDirection_4() { return &___bendDirection_4; }
	inline void set_bendDirection_4(int32_t value)
	{
		___bendDirection_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCONSTRAINT_TAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E_H
#ifndef IKCONSTRAINTDATA_TD7BD226278FDA94E290E113485A026AA3EE8DB1C_H
#define IKCONSTRAINTDATA_TD7BD226278FDA94E290E113485A026AA3EE8DB1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.IkConstraintData
struct  IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C  : public RuntimeObject
{
public:
	// System.String Spine.IkConstraintData::name
	String_t* ___name_0;
	// System.Int32 Spine.IkConstraintData::order
	int32_t ___order_1;
	// System.Collections.Generic.List`1<Spine.BoneData> Spine.IkConstraintData::bones
	List_1_tAB2206C3DA0E2861C635E880865AF7DC16BC4CC5 * ___bones_2;
	// Spine.BoneData Spine.IkConstraintData::target
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * ___target_3;
	// System.Int32 Spine.IkConstraintData::bendDirection
	int32_t ___bendDirection_4;
	// System.Single Spine.IkConstraintData::mix
	float ___mix_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_bones_2() { return static_cast<int32_t>(offsetof(IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C, ___bones_2)); }
	inline List_1_tAB2206C3DA0E2861C635E880865AF7DC16BC4CC5 * get_bones_2() const { return ___bones_2; }
	inline List_1_tAB2206C3DA0E2861C635E880865AF7DC16BC4CC5 ** get_address_of_bones_2() { return &___bones_2; }
	inline void set_bones_2(List_1_tAB2206C3DA0E2861C635E880865AF7DC16BC4CC5 * value)
	{
		___bones_2 = value;
		Il2CppCodeGenWriteBarrier((&___bones_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C, ___target_3)); }
	inline BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * get_target_3() const { return ___target_3; }
	inline BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_bendDirection_4() { return static_cast<int32_t>(offsetof(IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C, ___bendDirection_4)); }
	inline int32_t get_bendDirection_4() const { return ___bendDirection_4; }
	inline int32_t* get_address_of_bendDirection_4() { return &___bendDirection_4; }
	inline void set_bendDirection_4(int32_t value)
	{
		___bendDirection_4 = value;
	}

	inline static int32_t get_offset_of_mix_5() { return static_cast<int32_t>(offsetof(IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C, ___mix_5)); }
	inline float get_mix_5() const { return ___mix_5; }
	inline float* get_address_of_mix_5() { return &___mix_5; }
	inline void set_mix_5(float value)
	{
		___mix_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCONSTRAINTDATA_TD7BD226278FDA94E290E113485A026AA3EE8DB1C_H
#ifndef JSON_TCF01EE62E034C72AAFCD42776B9708C8D856DB44_H
#define JSON_TCF01EE62E034C72AAFCD42776B9708C8D856DB44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Json
struct  Json_tCF01EE62E034C72AAFCD42776B9708C8D856DB44  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_TCF01EE62E034C72AAFCD42776B9708C8D856DB44_H
#ifndef MATHUTILS_T2C1A76C4955650106EF6D283FF52B27B651316E0_H
#define MATHUTILS_T2C1A76C4955650106EF6D283FF52B27B651316E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MathUtils
struct  MathUtils_t2C1A76C4955650106EF6D283FF52B27B651316E0  : public RuntimeObject
{
public:

public:
};

struct MathUtils_t2C1A76C4955650106EF6D283FF52B27B651316E0_StaticFields
{
public:
	// System.Single[] Spine.MathUtils::sin
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___sin_11;

public:
	inline static int32_t get_offset_of_sin_11() { return static_cast<int32_t>(offsetof(MathUtils_t2C1A76C4955650106EF6D283FF52B27B651316E0_StaticFields, ___sin_11)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_sin_11() const { return ___sin_11; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_sin_11() { return &___sin_11; }
	inline void set_sin_11(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___sin_11 = value;
		Il2CppCodeGenWriteBarrier((&___sin_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_T2C1A76C4955650106EF6D283FF52B27B651316E0_H
#ifndef PATHCONSTRAINT_TA8A49113C18D00DA507EE4708F2063363C77DCA0_H
#define PATHCONSTRAINT_TA8A49113C18D00DA507EE4708F2063363C77DCA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraint
struct  PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0  : public RuntimeObject
{
public:
	// Spine.PathConstraintData Spine.PathConstraint::data
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827 * ___data_4;
	// Spine.ExposedList`1<Spine.Bone> Spine.PathConstraint::bones
	ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * ___bones_5;
	// Spine.Slot Spine.PathConstraint::target
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51 * ___target_6;
	// System.Single Spine.PathConstraint::position
	float ___position_7;
	// System.Single Spine.PathConstraint::spacing
	float ___spacing_8;
	// System.Single Spine.PathConstraint::rotateMix
	float ___rotateMix_9;
	// System.Single Spine.PathConstraint::translateMix
	float ___translateMix_10;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::spaces
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___spaces_11;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::positions
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___positions_12;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::world
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___world_13;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::curves
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___curves_14;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::lengths
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___lengths_15;
	// System.Single[] Spine.PathConstraint::segments
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___segments_16;

public:
	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___data_4)); }
	inline PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827 * get_data_4() const { return ___data_4; }
	inline PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827 ** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827 * value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}

	inline static int32_t get_offset_of_bones_5() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___bones_5)); }
	inline ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * get_bones_5() const { return ___bones_5; }
	inline ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D ** get_address_of_bones_5() { return &___bones_5; }
	inline void set_bones_5(ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * value)
	{
		___bones_5 = value;
		Il2CppCodeGenWriteBarrier((&___bones_5), value);
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___target_6)); }
	inline Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51 * get_target_6() const { return ___target_6; }
	inline Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_position_7() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___position_7)); }
	inline float get_position_7() const { return ___position_7; }
	inline float* get_address_of_position_7() { return &___position_7; }
	inline void set_position_7(float value)
	{
		___position_7 = value;
	}

	inline static int32_t get_offset_of_spacing_8() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___spacing_8)); }
	inline float get_spacing_8() const { return ___spacing_8; }
	inline float* get_address_of_spacing_8() { return &___spacing_8; }
	inline void set_spacing_8(float value)
	{
		___spacing_8 = value;
	}

	inline static int32_t get_offset_of_rotateMix_9() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___rotateMix_9)); }
	inline float get_rotateMix_9() const { return ___rotateMix_9; }
	inline float* get_address_of_rotateMix_9() { return &___rotateMix_9; }
	inline void set_rotateMix_9(float value)
	{
		___rotateMix_9 = value;
	}

	inline static int32_t get_offset_of_translateMix_10() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___translateMix_10)); }
	inline float get_translateMix_10() const { return ___translateMix_10; }
	inline float* get_address_of_translateMix_10() { return &___translateMix_10; }
	inline void set_translateMix_10(float value)
	{
		___translateMix_10 = value;
	}

	inline static int32_t get_offset_of_spaces_11() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___spaces_11)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_spaces_11() const { return ___spaces_11; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_spaces_11() { return &___spaces_11; }
	inline void set_spaces_11(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___spaces_11 = value;
		Il2CppCodeGenWriteBarrier((&___spaces_11), value);
	}

	inline static int32_t get_offset_of_positions_12() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___positions_12)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_positions_12() const { return ___positions_12; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_positions_12() { return &___positions_12; }
	inline void set_positions_12(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___positions_12 = value;
		Il2CppCodeGenWriteBarrier((&___positions_12), value);
	}

	inline static int32_t get_offset_of_world_13() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___world_13)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_world_13() const { return ___world_13; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_world_13() { return &___world_13; }
	inline void set_world_13(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___world_13 = value;
		Il2CppCodeGenWriteBarrier((&___world_13), value);
	}

	inline static int32_t get_offset_of_curves_14() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___curves_14)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_curves_14() const { return ___curves_14; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_curves_14() { return &___curves_14; }
	inline void set_curves_14(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___curves_14 = value;
		Il2CppCodeGenWriteBarrier((&___curves_14), value);
	}

	inline static int32_t get_offset_of_lengths_15() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___lengths_15)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_lengths_15() const { return ___lengths_15; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_lengths_15() { return &___lengths_15; }
	inline void set_lengths_15(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___lengths_15 = value;
		Il2CppCodeGenWriteBarrier((&___lengths_15), value);
	}

	inline static int32_t get_offset_of_segments_16() { return static_cast<int32_t>(offsetof(PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0, ___segments_16)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_segments_16() const { return ___segments_16; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_segments_16() { return &___segments_16; }
	inline void set_segments_16(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___segments_16 = value;
		Il2CppCodeGenWriteBarrier((&___segments_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINT_TA8A49113C18D00DA507EE4708F2063363C77DCA0_H
#ifndef POLYGON_T6627F1043ADF312F9255C0DDD6FC0354CEFE0D7A_H
#define POLYGON_T6627F1043ADF312F9255C0DDD6FC0354CEFE0D7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Polygon
struct  Polygon_t6627F1043ADF312F9255C0DDD6FC0354CEFE0D7A  : public RuntimeObject
{
public:
	// System.Single[] Spine.Polygon::<Vertices>k__BackingField
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___U3CVerticesU3Ek__BackingField_0;
	// System.Int32 Spine.Polygon::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CVerticesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Polygon_t6627F1043ADF312F9255C0DDD6FC0354CEFE0D7A, ___U3CVerticesU3Ek__BackingField_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_U3CVerticesU3Ek__BackingField_0() const { return ___U3CVerticesU3Ek__BackingField_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_U3CVerticesU3Ek__BackingField_0() { return &___U3CVerticesU3Ek__BackingField_0; }
	inline void set_U3CVerticesU3Ek__BackingField_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___U3CVerticesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVerticesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Polygon_t6627F1043ADF312F9255C0DDD6FC0354CEFE0D7A, ___U3CCountU3Ek__BackingField_1)); }
	inline int32_t get_U3CCountU3Ek__BackingField_1() const { return ___U3CCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_1() { return &___U3CCountU3Ek__BackingField_1; }
	inline void set_U3CCountU3Ek__BackingField_1(int32_t value)
	{
		___U3CCountU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGON_T6627F1043ADF312F9255C0DDD6FC0354CEFE0D7A_H
#ifndef SKELETON_T9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782_H
#define SKELETON_T9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skeleton
struct  Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782  : public RuntimeObject
{
public:
	// Spine.SkeletonData Spine.Skeleton::data
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.Skeleton::bones
	ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * ___bones_1;
	// Spine.ExposedList`1<Spine.Slot> Spine.Skeleton::slots
	ExposedList_1_t4B1D0FC5A5AB0104620A337E1E1B8DA2164330D9 * ___slots_2;
	// Spine.ExposedList`1<Spine.Slot> Spine.Skeleton::drawOrder
	ExposedList_1_t4B1D0FC5A5AB0104620A337E1E1B8DA2164330D9 * ___drawOrder_3;
	// Spine.ExposedList`1<Spine.IkConstraint> Spine.Skeleton::ikConstraints
	ExposedList_1_t1A9F3DCF9F866E2D4DE392BB6ABC75F9178B6F61 * ___ikConstraints_4;
	// Spine.ExposedList`1<Spine.TransformConstraint> Spine.Skeleton::transformConstraints
	ExposedList_1_t4435BD427EE73EEE2B9867878692B1E8963D3811 * ___transformConstraints_5;
	// Spine.ExposedList`1<Spine.PathConstraint> Spine.Skeleton::pathConstraints
	ExposedList_1_t8C85350CD7E4838C9E1AFB8184A81C721B94675B * ___pathConstraints_6;
	// Spine.ExposedList`1<Spine.IUpdatable> Spine.Skeleton::updateCache
	ExposedList_1_t0567DA28C0F68E726B4130E04EBA18ABAAF63D1E * ___updateCache_7;
	// Spine.ExposedList`1<Spine.Bone> Spine.Skeleton::updateCacheReset
	ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * ___updateCacheReset_8;
	// Spine.Skin Spine.Skeleton::skin
	Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3 * ___skin_9;
	// System.Single Spine.Skeleton::r
	float ___r_10;
	// System.Single Spine.Skeleton::g
	float ___g_11;
	// System.Single Spine.Skeleton::b
	float ___b_12;
	// System.Single Spine.Skeleton::a
	float ___a_13;
	// System.Single Spine.Skeleton::time
	float ___time_14;
	// System.Boolean Spine.Skeleton::flipX
	bool ___flipX_15;
	// System.Boolean Spine.Skeleton::flipY
	bool ___flipY_16;
	// System.Single Spine.Skeleton::x
	float ___x_17;
	// System.Single Spine.Skeleton::y
	float ___y_18;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___data_0)); }
	inline SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 * get_data_0() const { return ___data_0; }
	inline SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___bones_1)); }
	inline ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_slots_2() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___slots_2)); }
	inline ExposedList_1_t4B1D0FC5A5AB0104620A337E1E1B8DA2164330D9 * get_slots_2() const { return ___slots_2; }
	inline ExposedList_1_t4B1D0FC5A5AB0104620A337E1E1B8DA2164330D9 ** get_address_of_slots_2() { return &___slots_2; }
	inline void set_slots_2(ExposedList_1_t4B1D0FC5A5AB0104620A337E1E1B8DA2164330D9 * value)
	{
		___slots_2 = value;
		Il2CppCodeGenWriteBarrier((&___slots_2), value);
	}

	inline static int32_t get_offset_of_drawOrder_3() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___drawOrder_3)); }
	inline ExposedList_1_t4B1D0FC5A5AB0104620A337E1E1B8DA2164330D9 * get_drawOrder_3() const { return ___drawOrder_3; }
	inline ExposedList_1_t4B1D0FC5A5AB0104620A337E1E1B8DA2164330D9 ** get_address_of_drawOrder_3() { return &___drawOrder_3; }
	inline void set_drawOrder_3(ExposedList_1_t4B1D0FC5A5AB0104620A337E1E1B8DA2164330D9 * value)
	{
		___drawOrder_3 = value;
		Il2CppCodeGenWriteBarrier((&___drawOrder_3), value);
	}

	inline static int32_t get_offset_of_ikConstraints_4() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___ikConstraints_4)); }
	inline ExposedList_1_t1A9F3DCF9F866E2D4DE392BB6ABC75F9178B6F61 * get_ikConstraints_4() const { return ___ikConstraints_4; }
	inline ExposedList_1_t1A9F3DCF9F866E2D4DE392BB6ABC75F9178B6F61 ** get_address_of_ikConstraints_4() { return &___ikConstraints_4; }
	inline void set_ikConstraints_4(ExposedList_1_t1A9F3DCF9F866E2D4DE392BB6ABC75F9178B6F61 * value)
	{
		___ikConstraints_4 = value;
		Il2CppCodeGenWriteBarrier((&___ikConstraints_4), value);
	}

	inline static int32_t get_offset_of_transformConstraints_5() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___transformConstraints_5)); }
	inline ExposedList_1_t4435BD427EE73EEE2B9867878692B1E8963D3811 * get_transformConstraints_5() const { return ___transformConstraints_5; }
	inline ExposedList_1_t4435BD427EE73EEE2B9867878692B1E8963D3811 ** get_address_of_transformConstraints_5() { return &___transformConstraints_5; }
	inline void set_transformConstraints_5(ExposedList_1_t4435BD427EE73EEE2B9867878692B1E8963D3811 * value)
	{
		___transformConstraints_5 = value;
		Il2CppCodeGenWriteBarrier((&___transformConstraints_5), value);
	}

	inline static int32_t get_offset_of_pathConstraints_6() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___pathConstraints_6)); }
	inline ExposedList_1_t8C85350CD7E4838C9E1AFB8184A81C721B94675B * get_pathConstraints_6() const { return ___pathConstraints_6; }
	inline ExposedList_1_t8C85350CD7E4838C9E1AFB8184A81C721B94675B ** get_address_of_pathConstraints_6() { return &___pathConstraints_6; }
	inline void set_pathConstraints_6(ExposedList_1_t8C85350CD7E4838C9E1AFB8184A81C721B94675B * value)
	{
		___pathConstraints_6 = value;
		Il2CppCodeGenWriteBarrier((&___pathConstraints_6), value);
	}

	inline static int32_t get_offset_of_updateCache_7() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___updateCache_7)); }
	inline ExposedList_1_t0567DA28C0F68E726B4130E04EBA18ABAAF63D1E * get_updateCache_7() const { return ___updateCache_7; }
	inline ExposedList_1_t0567DA28C0F68E726B4130E04EBA18ABAAF63D1E ** get_address_of_updateCache_7() { return &___updateCache_7; }
	inline void set_updateCache_7(ExposedList_1_t0567DA28C0F68E726B4130E04EBA18ABAAF63D1E * value)
	{
		___updateCache_7 = value;
		Il2CppCodeGenWriteBarrier((&___updateCache_7), value);
	}

	inline static int32_t get_offset_of_updateCacheReset_8() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___updateCacheReset_8)); }
	inline ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * get_updateCacheReset_8() const { return ___updateCacheReset_8; }
	inline ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D ** get_address_of_updateCacheReset_8() { return &___updateCacheReset_8; }
	inline void set_updateCacheReset_8(ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * value)
	{
		___updateCacheReset_8 = value;
		Il2CppCodeGenWriteBarrier((&___updateCacheReset_8), value);
	}

	inline static int32_t get_offset_of_skin_9() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___skin_9)); }
	inline Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3 * get_skin_9() const { return ___skin_9; }
	inline Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3 ** get_address_of_skin_9() { return &___skin_9; }
	inline void set_skin_9(Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3 * value)
	{
		___skin_9 = value;
		Il2CppCodeGenWriteBarrier((&___skin_9), value);
	}

	inline static int32_t get_offset_of_r_10() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___r_10)); }
	inline float get_r_10() const { return ___r_10; }
	inline float* get_address_of_r_10() { return &___r_10; }
	inline void set_r_10(float value)
	{
		___r_10 = value;
	}

	inline static int32_t get_offset_of_g_11() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___g_11)); }
	inline float get_g_11() const { return ___g_11; }
	inline float* get_address_of_g_11() { return &___g_11; }
	inline void set_g_11(float value)
	{
		___g_11 = value;
	}

	inline static int32_t get_offset_of_b_12() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___b_12)); }
	inline float get_b_12() const { return ___b_12; }
	inline float* get_address_of_b_12() { return &___b_12; }
	inline void set_b_12(float value)
	{
		___b_12 = value;
	}

	inline static int32_t get_offset_of_a_13() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___a_13)); }
	inline float get_a_13() const { return ___a_13; }
	inline float* get_address_of_a_13() { return &___a_13; }
	inline void set_a_13(float value)
	{
		___a_13 = value;
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___time_14)); }
	inline float get_time_14() const { return ___time_14; }
	inline float* get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(float value)
	{
		___time_14 = value;
	}

	inline static int32_t get_offset_of_flipX_15() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___flipX_15)); }
	inline bool get_flipX_15() const { return ___flipX_15; }
	inline bool* get_address_of_flipX_15() { return &___flipX_15; }
	inline void set_flipX_15(bool value)
	{
		___flipX_15 = value;
	}

	inline static int32_t get_offset_of_flipY_16() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___flipY_16)); }
	inline bool get_flipY_16() const { return ___flipY_16; }
	inline bool* get_address_of_flipY_16() { return &___flipY_16; }
	inline void set_flipY_16(bool value)
	{
		___flipY_16 = value;
	}

	inline static int32_t get_offset_of_x_17() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___x_17)); }
	inline float get_x_17() const { return ___x_17; }
	inline float* get_address_of_x_17() { return &___x_17; }
	inline void set_x_17(float value)
	{
		___x_17 = value;
	}

	inline static int32_t get_offset_of_y_18() { return static_cast<int32_t>(offsetof(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782, ___y_18)); }
	inline float get_y_18() const { return ___y_18; }
	inline float* get_address_of_y_18() { return &___y_18; }
	inline void set_y_18(float value)
	{
		___y_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETON_T9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782_H
#ifndef SKELETONBINARY_T7C428D1C0AD9A5D73F4F5B9667D3E63332C33054_H
#define SKELETONBINARY_T7C428D1C0AD9A5D73F4F5B9667D3E63332C33054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonBinary
struct  SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054  : public RuntimeObject
{
public:
	// System.Single Spine.SkeletonBinary::<Scale>k__BackingField
	float ___U3CScaleU3Ek__BackingField_13;
	// Spine.AttachmentLoader Spine.SkeletonBinary::attachmentLoader
	RuntimeObject* ___attachmentLoader_14;
	// System.Byte[] Spine.SkeletonBinary::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_15;
	// System.Collections.Generic.List`1<Spine.SkeletonJson/LinkedMesh> Spine.SkeletonBinary::linkedMeshes
	List_1_t5E2E60F21138E85F870ACD01F967CE1376B031C5 * ___linkedMeshes_16;

public:
	inline static int32_t get_offset_of_U3CScaleU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054, ___U3CScaleU3Ek__BackingField_13)); }
	inline float get_U3CScaleU3Ek__BackingField_13() const { return ___U3CScaleU3Ek__BackingField_13; }
	inline float* get_address_of_U3CScaleU3Ek__BackingField_13() { return &___U3CScaleU3Ek__BackingField_13; }
	inline void set_U3CScaleU3Ek__BackingField_13(float value)
	{
		___U3CScaleU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_attachmentLoader_14() { return static_cast<int32_t>(offsetof(SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054, ___attachmentLoader_14)); }
	inline RuntimeObject* get_attachmentLoader_14() const { return ___attachmentLoader_14; }
	inline RuntimeObject** get_address_of_attachmentLoader_14() { return &___attachmentLoader_14; }
	inline void set_attachmentLoader_14(RuntimeObject* value)
	{
		___attachmentLoader_14 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentLoader_14), value);
	}

	inline static int32_t get_offset_of_buffer_15() { return static_cast<int32_t>(offsetof(SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054, ___buffer_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_15() const { return ___buffer_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_15() { return &___buffer_15; }
	inline void set_buffer_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_15 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_15), value);
	}

	inline static int32_t get_offset_of_linkedMeshes_16() { return static_cast<int32_t>(offsetof(SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054, ___linkedMeshes_16)); }
	inline List_1_t5E2E60F21138E85F870ACD01F967CE1376B031C5 * get_linkedMeshes_16() const { return ___linkedMeshes_16; }
	inline List_1_t5E2E60F21138E85F870ACD01F967CE1376B031C5 ** get_address_of_linkedMeshes_16() { return &___linkedMeshes_16; }
	inline void set_linkedMeshes_16(List_1_t5E2E60F21138E85F870ACD01F967CE1376B031C5 * value)
	{
		___linkedMeshes_16 = value;
		Il2CppCodeGenWriteBarrier((&___linkedMeshes_16), value);
	}
};

struct SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054_StaticFields
{
public:
	// Spine.TransformMode[] Spine.SkeletonBinary::TransformModeValues
	TransformModeU5BU5D_t4191A1BDFB015DC5355A575DDBE8B20CC00F0CB3* ___TransformModeValues_17;

public:
	inline static int32_t get_offset_of_TransformModeValues_17() { return static_cast<int32_t>(offsetof(SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054_StaticFields, ___TransformModeValues_17)); }
	inline TransformModeU5BU5D_t4191A1BDFB015DC5355A575DDBE8B20CC00F0CB3* get_TransformModeValues_17() const { return ___TransformModeValues_17; }
	inline TransformModeU5BU5D_t4191A1BDFB015DC5355A575DDBE8B20CC00F0CB3** get_address_of_TransformModeValues_17() { return &___TransformModeValues_17; }
	inline void set_TransformModeValues_17(TransformModeU5BU5D_t4191A1BDFB015DC5355A575DDBE8B20CC00F0CB3* value)
	{
		___TransformModeValues_17 = value;
		Il2CppCodeGenWriteBarrier((&___TransformModeValues_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONBINARY_T7C428D1C0AD9A5D73F4F5B9667D3E63332C33054_H
#ifndef VERTICES_T2F849B9930066327474D954DD7286F4208BC18A3_H
#define VERTICES_T2F849B9930066327474D954DD7286F4208BC18A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonBinary/Vertices
struct  Vertices_t2F849B9930066327474D954DD7286F4208BC18A3  : public RuntimeObject
{
public:
	// System.Int32[] Spine.SkeletonBinary/Vertices::bones
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___bones_0;
	// System.Single[] Spine.SkeletonBinary/Vertices::vertices
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___vertices_1;

public:
	inline static int32_t get_offset_of_bones_0() { return static_cast<int32_t>(offsetof(Vertices_t2F849B9930066327474D954DD7286F4208BC18A3, ___bones_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_bones_0() const { return ___bones_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_bones_0() { return &___bones_0; }
	inline void set_bones_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___bones_0 = value;
		Il2CppCodeGenWriteBarrier((&___bones_0), value);
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(Vertices_t2F849B9930066327474D954DD7286F4208BC18A3, ___vertices_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_vertices_1() const { return ___vertices_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___vertices_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICES_T2F849B9930066327474D954DD7286F4208BC18A3_H
#ifndef SKELETONBOUNDS_TE88393981F88D5BB5933BA1BA7CF337B81A7DF11_H
#define SKELETONBOUNDS_TE88393981F88D5BB5933BA1BA7CF337B81A7DF11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonBounds
struct  SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11  : public RuntimeObject
{
public:
	// Spine.ExposedList`1<Spine.Polygon> Spine.SkeletonBounds::polygonPool
	ExposedList_1_t2F106219E665F6208C2FB3B19D3451B206277EE9 * ___polygonPool_0;
	// System.Single Spine.SkeletonBounds::minX
	float ___minX_1;
	// System.Single Spine.SkeletonBounds::minY
	float ___minY_2;
	// System.Single Spine.SkeletonBounds::maxX
	float ___maxX_3;
	// System.Single Spine.SkeletonBounds::maxY
	float ___maxY_4;
	// Spine.ExposedList`1<Spine.BoundingBoxAttachment> Spine.SkeletonBounds::<BoundingBoxes>k__BackingField
	ExposedList_1_t606BD1E3EAA809DF858495E0E8DE51FBA010D210 * ___U3CBoundingBoxesU3Ek__BackingField_5;
	// Spine.ExposedList`1<Spine.Polygon> Spine.SkeletonBounds::<Polygons>k__BackingField
	ExposedList_1_t2F106219E665F6208C2FB3B19D3451B206277EE9 * ___U3CPolygonsU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_polygonPool_0() { return static_cast<int32_t>(offsetof(SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11, ___polygonPool_0)); }
	inline ExposedList_1_t2F106219E665F6208C2FB3B19D3451B206277EE9 * get_polygonPool_0() const { return ___polygonPool_0; }
	inline ExposedList_1_t2F106219E665F6208C2FB3B19D3451B206277EE9 ** get_address_of_polygonPool_0() { return &___polygonPool_0; }
	inline void set_polygonPool_0(ExposedList_1_t2F106219E665F6208C2FB3B19D3451B206277EE9 * value)
	{
		___polygonPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___polygonPool_0), value);
	}

	inline static int32_t get_offset_of_minX_1() { return static_cast<int32_t>(offsetof(SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11, ___minX_1)); }
	inline float get_minX_1() const { return ___minX_1; }
	inline float* get_address_of_minX_1() { return &___minX_1; }
	inline void set_minX_1(float value)
	{
		___minX_1 = value;
	}

	inline static int32_t get_offset_of_minY_2() { return static_cast<int32_t>(offsetof(SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11, ___minY_2)); }
	inline float get_minY_2() const { return ___minY_2; }
	inline float* get_address_of_minY_2() { return &___minY_2; }
	inline void set_minY_2(float value)
	{
		___minY_2 = value;
	}

	inline static int32_t get_offset_of_maxX_3() { return static_cast<int32_t>(offsetof(SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11, ___maxX_3)); }
	inline float get_maxX_3() const { return ___maxX_3; }
	inline float* get_address_of_maxX_3() { return &___maxX_3; }
	inline void set_maxX_3(float value)
	{
		___maxX_3 = value;
	}

	inline static int32_t get_offset_of_maxY_4() { return static_cast<int32_t>(offsetof(SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11, ___maxY_4)); }
	inline float get_maxY_4() const { return ___maxY_4; }
	inline float* get_address_of_maxY_4() { return &___maxY_4; }
	inline void set_maxY_4(float value)
	{
		___maxY_4 = value;
	}

	inline static int32_t get_offset_of_U3CBoundingBoxesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11, ___U3CBoundingBoxesU3Ek__BackingField_5)); }
	inline ExposedList_1_t606BD1E3EAA809DF858495E0E8DE51FBA010D210 * get_U3CBoundingBoxesU3Ek__BackingField_5() const { return ___U3CBoundingBoxesU3Ek__BackingField_5; }
	inline ExposedList_1_t606BD1E3EAA809DF858495E0E8DE51FBA010D210 ** get_address_of_U3CBoundingBoxesU3Ek__BackingField_5() { return &___U3CBoundingBoxesU3Ek__BackingField_5; }
	inline void set_U3CBoundingBoxesU3Ek__BackingField_5(ExposedList_1_t606BD1E3EAA809DF858495E0E8DE51FBA010D210 * value)
	{
		___U3CBoundingBoxesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoundingBoxesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CPolygonsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11, ___U3CPolygonsU3Ek__BackingField_6)); }
	inline ExposedList_1_t2F106219E665F6208C2FB3B19D3451B206277EE9 * get_U3CPolygonsU3Ek__BackingField_6() const { return ___U3CPolygonsU3Ek__BackingField_6; }
	inline ExposedList_1_t2F106219E665F6208C2FB3B19D3451B206277EE9 ** get_address_of_U3CPolygonsU3Ek__BackingField_6() { return &___U3CPolygonsU3Ek__BackingField_6; }
	inline void set_U3CPolygonsU3Ek__BackingField_6(ExposedList_1_t2F106219E665F6208C2FB3B19D3451B206277EE9 * value)
	{
		___U3CPolygonsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPolygonsU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONBOUNDS_TE88393981F88D5BB5933BA1BA7CF337B81A7DF11_H
#ifndef SKELETONCLIPPING_TDC815E68228278D3942B5AF150FCEB7EAA3F4FF5_H
#define SKELETONCLIPPING_TDC815E68228278D3942B5AF150FCEB7EAA3F4FF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonClipping
struct  SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5  : public RuntimeObject
{
public:
	// Spine.Triangulator Spine.SkeletonClipping::triangulator
	Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC * ___triangulator_0;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clippingPolygon
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___clippingPolygon_1;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clipOutput
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___clipOutput_2;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clippedVertices
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___clippedVertices_3;
	// Spine.ExposedList`1<System.Int32> Spine.SkeletonClipping::clippedTriangles
	ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * ___clippedTriangles_4;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clippedUVs
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___clippedUVs_5;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::scratch
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___scratch_6;
	// Spine.ClippingAttachment Spine.SkeletonClipping::clipAttachment
	ClippingAttachment_tB72A005F3E80A08C9BF2D903BBD1D951342AB490 * ___clipAttachment_7;
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>> Spine.SkeletonClipping::clippingPolygons
	ExposedList_1_t9581BAC884D675A26267860B1FF48E5CC396C29D * ___clippingPolygons_8;

public:
	inline static int32_t get_offset_of_triangulator_0() { return static_cast<int32_t>(offsetof(SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5, ___triangulator_0)); }
	inline Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC * get_triangulator_0() const { return ___triangulator_0; }
	inline Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC ** get_address_of_triangulator_0() { return &___triangulator_0; }
	inline void set_triangulator_0(Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC * value)
	{
		___triangulator_0 = value;
		Il2CppCodeGenWriteBarrier((&___triangulator_0), value);
	}

	inline static int32_t get_offset_of_clippingPolygon_1() { return static_cast<int32_t>(offsetof(SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5, ___clippingPolygon_1)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_clippingPolygon_1() const { return ___clippingPolygon_1; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_clippingPolygon_1() { return &___clippingPolygon_1; }
	inline void set_clippingPolygon_1(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___clippingPolygon_1 = value;
		Il2CppCodeGenWriteBarrier((&___clippingPolygon_1), value);
	}

	inline static int32_t get_offset_of_clipOutput_2() { return static_cast<int32_t>(offsetof(SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5, ___clipOutput_2)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_clipOutput_2() const { return ___clipOutput_2; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_clipOutput_2() { return &___clipOutput_2; }
	inline void set_clipOutput_2(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___clipOutput_2 = value;
		Il2CppCodeGenWriteBarrier((&___clipOutput_2), value);
	}

	inline static int32_t get_offset_of_clippedVertices_3() { return static_cast<int32_t>(offsetof(SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5, ___clippedVertices_3)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_clippedVertices_3() const { return ___clippedVertices_3; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_clippedVertices_3() { return &___clippedVertices_3; }
	inline void set_clippedVertices_3(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___clippedVertices_3 = value;
		Il2CppCodeGenWriteBarrier((&___clippedVertices_3), value);
	}

	inline static int32_t get_offset_of_clippedTriangles_4() { return static_cast<int32_t>(offsetof(SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5, ___clippedTriangles_4)); }
	inline ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * get_clippedTriangles_4() const { return ___clippedTriangles_4; }
	inline ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 ** get_address_of_clippedTriangles_4() { return &___clippedTriangles_4; }
	inline void set_clippedTriangles_4(ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * value)
	{
		___clippedTriangles_4 = value;
		Il2CppCodeGenWriteBarrier((&___clippedTriangles_4), value);
	}

	inline static int32_t get_offset_of_clippedUVs_5() { return static_cast<int32_t>(offsetof(SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5, ___clippedUVs_5)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_clippedUVs_5() const { return ___clippedUVs_5; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_clippedUVs_5() { return &___clippedUVs_5; }
	inline void set_clippedUVs_5(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___clippedUVs_5 = value;
		Il2CppCodeGenWriteBarrier((&___clippedUVs_5), value);
	}

	inline static int32_t get_offset_of_scratch_6() { return static_cast<int32_t>(offsetof(SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5, ___scratch_6)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_scratch_6() const { return ___scratch_6; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_scratch_6() { return &___scratch_6; }
	inline void set_scratch_6(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___scratch_6 = value;
		Il2CppCodeGenWriteBarrier((&___scratch_6), value);
	}

	inline static int32_t get_offset_of_clipAttachment_7() { return static_cast<int32_t>(offsetof(SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5, ___clipAttachment_7)); }
	inline ClippingAttachment_tB72A005F3E80A08C9BF2D903BBD1D951342AB490 * get_clipAttachment_7() const { return ___clipAttachment_7; }
	inline ClippingAttachment_tB72A005F3E80A08C9BF2D903BBD1D951342AB490 ** get_address_of_clipAttachment_7() { return &___clipAttachment_7; }
	inline void set_clipAttachment_7(ClippingAttachment_tB72A005F3E80A08C9BF2D903BBD1D951342AB490 * value)
	{
		___clipAttachment_7 = value;
		Il2CppCodeGenWriteBarrier((&___clipAttachment_7), value);
	}

	inline static int32_t get_offset_of_clippingPolygons_8() { return static_cast<int32_t>(offsetof(SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5, ___clippingPolygons_8)); }
	inline ExposedList_1_t9581BAC884D675A26267860B1FF48E5CC396C29D * get_clippingPolygons_8() const { return ___clippingPolygons_8; }
	inline ExposedList_1_t9581BAC884D675A26267860B1FF48E5CC396C29D ** get_address_of_clippingPolygons_8() { return &___clippingPolygons_8; }
	inline void set_clippingPolygons_8(ExposedList_1_t9581BAC884D675A26267860B1FF48E5CC396C29D * value)
	{
		___clippingPolygons_8 = value;
		Il2CppCodeGenWriteBarrier((&___clippingPolygons_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONCLIPPING_TDC815E68228278D3942B5AF150FCEB7EAA3F4FF5_H
#ifndef SKELETONDATA_T585E301AA70D4EFBC1032C86E0A8FD9B752BBA20_H
#define SKELETONDATA_T585E301AA70D4EFBC1032C86E0A8FD9B752BBA20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonData
struct  SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20  : public RuntimeObject
{
public:
	// System.String Spine.SkeletonData::name
	String_t* ___name_0;
	// Spine.ExposedList`1<Spine.BoneData> Spine.SkeletonData::bones
	ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899 * ___bones_1;
	// Spine.ExposedList`1<Spine.SlotData> Spine.SkeletonData::slots
	ExposedList_1_tCC329C3B6D6C1CBF59931DE72BF0C1F508ED3D58 * ___slots_2;
	// Spine.ExposedList`1<Spine.Skin> Spine.SkeletonData::skins
	ExposedList_1_t997F8F436E9E68E760B5A3629BF3652425FC0D08 * ___skins_3;
	// Spine.Skin Spine.SkeletonData::defaultSkin
	Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3 * ___defaultSkin_4;
	// Spine.ExposedList`1<Spine.EventData> Spine.SkeletonData::events
	ExposedList_1_tE97D9108C86EA6DAE531F84FB41144E092FB3F69 * ___events_5;
	// Spine.ExposedList`1<Spine.Animation> Spine.SkeletonData::animations
	ExposedList_1_t96091D01C54C896967DD0ADE17658ED3963868D7 * ___animations_6;
	// Spine.ExposedList`1<Spine.IkConstraintData> Spine.SkeletonData::ikConstraints
	ExposedList_1_tD5F36B29DF6FF4D72C58216E45C800B549C65E12 * ___ikConstraints_7;
	// Spine.ExposedList`1<Spine.TransformConstraintData> Spine.SkeletonData::transformConstraints
	ExposedList_1_tC1E8D61EFB149631739A083EC35EC9D95950CFEA * ___transformConstraints_8;
	// Spine.ExposedList`1<Spine.PathConstraintData> Spine.SkeletonData::pathConstraints
	ExposedList_1_t9CB1A4F354891A5A8DCFA3327FA1BC84B43FAC52 * ___pathConstraints_9;
	// System.Single Spine.SkeletonData::width
	float ___width_10;
	// System.Single Spine.SkeletonData::height
	float ___height_11;
	// System.String Spine.SkeletonData::version
	String_t* ___version_12;
	// System.String Spine.SkeletonData::hash
	String_t* ___hash_13;
	// System.Single Spine.SkeletonData::fps
	float ___fps_14;
	// System.String Spine.SkeletonData::imagesPath
	String_t* ___imagesPath_15;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___bones_1)); }
	inline ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_slots_2() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___slots_2)); }
	inline ExposedList_1_tCC329C3B6D6C1CBF59931DE72BF0C1F508ED3D58 * get_slots_2() const { return ___slots_2; }
	inline ExposedList_1_tCC329C3B6D6C1CBF59931DE72BF0C1F508ED3D58 ** get_address_of_slots_2() { return &___slots_2; }
	inline void set_slots_2(ExposedList_1_tCC329C3B6D6C1CBF59931DE72BF0C1F508ED3D58 * value)
	{
		___slots_2 = value;
		Il2CppCodeGenWriteBarrier((&___slots_2), value);
	}

	inline static int32_t get_offset_of_skins_3() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___skins_3)); }
	inline ExposedList_1_t997F8F436E9E68E760B5A3629BF3652425FC0D08 * get_skins_3() const { return ___skins_3; }
	inline ExposedList_1_t997F8F436E9E68E760B5A3629BF3652425FC0D08 ** get_address_of_skins_3() { return &___skins_3; }
	inline void set_skins_3(ExposedList_1_t997F8F436E9E68E760B5A3629BF3652425FC0D08 * value)
	{
		___skins_3 = value;
		Il2CppCodeGenWriteBarrier((&___skins_3), value);
	}

	inline static int32_t get_offset_of_defaultSkin_4() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___defaultSkin_4)); }
	inline Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3 * get_defaultSkin_4() const { return ___defaultSkin_4; }
	inline Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3 ** get_address_of_defaultSkin_4() { return &___defaultSkin_4; }
	inline void set_defaultSkin_4(Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3 * value)
	{
		___defaultSkin_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSkin_4), value);
	}

	inline static int32_t get_offset_of_events_5() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___events_5)); }
	inline ExposedList_1_tE97D9108C86EA6DAE531F84FB41144E092FB3F69 * get_events_5() const { return ___events_5; }
	inline ExposedList_1_tE97D9108C86EA6DAE531F84FB41144E092FB3F69 ** get_address_of_events_5() { return &___events_5; }
	inline void set_events_5(ExposedList_1_tE97D9108C86EA6DAE531F84FB41144E092FB3F69 * value)
	{
		___events_5 = value;
		Il2CppCodeGenWriteBarrier((&___events_5), value);
	}

	inline static int32_t get_offset_of_animations_6() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___animations_6)); }
	inline ExposedList_1_t96091D01C54C896967DD0ADE17658ED3963868D7 * get_animations_6() const { return ___animations_6; }
	inline ExposedList_1_t96091D01C54C896967DD0ADE17658ED3963868D7 ** get_address_of_animations_6() { return &___animations_6; }
	inline void set_animations_6(ExposedList_1_t96091D01C54C896967DD0ADE17658ED3963868D7 * value)
	{
		___animations_6 = value;
		Il2CppCodeGenWriteBarrier((&___animations_6), value);
	}

	inline static int32_t get_offset_of_ikConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___ikConstraints_7)); }
	inline ExposedList_1_tD5F36B29DF6FF4D72C58216E45C800B549C65E12 * get_ikConstraints_7() const { return ___ikConstraints_7; }
	inline ExposedList_1_tD5F36B29DF6FF4D72C58216E45C800B549C65E12 ** get_address_of_ikConstraints_7() { return &___ikConstraints_7; }
	inline void set_ikConstraints_7(ExposedList_1_tD5F36B29DF6FF4D72C58216E45C800B549C65E12 * value)
	{
		___ikConstraints_7 = value;
		Il2CppCodeGenWriteBarrier((&___ikConstraints_7), value);
	}

	inline static int32_t get_offset_of_transformConstraints_8() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___transformConstraints_8)); }
	inline ExposedList_1_tC1E8D61EFB149631739A083EC35EC9D95950CFEA * get_transformConstraints_8() const { return ___transformConstraints_8; }
	inline ExposedList_1_tC1E8D61EFB149631739A083EC35EC9D95950CFEA ** get_address_of_transformConstraints_8() { return &___transformConstraints_8; }
	inline void set_transformConstraints_8(ExposedList_1_tC1E8D61EFB149631739A083EC35EC9D95950CFEA * value)
	{
		___transformConstraints_8 = value;
		Il2CppCodeGenWriteBarrier((&___transformConstraints_8), value);
	}

	inline static int32_t get_offset_of_pathConstraints_9() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___pathConstraints_9)); }
	inline ExposedList_1_t9CB1A4F354891A5A8DCFA3327FA1BC84B43FAC52 * get_pathConstraints_9() const { return ___pathConstraints_9; }
	inline ExposedList_1_t9CB1A4F354891A5A8DCFA3327FA1BC84B43FAC52 ** get_address_of_pathConstraints_9() { return &___pathConstraints_9; }
	inline void set_pathConstraints_9(ExposedList_1_t9CB1A4F354891A5A8DCFA3327FA1BC84B43FAC52 * value)
	{
		___pathConstraints_9 = value;
		Il2CppCodeGenWriteBarrier((&___pathConstraints_9), value);
	}

	inline static int32_t get_offset_of_width_10() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___width_10)); }
	inline float get_width_10() const { return ___width_10; }
	inline float* get_address_of_width_10() { return &___width_10; }
	inline void set_width_10(float value)
	{
		___width_10 = value;
	}

	inline static int32_t get_offset_of_height_11() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___height_11)); }
	inline float get_height_11() const { return ___height_11; }
	inline float* get_address_of_height_11() { return &___height_11; }
	inline void set_height_11(float value)
	{
		___height_11 = value;
	}

	inline static int32_t get_offset_of_version_12() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___version_12)); }
	inline String_t* get_version_12() const { return ___version_12; }
	inline String_t** get_address_of_version_12() { return &___version_12; }
	inline void set_version_12(String_t* value)
	{
		___version_12 = value;
		Il2CppCodeGenWriteBarrier((&___version_12), value);
	}

	inline static int32_t get_offset_of_hash_13() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___hash_13)); }
	inline String_t* get_hash_13() const { return ___hash_13; }
	inline String_t** get_address_of_hash_13() { return &___hash_13; }
	inline void set_hash_13(String_t* value)
	{
		___hash_13 = value;
		Il2CppCodeGenWriteBarrier((&___hash_13), value);
	}

	inline static int32_t get_offset_of_fps_14() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___fps_14)); }
	inline float get_fps_14() const { return ___fps_14; }
	inline float* get_address_of_fps_14() { return &___fps_14; }
	inline void set_fps_14(float value)
	{
		___fps_14 = value;
	}

	inline static int32_t get_offset_of_imagesPath_15() { return static_cast<int32_t>(offsetof(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20, ___imagesPath_15)); }
	inline String_t* get_imagesPath_15() const { return ___imagesPath_15; }
	inline String_t** get_address_of_imagesPath_15() { return &___imagesPath_15; }
	inline void set_imagesPath_15(String_t* value)
	{
		___imagesPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___imagesPath_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONDATA_T585E301AA70D4EFBC1032C86E0A8FD9B752BBA20_H
#ifndef SKELETONEXTENSIONS_T9B39ECE761E6685FEC80BFA351559A3FBFC8453C_H
#define SKELETONEXTENSIONS_T9B39ECE761E6685FEC80BFA351559A3FBFC8453C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonExtensions
struct  SkeletonExtensions_t9B39ECE761E6685FEC80BFA351559A3FBFC8453C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONEXTENSIONS_T9B39ECE761E6685FEC80BFA351559A3FBFC8453C_H
#ifndef SKELETONJSON_T657D553E1492D5959AEF75BCA0F551C15449E4EB_H
#define SKELETONJSON_T657D553E1492D5959AEF75BCA0F551C15449E4EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonJson
struct  SkeletonJson_t657D553E1492D5959AEF75BCA0F551C15449E4EB  : public RuntimeObject
{
public:
	// System.Single Spine.SkeletonJson::<Scale>k__BackingField
	float ___U3CScaleU3Ek__BackingField_0;
	// Spine.AttachmentLoader Spine.SkeletonJson::attachmentLoader
	RuntimeObject* ___attachmentLoader_1;
	// System.Collections.Generic.List`1<Spine.SkeletonJson/LinkedMesh> Spine.SkeletonJson::linkedMeshes
	List_1_t5E2E60F21138E85F870ACD01F967CE1376B031C5 * ___linkedMeshes_2;

public:
	inline static int32_t get_offset_of_U3CScaleU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SkeletonJson_t657D553E1492D5959AEF75BCA0F551C15449E4EB, ___U3CScaleU3Ek__BackingField_0)); }
	inline float get_U3CScaleU3Ek__BackingField_0() const { return ___U3CScaleU3Ek__BackingField_0; }
	inline float* get_address_of_U3CScaleU3Ek__BackingField_0() { return &___U3CScaleU3Ek__BackingField_0; }
	inline void set_U3CScaleU3Ek__BackingField_0(float value)
	{
		___U3CScaleU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_attachmentLoader_1() { return static_cast<int32_t>(offsetof(SkeletonJson_t657D553E1492D5959AEF75BCA0F551C15449E4EB, ___attachmentLoader_1)); }
	inline RuntimeObject* get_attachmentLoader_1() const { return ___attachmentLoader_1; }
	inline RuntimeObject** get_address_of_attachmentLoader_1() { return &___attachmentLoader_1; }
	inline void set_attachmentLoader_1(RuntimeObject* value)
	{
		___attachmentLoader_1 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentLoader_1), value);
	}

	inline static int32_t get_offset_of_linkedMeshes_2() { return static_cast<int32_t>(offsetof(SkeletonJson_t657D553E1492D5959AEF75BCA0F551C15449E4EB, ___linkedMeshes_2)); }
	inline List_1_t5E2E60F21138E85F870ACD01F967CE1376B031C5 * get_linkedMeshes_2() const { return ___linkedMeshes_2; }
	inline List_1_t5E2E60F21138E85F870ACD01F967CE1376B031C5 ** get_address_of_linkedMeshes_2() { return &___linkedMeshes_2; }
	inline void set_linkedMeshes_2(List_1_t5E2E60F21138E85F870ACD01F967CE1376B031C5 * value)
	{
		___linkedMeshes_2 = value;
		Il2CppCodeGenWriteBarrier((&___linkedMeshes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONJSON_T657D553E1492D5959AEF75BCA0F551C15449E4EB_H
#ifndef LINKEDMESH_T40F8CC003EFF4865870FB4881D56E9A4E02FBD35_H
#define LINKEDMESH_T40F8CC003EFF4865870FB4881D56E9A4E02FBD35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonJson/LinkedMesh
struct  LinkedMesh_t40F8CC003EFF4865870FB4881D56E9A4E02FBD35  : public RuntimeObject
{
public:
	// System.String Spine.SkeletonJson/LinkedMesh::parent
	String_t* ___parent_0;
	// System.String Spine.SkeletonJson/LinkedMesh::skin
	String_t* ___skin_1;
	// System.Int32 Spine.SkeletonJson/LinkedMesh::slotIndex
	int32_t ___slotIndex_2;
	// Spine.MeshAttachment Spine.SkeletonJson/LinkedMesh::mesh
	MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392 * ___mesh_3;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(LinkedMesh_t40F8CC003EFF4865870FB4881D56E9A4E02FBD35, ___parent_0)); }
	inline String_t* get_parent_0() const { return ___parent_0; }
	inline String_t** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(String_t* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_skin_1() { return static_cast<int32_t>(offsetof(LinkedMesh_t40F8CC003EFF4865870FB4881D56E9A4E02FBD35, ___skin_1)); }
	inline String_t* get_skin_1() const { return ___skin_1; }
	inline String_t** get_address_of_skin_1() { return &___skin_1; }
	inline void set_skin_1(String_t* value)
	{
		___skin_1 = value;
		Il2CppCodeGenWriteBarrier((&___skin_1), value);
	}

	inline static int32_t get_offset_of_slotIndex_2() { return static_cast<int32_t>(offsetof(LinkedMesh_t40F8CC003EFF4865870FB4881D56E9A4E02FBD35, ___slotIndex_2)); }
	inline int32_t get_slotIndex_2() const { return ___slotIndex_2; }
	inline int32_t* get_address_of_slotIndex_2() { return &___slotIndex_2; }
	inline void set_slotIndex_2(int32_t value)
	{
		___slotIndex_2 = value;
	}

	inline static int32_t get_offset_of_mesh_3() { return static_cast<int32_t>(offsetof(LinkedMesh_t40F8CC003EFF4865870FB4881D56E9A4E02FBD35, ___mesh_3)); }
	inline MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392 * get_mesh_3() const { return ___mesh_3; }
	inline MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392 ** get_address_of_mesh_3() { return &___mesh_3; }
	inline void set_mesh_3(MeshAttachment_tC2FBF4DAB3D6E89BE8A4B5ABCCC98ED1F6B4C392 * value)
	{
		___mesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDMESH_T40F8CC003EFF4865870FB4881D56E9A4E02FBD35_H
#ifndef SKIN_T8F012B72B8B5F23850687C4ECA4712913276C0B3_H
#define SKIN_T8F012B72B8B5F23850687C4ECA4712913276C0B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skin
struct  Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3  : public RuntimeObject
{
public:
	// System.String Spine.Skin::name
	String_t* ___name_0;
	// System.Collections.Generic.Dictionary`2<Spine.Skin/AttachmentKeyTuple,Spine.Attachment> Spine.Skin::attachments
	Dictionary_2_tB466C12C0D76C8DDE825C7FB828D002CA6E4F943 * ___attachments_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_attachments_1() { return static_cast<int32_t>(offsetof(Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3, ___attachments_1)); }
	inline Dictionary_2_tB466C12C0D76C8DDE825C7FB828D002CA6E4F943 * get_attachments_1() const { return ___attachments_1; }
	inline Dictionary_2_tB466C12C0D76C8DDE825C7FB828D002CA6E4F943 ** get_address_of_attachments_1() { return &___attachments_1; }
	inline void set_attachments_1(Dictionary_2_tB466C12C0D76C8DDE825C7FB828D002CA6E4F943 * value)
	{
		___attachments_1 = value;
		Il2CppCodeGenWriteBarrier((&___attachments_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKIN_T8F012B72B8B5F23850687C4ECA4712913276C0B3_H
#ifndef ATTACHMENTKEYTUPLECOMPARER_T72271CF22CD25E9065A4A91EF1BD00613B609291_H
#define ATTACHMENTKEYTUPLECOMPARER_T72271CF22CD25E9065A4A91EF1BD00613B609291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skin/AttachmentKeyTupleComparer
struct  AttachmentKeyTupleComparer_t72271CF22CD25E9065A4A91EF1BD00613B609291  : public RuntimeObject
{
public:

public:
};

struct AttachmentKeyTupleComparer_t72271CF22CD25E9065A4A91EF1BD00613B609291_StaticFields
{
public:
	// Spine.Skin/AttachmentKeyTupleComparer Spine.Skin/AttachmentKeyTupleComparer::Instance
	AttachmentKeyTupleComparer_t72271CF22CD25E9065A4A91EF1BD00613B609291 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(AttachmentKeyTupleComparer_t72271CF22CD25E9065A4A91EF1BD00613B609291_StaticFields, ___Instance_0)); }
	inline AttachmentKeyTupleComparer_t72271CF22CD25E9065A4A91EF1BD00613B609291 * get_Instance_0() const { return ___Instance_0; }
	inline AttachmentKeyTupleComparer_t72271CF22CD25E9065A4A91EF1BD00613B609291 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(AttachmentKeyTupleComparer_t72271CF22CD25E9065A4A91EF1BD00613B609291 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTKEYTUPLECOMPARER_T72271CF22CD25E9065A4A91EF1BD00613B609291_H
#ifndef SLOT_T6A54718FF0CEC915B6B31FE209FAF429CEF9DA51_H
#define SLOT_T6A54718FF0CEC915B6B31FE209FAF429CEF9DA51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Slot
struct  Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51  : public RuntimeObject
{
public:
	// Spine.SlotData Spine.Slot::data
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738 * ___data_0;
	// Spine.Bone Spine.Slot::bone
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * ___bone_1;
	// System.Single Spine.Slot::r
	float ___r_2;
	// System.Single Spine.Slot::g
	float ___g_3;
	// System.Single Spine.Slot::b
	float ___b_4;
	// System.Single Spine.Slot::a
	float ___a_5;
	// System.Single Spine.Slot::r2
	float ___r2_6;
	// System.Single Spine.Slot::g2
	float ___g2_7;
	// System.Single Spine.Slot::b2
	float ___b2_8;
	// System.Boolean Spine.Slot::hasSecondColor
	bool ___hasSecondColor_9;
	// Spine.Attachment Spine.Slot::attachment
	Attachment_tCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92 * ___attachment_10;
	// System.Single Spine.Slot::attachmentTime
	float ___attachmentTime_11;
	// Spine.ExposedList`1<System.Single> Spine.Slot::attachmentVertices
	ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * ___attachmentVertices_12;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___data_0)); }
	inline SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738 * get_data_0() const { return ___data_0; }
	inline SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bone_1() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___bone_1)); }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * get_bone_1() const { return ___bone_1; }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E ** get_address_of_bone_1() { return &___bone_1; }
	inline void set_bone_1(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * value)
	{
		___bone_1 = value;
		Il2CppCodeGenWriteBarrier((&___bone_1), value);
	}

	inline static int32_t get_offset_of_r_2() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___r_2)); }
	inline float get_r_2() const { return ___r_2; }
	inline float* get_address_of_r_2() { return &___r_2; }
	inline void set_r_2(float value)
	{
		___r_2 = value;
	}

	inline static int32_t get_offset_of_g_3() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___g_3)); }
	inline float get_g_3() const { return ___g_3; }
	inline float* get_address_of_g_3() { return &___g_3; }
	inline void set_g_3(float value)
	{
		___g_3 = value;
	}

	inline static int32_t get_offset_of_b_4() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___b_4)); }
	inline float get_b_4() const { return ___b_4; }
	inline float* get_address_of_b_4() { return &___b_4; }
	inline void set_b_4(float value)
	{
		___b_4 = value;
	}

	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___a_5)); }
	inline float get_a_5() const { return ___a_5; }
	inline float* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(float value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_r2_6() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___r2_6)); }
	inline float get_r2_6() const { return ___r2_6; }
	inline float* get_address_of_r2_6() { return &___r2_6; }
	inline void set_r2_6(float value)
	{
		___r2_6 = value;
	}

	inline static int32_t get_offset_of_g2_7() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___g2_7)); }
	inline float get_g2_7() const { return ___g2_7; }
	inline float* get_address_of_g2_7() { return &___g2_7; }
	inline void set_g2_7(float value)
	{
		___g2_7 = value;
	}

	inline static int32_t get_offset_of_b2_8() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___b2_8)); }
	inline float get_b2_8() const { return ___b2_8; }
	inline float* get_address_of_b2_8() { return &___b2_8; }
	inline void set_b2_8(float value)
	{
		___b2_8 = value;
	}

	inline static int32_t get_offset_of_hasSecondColor_9() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___hasSecondColor_9)); }
	inline bool get_hasSecondColor_9() const { return ___hasSecondColor_9; }
	inline bool* get_address_of_hasSecondColor_9() { return &___hasSecondColor_9; }
	inline void set_hasSecondColor_9(bool value)
	{
		___hasSecondColor_9 = value;
	}

	inline static int32_t get_offset_of_attachment_10() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___attachment_10)); }
	inline Attachment_tCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92 * get_attachment_10() const { return ___attachment_10; }
	inline Attachment_tCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92 ** get_address_of_attachment_10() { return &___attachment_10; }
	inline void set_attachment_10(Attachment_tCB70F4D1AD1E5B0FE9EFE2890064CDDD18BDCA92 * value)
	{
		___attachment_10 = value;
		Il2CppCodeGenWriteBarrier((&___attachment_10), value);
	}

	inline static int32_t get_offset_of_attachmentTime_11() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___attachmentTime_11)); }
	inline float get_attachmentTime_11() const { return ___attachmentTime_11; }
	inline float* get_address_of_attachmentTime_11() { return &___attachmentTime_11; }
	inline void set_attachmentTime_11(float value)
	{
		___attachmentTime_11 = value;
	}

	inline static int32_t get_offset_of_attachmentVertices_12() { return static_cast<int32_t>(offsetof(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51, ___attachmentVertices_12)); }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * get_attachmentVertices_12() const { return ___attachmentVertices_12; }
	inline ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F ** get_address_of_attachmentVertices_12() { return &___attachmentVertices_12; }
	inline void set_attachmentVertices_12(ExposedList_1_tF20402C32EF504E03EFC08C88CB189770BB0517F * value)
	{
		___attachmentVertices_12 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentVertices_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T6A54718FF0CEC915B6B31FE209FAF429CEF9DA51_H
#ifndef TRANSFORMCONSTRAINT_T1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A_H
#define TRANSFORMCONSTRAINT_T1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformConstraint
struct  TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A  : public RuntimeObject
{
public:
	// Spine.TransformConstraintData Spine.TransformConstraint::data
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8 * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.TransformConstraint::bones
	ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * ___bones_1;
	// Spine.Bone Spine.TransformConstraint::target
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * ___target_2;
	// System.Single Spine.TransformConstraint::rotateMix
	float ___rotateMix_3;
	// System.Single Spine.TransformConstraint::translateMix
	float ___translateMix_4;
	// System.Single Spine.TransformConstraint::scaleMix
	float ___scaleMix_5;
	// System.Single Spine.TransformConstraint::shearMix
	float ___shearMix_6;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A, ___data_0)); }
	inline TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8 * get_data_0() const { return ___data_0; }
	inline TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A, ___bones_1)); }
	inline ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_tDF9269F703A5323C0645C171B5B10E274E7F2E2D * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A, ___target_2)); }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * get_target_2() const { return ___target_2; }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_rotateMix_3() { return static_cast<int32_t>(offsetof(TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A, ___rotateMix_3)); }
	inline float get_rotateMix_3() const { return ___rotateMix_3; }
	inline float* get_address_of_rotateMix_3() { return &___rotateMix_3; }
	inline void set_rotateMix_3(float value)
	{
		___rotateMix_3 = value;
	}

	inline static int32_t get_offset_of_translateMix_4() { return static_cast<int32_t>(offsetof(TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A, ___translateMix_4)); }
	inline float get_translateMix_4() const { return ___translateMix_4; }
	inline float* get_address_of_translateMix_4() { return &___translateMix_4; }
	inline void set_translateMix_4(float value)
	{
		___translateMix_4 = value;
	}

	inline static int32_t get_offset_of_scaleMix_5() { return static_cast<int32_t>(offsetof(TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A, ___scaleMix_5)); }
	inline float get_scaleMix_5() const { return ___scaleMix_5; }
	inline float* get_address_of_scaleMix_5() { return &___scaleMix_5; }
	inline void set_scaleMix_5(float value)
	{
		___scaleMix_5 = value;
	}

	inline static int32_t get_offset_of_shearMix_6() { return static_cast<int32_t>(offsetof(TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A, ___shearMix_6)); }
	inline float get_shearMix_6() const { return ___shearMix_6; }
	inline float* get_address_of_shearMix_6() { return &___shearMix_6; }
	inline void set_shearMix_6(float value)
	{
		___shearMix_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONSTRAINT_T1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A_H
#ifndef TRANSFORMCONSTRAINTDATA_T6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8_H
#define TRANSFORMCONSTRAINTDATA_T6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformConstraintData
struct  TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8  : public RuntimeObject
{
public:
	// System.String Spine.TransformConstraintData::name
	String_t* ___name_0;
	// System.Int32 Spine.TransformConstraintData::order
	int32_t ___order_1;
	// Spine.ExposedList`1<Spine.BoneData> Spine.TransformConstraintData::bones
	ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899 * ___bones_2;
	// Spine.BoneData Spine.TransformConstraintData::target
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * ___target_3;
	// System.Single Spine.TransformConstraintData::rotateMix
	float ___rotateMix_4;
	// System.Single Spine.TransformConstraintData::translateMix
	float ___translateMix_5;
	// System.Single Spine.TransformConstraintData::scaleMix
	float ___scaleMix_6;
	// System.Single Spine.TransformConstraintData::shearMix
	float ___shearMix_7;
	// System.Single Spine.TransformConstraintData::offsetRotation
	float ___offsetRotation_8;
	// System.Single Spine.TransformConstraintData::offsetX
	float ___offsetX_9;
	// System.Single Spine.TransformConstraintData::offsetY
	float ___offsetY_10;
	// System.Single Spine.TransformConstraintData::offsetScaleX
	float ___offsetScaleX_11;
	// System.Single Spine.TransformConstraintData::offsetScaleY
	float ___offsetScaleY_12;
	// System.Single Spine.TransformConstraintData::offsetShearY
	float ___offsetShearY_13;
	// System.Boolean Spine.TransformConstraintData::relative
	bool ___relative_14;
	// System.Boolean Spine.TransformConstraintData::local
	bool ___local_15;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_bones_2() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___bones_2)); }
	inline ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899 * get_bones_2() const { return ___bones_2; }
	inline ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899 ** get_address_of_bones_2() { return &___bones_2; }
	inline void set_bones_2(ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899 * value)
	{
		___bones_2 = value;
		Il2CppCodeGenWriteBarrier((&___bones_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___target_3)); }
	inline BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * get_target_3() const { return ___target_3; }
	inline BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_rotateMix_4() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___rotateMix_4)); }
	inline float get_rotateMix_4() const { return ___rotateMix_4; }
	inline float* get_address_of_rotateMix_4() { return &___rotateMix_4; }
	inline void set_rotateMix_4(float value)
	{
		___rotateMix_4 = value;
	}

	inline static int32_t get_offset_of_translateMix_5() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___translateMix_5)); }
	inline float get_translateMix_5() const { return ___translateMix_5; }
	inline float* get_address_of_translateMix_5() { return &___translateMix_5; }
	inline void set_translateMix_5(float value)
	{
		___translateMix_5 = value;
	}

	inline static int32_t get_offset_of_scaleMix_6() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___scaleMix_6)); }
	inline float get_scaleMix_6() const { return ___scaleMix_6; }
	inline float* get_address_of_scaleMix_6() { return &___scaleMix_6; }
	inline void set_scaleMix_6(float value)
	{
		___scaleMix_6 = value;
	}

	inline static int32_t get_offset_of_shearMix_7() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___shearMix_7)); }
	inline float get_shearMix_7() const { return ___shearMix_7; }
	inline float* get_address_of_shearMix_7() { return &___shearMix_7; }
	inline void set_shearMix_7(float value)
	{
		___shearMix_7 = value;
	}

	inline static int32_t get_offset_of_offsetRotation_8() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___offsetRotation_8)); }
	inline float get_offsetRotation_8() const { return ___offsetRotation_8; }
	inline float* get_address_of_offsetRotation_8() { return &___offsetRotation_8; }
	inline void set_offsetRotation_8(float value)
	{
		___offsetRotation_8 = value;
	}

	inline static int32_t get_offset_of_offsetX_9() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___offsetX_9)); }
	inline float get_offsetX_9() const { return ___offsetX_9; }
	inline float* get_address_of_offsetX_9() { return &___offsetX_9; }
	inline void set_offsetX_9(float value)
	{
		___offsetX_9 = value;
	}

	inline static int32_t get_offset_of_offsetY_10() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___offsetY_10)); }
	inline float get_offsetY_10() const { return ___offsetY_10; }
	inline float* get_address_of_offsetY_10() { return &___offsetY_10; }
	inline void set_offsetY_10(float value)
	{
		___offsetY_10 = value;
	}

	inline static int32_t get_offset_of_offsetScaleX_11() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___offsetScaleX_11)); }
	inline float get_offsetScaleX_11() const { return ___offsetScaleX_11; }
	inline float* get_address_of_offsetScaleX_11() { return &___offsetScaleX_11; }
	inline void set_offsetScaleX_11(float value)
	{
		___offsetScaleX_11 = value;
	}

	inline static int32_t get_offset_of_offsetScaleY_12() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___offsetScaleY_12)); }
	inline float get_offsetScaleY_12() const { return ___offsetScaleY_12; }
	inline float* get_address_of_offsetScaleY_12() { return &___offsetScaleY_12; }
	inline void set_offsetScaleY_12(float value)
	{
		___offsetScaleY_12 = value;
	}

	inline static int32_t get_offset_of_offsetShearY_13() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___offsetShearY_13)); }
	inline float get_offsetShearY_13() const { return ___offsetShearY_13; }
	inline float* get_address_of_offsetShearY_13() { return &___offsetShearY_13; }
	inline void set_offsetShearY_13(float value)
	{
		___offsetShearY_13 = value;
	}

	inline static int32_t get_offset_of_relative_14() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___relative_14)); }
	inline bool get_relative_14() const { return ___relative_14; }
	inline bool* get_address_of_relative_14() { return &___relative_14; }
	inline void set_relative_14(bool value)
	{
		___relative_14 = value;
	}

	inline static int32_t get_offset_of_local_15() { return static_cast<int32_t>(offsetof(TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8, ___local_15)); }
	inline bool get_local_15() const { return ___local_15; }
	inline bool* get_address_of_local_15() { return &___local_15; }
	inline void set_local_15(bool value)
	{
		___local_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONSTRAINTDATA_T6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8_H
#ifndef TRIANGULATOR_TA9050C221F20E12389B99839CFDC8CB3D8DFF8FC_H
#define TRIANGULATOR_TA9050C221F20E12389B99839CFDC8CB3D8DFF8FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Triangulator
struct  Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC  : public RuntimeObject
{
public:
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>> Spine.Triangulator::convexPolygons
	ExposedList_1_t9581BAC884D675A26267860B1FF48E5CC396C29D * ___convexPolygons_0;
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Int32>> Spine.Triangulator::convexPolygonsIndices
	ExposedList_1_tA2B5C15C080DBB9671A5521E5A131BF5D0611DC3 * ___convexPolygonsIndices_1;
	// Spine.ExposedList`1<System.Int32> Spine.Triangulator::indicesArray
	ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * ___indicesArray_2;
	// Spine.ExposedList`1<System.Boolean> Spine.Triangulator::isConcaveArray
	ExposedList_1_tAF5D637957B8D720F397F5305984F4DEAA6D8B69 * ___isConcaveArray_3;
	// Spine.ExposedList`1<System.Int32> Spine.Triangulator::triangles
	ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * ___triangles_4;
	// Spine.Pool`1<Spine.ExposedList`1<System.Single>> Spine.Triangulator::polygonPool
	Pool_1_t1685D1DC5C6E6B864C29696C73DB5A637C8967D1 * ___polygonPool_5;
	// Spine.Pool`1<Spine.ExposedList`1<System.Int32>> Spine.Triangulator::polygonIndicesPool
	Pool_1_t8B778139132F28F6ABAE16BE9D4E0CBA7AA4C50B * ___polygonIndicesPool_6;

public:
	inline static int32_t get_offset_of_convexPolygons_0() { return static_cast<int32_t>(offsetof(Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC, ___convexPolygons_0)); }
	inline ExposedList_1_t9581BAC884D675A26267860B1FF48E5CC396C29D * get_convexPolygons_0() const { return ___convexPolygons_0; }
	inline ExposedList_1_t9581BAC884D675A26267860B1FF48E5CC396C29D ** get_address_of_convexPolygons_0() { return &___convexPolygons_0; }
	inline void set_convexPolygons_0(ExposedList_1_t9581BAC884D675A26267860B1FF48E5CC396C29D * value)
	{
		___convexPolygons_0 = value;
		Il2CppCodeGenWriteBarrier((&___convexPolygons_0), value);
	}

	inline static int32_t get_offset_of_convexPolygonsIndices_1() { return static_cast<int32_t>(offsetof(Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC, ___convexPolygonsIndices_1)); }
	inline ExposedList_1_tA2B5C15C080DBB9671A5521E5A131BF5D0611DC3 * get_convexPolygonsIndices_1() const { return ___convexPolygonsIndices_1; }
	inline ExposedList_1_tA2B5C15C080DBB9671A5521E5A131BF5D0611DC3 ** get_address_of_convexPolygonsIndices_1() { return &___convexPolygonsIndices_1; }
	inline void set_convexPolygonsIndices_1(ExposedList_1_tA2B5C15C080DBB9671A5521E5A131BF5D0611DC3 * value)
	{
		___convexPolygonsIndices_1 = value;
		Il2CppCodeGenWriteBarrier((&___convexPolygonsIndices_1), value);
	}

	inline static int32_t get_offset_of_indicesArray_2() { return static_cast<int32_t>(offsetof(Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC, ___indicesArray_2)); }
	inline ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * get_indicesArray_2() const { return ___indicesArray_2; }
	inline ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 ** get_address_of_indicesArray_2() { return &___indicesArray_2; }
	inline void set_indicesArray_2(ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * value)
	{
		___indicesArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___indicesArray_2), value);
	}

	inline static int32_t get_offset_of_isConcaveArray_3() { return static_cast<int32_t>(offsetof(Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC, ___isConcaveArray_3)); }
	inline ExposedList_1_tAF5D637957B8D720F397F5305984F4DEAA6D8B69 * get_isConcaveArray_3() const { return ___isConcaveArray_3; }
	inline ExposedList_1_tAF5D637957B8D720F397F5305984F4DEAA6D8B69 ** get_address_of_isConcaveArray_3() { return &___isConcaveArray_3; }
	inline void set_isConcaveArray_3(ExposedList_1_tAF5D637957B8D720F397F5305984F4DEAA6D8B69 * value)
	{
		___isConcaveArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___isConcaveArray_3), value);
	}

	inline static int32_t get_offset_of_triangles_4() { return static_cast<int32_t>(offsetof(Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC, ___triangles_4)); }
	inline ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * get_triangles_4() const { return ___triangles_4; }
	inline ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 ** get_address_of_triangles_4() { return &___triangles_4; }
	inline void set_triangles_4(ExposedList_1_tA1DFED4E8A67C3DA60DC9FC8D8B9CD39BAC7F960 * value)
	{
		___triangles_4 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_4), value);
	}

	inline static int32_t get_offset_of_polygonPool_5() { return static_cast<int32_t>(offsetof(Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC, ___polygonPool_5)); }
	inline Pool_1_t1685D1DC5C6E6B864C29696C73DB5A637C8967D1 * get_polygonPool_5() const { return ___polygonPool_5; }
	inline Pool_1_t1685D1DC5C6E6B864C29696C73DB5A637C8967D1 ** get_address_of_polygonPool_5() { return &___polygonPool_5; }
	inline void set_polygonPool_5(Pool_1_t1685D1DC5C6E6B864C29696C73DB5A637C8967D1 * value)
	{
		___polygonPool_5 = value;
		Il2CppCodeGenWriteBarrier((&___polygonPool_5), value);
	}

	inline static int32_t get_offset_of_polygonIndicesPool_6() { return static_cast<int32_t>(offsetof(Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC, ___polygonIndicesPool_6)); }
	inline Pool_1_t8B778139132F28F6ABAE16BE9D4E0CBA7AA4C50B * get_polygonIndicesPool_6() const { return ___polygonIndicesPool_6; }
	inline Pool_1_t8B778139132F28F6ABAE16BE9D4E0CBA7AA4C50B ** get_address_of_polygonIndicesPool_6() { return &___polygonIndicesPool_6; }
	inline void set_polygonIndicesPool_6(Pool_1_t8B778139132F28F6ABAE16BE9D4E0CBA7AA4C50B * value)
	{
		___polygonIndicesPool_6 = value;
		Il2CppCodeGenWriteBarrier((&___polygonIndicesPool_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATOR_TA9050C221F20E12389B99839CFDC8CB3D8DFF8FC_H
#ifndef MATERIALSTEXTURELOADER_TF62D3F45F7142E9015E50D858C4C5CFD1FF864FF_H
#define MATERIALSTEXTURELOADER_TF62D3F45F7142E9015E50D858C4C5CFD1FF864FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MaterialsTextureLoader
struct  MaterialsTextureLoader_tF62D3F45F7142E9015E50D858C4C5CFD1FF864FF  : public RuntimeObject
{
public:
	// Spine.Unity.AtlasAsset Spine.Unity.MaterialsTextureLoader::atlasAsset
	AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 * ___atlasAsset_0;

public:
	inline static int32_t get_offset_of_atlasAsset_0() { return static_cast<int32_t>(offsetof(MaterialsTextureLoader_tF62D3F45F7142E9015E50D858C4C5CFD1FF864FF, ___atlasAsset_0)); }
	inline AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 * get_atlasAsset_0() const { return ___atlasAsset_0; }
	inline AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 ** get_address_of_atlasAsset_0() { return &___atlasAsset_0; }
	inline void set_atlasAsset_0(AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47 * value)
	{
		___atlasAsset_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAsset_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALSTEXTURELOADER_TF62D3F45F7142E9015E50D858C4C5CFD1FF864FF_H
#ifndef MESHRENDERERBUFFERS_T4C818805F575A9FB385E0AC7F4E8FD56623AA107_H
#define MESHRENDERERBUFFERS_T4C818805F575A9FB385E0AC7F4E8FD56623AA107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshRendererBuffers
struct  MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107  : public RuntimeObject
{
public:
	// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh> Spine.Unity.MeshRendererBuffers::doubleBufferedMesh
	DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC * ___doubleBufferedMesh_0;
	// Spine.ExposedList`1<UnityEngine.Material> Spine.Unity.MeshRendererBuffers::submeshMaterials
	ExposedList_1_tF74661C2F8E20C6B8429AFE8DBE767C1B055095F * ___submeshMaterials_1;
	// UnityEngine.Material[] Spine.Unity.MeshRendererBuffers::sharedMaterials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___sharedMaterials_2;

public:
	inline static int32_t get_offset_of_doubleBufferedMesh_0() { return static_cast<int32_t>(offsetof(MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107, ___doubleBufferedMesh_0)); }
	inline DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC * get_doubleBufferedMesh_0() const { return ___doubleBufferedMesh_0; }
	inline DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC ** get_address_of_doubleBufferedMesh_0() { return &___doubleBufferedMesh_0; }
	inline void set_doubleBufferedMesh_0(DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC * value)
	{
		___doubleBufferedMesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___doubleBufferedMesh_0), value);
	}

	inline static int32_t get_offset_of_submeshMaterials_1() { return static_cast<int32_t>(offsetof(MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107, ___submeshMaterials_1)); }
	inline ExposedList_1_tF74661C2F8E20C6B8429AFE8DBE767C1B055095F * get_submeshMaterials_1() const { return ___submeshMaterials_1; }
	inline ExposedList_1_tF74661C2F8E20C6B8429AFE8DBE767C1B055095F ** get_address_of_submeshMaterials_1() { return &___submeshMaterials_1; }
	inline void set_submeshMaterials_1(ExposedList_1_tF74661C2F8E20C6B8429AFE8DBE767C1B055095F * value)
	{
		___submeshMaterials_1 = value;
		Il2CppCodeGenWriteBarrier((&___submeshMaterials_1), value);
	}

	inline static int32_t get_offset_of_sharedMaterials_2() { return static_cast<int32_t>(offsetof(MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107, ___sharedMaterials_2)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_sharedMaterials_2() const { return ___sharedMaterials_2; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_sharedMaterials_2() { return &___sharedMaterials_2; }
	inline void set_sharedMaterials_2(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___sharedMaterials_2 = value;
		Il2CppCodeGenWriteBarrier((&___sharedMaterials_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERERBUFFERS_T4C818805F575A9FB385E0AC7F4E8FD56623AA107_H
#ifndef SMARTMESH_T600BE08EF446BE08EEA7958EEFE801B34037E068_H
#define SMARTMESH_T600BE08EF446BE08EEA7958EEFE801B34037E068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshRendererBuffers/SmartMesh
struct  SmartMesh_t600BE08EF446BE08EEA7958EEFE801B34037E068  : public RuntimeObject
{
public:
	// UnityEngine.Mesh Spine.Unity.MeshRendererBuffers/SmartMesh::mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_0;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.MeshRendererBuffers/SmartMesh::instructionUsed
	SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * ___instructionUsed_1;

public:
	inline static int32_t get_offset_of_mesh_0() { return static_cast<int32_t>(offsetof(SmartMesh_t600BE08EF446BE08EEA7958EEFE801B34037E068, ___mesh_0)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_mesh_0() const { return ___mesh_0; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_mesh_0() { return &___mesh_0; }
	inline void set_mesh_0(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___mesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_0), value);
	}

	inline static int32_t get_offset_of_instructionUsed_1() { return static_cast<int32_t>(offsetof(SmartMesh_t600BE08EF446BE08EEA7958EEFE801B34037E068, ___instructionUsed_1)); }
	inline SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * get_instructionUsed_1() const { return ___instructionUsed_1; }
	inline SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A ** get_address_of_instructionUsed_1() { return &___instructionUsed_1; }
	inline void set_instructionUsed_1(SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * value)
	{
		___instructionUsed_1 = value;
		Il2CppCodeGenWriteBarrier((&___instructionUsed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTMESH_T600BE08EF446BE08EEA7958EEFE801B34037E068_H
#ifndef REGIONLESSATTACHMENTLOADER_TBF85DAF1D0DA865CAC8D97AF79DE3D7FF3ED762C_H
#define REGIONLESSATTACHMENTLOADER_TBF85DAF1D0DA865CAC8D97AF79DE3D7FF3ED762C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.RegionlessAttachmentLoader
struct  RegionlessAttachmentLoader_tBF85DAF1D0DA865CAC8D97AF79DE3D7FF3ED762C  : public RuntimeObject
{
public:

public:
};

struct RegionlessAttachmentLoader_tBF85DAF1D0DA865CAC8D97AF79DE3D7FF3ED762C_StaticFields
{
public:
	// Spine.AtlasRegion Spine.Unity.RegionlessAttachmentLoader::emptyRegion
	AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30 * ___emptyRegion_0;

public:
	inline static int32_t get_offset_of_emptyRegion_0() { return static_cast<int32_t>(offsetof(RegionlessAttachmentLoader_tBF85DAF1D0DA865CAC8D97AF79DE3D7FF3ED762C_StaticFields, ___emptyRegion_0)); }
	inline AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30 * get_emptyRegion_0() const { return ___emptyRegion_0; }
	inline AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30 ** get_address_of_emptyRegion_0() { return &___emptyRegion_0; }
	inline void set_emptyRegion_0(AtlasRegion_tDC32F693E116CBB7C9DD128D1D9A168BA00AEF30 * value)
	{
		___emptyRegion_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyRegion_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONLESSATTACHMENTLOADER_TBF85DAF1D0DA865CAC8D97AF79DE3D7FF3ED762C_H
#ifndef MECANIMTRANSLATOR_TB7158724E1E6409994986C35527D525190B1E1D1_H
#define MECANIMTRANSLATOR_TB7158724E1E6409994986C35527D525190B1E1D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator
struct  MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.SkeletonAnimator/MecanimTranslator::autoReset
	bool ___autoReset_0;
	// Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode[] Spine.Unity.SkeletonAnimator/MecanimTranslator::layerMixModes
	MixModeU5BU5D_t28548C79DF95F6D1F3B52866B08499B0EFDC0910* ___layerMixModes_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,Spine.Animation> Spine.Unity.SkeletonAnimator/MecanimTranslator::animationTable
	Dictionary_2_t9EC5A030DCF7A83F350091E7207E8A0CB9825F43 * ___animationTable_2;
	// System.Collections.Generic.Dictionary`2<UnityEngine.AnimationClip,System.Int32> Spine.Unity.SkeletonAnimator/MecanimTranslator::clipNameHashCodeTable
	Dictionary_2_tCC3E0800B3394E0A2AB78BDA00108CAFD03D05F4 * ___clipNameHashCodeTable_3;
	// System.Collections.Generic.List`1<Spine.Animation> Spine.Unity.SkeletonAnimator/MecanimTranslator::previousAnimations
	List_1_tDE08A2AA48EDC4BAF1D96E78046A75F98F7FA7DD * ___previousAnimations_4;
	// Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos[] Spine.Unity.SkeletonAnimator/MecanimTranslator::layerClipInfos
	ClipInfosU5BU5D_tC874B18B2FBA9F9DE751EFB44564F4A8DD94CF37* ___layerClipInfos_5;
	// UnityEngine.Animator Spine.Unity.SkeletonAnimator/MecanimTranslator::animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___animator_6;

public:
	inline static int32_t get_offset_of_autoReset_0() { return static_cast<int32_t>(offsetof(MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1, ___autoReset_0)); }
	inline bool get_autoReset_0() const { return ___autoReset_0; }
	inline bool* get_address_of_autoReset_0() { return &___autoReset_0; }
	inline void set_autoReset_0(bool value)
	{
		___autoReset_0 = value;
	}

	inline static int32_t get_offset_of_layerMixModes_1() { return static_cast<int32_t>(offsetof(MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1, ___layerMixModes_1)); }
	inline MixModeU5BU5D_t28548C79DF95F6D1F3B52866B08499B0EFDC0910* get_layerMixModes_1() const { return ___layerMixModes_1; }
	inline MixModeU5BU5D_t28548C79DF95F6D1F3B52866B08499B0EFDC0910** get_address_of_layerMixModes_1() { return &___layerMixModes_1; }
	inline void set_layerMixModes_1(MixModeU5BU5D_t28548C79DF95F6D1F3B52866B08499B0EFDC0910* value)
	{
		___layerMixModes_1 = value;
		Il2CppCodeGenWriteBarrier((&___layerMixModes_1), value);
	}

	inline static int32_t get_offset_of_animationTable_2() { return static_cast<int32_t>(offsetof(MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1, ___animationTable_2)); }
	inline Dictionary_2_t9EC5A030DCF7A83F350091E7207E8A0CB9825F43 * get_animationTable_2() const { return ___animationTable_2; }
	inline Dictionary_2_t9EC5A030DCF7A83F350091E7207E8A0CB9825F43 ** get_address_of_animationTable_2() { return &___animationTable_2; }
	inline void set_animationTable_2(Dictionary_2_t9EC5A030DCF7A83F350091E7207E8A0CB9825F43 * value)
	{
		___animationTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___animationTable_2), value);
	}

	inline static int32_t get_offset_of_clipNameHashCodeTable_3() { return static_cast<int32_t>(offsetof(MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1, ___clipNameHashCodeTable_3)); }
	inline Dictionary_2_tCC3E0800B3394E0A2AB78BDA00108CAFD03D05F4 * get_clipNameHashCodeTable_3() const { return ___clipNameHashCodeTable_3; }
	inline Dictionary_2_tCC3E0800B3394E0A2AB78BDA00108CAFD03D05F4 ** get_address_of_clipNameHashCodeTable_3() { return &___clipNameHashCodeTable_3; }
	inline void set_clipNameHashCodeTable_3(Dictionary_2_tCC3E0800B3394E0A2AB78BDA00108CAFD03D05F4 * value)
	{
		___clipNameHashCodeTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___clipNameHashCodeTable_3), value);
	}

	inline static int32_t get_offset_of_previousAnimations_4() { return static_cast<int32_t>(offsetof(MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1, ___previousAnimations_4)); }
	inline List_1_tDE08A2AA48EDC4BAF1D96E78046A75F98F7FA7DD * get_previousAnimations_4() const { return ___previousAnimations_4; }
	inline List_1_tDE08A2AA48EDC4BAF1D96E78046A75F98F7FA7DD ** get_address_of_previousAnimations_4() { return &___previousAnimations_4; }
	inline void set_previousAnimations_4(List_1_tDE08A2AA48EDC4BAF1D96E78046A75F98F7FA7DD * value)
	{
		___previousAnimations_4 = value;
		Il2CppCodeGenWriteBarrier((&___previousAnimations_4), value);
	}

	inline static int32_t get_offset_of_layerClipInfos_5() { return static_cast<int32_t>(offsetof(MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1, ___layerClipInfos_5)); }
	inline ClipInfosU5BU5D_tC874B18B2FBA9F9DE751EFB44564F4A8DD94CF37* get_layerClipInfos_5() const { return ___layerClipInfos_5; }
	inline ClipInfosU5BU5D_tC874B18B2FBA9F9DE751EFB44564F4A8DD94CF37** get_address_of_layerClipInfos_5() { return &___layerClipInfos_5; }
	inline void set_layerClipInfos_5(ClipInfosU5BU5D_tC874B18B2FBA9F9DE751EFB44564F4A8DD94CF37* value)
	{
		___layerClipInfos_5 = value;
		Il2CppCodeGenWriteBarrier((&___layerClipInfos_5), value);
	}

	inline static int32_t get_offset_of_animator_6() { return static_cast<int32_t>(offsetof(MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1, ___animator_6)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_animator_6() const { return ___animator_6; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_animator_6() { return &___animator_6; }
	inline void set_animator_6(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___animator_6 = value;
		Il2CppCodeGenWriteBarrier((&___animator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MECANIMTRANSLATOR_TB7158724E1E6409994986C35527D525190B1E1D1_H
#ifndef ANIMATIONCLIPEQUALITYCOMPARER_T70C5049B6149030ABD8CC89712AFB4E0C3AABADA_H
#define ANIMATIONCLIPEQUALITYCOMPARER_T70C5049B6149030ABD8CC89712AFB4E0C3AABADA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/AnimationClipEqualityComparer
struct  AnimationClipEqualityComparer_t70C5049B6149030ABD8CC89712AFB4E0C3AABADA  : public RuntimeObject
{
public:

public:
};

struct AnimationClipEqualityComparer_t70C5049B6149030ABD8CC89712AFB4E0C3AABADA_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.AnimationClip> Spine.Unity.SkeletonAnimator/MecanimTranslator/AnimationClipEqualityComparer::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(AnimationClipEqualityComparer_t70C5049B6149030ABD8CC89712AFB4E0C3AABADA_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIPEQUALITYCOMPARER_T70C5049B6149030ABD8CC89712AFB4E0C3AABADA_H
#ifndef INTEQUALITYCOMPARER_T2F935A9DFA1A81628BB410C36C80AB41D12CB0D6_H
#define INTEQUALITYCOMPARER_T2F935A9DFA1A81628BB410C36C80AB41D12CB0D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/IntEqualityComparer
struct  IntEqualityComparer_t2F935A9DFA1A81628BB410C36C80AB41D12CB0D6  : public RuntimeObject
{
public:

public:
};

struct IntEqualityComparer_t2F935A9DFA1A81628BB410C36C80AB41D12CB0D6_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<System.Int32> Spine.Unity.SkeletonAnimator/MecanimTranslator/IntEqualityComparer::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(IntEqualityComparer_t2F935A9DFA1A81628BB410C36C80AB41D12CB0D6_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEQUALITYCOMPARER_T2F935A9DFA1A81628BB410C36C80AB41D12CB0D6_H
#ifndef SKELETONEXTENSIONS_T66E50F5258A0713714C11F389E7FFA78EAB66699_H
#define SKELETONEXTENSIONS_T66E50F5258A0713714C11F389E7FFA78EAB66699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonExtensions
struct  SkeletonExtensions_t66E50F5258A0713714C11F389E7FFA78EAB66699  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONEXTENSIONS_T66E50F5258A0713714C11F389E7FFA78EAB66699_H
#ifndef SKELETONRENDERERINSTRUCTION_T20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A_H
#define SKELETONRENDERERINSTRUCTION_T20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRendererInstruction
struct  SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.SkeletonRendererInstruction::immutableTriangles
	bool ___immutableTriangles_0;
	// Spine.ExposedList`1<Spine.Unity.SubmeshInstruction> Spine.Unity.SkeletonRendererInstruction::submeshInstructions
	ExposedList_1_tF0ACB19929B7EFBF260E9588A963EC02D1EF6571 * ___submeshInstructions_1;
	// System.Boolean Spine.Unity.SkeletonRendererInstruction::hasActiveClipping
	bool ___hasActiveClipping_2;
	// System.Int32 Spine.Unity.SkeletonRendererInstruction::rawVertexCount
	int32_t ___rawVertexCount_3;
	// Spine.ExposedList`1<Spine.Attachment> Spine.Unity.SkeletonRendererInstruction::attachments
	ExposedList_1_tE8852F51C99129DA2CBB62736EDCC23FB9604F74 * ___attachments_4;

public:
	inline static int32_t get_offset_of_immutableTriangles_0() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A, ___immutableTriangles_0)); }
	inline bool get_immutableTriangles_0() const { return ___immutableTriangles_0; }
	inline bool* get_address_of_immutableTriangles_0() { return &___immutableTriangles_0; }
	inline void set_immutableTriangles_0(bool value)
	{
		___immutableTriangles_0 = value;
	}

	inline static int32_t get_offset_of_submeshInstructions_1() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A, ___submeshInstructions_1)); }
	inline ExposedList_1_tF0ACB19929B7EFBF260E9588A963EC02D1EF6571 * get_submeshInstructions_1() const { return ___submeshInstructions_1; }
	inline ExposedList_1_tF0ACB19929B7EFBF260E9588A963EC02D1EF6571 ** get_address_of_submeshInstructions_1() { return &___submeshInstructions_1; }
	inline void set_submeshInstructions_1(ExposedList_1_tF0ACB19929B7EFBF260E9588A963EC02D1EF6571 * value)
	{
		___submeshInstructions_1 = value;
		Il2CppCodeGenWriteBarrier((&___submeshInstructions_1), value);
	}

	inline static int32_t get_offset_of_hasActiveClipping_2() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A, ___hasActiveClipping_2)); }
	inline bool get_hasActiveClipping_2() const { return ___hasActiveClipping_2; }
	inline bool* get_address_of_hasActiveClipping_2() { return &___hasActiveClipping_2; }
	inline void set_hasActiveClipping_2(bool value)
	{
		___hasActiveClipping_2 = value;
	}

	inline static int32_t get_offset_of_rawVertexCount_3() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A, ___rawVertexCount_3)); }
	inline int32_t get_rawVertexCount_3() const { return ___rawVertexCount_3; }
	inline int32_t* get_address_of_rawVertexCount_3() { return &___rawVertexCount_3; }
	inline void set_rawVertexCount_3(int32_t value)
	{
		___rawVertexCount_3 = value;
	}

	inline static int32_t get_offset_of_attachments_4() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A, ___attachments_4)); }
	inline ExposedList_1_tE8852F51C99129DA2CBB62736EDCC23FB9604F74 * get_attachments_4() const { return ___attachments_4; }
	inline ExposedList_1_tE8852F51C99129DA2CBB62736EDCC23FB9604F74 ** get_address_of_attachments_4() { return &___attachments_4; }
	inline void set_attachments_4(ExposedList_1_tE8852F51C99129DA2CBB62736EDCC23FB9604F74 * value)
	{
		___attachments_4 = value;
		Il2CppCodeGenWriteBarrier((&___attachments_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERERINSTRUCTION_T20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A_H
#ifndef WAITFORSPINEANIMATIONCOMPLETE_T46B9D7E64E134CDC3520A1750BB1B6B4743D1D34_H
#define WAITFORSPINEANIMATIONCOMPLETE_T46B9D7E64E134CDC3520A1750BB1B6B4743D1D34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineAnimationComplete
struct  WaitForSpineAnimationComplete_t46B9D7E64E134CDC3520A1750BB1B6B4743D1D34  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.WaitForSpineAnimationComplete::m_WasFired
	bool ___m_WasFired_0;

public:
	inline static int32_t get_offset_of_m_WasFired_0() { return static_cast<int32_t>(offsetof(WaitForSpineAnimationComplete_t46B9D7E64E134CDC3520A1750BB1B6B4743D1D34, ___m_WasFired_0)); }
	inline bool get_m_WasFired_0() const { return ___m_WasFired_0; }
	inline bool* get_address_of_m_WasFired_0() { return &___m_WasFired_0; }
	inline void set_m_WasFired_0(bool value)
	{
		___m_WasFired_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINEANIMATIONCOMPLETE_T46B9D7E64E134CDC3520A1750BB1B6B4743D1D34_H
#ifndef WAITFORSPINEEVENT_TA8441A771D71E682D87CE453D33368F15177278A_H
#define WAITFORSPINEEVENT_TA8441A771D71E682D87CE453D33368F15177278A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineEvent
struct  WaitForSpineEvent_tA8441A771D71E682D87CE453D33368F15177278A  : public RuntimeObject
{
public:
	// Spine.EventData Spine.Unity.WaitForSpineEvent::m_TargetEvent
	EventData_t34B08BE715A229D34906F96784419D669DE4869E * ___m_TargetEvent_0;
	// System.String Spine.Unity.WaitForSpineEvent::m_EventName
	String_t* ___m_EventName_1;
	// Spine.AnimationState Spine.Unity.WaitForSpineEvent::m_AnimationState
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * ___m_AnimationState_2;
	// System.Boolean Spine.Unity.WaitForSpineEvent::m_WasFired
	bool ___m_WasFired_3;
	// System.Boolean Spine.Unity.WaitForSpineEvent::m_unsubscribeAfterFiring
	bool ___m_unsubscribeAfterFiring_4;

public:
	inline static int32_t get_offset_of_m_TargetEvent_0() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_tA8441A771D71E682D87CE453D33368F15177278A, ___m_TargetEvent_0)); }
	inline EventData_t34B08BE715A229D34906F96784419D669DE4869E * get_m_TargetEvent_0() const { return ___m_TargetEvent_0; }
	inline EventData_t34B08BE715A229D34906F96784419D669DE4869E ** get_address_of_m_TargetEvent_0() { return &___m_TargetEvent_0; }
	inline void set_m_TargetEvent_0(EventData_t34B08BE715A229D34906F96784419D669DE4869E * value)
	{
		___m_TargetEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetEvent_0), value);
	}

	inline static int32_t get_offset_of_m_EventName_1() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_tA8441A771D71E682D87CE453D33368F15177278A, ___m_EventName_1)); }
	inline String_t* get_m_EventName_1() const { return ___m_EventName_1; }
	inline String_t** get_address_of_m_EventName_1() { return &___m_EventName_1; }
	inline void set_m_EventName_1(String_t* value)
	{
		___m_EventName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_1), value);
	}

	inline static int32_t get_offset_of_m_AnimationState_2() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_tA8441A771D71E682D87CE453D33368F15177278A, ___m_AnimationState_2)); }
	inline AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * get_m_AnimationState_2() const { return ___m_AnimationState_2; }
	inline AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 ** get_address_of_m_AnimationState_2() { return &___m_AnimationState_2; }
	inline void set_m_AnimationState_2(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * value)
	{
		___m_AnimationState_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationState_2), value);
	}

	inline static int32_t get_offset_of_m_WasFired_3() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_tA8441A771D71E682D87CE453D33368F15177278A, ___m_WasFired_3)); }
	inline bool get_m_WasFired_3() const { return ___m_WasFired_3; }
	inline bool* get_address_of_m_WasFired_3() { return &___m_WasFired_3; }
	inline void set_m_WasFired_3(bool value)
	{
		___m_WasFired_3 = value;
	}

	inline static int32_t get_offset_of_m_unsubscribeAfterFiring_4() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_tA8441A771D71E682D87CE453D33368F15177278A, ___m_unsubscribeAfterFiring_4)); }
	inline bool get_m_unsubscribeAfterFiring_4() const { return ___m_unsubscribeAfterFiring_4; }
	inline bool* get_address_of_m_unsubscribeAfterFiring_4() { return &___m_unsubscribeAfterFiring_4; }
	inline void set_m_unsubscribeAfterFiring_4(bool value)
	{
		___m_unsubscribeAfterFiring_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINEEVENT_TA8441A771D71E682D87CE453D33368F15177278A_H
#ifndef WAITFORSPINETRACKENTRYEND_T6B8DE50A564C92F3E73A25FA6C245592B5D77776_H
#define WAITFORSPINETRACKENTRYEND_T6B8DE50A564C92F3E73A25FA6C245592B5D77776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineTrackEntryEnd
struct  WaitForSpineTrackEntryEnd_t6B8DE50A564C92F3E73A25FA6C245592B5D77776  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.WaitForSpineTrackEntryEnd::m_WasFired
	bool ___m_WasFired_0;

public:
	inline static int32_t get_offset_of_m_WasFired_0() { return static_cast<int32_t>(offsetof(WaitForSpineTrackEntryEnd_t6B8DE50A564C92F3E73A25FA6C245592B5D77776, ___m_WasFired_0)); }
	inline bool get_m_WasFired_0() const { return ___m_WasFired_0; }
	inline bool* get_address_of_m_WasFired_0() { return &___m_WasFired_0; }
	inline void set_m_WasFired_0(bool value)
	{
		___m_WasFired_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINETRACKENTRYEND_T6B8DE50A564C92F3E73A25FA6C245592B5D77776_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef PLAYABLEBEHAVIOUR_T5F4AA32E735199182CC5F57D426D27BE8ABA8F01_H
#define PLAYABLEBEHAVIOUR_T5F4AA32E735199182CC5F57D426D27BE8ABA8F01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T5F4AA32E735199182CC5F57D426D27BE8ABA8F01_H
#ifndef BONEMATRIX_TC33EDC47C7F65348ED57581B5302B35859F5B433_H
#define BONEMATRIX_TC33EDC47C7F65348ED57581B5302B35859F5B433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BoneMatrix
struct  BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433 
{
public:
	// System.Single Spine.BoneMatrix::a
	float ___a_0;
	// System.Single Spine.BoneMatrix::b
	float ___b_1;
	// System.Single Spine.BoneMatrix::c
	float ___c_2;
	// System.Single Spine.BoneMatrix::d
	float ___d_3;
	// System.Single Spine.BoneMatrix::x
	float ___x_4;
	// System.Single Spine.BoneMatrix::y
	float ___y_5;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433, ___a_0)); }
	inline float get_a_0() const { return ___a_0; }
	inline float* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(float value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433, ___b_1)); }
	inline float get_b_1() const { return ___b_1; }
	inline float* get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(float value)
	{
		___b_1 = value;
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433, ___c_2)); }
	inline float get_c_2() const { return ___c_2; }
	inline float* get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(float value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433, ___d_3)); }
	inline float get_d_3() const { return ___d_3; }
	inline float* get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(float value)
	{
		___d_3 = value;
	}

	inline static int32_t get_offset_of_x_4() { return static_cast<int32_t>(offsetof(BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433, ___x_4)); }
	inline float get_x_4() const { return ___x_4; }
	inline float* get_address_of_x_4() { return &___x_4; }
	inline void set_x_4(float value)
	{
		___x_4 = value;
	}

	inline static int32_t get_offset_of_y_5() { return static_cast<int32_t>(offsetof(BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433, ___y_5)); }
	inline float get_y_5() const { return ___y_5; }
	inline float* get_address_of_y_5() { return &___y_5; }
	inline void set_y_5(float value)
	{
		___y_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEMATRIX_TC33EDC47C7F65348ED57581B5302B35859F5B433_H
#ifndef ATTACHMENTKEYTUPLE_TF6B72B8FC590685692B39913EDAA7D910E816946_H
#define ATTACHMENTKEYTUPLE_TF6B72B8FC590685692B39913EDAA7D910E816946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skin/AttachmentKeyTuple
struct  AttachmentKeyTuple_tF6B72B8FC590685692B39913EDAA7D910E816946 
{
public:
	// System.Int32 Spine.Skin/AttachmentKeyTuple::slotIndex
	int32_t ___slotIndex_0;
	// System.String Spine.Skin/AttachmentKeyTuple::name
	String_t* ___name_1;
	// System.Int32 Spine.Skin/AttachmentKeyTuple::nameHashCode
	int32_t ___nameHashCode_2;

public:
	inline static int32_t get_offset_of_slotIndex_0() { return static_cast<int32_t>(offsetof(AttachmentKeyTuple_tF6B72B8FC590685692B39913EDAA7D910E816946, ___slotIndex_0)); }
	inline int32_t get_slotIndex_0() const { return ___slotIndex_0; }
	inline int32_t* get_address_of_slotIndex_0() { return &___slotIndex_0; }
	inline void set_slotIndex_0(int32_t value)
	{
		___slotIndex_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(AttachmentKeyTuple_tF6B72B8FC590685692B39913EDAA7D910E816946, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_nameHashCode_2() { return static_cast<int32_t>(offsetof(AttachmentKeyTuple_tF6B72B8FC590685692B39913EDAA7D910E816946, ___nameHashCode_2)); }
	inline int32_t get_nameHashCode_2() const { return ___nameHashCode_2; }
	inline int32_t* get_address_of_nameHashCode_2() { return &___nameHashCode_2; }
	inline void set_nameHashCode_2(int32_t value)
	{
		___nameHashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Skin/AttachmentKeyTuple
struct AttachmentKeyTuple_tF6B72B8FC590685692B39913EDAA7D910E816946_marshaled_pinvoke
{
	int32_t ___slotIndex_0;
	char* ___name_1;
	int32_t ___nameHashCode_2;
};
// Native definition for COM marshalling of Spine.Skin/AttachmentKeyTuple
struct AttachmentKeyTuple_tF6B72B8FC590685692B39913EDAA7D910E816946_marshaled_com
{
	int32_t ___slotIndex_0;
	Il2CppChar* ___name_1;
	int32_t ___nameHashCode_2;
};
#endif // ATTACHMENTKEYTUPLE_TF6B72B8FC590685692B39913EDAA7D910E816946_H
#ifndef SETTINGS_TDAD9B4E9DC683D5ACC2903EE333276598006710D_H
#define SETTINGS_TDAD9B4E9DC683D5ACC2903EE333276598006710D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGenerator/Settings
struct  Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D 
{
public:
	// System.Boolean Spine.Unity.MeshGenerator/Settings::useClipping
	bool ___useClipping_0;
	// System.Single Spine.Unity.MeshGenerator/Settings::zSpacing
	float ___zSpacing_1;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::pmaVertexColors
	bool ___pmaVertexColors_2;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::tintBlack
	bool ___tintBlack_3;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::calculateTangents
	bool ___calculateTangents_4;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::addNormals
	bool ___addNormals_5;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::immutableTriangles
	bool ___immutableTriangles_6;

public:
	inline static int32_t get_offset_of_useClipping_0() { return static_cast<int32_t>(offsetof(Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D, ___useClipping_0)); }
	inline bool get_useClipping_0() const { return ___useClipping_0; }
	inline bool* get_address_of_useClipping_0() { return &___useClipping_0; }
	inline void set_useClipping_0(bool value)
	{
		___useClipping_0 = value;
	}

	inline static int32_t get_offset_of_zSpacing_1() { return static_cast<int32_t>(offsetof(Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D, ___zSpacing_1)); }
	inline float get_zSpacing_1() const { return ___zSpacing_1; }
	inline float* get_address_of_zSpacing_1() { return &___zSpacing_1; }
	inline void set_zSpacing_1(float value)
	{
		___zSpacing_1 = value;
	}

	inline static int32_t get_offset_of_pmaVertexColors_2() { return static_cast<int32_t>(offsetof(Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D, ___pmaVertexColors_2)); }
	inline bool get_pmaVertexColors_2() const { return ___pmaVertexColors_2; }
	inline bool* get_address_of_pmaVertexColors_2() { return &___pmaVertexColors_2; }
	inline void set_pmaVertexColors_2(bool value)
	{
		___pmaVertexColors_2 = value;
	}

	inline static int32_t get_offset_of_tintBlack_3() { return static_cast<int32_t>(offsetof(Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D, ___tintBlack_3)); }
	inline bool get_tintBlack_3() const { return ___tintBlack_3; }
	inline bool* get_address_of_tintBlack_3() { return &___tintBlack_3; }
	inline void set_tintBlack_3(bool value)
	{
		___tintBlack_3 = value;
	}

	inline static int32_t get_offset_of_calculateTangents_4() { return static_cast<int32_t>(offsetof(Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D, ___calculateTangents_4)); }
	inline bool get_calculateTangents_4() const { return ___calculateTangents_4; }
	inline bool* get_address_of_calculateTangents_4() { return &___calculateTangents_4; }
	inline void set_calculateTangents_4(bool value)
	{
		___calculateTangents_4 = value;
	}

	inline static int32_t get_offset_of_addNormals_5() { return static_cast<int32_t>(offsetof(Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D, ___addNormals_5)); }
	inline bool get_addNormals_5() const { return ___addNormals_5; }
	inline bool* get_address_of_addNormals_5() { return &___addNormals_5; }
	inline void set_addNormals_5(bool value)
	{
		___addNormals_5 = value;
	}

	inline static int32_t get_offset_of_immutableTriangles_6() { return static_cast<int32_t>(offsetof(Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D, ___immutableTriangles_6)); }
	inline bool get_immutableTriangles_6() const { return ___immutableTriangles_6; }
	inline bool* get_address_of_immutableTriangles_6() { return &___immutableTriangles_6; }
	inline void set_immutableTriangles_6(bool value)
	{
		___immutableTriangles_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.MeshGenerator/Settings
struct Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D_marshaled_pinvoke
{
	int32_t ___useClipping_0;
	float ___zSpacing_1;
	int32_t ___pmaVertexColors_2;
	int32_t ___tintBlack_3;
	int32_t ___calculateTangents_4;
	int32_t ___addNormals_5;
	int32_t ___immutableTriangles_6;
};
// Native definition for COM marshalling of Spine.Unity.MeshGenerator/Settings
struct Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D_marshaled_com
{
	int32_t ___useClipping_0;
	float ___zSpacing_1;
	int32_t ___pmaVertexColors_2;
	int32_t ___tintBlack_3;
	int32_t ___calculateTangents_4;
	int32_t ___addNormals_5;
	int32_t ___immutableTriangles_6;
};
#endif // SETTINGS_TDAD9B4E9DC683D5ACC2903EE333276598006710D_H
#ifndef MESHGENERATORBUFFERS_TF05F06E835D0C4A74373E59C5DE0753892CFBD58_H
#define MESHGENERATORBUFFERS_TF05F06E835D0C4A74373E59C5DE0753892CFBD58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGeneratorBuffers
struct  MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58 
{
public:
	// System.Int32 Spine.Unity.MeshGeneratorBuffers::vertexCount
	int32_t ___vertexCount_0;
	// UnityEngine.Vector3[] Spine.Unity.MeshGeneratorBuffers::vertexBuffer
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___vertexBuffer_1;
	// UnityEngine.Vector2[] Spine.Unity.MeshGeneratorBuffers::uvBuffer
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___uvBuffer_2;
	// UnityEngine.Color32[] Spine.Unity.MeshGeneratorBuffers::colorBuffer
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___colorBuffer_3;
	// Spine.Unity.MeshGenerator Spine.Unity.MeshGeneratorBuffers::meshGenerator
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * ___meshGenerator_4;

public:
	inline static int32_t get_offset_of_vertexCount_0() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58, ___vertexCount_0)); }
	inline int32_t get_vertexCount_0() const { return ___vertexCount_0; }
	inline int32_t* get_address_of_vertexCount_0() { return &___vertexCount_0; }
	inline void set_vertexCount_0(int32_t value)
	{
		___vertexCount_0 = value;
	}

	inline static int32_t get_offset_of_vertexBuffer_1() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58, ___vertexBuffer_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_vertexBuffer_1() const { return ___vertexBuffer_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_vertexBuffer_1() { return &___vertexBuffer_1; }
	inline void set_vertexBuffer_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___vertexBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertexBuffer_1), value);
	}

	inline static int32_t get_offset_of_uvBuffer_2() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58, ___uvBuffer_2)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_uvBuffer_2() const { return ___uvBuffer_2; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_uvBuffer_2() { return &___uvBuffer_2; }
	inline void set_uvBuffer_2(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___uvBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___uvBuffer_2), value);
	}

	inline static int32_t get_offset_of_colorBuffer_3() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58, ___colorBuffer_3)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_colorBuffer_3() const { return ___colorBuffer_3; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_colorBuffer_3() { return &___colorBuffer_3; }
	inline void set_colorBuffer_3(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___colorBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___colorBuffer_3), value);
	}

	inline static int32_t get_offset_of_meshGenerator_4() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58, ___meshGenerator_4)); }
	inline MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * get_meshGenerator_4() const { return ___meshGenerator_4; }
	inline MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 ** get_address_of_meshGenerator_4() { return &___meshGenerator_4; }
	inline void set_meshGenerator_4(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * value)
	{
		___meshGenerator_4 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.MeshGeneratorBuffers
struct MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58_marshaled_pinvoke
{
	int32_t ___vertexCount_0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___vertexBuffer_1;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * ___uvBuffer_2;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * ___colorBuffer_3;
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * ___meshGenerator_4;
};
// Native definition for COM marshalling of Spine.Unity.MeshGeneratorBuffers
struct MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58_marshaled_com
{
	int32_t ___vertexCount_0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___vertexBuffer_1;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * ___uvBuffer_2;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * ___colorBuffer_3;
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * ___meshGenerator_4;
};
#endif // MESHGENERATORBUFFERS_TF05F06E835D0C4A74373E59C5DE0753892CFBD58_H
#ifndef SPINEANIMATIONSTATEBEHAVIOUR_TDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57_H
#define SPINEANIMATIONSTATEBEHAVIOUR_TDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Playables.SpineAnimationStateBehaviour
struct  SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57  : public PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01
{
public:
	// Spine.Unity.AnimationReferenceAsset Spine.Unity.Playables.SpineAnimationStateBehaviour::animationReference
	AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB * ___animationReference_0;
	// System.Boolean Spine.Unity.Playables.SpineAnimationStateBehaviour::loop
	bool ___loop_1;
	// System.Boolean Spine.Unity.Playables.SpineAnimationStateBehaviour::customDuration
	bool ___customDuration_2;
	// System.Single Spine.Unity.Playables.SpineAnimationStateBehaviour::mixDuration
	float ___mixDuration_3;
	// System.Single Spine.Unity.Playables.SpineAnimationStateBehaviour::attachmentThreshold
	float ___attachmentThreshold_4;
	// System.Single Spine.Unity.Playables.SpineAnimationStateBehaviour::eventThreshold
	float ___eventThreshold_5;
	// System.Single Spine.Unity.Playables.SpineAnimationStateBehaviour::drawOrderThreshold
	float ___drawOrderThreshold_6;

public:
	inline static int32_t get_offset_of_animationReference_0() { return static_cast<int32_t>(offsetof(SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57, ___animationReference_0)); }
	inline AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB * get_animationReference_0() const { return ___animationReference_0; }
	inline AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB ** get_address_of_animationReference_0() { return &___animationReference_0; }
	inline void set_animationReference_0(AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB * value)
	{
		___animationReference_0 = value;
		Il2CppCodeGenWriteBarrier((&___animationReference_0), value);
	}

	inline static int32_t get_offset_of_loop_1() { return static_cast<int32_t>(offsetof(SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57, ___loop_1)); }
	inline bool get_loop_1() const { return ___loop_1; }
	inline bool* get_address_of_loop_1() { return &___loop_1; }
	inline void set_loop_1(bool value)
	{
		___loop_1 = value;
	}

	inline static int32_t get_offset_of_customDuration_2() { return static_cast<int32_t>(offsetof(SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57, ___customDuration_2)); }
	inline bool get_customDuration_2() const { return ___customDuration_2; }
	inline bool* get_address_of_customDuration_2() { return &___customDuration_2; }
	inline void set_customDuration_2(bool value)
	{
		___customDuration_2 = value;
	}

	inline static int32_t get_offset_of_mixDuration_3() { return static_cast<int32_t>(offsetof(SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57, ___mixDuration_3)); }
	inline float get_mixDuration_3() const { return ___mixDuration_3; }
	inline float* get_address_of_mixDuration_3() { return &___mixDuration_3; }
	inline void set_mixDuration_3(float value)
	{
		___mixDuration_3 = value;
	}

	inline static int32_t get_offset_of_attachmentThreshold_4() { return static_cast<int32_t>(offsetof(SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57, ___attachmentThreshold_4)); }
	inline float get_attachmentThreshold_4() const { return ___attachmentThreshold_4; }
	inline float* get_address_of_attachmentThreshold_4() { return &___attachmentThreshold_4; }
	inline void set_attachmentThreshold_4(float value)
	{
		___attachmentThreshold_4 = value;
	}

	inline static int32_t get_offset_of_eventThreshold_5() { return static_cast<int32_t>(offsetof(SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57, ___eventThreshold_5)); }
	inline float get_eventThreshold_5() const { return ___eventThreshold_5; }
	inline float* get_address_of_eventThreshold_5() { return &___eventThreshold_5; }
	inline void set_eventThreshold_5(float value)
	{
		___eventThreshold_5 = value;
	}

	inline static int32_t get_offset_of_drawOrderThreshold_6() { return static_cast<int32_t>(offsetof(SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57, ___drawOrderThreshold_6)); }
	inline float get_drawOrderThreshold_6() const { return ___drawOrderThreshold_6; }
	inline float* get_address_of_drawOrderThreshold_6() { return &___drawOrderThreshold_6; }
	inline void set_drawOrderThreshold_6(float value)
	{
		___drawOrderThreshold_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEANIMATIONSTATEBEHAVIOUR_TDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57_H
#ifndef SPINEANIMATIONSTATEMIXERBEHAVIOUR_TF81EF839803A8347EC0558CEBEB489A2DE102E59_H
#define SPINEANIMATIONSTATEMIXERBEHAVIOUR_TF81EF839803A8347EC0558CEBEB489A2DE102E59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Playables.SpineAnimationStateMixerBehaviour
struct  SpineAnimationStateMixerBehaviour_tF81EF839803A8347EC0558CEBEB489A2DE102E59  : public PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01
{
public:
	// System.Single[] Spine.Unity.Playables.SpineAnimationStateMixerBehaviour::lastInputWeights
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___lastInputWeights_0;

public:
	inline static int32_t get_offset_of_lastInputWeights_0() { return static_cast<int32_t>(offsetof(SpineAnimationStateMixerBehaviour_tF81EF839803A8347EC0558CEBEB489A2DE102E59, ___lastInputWeights_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_lastInputWeights_0() const { return ___lastInputWeights_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_lastInputWeights_0() { return &___lastInputWeights_0; }
	inline void set_lastInputWeights_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___lastInputWeights_0 = value;
		Il2CppCodeGenWriteBarrier((&___lastInputWeights_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEANIMATIONSTATEMIXERBEHAVIOUR_TF81EF839803A8347EC0558CEBEB489A2DE102E59_H
#ifndef SPINESKELETONFLIPMIXERBEHAVIOUR_T63713C4E085F70991F4886646AAFD6AB265C986F_H
#define SPINESKELETONFLIPMIXERBEHAVIOUR_T63713C4E085F70991F4886646AAFD6AB265C986F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Playables.SpineSkeletonFlipMixerBehaviour
struct  SpineSkeletonFlipMixerBehaviour_t63713C4E085F70991F4886646AAFD6AB265C986F  : public PlayableBehaviour_t5F4AA32E735199182CC5F57D426D27BE8ABA8F01
{
public:
	// System.Boolean Spine.Unity.Playables.SpineSkeletonFlipMixerBehaviour::defaultFlipX
	bool ___defaultFlipX_0;
	// System.Boolean Spine.Unity.Playables.SpineSkeletonFlipMixerBehaviour::defaultFlipY
	bool ___defaultFlipY_1;
	// Spine.Unity.Playables.SpinePlayableHandleBase Spine.Unity.Playables.SpineSkeletonFlipMixerBehaviour::playableHandle
	SpinePlayableHandleBase_tB5F200A1F15E5F638384F2B4B26C076C4E4810AC * ___playableHandle_2;
	// System.Boolean Spine.Unity.Playables.SpineSkeletonFlipMixerBehaviour::m_FirstFrameHappened
	bool ___m_FirstFrameHappened_3;

public:
	inline static int32_t get_offset_of_defaultFlipX_0() { return static_cast<int32_t>(offsetof(SpineSkeletonFlipMixerBehaviour_t63713C4E085F70991F4886646AAFD6AB265C986F, ___defaultFlipX_0)); }
	inline bool get_defaultFlipX_0() const { return ___defaultFlipX_0; }
	inline bool* get_address_of_defaultFlipX_0() { return &___defaultFlipX_0; }
	inline void set_defaultFlipX_0(bool value)
	{
		___defaultFlipX_0 = value;
	}

	inline static int32_t get_offset_of_defaultFlipY_1() { return static_cast<int32_t>(offsetof(SpineSkeletonFlipMixerBehaviour_t63713C4E085F70991F4886646AAFD6AB265C986F, ___defaultFlipY_1)); }
	inline bool get_defaultFlipY_1() const { return ___defaultFlipY_1; }
	inline bool* get_address_of_defaultFlipY_1() { return &___defaultFlipY_1; }
	inline void set_defaultFlipY_1(bool value)
	{
		___defaultFlipY_1 = value;
	}

	inline static int32_t get_offset_of_playableHandle_2() { return static_cast<int32_t>(offsetof(SpineSkeletonFlipMixerBehaviour_t63713C4E085F70991F4886646AAFD6AB265C986F, ___playableHandle_2)); }
	inline SpinePlayableHandleBase_tB5F200A1F15E5F638384F2B4B26C076C4E4810AC * get_playableHandle_2() const { return ___playableHandle_2; }
	inline SpinePlayableHandleBase_tB5F200A1F15E5F638384F2B4B26C076C4E4810AC ** get_address_of_playableHandle_2() { return &___playableHandle_2; }
	inline void set_playableHandle_2(SpinePlayableHandleBase_tB5F200A1F15E5F638384F2B4B26C076C4E4810AC * value)
	{
		___playableHandle_2 = value;
		Il2CppCodeGenWriteBarrier((&___playableHandle_2), value);
	}

	inline static int32_t get_offset_of_m_FirstFrameHappened_3() { return static_cast<int32_t>(offsetof(SpineSkeletonFlipMixerBehaviour_t63713C4E085F70991F4886646AAFD6AB265C986F, ___m_FirstFrameHappened_3)); }
	inline bool get_m_FirstFrameHappened_3() const { return ___m_FirstFrameHappened_3; }
	inline bool* get_address_of_m_FirstFrameHappened_3() { return &___m_FirstFrameHappened_3; }
	inline void set_m_FirstFrameHappened_3(bool value)
	{
		___m_FirstFrameHappened_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESKELETONFLIPMIXERBEHAVIOUR_T63713C4E085F70991F4886646AAFD6AB265C986F_H
#ifndef HIERARCHY_T983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8_H
#define HIERARCHY_T983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttachment/Hierarchy
struct  Hierarchy_t983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8 
{
public:
	// System.String Spine.Unity.SpineAttachment/Hierarchy::skin
	String_t* ___skin_0;
	// System.String Spine.Unity.SpineAttachment/Hierarchy::slot
	String_t* ___slot_1;
	// System.String Spine.Unity.SpineAttachment/Hierarchy::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_skin_0() { return static_cast<int32_t>(offsetof(Hierarchy_t983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8, ___skin_0)); }
	inline String_t* get_skin_0() const { return ___skin_0; }
	inline String_t** get_address_of_skin_0() { return &___skin_0; }
	inline void set_skin_0(String_t* value)
	{
		___skin_0 = value;
		Il2CppCodeGenWriteBarrier((&___skin_0), value);
	}

	inline static int32_t get_offset_of_slot_1() { return static_cast<int32_t>(offsetof(Hierarchy_t983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8, ___slot_1)); }
	inline String_t* get_slot_1() const { return ___slot_1; }
	inline String_t** get_address_of_slot_1() { return &___slot_1; }
	inline void set_slot_1(String_t* value)
	{
		___slot_1 = value;
		Il2CppCodeGenWriteBarrier((&___slot_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Hierarchy_t983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.SpineAttachment/Hierarchy
struct Hierarchy_t983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8_marshaled_pinvoke
{
	char* ___skin_0;
	char* ___slot_1;
	char* ___name_2;
};
// Native definition for COM marshalling of Spine.Unity.SpineAttachment/Hierarchy
struct Hierarchy_t983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8_marshaled_com
{
	Il2CppChar* ___skin_0;
	Il2CppChar* ___slot_1;
	Il2CppChar* ___name_2;
};
#endif // HIERARCHY_T983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8_H
#ifndef SUBMESHINSTRUCTION_TCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE_H
#define SUBMESHINSTRUCTION_TCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SubmeshInstruction
struct  SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE 
{
public:
	// Spine.Skeleton Spine.Unity.SubmeshInstruction::skeleton
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * ___skeleton_0;
	// System.Int32 Spine.Unity.SubmeshInstruction::startSlot
	int32_t ___startSlot_1;
	// System.Int32 Spine.Unity.SubmeshInstruction::endSlot
	int32_t ___endSlot_2;
	// UnityEngine.Material Spine.Unity.SubmeshInstruction::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	// System.Boolean Spine.Unity.SubmeshInstruction::forceSeparate
	bool ___forceSeparate_4;
	// System.Int32 Spine.Unity.SubmeshInstruction::preActiveClippingSlotSource
	int32_t ___preActiveClippingSlotSource_5;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawTriangleCount
	int32_t ___rawTriangleCount_6;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawVertexCount
	int32_t ___rawVertexCount_7;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawFirstVertexIndex
	int32_t ___rawFirstVertexIndex_8;
	// System.Boolean Spine.Unity.SubmeshInstruction::hasClipping
	bool ___hasClipping_9;

public:
	inline static int32_t get_offset_of_skeleton_0() { return static_cast<int32_t>(offsetof(SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE, ___skeleton_0)); }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * get_skeleton_0() const { return ___skeleton_0; }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 ** get_address_of_skeleton_0() { return &___skeleton_0; }
	inline void set_skeleton_0(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * value)
	{
		___skeleton_0 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_0), value);
	}

	inline static int32_t get_offset_of_startSlot_1() { return static_cast<int32_t>(offsetof(SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE, ___startSlot_1)); }
	inline int32_t get_startSlot_1() const { return ___startSlot_1; }
	inline int32_t* get_address_of_startSlot_1() { return &___startSlot_1; }
	inline void set_startSlot_1(int32_t value)
	{
		___startSlot_1 = value;
	}

	inline static int32_t get_offset_of_endSlot_2() { return static_cast<int32_t>(offsetof(SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE, ___endSlot_2)); }
	inline int32_t get_endSlot_2() const { return ___endSlot_2; }
	inline int32_t* get_address_of_endSlot_2() { return &___endSlot_2; }
	inline void set_endSlot_2(int32_t value)
	{
		___endSlot_2 = value;
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE, ___material_3)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_3() const { return ___material_3; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_forceSeparate_4() { return static_cast<int32_t>(offsetof(SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE, ___forceSeparate_4)); }
	inline bool get_forceSeparate_4() const { return ___forceSeparate_4; }
	inline bool* get_address_of_forceSeparate_4() { return &___forceSeparate_4; }
	inline void set_forceSeparate_4(bool value)
	{
		___forceSeparate_4 = value;
	}

	inline static int32_t get_offset_of_preActiveClippingSlotSource_5() { return static_cast<int32_t>(offsetof(SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE, ___preActiveClippingSlotSource_5)); }
	inline int32_t get_preActiveClippingSlotSource_5() const { return ___preActiveClippingSlotSource_5; }
	inline int32_t* get_address_of_preActiveClippingSlotSource_5() { return &___preActiveClippingSlotSource_5; }
	inline void set_preActiveClippingSlotSource_5(int32_t value)
	{
		___preActiveClippingSlotSource_5 = value;
	}

	inline static int32_t get_offset_of_rawTriangleCount_6() { return static_cast<int32_t>(offsetof(SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE, ___rawTriangleCount_6)); }
	inline int32_t get_rawTriangleCount_6() const { return ___rawTriangleCount_6; }
	inline int32_t* get_address_of_rawTriangleCount_6() { return &___rawTriangleCount_6; }
	inline void set_rawTriangleCount_6(int32_t value)
	{
		___rawTriangleCount_6 = value;
	}

	inline static int32_t get_offset_of_rawVertexCount_7() { return static_cast<int32_t>(offsetof(SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE, ___rawVertexCount_7)); }
	inline int32_t get_rawVertexCount_7() const { return ___rawVertexCount_7; }
	inline int32_t* get_address_of_rawVertexCount_7() { return &___rawVertexCount_7; }
	inline void set_rawVertexCount_7(int32_t value)
	{
		___rawVertexCount_7 = value;
	}

	inline static int32_t get_offset_of_rawFirstVertexIndex_8() { return static_cast<int32_t>(offsetof(SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE, ___rawFirstVertexIndex_8)); }
	inline int32_t get_rawFirstVertexIndex_8() const { return ___rawFirstVertexIndex_8; }
	inline int32_t* get_address_of_rawFirstVertexIndex_8() { return &___rawFirstVertexIndex_8; }
	inline void set_rawFirstVertexIndex_8(int32_t value)
	{
		___rawFirstVertexIndex_8 = value;
	}

	inline static int32_t get_offset_of_hasClipping_9() { return static_cast<int32_t>(offsetof(SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE, ___hasClipping_9)); }
	inline bool get_hasClipping_9() const { return ___hasClipping_9; }
	inline bool* get_address_of_hasClipping_9() { return &___hasClipping_9; }
	inline void set_hasClipping_9(bool value)
	{
		___hasClipping_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.SubmeshInstruction
struct SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE_marshaled_pinvoke
{
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * ___skeleton_0;
	int32_t ___startSlot_1;
	int32_t ___endSlot_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___forceSeparate_4;
	int32_t ___preActiveClippingSlotSource_5;
	int32_t ___rawTriangleCount_6;
	int32_t ___rawVertexCount_7;
	int32_t ___rawFirstVertexIndex_8;
	int32_t ___hasClipping_9;
};
// Native definition for COM marshalling of Spine.Unity.SubmeshInstruction
struct SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE_marshaled_com
{
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * ___skeleton_0;
	int32_t ___startSlot_1;
	int32_t ___endSlot_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___forceSeparate_4;
	int32_t ___preActiveClippingSlotSource_5;
	int32_t ___rawTriangleCount_6;
	int32_t ___rawVertexCount_7;
	int32_t ___rawFirstVertexIndex_8;
	int32_t ___hasClipping_9;
};
#endif // SUBMESHINSTRUCTION_TCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef ANIMATORSTATEINFO_TF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2_H
#define ANIMATORSTATEINFO_TF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTATEINFO_TF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#define PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifndef DISCRETETIME_T046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC_H
#define DISCRETETIME_T046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.DiscreteTime
struct  DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC 
{
public:
	// System.Int64 UnityEngine.Timeline.DiscreteTime::m_DiscreteTime
	int64_t ___m_DiscreteTime_2;

public:
	inline static int32_t get_offset_of_m_DiscreteTime_2() { return static_cast<int32_t>(offsetof(DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC, ___m_DiscreteTime_2)); }
	inline int64_t get_m_DiscreteTime_2() const { return ___m_DiscreteTime_2; }
	inline int64_t* get_address_of_m_DiscreteTime_2() { return &___m_DiscreteTime_2; }
	inline void set_m_DiscreteTime_2(int64_t value)
	{
		___m_DiscreteTime_2 = value;
	}
};

struct DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC_StaticFields
{
public:
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.DiscreteTime::kMaxTime
	DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  ___kMaxTime_1;

public:
	inline static int32_t get_offset_of_kMaxTime_1() { return static_cast<int32_t>(offsetof(DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC_StaticFields, ___kMaxTime_1)); }
	inline DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  get_kMaxTime_1() const { return ___kMaxTime_1; }
	inline DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC * get_address_of_kMaxTime_1() { return &___kMaxTime_1; }
	inline void set_kMaxTime_1(DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  value)
	{
		___kMaxTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCRETETIME_T046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef BLENDMODE_T36C159CFDBBD6C33AFD2B2392A2EA71889DDD0E1_H
#define BLENDMODE_T36C159CFDBBD6C33AFD2B2392A2EA71889DDD0E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BlendMode
struct  BlendMode_t36C159CFDBBD6C33AFD2B2392A2EA71889DDD0E1 
{
public:
	// System.Int32 Spine.BlendMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlendMode_t36C159CFDBBD6C33AFD2B2392A2EA71889DDD0E1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDMODE_T36C159CFDBBD6C33AFD2B2392A2EA71889DDD0E1_H
#ifndef POSITIONMODE_TD2BDC3F988FFDCB312D7B450C21B396BCE2D0570_H
#define POSITIONMODE_TD2BDC3F988FFDCB312D7B450C21B396BCE2D0570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PositionMode
struct  PositionMode_tD2BDC3F988FFDCB312D7B450C21B396BCE2D0570 
{
public:
	// System.Int32 Spine.PositionMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PositionMode_tD2BDC3F988FFDCB312D7B450C21B396BCE2D0570, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONMODE_TD2BDC3F988FFDCB312D7B450C21B396BCE2D0570_H
#ifndef ROTATEMODE_T9D7DDC5DCACDAFA1AF43170AF895DDF3AEE0C79B_H
#define ROTATEMODE_T9D7DDC5DCACDAFA1AF43170AF895DDF3AEE0C79B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.RotateMode
struct  RotateMode_t9D7DDC5DCACDAFA1AF43170AF895DDF3AEE0C79B 
{
public:
	// System.Int32 Spine.RotateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotateMode_t9D7DDC5DCACDAFA1AF43170AF895DDF3AEE0C79B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T9D7DDC5DCACDAFA1AF43170AF895DDF3AEE0C79B_H
#ifndef SPACINGMODE_T1BDCF0168CC650A8579A9348522FB739B433E66A_H
#define SPACINGMODE_T1BDCF0168CC650A8579A9348522FB739B433E66A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SpacingMode
struct  SpacingMode_t1BDCF0168CC650A8579A9348522FB739B433E66A 
{
public:
	// System.Int32 Spine.SpacingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpacingMode_t1BDCF0168CC650A8579A9348522FB739B433E66A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACINGMODE_T1BDCF0168CC650A8579A9348522FB739B433E66A_H
#ifndef MESHGENERATOR_T9641A8F39530EFA0918ABD2F91E8BFC247FD4157_H
#define MESHGENERATOR_T9641A8F39530EFA0918ABD2F91E8BFC247FD4157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGenerator
struct  MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157  : public RuntimeObject
{
public:
	// Spine.Unity.MeshGenerator/Settings Spine.Unity.MeshGenerator::settings
	Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D  ___settings_0;
	// Spine.ExposedList`1<UnityEngine.Vector3> Spine.Unity.MeshGenerator::vertexBuffer
	ExposedList_1_tC4D3977D6C3902E2345F2E4F5C1EB9A7D4CC8CCF * ___vertexBuffer_3;
	// Spine.ExposedList`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::uvBuffer
	ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C * ___uvBuffer_4;
	// Spine.ExposedList`1<UnityEngine.Color32> Spine.Unity.MeshGenerator::colorBuffer
	ExposedList_1_tE4914E468AA30F682A54A8A684D2EE51E644F58A * ___colorBuffer_5;
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Int32>> Spine.Unity.MeshGenerator::submeshes
	ExposedList_1_tA2B5C15C080DBB9671A5521E5A131BF5D0611DC3 * ___submeshes_6;
	// UnityEngine.Vector2 Spine.Unity.MeshGenerator::meshBoundsMin
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___meshBoundsMin_7;
	// UnityEngine.Vector2 Spine.Unity.MeshGenerator::meshBoundsMax
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___meshBoundsMax_8;
	// System.Single Spine.Unity.MeshGenerator::meshBoundsThickness
	float ___meshBoundsThickness_9;
	// System.Int32 Spine.Unity.MeshGenerator::submeshIndex
	int32_t ___submeshIndex_10;
	// Spine.SkeletonClipping Spine.Unity.MeshGenerator::clipper
	SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5 * ___clipper_11;
	// System.Single[] Spine.Unity.MeshGenerator::tempVerts
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___tempVerts_12;
	// System.Int32[] Spine.Unity.MeshGenerator::regionTriangles
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___regionTriangles_13;
	// UnityEngine.Vector3[] Spine.Unity.MeshGenerator::normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___normals_14;
	// UnityEngine.Vector4[] Spine.Unity.MeshGenerator::tangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___tangents_15;
	// UnityEngine.Vector2[] Spine.Unity.MeshGenerator::tempTanBuffer
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___tempTanBuffer_16;
	// Spine.ExposedList`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::uv2
	ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C * ___uv2_17;
	// Spine.ExposedList`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::uv3
	ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C * ___uv3_18;

public:
	inline static int32_t get_offset_of_settings_0() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___settings_0)); }
	inline Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D  get_settings_0() const { return ___settings_0; }
	inline Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D * get_address_of_settings_0() { return &___settings_0; }
	inline void set_settings_0(Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D  value)
	{
		___settings_0 = value;
	}

	inline static int32_t get_offset_of_vertexBuffer_3() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___vertexBuffer_3)); }
	inline ExposedList_1_tC4D3977D6C3902E2345F2E4F5C1EB9A7D4CC8CCF * get_vertexBuffer_3() const { return ___vertexBuffer_3; }
	inline ExposedList_1_tC4D3977D6C3902E2345F2E4F5C1EB9A7D4CC8CCF ** get_address_of_vertexBuffer_3() { return &___vertexBuffer_3; }
	inline void set_vertexBuffer_3(ExposedList_1_tC4D3977D6C3902E2345F2E4F5C1EB9A7D4CC8CCF * value)
	{
		___vertexBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___vertexBuffer_3), value);
	}

	inline static int32_t get_offset_of_uvBuffer_4() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___uvBuffer_4)); }
	inline ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C * get_uvBuffer_4() const { return ___uvBuffer_4; }
	inline ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C ** get_address_of_uvBuffer_4() { return &___uvBuffer_4; }
	inline void set_uvBuffer_4(ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C * value)
	{
		___uvBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___uvBuffer_4), value);
	}

	inline static int32_t get_offset_of_colorBuffer_5() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___colorBuffer_5)); }
	inline ExposedList_1_tE4914E468AA30F682A54A8A684D2EE51E644F58A * get_colorBuffer_5() const { return ___colorBuffer_5; }
	inline ExposedList_1_tE4914E468AA30F682A54A8A684D2EE51E644F58A ** get_address_of_colorBuffer_5() { return &___colorBuffer_5; }
	inline void set_colorBuffer_5(ExposedList_1_tE4914E468AA30F682A54A8A684D2EE51E644F58A * value)
	{
		___colorBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___colorBuffer_5), value);
	}

	inline static int32_t get_offset_of_submeshes_6() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___submeshes_6)); }
	inline ExposedList_1_tA2B5C15C080DBB9671A5521E5A131BF5D0611DC3 * get_submeshes_6() const { return ___submeshes_6; }
	inline ExposedList_1_tA2B5C15C080DBB9671A5521E5A131BF5D0611DC3 ** get_address_of_submeshes_6() { return &___submeshes_6; }
	inline void set_submeshes_6(ExposedList_1_tA2B5C15C080DBB9671A5521E5A131BF5D0611DC3 * value)
	{
		___submeshes_6 = value;
		Il2CppCodeGenWriteBarrier((&___submeshes_6), value);
	}

	inline static int32_t get_offset_of_meshBoundsMin_7() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___meshBoundsMin_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_meshBoundsMin_7() const { return ___meshBoundsMin_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_meshBoundsMin_7() { return &___meshBoundsMin_7; }
	inline void set_meshBoundsMin_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___meshBoundsMin_7 = value;
	}

	inline static int32_t get_offset_of_meshBoundsMax_8() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___meshBoundsMax_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_meshBoundsMax_8() const { return ___meshBoundsMax_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_meshBoundsMax_8() { return &___meshBoundsMax_8; }
	inline void set_meshBoundsMax_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___meshBoundsMax_8 = value;
	}

	inline static int32_t get_offset_of_meshBoundsThickness_9() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___meshBoundsThickness_9)); }
	inline float get_meshBoundsThickness_9() const { return ___meshBoundsThickness_9; }
	inline float* get_address_of_meshBoundsThickness_9() { return &___meshBoundsThickness_9; }
	inline void set_meshBoundsThickness_9(float value)
	{
		___meshBoundsThickness_9 = value;
	}

	inline static int32_t get_offset_of_submeshIndex_10() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___submeshIndex_10)); }
	inline int32_t get_submeshIndex_10() const { return ___submeshIndex_10; }
	inline int32_t* get_address_of_submeshIndex_10() { return &___submeshIndex_10; }
	inline void set_submeshIndex_10(int32_t value)
	{
		___submeshIndex_10 = value;
	}

	inline static int32_t get_offset_of_clipper_11() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___clipper_11)); }
	inline SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5 * get_clipper_11() const { return ___clipper_11; }
	inline SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5 ** get_address_of_clipper_11() { return &___clipper_11; }
	inline void set_clipper_11(SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5 * value)
	{
		___clipper_11 = value;
		Il2CppCodeGenWriteBarrier((&___clipper_11), value);
	}

	inline static int32_t get_offset_of_tempVerts_12() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___tempVerts_12)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_tempVerts_12() const { return ___tempVerts_12; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_tempVerts_12() { return &___tempVerts_12; }
	inline void set_tempVerts_12(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___tempVerts_12 = value;
		Il2CppCodeGenWriteBarrier((&___tempVerts_12), value);
	}

	inline static int32_t get_offset_of_regionTriangles_13() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___regionTriangles_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_regionTriangles_13() const { return ___regionTriangles_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_regionTriangles_13() { return &___regionTriangles_13; }
	inline void set_regionTriangles_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___regionTriangles_13 = value;
		Il2CppCodeGenWriteBarrier((&___regionTriangles_13), value);
	}

	inline static int32_t get_offset_of_normals_14() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___normals_14)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_normals_14() const { return ___normals_14; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_normals_14() { return &___normals_14; }
	inline void set_normals_14(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___normals_14 = value;
		Il2CppCodeGenWriteBarrier((&___normals_14), value);
	}

	inline static int32_t get_offset_of_tangents_15() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___tangents_15)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_tangents_15() const { return ___tangents_15; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_tangents_15() { return &___tangents_15; }
	inline void set_tangents_15(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___tangents_15 = value;
		Il2CppCodeGenWriteBarrier((&___tangents_15), value);
	}

	inline static int32_t get_offset_of_tempTanBuffer_16() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___tempTanBuffer_16)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_tempTanBuffer_16() const { return ___tempTanBuffer_16; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_tempTanBuffer_16() { return &___tempTanBuffer_16; }
	inline void set_tempTanBuffer_16(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___tempTanBuffer_16 = value;
		Il2CppCodeGenWriteBarrier((&___tempTanBuffer_16), value);
	}

	inline static int32_t get_offset_of_uv2_17() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___uv2_17)); }
	inline ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C * get_uv2_17() const { return ___uv2_17; }
	inline ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C ** get_address_of_uv2_17() { return &___uv2_17; }
	inline void set_uv2_17(ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C * value)
	{
		___uv2_17 = value;
		Il2CppCodeGenWriteBarrier((&___uv2_17), value);
	}

	inline static int32_t get_offset_of_uv3_18() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157, ___uv3_18)); }
	inline ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C * get_uv3_18() const { return ___uv3_18; }
	inline ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C ** get_address_of_uv3_18() { return &___uv3_18; }
	inline void set_uv3_18(ExposedList_1_t5D974D57FD08557F5B190D2CA556AF28559D545C * value)
	{
		___uv3_18 = value;
		Il2CppCodeGenWriteBarrier((&___uv3_18), value);
	}
};

struct MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Spine.Unity.MeshGenerator::AttachmentVerts
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___AttachmentVerts_19;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::AttachmentUVs
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___AttachmentUVs_20;
	// System.Collections.Generic.List`1<UnityEngine.Color32> Spine.Unity.MeshGenerator::AttachmentColors32
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___AttachmentColors32_21;
	// System.Collections.Generic.List`1<System.Int32> Spine.Unity.MeshGenerator::AttachmentIndices
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___AttachmentIndices_22;

public:
	inline static int32_t get_offset_of_AttachmentVerts_19() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157_StaticFields, ___AttachmentVerts_19)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_AttachmentVerts_19() const { return ___AttachmentVerts_19; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_AttachmentVerts_19() { return &___AttachmentVerts_19; }
	inline void set_AttachmentVerts_19(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___AttachmentVerts_19 = value;
		Il2CppCodeGenWriteBarrier((&___AttachmentVerts_19), value);
	}

	inline static int32_t get_offset_of_AttachmentUVs_20() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157_StaticFields, ___AttachmentUVs_20)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_AttachmentUVs_20() const { return ___AttachmentUVs_20; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_AttachmentUVs_20() { return &___AttachmentUVs_20; }
	inline void set_AttachmentUVs_20(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___AttachmentUVs_20 = value;
		Il2CppCodeGenWriteBarrier((&___AttachmentUVs_20), value);
	}

	inline static int32_t get_offset_of_AttachmentColors32_21() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157_StaticFields, ___AttachmentColors32_21)); }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * get_AttachmentColors32_21() const { return ___AttachmentColors32_21; }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 ** get_address_of_AttachmentColors32_21() { return &___AttachmentColors32_21; }
	inline void set_AttachmentColors32_21(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * value)
	{
		___AttachmentColors32_21 = value;
		Il2CppCodeGenWriteBarrier((&___AttachmentColors32_21), value);
	}

	inline static int32_t get_offset_of_AttachmentIndices_22() { return static_cast<int32_t>(offsetof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157_StaticFields, ___AttachmentIndices_22)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_AttachmentIndices_22() const { return ___AttachmentIndices_22; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_AttachmentIndices_22() { return &___AttachmentIndices_22; }
	inline void set_AttachmentIndices_22(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___AttachmentIndices_22 = value;
		Il2CppCodeGenWriteBarrier((&___AttachmentIndices_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHGENERATOR_T9641A8F39530EFA0918ABD2F91E8BFC247FD4157_H
#ifndef CLIPINFOS_T192425775CEDCFA54B70EE34A91B4765F20F037D_H
#define CLIPINFOS_T192425775CEDCFA54B70EE34A91B4765F20F037D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos
struct  ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos::isInterruptionActive
	bool ___isInterruptionActive_0;
	// System.Boolean Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos::isLastFrameOfInterruption
	bool ___isLastFrameOfInterruption_1;
	// System.Int32 Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos::clipInfoCount
	int32_t ___clipInfoCount_2;
	// System.Int32 Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos::nextClipInfoCount
	int32_t ___nextClipInfoCount_3;
	// System.Int32 Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos::interruptingClipInfoCount
	int32_t ___interruptingClipInfoCount_4;
	// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo> Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos::clipInfos
	List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B * ___clipInfos_5;
	// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo> Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos::nextClipInfos
	List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B * ___nextClipInfos_6;
	// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo> Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos::interruptingClipInfos
	List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B * ___interruptingClipInfos_7;
	// UnityEngine.AnimatorStateInfo Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos::stateInfo
	AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  ___stateInfo_8;
	// UnityEngine.AnimatorStateInfo Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos::nextStateInfo
	AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  ___nextStateInfo_9;
	// UnityEngine.AnimatorStateInfo Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos::interruptingStateInfo
	AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  ___interruptingStateInfo_10;
	// System.Single Spine.Unity.SkeletonAnimator/MecanimTranslator/ClipInfos::interruptingClipTimeAddition
	float ___interruptingClipTimeAddition_11;

public:
	inline static int32_t get_offset_of_isInterruptionActive_0() { return static_cast<int32_t>(offsetof(ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D, ___isInterruptionActive_0)); }
	inline bool get_isInterruptionActive_0() const { return ___isInterruptionActive_0; }
	inline bool* get_address_of_isInterruptionActive_0() { return &___isInterruptionActive_0; }
	inline void set_isInterruptionActive_0(bool value)
	{
		___isInterruptionActive_0 = value;
	}

	inline static int32_t get_offset_of_isLastFrameOfInterruption_1() { return static_cast<int32_t>(offsetof(ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D, ___isLastFrameOfInterruption_1)); }
	inline bool get_isLastFrameOfInterruption_1() const { return ___isLastFrameOfInterruption_1; }
	inline bool* get_address_of_isLastFrameOfInterruption_1() { return &___isLastFrameOfInterruption_1; }
	inline void set_isLastFrameOfInterruption_1(bool value)
	{
		___isLastFrameOfInterruption_1 = value;
	}

	inline static int32_t get_offset_of_clipInfoCount_2() { return static_cast<int32_t>(offsetof(ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D, ___clipInfoCount_2)); }
	inline int32_t get_clipInfoCount_2() const { return ___clipInfoCount_2; }
	inline int32_t* get_address_of_clipInfoCount_2() { return &___clipInfoCount_2; }
	inline void set_clipInfoCount_2(int32_t value)
	{
		___clipInfoCount_2 = value;
	}

	inline static int32_t get_offset_of_nextClipInfoCount_3() { return static_cast<int32_t>(offsetof(ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D, ___nextClipInfoCount_3)); }
	inline int32_t get_nextClipInfoCount_3() const { return ___nextClipInfoCount_3; }
	inline int32_t* get_address_of_nextClipInfoCount_3() { return &___nextClipInfoCount_3; }
	inline void set_nextClipInfoCount_3(int32_t value)
	{
		___nextClipInfoCount_3 = value;
	}

	inline static int32_t get_offset_of_interruptingClipInfoCount_4() { return static_cast<int32_t>(offsetof(ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D, ___interruptingClipInfoCount_4)); }
	inline int32_t get_interruptingClipInfoCount_4() const { return ___interruptingClipInfoCount_4; }
	inline int32_t* get_address_of_interruptingClipInfoCount_4() { return &___interruptingClipInfoCount_4; }
	inline void set_interruptingClipInfoCount_4(int32_t value)
	{
		___interruptingClipInfoCount_4 = value;
	}

	inline static int32_t get_offset_of_clipInfos_5() { return static_cast<int32_t>(offsetof(ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D, ___clipInfos_5)); }
	inline List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B * get_clipInfos_5() const { return ___clipInfos_5; }
	inline List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B ** get_address_of_clipInfos_5() { return &___clipInfos_5; }
	inline void set_clipInfos_5(List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B * value)
	{
		___clipInfos_5 = value;
		Il2CppCodeGenWriteBarrier((&___clipInfos_5), value);
	}

	inline static int32_t get_offset_of_nextClipInfos_6() { return static_cast<int32_t>(offsetof(ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D, ___nextClipInfos_6)); }
	inline List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B * get_nextClipInfos_6() const { return ___nextClipInfos_6; }
	inline List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B ** get_address_of_nextClipInfos_6() { return &___nextClipInfos_6; }
	inline void set_nextClipInfos_6(List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B * value)
	{
		___nextClipInfos_6 = value;
		Il2CppCodeGenWriteBarrier((&___nextClipInfos_6), value);
	}

	inline static int32_t get_offset_of_interruptingClipInfos_7() { return static_cast<int32_t>(offsetof(ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D, ___interruptingClipInfos_7)); }
	inline List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B * get_interruptingClipInfos_7() const { return ___interruptingClipInfos_7; }
	inline List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B ** get_address_of_interruptingClipInfos_7() { return &___interruptingClipInfos_7; }
	inline void set_interruptingClipInfos_7(List_1_t34BD75698B68DE47D684862A345F6F9797CE3F9B * value)
	{
		___interruptingClipInfos_7 = value;
		Il2CppCodeGenWriteBarrier((&___interruptingClipInfos_7), value);
	}

	inline static int32_t get_offset_of_stateInfo_8() { return static_cast<int32_t>(offsetof(ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D, ___stateInfo_8)); }
	inline AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  get_stateInfo_8() const { return ___stateInfo_8; }
	inline AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 * get_address_of_stateInfo_8() { return &___stateInfo_8; }
	inline void set_stateInfo_8(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  value)
	{
		___stateInfo_8 = value;
	}

	inline static int32_t get_offset_of_nextStateInfo_9() { return static_cast<int32_t>(offsetof(ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D, ___nextStateInfo_9)); }
	inline AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  get_nextStateInfo_9() const { return ___nextStateInfo_9; }
	inline AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 * get_address_of_nextStateInfo_9() { return &___nextStateInfo_9; }
	inline void set_nextStateInfo_9(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  value)
	{
		___nextStateInfo_9 = value;
	}

	inline static int32_t get_offset_of_interruptingStateInfo_10() { return static_cast<int32_t>(offsetof(ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D, ___interruptingStateInfo_10)); }
	inline AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  get_interruptingStateInfo_10() const { return ___interruptingStateInfo_10; }
	inline AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2 * get_address_of_interruptingStateInfo_10() { return &___interruptingStateInfo_10; }
	inline void set_interruptingStateInfo_10(AnimatorStateInfo_tF6D8ADF771CD13DC578AC9A574FD33CC99AD46E2  value)
	{
		___interruptingStateInfo_10 = value;
	}

	inline static int32_t get_offset_of_interruptingClipTimeAddition_11() { return static_cast<int32_t>(offsetof(ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D, ___interruptingClipTimeAddition_11)); }
	inline float get_interruptingClipTimeAddition_11() const { return ___interruptingClipTimeAddition_11; }
	inline float* get_address_of_interruptingClipTimeAddition_11() { return &___interruptingClipTimeAddition_11; }
	inline void set_interruptingClipTimeAddition_11(float value)
	{
		___interruptingClipTimeAddition_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPINFOS_T192425775CEDCFA54B70EE34A91B4765F20F037D_H
#ifndef MIXMODE_TD6C282143E8C24B95BABD1A52AA415F5E550E20D_H
#define MIXMODE_TD6C282143E8C24B95BABD1A52AA415F5E550E20D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode
struct  MixMode_tD6C282143E8C24B95BABD1A52AA415F5E550E20D 
{
public:
	// System.Int32 Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MixMode_tD6C282143E8C24B95BABD1A52AA415F5E550E20D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXMODE_TD6C282143E8C24B95BABD1A52AA415F5E550E20D_H
#ifndef MODE_T72E168DBEE144A1374AA8A46C48943B70BE23CAE_H
#define MODE_T72E168DBEE144A1374AA8A46C48943B70BE23CAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityBone/Mode
struct  Mode_t72E168DBEE144A1374AA8A46C48943B70BE23CAE 
{
public:
	// System.Int32 Spine.Unity.SkeletonUtilityBone/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t72E168DBEE144A1374AA8A46C48943B70BE23CAE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T72E168DBEE144A1374AA8A46C48943B70BE23CAE_H
#ifndef UPDATEPHASE_T08F95CD9252DCA7FE22968C8933654BF534C4178_H
#define UPDATEPHASE_T08F95CD9252DCA7FE22968C8933654BF534C4178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityBone/UpdatePhase
struct  UpdatePhase_t08F95CD9252DCA7FE22968C8933654BF534C4178 
{
public:
	// System.Int32 Spine.Unity.SkeletonUtilityBone/UpdatePhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdatePhase_t08F95CD9252DCA7FE22968C8933654BF534C4178, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEPHASE_T08F95CD9252DCA7FE22968C8933654BF534C4178_H
#ifndef SPINEATLASREGION_TC958B6954F6EC3B396C047E2177627075103E68C_H
#define SPINEATLASREGION_TC958B6954F6EC3B396C047E2177627075103E68C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAtlasRegion
struct  SpineAtlasRegion_tC958B6954F6EC3B396C047E2177627075103E68C  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:
	// System.String Spine.Unity.SpineAtlasRegion::atlasAssetField
	String_t* ___atlasAssetField_0;

public:
	inline static int32_t get_offset_of_atlasAssetField_0() { return static_cast<int32_t>(offsetof(SpineAtlasRegion_tC958B6954F6EC3B396C047E2177627075103E68C, ___atlasAssetField_0)); }
	inline String_t* get_atlasAssetField_0() const { return ___atlasAssetField_0; }
	inline String_t** get_address_of_atlasAssetField_0() { return &___atlasAssetField_0; }
	inline void set_atlasAssetField_0(String_t* value)
	{
		___atlasAssetField_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAssetField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATLASREGION_TC958B6954F6EC3B396C047E2177627075103E68C_H
#ifndef SPINEATTRIBUTEBASE_T9B9F73E3A2B3FD57076801623D1610A959DE7764_H
#define SPINEATTRIBUTEBASE_T9B9F73E3A2B3FD57076801623D1610A959DE7764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttributeBase
struct  SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:
	// System.String Spine.Unity.SpineAttributeBase::dataField
	String_t* ___dataField_0;
	// System.String Spine.Unity.SpineAttributeBase::startsWith
	String_t* ___startsWith_1;
	// System.Boolean Spine.Unity.SpineAttributeBase::includeNone
	bool ___includeNone_2;
	// System.Boolean Spine.Unity.SpineAttributeBase::fallbackToTextField
	bool ___fallbackToTextField_3;

public:
	inline static int32_t get_offset_of_dataField_0() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764, ___dataField_0)); }
	inline String_t* get_dataField_0() const { return ___dataField_0; }
	inline String_t** get_address_of_dataField_0() { return &___dataField_0; }
	inline void set_dataField_0(String_t* value)
	{
		___dataField_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataField_0), value);
	}

	inline static int32_t get_offset_of_startsWith_1() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764, ___startsWith_1)); }
	inline String_t* get_startsWith_1() const { return ___startsWith_1; }
	inline String_t** get_address_of_startsWith_1() { return &___startsWith_1; }
	inline void set_startsWith_1(String_t* value)
	{
		___startsWith_1 = value;
		Il2CppCodeGenWriteBarrier((&___startsWith_1), value);
	}

	inline static int32_t get_offset_of_includeNone_2() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764, ___includeNone_2)); }
	inline bool get_includeNone_2() const { return ___includeNone_2; }
	inline bool* get_address_of_includeNone_2() { return &___includeNone_2; }
	inline void set_includeNone_2(bool value)
	{
		___includeNone_2 = value;
	}

	inline static int32_t get_offset_of_fallbackToTextField_3() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764, ___fallbackToTextField_3)); }
	inline bool get_fallbackToTextField_3() const { return ___fallbackToTextField_3; }
	inline bool* get_address_of_fallbackToTextField_3() { return &___fallbackToTextField_3; }
	inline void set_fallbackToTextField_3(bool value)
	{
		___fallbackToTextField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATTRIBUTEBASE_T9B9F73E3A2B3FD57076801623D1610A959DE7764_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#define HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T30B57DC00548E963A569318C8F4A4123E7447E37_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef PATHCONSTRAINTDATA_TC6928338E396BBEFBD013C1C62F935D968992827_H
#define PATHCONSTRAINTDATA_TC6928338E396BBEFBD013C1C62F935D968992827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintData
struct  PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827  : public RuntimeObject
{
public:
	// System.String Spine.PathConstraintData::name
	String_t* ___name_0;
	// System.Int32 Spine.PathConstraintData::order
	int32_t ___order_1;
	// Spine.ExposedList`1<Spine.BoneData> Spine.PathConstraintData::bones
	ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899 * ___bones_2;
	// Spine.SlotData Spine.PathConstraintData::target
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738 * ___target_3;
	// Spine.PositionMode Spine.PathConstraintData::positionMode
	int32_t ___positionMode_4;
	// Spine.SpacingMode Spine.PathConstraintData::spacingMode
	int32_t ___spacingMode_5;
	// Spine.RotateMode Spine.PathConstraintData::rotateMode
	int32_t ___rotateMode_6;
	// System.Single Spine.PathConstraintData::offsetRotation
	float ___offsetRotation_7;
	// System.Single Spine.PathConstraintData::position
	float ___position_8;
	// System.Single Spine.PathConstraintData::spacing
	float ___spacing_9;
	// System.Single Spine.PathConstraintData::rotateMix
	float ___rotateMix_10;
	// System.Single Spine.PathConstraintData::translateMix
	float ___translateMix_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_bones_2() { return static_cast<int32_t>(offsetof(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827, ___bones_2)); }
	inline ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899 * get_bones_2() const { return ___bones_2; }
	inline ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899 ** get_address_of_bones_2() { return &___bones_2; }
	inline void set_bones_2(ExposedList_1_t34D453E1FA928DA90667824F3FDBEC09A1D8D899 * value)
	{
		___bones_2 = value;
		Il2CppCodeGenWriteBarrier((&___bones_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827, ___target_3)); }
	inline SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738 * get_target_3() const { return ___target_3; }
	inline SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_positionMode_4() { return static_cast<int32_t>(offsetof(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827, ___positionMode_4)); }
	inline int32_t get_positionMode_4() const { return ___positionMode_4; }
	inline int32_t* get_address_of_positionMode_4() { return &___positionMode_4; }
	inline void set_positionMode_4(int32_t value)
	{
		___positionMode_4 = value;
	}

	inline static int32_t get_offset_of_spacingMode_5() { return static_cast<int32_t>(offsetof(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827, ___spacingMode_5)); }
	inline int32_t get_spacingMode_5() const { return ___spacingMode_5; }
	inline int32_t* get_address_of_spacingMode_5() { return &___spacingMode_5; }
	inline void set_spacingMode_5(int32_t value)
	{
		___spacingMode_5 = value;
	}

	inline static int32_t get_offset_of_rotateMode_6() { return static_cast<int32_t>(offsetof(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827, ___rotateMode_6)); }
	inline int32_t get_rotateMode_6() const { return ___rotateMode_6; }
	inline int32_t* get_address_of_rotateMode_6() { return &___rotateMode_6; }
	inline void set_rotateMode_6(int32_t value)
	{
		___rotateMode_6 = value;
	}

	inline static int32_t get_offset_of_offsetRotation_7() { return static_cast<int32_t>(offsetof(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827, ___offsetRotation_7)); }
	inline float get_offsetRotation_7() const { return ___offsetRotation_7; }
	inline float* get_address_of_offsetRotation_7() { return &___offsetRotation_7; }
	inline void set_offsetRotation_7(float value)
	{
		___offsetRotation_7 = value;
	}

	inline static int32_t get_offset_of_position_8() { return static_cast<int32_t>(offsetof(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827, ___position_8)); }
	inline float get_position_8() const { return ___position_8; }
	inline float* get_address_of_position_8() { return &___position_8; }
	inline void set_position_8(float value)
	{
		___position_8 = value;
	}

	inline static int32_t get_offset_of_spacing_9() { return static_cast<int32_t>(offsetof(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827, ___spacing_9)); }
	inline float get_spacing_9() const { return ___spacing_9; }
	inline float* get_address_of_spacing_9() { return &___spacing_9; }
	inline void set_spacing_9(float value)
	{
		___spacing_9 = value;
	}

	inline static int32_t get_offset_of_rotateMix_10() { return static_cast<int32_t>(offsetof(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827, ___rotateMix_10)); }
	inline float get_rotateMix_10() const { return ___rotateMix_10; }
	inline float* get_address_of_rotateMix_10() { return &___rotateMix_10; }
	inline void set_rotateMix_10(float value)
	{
		___rotateMix_10 = value;
	}

	inline static int32_t get_offset_of_translateMix_11() { return static_cast<int32_t>(offsetof(PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827, ___translateMix_11)); }
	inline float get_translateMix_11() const { return ___translateMix_11; }
	inline float* get_address_of_translateMix_11() { return &___translateMix_11; }
	inline void set_translateMix_11(float value)
	{
		___translateMix_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTDATA_TC6928338E396BBEFBD013C1C62F935D968992827_H
#ifndef SLOTDATA_TDC4C24F4167DCD16DC14E99E265C6ED2F9851738_H
#define SLOTDATA_TDC4C24F4167DCD16DC14E99E265C6ED2F9851738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SlotData
struct  SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738  : public RuntimeObject
{
public:
	// System.Int32 Spine.SlotData::index
	int32_t ___index_0;
	// System.String Spine.SlotData::name
	String_t* ___name_1;
	// Spine.BoneData Spine.SlotData::boneData
	BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * ___boneData_2;
	// System.Single Spine.SlotData::r
	float ___r_3;
	// System.Single Spine.SlotData::g
	float ___g_4;
	// System.Single Spine.SlotData::b
	float ___b_5;
	// System.Single Spine.SlotData::a
	float ___a_6;
	// System.Single Spine.SlotData::r2
	float ___r2_7;
	// System.Single Spine.SlotData::g2
	float ___g2_8;
	// System.Single Spine.SlotData::b2
	float ___b2_9;
	// System.Boolean Spine.SlotData::hasSecondColor
	bool ___hasSecondColor_10;
	// System.String Spine.SlotData::attachmentName
	String_t* ___attachmentName_11;
	// Spine.BlendMode Spine.SlotData::blendMode
	int32_t ___blendMode_12;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_boneData_2() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___boneData_2)); }
	inline BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * get_boneData_2() const { return ___boneData_2; }
	inline BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F ** get_address_of_boneData_2() { return &___boneData_2; }
	inline void set_boneData_2(BoneData_t0CC0FD7D5CA74A9DA87CED52D1EE4D667256D89F * value)
	{
		___boneData_2 = value;
		Il2CppCodeGenWriteBarrier((&___boneData_2), value);
	}

	inline static int32_t get_offset_of_r_3() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___r_3)); }
	inline float get_r_3() const { return ___r_3; }
	inline float* get_address_of_r_3() { return &___r_3; }
	inline void set_r_3(float value)
	{
		___r_3 = value;
	}

	inline static int32_t get_offset_of_g_4() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___g_4)); }
	inline float get_g_4() const { return ___g_4; }
	inline float* get_address_of_g_4() { return &___g_4; }
	inline void set_g_4(float value)
	{
		___g_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___b_5)); }
	inline float get_b_5() const { return ___b_5; }
	inline float* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(float value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_a_6() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___a_6)); }
	inline float get_a_6() const { return ___a_6; }
	inline float* get_address_of_a_6() { return &___a_6; }
	inline void set_a_6(float value)
	{
		___a_6 = value;
	}

	inline static int32_t get_offset_of_r2_7() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___r2_7)); }
	inline float get_r2_7() const { return ___r2_7; }
	inline float* get_address_of_r2_7() { return &___r2_7; }
	inline void set_r2_7(float value)
	{
		___r2_7 = value;
	}

	inline static int32_t get_offset_of_g2_8() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___g2_8)); }
	inline float get_g2_8() const { return ___g2_8; }
	inline float* get_address_of_g2_8() { return &___g2_8; }
	inline void set_g2_8(float value)
	{
		___g2_8 = value;
	}

	inline static int32_t get_offset_of_b2_9() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___b2_9)); }
	inline float get_b2_9() const { return ___b2_9; }
	inline float* get_address_of_b2_9() { return &___b2_9; }
	inline void set_b2_9(float value)
	{
		___b2_9 = value;
	}

	inline static int32_t get_offset_of_hasSecondColor_10() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___hasSecondColor_10)); }
	inline bool get_hasSecondColor_10() const { return ___hasSecondColor_10; }
	inline bool* get_address_of_hasSecondColor_10() { return &___hasSecondColor_10; }
	inline void set_hasSecondColor_10(bool value)
	{
		___hasSecondColor_10 = value;
	}

	inline static int32_t get_offset_of_attachmentName_11() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___attachmentName_11)); }
	inline String_t* get_attachmentName_11() const { return ___attachmentName_11; }
	inline String_t** get_address_of_attachmentName_11() { return &___attachmentName_11; }
	inline void set_attachmentName_11(String_t* value)
	{
		___attachmentName_11 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentName_11), value);
	}

	inline static int32_t get_offset_of_blendMode_12() { return static_cast<int32_t>(offsetof(SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738, ___blendMode_12)); }
	inline int32_t get_blendMode_12() const { return ___blendMode_12; }
	inline int32_t* get_address_of_blendMode_12() { return &___blendMode_12; }
	inline void set_blendMode_12(int32_t value)
	{
		___blendMode_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTDATA_TDC4C24F4167DCD16DC14E99E265C6ED2F9851738_H
#ifndef SPINEANIMATION_TDC6BD9FC4A4C86FA45DE0724A9C4C0CC7AA0C210_H
#define SPINEANIMATION_TDC6BD9FC4A4C86FA45DE0724A9C4C0CC7AA0C210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAnimation
struct  SpineAnimation_tDC6BD9FC4A4C86FA45DE0724A9C4C0CC7AA0C210  : public SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEANIMATION_TDC6BD9FC4A4C86FA45DE0724A9C4C0CC7AA0C210_H
#ifndef SPINEATTACHMENT_T8C50B5C8D70F662B66900A40043A9EE6E72510DE_H
#define SPINEATTACHMENT_T8C50B5C8D70F662B66900A40043A9EE6E72510DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttachment
struct  SpineAttachment_t8C50B5C8D70F662B66900A40043A9EE6E72510DE  : public SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764
{
public:
	// System.Boolean Spine.Unity.SpineAttachment::returnAttachmentPath
	bool ___returnAttachmentPath_4;
	// System.Boolean Spine.Unity.SpineAttachment::currentSkinOnly
	bool ___currentSkinOnly_5;
	// System.Boolean Spine.Unity.SpineAttachment::placeholdersOnly
	bool ___placeholdersOnly_6;
	// System.String Spine.Unity.SpineAttachment::skinField
	String_t* ___skinField_7;
	// System.String Spine.Unity.SpineAttachment::slotField
	String_t* ___slotField_8;

public:
	inline static int32_t get_offset_of_returnAttachmentPath_4() { return static_cast<int32_t>(offsetof(SpineAttachment_t8C50B5C8D70F662B66900A40043A9EE6E72510DE, ___returnAttachmentPath_4)); }
	inline bool get_returnAttachmentPath_4() const { return ___returnAttachmentPath_4; }
	inline bool* get_address_of_returnAttachmentPath_4() { return &___returnAttachmentPath_4; }
	inline void set_returnAttachmentPath_4(bool value)
	{
		___returnAttachmentPath_4 = value;
	}

	inline static int32_t get_offset_of_currentSkinOnly_5() { return static_cast<int32_t>(offsetof(SpineAttachment_t8C50B5C8D70F662B66900A40043A9EE6E72510DE, ___currentSkinOnly_5)); }
	inline bool get_currentSkinOnly_5() const { return ___currentSkinOnly_5; }
	inline bool* get_address_of_currentSkinOnly_5() { return &___currentSkinOnly_5; }
	inline void set_currentSkinOnly_5(bool value)
	{
		___currentSkinOnly_5 = value;
	}

	inline static int32_t get_offset_of_placeholdersOnly_6() { return static_cast<int32_t>(offsetof(SpineAttachment_t8C50B5C8D70F662B66900A40043A9EE6E72510DE, ___placeholdersOnly_6)); }
	inline bool get_placeholdersOnly_6() const { return ___placeholdersOnly_6; }
	inline bool* get_address_of_placeholdersOnly_6() { return &___placeholdersOnly_6; }
	inline void set_placeholdersOnly_6(bool value)
	{
		___placeholdersOnly_6 = value;
	}

	inline static int32_t get_offset_of_skinField_7() { return static_cast<int32_t>(offsetof(SpineAttachment_t8C50B5C8D70F662B66900A40043A9EE6E72510DE, ___skinField_7)); }
	inline String_t* get_skinField_7() const { return ___skinField_7; }
	inline String_t** get_address_of_skinField_7() { return &___skinField_7; }
	inline void set_skinField_7(String_t* value)
	{
		___skinField_7 = value;
		Il2CppCodeGenWriteBarrier((&___skinField_7), value);
	}

	inline static int32_t get_offset_of_slotField_8() { return static_cast<int32_t>(offsetof(SpineAttachment_t8C50B5C8D70F662B66900A40043A9EE6E72510DE, ___slotField_8)); }
	inline String_t* get_slotField_8() const { return ___slotField_8; }
	inline String_t** get_address_of_slotField_8() { return &___slotField_8; }
	inline void set_slotField_8(String_t* value)
	{
		___slotField_8 = value;
		Il2CppCodeGenWriteBarrier((&___slotField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATTACHMENT_T8C50B5C8D70F662B66900A40043A9EE6E72510DE_H
#ifndef SPINEBONE_T689D9B8ABFA81DC532671502108C180F3CFB7B35_H
#define SPINEBONE_T689D9B8ABFA81DC532671502108C180F3CFB7B35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineBone
struct  SpineBone_t689D9B8ABFA81DC532671502108C180F3CFB7B35  : public SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEBONE_T689D9B8ABFA81DC532671502108C180F3CFB7B35_H
#ifndef SPINEEVENT_TDD867EFA230B93A502C0509421FB41D137E1823B_H
#define SPINEEVENT_TDD867EFA230B93A502C0509421FB41D137E1823B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineEvent
struct  SpineEvent_tDD867EFA230B93A502C0509421FB41D137E1823B  : public SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEEVENT_TDD867EFA230B93A502C0509421FB41D137E1823B_H
#ifndef SPINEIKCONSTRAINT_T3F02582BB7C221CFD1CDA00B7DE7455B1D6F0731_H
#define SPINEIKCONSTRAINT_T3F02582BB7C221CFD1CDA00B7DE7455B1D6F0731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineIkConstraint
struct  SpineIkConstraint_t3F02582BB7C221CFD1CDA00B7DE7455B1D6F0731  : public SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEIKCONSTRAINT_T3F02582BB7C221CFD1CDA00B7DE7455B1D6F0731_H
#ifndef SPINEMESH_TA74327C2396922F1F5CD0C46F2572A15DAAAD41C_H
#define SPINEMESH_TA74327C2396922F1F5CD0C46F2572A15DAAAD41C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineMesh
struct  SpineMesh_tA74327C2396922F1F5CD0C46F2572A15DAAAD41C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEMESH_TA74327C2396922F1F5CD0C46F2572A15DAAAD41C_H
#ifndef SPINEPATHCONSTRAINT_T5332F6467925E378ADBC480ACBBF6AED70B4ABF2_H
#define SPINEPATHCONSTRAINT_T5332F6467925E378ADBC480ACBBF6AED70B4ABF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpinePathConstraint
struct  SpinePathConstraint_t5332F6467925E378ADBC480ACBBF6AED70B4ABF2  : public SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEPATHCONSTRAINT_T5332F6467925E378ADBC480ACBBF6AED70B4ABF2_H
#ifndef SPINESKIN_T2F9AA1C8D5422D2C57A36817C5EE0A2CC3216E8D_H
#define SPINESKIN_T2F9AA1C8D5422D2C57A36817C5EE0A2CC3216E8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineSkin
struct  SpineSkin_t2F9AA1C8D5422D2C57A36817C5EE0A2CC3216E8D  : public SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESKIN_T2F9AA1C8D5422D2C57A36817C5EE0A2CC3216E8D_H
#ifndef SPINESLOT_TD60AA77C245C9340EA797F092E25E231D641DACD_H
#define SPINESLOT_TD60AA77C245C9340EA797F092E25E231D641DACD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineSlot
struct  SpineSlot_tD60AA77C245C9340EA797F092E25E231D641DACD  : public SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764
{
public:
	// System.Boolean Spine.Unity.SpineSlot::containsBoundingBoxes
	bool ___containsBoundingBoxes_4;

public:
	inline static int32_t get_offset_of_containsBoundingBoxes_4() { return static_cast<int32_t>(offsetof(SpineSlot_tD60AA77C245C9340EA797F092E25E231D641DACD, ___containsBoundingBoxes_4)); }
	inline bool get_containsBoundingBoxes_4() const { return ___containsBoundingBoxes_4; }
	inline bool* get_address_of_containsBoundingBoxes_4() { return &___containsBoundingBoxes_4; }
	inline void set_containsBoundingBoxes_4(bool value)
	{
		___containsBoundingBoxes_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESLOT_TD60AA77C245C9340EA797F092E25E231D641DACD_H
#ifndef SPINETRANSFORMCONSTRAINT_TC537B1A9801E08AF9BD2958A75DB6BE68196491F_H
#define SPINETRANSFORMCONSTRAINT_TC537B1A9801E08AF9BD2958A75DB6BE68196491F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineTransformConstraint
struct  SpineTransformConstraint_tC537B1A9801E08AF9BD2958A75DB6BE68196491F  : public SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINETRANSFORMCONSTRAINT_TC537B1A9801E08AF9BD2958A75DB6BE68196491F_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef ANIMATIONREFERENCEASSET_TE5971853301FB052014721823A240B976A89BDCB_H
#define ANIMATIONREFERENCEASSET_TE5971853301FB052014721823A240B976A89BDCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.AnimationReferenceAsset
struct  AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Spine.Unity.SkeletonDataAsset Spine.Unity.AnimationReferenceAsset::skeletonDataAsset
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * ___skeletonDataAsset_5;
	// System.String Spine.Unity.AnimationReferenceAsset::animationName
	String_t* ___animationName_6;
	// Spine.Animation Spine.Unity.AnimationReferenceAsset::animation
	Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * ___animation_7;

public:
	inline static int32_t get_offset_of_skeletonDataAsset_5() { return static_cast<int32_t>(offsetof(AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB, ___skeletonDataAsset_5)); }
	inline SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * get_skeletonDataAsset_5() const { return ___skeletonDataAsset_5; }
	inline SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 ** get_address_of_skeletonDataAsset_5() { return &___skeletonDataAsset_5; }
	inline void set_skeletonDataAsset_5(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * value)
	{
		___skeletonDataAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_5), value);
	}

	inline static int32_t get_offset_of_animationName_6() { return static_cast<int32_t>(offsetof(AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB, ___animationName_6)); }
	inline String_t* get_animationName_6() const { return ___animationName_6; }
	inline String_t** get_address_of_animationName_6() { return &___animationName_6; }
	inline void set_animationName_6(String_t* value)
	{
		___animationName_6 = value;
		Il2CppCodeGenWriteBarrier((&___animationName_6), value);
	}

	inline static int32_t get_offset_of_animation_7() { return static_cast<int32_t>(offsetof(AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB, ___animation_7)); }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * get_animation_7() const { return ___animation_7; }
	inline Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 ** get_address_of_animation_7() { return &___animation_7; }
	inline void set_animation_7(Animation_t7943402608B3C2228DDB4C2496C65B87CFD5B482 * value)
	{
		___animation_7 = value;
		Il2CppCodeGenWriteBarrier((&___animation_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONREFERENCEASSET_TE5971853301FB052014721823A240B976A89BDCB_H
#ifndef ATLASASSET_TA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47_H
#define ATLASASSET_TA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.AtlasAsset
struct  AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.TextAsset Spine.Unity.AtlasAsset::atlasFile
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___atlasFile_4;
	// UnityEngine.Material[] Spine.Unity.AtlasAsset::materials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___materials_5;
	// Spine.Atlas Spine.Unity.AtlasAsset::atlas
	Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4 * ___atlas_6;

public:
	inline static int32_t get_offset_of_atlasFile_4() { return static_cast<int32_t>(offsetof(AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47, ___atlasFile_4)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_atlasFile_4() const { return ___atlasFile_4; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_atlasFile_4() { return &___atlasFile_4; }
	inline void set_atlasFile_4(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___atlasFile_4 = value;
		Il2CppCodeGenWriteBarrier((&___atlasFile_4), value);
	}

	inline static int32_t get_offset_of_materials_5() { return static_cast<int32_t>(offsetof(AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47, ___materials_5)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_materials_5() const { return ___materials_5; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_materials_5() { return &___materials_5; }
	inline void set_materials_5(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___materials_5 = value;
		Il2CppCodeGenWriteBarrier((&___materials_5), value);
	}

	inline static int32_t get_offset_of_atlas_6() { return static_cast<int32_t>(offsetof(AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47, ___atlas_6)); }
	inline Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4 * get_atlas_6() const { return ___atlas_6; }
	inline Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4 ** get_address_of_atlas_6() { return &___atlas_6; }
	inline void set_atlas_6(Atlas_t59791993EEB5BEAEAF07A47B141C848194676CE4 * value)
	{
		___atlas_6 = value;
		Il2CppCodeGenWriteBarrier((&___atlas_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASASSET_TA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47_H
#ifndef EVENTDATAREFERENCEASSET_T29895D35810EFF3CE3F67E931A1171ECDB397E28_H
#define EVENTDATAREFERENCEASSET_T29895D35810EFF3CE3F67E931A1171ECDB397E28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.EventDataReferenceAsset
struct  EventDataReferenceAsset_t29895D35810EFF3CE3F67E931A1171ECDB397E28  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Spine.Unity.SkeletonDataAsset Spine.Unity.EventDataReferenceAsset::skeletonDataAsset
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * ___skeletonDataAsset_5;
	// System.String Spine.Unity.EventDataReferenceAsset::eventName
	String_t* ___eventName_6;
	// Spine.EventData Spine.Unity.EventDataReferenceAsset::eventData
	EventData_t34B08BE715A229D34906F96784419D669DE4869E * ___eventData_7;

public:
	inline static int32_t get_offset_of_skeletonDataAsset_5() { return static_cast<int32_t>(offsetof(EventDataReferenceAsset_t29895D35810EFF3CE3F67E931A1171ECDB397E28, ___skeletonDataAsset_5)); }
	inline SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * get_skeletonDataAsset_5() const { return ___skeletonDataAsset_5; }
	inline SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 ** get_address_of_skeletonDataAsset_5() { return &___skeletonDataAsset_5; }
	inline void set_skeletonDataAsset_5(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * value)
	{
		___skeletonDataAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_5), value);
	}

	inline static int32_t get_offset_of_eventName_6() { return static_cast<int32_t>(offsetof(EventDataReferenceAsset_t29895D35810EFF3CE3F67E931A1171ECDB397E28, ___eventName_6)); }
	inline String_t* get_eventName_6() const { return ___eventName_6; }
	inline String_t** get_address_of_eventName_6() { return &___eventName_6; }
	inline void set_eventName_6(String_t* value)
	{
		___eventName_6 = value;
		Il2CppCodeGenWriteBarrier((&___eventName_6), value);
	}

	inline static int32_t get_offset_of_eventData_7() { return static_cast<int32_t>(offsetof(EventDataReferenceAsset_t29895D35810EFF3CE3F67E931A1171ECDB397E28, ___eventData_7)); }
	inline EventData_t34B08BE715A229D34906F96784419D669DE4869E * get_eventData_7() const { return ___eventData_7; }
	inline EventData_t34B08BE715A229D34906F96784419D669DE4869E ** get_address_of_eventData_7() { return &___eventData_7; }
	inline void set_eventData_7(EventData_t34B08BE715A229D34906F96784419D669DE4869E * value)
	{
		___eventData_7 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDATAREFERENCEASSET_T29895D35810EFF3CE3F67E931A1171ECDB397E28_H
#ifndef MESHGENERATORDELEGATE_TD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4_H
#define MESHGENERATORDELEGATE_TD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGeneratorDelegate
struct  MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHGENERATORDELEGATE_TD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4_H
#ifndef SPINEEVENTDELEGATE_TE33FEE8B9A419F03B4559C77F6CA2E23F1AD8FC7_H
#define SPINEEVENTDELEGATE_TE33FEE8B9A419F03B4559C77F6CA2E23F1AD8FC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Playables.SpineEventDelegate
struct  SpineEventDelegate_tE33FEE8B9A419F03B4559C77F6CA2E23F1AD8FC7  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEEVENTDELEGATE_TE33FEE8B9A419F03B4559C77F6CA2E23F1AD8FC7_H
#ifndef SKELETONDATAASSET_TFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5_H
#define SKELETONDATAASSET_TFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonDataAsset
struct  SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Spine.Unity.AtlasAsset[] Spine.Unity.SkeletonDataAsset::atlasAssets
	AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182* ___atlasAssets_4;
	// System.Single Spine.Unity.SkeletonDataAsset::scale
	float ___scale_5;
	// UnityEngine.TextAsset Spine.Unity.SkeletonDataAsset::skeletonJSON
	TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * ___skeletonJSON_6;
	// System.String[] Spine.Unity.SkeletonDataAsset::fromAnimation
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___fromAnimation_7;
	// System.String[] Spine.Unity.SkeletonDataAsset::toAnimation
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___toAnimation_8;
	// System.Single[] Spine.Unity.SkeletonDataAsset::duration
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___duration_9;
	// System.Single Spine.Unity.SkeletonDataAsset::defaultMix
	float ___defaultMix_10;
	// UnityEngine.RuntimeAnimatorController Spine.Unity.SkeletonDataAsset::controller
	RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * ___controller_11;
	// Spine.SkeletonData Spine.Unity.SkeletonDataAsset::skeletonData
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 * ___skeletonData_12;
	// Spine.AnimationStateData Spine.Unity.SkeletonDataAsset::stateData
	AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C * ___stateData_13;

public:
	inline static int32_t get_offset_of_atlasAssets_4() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___atlasAssets_4)); }
	inline AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182* get_atlasAssets_4() const { return ___atlasAssets_4; }
	inline AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182** get_address_of_atlasAssets_4() { return &___atlasAssets_4; }
	inline void set_atlasAssets_4(AtlasAssetU5BU5D_t80CBA4CCA43F50AFDDCF07B26F04C84823E7B182* value)
	{
		___atlasAssets_4 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAssets_4), value);
	}

	inline static int32_t get_offset_of_scale_5() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___scale_5)); }
	inline float get_scale_5() const { return ___scale_5; }
	inline float* get_address_of_scale_5() { return &___scale_5; }
	inline void set_scale_5(float value)
	{
		___scale_5 = value;
	}

	inline static int32_t get_offset_of_skeletonJSON_6() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___skeletonJSON_6)); }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * get_skeletonJSON_6() const { return ___skeletonJSON_6; }
	inline TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E ** get_address_of_skeletonJSON_6() { return &___skeletonJSON_6; }
	inline void set_skeletonJSON_6(TextAsset_tEE9F5A28C3B564D6BA849C45C13192B9E0EF8D4E * value)
	{
		___skeletonJSON_6 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonJSON_6), value);
	}

	inline static int32_t get_offset_of_fromAnimation_7() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___fromAnimation_7)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_fromAnimation_7() const { return ___fromAnimation_7; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_fromAnimation_7() { return &___fromAnimation_7; }
	inline void set_fromAnimation_7(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___fromAnimation_7 = value;
		Il2CppCodeGenWriteBarrier((&___fromAnimation_7), value);
	}

	inline static int32_t get_offset_of_toAnimation_8() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___toAnimation_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_toAnimation_8() const { return ___toAnimation_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_toAnimation_8() { return &___toAnimation_8; }
	inline void set_toAnimation_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___toAnimation_8 = value;
		Il2CppCodeGenWriteBarrier((&___toAnimation_8), value);
	}

	inline static int32_t get_offset_of_duration_9() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___duration_9)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_duration_9() const { return ___duration_9; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_duration_9() { return &___duration_9; }
	inline void set_duration_9(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___duration_9 = value;
		Il2CppCodeGenWriteBarrier((&___duration_9), value);
	}

	inline static int32_t get_offset_of_defaultMix_10() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___defaultMix_10)); }
	inline float get_defaultMix_10() const { return ___defaultMix_10; }
	inline float* get_address_of_defaultMix_10() { return &___defaultMix_10; }
	inline void set_defaultMix_10(float value)
	{
		___defaultMix_10 = value;
	}

	inline static int32_t get_offset_of_controller_11() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___controller_11)); }
	inline RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * get_controller_11() const { return ___controller_11; }
	inline RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD ** get_address_of_controller_11() { return &___controller_11; }
	inline void set_controller_11(RuntimeAnimatorController_tDA6672C8194522C2F60F8F2F241657E57C3520BD * value)
	{
		___controller_11 = value;
		Il2CppCodeGenWriteBarrier((&___controller_11), value);
	}

	inline static int32_t get_offset_of_skeletonData_12() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___skeletonData_12)); }
	inline SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 * get_skeletonData_12() const { return ___skeletonData_12; }
	inline SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 ** get_address_of_skeletonData_12() { return &___skeletonData_12; }
	inline void set_skeletonData_12(SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20 * value)
	{
		___skeletonData_12 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonData_12), value);
	}

	inline static int32_t get_offset_of_stateData_13() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5, ___stateData_13)); }
	inline AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C * get_stateData_13() const { return ___stateData_13; }
	inline AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C ** get_address_of_stateData_13() { return &___stateData_13; }
	inline void set_stateData_13(AnimationStateData_t1A56593FE94A1CA3E4DCD574014C1D6663E9DB1C * value)
	{
		___stateData_13 = value;
		Il2CppCodeGenWriteBarrier((&___stateData_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONDATAASSET_TFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5_H
#ifndef INSTRUCTIONDELEGATE_T5D855F63CA53EA174005317091099D0BAA478F54_H
#define INSTRUCTIONDELEGATE_T5D855F63CA53EA174005317091099D0BAA478F54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer/InstructionDelegate
struct  InstructionDelegate_t5D855F63CA53EA174005317091099D0BAA478F54  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTRUCTIONDELEGATE_T5D855F63CA53EA174005317091099D0BAA478F54_H
#ifndef SKELETONRENDERERDELEGATE_TF51A39DB9B998C96CEEE3C8F0F55FB65070EBBF7_H
#define SKELETONRENDERERDELEGATE_TF51A39DB9B998C96CEEE3C8F0F55FB65070EBBF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate
struct  SkeletonRendererDelegate_tF51A39DB9B998C96CEEE3C8F0F55FB65070EBBF7  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERERDELEGATE_TF51A39DB9B998C96CEEE3C8F0F55FB65070EBBF7_H
#ifndef SKELETONUTILITYDELEGATE_T40A11BA492FE4C516EBAF8D63834116D5865092B_H
#define SKELETONUTILITYDELEGATE_T40A11BA492FE4C516EBAF8D63834116D5865092B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate
struct  SkeletonUtilityDelegate_t40A11BA492FE4C516EBAF8D63834116D5865092B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYDELEGATE_T40A11BA492FE4C516EBAF8D63834116D5865092B_H
#ifndef UPDATEBONESDELEGATE_T945292DDB255F75682E84AB1D494E86CFED0D513_H
#define UPDATEBONESDELEGATE_T945292DDB255F75682E84AB1D494E86CFED0D513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.UpdateBonesDelegate
struct  UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEBONESDELEGATE_T945292DDB255F75682E84AB1D494E86CFED0D513_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef PLAYABLEASSET_T28B670EFE526C0D383A1C5A5AE2A150424E989AD_H
#define PLAYABLEASSET_T28B670EFE526C0D383A1C5A5AE2A150424E989AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T28B670EFE526C0D383A1C5A5AE2A150424E989AD_H
#ifndef SPINEANIMATIONSTATECLIP_TBB4F4FD5BB543784F15A417D9AE652C034ADCCBB_H
#define SPINEANIMATIONSTATECLIP_TBB4F4FD5BB543784F15A417D9AE652C034ADCCBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Playables.SpineAnimationStateClip
struct  SpineAnimationStateClip_tBB4F4FD5BB543784F15A417D9AE652C034ADCCBB  : public PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD
{
public:
	// Spine.Unity.Playables.SpineAnimationStateBehaviour Spine.Unity.Playables.SpineAnimationStateClip::template
	SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57 * ___template_4;

public:
	inline static int32_t get_offset_of_template_4() { return static_cast<int32_t>(offsetof(SpineAnimationStateClip_tBB4F4FD5BB543784F15A417D9AE652C034ADCCBB, ___template_4)); }
	inline SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57 * get_template_4() const { return ___template_4; }
	inline SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57 ** get_address_of_template_4() { return &___template_4; }
	inline void set_template_4(SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57 * value)
	{
		___template_4 = value;
		Il2CppCodeGenWriteBarrier((&___template_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEANIMATIONSTATECLIP_TBB4F4FD5BB543784F15A417D9AE652C034ADCCBB_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef TRACKASSET_T6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_H
#define TRACKASSET_T6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset
struct  TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC  : public PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD
{
public:
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Locked
	bool ___m_Locked_4;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Muted
	bool ___m_Muted_5;
	// System.String UnityEngine.Timeline.TrackAsset::m_CustomPlayableFullTypename
	String_t* ___m_CustomPlayableFullTypename_6;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TrackAsset::m_AnimClip
	AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * ___m_AnimClip_7;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.TrackAsset::m_Parent
	PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD * ___m_Parent_8;
	// System.Collections.Generic.List`1<UnityEngine.ScriptableObject> UnityEngine.Timeline.TrackAsset::m_Children
	List_1_t803BD2FB729584A0A796EBF33774257912427B4E * ___m_Children_9;
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_ItemsHash
	int32_t ___m_ItemsHash_10;
	// UnityEngine.Timeline.TimelineClip[] UnityEngine.Timeline.TrackAsset::m_ClipsCache
	TimelineClipU5BU5D_t54DF64E1454792297ECC9A75D1E33DB9293334A3* ___m_ClipsCache_11;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_Start
	DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  ___m_Start_12;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_End
	DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  ___m_End_13;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_CacheSorted
	bool ___m_CacheSorted_14;
	// System.Collections.Generic.IEnumerable`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TrackAsset::m_ChildTrackCache
	RuntimeObject* ___m_ChildTrackCache_16;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::m_Clips
	List_1_tBE0C2267D3E6C51CE882E2B6B95F8E67011B1376 * ___m_Clips_18;
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_Version
	int32_t ___m_Version_21;

public:
	inline static int32_t get_offset_of_m_Locked_4() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Locked_4)); }
	inline bool get_m_Locked_4() const { return ___m_Locked_4; }
	inline bool* get_address_of_m_Locked_4() { return &___m_Locked_4; }
	inline void set_m_Locked_4(bool value)
	{
		___m_Locked_4 = value;
	}

	inline static int32_t get_offset_of_m_Muted_5() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Muted_5)); }
	inline bool get_m_Muted_5() const { return ___m_Muted_5; }
	inline bool* get_address_of_m_Muted_5() { return &___m_Muted_5; }
	inline void set_m_Muted_5(bool value)
	{
		___m_Muted_5 = value;
	}

	inline static int32_t get_offset_of_m_CustomPlayableFullTypename_6() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_CustomPlayableFullTypename_6)); }
	inline String_t* get_m_CustomPlayableFullTypename_6() const { return ___m_CustomPlayableFullTypename_6; }
	inline String_t** get_address_of_m_CustomPlayableFullTypename_6() { return &___m_CustomPlayableFullTypename_6; }
	inline void set_m_CustomPlayableFullTypename_6(String_t* value)
	{
		___m_CustomPlayableFullTypename_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomPlayableFullTypename_6), value);
	}

	inline static int32_t get_offset_of_m_AnimClip_7() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_AnimClip_7)); }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * get_m_AnimClip_7() const { return ___m_AnimClip_7; }
	inline AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE ** get_address_of_m_AnimClip_7() { return &___m_AnimClip_7; }
	inline void set_m_AnimClip_7(AnimationClip_t336CFC94F6275526DC0B9BEEF833D4D89D6DEDDE * value)
	{
		___m_AnimClip_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimClip_7), value);
	}

	inline static int32_t get_offset_of_m_Parent_8() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Parent_8)); }
	inline PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD * get_m_Parent_8() const { return ___m_Parent_8; }
	inline PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD ** get_address_of_m_Parent_8() { return &___m_Parent_8; }
	inline void set_m_Parent_8(PlayableAsset_t28B670EFE526C0D383A1C5A5AE2A150424E989AD * value)
	{
		___m_Parent_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parent_8), value);
	}

	inline static int32_t get_offset_of_m_Children_9() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Children_9)); }
	inline List_1_t803BD2FB729584A0A796EBF33774257912427B4E * get_m_Children_9() const { return ___m_Children_9; }
	inline List_1_t803BD2FB729584A0A796EBF33774257912427B4E ** get_address_of_m_Children_9() { return &___m_Children_9; }
	inline void set_m_Children_9(List_1_t803BD2FB729584A0A796EBF33774257912427B4E * value)
	{
		___m_Children_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Children_9), value);
	}

	inline static int32_t get_offset_of_m_ItemsHash_10() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_ItemsHash_10)); }
	inline int32_t get_m_ItemsHash_10() const { return ___m_ItemsHash_10; }
	inline int32_t* get_address_of_m_ItemsHash_10() { return &___m_ItemsHash_10; }
	inline void set_m_ItemsHash_10(int32_t value)
	{
		___m_ItemsHash_10 = value;
	}

	inline static int32_t get_offset_of_m_ClipsCache_11() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_ClipsCache_11)); }
	inline TimelineClipU5BU5D_t54DF64E1454792297ECC9A75D1E33DB9293334A3* get_m_ClipsCache_11() const { return ___m_ClipsCache_11; }
	inline TimelineClipU5BU5D_t54DF64E1454792297ECC9A75D1E33DB9293334A3** get_address_of_m_ClipsCache_11() { return &___m_ClipsCache_11; }
	inline void set_m_ClipsCache_11(TimelineClipU5BU5D_t54DF64E1454792297ECC9A75D1E33DB9293334A3* value)
	{
		___m_ClipsCache_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClipsCache_11), value);
	}

	inline static int32_t get_offset_of_m_Start_12() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Start_12)); }
	inline DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  get_m_Start_12() const { return ___m_Start_12; }
	inline DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC * get_address_of_m_Start_12() { return &___m_Start_12; }
	inline void set_m_Start_12(DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  value)
	{
		___m_Start_12 = value;
	}

	inline static int32_t get_offset_of_m_End_13() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_End_13)); }
	inline DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  get_m_End_13() const { return ___m_End_13; }
	inline DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC * get_address_of_m_End_13() { return &___m_End_13; }
	inline void set_m_End_13(DiscreteTime_t046D6A2A06BCF3D3853E9CAFE33CB138C0E164FC  value)
	{
		___m_End_13 = value;
	}

	inline static int32_t get_offset_of_m_CacheSorted_14() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_CacheSorted_14)); }
	inline bool get_m_CacheSorted_14() const { return ___m_CacheSorted_14; }
	inline bool* get_address_of_m_CacheSorted_14() { return &___m_CacheSorted_14; }
	inline void set_m_CacheSorted_14(bool value)
	{
		___m_CacheSorted_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildTrackCache_16() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_ChildTrackCache_16)); }
	inline RuntimeObject* get_m_ChildTrackCache_16() const { return ___m_ChildTrackCache_16; }
	inline RuntimeObject** get_address_of_m_ChildTrackCache_16() { return &___m_ChildTrackCache_16; }
	inline void set_m_ChildTrackCache_16(RuntimeObject* value)
	{
		___m_ChildTrackCache_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChildTrackCache_16), value);
	}

	inline static int32_t get_offset_of_m_Clips_18() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Clips_18)); }
	inline List_1_tBE0C2267D3E6C51CE882E2B6B95F8E67011B1376 * get_m_Clips_18() const { return ___m_Clips_18; }
	inline List_1_tBE0C2267D3E6C51CE882E2B6B95F8E67011B1376 ** get_address_of_m_Clips_18() { return &___m_Clips_18; }
	inline void set_m_Clips_18(List_1_tBE0C2267D3E6C51CE882E2B6B95F8E67011B1376 * value)
	{
		___m_Clips_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clips_18), value);
	}

	inline static int32_t get_offset_of_m_Version_21() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC, ___m_Version_21)); }
	inline int32_t get_m_Version_21() const { return ___m_Version_21; }
	inline int32_t* get_address_of_m_Version_21() { return &___m_Version_21; }
	inline void set_m_Version_21(int32_t value)
	{
		___m_Version_21 = value;
	}
};

struct TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields
{
public:
	// UnityEngine.Timeline.TrackAsset[] UnityEngine.Timeline.TrackAsset::s_EmptyCache
	TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E* ___s_EmptyCache_15;
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute> UnityEngine.Timeline.TrackAsset::s_TrackBindingTypeAttributeCache
	Dictionary_2_t58C9CA6651216CBA1E51B3DE5F86E81AB30077B1 * ___s_TrackBindingTypeAttributeCache_17;
	// System.Action`3<UnityEngine.Timeline.TimelineClip,UnityEngine.GameObject,UnityEngine.Playables.Playable> UnityEngine.Timeline.TrackAsset::OnPlayableCreate
	Action_3_t94294D297455C1B4C4B6ACA47BC2B4019ECE97CB * ___OnPlayableCreate_19;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::<>f__am$cache0
	Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8 * ___U3CU3Ef__amU24cache0_22;

public:
	inline static int32_t get_offset_of_s_EmptyCache_15() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields, ___s_EmptyCache_15)); }
	inline TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E* get_s_EmptyCache_15() const { return ___s_EmptyCache_15; }
	inline TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E** get_address_of_s_EmptyCache_15() { return &___s_EmptyCache_15; }
	inline void set_s_EmptyCache_15(TrackAssetU5BU5D_tC31A3552CA774F0CE3BE5E6678D1AAB7B3E2845E* value)
	{
		___s_EmptyCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_EmptyCache_15), value);
	}

	inline static int32_t get_offset_of_s_TrackBindingTypeAttributeCache_17() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields, ___s_TrackBindingTypeAttributeCache_17)); }
	inline Dictionary_2_t58C9CA6651216CBA1E51B3DE5F86E81AB30077B1 * get_s_TrackBindingTypeAttributeCache_17() const { return ___s_TrackBindingTypeAttributeCache_17; }
	inline Dictionary_2_t58C9CA6651216CBA1E51B3DE5F86E81AB30077B1 ** get_address_of_s_TrackBindingTypeAttributeCache_17() { return &___s_TrackBindingTypeAttributeCache_17; }
	inline void set_s_TrackBindingTypeAttributeCache_17(Dictionary_2_t58C9CA6651216CBA1E51B3DE5F86E81AB30077B1 * value)
	{
		___s_TrackBindingTypeAttributeCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrackBindingTypeAttributeCache_17), value);
	}

	inline static int32_t get_offset_of_OnPlayableCreate_19() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields, ___OnPlayableCreate_19)); }
	inline Action_3_t94294D297455C1B4C4B6ACA47BC2B4019ECE97CB * get_OnPlayableCreate_19() const { return ___OnPlayableCreate_19; }
	inline Action_3_t94294D297455C1B4C4B6ACA47BC2B4019ECE97CB ** get_address_of_OnPlayableCreate_19() { return &___OnPlayableCreate_19; }
	inline void set_OnPlayableCreate_19(Action_3_t94294D297455C1B4C4B6ACA47BC2B4019ECE97CB * value)
	{
		___OnPlayableCreate_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnPlayableCreate_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_22() { return static_cast<int32_t>(offsetof(TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_StaticFields, ___U3CU3Ef__amU24cache0_22)); }
	inline Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8 * get_U3CU3Ef__amU24cache0_22() const { return ___U3CU3Ef__amU24cache0_22; }
	inline Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8 ** get_address_of_U3CU3Ef__amU24cache0_22() { return &___U3CU3Ef__amU24cache0_22; }
	inline void set_U3CU3Ef__amU24cache0_22(Comparison_1_t7614A1867A1D09C766E3C9AECCEC0C38CFDDCFB8 * value)
	{
		___U3CU3Ef__amU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKASSET_T6007D636CC5AC4D63FC2CE91D66F1478C23E32EC_H
#ifndef BONEFOLLOWER_T0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0_H
#define BONEFOLLOWER_T0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.BoneFollower
struct  BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.BoneFollower::skeletonRenderer
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * ___skeletonRenderer_4;
	// System.String Spine.Unity.BoneFollower::boneName
	String_t* ___boneName_5;
	// System.Boolean Spine.Unity.BoneFollower::followZPosition
	bool ___followZPosition_6;
	// System.Boolean Spine.Unity.BoneFollower::followBoneRotation
	bool ___followBoneRotation_7;
	// System.Boolean Spine.Unity.BoneFollower::followSkeletonFlip
	bool ___followSkeletonFlip_8;
	// System.Boolean Spine.Unity.BoneFollower::followLocalScale
	bool ___followLocalScale_9;
	// System.Boolean Spine.Unity.BoneFollower::initializeOnAwake
	bool ___initializeOnAwake_10;
	// System.Boolean Spine.Unity.BoneFollower::valid
	bool ___valid_11;
	// Spine.Bone Spine.Unity.BoneFollower::bone
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * ___bone_12;
	// UnityEngine.Transform Spine.Unity.BoneFollower::skeletonTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___skeletonTransform_13;
	// System.Boolean Spine.Unity.BoneFollower::skeletonTransformIsParent
	bool ___skeletonTransformIsParent_14;

public:
	inline static int32_t get_offset_of_skeletonRenderer_4() { return static_cast<int32_t>(offsetof(BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0, ___skeletonRenderer_4)); }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * get_skeletonRenderer_4() const { return ___skeletonRenderer_4; }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 ** get_address_of_skeletonRenderer_4() { return &___skeletonRenderer_4; }
	inline void set_skeletonRenderer_4(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * value)
	{
		___skeletonRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_4), value);
	}

	inline static int32_t get_offset_of_boneName_5() { return static_cast<int32_t>(offsetof(BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0, ___boneName_5)); }
	inline String_t* get_boneName_5() const { return ___boneName_5; }
	inline String_t** get_address_of_boneName_5() { return &___boneName_5; }
	inline void set_boneName_5(String_t* value)
	{
		___boneName_5 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_5), value);
	}

	inline static int32_t get_offset_of_followZPosition_6() { return static_cast<int32_t>(offsetof(BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0, ___followZPosition_6)); }
	inline bool get_followZPosition_6() const { return ___followZPosition_6; }
	inline bool* get_address_of_followZPosition_6() { return &___followZPosition_6; }
	inline void set_followZPosition_6(bool value)
	{
		___followZPosition_6 = value;
	}

	inline static int32_t get_offset_of_followBoneRotation_7() { return static_cast<int32_t>(offsetof(BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0, ___followBoneRotation_7)); }
	inline bool get_followBoneRotation_7() const { return ___followBoneRotation_7; }
	inline bool* get_address_of_followBoneRotation_7() { return &___followBoneRotation_7; }
	inline void set_followBoneRotation_7(bool value)
	{
		___followBoneRotation_7 = value;
	}

	inline static int32_t get_offset_of_followSkeletonFlip_8() { return static_cast<int32_t>(offsetof(BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0, ___followSkeletonFlip_8)); }
	inline bool get_followSkeletonFlip_8() const { return ___followSkeletonFlip_8; }
	inline bool* get_address_of_followSkeletonFlip_8() { return &___followSkeletonFlip_8; }
	inline void set_followSkeletonFlip_8(bool value)
	{
		___followSkeletonFlip_8 = value;
	}

	inline static int32_t get_offset_of_followLocalScale_9() { return static_cast<int32_t>(offsetof(BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0, ___followLocalScale_9)); }
	inline bool get_followLocalScale_9() const { return ___followLocalScale_9; }
	inline bool* get_address_of_followLocalScale_9() { return &___followLocalScale_9; }
	inline void set_followLocalScale_9(bool value)
	{
		___followLocalScale_9 = value;
	}

	inline static int32_t get_offset_of_initializeOnAwake_10() { return static_cast<int32_t>(offsetof(BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0, ___initializeOnAwake_10)); }
	inline bool get_initializeOnAwake_10() const { return ___initializeOnAwake_10; }
	inline bool* get_address_of_initializeOnAwake_10() { return &___initializeOnAwake_10; }
	inline void set_initializeOnAwake_10(bool value)
	{
		___initializeOnAwake_10 = value;
	}

	inline static int32_t get_offset_of_valid_11() { return static_cast<int32_t>(offsetof(BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0, ___valid_11)); }
	inline bool get_valid_11() const { return ___valid_11; }
	inline bool* get_address_of_valid_11() { return &___valid_11; }
	inline void set_valid_11(bool value)
	{
		___valid_11 = value;
	}

	inline static int32_t get_offset_of_bone_12() { return static_cast<int32_t>(offsetof(BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0, ___bone_12)); }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * get_bone_12() const { return ___bone_12; }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E ** get_address_of_bone_12() { return &___bone_12; }
	inline void set_bone_12(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * value)
	{
		___bone_12 = value;
		Il2CppCodeGenWriteBarrier((&___bone_12), value);
	}

	inline static int32_t get_offset_of_skeletonTransform_13() { return static_cast<int32_t>(offsetof(BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0, ___skeletonTransform_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_skeletonTransform_13() const { return ___skeletonTransform_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_skeletonTransform_13() { return &___skeletonTransform_13; }
	inline void set_skeletonTransform_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___skeletonTransform_13 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_13), value);
	}

	inline static int32_t get_offset_of_skeletonTransformIsParent_14() { return static_cast<int32_t>(offsetof(BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0, ___skeletonTransformIsParent_14)); }
	inline bool get_skeletonTransformIsParent_14() const { return ___skeletonTransformIsParent_14; }
	inline bool* get_address_of_skeletonTransformIsParent_14() { return &___skeletonTransformIsParent_14; }
	inline void set_skeletonTransformIsParent_14(bool value)
	{
		___skeletonTransformIsParent_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEFOLLOWER_T0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0_H
#ifndef BONEFOLLOWERGRAPHIC_TFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4_H
#define BONEFOLLOWERGRAPHIC_TFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.BoneFollowerGraphic
struct  BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.SkeletonGraphic Spine.Unity.BoneFollowerGraphic::skeletonGraphic
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * ___skeletonGraphic_4;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::initializeOnAwake
	bool ___initializeOnAwake_5;
	// System.String Spine.Unity.BoneFollowerGraphic::boneName
	String_t* ___boneName_6;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followBoneRotation
	bool ___followBoneRotation_7;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followSkeletonFlip
	bool ___followSkeletonFlip_8;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followLocalScale
	bool ___followLocalScale_9;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followZPosition
	bool ___followZPosition_10;
	// Spine.Bone Spine.Unity.BoneFollowerGraphic::bone
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * ___bone_11;
	// UnityEngine.Transform Spine.Unity.BoneFollowerGraphic::skeletonTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___skeletonTransform_12;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::skeletonTransformIsParent
	bool ___skeletonTransformIsParent_13;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::valid
	bool ___valid_14;

public:
	inline static int32_t get_offset_of_skeletonGraphic_4() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4, ___skeletonGraphic_4)); }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * get_skeletonGraphic_4() const { return ___skeletonGraphic_4; }
	inline SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A ** get_address_of_skeletonGraphic_4() { return &___skeletonGraphic_4; }
	inline void set_skeletonGraphic_4(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A * value)
	{
		___skeletonGraphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonGraphic_4), value);
	}

	inline static int32_t get_offset_of_initializeOnAwake_5() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4, ___initializeOnAwake_5)); }
	inline bool get_initializeOnAwake_5() const { return ___initializeOnAwake_5; }
	inline bool* get_address_of_initializeOnAwake_5() { return &___initializeOnAwake_5; }
	inline void set_initializeOnAwake_5(bool value)
	{
		___initializeOnAwake_5 = value;
	}

	inline static int32_t get_offset_of_boneName_6() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4, ___boneName_6)); }
	inline String_t* get_boneName_6() const { return ___boneName_6; }
	inline String_t** get_address_of_boneName_6() { return &___boneName_6; }
	inline void set_boneName_6(String_t* value)
	{
		___boneName_6 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_6), value);
	}

	inline static int32_t get_offset_of_followBoneRotation_7() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4, ___followBoneRotation_7)); }
	inline bool get_followBoneRotation_7() const { return ___followBoneRotation_7; }
	inline bool* get_address_of_followBoneRotation_7() { return &___followBoneRotation_7; }
	inline void set_followBoneRotation_7(bool value)
	{
		___followBoneRotation_7 = value;
	}

	inline static int32_t get_offset_of_followSkeletonFlip_8() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4, ___followSkeletonFlip_8)); }
	inline bool get_followSkeletonFlip_8() const { return ___followSkeletonFlip_8; }
	inline bool* get_address_of_followSkeletonFlip_8() { return &___followSkeletonFlip_8; }
	inline void set_followSkeletonFlip_8(bool value)
	{
		___followSkeletonFlip_8 = value;
	}

	inline static int32_t get_offset_of_followLocalScale_9() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4, ___followLocalScale_9)); }
	inline bool get_followLocalScale_9() const { return ___followLocalScale_9; }
	inline bool* get_address_of_followLocalScale_9() { return &___followLocalScale_9; }
	inline void set_followLocalScale_9(bool value)
	{
		___followLocalScale_9 = value;
	}

	inline static int32_t get_offset_of_followZPosition_10() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4, ___followZPosition_10)); }
	inline bool get_followZPosition_10() const { return ___followZPosition_10; }
	inline bool* get_address_of_followZPosition_10() { return &___followZPosition_10; }
	inline void set_followZPosition_10(bool value)
	{
		___followZPosition_10 = value;
	}

	inline static int32_t get_offset_of_bone_11() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4, ___bone_11)); }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * get_bone_11() const { return ___bone_11; }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E ** get_address_of_bone_11() { return &___bone_11; }
	inline void set_bone_11(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * value)
	{
		___bone_11 = value;
		Il2CppCodeGenWriteBarrier((&___bone_11), value);
	}

	inline static int32_t get_offset_of_skeletonTransform_12() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4, ___skeletonTransform_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_skeletonTransform_12() const { return ___skeletonTransform_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_skeletonTransform_12() { return &___skeletonTransform_12; }
	inline void set_skeletonTransform_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___skeletonTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_12), value);
	}

	inline static int32_t get_offset_of_skeletonTransformIsParent_13() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4, ___skeletonTransformIsParent_13)); }
	inline bool get_skeletonTransformIsParent_13() const { return ___skeletonTransformIsParent_13; }
	inline bool* get_address_of_skeletonTransformIsParent_13() { return &___skeletonTransformIsParent_13; }
	inline void set_skeletonTransformIsParent_13(bool value)
	{
		___skeletonTransformIsParent_13 = value;
	}

	inline static int32_t get_offset_of_valid_14() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4, ___valid_14)); }
	inline bool get_valid_14() const { return ___valid_14; }
	inline bool* get_address_of_valid_14() { return &___valid_14; }
	inline void set_valid_14(bool value)
	{
		___valid_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEFOLLOWERGRAPHIC_TFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4_H
#ifndef BOUNDINGBOXFOLLOWER_T1626DCCB976CB582AAEB134FB4899D667D14E80B_H
#define BOUNDINGBOXFOLLOWER_T1626DCCB976CB582AAEB134FB4899D667D14E80B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.BoundingBoxFollower
struct  BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.BoundingBoxFollower::skeletonRenderer
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * ___skeletonRenderer_5;
	// System.String Spine.Unity.BoundingBoxFollower::slotName
	String_t* ___slotName_6;
	// System.Boolean Spine.Unity.BoundingBoxFollower::isTrigger
	bool ___isTrigger_7;
	// System.Boolean Spine.Unity.BoundingBoxFollower::clearStateOnDisable
	bool ___clearStateOnDisable_8;
	// Spine.Slot Spine.Unity.BoundingBoxFollower::slot
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51 * ___slot_9;
	// Spine.BoundingBoxAttachment Spine.Unity.BoundingBoxFollower::currentAttachment
	BoundingBoxAttachment_tE2A96EC447B439870D918C9F72B9093D49707931 * ___currentAttachment_10;
	// System.String Spine.Unity.BoundingBoxFollower::currentAttachmentName
	String_t* ___currentAttachmentName_11;
	// UnityEngine.PolygonCollider2D Spine.Unity.BoundingBoxFollower::currentCollider
	PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 * ___currentCollider_12;
	// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,UnityEngine.PolygonCollider2D> Spine.Unity.BoundingBoxFollower::colliderTable
	Dictionary_2_tF77553BC41651B08657E2A18F051DC632966797D * ___colliderTable_13;
	// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,System.String> Spine.Unity.BoundingBoxFollower::nameTable
	Dictionary_2_tE1AD6DF5433910BA9C3CE74FBEA2DF0456202D27 * ___nameTable_14;

public:
	inline static int32_t get_offset_of_skeletonRenderer_5() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B, ___skeletonRenderer_5)); }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * get_skeletonRenderer_5() const { return ___skeletonRenderer_5; }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 ** get_address_of_skeletonRenderer_5() { return &___skeletonRenderer_5; }
	inline void set_skeletonRenderer_5(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * value)
	{
		___skeletonRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_5), value);
	}

	inline static int32_t get_offset_of_slotName_6() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B, ___slotName_6)); }
	inline String_t* get_slotName_6() const { return ___slotName_6; }
	inline String_t** get_address_of_slotName_6() { return &___slotName_6; }
	inline void set_slotName_6(String_t* value)
	{
		___slotName_6 = value;
		Il2CppCodeGenWriteBarrier((&___slotName_6), value);
	}

	inline static int32_t get_offset_of_isTrigger_7() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B, ___isTrigger_7)); }
	inline bool get_isTrigger_7() const { return ___isTrigger_7; }
	inline bool* get_address_of_isTrigger_7() { return &___isTrigger_7; }
	inline void set_isTrigger_7(bool value)
	{
		___isTrigger_7 = value;
	}

	inline static int32_t get_offset_of_clearStateOnDisable_8() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B, ___clearStateOnDisable_8)); }
	inline bool get_clearStateOnDisable_8() const { return ___clearStateOnDisable_8; }
	inline bool* get_address_of_clearStateOnDisable_8() { return &___clearStateOnDisable_8; }
	inline void set_clearStateOnDisable_8(bool value)
	{
		___clearStateOnDisable_8 = value;
	}

	inline static int32_t get_offset_of_slot_9() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B, ___slot_9)); }
	inline Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51 * get_slot_9() const { return ___slot_9; }
	inline Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51 ** get_address_of_slot_9() { return &___slot_9; }
	inline void set_slot_9(Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51 * value)
	{
		___slot_9 = value;
		Il2CppCodeGenWriteBarrier((&___slot_9), value);
	}

	inline static int32_t get_offset_of_currentAttachment_10() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B, ___currentAttachment_10)); }
	inline BoundingBoxAttachment_tE2A96EC447B439870D918C9F72B9093D49707931 * get_currentAttachment_10() const { return ___currentAttachment_10; }
	inline BoundingBoxAttachment_tE2A96EC447B439870D918C9F72B9093D49707931 ** get_address_of_currentAttachment_10() { return &___currentAttachment_10; }
	inline void set_currentAttachment_10(BoundingBoxAttachment_tE2A96EC447B439870D918C9F72B9093D49707931 * value)
	{
		___currentAttachment_10 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttachment_10), value);
	}

	inline static int32_t get_offset_of_currentAttachmentName_11() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B, ___currentAttachmentName_11)); }
	inline String_t* get_currentAttachmentName_11() const { return ___currentAttachmentName_11; }
	inline String_t** get_address_of_currentAttachmentName_11() { return &___currentAttachmentName_11; }
	inline void set_currentAttachmentName_11(String_t* value)
	{
		___currentAttachmentName_11 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttachmentName_11), value);
	}

	inline static int32_t get_offset_of_currentCollider_12() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B, ___currentCollider_12)); }
	inline PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 * get_currentCollider_12() const { return ___currentCollider_12; }
	inline PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 ** get_address_of_currentCollider_12() { return &___currentCollider_12; }
	inline void set_currentCollider_12(PolygonCollider2D_t360CC66FD7BAF5F4FADE54EB23E58E95D0B8CD81 * value)
	{
		___currentCollider_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentCollider_12), value);
	}

	inline static int32_t get_offset_of_colliderTable_13() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B, ___colliderTable_13)); }
	inline Dictionary_2_tF77553BC41651B08657E2A18F051DC632966797D * get_colliderTable_13() const { return ___colliderTable_13; }
	inline Dictionary_2_tF77553BC41651B08657E2A18F051DC632966797D ** get_address_of_colliderTable_13() { return &___colliderTable_13; }
	inline void set_colliderTable_13(Dictionary_2_tF77553BC41651B08657E2A18F051DC632966797D * value)
	{
		___colliderTable_13 = value;
		Il2CppCodeGenWriteBarrier((&___colliderTable_13), value);
	}

	inline static int32_t get_offset_of_nameTable_14() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B, ___nameTable_14)); }
	inline Dictionary_2_tE1AD6DF5433910BA9C3CE74FBEA2DF0456202D27 * get_nameTable_14() const { return ___nameTable_14; }
	inline Dictionary_2_tE1AD6DF5433910BA9C3CE74FBEA2DF0456202D27 ** get_address_of_nameTable_14() { return &___nameTable_14; }
	inline void set_nameTable_14(Dictionary_2_tE1AD6DF5433910BA9C3CE74FBEA2DF0456202D27 * value)
	{
		___nameTable_14 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_14), value);
	}
};

struct BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B_StaticFields
{
public:
	// System.Boolean Spine.Unity.BoundingBoxFollower::DebugMessages
	bool ___DebugMessages_4;

public:
	inline static int32_t get_offset_of_DebugMessages_4() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B_StaticFields, ___DebugMessages_4)); }
	inline bool get_DebugMessages_4() const { return ___DebugMessages_4; }
	inline bool* get_address_of_DebugMessages_4() { return &___DebugMessages_4; }
	inline void set_DebugMessages_4(bool value)
	{
		___DebugMessages_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXFOLLOWER_T1626DCCB976CB582AAEB134FB4899D667D14E80B_H
#ifndef SPINEANIMATIONSTATETRACK_T85DEB3FA9EDAE7A6326A51D4A0A208E0C9678659_H
#define SPINEANIMATIONSTATETRACK_T85DEB3FA9EDAE7A6326A51D4A0A208E0C9678659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Playables.SpineAnimationStateTrack
struct  SpineAnimationStateTrack_t85DEB3FA9EDAE7A6326A51D4A0A208E0C9678659  : public TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEANIMATIONSTATETRACK_T85DEB3FA9EDAE7A6326A51D4A0A208E0C9678659_H
#ifndef SPINEPLAYABLEHANDLEBASE_TB5F200A1F15E5F638384F2B4B26C076C4E4810AC_H
#define SPINEPLAYABLEHANDLEBASE_TB5F200A1F15E5F638384F2B4B26C076C4E4810AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Playables.SpinePlayableHandleBase
struct  SpinePlayableHandleBase_tB5F200A1F15E5F638384F2B4B26C076C4E4810AC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.Playables.SpineEventDelegate Spine.Unity.Playables.SpinePlayableHandleBase::AnimationEvents
	SpineEventDelegate_tE33FEE8B9A419F03B4559C77F6CA2E23F1AD8FC7 * ___AnimationEvents_4;

public:
	inline static int32_t get_offset_of_AnimationEvents_4() { return static_cast<int32_t>(offsetof(SpinePlayableHandleBase_tB5F200A1F15E5F638384F2B4B26C076C4E4810AC, ___AnimationEvents_4)); }
	inline SpineEventDelegate_tE33FEE8B9A419F03B4559C77F6CA2E23F1AD8FC7 * get_AnimationEvents_4() const { return ___AnimationEvents_4; }
	inline SpineEventDelegate_tE33FEE8B9A419F03B4559C77F6CA2E23F1AD8FC7 ** get_address_of_AnimationEvents_4() { return &___AnimationEvents_4; }
	inline void set_AnimationEvents_4(SpineEventDelegate_tE33FEE8B9A419F03B4559C77F6CA2E23F1AD8FC7 * value)
	{
		___AnimationEvents_4 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationEvents_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEPLAYABLEHANDLEBASE_TB5F200A1F15E5F638384F2B4B26C076C4E4810AC_H
#ifndef SPINESKELETONFLIPTRACK_T4AB91DCA36CA7BA88FE8CA96A9626AB42097E127_H
#define SPINESKELETONFLIPTRACK_T4AB91DCA36CA7BA88FE8CA96A9626AB42097E127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Playables.SpineSkeletonFlipTrack
struct  SpineSkeletonFlipTrack_t4AB91DCA36CA7BA88FE8CA96A9626AB42097E127  : public TrackAsset_t6007D636CC5AC4D63FC2CE91D66F1478C23E32EC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESKELETONFLIPTRACK_T4AB91DCA36CA7BA88FE8CA96A9626AB42097E127_H
#ifndef POINTFOLLOWER_TADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3_H
#define POINTFOLLOWER_TADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.PointFollower
struct  PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.PointFollower::skeletonRenderer
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * ___skeletonRenderer_4;
	// System.String Spine.Unity.PointFollower::slotName
	String_t* ___slotName_5;
	// System.String Spine.Unity.PointFollower::pointAttachmentName
	String_t* ___pointAttachmentName_6;
	// System.Boolean Spine.Unity.PointFollower::followRotation
	bool ___followRotation_7;
	// System.Boolean Spine.Unity.PointFollower::followSkeletonFlip
	bool ___followSkeletonFlip_8;
	// System.Boolean Spine.Unity.PointFollower::followSkeletonZPosition
	bool ___followSkeletonZPosition_9;
	// UnityEngine.Transform Spine.Unity.PointFollower::skeletonTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___skeletonTransform_10;
	// System.Boolean Spine.Unity.PointFollower::skeletonTransformIsParent
	bool ___skeletonTransformIsParent_11;
	// Spine.PointAttachment Spine.Unity.PointFollower::point
	PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B * ___point_12;
	// Spine.Bone Spine.Unity.PointFollower::bone
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * ___bone_13;
	// System.Boolean Spine.Unity.PointFollower::valid
	bool ___valid_14;

public:
	inline static int32_t get_offset_of_skeletonRenderer_4() { return static_cast<int32_t>(offsetof(PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3, ___skeletonRenderer_4)); }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * get_skeletonRenderer_4() const { return ___skeletonRenderer_4; }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 ** get_address_of_skeletonRenderer_4() { return &___skeletonRenderer_4; }
	inline void set_skeletonRenderer_4(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * value)
	{
		___skeletonRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_4), value);
	}

	inline static int32_t get_offset_of_slotName_5() { return static_cast<int32_t>(offsetof(PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3, ___slotName_5)); }
	inline String_t* get_slotName_5() const { return ___slotName_5; }
	inline String_t** get_address_of_slotName_5() { return &___slotName_5; }
	inline void set_slotName_5(String_t* value)
	{
		___slotName_5 = value;
		Il2CppCodeGenWriteBarrier((&___slotName_5), value);
	}

	inline static int32_t get_offset_of_pointAttachmentName_6() { return static_cast<int32_t>(offsetof(PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3, ___pointAttachmentName_6)); }
	inline String_t* get_pointAttachmentName_6() const { return ___pointAttachmentName_6; }
	inline String_t** get_address_of_pointAttachmentName_6() { return &___pointAttachmentName_6; }
	inline void set_pointAttachmentName_6(String_t* value)
	{
		___pointAttachmentName_6 = value;
		Il2CppCodeGenWriteBarrier((&___pointAttachmentName_6), value);
	}

	inline static int32_t get_offset_of_followRotation_7() { return static_cast<int32_t>(offsetof(PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3, ___followRotation_7)); }
	inline bool get_followRotation_7() const { return ___followRotation_7; }
	inline bool* get_address_of_followRotation_7() { return &___followRotation_7; }
	inline void set_followRotation_7(bool value)
	{
		___followRotation_7 = value;
	}

	inline static int32_t get_offset_of_followSkeletonFlip_8() { return static_cast<int32_t>(offsetof(PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3, ___followSkeletonFlip_8)); }
	inline bool get_followSkeletonFlip_8() const { return ___followSkeletonFlip_8; }
	inline bool* get_address_of_followSkeletonFlip_8() { return &___followSkeletonFlip_8; }
	inline void set_followSkeletonFlip_8(bool value)
	{
		___followSkeletonFlip_8 = value;
	}

	inline static int32_t get_offset_of_followSkeletonZPosition_9() { return static_cast<int32_t>(offsetof(PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3, ___followSkeletonZPosition_9)); }
	inline bool get_followSkeletonZPosition_9() const { return ___followSkeletonZPosition_9; }
	inline bool* get_address_of_followSkeletonZPosition_9() { return &___followSkeletonZPosition_9; }
	inline void set_followSkeletonZPosition_9(bool value)
	{
		___followSkeletonZPosition_9 = value;
	}

	inline static int32_t get_offset_of_skeletonTransform_10() { return static_cast<int32_t>(offsetof(PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3, ___skeletonTransform_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_skeletonTransform_10() const { return ___skeletonTransform_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_skeletonTransform_10() { return &___skeletonTransform_10; }
	inline void set_skeletonTransform_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___skeletonTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_10), value);
	}

	inline static int32_t get_offset_of_skeletonTransformIsParent_11() { return static_cast<int32_t>(offsetof(PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3, ___skeletonTransformIsParent_11)); }
	inline bool get_skeletonTransformIsParent_11() const { return ___skeletonTransformIsParent_11; }
	inline bool* get_address_of_skeletonTransformIsParent_11() { return &___skeletonTransformIsParent_11; }
	inline void set_skeletonTransformIsParent_11(bool value)
	{
		___skeletonTransformIsParent_11 = value;
	}

	inline static int32_t get_offset_of_point_12() { return static_cast<int32_t>(offsetof(PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3, ___point_12)); }
	inline PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B * get_point_12() const { return ___point_12; }
	inline PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B ** get_address_of_point_12() { return &___point_12; }
	inline void set_point_12(PointAttachment_t8F365CFDF058CDC5566E801DE1DAFE637F194D1B * value)
	{
		___point_12 = value;
		Il2CppCodeGenWriteBarrier((&___point_12), value);
	}

	inline static int32_t get_offset_of_bone_13() { return static_cast<int32_t>(offsetof(PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3, ___bone_13)); }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * get_bone_13() const { return ___bone_13; }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E ** get_address_of_bone_13() { return &___bone_13; }
	inline void set_bone_13(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * value)
	{
		___bone_13 = value;
		Il2CppCodeGenWriteBarrier((&___bone_13), value);
	}

	inline static int32_t get_offset_of_valid_14() { return static_cast<int32_t>(offsetof(PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3, ___valid_14)); }
	inline bool get_valid_14() const { return ___valid_14; }
	inline bool* get_address_of_valid_14() { return &___valid_14; }
	inline void set_valid_14(bool value)
	{
		___valid_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTFOLLOWER_TADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3_H
#ifndef SKELETONRENDERER_T2966DEE83F2476605CF48837468900CB93AA7F03_H
#define SKELETONRENDERER_T2966DEE83F2476605CF48837468900CB93AA7F03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer
struct  SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate Spine.Unity.SkeletonRenderer::OnRebuild
	SkeletonRendererDelegate_tF51A39DB9B998C96CEEE3C8F0F55FB65070EBBF7 * ___OnRebuild_4;
	// Spine.Unity.MeshGeneratorDelegate Spine.Unity.SkeletonRenderer::OnPostProcessVertices
	MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4 * ___OnPostProcessVertices_5;
	// Spine.Unity.SkeletonDataAsset Spine.Unity.SkeletonRenderer::skeletonDataAsset
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * ___skeletonDataAsset_6;
	// System.String Spine.Unity.SkeletonRenderer::initialSkinName
	String_t* ___initialSkinName_7;
	// System.Boolean Spine.Unity.SkeletonRenderer::initialFlipX
	bool ___initialFlipX_8;
	// System.Boolean Spine.Unity.SkeletonRenderer::initialFlipY
	bool ___initialFlipY_9;
	// System.String[] Spine.Unity.SkeletonRenderer::separatorSlotNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___separatorSlotNames_10;
	// System.Collections.Generic.List`1<Spine.Slot> Spine.Unity.SkeletonRenderer::separatorSlots
	List_1_t92152F4927754904CFC1D788B0E4A24816D37E1A * ___separatorSlots_11;
	// System.Single Spine.Unity.SkeletonRenderer::zSpacing
	float ___zSpacing_12;
	// System.Boolean Spine.Unity.SkeletonRenderer::useClipping
	bool ___useClipping_13;
	// System.Boolean Spine.Unity.SkeletonRenderer::immutableTriangles
	bool ___immutableTriangles_14;
	// System.Boolean Spine.Unity.SkeletonRenderer::pmaVertexColors
	bool ___pmaVertexColors_15;
	// System.Boolean Spine.Unity.SkeletonRenderer::clearStateOnDisable
	bool ___clearStateOnDisable_16;
	// System.Boolean Spine.Unity.SkeletonRenderer::tintBlack
	bool ___tintBlack_17;
	// System.Boolean Spine.Unity.SkeletonRenderer::singleSubmesh
	bool ___singleSubmesh_18;
	// System.Boolean Spine.Unity.SkeletonRenderer::addNormals
	bool ___addNormals_19;
	// System.Boolean Spine.Unity.SkeletonRenderer::calculateTangents
	bool ___calculateTangents_20;
	// System.Boolean Spine.Unity.SkeletonRenderer::logErrors
	bool ___logErrors_21;
	// System.Boolean Spine.Unity.SkeletonRenderer::disableRenderingOnOverride
	bool ___disableRenderingOnOverride_22;
	// Spine.Unity.SkeletonRenderer/InstructionDelegate Spine.Unity.SkeletonRenderer::generateMeshOverride
	InstructionDelegate_t5D855F63CA53EA174005317091099D0BAA478F54 * ___generateMeshOverride_23;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material> Spine.Unity.SkeletonRenderer::customMaterialOverride
	Dictionary_2_t87B6F3EB6208E12A08960AA208E68480EA2C7926 * ___customMaterialOverride_24;
	// System.Collections.Generic.Dictionary`2<Spine.Slot,UnityEngine.Material> Spine.Unity.SkeletonRenderer::customSlotMaterials
	Dictionary_2_t162335B8709163DD53742586B4CCB3D1D7F824B1 * ___customSlotMaterials_25;
	// UnityEngine.MeshRenderer Spine.Unity.SkeletonRenderer::meshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___meshRenderer_26;
	// UnityEngine.MeshFilter Spine.Unity.SkeletonRenderer::meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___meshFilter_27;
	// System.Boolean Spine.Unity.SkeletonRenderer::valid
	bool ___valid_28;
	// Spine.Skeleton Spine.Unity.SkeletonRenderer::skeleton
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * ___skeleton_29;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.SkeletonRenderer::currentInstructions
	SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * ___currentInstructions_30;
	// Spine.Unity.MeshGenerator Spine.Unity.SkeletonRenderer::meshGenerator
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * ___meshGenerator_31;
	// Spine.Unity.MeshRendererBuffers Spine.Unity.SkeletonRenderer::rendererBuffers
	MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107 * ___rendererBuffers_32;

public:
	inline static int32_t get_offset_of_OnRebuild_4() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___OnRebuild_4)); }
	inline SkeletonRendererDelegate_tF51A39DB9B998C96CEEE3C8F0F55FB65070EBBF7 * get_OnRebuild_4() const { return ___OnRebuild_4; }
	inline SkeletonRendererDelegate_tF51A39DB9B998C96CEEE3C8F0F55FB65070EBBF7 ** get_address_of_OnRebuild_4() { return &___OnRebuild_4; }
	inline void set_OnRebuild_4(SkeletonRendererDelegate_tF51A39DB9B998C96CEEE3C8F0F55FB65070EBBF7 * value)
	{
		___OnRebuild_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnRebuild_4), value);
	}

	inline static int32_t get_offset_of_OnPostProcessVertices_5() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___OnPostProcessVertices_5)); }
	inline MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4 * get_OnPostProcessVertices_5() const { return ___OnPostProcessVertices_5; }
	inline MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4 ** get_address_of_OnPostProcessVertices_5() { return &___OnPostProcessVertices_5; }
	inline void set_OnPostProcessVertices_5(MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4 * value)
	{
		___OnPostProcessVertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostProcessVertices_5), value);
	}

	inline static int32_t get_offset_of_skeletonDataAsset_6() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___skeletonDataAsset_6)); }
	inline SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * get_skeletonDataAsset_6() const { return ___skeletonDataAsset_6; }
	inline SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 ** get_address_of_skeletonDataAsset_6() { return &___skeletonDataAsset_6; }
	inline void set_skeletonDataAsset_6(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * value)
	{
		___skeletonDataAsset_6 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_6), value);
	}

	inline static int32_t get_offset_of_initialSkinName_7() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___initialSkinName_7)); }
	inline String_t* get_initialSkinName_7() const { return ___initialSkinName_7; }
	inline String_t** get_address_of_initialSkinName_7() { return &___initialSkinName_7; }
	inline void set_initialSkinName_7(String_t* value)
	{
		___initialSkinName_7 = value;
		Il2CppCodeGenWriteBarrier((&___initialSkinName_7), value);
	}

	inline static int32_t get_offset_of_initialFlipX_8() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___initialFlipX_8)); }
	inline bool get_initialFlipX_8() const { return ___initialFlipX_8; }
	inline bool* get_address_of_initialFlipX_8() { return &___initialFlipX_8; }
	inline void set_initialFlipX_8(bool value)
	{
		___initialFlipX_8 = value;
	}

	inline static int32_t get_offset_of_initialFlipY_9() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___initialFlipY_9)); }
	inline bool get_initialFlipY_9() const { return ___initialFlipY_9; }
	inline bool* get_address_of_initialFlipY_9() { return &___initialFlipY_9; }
	inline void set_initialFlipY_9(bool value)
	{
		___initialFlipY_9 = value;
	}

	inline static int32_t get_offset_of_separatorSlotNames_10() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___separatorSlotNames_10)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_separatorSlotNames_10() const { return ___separatorSlotNames_10; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_separatorSlotNames_10() { return &___separatorSlotNames_10; }
	inline void set_separatorSlotNames_10(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___separatorSlotNames_10 = value;
		Il2CppCodeGenWriteBarrier((&___separatorSlotNames_10), value);
	}

	inline static int32_t get_offset_of_separatorSlots_11() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___separatorSlots_11)); }
	inline List_1_t92152F4927754904CFC1D788B0E4A24816D37E1A * get_separatorSlots_11() const { return ___separatorSlots_11; }
	inline List_1_t92152F4927754904CFC1D788B0E4A24816D37E1A ** get_address_of_separatorSlots_11() { return &___separatorSlots_11; }
	inline void set_separatorSlots_11(List_1_t92152F4927754904CFC1D788B0E4A24816D37E1A * value)
	{
		___separatorSlots_11 = value;
		Il2CppCodeGenWriteBarrier((&___separatorSlots_11), value);
	}

	inline static int32_t get_offset_of_zSpacing_12() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___zSpacing_12)); }
	inline float get_zSpacing_12() const { return ___zSpacing_12; }
	inline float* get_address_of_zSpacing_12() { return &___zSpacing_12; }
	inline void set_zSpacing_12(float value)
	{
		___zSpacing_12 = value;
	}

	inline static int32_t get_offset_of_useClipping_13() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___useClipping_13)); }
	inline bool get_useClipping_13() const { return ___useClipping_13; }
	inline bool* get_address_of_useClipping_13() { return &___useClipping_13; }
	inline void set_useClipping_13(bool value)
	{
		___useClipping_13 = value;
	}

	inline static int32_t get_offset_of_immutableTriangles_14() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___immutableTriangles_14)); }
	inline bool get_immutableTriangles_14() const { return ___immutableTriangles_14; }
	inline bool* get_address_of_immutableTriangles_14() { return &___immutableTriangles_14; }
	inline void set_immutableTriangles_14(bool value)
	{
		___immutableTriangles_14 = value;
	}

	inline static int32_t get_offset_of_pmaVertexColors_15() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___pmaVertexColors_15)); }
	inline bool get_pmaVertexColors_15() const { return ___pmaVertexColors_15; }
	inline bool* get_address_of_pmaVertexColors_15() { return &___pmaVertexColors_15; }
	inline void set_pmaVertexColors_15(bool value)
	{
		___pmaVertexColors_15 = value;
	}

	inline static int32_t get_offset_of_clearStateOnDisable_16() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___clearStateOnDisable_16)); }
	inline bool get_clearStateOnDisable_16() const { return ___clearStateOnDisable_16; }
	inline bool* get_address_of_clearStateOnDisable_16() { return &___clearStateOnDisable_16; }
	inline void set_clearStateOnDisable_16(bool value)
	{
		___clearStateOnDisable_16 = value;
	}

	inline static int32_t get_offset_of_tintBlack_17() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___tintBlack_17)); }
	inline bool get_tintBlack_17() const { return ___tintBlack_17; }
	inline bool* get_address_of_tintBlack_17() { return &___tintBlack_17; }
	inline void set_tintBlack_17(bool value)
	{
		___tintBlack_17 = value;
	}

	inline static int32_t get_offset_of_singleSubmesh_18() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___singleSubmesh_18)); }
	inline bool get_singleSubmesh_18() const { return ___singleSubmesh_18; }
	inline bool* get_address_of_singleSubmesh_18() { return &___singleSubmesh_18; }
	inline void set_singleSubmesh_18(bool value)
	{
		___singleSubmesh_18 = value;
	}

	inline static int32_t get_offset_of_addNormals_19() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___addNormals_19)); }
	inline bool get_addNormals_19() const { return ___addNormals_19; }
	inline bool* get_address_of_addNormals_19() { return &___addNormals_19; }
	inline void set_addNormals_19(bool value)
	{
		___addNormals_19 = value;
	}

	inline static int32_t get_offset_of_calculateTangents_20() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___calculateTangents_20)); }
	inline bool get_calculateTangents_20() const { return ___calculateTangents_20; }
	inline bool* get_address_of_calculateTangents_20() { return &___calculateTangents_20; }
	inline void set_calculateTangents_20(bool value)
	{
		___calculateTangents_20 = value;
	}

	inline static int32_t get_offset_of_logErrors_21() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___logErrors_21)); }
	inline bool get_logErrors_21() const { return ___logErrors_21; }
	inline bool* get_address_of_logErrors_21() { return &___logErrors_21; }
	inline void set_logErrors_21(bool value)
	{
		___logErrors_21 = value;
	}

	inline static int32_t get_offset_of_disableRenderingOnOverride_22() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___disableRenderingOnOverride_22)); }
	inline bool get_disableRenderingOnOverride_22() const { return ___disableRenderingOnOverride_22; }
	inline bool* get_address_of_disableRenderingOnOverride_22() { return &___disableRenderingOnOverride_22; }
	inline void set_disableRenderingOnOverride_22(bool value)
	{
		___disableRenderingOnOverride_22 = value;
	}

	inline static int32_t get_offset_of_generateMeshOverride_23() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___generateMeshOverride_23)); }
	inline InstructionDelegate_t5D855F63CA53EA174005317091099D0BAA478F54 * get_generateMeshOverride_23() const { return ___generateMeshOverride_23; }
	inline InstructionDelegate_t5D855F63CA53EA174005317091099D0BAA478F54 ** get_address_of_generateMeshOverride_23() { return &___generateMeshOverride_23; }
	inline void set_generateMeshOverride_23(InstructionDelegate_t5D855F63CA53EA174005317091099D0BAA478F54 * value)
	{
		___generateMeshOverride_23 = value;
		Il2CppCodeGenWriteBarrier((&___generateMeshOverride_23), value);
	}

	inline static int32_t get_offset_of_customMaterialOverride_24() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___customMaterialOverride_24)); }
	inline Dictionary_2_t87B6F3EB6208E12A08960AA208E68480EA2C7926 * get_customMaterialOverride_24() const { return ___customMaterialOverride_24; }
	inline Dictionary_2_t87B6F3EB6208E12A08960AA208E68480EA2C7926 ** get_address_of_customMaterialOverride_24() { return &___customMaterialOverride_24; }
	inline void set_customMaterialOverride_24(Dictionary_2_t87B6F3EB6208E12A08960AA208E68480EA2C7926 * value)
	{
		___customMaterialOverride_24 = value;
		Il2CppCodeGenWriteBarrier((&___customMaterialOverride_24), value);
	}

	inline static int32_t get_offset_of_customSlotMaterials_25() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___customSlotMaterials_25)); }
	inline Dictionary_2_t162335B8709163DD53742586B4CCB3D1D7F824B1 * get_customSlotMaterials_25() const { return ___customSlotMaterials_25; }
	inline Dictionary_2_t162335B8709163DD53742586B4CCB3D1D7F824B1 ** get_address_of_customSlotMaterials_25() { return &___customSlotMaterials_25; }
	inline void set_customSlotMaterials_25(Dictionary_2_t162335B8709163DD53742586B4CCB3D1D7F824B1 * value)
	{
		___customSlotMaterials_25 = value;
		Il2CppCodeGenWriteBarrier((&___customSlotMaterials_25), value);
	}

	inline static int32_t get_offset_of_meshRenderer_26() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___meshRenderer_26)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_meshRenderer_26() const { return ___meshRenderer_26; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_meshRenderer_26() { return &___meshRenderer_26; }
	inline void set_meshRenderer_26(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___meshRenderer_26 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_26), value);
	}

	inline static int32_t get_offset_of_meshFilter_27() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___meshFilter_27)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_meshFilter_27() const { return ___meshFilter_27; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_meshFilter_27() { return &___meshFilter_27; }
	inline void set_meshFilter_27(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___meshFilter_27 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_27), value);
	}

	inline static int32_t get_offset_of_valid_28() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___valid_28)); }
	inline bool get_valid_28() const { return ___valid_28; }
	inline bool* get_address_of_valid_28() { return &___valid_28; }
	inline void set_valid_28(bool value)
	{
		___valid_28 = value;
	}

	inline static int32_t get_offset_of_skeleton_29() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___skeleton_29)); }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * get_skeleton_29() const { return ___skeleton_29; }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 ** get_address_of_skeleton_29() { return &___skeleton_29; }
	inline void set_skeleton_29(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * value)
	{
		___skeleton_29 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_29), value);
	}

	inline static int32_t get_offset_of_currentInstructions_30() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___currentInstructions_30)); }
	inline SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * get_currentInstructions_30() const { return ___currentInstructions_30; }
	inline SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A ** get_address_of_currentInstructions_30() { return &___currentInstructions_30; }
	inline void set_currentInstructions_30(SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * value)
	{
		___currentInstructions_30 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_30), value);
	}

	inline static int32_t get_offset_of_meshGenerator_31() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___meshGenerator_31)); }
	inline MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * get_meshGenerator_31() const { return ___meshGenerator_31; }
	inline MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 ** get_address_of_meshGenerator_31() { return &___meshGenerator_31; }
	inline void set_meshGenerator_31(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * value)
	{
		___meshGenerator_31 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_31), value);
	}

	inline static int32_t get_offset_of_rendererBuffers_32() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03, ___rendererBuffers_32)); }
	inline MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107 * get_rendererBuffers_32() const { return ___rendererBuffers_32; }
	inline MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107 ** get_address_of_rendererBuffers_32() { return &___rendererBuffers_32; }
	inline void set_rendererBuffers_32(MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107 * value)
	{
		___rendererBuffers_32 = value;
		Il2CppCodeGenWriteBarrier((&___rendererBuffers_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERER_T2966DEE83F2476605CF48837468900CB93AA7F03_H
#ifndef SKELETONUTILITY_TDCD74FBD80AFA061668B110AEFADF4FE5929C9B0_H
#define SKELETONUTILITY_TDCD74FBD80AFA061668B110AEFADF4FE5929C9B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtility
struct  SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate Spine.Unity.SkeletonUtility::OnReset
	SkeletonUtilityDelegate_t40A11BA492FE4C516EBAF8D63834116D5865092B * ___OnReset_4;
	// UnityEngine.Transform Spine.Unity.SkeletonUtility::boneRoot
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___boneRoot_5;
	// Spine.Unity.SkeletonRenderer Spine.Unity.SkeletonUtility::skeletonRenderer
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * ___skeletonRenderer_6;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.SkeletonUtility::skeletonAnimation
	RuntimeObject* ___skeletonAnimation_7;
	// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityBone> Spine.Unity.SkeletonUtility::utilityBones
	List_1_t4B4918D0C123A9AB1B8B4BEBD5E6D7932B435CBB * ___utilityBones_8;
	// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityConstraint> Spine.Unity.SkeletonUtility::utilityConstraints
	List_1_tFF497566FC0F9D6C9AC5D51FFB9904C090AACEEB * ___utilityConstraints_9;
	// System.Boolean Spine.Unity.SkeletonUtility::hasTransformBones
	bool ___hasTransformBones_10;
	// System.Boolean Spine.Unity.SkeletonUtility::hasUtilityConstraints
	bool ___hasUtilityConstraints_11;
	// System.Boolean Spine.Unity.SkeletonUtility::needToReprocessBones
	bool ___needToReprocessBones_12;

public:
	inline static int32_t get_offset_of_OnReset_4() { return static_cast<int32_t>(offsetof(SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0, ___OnReset_4)); }
	inline SkeletonUtilityDelegate_t40A11BA492FE4C516EBAF8D63834116D5865092B * get_OnReset_4() const { return ___OnReset_4; }
	inline SkeletonUtilityDelegate_t40A11BA492FE4C516EBAF8D63834116D5865092B ** get_address_of_OnReset_4() { return &___OnReset_4; }
	inline void set_OnReset_4(SkeletonUtilityDelegate_t40A11BA492FE4C516EBAF8D63834116D5865092B * value)
	{
		___OnReset_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnReset_4), value);
	}

	inline static int32_t get_offset_of_boneRoot_5() { return static_cast<int32_t>(offsetof(SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0, ___boneRoot_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_boneRoot_5() const { return ___boneRoot_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_boneRoot_5() { return &___boneRoot_5; }
	inline void set_boneRoot_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___boneRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___boneRoot_5), value);
	}

	inline static int32_t get_offset_of_skeletonRenderer_6() { return static_cast<int32_t>(offsetof(SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0, ___skeletonRenderer_6)); }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * get_skeletonRenderer_6() const { return ___skeletonRenderer_6; }
	inline SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 ** get_address_of_skeletonRenderer_6() { return &___skeletonRenderer_6; }
	inline void set_skeletonRenderer_6(SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03 * value)
	{
		___skeletonRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_6), value);
	}

	inline static int32_t get_offset_of_skeletonAnimation_7() { return static_cast<int32_t>(offsetof(SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0, ___skeletonAnimation_7)); }
	inline RuntimeObject* get_skeletonAnimation_7() const { return ___skeletonAnimation_7; }
	inline RuntimeObject** get_address_of_skeletonAnimation_7() { return &___skeletonAnimation_7; }
	inline void set_skeletonAnimation_7(RuntimeObject* value)
	{
		___skeletonAnimation_7 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonAnimation_7), value);
	}

	inline static int32_t get_offset_of_utilityBones_8() { return static_cast<int32_t>(offsetof(SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0, ___utilityBones_8)); }
	inline List_1_t4B4918D0C123A9AB1B8B4BEBD5E6D7932B435CBB * get_utilityBones_8() const { return ___utilityBones_8; }
	inline List_1_t4B4918D0C123A9AB1B8B4BEBD5E6D7932B435CBB ** get_address_of_utilityBones_8() { return &___utilityBones_8; }
	inline void set_utilityBones_8(List_1_t4B4918D0C123A9AB1B8B4BEBD5E6D7932B435CBB * value)
	{
		___utilityBones_8 = value;
		Il2CppCodeGenWriteBarrier((&___utilityBones_8), value);
	}

	inline static int32_t get_offset_of_utilityConstraints_9() { return static_cast<int32_t>(offsetof(SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0, ___utilityConstraints_9)); }
	inline List_1_tFF497566FC0F9D6C9AC5D51FFB9904C090AACEEB * get_utilityConstraints_9() const { return ___utilityConstraints_9; }
	inline List_1_tFF497566FC0F9D6C9AC5D51FFB9904C090AACEEB ** get_address_of_utilityConstraints_9() { return &___utilityConstraints_9; }
	inline void set_utilityConstraints_9(List_1_tFF497566FC0F9D6C9AC5D51FFB9904C090AACEEB * value)
	{
		___utilityConstraints_9 = value;
		Il2CppCodeGenWriteBarrier((&___utilityConstraints_9), value);
	}

	inline static int32_t get_offset_of_hasTransformBones_10() { return static_cast<int32_t>(offsetof(SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0, ___hasTransformBones_10)); }
	inline bool get_hasTransformBones_10() const { return ___hasTransformBones_10; }
	inline bool* get_address_of_hasTransformBones_10() { return &___hasTransformBones_10; }
	inline void set_hasTransformBones_10(bool value)
	{
		___hasTransformBones_10 = value;
	}

	inline static int32_t get_offset_of_hasUtilityConstraints_11() { return static_cast<int32_t>(offsetof(SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0, ___hasUtilityConstraints_11)); }
	inline bool get_hasUtilityConstraints_11() const { return ___hasUtilityConstraints_11; }
	inline bool* get_address_of_hasUtilityConstraints_11() { return &___hasUtilityConstraints_11; }
	inline void set_hasUtilityConstraints_11(bool value)
	{
		___hasUtilityConstraints_11 = value;
	}

	inline static int32_t get_offset_of_needToReprocessBones_12() { return static_cast<int32_t>(offsetof(SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0, ___needToReprocessBones_12)); }
	inline bool get_needToReprocessBones_12() const { return ___needToReprocessBones_12; }
	inline bool* get_address_of_needToReprocessBones_12() { return &___needToReprocessBones_12; }
	inline void set_needToReprocessBones_12(bool value)
	{
		___needToReprocessBones_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITY_TDCD74FBD80AFA061668B110AEFADF4FE5929C9B0_H
#ifndef SKELETONUTILITYBONE_T0C75ECC56D7800633F25C86F36D4915DD9934D39_H
#define SKELETONUTILITYBONE_T0C75ECC56D7800633F25C86F36D4915DD9934D39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityBone
struct  SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Spine.Unity.SkeletonUtilityBone::boneName
	String_t* ___boneName_4;
	// UnityEngine.Transform Spine.Unity.SkeletonUtilityBone::parentReference
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parentReference_5;
	// Spine.Unity.SkeletonUtilityBone/Mode Spine.Unity.SkeletonUtilityBone::mode
	int32_t ___mode_6;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::position
	bool ___position_7;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::rotation
	bool ___rotation_8;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::scale
	bool ___scale_9;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::zPosition
	bool ___zPosition_10;
	// System.Single Spine.Unity.SkeletonUtilityBone::overrideAlpha
	float ___overrideAlpha_11;
	// Spine.Unity.SkeletonUtility Spine.Unity.SkeletonUtilityBone::skeletonUtility
	SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0 * ___skeletonUtility_12;
	// Spine.Bone Spine.Unity.SkeletonUtilityBone::bone
	Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * ___bone_13;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::transformLerpComplete
	bool ___transformLerpComplete_14;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::valid
	bool ___valid_15;
	// UnityEngine.Transform Spine.Unity.SkeletonUtilityBone::cachedTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cachedTransform_16;
	// UnityEngine.Transform Spine.Unity.SkeletonUtilityBone::skeletonTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___skeletonTransform_17;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::incompatibleTransformMode
	bool ___incompatibleTransformMode_18;

public:
	inline static int32_t get_offset_of_boneName_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___boneName_4)); }
	inline String_t* get_boneName_4() const { return ___boneName_4; }
	inline String_t** get_address_of_boneName_4() { return &___boneName_4; }
	inline void set_boneName_4(String_t* value)
	{
		___boneName_4 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_4), value);
	}

	inline static int32_t get_offset_of_parentReference_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___parentReference_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_parentReference_5() const { return ___parentReference_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_parentReference_5() { return &___parentReference_5; }
	inline void set_parentReference_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___parentReference_5 = value;
		Il2CppCodeGenWriteBarrier((&___parentReference_5), value);
	}

	inline static int32_t get_offset_of_mode_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___mode_6)); }
	inline int32_t get_mode_6() const { return ___mode_6; }
	inline int32_t* get_address_of_mode_6() { return &___mode_6; }
	inline void set_mode_6(int32_t value)
	{
		___mode_6 = value;
	}

	inline static int32_t get_offset_of_position_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___position_7)); }
	inline bool get_position_7() const { return ___position_7; }
	inline bool* get_address_of_position_7() { return &___position_7; }
	inline void set_position_7(bool value)
	{
		___position_7 = value;
	}

	inline static int32_t get_offset_of_rotation_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___rotation_8)); }
	inline bool get_rotation_8() const { return ___rotation_8; }
	inline bool* get_address_of_rotation_8() { return &___rotation_8; }
	inline void set_rotation_8(bool value)
	{
		___rotation_8 = value;
	}

	inline static int32_t get_offset_of_scale_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___scale_9)); }
	inline bool get_scale_9() const { return ___scale_9; }
	inline bool* get_address_of_scale_9() { return &___scale_9; }
	inline void set_scale_9(bool value)
	{
		___scale_9 = value;
	}

	inline static int32_t get_offset_of_zPosition_10() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___zPosition_10)); }
	inline bool get_zPosition_10() const { return ___zPosition_10; }
	inline bool* get_address_of_zPosition_10() { return &___zPosition_10; }
	inline void set_zPosition_10(bool value)
	{
		___zPosition_10 = value;
	}

	inline static int32_t get_offset_of_overrideAlpha_11() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___overrideAlpha_11)); }
	inline float get_overrideAlpha_11() const { return ___overrideAlpha_11; }
	inline float* get_address_of_overrideAlpha_11() { return &___overrideAlpha_11; }
	inline void set_overrideAlpha_11(float value)
	{
		___overrideAlpha_11 = value;
	}

	inline static int32_t get_offset_of_skeletonUtility_12() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___skeletonUtility_12)); }
	inline SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0 * get_skeletonUtility_12() const { return ___skeletonUtility_12; }
	inline SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0 ** get_address_of_skeletonUtility_12() { return &___skeletonUtility_12; }
	inline void set_skeletonUtility_12(SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0 * value)
	{
		___skeletonUtility_12 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonUtility_12), value);
	}

	inline static int32_t get_offset_of_bone_13() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___bone_13)); }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * get_bone_13() const { return ___bone_13; }
	inline Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E ** get_address_of_bone_13() { return &___bone_13; }
	inline void set_bone_13(Bone_t97C936A57AA46DB1B135D3853C7223DE467A799E * value)
	{
		___bone_13 = value;
		Il2CppCodeGenWriteBarrier((&___bone_13), value);
	}

	inline static int32_t get_offset_of_transformLerpComplete_14() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___transformLerpComplete_14)); }
	inline bool get_transformLerpComplete_14() const { return ___transformLerpComplete_14; }
	inline bool* get_address_of_transformLerpComplete_14() { return &___transformLerpComplete_14; }
	inline void set_transformLerpComplete_14(bool value)
	{
		___transformLerpComplete_14 = value;
	}

	inline static int32_t get_offset_of_valid_15() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___valid_15)); }
	inline bool get_valid_15() const { return ___valid_15; }
	inline bool* get_address_of_valid_15() { return &___valid_15; }
	inline void set_valid_15(bool value)
	{
		___valid_15 = value;
	}

	inline static int32_t get_offset_of_cachedTransform_16() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___cachedTransform_16)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_cachedTransform_16() const { return ___cachedTransform_16; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_cachedTransform_16() { return &___cachedTransform_16; }
	inline void set_cachedTransform_16(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___cachedTransform_16 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_16), value);
	}

	inline static int32_t get_offset_of_skeletonTransform_17() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___skeletonTransform_17)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_skeletonTransform_17() const { return ___skeletonTransform_17; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_skeletonTransform_17() { return &___skeletonTransform_17; }
	inline void set_skeletonTransform_17(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___skeletonTransform_17 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_17), value);
	}

	inline static int32_t get_offset_of_incompatibleTransformMode_18() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39, ___incompatibleTransformMode_18)); }
	inline bool get_incompatibleTransformMode_18() const { return ___incompatibleTransformMode_18; }
	inline bool* get_address_of_incompatibleTransformMode_18() { return &___incompatibleTransformMode_18; }
	inline void set_incompatibleTransformMode_18(bool value)
	{
		___incompatibleTransformMode_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYBONE_T0C75ECC56D7800633F25C86F36D4915DD9934D39_H
#ifndef SKELETONUTILITYCONSTRAINT_TC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0_H
#define SKELETONUTILITYCONSTRAINT_TC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityConstraint
struct  SkeletonUtilityConstraint_tC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Spine.Unity.SkeletonUtilityBone Spine.Unity.SkeletonUtilityConstraint::utilBone
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39 * ___utilBone_4;
	// Spine.Unity.SkeletonUtility Spine.Unity.SkeletonUtilityConstraint::skeletonUtility
	SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0 * ___skeletonUtility_5;

public:
	inline static int32_t get_offset_of_utilBone_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityConstraint_tC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0, ___utilBone_4)); }
	inline SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39 * get_utilBone_4() const { return ___utilBone_4; }
	inline SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39 ** get_address_of_utilBone_4() { return &___utilBone_4; }
	inline void set_utilBone_4(SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39 * value)
	{
		___utilBone_4 = value;
		Il2CppCodeGenWriteBarrier((&___utilBone_4), value);
	}

	inline static int32_t get_offset_of_skeletonUtility_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityConstraint_tC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0, ___skeletonUtility_5)); }
	inline SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0 * get_skeletonUtility_5() const { return ___skeletonUtility_5; }
	inline SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0 ** get_address_of_skeletonUtility_5() { return &___skeletonUtility_5; }
	inline void set_skeletonUtility_5(SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0 * value)
	{
		___skeletonUtility_5 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonUtility_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYCONSTRAINT_TC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef SKELETONANIMATIONPLAYABLEHANDLE_T791E2A077EA0F1F27D928E1272890E82F59E2113_H
#define SKELETONANIMATIONPLAYABLEHANDLE_T791E2A077EA0F1F27D928E1272890E82F59E2113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Playables.SkeletonAnimationPlayableHandle
struct  SkeletonAnimationPlayableHandle_t791E2A077EA0F1F27D928E1272890E82F59E2113  : public SpinePlayableHandleBase_tB5F200A1F15E5F638384F2B4B26C076C4E4810AC
{
public:
	// Spine.Unity.SkeletonAnimation Spine.Unity.Playables.SkeletonAnimationPlayableHandle::skeletonAnimation
	SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D * ___skeletonAnimation_5;

public:
	inline static int32_t get_offset_of_skeletonAnimation_5() { return static_cast<int32_t>(offsetof(SkeletonAnimationPlayableHandle_t791E2A077EA0F1F27D928E1272890E82F59E2113, ___skeletonAnimation_5)); }
	inline SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D * get_skeletonAnimation_5() const { return ___skeletonAnimation_5; }
	inline SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D ** get_address_of_skeletonAnimation_5() { return &___skeletonAnimation_5; }
	inline void set_skeletonAnimation_5(SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D * value)
	{
		___skeletonAnimation_5 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonAnimation_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONANIMATIONPLAYABLEHANDLE_T791E2A077EA0F1F27D928E1272890E82F59E2113_H
#ifndef SKELETONANIMATION_T24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D_H
#define SKELETONANIMATION_T24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimation
struct  SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D  : public SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03
{
public:
	// Spine.AnimationState Spine.Unity.SkeletonAnimation::state
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * ___state_33;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateLocal
	UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * ____UpdateLocal_34;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateWorld
	UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * ____UpdateWorld_35;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateComplete
	UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * ____UpdateComplete_36;
	// System.String Spine.Unity.SkeletonAnimation::_animationName
	String_t* ____animationName_37;
	// System.Boolean Spine.Unity.SkeletonAnimation::loop
	bool ___loop_38;
	// System.Single Spine.Unity.SkeletonAnimation::timeScale
	float ___timeScale_39;

public:
	inline static int32_t get_offset_of_state_33() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D, ___state_33)); }
	inline AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * get_state_33() const { return ___state_33; }
	inline AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 ** get_address_of_state_33() { return &___state_33; }
	inline void set_state_33(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * value)
	{
		___state_33 = value;
		Il2CppCodeGenWriteBarrier((&___state_33), value);
	}

	inline static int32_t get_offset_of__UpdateLocal_34() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D, ____UpdateLocal_34)); }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * get__UpdateLocal_34() const { return ____UpdateLocal_34; }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 ** get_address_of__UpdateLocal_34() { return &____UpdateLocal_34; }
	inline void set__UpdateLocal_34(UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * value)
	{
		____UpdateLocal_34 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateLocal_34), value);
	}

	inline static int32_t get_offset_of__UpdateWorld_35() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D, ____UpdateWorld_35)); }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * get__UpdateWorld_35() const { return ____UpdateWorld_35; }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 ** get_address_of__UpdateWorld_35() { return &____UpdateWorld_35; }
	inline void set__UpdateWorld_35(UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * value)
	{
		____UpdateWorld_35 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateWorld_35), value);
	}

	inline static int32_t get_offset_of__UpdateComplete_36() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D, ____UpdateComplete_36)); }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * get__UpdateComplete_36() const { return ____UpdateComplete_36; }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 ** get_address_of__UpdateComplete_36() { return &____UpdateComplete_36; }
	inline void set__UpdateComplete_36(UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * value)
	{
		____UpdateComplete_36 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateComplete_36), value);
	}

	inline static int32_t get_offset_of__animationName_37() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D, ____animationName_37)); }
	inline String_t* get__animationName_37() const { return ____animationName_37; }
	inline String_t** get_address_of__animationName_37() { return &____animationName_37; }
	inline void set__animationName_37(String_t* value)
	{
		____animationName_37 = value;
		Il2CppCodeGenWriteBarrier((&____animationName_37), value);
	}

	inline static int32_t get_offset_of_loop_38() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D, ___loop_38)); }
	inline bool get_loop_38() const { return ___loop_38; }
	inline bool* get_address_of_loop_38() { return &___loop_38; }
	inline void set_loop_38(bool value)
	{
		___loop_38 = value;
	}

	inline static int32_t get_offset_of_timeScale_39() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D, ___timeScale_39)); }
	inline float get_timeScale_39() const { return ___timeScale_39; }
	inline float* get_address_of_timeScale_39() { return &___timeScale_39; }
	inline void set_timeScale_39(float value)
	{
		___timeScale_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONANIMATION_T24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D_H
#ifndef SKELETONANIMATOR_T8A6205140214563D0BA33F43CB9A6989D9AA1DFD_H
#define SKELETONANIMATOR_T8A6205140214563D0BA33F43CB9A6989D9AA1DFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator
struct  SkeletonAnimator_t8A6205140214563D0BA33F43CB9A6989D9AA1DFD  : public SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03
{
public:
	// Spine.Unity.SkeletonAnimator/MecanimTranslator Spine.Unity.SkeletonAnimator::translator
	MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1 * ___translator_33;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimator::_UpdateLocal
	UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * ____UpdateLocal_34;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimator::_UpdateWorld
	UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * ____UpdateWorld_35;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimator::_UpdateComplete
	UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * ____UpdateComplete_36;

public:
	inline static int32_t get_offset_of_translator_33() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t8A6205140214563D0BA33F43CB9A6989D9AA1DFD, ___translator_33)); }
	inline MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1 * get_translator_33() const { return ___translator_33; }
	inline MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1 ** get_address_of_translator_33() { return &___translator_33; }
	inline void set_translator_33(MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1 * value)
	{
		___translator_33 = value;
		Il2CppCodeGenWriteBarrier((&___translator_33), value);
	}

	inline static int32_t get_offset_of__UpdateLocal_34() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t8A6205140214563D0BA33F43CB9A6989D9AA1DFD, ____UpdateLocal_34)); }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * get__UpdateLocal_34() const { return ____UpdateLocal_34; }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 ** get_address_of__UpdateLocal_34() { return &____UpdateLocal_34; }
	inline void set__UpdateLocal_34(UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * value)
	{
		____UpdateLocal_34 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateLocal_34), value);
	}

	inline static int32_t get_offset_of__UpdateWorld_35() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t8A6205140214563D0BA33F43CB9A6989D9AA1DFD, ____UpdateWorld_35)); }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * get__UpdateWorld_35() const { return ____UpdateWorld_35; }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 ** get_address_of__UpdateWorld_35() { return &____UpdateWorld_35; }
	inline void set__UpdateWorld_35(UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * value)
	{
		____UpdateWorld_35 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateWorld_35), value);
	}

	inline static int32_t get_offset_of__UpdateComplete_36() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t8A6205140214563D0BA33F43CB9A6989D9AA1DFD, ____UpdateComplete_36)); }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * get__UpdateComplete_36() const { return ____UpdateComplete_36; }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 ** get_address_of__UpdateComplete_36() { return &____UpdateComplete_36; }
	inline void set__UpdateComplete_36(UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * value)
	{
		____UpdateComplete_36 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateComplete_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONANIMATOR_T8A6205140214563D0BA33F43CB9A6989D9AA1DFD_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef SKELETONGRAPHIC_T5BE1CA256FF1DA1D464015B2E074DFB6B910106A_H
#define SKELETONGRAPHIC_T5BE1CA256FF1DA1D464015B2E074DFB6B910106A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonGraphic
struct  SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// Spine.Unity.SkeletonDataAsset Spine.Unity.SkeletonGraphic::skeletonDataAsset
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * ___skeletonDataAsset_30;
	// System.String Spine.Unity.SkeletonGraphic::initialSkinName
	String_t* ___initialSkinName_31;
	// System.Boolean Spine.Unity.SkeletonGraphic::initialFlipX
	bool ___initialFlipX_32;
	// System.Boolean Spine.Unity.SkeletonGraphic::initialFlipY
	bool ___initialFlipY_33;
	// System.String Spine.Unity.SkeletonGraphic::startingAnimation
	String_t* ___startingAnimation_34;
	// System.Boolean Spine.Unity.SkeletonGraphic::startingLoop
	bool ___startingLoop_35;
	// System.Single Spine.Unity.SkeletonGraphic::timeScale
	float ___timeScale_36;
	// System.Boolean Spine.Unity.SkeletonGraphic::freeze
	bool ___freeze_37;
	// System.Boolean Spine.Unity.SkeletonGraphic::unscaledTime
	bool ___unscaledTime_38;
	// UnityEngine.Texture Spine.Unity.SkeletonGraphic::overrideTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___overrideTexture_39;
	// Spine.Skeleton Spine.Unity.SkeletonGraphic::skeleton
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * ___skeleton_40;
	// Spine.AnimationState Spine.Unity.SkeletonGraphic::state
	AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * ___state_41;
	// Spine.Unity.MeshGenerator Spine.Unity.SkeletonGraphic::meshGenerator
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * ___meshGenerator_42;
	// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh> Spine.Unity.SkeletonGraphic::meshBuffers
	DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC * ___meshBuffers_43;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.SkeletonGraphic::currentInstructions
	SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * ___currentInstructions_44;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateLocal
	UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * ___UpdateLocal_45;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateWorld
	UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * ___UpdateWorld_46;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateComplete
	UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * ___UpdateComplete_47;
	// Spine.Unity.MeshGeneratorDelegate Spine.Unity.SkeletonGraphic::OnPostProcessVertices
	MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4 * ___OnPostProcessVertices_48;

public:
	inline static int32_t get_offset_of_skeletonDataAsset_30() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___skeletonDataAsset_30)); }
	inline SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * get_skeletonDataAsset_30() const { return ___skeletonDataAsset_30; }
	inline SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 ** get_address_of_skeletonDataAsset_30() { return &___skeletonDataAsset_30; }
	inline void set_skeletonDataAsset_30(SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5 * value)
	{
		___skeletonDataAsset_30 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_30), value);
	}

	inline static int32_t get_offset_of_initialSkinName_31() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___initialSkinName_31)); }
	inline String_t* get_initialSkinName_31() const { return ___initialSkinName_31; }
	inline String_t** get_address_of_initialSkinName_31() { return &___initialSkinName_31; }
	inline void set_initialSkinName_31(String_t* value)
	{
		___initialSkinName_31 = value;
		Il2CppCodeGenWriteBarrier((&___initialSkinName_31), value);
	}

	inline static int32_t get_offset_of_initialFlipX_32() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___initialFlipX_32)); }
	inline bool get_initialFlipX_32() const { return ___initialFlipX_32; }
	inline bool* get_address_of_initialFlipX_32() { return &___initialFlipX_32; }
	inline void set_initialFlipX_32(bool value)
	{
		___initialFlipX_32 = value;
	}

	inline static int32_t get_offset_of_initialFlipY_33() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___initialFlipY_33)); }
	inline bool get_initialFlipY_33() const { return ___initialFlipY_33; }
	inline bool* get_address_of_initialFlipY_33() { return &___initialFlipY_33; }
	inline void set_initialFlipY_33(bool value)
	{
		___initialFlipY_33 = value;
	}

	inline static int32_t get_offset_of_startingAnimation_34() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___startingAnimation_34)); }
	inline String_t* get_startingAnimation_34() const { return ___startingAnimation_34; }
	inline String_t** get_address_of_startingAnimation_34() { return &___startingAnimation_34; }
	inline void set_startingAnimation_34(String_t* value)
	{
		___startingAnimation_34 = value;
		Il2CppCodeGenWriteBarrier((&___startingAnimation_34), value);
	}

	inline static int32_t get_offset_of_startingLoop_35() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___startingLoop_35)); }
	inline bool get_startingLoop_35() const { return ___startingLoop_35; }
	inline bool* get_address_of_startingLoop_35() { return &___startingLoop_35; }
	inline void set_startingLoop_35(bool value)
	{
		___startingLoop_35 = value;
	}

	inline static int32_t get_offset_of_timeScale_36() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___timeScale_36)); }
	inline float get_timeScale_36() const { return ___timeScale_36; }
	inline float* get_address_of_timeScale_36() { return &___timeScale_36; }
	inline void set_timeScale_36(float value)
	{
		___timeScale_36 = value;
	}

	inline static int32_t get_offset_of_freeze_37() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___freeze_37)); }
	inline bool get_freeze_37() const { return ___freeze_37; }
	inline bool* get_address_of_freeze_37() { return &___freeze_37; }
	inline void set_freeze_37(bool value)
	{
		___freeze_37 = value;
	}

	inline static int32_t get_offset_of_unscaledTime_38() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___unscaledTime_38)); }
	inline bool get_unscaledTime_38() const { return ___unscaledTime_38; }
	inline bool* get_address_of_unscaledTime_38() { return &___unscaledTime_38; }
	inline void set_unscaledTime_38(bool value)
	{
		___unscaledTime_38 = value;
	}

	inline static int32_t get_offset_of_overrideTexture_39() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___overrideTexture_39)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_overrideTexture_39() const { return ___overrideTexture_39; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_overrideTexture_39() { return &___overrideTexture_39; }
	inline void set_overrideTexture_39(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___overrideTexture_39 = value;
		Il2CppCodeGenWriteBarrier((&___overrideTexture_39), value);
	}

	inline static int32_t get_offset_of_skeleton_40() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___skeleton_40)); }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * get_skeleton_40() const { return ___skeleton_40; }
	inline Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 ** get_address_of_skeleton_40() { return &___skeleton_40; }
	inline void set_skeleton_40(Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782 * value)
	{
		___skeleton_40 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_40), value);
	}

	inline static int32_t get_offset_of_state_41() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___state_41)); }
	inline AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * get_state_41() const { return ___state_41; }
	inline AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 ** get_address_of_state_41() { return &___state_41; }
	inline void set_state_41(AnimationState_t8C505E02BE0DB4858362AA74C02FB30085250DE0 * value)
	{
		___state_41 = value;
		Il2CppCodeGenWriteBarrier((&___state_41), value);
	}

	inline static int32_t get_offset_of_meshGenerator_42() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___meshGenerator_42)); }
	inline MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * get_meshGenerator_42() const { return ___meshGenerator_42; }
	inline MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 ** get_address_of_meshGenerator_42() { return &___meshGenerator_42; }
	inline void set_meshGenerator_42(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157 * value)
	{
		___meshGenerator_42 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_42), value);
	}

	inline static int32_t get_offset_of_meshBuffers_43() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___meshBuffers_43)); }
	inline DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC * get_meshBuffers_43() const { return ___meshBuffers_43; }
	inline DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC ** get_address_of_meshBuffers_43() { return &___meshBuffers_43; }
	inline void set_meshBuffers_43(DoubleBuffered_1_tDCF0B541C868608A99A7F2E4671A22FFA00AC5CC * value)
	{
		___meshBuffers_43 = value;
		Il2CppCodeGenWriteBarrier((&___meshBuffers_43), value);
	}

	inline static int32_t get_offset_of_currentInstructions_44() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___currentInstructions_44)); }
	inline SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * get_currentInstructions_44() const { return ___currentInstructions_44; }
	inline SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A ** get_address_of_currentInstructions_44() { return &___currentInstructions_44; }
	inline void set_currentInstructions_44(SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A * value)
	{
		___currentInstructions_44 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_44), value);
	}

	inline static int32_t get_offset_of_UpdateLocal_45() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___UpdateLocal_45)); }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * get_UpdateLocal_45() const { return ___UpdateLocal_45; }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 ** get_address_of_UpdateLocal_45() { return &___UpdateLocal_45; }
	inline void set_UpdateLocal_45(UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * value)
	{
		___UpdateLocal_45 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateLocal_45), value);
	}

	inline static int32_t get_offset_of_UpdateWorld_46() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___UpdateWorld_46)); }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * get_UpdateWorld_46() const { return ___UpdateWorld_46; }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 ** get_address_of_UpdateWorld_46() { return &___UpdateWorld_46; }
	inline void set_UpdateWorld_46(UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * value)
	{
		___UpdateWorld_46 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateWorld_46), value);
	}

	inline static int32_t get_offset_of_UpdateComplete_47() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___UpdateComplete_47)); }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * get_UpdateComplete_47() const { return ___UpdateComplete_47; }
	inline UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 ** get_address_of_UpdateComplete_47() { return &___UpdateComplete_47; }
	inline void set_UpdateComplete_47(UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513 * value)
	{
		___UpdateComplete_47 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateComplete_47), value);
	}

	inline static int32_t get_offset_of_OnPostProcessVertices_48() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A, ___OnPostProcessVertices_48)); }
	inline MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4 * get_OnPostProcessVertices_48() const { return ___OnPostProcessVertices_48; }
	inline MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4 ** get_address_of_OnPostProcessVertices_48() { return &___OnPostProcessVertices_48; }
	inline void set_OnPostProcessVertices_48(MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4 * value)
	{
		___OnPostProcessVertices_48 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostProcessVertices_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGRAPHIC_T5BE1CA256FF1DA1D464015B2E074DFB6B910106A_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2900[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (IkConstraint_tAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2902[5] = 
{
	IkConstraint_tAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E::get_offset_of_data_0(),
	IkConstraint_tAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E::get_offset_of_bones_1(),
	IkConstraint_tAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E::get_offset_of_target_2(),
	IkConstraint_tAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E::get_offset_of_mix_3(),
	IkConstraint_tAF54ED4A58DEF27DE79E3C3B2CD84D035AAB063E::get_offset_of_bendDirection_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2903[6] = 
{
	IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C::get_offset_of_name_0(),
	IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C::get_offset_of_order_1(),
	IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C::get_offset_of_bones_2(),
	IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C::get_offset_of_target_3(),
	IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C::get_offset_of_bendDirection_4(),
	IkConstraintData_tD7BD226278FDA94E290E113485A026AA3EE8DB1C::get_offset_of_mix_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (Json_tCF01EE62E034C72AAFCD42776B9708C8D856DB44), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (MathUtils_t2C1A76C4955650106EF6D283FF52B27B651316E0), -1, sizeof(MathUtils_t2C1A76C4955650106EF6D283FF52B27B651316E0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2906[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	MathUtils_t2C1A76C4955650106EF6D283FF52B27B651316E0_StaticFields::get_offset_of_sin_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2907[17] = 
{
	0,
	0,
	0,
	0,
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_data_4(),
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_bones_5(),
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_target_6(),
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_position_7(),
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_spacing_8(),
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_rotateMix_9(),
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_translateMix_10(),
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_spaces_11(),
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_positions_12(),
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_world_13(),
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_curves_14(),
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_lengths_15(),
	PathConstraint_tA8A49113C18D00DA507EE4708F2063363C77DCA0::get_offset_of_segments_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[12] = 
{
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827::get_offset_of_name_0(),
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827::get_offset_of_order_1(),
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827::get_offset_of_bones_2(),
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827::get_offset_of_target_3(),
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827::get_offset_of_positionMode_4(),
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827::get_offset_of_spacingMode_5(),
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827::get_offset_of_rotateMode_6(),
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827::get_offset_of_offsetRotation_7(),
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827::get_offset_of_position_8(),
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827::get_offset_of_spacing_9(),
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827::get_offset_of_rotateMix_10(),
	PathConstraintData_tC6928338E396BBEFBD013C1C62F935D968992827::get_offset_of_translateMix_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (PositionMode_tD2BDC3F988FFDCB312D7B450C21B396BCE2D0570)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2909[3] = 
{
	PositionMode_tD2BDC3F988FFDCB312D7B450C21B396BCE2D0570::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (SpacingMode_t1BDCF0168CC650A8579A9348522FB739B433E66A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2910[4] = 
{
	SpacingMode_t1BDCF0168CC650A8579A9348522FB739B433E66A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (RotateMode_t9D7DDC5DCACDAFA1AF43170AF895DDF3AEE0C79B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2911[4] = 
{
	RotateMode_t9D7DDC5DCACDAFA1AF43170AF895DDF3AEE0C79B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2912[19] = 
{
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_data_0(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_bones_1(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_slots_2(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_drawOrder_3(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_ikConstraints_4(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_transformConstraints_5(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_pathConstraints_6(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_updateCache_7(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_updateCacheReset_8(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_skin_9(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_r_10(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_g_11(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_b_12(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_a_13(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_time_14(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_flipX_15(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_flipY_16(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_x_17(),
	Skeleton_t9909A1E7C80D3C45B0A1F8483D3A680C9AB5D782::get_offset_of_y_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054), -1, sizeof(SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2913[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054::get_offset_of_U3CScaleU3Ek__BackingField_13(),
	SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054::get_offset_of_attachmentLoader_14(),
	SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054::get_offset_of_buffer_15(),
	SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054::get_offset_of_linkedMeshes_16(),
	SkeletonBinary_t7C428D1C0AD9A5D73F4F5B9667D3E63332C33054_StaticFields::get_offset_of_TransformModeValues_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (Vertices_t2F849B9930066327474D954DD7286F4208BC18A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[2] = 
{
	Vertices_t2F849B9930066327474D954DD7286F4208BC18A3::get_offset_of_bones_0(),
	Vertices_t2F849B9930066327474D954DD7286F4208BC18A3::get_offset_of_vertices_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2915[7] = 
{
	SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11::get_offset_of_polygonPool_0(),
	SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11::get_offset_of_minX_1(),
	SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11::get_offset_of_minY_2(),
	SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11::get_offset_of_maxX_3(),
	SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11::get_offset_of_maxY_4(),
	SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11::get_offset_of_U3CBoundingBoxesU3Ek__BackingField_5(),
	SkeletonBounds_tE88393981F88D5BB5933BA1BA7CF337B81A7DF11::get_offset_of_U3CPolygonsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (Polygon_t6627F1043ADF312F9255C0DDD6FC0354CEFE0D7A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2916[2] = 
{
	Polygon_t6627F1043ADF312F9255C0DDD6FC0354CEFE0D7A::get_offset_of_U3CVerticesU3Ek__BackingField_0(),
	Polygon_t6627F1043ADF312F9255C0DDD6FC0354CEFE0D7A::get_offset_of_U3CCountU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2917[9] = 
{
	SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5::get_offset_of_triangulator_0(),
	SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5::get_offset_of_clippingPolygon_1(),
	SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5::get_offset_of_clipOutput_2(),
	SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5::get_offset_of_clippedVertices_3(),
	SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5::get_offset_of_clippedTriangles_4(),
	SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5::get_offset_of_clippedUVs_5(),
	SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5::get_offset_of_scratch_6(),
	SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5::get_offset_of_clipAttachment_7(),
	SkeletonClipping_tDC815E68228278D3942B5AF150FCEB7EAA3F4FF5::get_offset_of_clippingPolygons_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2918[16] = 
{
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_name_0(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_bones_1(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_slots_2(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_skins_3(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_defaultSkin_4(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_events_5(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_animations_6(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_ikConstraints_7(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_transformConstraints_8(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_pathConstraints_9(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_width_10(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_height_11(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_version_12(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_hash_13(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_fps_14(),
	SkeletonData_t585E301AA70D4EFBC1032C86E0A8FD9B752BBA20::get_offset_of_imagesPath_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (SkeletonJson_t657D553E1492D5959AEF75BCA0F551C15449E4EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[3] = 
{
	SkeletonJson_t657D553E1492D5959AEF75BCA0F551C15449E4EB::get_offset_of_U3CScaleU3Ek__BackingField_0(),
	SkeletonJson_t657D553E1492D5959AEF75BCA0F551C15449E4EB::get_offset_of_attachmentLoader_1(),
	SkeletonJson_t657D553E1492D5959AEF75BCA0F551C15449E4EB::get_offset_of_linkedMeshes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (LinkedMesh_t40F8CC003EFF4865870FB4881D56E9A4E02FBD35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2920[4] = 
{
	LinkedMesh_t40F8CC003EFF4865870FB4881D56E9A4E02FBD35::get_offset_of_parent_0(),
	LinkedMesh_t40F8CC003EFF4865870FB4881D56E9A4E02FBD35::get_offset_of_skin_1(),
	LinkedMesh_t40F8CC003EFF4865870FB4881D56E9A4E02FBD35::get_offset_of_slotIndex_2(),
	LinkedMesh_t40F8CC003EFF4865870FB4881D56E9A4E02FBD35::get_offset_of_mesh_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2921[2] = 
{
	Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3::get_offset_of_name_0(),
	Skin_t8F012B72B8B5F23850687C4ECA4712913276C0B3::get_offset_of_attachments_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (AttachmentKeyTuple_tF6B72B8FC590685692B39913EDAA7D910E816946)+ sizeof (RuntimeObject), sizeof(AttachmentKeyTuple_tF6B72B8FC590685692B39913EDAA7D910E816946_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2922[3] = 
{
	AttachmentKeyTuple_tF6B72B8FC590685692B39913EDAA7D910E816946::get_offset_of_slotIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttachmentKeyTuple_tF6B72B8FC590685692B39913EDAA7D910E816946::get_offset_of_name_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttachmentKeyTuple_tF6B72B8FC590685692B39913EDAA7D910E816946::get_offset_of_nameHashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (AttachmentKeyTupleComparer_t72271CF22CD25E9065A4A91EF1BD00613B609291), -1, sizeof(AttachmentKeyTupleComparer_t72271CF22CD25E9065A4A91EF1BD00613B609291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2923[1] = 
{
	AttachmentKeyTupleComparer_t72271CF22CD25E9065A4A91EF1BD00613B609291_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[13] = 
{
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_data_0(),
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_bone_1(),
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_r_2(),
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_g_3(),
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_b_4(),
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_a_5(),
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_r2_6(),
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_g2_7(),
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_b2_8(),
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_hasSecondColor_9(),
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_attachment_10(),
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_attachmentTime_11(),
	Slot_t6A54718FF0CEC915B6B31FE209FAF429CEF9DA51::get_offset_of_attachmentVertices_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2925[13] = 
{
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_index_0(),
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_name_1(),
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_boneData_2(),
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_r_3(),
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_g_4(),
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_b_5(),
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_a_6(),
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_r2_7(),
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_g2_8(),
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_b2_9(),
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_hasSecondColor_10(),
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_attachmentName_11(),
	SlotData_tDC4C24F4167DCD16DC14E99E265C6ED2F9851738::get_offset_of_blendMode_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2926[7] = 
{
	TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A::get_offset_of_data_0(),
	TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A::get_offset_of_bones_1(),
	TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A::get_offset_of_target_2(),
	TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A::get_offset_of_rotateMix_3(),
	TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A::get_offset_of_translateMix_4(),
	TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A::get_offset_of_scaleMix_5(),
	TransformConstraint_t1AB20C71BE4D18F200ED62ABB5953A7E0B58DC9A::get_offset_of_shearMix_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[16] = 
{
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_name_0(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_order_1(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_bones_2(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_target_3(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_rotateMix_4(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_translateMix_5(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_scaleMix_6(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_shearMix_7(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_offsetRotation_8(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_offsetX_9(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_offsetY_10(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_offsetScaleX_11(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_offsetScaleY_12(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_offsetShearY_13(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_relative_14(),
	TransformConstraintData_t6EC8091EE4BFF032946F6DCA6FDF89F853C1BBE8::get_offset_of_local_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2928[7] = 
{
	Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC::get_offset_of_convexPolygons_0(),
	Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC::get_offset_of_convexPolygonsIndices_1(),
	Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC::get_offset_of_indicesArray_2(),
	Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC::get_offset_of_isConcaveArray_3(),
	Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC::get_offset_of_triangles_4(),
	Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC::get_offset_of_polygonPool_5(),
	Triangulator_tA9050C221F20E12389B99839CFDC8CB3D8DFF8FC::get_offset_of_polygonIndicesPool_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433)+ sizeof (RuntimeObject), sizeof(BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2929[6] = 
{
	BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433::get_offset_of_a_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433::get_offset_of_b_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433::get_offset_of_c_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433::get_offset_of_d_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433::get_offset_of_x_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoneMatrix_tC33EDC47C7F65348ED57581B5302B35859F5B433::get_offset_of_y_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (SkeletonExtensions_t9B39ECE761E6685FEC80BFA351559A3FBFC8453C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2931[4] = 
{
	0,
	AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB::get_offset_of_skeletonDataAsset_5(),
	AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB::get_offset_of_animationName_6(),
	AnimationReferenceAsset_tE5971853301FB052014721823A240B976A89BDCB::get_offset_of_animation_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2932[3] = 
{
	AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47::get_offset_of_atlasFile_4(),
	AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47::get_offset_of_materials_5(),
	AtlasAsset_tA2047C4BE3E91B4E1BFB612269BC0D3381BF9C47::get_offset_of_atlas_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (MaterialsTextureLoader_tF62D3F45F7142E9015E50D858C4C5CFD1FF864FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2933[1] = 
{
	MaterialsTextureLoader_tF62D3F45F7142E9015E50D858C4C5CFD1FF864FF::get_offset_of_atlasAsset_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (EventDataReferenceAsset_t29895D35810EFF3CE3F67E931A1171ECDB397E28), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2934[4] = 
{
	0,
	EventDataReferenceAsset_t29895D35810EFF3CE3F67E931A1171ECDB397E28::get_offset_of_skeletonDataAsset_5(),
	EventDataReferenceAsset_t29895D35810EFF3CE3F67E931A1171ECDB397E28::get_offset_of_eventName_6(),
	EventDataReferenceAsset_t29895D35810EFF3CE3F67E931A1171ECDB397E28::get_offset_of_eventData_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (RegionlessAttachmentLoader_tBF85DAF1D0DA865CAC8D97AF79DE3D7FF3ED762C), -1, sizeof(RegionlessAttachmentLoader_tBF85DAF1D0DA865CAC8D97AF79DE3D7FF3ED762C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2935[1] = 
{
	RegionlessAttachmentLoader_tBF85DAF1D0DA865CAC8D97AF79DE3D7FF3ED762C_StaticFields::get_offset_of_emptyRegion_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2936[10] = 
{
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5::get_offset_of_atlasAssets_4(),
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5::get_offset_of_scale_5(),
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5::get_offset_of_skeletonJSON_6(),
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5::get_offset_of_fromAnimation_7(),
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5::get_offset_of_toAnimation_8(),
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5::get_offset_of_duration_9(),
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5::get_offset_of_defaultMix_10(),
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5::get_offset_of_controller_11(),
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5::get_offset_of_skeletonData_12(),
	SkeletonDataAsset_tFDB48C5757B5F7B7B1A34BB94EEF32A304916AE5::get_offset_of_stateData_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2937[11] = 
{
	BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0::get_offset_of_skeletonRenderer_4(),
	BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0::get_offset_of_boneName_5(),
	BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0::get_offset_of_followZPosition_6(),
	BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0::get_offset_of_followBoneRotation_7(),
	BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0::get_offset_of_followSkeletonFlip_8(),
	BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0::get_offset_of_followLocalScale_9(),
	BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0::get_offset_of_initializeOnAwake_10(),
	BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0::get_offset_of_valid_11(),
	BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0::get_offset_of_bone_12(),
	BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0::get_offset_of_skeletonTransform_13(),
	BoneFollower_t0C80DFBCC7EC5D3AB410853D20FF21B06919A4A0::get_offset_of_skeletonTransformIsParent_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[11] = 
{
	PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3::get_offset_of_skeletonRenderer_4(),
	PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3::get_offset_of_slotName_5(),
	PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3::get_offset_of_pointAttachmentName_6(),
	PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3::get_offset_of_followRotation_7(),
	PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3::get_offset_of_followSkeletonFlip_8(),
	PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3::get_offset_of_followSkeletonZPosition_9(),
	PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3::get_offset_of_skeletonTransform_10(),
	PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3::get_offset_of_skeletonTransformIsParent_11(),
	PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3::get_offset_of_point_12(),
	PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3::get_offset_of_bone_13(),
	PointFollower_tADE4812111BA2DDFAF5DE3764BE89A2CE2530DE3::get_offset_of_valid_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2939[7] = 
{
	SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D::get_offset_of_state_33(),
	SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D::get_offset_of__UpdateLocal_34(),
	SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D::get_offset_of__UpdateWorld_35(),
	SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D::get_offset_of__UpdateComplete_36(),
	SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D::get_offset_of__animationName_37(),
	SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D::get_offset_of_loop_38(),
	SkeletonAnimation_t24B5B36CBE1A2E8F96F4E8BDAC4E9B86BD5C507D::get_offset_of_timeScale_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (SkeletonAnimator_t8A6205140214563D0BA33F43CB9A6989D9AA1DFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[4] = 
{
	SkeletonAnimator_t8A6205140214563D0BA33F43CB9A6989D9AA1DFD::get_offset_of_translator_33(),
	SkeletonAnimator_t8A6205140214563D0BA33F43CB9A6989D9AA1DFD::get_offset_of__UpdateLocal_34(),
	SkeletonAnimator_t8A6205140214563D0BA33F43CB9A6989D9AA1DFD::get_offset_of__UpdateWorld_35(),
	SkeletonAnimator_t8A6205140214563D0BA33F43CB9A6989D9AA1DFD::get_offset_of__UpdateComplete_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2941[7] = 
{
	MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1::get_offset_of_autoReset_0(),
	MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1::get_offset_of_layerMixModes_1(),
	MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1::get_offset_of_animationTable_2(),
	MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1::get_offset_of_clipNameHashCodeTable_3(),
	MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1::get_offset_of_previousAnimations_4(),
	MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1::get_offset_of_layerClipInfos_5(),
	MecanimTranslator_tB7158724E1E6409994986C35527D525190B1E1D1::get_offset_of_animator_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (MixMode_tD6C282143E8C24B95BABD1A52AA415F5E550E20D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2942[4] = 
{
	MixMode_tD6C282143E8C24B95BABD1A52AA415F5E550E20D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2943[12] = 
{
	ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D::get_offset_of_isInterruptionActive_0(),
	ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D::get_offset_of_isLastFrameOfInterruption_1(),
	ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D::get_offset_of_clipInfoCount_2(),
	ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D::get_offset_of_nextClipInfoCount_3(),
	ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D::get_offset_of_interruptingClipInfoCount_4(),
	ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D::get_offset_of_clipInfos_5(),
	ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D::get_offset_of_nextClipInfos_6(),
	ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D::get_offset_of_interruptingClipInfos_7(),
	ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D::get_offset_of_stateInfo_8(),
	ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D::get_offset_of_nextStateInfo_9(),
	ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D::get_offset_of_interruptingStateInfo_10(),
	ClipInfos_t192425775CEDCFA54B70EE34A91B4765F20F037D::get_offset_of_interruptingClipTimeAddition_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (AnimationClipEqualityComparer_t70C5049B6149030ABD8CC89712AFB4E0C3AABADA), -1, sizeof(AnimationClipEqualityComparer_t70C5049B6149030ABD8CC89712AFB4E0C3AABADA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2944[1] = 
{
	AnimationClipEqualityComparer_t70C5049B6149030ABD8CC89712AFB4E0C3AABADA_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (IntEqualityComparer_t2F935A9DFA1A81628BB410C36C80AB41D12CB0D6), -1, sizeof(IntEqualityComparer_t2F935A9DFA1A81628BB410C36C80AB41D12CB0D6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2945[1] = 
{
	IntEqualityComparer_t2F935A9DFA1A81628BB410C36C80AB41D12CB0D6_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[29] = 
{
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_OnRebuild_4(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_OnPostProcessVertices_5(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_skeletonDataAsset_6(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_initialSkinName_7(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_initialFlipX_8(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_initialFlipY_9(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_separatorSlotNames_10(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_separatorSlots_11(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_zSpacing_12(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_useClipping_13(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_immutableTriangles_14(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_pmaVertexColors_15(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_clearStateOnDisable_16(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_tintBlack_17(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_singleSubmesh_18(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_addNormals_19(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_calculateTangents_20(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_logErrors_21(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_disableRenderingOnOverride_22(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_generateMeshOverride_23(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_customMaterialOverride_24(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_customSlotMaterials_25(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_meshRenderer_26(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_meshFilter_27(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_valid_28(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_skeleton_29(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_currentInstructions_30(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_meshGenerator_31(),
	SkeletonRenderer_t2966DEE83F2476605CF48837468900CB93AA7F03::get_offset_of_rendererBuffers_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (SkeletonRendererDelegate_tF51A39DB9B998C96CEEE3C8F0F55FB65070EBBF7), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (InstructionDelegate_t5D855F63CA53EA174005317091099D0BAA478F54), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (UpdateBonesDelegate_t945292DDB255F75682E84AB1D494E86CFED0D513), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2956[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (SpineMesh_tA74327C2396922F1F5CD0C46F2572A15DAAAD41C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2957[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[10] = 
{
	SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE::get_offset_of_skeleton_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE::get_offset_of_startSlot_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE::get_offset_of_endSlot_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE::get_offset_of_material_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE::get_offset_of_forceSeparate_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE::get_offset_of_preActiveClippingSlotSource_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE::get_offset_of_rawTriangleCount_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE::get_offset_of_rawVertexCount_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE::get_offset_of_rawFirstVertexIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_tCB674E0D4A772D4FA3A6F9D9E07304F7F909D2BE::get_offset_of_hasClipping_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (MeshGeneratorDelegate_tD6862EEC6BDB67C63F0EDCD4992FFCEAED10A7C4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2960[5] = 
{
	MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58::get_offset_of_vertexCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58::get_offset_of_vertexBuffer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58::get_offset_of_uvBuffer_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58::get_offset_of_colorBuffer_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_tF05F06E835D0C4A74373E59C5DE0753892CFBD58::get_offset_of_meshGenerator_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157), -1, sizeof(MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2961[23] = 
{
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_settings_0(),
	0,
	0,
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_vertexBuffer_3(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_uvBuffer_4(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_colorBuffer_5(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_submeshes_6(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_meshBoundsMin_7(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_meshBoundsMax_8(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_meshBoundsThickness_9(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_submeshIndex_10(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_clipper_11(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_tempVerts_12(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_regionTriangles_13(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_normals_14(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_tangents_15(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_tempTanBuffer_16(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_uv2_17(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157::get_offset_of_uv3_18(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157_StaticFields::get_offset_of_AttachmentVerts_19(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157_StaticFields::get_offset_of_AttachmentUVs_20(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157_StaticFields::get_offset_of_AttachmentColors32_21(),
	MeshGenerator_t9641A8F39530EFA0918ABD2F91E8BFC247FD4157_StaticFields::get_offset_of_AttachmentIndices_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D)+ sizeof (RuntimeObject), sizeof(Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2962[7] = 
{
	Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D::get_offset_of_useClipping_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D::get_offset_of_zSpacing_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D::get_offset_of_pmaVertexColors_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D::get_offset_of_tintBlack_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D::get_offset_of_calculateTangents_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D::get_offset_of_addNormals_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_tDAD9B4E9DC683D5ACC2903EE333276598006710D::get_offset_of_immutableTriangles_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2963[3] = 
{
	MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107::get_offset_of_doubleBufferedMesh_0(),
	MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107::get_offset_of_submeshMaterials_1(),
	MeshRendererBuffers_t4C818805F575A9FB385E0AC7F4E8FD56623AA107::get_offset_of_sharedMaterials_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (SmartMesh_t600BE08EF446BE08EEA7958EEFE801B34037E068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2964[2] = 
{
	SmartMesh_t600BE08EF446BE08EEA7958EEFE801B34037E068::get_offset_of_mesh_0(),
	SmartMesh_t600BE08EF446BE08EEA7958EEFE801B34037E068::get_offset_of_instructionUsed_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[5] = 
{
	SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A::get_offset_of_immutableTriangles_0(),
	SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A::get_offset_of_submeshInstructions_1(),
	SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A::get_offset_of_hasActiveClipping_2(),
	SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A::get_offset_of_rawVertexCount_3(),
	SkeletonRendererInstruction_t20D144DA4F6A8EC7612E4C61064D6B7DE3DA0C4A::get_offset_of_attachments_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B), -1, sizeof(BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2966[11] = 
{
	BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B_StaticFields::get_offset_of_DebugMessages_4(),
	BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B::get_offset_of_skeletonRenderer_5(),
	BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B::get_offset_of_slotName_6(),
	BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B::get_offset_of_isTrigger_7(),
	BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B::get_offset_of_clearStateOnDisable_8(),
	BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B::get_offset_of_slot_9(),
	BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B::get_offset_of_currentAttachment_10(),
	BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B::get_offset_of_currentAttachmentName_11(),
	BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B::get_offset_of_currentCollider_12(),
	BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B::get_offset_of_colliderTable_13(),
	BoundingBoxFollower_t1626DCCB976CB582AAEB134FB4899D667D14E80B::get_offset_of_nameTable_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2967[11] = 
{
	BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4::get_offset_of_skeletonGraphic_4(),
	BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4::get_offset_of_initializeOnAwake_5(),
	BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4::get_offset_of_boneName_6(),
	BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4::get_offset_of_followBoneRotation_7(),
	BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4::get_offset_of_followSkeletonFlip_8(),
	BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4::get_offset_of_followLocalScale_9(),
	BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4::get_offset_of_followZPosition_10(),
	BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4::get_offset_of_bone_11(),
	BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4::get_offset_of_skeletonTransform_12(),
	BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4::get_offset_of_skeletonTransformIsParent_13(),
	BoneFollowerGraphic_tFC6E6AFD5E07114998D4D6DAF64146AC8D8F1BD4::get_offset_of_valid_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2968[19] = 
{
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_skeletonDataAsset_30(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_initialSkinName_31(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_initialFlipX_32(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_initialFlipY_33(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_startingAnimation_34(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_startingLoop_35(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_timeScale_36(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_freeze_37(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_unscaledTime_38(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_overrideTexture_39(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_skeleton_40(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_state_41(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_meshGenerator_42(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_meshBuffers_43(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_currentInstructions_44(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_UpdateLocal_45(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_UpdateWorld_46(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_UpdateComplete_47(),
	SkeletonGraphic_t5BE1CA256FF1DA1D464015B2E074DFB6B910106A::get_offset_of_OnPostProcessVertices_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (WaitForSpineAnimationComplete_t46B9D7E64E134CDC3520A1750BB1B6B4743D1D34), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2969[1] = 
{
	WaitForSpineAnimationComplete_t46B9D7E64E134CDC3520A1750BB1B6B4743D1D34::get_offset_of_m_WasFired_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (WaitForSpineEvent_tA8441A771D71E682D87CE453D33368F15177278A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2970[5] = 
{
	WaitForSpineEvent_tA8441A771D71E682D87CE453D33368F15177278A::get_offset_of_m_TargetEvent_0(),
	WaitForSpineEvent_tA8441A771D71E682D87CE453D33368F15177278A::get_offset_of_m_EventName_1(),
	WaitForSpineEvent_tA8441A771D71E682D87CE453D33368F15177278A::get_offset_of_m_AnimationState_2(),
	WaitForSpineEvent_tA8441A771D71E682D87CE453D33368F15177278A::get_offset_of_m_WasFired_3(),
	WaitForSpineEvent_tA8441A771D71E682D87CE453D33368F15177278A::get_offset_of_m_unsubscribeAfterFiring_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (WaitForSpineTrackEntryEnd_t6B8DE50A564C92F3E73A25FA6C245592B5D77776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[1] = 
{
	WaitForSpineTrackEntryEnd_t6B8DE50A564C92F3E73A25FA6C245592B5D77776::get_offset_of_m_WasFired_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (SkeletonExtensions_t66E50F5258A0713714C11F389E7FFA78EAB66699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2972[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[9] = 
{
	SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0::get_offset_of_OnReset_4(),
	SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0::get_offset_of_boneRoot_5(),
	SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0::get_offset_of_skeletonRenderer_6(),
	SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0::get_offset_of_skeletonAnimation_7(),
	SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0::get_offset_of_utilityBones_8(),
	SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0::get_offset_of_utilityConstraints_9(),
	SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0::get_offset_of_hasTransformBones_10(),
	SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0::get_offset_of_hasUtilityConstraints_11(),
	SkeletonUtility_tDCD74FBD80AFA061668B110AEFADF4FE5929C9B0::get_offset_of_needToReprocessBones_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (SkeletonUtilityDelegate_t40A11BA492FE4C516EBAF8D63834116D5865092B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2975[15] = 
{
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_boneName_4(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_parentReference_5(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_mode_6(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_position_7(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_rotation_8(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_scale_9(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_zPosition_10(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_overrideAlpha_11(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_skeletonUtility_12(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_bone_13(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_transformLerpComplete_14(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_valid_15(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_cachedTransform_16(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_skeletonTransform_17(),
	SkeletonUtilityBone_t0C75ECC56D7800633F25C86F36D4915DD9934D39::get_offset_of_incompatibleTransformMode_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (Mode_t72E168DBEE144A1374AA8A46C48943B70BE23CAE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2976[3] = 
{
	Mode_t72E168DBEE144A1374AA8A46C48943B70BE23CAE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (UpdatePhase_t08F95CD9252DCA7FE22968C8933654BF534C4178)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2977[4] = 
{
	UpdatePhase_t08F95CD9252DCA7FE22968C8933654BF534C4178::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (SkeletonUtilityConstraint_tC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2978[2] = 
{
	SkeletonUtilityConstraint_tC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0::get_offset_of_utilBone_4(),
	SkeletonUtilityConstraint_tC6C1D6B95304403F9EFCCAD28FAF7AE4C7EC18B0::get_offset_of_skeletonUtility_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[4] = 
{
	SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764::get_offset_of_dataField_0(),
	SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764::get_offset_of_startsWith_1(),
	SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764::get_offset_of_includeNone_2(),
	SpineAttributeBase_t9B9F73E3A2B3FD57076801623D1610A959DE7764::get_offset_of_fallbackToTextField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (SpineSlot_tD60AA77C245C9340EA797F092E25E231D641DACD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2980[1] = 
{
	SpineSlot_tD60AA77C245C9340EA797F092E25E231D641DACD::get_offset_of_containsBoundingBoxes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (SpineEvent_tDD867EFA230B93A502C0509421FB41D137E1823B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (SpineIkConstraint_t3F02582BB7C221CFD1CDA00B7DE7455B1D6F0731), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (SpinePathConstraint_t5332F6467925E378ADBC480ACBBF6AED70B4ABF2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (SpineTransformConstraint_tC537B1A9801E08AF9BD2958A75DB6BE68196491F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (SpineSkin_t2F9AA1C8D5422D2C57A36817C5EE0A2CC3216E8D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (SpineAnimation_tDC6BD9FC4A4C86FA45DE0724A9C4C0CC7AA0C210), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (SpineAttachment_t8C50B5C8D70F662B66900A40043A9EE6E72510DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2987[5] = 
{
	SpineAttachment_t8C50B5C8D70F662B66900A40043A9EE6E72510DE::get_offset_of_returnAttachmentPath_4(),
	SpineAttachment_t8C50B5C8D70F662B66900A40043A9EE6E72510DE::get_offset_of_currentSkinOnly_5(),
	SpineAttachment_t8C50B5C8D70F662B66900A40043A9EE6E72510DE::get_offset_of_placeholdersOnly_6(),
	SpineAttachment_t8C50B5C8D70F662B66900A40043A9EE6E72510DE::get_offset_of_skinField_7(),
	SpineAttachment_t8C50B5C8D70F662B66900A40043A9EE6E72510DE::get_offset_of_slotField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (Hierarchy_t983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8)+ sizeof (RuntimeObject), sizeof(Hierarchy_t983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2988[3] = 
{
	Hierarchy_t983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8::get_offset_of_skin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Hierarchy_t983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8::get_offset_of_slot_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Hierarchy_t983A91CD8085F935AD3EA08BA9AEB4737D9CA6D8::get_offset_of_name_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (SpineBone_t689D9B8ABFA81DC532671502108C180F3CFB7B35), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (SpineAtlasRegion_tC958B6954F6EC3B396C047E2177627075103E68C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2990[1] = 
{
	SpineAtlasRegion_tC958B6954F6EC3B396C047E2177627075103E68C::get_offset_of_atlasAssetField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (SkeletonAnimationPlayableHandle_t791E2A077EA0F1F27D928E1272890E82F59E2113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2991[1] = 
{
	SkeletonAnimationPlayableHandle_t791E2A077EA0F1F27D928E1272890E82F59E2113::get_offset_of_skeletonAnimation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (SpineEventDelegate_tE33FEE8B9A419F03B4559C77F6CA2E23F1AD8FC7), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (SpinePlayableHandleBase_tB5F200A1F15E5F638384F2B4B26C076C4E4810AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2993[1] = 
{
	SpinePlayableHandleBase_tB5F200A1F15E5F638384F2B4B26C076C4E4810AC::get_offset_of_AnimationEvents_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2994[7] = 
{
	SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57::get_offset_of_animationReference_0(),
	SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57::get_offset_of_loop_1(),
	SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57::get_offset_of_customDuration_2(),
	SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57::get_offset_of_mixDuration_3(),
	SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57::get_offset_of_attachmentThreshold_4(),
	SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57::get_offset_of_eventThreshold_5(),
	SpineAnimationStateBehaviour_tDA89BEA20BC9F29A3204FA9E0AE3E648E40E6D57::get_offset_of_drawOrderThreshold_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (SpineAnimationStateClip_tBB4F4FD5BB543784F15A417D9AE652C034ADCCBB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2995[1] = 
{
	SpineAnimationStateClip_tBB4F4FD5BB543784F15A417D9AE652C034ADCCBB::get_offset_of_template_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (SpineAnimationStateMixerBehaviour_tF81EF839803A8347EC0558CEBEB489A2DE102E59), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2996[1] = 
{
	SpineAnimationStateMixerBehaviour_tF81EF839803A8347EC0558CEBEB489A2DE102E59::get_offset_of_lastInputWeights_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (SpineAnimationStateTrack_t85DEB3FA9EDAE7A6326A51D4A0A208E0C9678659), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (SpineSkeletonFlipMixerBehaviour_t63713C4E085F70991F4886646AAFD6AB265C986F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2998[4] = 
{
	SpineSkeletonFlipMixerBehaviour_t63713C4E085F70991F4886646AAFD6AB265C986F::get_offset_of_defaultFlipX_0(),
	SpineSkeletonFlipMixerBehaviour_t63713C4E085F70991F4886646AAFD6AB265C986F::get_offset_of_defaultFlipY_1(),
	SpineSkeletonFlipMixerBehaviour_t63713C4E085F70991F4886646AAFD6AB265C986F::get_offset_of_playableHandle_2(),
	SpineSkeletonFlipMixerBehaviour_t63713C4E085F70991F4886646AAFD6AB265C986F::get_offset_of_m_FirstFrameHappened_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (SpineSkeletonFlipTrack_t4AB91DCA36CA7BA88FE8CA96A9626AB42097E127), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
